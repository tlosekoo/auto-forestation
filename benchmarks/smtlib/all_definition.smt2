(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (nlist 0) (elist 0) (etree 0) (epair 0) (npair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nnil)
			(ncons (nhd nat) (ntl nlist))
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
		(
			(epairc (efst elt) (esnd elt))
		)
		(
			(npairc (nfst nat) (nsnd nat))
		)
	)
)



(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec leq_elt ((e1 elt) (e2 elt)) Bool
	(
		match e1
		(
			(a true)
			(b 
				(
					match e2
					( 
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec le_elt ((e1 elt) (e2 elt)) Bool
	(
		match e1
		(
			(b false)
			(a 
				(
					match e2
					( 
						(a false)
						(b true)
					)
				)
			)
		)
	)
)




(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)


(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(define-fun-rec not_zero ((n nat)) Bool
	(
		match n
		(	
			(z false)
			((s nn) true)
		)
	)
)

(define-fun-rec is_even ((n nat)) Bool 
	(
		match n
		(
			(z true)
			((s nn) (
						match nn
						(
							(z false)
							((s n3) (is_even n3))
						)
					)
			)
		)
	)
)

(define-fun-rec is_odd ((n nat)) Bool 
	(
		match n
		(
			(z false)
			((s nn) (
						match nn
						(
							(z true)
							((s n3) (is_odd n3))
						)
					)
			)
		)
	)
)

(define-fun-rec  max_rec
  ((x nat) (y nat)) nat
  (match x
    ((z y)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (s (max_rec u x2)))))))))

(define-fun-rec  max_ite
  ((x nat) (y nat)) nat
  (ite (leq_nat x y) y x)
)
(define-fun-rec  min_ite
  ((x nat) (y nat)) nat
  (ite (leq_nat x y) x y)
)


(define-fun-rec double ((n nat)) nat 
	(
		match n
		(
			(z z)
			((s nn) (s (s (double nn))))
		)
	)
)







(define-fun-rec not_nil_elt ((l elist)) Bool
	(
		match l
		( 
			(enil false)
			( (econs x ll) true)
		)
	)
)

(define-fun-rec take_elt ((x nat) (y elist)) elist
  (match x
    ((z enil)
     ((s u)
      (match y
        ((enil enil)
         ((econs x2 x3) (econs x2 (take_elt u x3)))))))))


(define-fun-rec length_elt ((l elist)) nat
	(
		match l 
		(
			(  enil    z)
			( (econs x ll) (s (length_elt ll)))
		)
	)
)
(define-fun-rec length_elt_relation ((l elist) (n nat)) Bool
	(
		match l 
		(
			(  enil    
				(match n
					(
						(z true)
						((s nn) false)
					)
				)
			)
			( (econs x ll) 
				(match n
					(
						(z false)
						((s nn) (length_elt_relation ll nn))
					)
				)
			)
		)
	)
)



(define-fun-rec last ((x nlist)) nat
  (match x
    ((nnil z)
     ((ncons y u)
      (match u
        ((nnil y)
         ((ncons x2 x3) (last (ncons x2 x3)))))))))
		 
(define-fun-rec length_nat ((l nlist)) nat
	(
		match l 
		(
			(  nnil    z)
			( (ncons x ll) (s (length_nat ll)))
		)
	)
)

(define-fun-rec count_elt ((x elt) (l elist)) nat
	(
		match l
		(
			(enil z)
			((econs h1 t1) (ite (= h1 x) (s (count_elt x t1)) (count_elt x t1)))
		)
	)
)


(define-fun-rec append_elt ((l1 elist) (l2 elist)) elist 
	(
		match l1
		(
			(enil l2)
			((econs h1 t1) (econs h1 (append_elt t1 l2)))
		)
	)
)
(define-fun-rec append_nat ((l1 nlist) (l2 nlist)) nlist 
	(
		match l1
		(
			(nnil l2)
			((ncons h1 t1) (ncons h1 (append_nat t1 l2)))
		)
	)
)

(define-fun-rec delete_all_elt ((x elt) (l elist)) elist
  (match l
  (
    (enil enil)
	((econs h t) (ite (= x h) (delete_all_elt x t) (econs h (delete_all_elt x t))))
  )
  )
)
(define-fun-rec delete_one_elt ((x elt) (l elist)) elist
  (match l
  (
    (enil enil)
	((econs h t) (ite (= x h) t (econs h (delete_one_elt x t))))
  )
  )
)

(define-fun-rec mem_elt ((e elt) (l elist)) Bool 
	(
		match l
		(
			(enil false)
			((econs h t) (ite (= e h) true (mem_elt e t)))
		)
	)
)

(define-fun-rec insert_elt ((x elt) (l elist)) elist
	(
		match l
		(
			(enil (econs x enil))
			((econs y z) (ite (leq_elt x y) (econs x (econs y z)) (econs y (insert_elt x z))))
		)
	)
)


(define-fun-rec insert_nat ((x nat) (l nlist)) nlist
	(
		match l
		(
			(nnil (ncons x nnil))
			((ncons y z) (ite (leq_nat x y) (ncons x (ncons y z)) (ncons y (insert_nat x z))))
		)
	)
)

(define-fun-rec sort_elt ((l elist)) elist
	(
		match l
		(
			(enil enil)
			((econs y z) (insert_elt y (sort_elt z))
		)
	)
))

(define-fun-rec sort_nat ((l nlist)) nlist
	(
		match l
		(
			(nnil nnil)
			((ncons y z) (insert_nat y (sort_nat z))
		)
	)
))

(define-fun-rec sorted_elt ((l elist)) Bool
	(
		match l 
		(
			(  enil    true)
			( (econs x xs) 
				(
					match xs 
					(
						(enil true)
						((econs y xss) (ite (leq_elt x y) (sorted_elt xs) false))
					)
				)
			)
		)
	)
)

(define-fun-rec drop_elt ((n nat) (l elist)) elist
  (match n
    (
		(z l)
     	((s u)
	      (match l
		  	(
		        (enil enil)
		        ((econs x2 x3) (drop_elt u x3))
			)
		  )
		)
	)
  )
)


(define-fun-rec prefix_elt ((p elist) (l elist)) Bool
  (match p
  	(
    	(enil true)
		((econs z zs)
      	   (match l
			  (
        		(enil false)
	    		((econs y2 ys)
		          (ite
        		    (= z y2) (prefix_elt zs ys) false))
			  )
			)
		)
	)
  )
)

(define-fun-rec prefix_nat ((p nlist) (l nlist)) Bool
  (match p
  	(
    	(nnil true)
		((ncons z zs)
      	   (match l
			  (
        		(nnil false)
	    		((ncons y2 ys)
		          (ite
        		    (= z y2) (prefix_nat zs ys) false))
			  )
			)
		)
	)
  )
)


(define-fun-rec numnodes ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (plus (numnodes t1) (numnodes t2))))
		)
	)
)


(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
;;						((s m) (and (shallower t1 m) (shallower t2 m)))
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)

(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (and (subtree ta1 tb1) (subtree ta2 tb2)) false)
						)

					)
				)
			)
		)
	)
)

(define-fun-rec memt_elt ((e elt)(t etree)) Bool
	(
		match t
		(
			(leaf false)
			((node e2 t1 t2) (ite (= e e2) true (ite (memt_elt e t1) true (memt_elt e t2))))
		)
	)
)

(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (max_ite (height t1) (height t2))))
		)
	)
)

(define-fun-rec depth ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (ite (leq_nat (depth t1) (depth t2)) (depth t2) (depth t1)))
			)
		)
	)
)

(define-fun-rec flip ((t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)

(define-fun swap_elt ((p epair)) epair
	(
		match p 
		(
			((epairc fst snd) (epairc snd fst))
		)
	)
)

(define-fun swap_nat ((p npair)) npair
	(
		match p 
		(
			((npairc fst snd) (npairc snd fst))
		)
	)
)