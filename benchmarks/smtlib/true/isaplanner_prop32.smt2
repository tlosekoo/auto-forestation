(set-logic HORN)


(declare-datatypes ((nat 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
	)
)

(define-fun-rec min ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z z)
         ((s y1) (s (min u y1)))))))))

(assert
    (forall ((a nat) (b nat))
      (= (min a b) (min b a)))
)

(check-sat)
