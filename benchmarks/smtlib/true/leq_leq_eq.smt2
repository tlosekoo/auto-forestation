(set-logic HORN)


(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec eqnat ((x nat) (y nat)) Bool
  (match x
    ((z
      (match y
        ((z true)
         ((s u) false))))
     ((s x2)
      (match y
        ((z false)
         ((s y2) (eqnat x2 y2))))))))

(define-fun-rec leq ((x nat) (y nat)) Bool
  (
	match x
	(
    	(z true)
     	((s z)
 	     	(
				match y
				(
					(z false)
			        ((s x2) (leq z x2))
				)
			)
		)
	)
  )
)

(assert
	(forall ((n nat) (m nat))
		(=> (and (leq n m) (leq m n)) (eqnat n m)))
)

(check-sat)