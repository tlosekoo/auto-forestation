(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec prefix ((x eltlist) (y eltlist)) Bool
  (match x
  	(
    	(nil true)
		((cons z zs)
      	   (match y
			  (
        		(nil false)
	    		((cons y2 ys)
		          (ite
        		    (= z y2) (prefix zs ys) false))
			  )
			)
		)
	)
  )
)


(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(assert (forall ((l1 eltlist) (l2 eltlist))
			(prefix l1 (append l1 l2))))

(check-sat)
