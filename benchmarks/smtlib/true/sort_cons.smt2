(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)


(define-fun-rec leq ((x elt)(y elt)) Bool
	(
		match x
		(
			(a true)
			(b 
				(
					match y 
					(
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec insert ((x elt) (l eltlist)) eltlist
	(
		match l
		(
			(nil (cons x nil))
			((cons y z) (ite (leq x y) (cons x (cons y z)) (cons y (insert x z))))
		)
	)
)

(define-fun-rec sort ((l eltlist)) eltlist
	(
		match l
		(
			(nil nil)
			((cons y z) (insert y (sort z))
		)
	)
))

(assert
	(forall ((l eltlist)) 
		(= (sort (cons a l)) (cons a (sort l)))
	)
)

(check-sat)