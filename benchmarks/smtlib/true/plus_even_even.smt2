(set-logic HORN)

(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))


(define-fun-rec is_even ((n nat)) Bool 
	(
		match n
		(
			(z true)
			((s nn) (
						match nn
						(
							(z false)
							((s n3) (is_even n3))
						)
					)
			)
		)
	)
)

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

; Property
; even x /\ even y --> even(plus(x,y))
(assert (forall ((x nat)(y nat)) 
    (=> (and (is_even x) (is_even y))
        (is_even (plus x y)))))


(check-sat)