(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z true)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(define-fun-rec count ((x elt) (l eltlist)) nat
	(
		match l
		(
			(nil z)
			((cons h1 t1) (ite (= h1 x) (s (count x t1)) (count x t1)))
		)
	)
)


(assert (forall ((x elt) (l eltlist))
			(leq (count x l) (length l))))

(check-sat)
