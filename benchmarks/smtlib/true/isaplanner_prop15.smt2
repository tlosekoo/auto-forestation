(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec leq ((x elt)(y elt)) Bool
	(
		match x
		(
			(a true)
			(b 
				(
					match y 
					(
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec insert ((x elt) (l eltlist)) eltlist
	(
		match l
		(
			(nil (cons x nil))
			((cons y z) (ite (leq x y) (cons x (cons y z)) (cons y (insert x z))))
		)
	)
)


(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(define-fun-rec leqnat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z true)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leqnat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(assert
	(forall ((x elt) (l eltlist)) 
		(le_nat (length l) (length (insert x l))))
)

(check-sat)