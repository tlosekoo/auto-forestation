
(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec take ((x nat) (y eltlist)) eltlist
  (match x
    ((z nil)
     ((s u)
      (match y
        ((nil nil)
         ((cons x2 x3) (cons x2 (take u x3)))))))))


(assert (forall ((xs eltlist)) 
      (= (take z xs) nil)))
      
(check-sat)
