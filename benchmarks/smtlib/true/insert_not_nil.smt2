(set-logic HORN)

(declare-datatypes ((elt 0) (elist 0)) 
	(
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)


(define-fun-rec not_nil_elt ((l elist)) Bool
	(
		match l
		( 
			(enil false)
			( (econs x ll) true)
		)
	)
)


(define-fun-rec leq_elt ((e1 elt) (e2 elt)) Bool
	(
		match e1
		(
			(a true)
			(b 
				(
					match e2
					( 
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec insert_elt ((x elt) (l elist)) elist
	(
		match l
		(
			(enil (econs x enil))
			((econs y z) (ite (leq_elt x y) (econs x (econs y z)) (econs y (insert_elt x z))))
		)
	)
)

(assert (forall ((l elist) (e elt) )
				(not_nil_elt (insert_elt e l))))


(check-sat)
