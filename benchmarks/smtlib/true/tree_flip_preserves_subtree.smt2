(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec flip ((t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)


(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (subtree ta1 tb1) (subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)


(assert 
	(forall ((s etree) (t etree))
		(=> (subtree s t) (subtree (flip s) (flip t)))
	)
)

(check-sat)
