(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z)
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)


(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)



(assert (forall ((l1 eltlist) (l2 eltlist))
			(= (length (append l1 l2)) (length (append l2 l1)))))



(check-sat)
