(set-logic HORN)


(declare-datatypes ((nat 0) (nat_bin_tree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(leaf)
			(node (hd nat) (ls nat_bin_tree) (rs nat_bin_tree))
		)
	)
)


(assert (forall ((ea nat) (ta1 nat_bin_tree) (ta2 nat_bin_tree) (eb nat) (tb1 nat_bin_tree) (tb2 nat_bin_tree))
			(=> (= (node ea ta1 ta2) (node eb tb1 tb2)) 
			(= ea eb))))
(assert (forall ((ea nat) (ta1 nat_bin_tree) (ta2 nat_bin_tree) (eb nat) (tb1 nat_bin_tree) (tb2 nat_bin_tree))
			(=> (= (node ea ta1 ta2) (node eb tb1 tb2)) 
			(= ta1 tb1))))
(assert (forall ((ea nat) (ta1 nat_bin_tree) (ta2 nat_bin_tree) (eb nat) (tb1 nat_bin_tree) (tb2 nat_bin_tree))
			(=> (= (node ea ta1 ta2) (node eb tb1 tb2)) 
			(= ta2 tb2))))


(check-sat)
