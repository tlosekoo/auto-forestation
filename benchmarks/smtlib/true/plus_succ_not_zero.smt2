(set-logic HORN)


(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(define-fun-rec not_zero ((n nat)) Bool
	(
		match n
		(	
			(z false)
			((s nn) true)
		)
	)
)

(assert
	(forall ((n nat) (m nat)) 
		(not_zero (plus (s n) m)))
)

(check-sat)