(set-logic HORN)


(declare-datatypes ((nat 0)(natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)


(define-fun-rec append ((l1 natlist) (l2 natlist)) natlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec member ((e nat) (l natlist)) Bool 
	(
		match l
		(
			(nil false)
			((cons h t) (ite (= e h) true (member e t)))
		)
	)
)

(assert (forall ((e nat) (l natlist))
			(member e (append l (cons e nil)))))


(check-sat)
