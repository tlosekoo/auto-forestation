(set-logic HORN)


(declare-datatypes ((nat 0)(elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)


(define-fun-rec count ((x elt) (l eltlist)) nat
	(
		match l
		(
			(nil z)
			((cons h1 t1) (ite (= h1 x) (s (count x t1)) (count x t1)))
		)
	)
)

(assert (forall ((x elt)(l1 eltlist))
			(= (count x (cons x l1)) (s (count x l1)))))

(check-sat)
