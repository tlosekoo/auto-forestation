(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)

(define-fun-rec append_elt ((l1 elist) (l2 elist)) elist 
	(
		match l1
		(
			(enil l2)
			((econs h1 t1) (econs h1 (append_elt t1 l2)))
		)
	)
)

(define-fun-rec length_elt ((l elist)) nat
	(
		match l 
		(
			(  enil    z)
			( (econs x ll) (s (length_elt ll)))
		)
	)
)

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(assert (forall ((l1 elist) (l2 elist) )
				(= (length_elt (append_elt l1 l2)) (plus (length_elt l1) (length_elt l2))))
)



(check-sat)
