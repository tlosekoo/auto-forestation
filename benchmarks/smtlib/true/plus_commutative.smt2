(set-logic HORN)


(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(assert
	(forall ((n nat) (m nat)) 
		(= (plus n m) (plus m n)))
)

(check-sat)