(set-logic HORN)


(declare-datatypes ((elt 0) (nat 0) (eltlist 0)) 
	(
		(
			(a)
			(b)
		)
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec le ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(define-fun fcons ((x elt) (l eltlist)) eltlist
	(cons x l)
)


(assert (forall ((x elt) (l eltlist))
			(le (length l) (length (fcons x l)))))

(check-sat)
