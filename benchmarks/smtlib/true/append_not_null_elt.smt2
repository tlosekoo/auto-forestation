(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)


(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec not_null ((l eltlist)) Bool
	(
		match l
		( 
			(nil false)
			( (cons x ll) true)
		)
	)
)

(assert (forall ((i elt) (l1 eltlist) (l2 eltlist))
		(=> (not_null l1) (not_null (append l1 l2)))))

(assert (forall ((i elt) (l1 eltlist) (l2 eltlist))
		(=> (not_null l2) (not_null (append l1 l2)))))


(check-sat)
