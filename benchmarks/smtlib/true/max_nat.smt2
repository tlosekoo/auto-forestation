(set-logic HORN)


(declare-datatypes ((nat 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
	)
)

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec max ((n nat)(m nat)) nat
	(ite (leq n m) m n))

(assert (forall ((i nat) (j nat))
		(leq i (max i j))))
		
(assert (forall ((i nat) (j nat))
		(leq j (max i j))))


(check-sat)
