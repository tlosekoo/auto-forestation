(set-logic HORN)


(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)

(assert
	(forall ((n nat) (m nat)) 
		(=> (leq m n) (leq m (s n)))))

(check-sat)