(set-logic HORN)


(declare-datatypes ((nat 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
	)
)

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec
  max
  ((x nat) (y nat)) nat
  (match x
    ((z y)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (s (max u x2)))))))))

(assert (forall ((i nat) (j nat))
		(=> (= (max i j) j) (leq i j))))
		
(assert (forall ((i nat) (j nat))
		(=> (leq i j) (= (max i j) j))))

(check-sat)
