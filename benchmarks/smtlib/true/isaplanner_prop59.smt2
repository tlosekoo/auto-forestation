(set-logic HORN)


(declare-datatypes ((nat 0)(natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)

(define-fun-rec append ((l1 natlist) (l2 natlist)) natlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec last ((x natlist)) nat
  (match x
    ((nil z)
     ((cons y u)
      (match u
        ((nil y)
         ((cons x2 x3) (last (cons x2 x3)))))))))

(assert (forall ((xs natlist) (ys natlist)) 
      (=> (= ys nil) (= (last (append xs ys)) (last xs)))))
	  
(check-sat)
