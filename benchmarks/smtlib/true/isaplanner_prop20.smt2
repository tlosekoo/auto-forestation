(set-logic HORN)


(declare-datatypes ((nat 0)(natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nnil)
			(ncons (nhd nat) (ntl natlist))
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec insert_nat ((x nat) (l natlist)) natlist
	(
		match l
		(
			(nnil (ncons x nnil))
			((ncons y z) (ite (leq_nat x y) (ncons x (ncons y z)) (ncons y (insert_nat x z))))
		)
	)
)

(define-fun-rec sort_nat ((l natlist)) natlist
	(
		match l
		(
			(nnil nnil)
			((ncons y z) (insert_nat y (sort_nat z))
		)
	)
))


(define-fun-rec length_nat ((l natlist)) nat
	(
		match l 
		(
			(  nnil    z)
			( (ncons x ll) (s (length_nat ll)))
		)
	)
)


(assert
	(forall ((l natlist)) 
		(= (length_nat l) (length_nat (sort_nat l)))
	)
)

(check-sat)