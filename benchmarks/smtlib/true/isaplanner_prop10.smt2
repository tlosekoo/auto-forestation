(set-logic HORN)


(declare-datatypes ((nat 0))
	(
		(
			(z) 
			(s (pred nat))
		)
  )
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    (
      (z z)
      ((s u)
        (match y
          (
            (z (s u))
            ((s x2) (minus u x2))
          )
        )
      )
    )
  )
)

(assert (forall ((m nat)) (= (minus m m) z)))

(check-sat)
