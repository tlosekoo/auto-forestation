(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)



(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(define-fun-rec numnodes ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (plus (numnodes t1) (numnodes t2))))
		)
	)
)

(define-fun-rec member ((e elt)(t etree)) Bool
	(
		match t
		(
			(leaf false)
			((node e2 t1 t2) (ite (= e e2) true (ite (member e t1) true (member e t2))))
		)
	)
)

(assert (forall ((e elt) (t1 etree))
			(=> (member e t1) (leq (s z) (numnodes t1)))))


(check-sat)
