(set-logic HORN)


(declare-datatypes ((nat 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
	)
)

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec min ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z z)
         ((s y1) (s (min u y1)))))))))

(assert (forall ((i nat) (j nat))
		(=> (= (min i j) j) (leq j i))))
		
(assert (forall ((i nat) (j nat))
		(=> (leq j i) (= (min i j) j))))

(check-sat)
