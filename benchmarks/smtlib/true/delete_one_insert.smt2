(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)

(define-fun-rec leq_elt ((e1 elt) (e2 elt)) Bool
	(
		match e1
		(
			(a true)
			(b 
				(
					match e2
					( 
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec insert_elt ((x elt) (l elist)) elist
	(
		match l
		(
			(enil (econs x enil))
			((econs y z) (ite (leq_elt x y) (econs x (econs y z)) (econs y (insert_elt x z))))
		)
	)
)

(define-fun-rec delete_one_elt ((x elt) (l elist)) elist
  (match l
  (
    (enil enil)
	((econs h t) (ite (= x h) t (econs h (delete_one_elt x t))))
  )
  )
)


(assert (forall ((l elist) (e elt))
				(= (delete_one_elt e (insert_elt e l)) l)
				)
)


(check-sat)
