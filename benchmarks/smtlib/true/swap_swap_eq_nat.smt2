(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (nlist 0) (elist 0) (etree 0) (epair 0) (npair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nnil)
			(ncons (nhd nat) (ntl nlist))
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
		(
			(epairc (efst elt) (esnd elt))
		)
		(
			(npairc (nfst nat) (nsnd nat))
		)
	)
)

(define-fun swap_nat ((p npair)) npair
	(
		match p 
		(
			((npairc fst snd) (npairc snd fst))
		)
	)
)


(assert
	(forall ((p npair))
		(= p (swap_nat (swap_nat p)))
	)
)

(check-sat)