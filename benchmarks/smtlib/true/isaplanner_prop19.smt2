(set-logic HORN)


(declare-datatypes ((nat 0)(elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec drop ((x nat) (l eltlist)) eltlist
  (match x
    (
		(z l)
     	((s u)
	      (match l
		  	(
		        (nil nil)
		        ((cons x2 x3) (drop u x3))
			)
		  )
		)
	)
  )
)


(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(assert (forall ((n nat)(l1 eltlist))
			(= (length (drop n l1)) (minus (length l1) n))))

(check-sat)
