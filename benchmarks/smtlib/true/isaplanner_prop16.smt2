(set-logic HORN)


(declare-datatypes ((nat 0)(natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)

(define-fun-rec last ((l natlist)) nat
	(
		match l
		(
			(nil z)
			((cons y u) 
			 (
				match u
				(
					(nil y)
					((cons x2 x3) (last (cons x2 x3)))
				)
			 )
			)
		)
	)
)


(assert
    (forall ((x nat) (xs natlist))
      (=> (= xs nil) (= (last (cons x xs)) x))))



(check-sat)