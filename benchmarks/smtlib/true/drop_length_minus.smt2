(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)

(define-fun-rec drop_elt ((n nat) (l elist)) elist
  (match n
    (
		(z l)
     	((s u)
	      (match l
		  	(
		        (enil enil)
		        ((econs x2 x3) (drop_elt u x3))
			)
		  )
		)
	)
  )
)




(define-fun-rec length_elt ((l elist)) nat
	(
		match l 
		(
			(  enil    z)
			( (econs x ll) (s (length_elt ll)))
		)
	)
)


(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))


(assert (forall ((l elist) (n nat))
				(= (length_elt (drop_elt n l)) (minus (length_elt l) n)))
)



(check-sat)
