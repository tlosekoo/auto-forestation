
(declare-datatypes ((nat 0)(elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec take ((x nat) (y eltlist)) eltlist
  (match x
    ((z nil)
     ((s u)
      (match y
        ((nil nil)
         ((cons x2 x3) (cons x2 (take u x3)))))))))

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    (
      (z z)
      ((s u)
        (match y
          (
            (z (s u))
            ((s x2) (minus u x2))
          )
        )
      )
    )
  )
)

(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(assert
     (forall ((n nat) (xs eltlist) (ys eltlist))
      (= (take n (append xs ys))
        (append (take n xs) (take (minus n (length xs)) ys)))))

(check-sat)
