(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						(z false)
						((s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec max ((n nat)(m nat)) nat
	(
		match n 
		(
			(z m)
			((s nn) 
				(
					match m
					(
						(z n)
						((s mm) (s (max nn mm)))
					)
				)
			)
		)
	)
)

(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (max (height t1) (height t2)))
			)
		)
	)
)

(define-fun-rec height_rb ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (height_rb t2))
			)
		)
	)
)

(assert (forall ((e elt)(t1 etree) (t2 etree))
			(leq_nat (height_rb t2) (height t2))))


(check-sat)
