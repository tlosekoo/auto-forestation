(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)



(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						(z false)
						((s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec max ((n nat)(m nat)) nat
	(ite (leq_nat n m) m n))


(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (max (height t1) (height t2))))
		)
	)
)

(define-fun-rec numnodes ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (plus (numnodes t1) (numnodes t2))))
		)
	)
)

(assert (forall ((t1 etree))
			(le_nat (height t1) (numnodes t1))))


(check-sat)
