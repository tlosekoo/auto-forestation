(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)



(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec max ((n nat)(m nat)) nat
	(ite (leq_nat n m) m n))


(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (ite (leq_nat (height t1) (height t2)) (height t2) (height t1)))
			)
		)
	)
)


(assert (forall ((e elt) (t1 etree) (t2 etree) (m nat))
			(=> (= m (max (height t1) (height t2)))
				(leq_nat (height (node e t1 t2)) m))))



(check-sat)
