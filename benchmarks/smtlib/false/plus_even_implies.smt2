(set-logic HORN)

(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))


(define-fun-rec is_even ((n nat)) Bool 
	(
		match n
		(
			(z true)
			((s nn) (
						match nn
						(
							(z false)
							((s n3) (is_even n3))
						)
					)
			)
		)
	)
)

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)


; Property (false) 
; even(plus(x,y)) --> even(x) /\ even(y)
(assert (forall ((x nat)(y nat)) 
    (=> (is_even (plus x y))
		(is_even x))))

(check-sat)