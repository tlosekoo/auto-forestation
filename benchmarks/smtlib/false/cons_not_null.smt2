(set-logic HORN)

(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)

(define-fun-rec not_null ((l natlist)) Bool
	(
		match l
		( 
			(nil false)
			( (cons x ll) true)
		)	
	)
)

(assert (forall ((i nat) (l1 natlist))
			(=> (not_null (cons i l1))
				(not_null l1))))



(check-sat)
