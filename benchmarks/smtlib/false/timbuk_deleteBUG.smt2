(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec delete ((x elt) (l eltlist)) eltlist
  (match l
  (
    (nil nil)
	((cons h t) (ite (= x h) (delete x t) (cons h (delete x t))))
  )
  )
)

(define-fun-rec mem ((e elt) (l eltlist)) Bool 
	(
		match l
		(
			(nil false)
			((cons h t) (ite (= e h) true (mem e t)))
		)
	)
)

(assert (forall ((i elt) (l eltlist))
			(mem i (delete i l))))


(check-sat)
