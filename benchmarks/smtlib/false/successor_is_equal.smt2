(set-logic HORN)

(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))


(assert
	(forall ((x nat)) (= x (s x)))
)

(check-sat)
