(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec delete_one ((x elt) (l eltlist)) eltlist
  (
    match l
    (
      (nil nil)
      ((cons y r) (ite (= x y) r (cons y (delete_one x r))))
    )
  )
)

(define-fun-rec delete_all ((x elt) (l eltlist)) eltlist
  (
    match l
    (
      (nil nil)
      ((cons y r) (ite (= x y) (delete_all x r) (cons y (delete_all x r))))
    )
  )
)

(define-fun-rec count ((x elt) (l eltlist)) nat
  (
    match l
    (
      (nil z)
      ((cons y r) (ite (= x y) (s (count x r)) (count x r)))
    )
  )
)

(assert (forall ((x elt)(l eltlist))
    (=> (= (delete_one x l) (delete_all x l))
        (= (count x l) (s z)))))

(check-sat)