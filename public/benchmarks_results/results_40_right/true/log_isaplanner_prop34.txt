Solving ../../../../benchmarks/smtlib/true/isaplanner_prop34.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(min, F:
{() -> min([s(u), z, z])
() -> min([z, y, z])
(min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)])}
(min([_eh, _fh, _gh]) /\ min([_eh, _fh, _hh])) -> eq_nat([_gh, _hh])
)
}

properties:
{(leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]), (min([i, j, j])) -> leq([j, i])}


over-approximation: {min}
under-approximation: {}

Clause system for inference is:

{
() -> leq([z, n2]) -> 0  ;  () -> min([s(u), z, z]) -> 0  ;  () -> min([z, y, z]) -> 0  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (min([i, j, j])) -> leq([j, i]) -> 0  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 0
}


Solving took 0.087605 seconds.
Proved
Model:
|_
{
leq -> 
{{{
Q={q_gen_2871, q_gen_2874},
Q_f={q_gen_2871},
Delta=
{
<s>(q_gen_2874) -> q_gen_2874
<z>() -> q_gen_2874
<s, s>(q_gen_2871) -> q_gen_2871
<z, s>(q_gen_2874) -> q_gen_2871
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<s>(q_gen_2870) -> q_gen_2870
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, s, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005002 s (model generation: 0.004778,  model checking: 0.000224):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> min([s(u), z, z]) -> 0  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (min([i, j, j])) -> leq([j, i]) -> 1  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 1
}
Sat witness: Yes: (() -> min([z, y, z]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.005315 s (model generation: 0.005133,  model checking: 0.000182):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868},
Q_f={q_gen_2868},
Delta=
{
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (min([i, j, j])) -> leq([j, i]) -> 1  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 1
}
Sat witness: Yes: (() -> min([s(u), z, z]), {
u -> z
})

-------------------------------------------
Step 2, which took 0.007671 s (model generation: 0.007567,  model checking: 0.000104):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (min([i, j, j])) -> leq([j, i]) -> 1  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 3, which took 0.013043 s (model generation: 0.008690,  model checking: 0.004353):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871},
Q_f={q_gen_2871},
Delta=
{
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (min([i, j, j])) -> leq([j, i]) -> 1  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: ((min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]), {
_dh -> z  ;  u -> z  ;  y1 -> z
})

-------------------------------------------
Step 4, which took 0.009560 s (model generation: 0.009461,  model checking: 0.000099):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871},
Q_f={q_gen_2871},
Delta=
{
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (min([i, j, j])) -> leq([j, i]) -> 4  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: ((min([i, j, j])) -> leq([j, i]), {
i -> s(z)  ;  j -> z
})

-------------------------------------------
Step 5, which took 0.007176 s (model generation: 0.006971,  model checking: 0.000205):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871, q_gen_2874},
Q_f={q_gen_2871},
Delta=
{
<z>() -> q_gen_2874
<z, s>(q_gen_2874) -> q_gen_2871
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (min([i, j, j])) -> leq([j, i]) -> 4  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 6, which took 0.011597 s (model generation: 0.010879,  model checking: 0.000718):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871, q_gen_2874},
Q_f={q_gen_2871},
Delta=
{
<z>() -> q_gen_2874
<s, s>(q_gen_2871) -> q_gen_2871
<z, s>(q_gen_2874) -> q_gen_2871
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 6  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 2  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (min([i, j, j])) -> leq([j, i]) -> 4  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: (() -> min([z, y, z]), {
y -> s(z)
})

-------------------------------------------
Step 7, which took 0.015151 s (model generation: 0.011473,  model checking: 0.003678):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871, q_gen_2874},
Q_f={q_gen_2871},
Delta=
{
<z>() -> q_gen_2874
<s, s>(q_gen_2871) -> q_gen_2871
<z, s>(q_gen_2874) -> q_gen_2871
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, s, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> min([s(u), z, z]) -> 6  ;  () -> min([z, y, z]) -> 6  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (min([i, j, j])) -> leq([j, i]) -> 4  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: (() -> min([s(u), z, z]), {
u -> s(z)
})

-------------------------------------------
Step 8, which took 0.007200 s (model generation: 0.007065,  model checking: 0.000135):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2871, q_gen_2874},
Q_f={q_gen_2871},
Delta=
{
<z>() -> q_gen_2874
<s, s>(q_gen_2871) -> q_gen_2871
<z, s>(q_gen_2874) -> q_gen_2871
<z, z>() -> q_gen_2871
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  min -> 
{{{
Q={q_gen_2868, q_gen_2870},
Q_f={q_gen_2868},
Delta=
{
<s>(q_gen_2870) -> q_gen_2870
<z>() -> q_gen_2870
<s, s, s>(q_gen_2868) -> q_gen_2868
<s, z, z>(q_gen_2870) -> q_gen_2868
<z, s, z>(q_gen_2870) -> q_gen_2868
<z, z, z>() -> q_gen_2868
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> min([s(u), z, z]) -> 6  ;  () -> min([z, y, z]) -> 6  ;  (leq([j, i]) /\ min([i, j, _jh])) -> eq_nat([_jh, j]) -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (min([i, j, j])) -> leq([j, i]) -> 4  ;  (min([u, y1, _dh])) -> min([s(u), s(y1), s(_dh)]) -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(s(z))
})

Total time: 0.087605
Reason for stopping: Proved

