Solving ../../../../benchmarks/smtlib/true/append_not_null_nat.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(append, F:
{() -> append([nil, l2, l2])
(append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)])}
(append([_hta, _ita, _jta]) /\ append([_hta, _ita, _kta])) -> eq_natlist([_jta, _kta])
)
(not_null, P:
{() -> not_null([cons(x, ll)])
(not_null([nil])) -> BOT}
)
}

properties:
{(append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]), (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta])}


over-approximation: {append}
under-approximation: {}

Clause system for inference is:

{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 0  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 0  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 0  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 0  ;  (not_null([nil])) -> BOT -> 0
}


Solving took 40.000176 seconds.
DontKnow. Stopped because: timeout
Working model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_613, q_gen_614, q_gen_615, q_gen_619, q_gen_620, q_gen_621, q_gen_622, q_gen_623, q_gen_624, q_gen_625, q_gen_626, q_gen_627, q_gen_628, q_gen_629, q_gen_630, q_gen_631, q_gen_632, q_gen_633, q_gen_634, q_gen_635, q_gen_636, q_gen_637, q_gen_638, q_gen_639, q_gen_640, q_gen_641, q_gen_642, q_gen_643, q_gen_644, q_gen_645, q_gen_646, q_gen_647, q_gen_648, q_gen_649, q_gen_650, q_gen_651, q_gen_652, q_gen_653, q_gen_654, q_gen_655, q_gen_656, q_gen_657, q_gen_658, q_gen_659, q_gen_660, q_gen_661, q_gen_662, q_gen_663, q_gen_664, q_gen_665, q_gen_666, q_gen_667, q_gen_668, q_gen_669, q_gen_670, q_gen_671, q_gen_672, q_gen_673, q_gen_674, q_gen_675, q_gen_676, q_gen_677, q_gen_678, q_gen_679, q_gen_680, q_gen_681, q_gen_682, q_gen_683, q_gen_684, q_gen_685, q_gen_686},
Q_f={},
Delta=
{
<nil>() -> q_gen_626
<z>() -> q_gen_627
<cons>(q_gen_627, q_gen_626) -> q_gen_634
<s>(q_gen_627) -> q_gen_640
<s>(q_gen_664) -> q_gen_663
<s>(q_gen_640) -> q_gen_664
<nil, nil>() -> q_gen_614
<z, z>() -> q_gen_615
<s, s>(q_gen_615) -> q_gen_621
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_623
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_630
<cons, cons>(q_gen_621, q_gen_614) -> q_gen_646
<nil, cons>(q_gen_640, q_gen_626) -> q_gen_649
<s, z>(q_gen_627) -> q_gen_650
<s, s>(q_gen_650) -> q_gen_666
<z, s>(q_gen_627) -> q_gen_672
<s, z>(q_gen_640) -> q_gen_682
<nil, nil, nil>() -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_613
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_619
<cons, nil, cons>(q_gen_621, q_gen_614) -> q_gen_620
<nil, cons, cons>(q_gen_615, q_gen_623) -> q_gen_622
<cons, cons, cons>(q_gen_628, q_gen_625) -> q_gen_624
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_625
<z, z, z>() -> q_gen_628
<cons, nil, cons>(q_gen_615, q_gen_630) -> q_gen_629
<nil, cons, cons>(q_gen_615, q_gen_630) -> q_gen_631
<cons, cons, cons>(q_gen_628, q_gen_633) -> q_gen_632
<nil, nil, cons>(q_gen_627, q_gen_634) -> q_gen_633
<cons, cons, cons>(q_gen_636, q_gen_625) -> q_gen_635
<s, z, s>(q_gen_615) -> q_gen_636
<nil, cons, cons>(q_gen_621, q_gen_614) -> q_gen_637
<cons, cons, cons>(q_gen_641, q_gen_639) -> q_gen_638
<nil, nil, cons>(q_gen_640, q_gen_626) -> q_gen_639
<s, s, s>(q_gen_628) -> q_gen_641
<cons, cons, cons>(q_gen_641, q_gen_612) -> q_gen_642
<cons, cons, cons>(q_gen_644, q_gen_620) -> q_gen_643
<z, s, z>(q_gen_627) -> q_gen_644
<nil, cons, cons>(q_gen_615, q_gen_646) -> q_gen_645
<cons, cons, cons>(q_gen_636, q_gen_648) -> q_gen_647
<nil, cons, cons>(q_gen_650, q_gen_649) -> q_gen_648
<cons, cons, cons>(q_gen_636, q_gen_652) -> q_gen_651
<cons, cons, cons>(q_gen_654, q_gen_653) -> q_gen_652
<nil, cons, cons>(q_gen_650, q_gen_630) -> q_gen_653
<s, z, s>(q_gen_650) -> q_gen_654
<cons, cons, cons>(q_gen_628, q_gen_656) -> q_gen_655
<cons, cons, cons>(q_gen_636, q_gen_657) -> q_gen_656
<cons, cons, cons>(q_gen_658, q_gen_633) -> q_gen_657
<s, s, s>(q_gen_659) -> q_gen_658
<s, z, z>(q_gen_627) -> q_gen_659
<cons, cons, cons>(q_gen_665, q_gen_661) -> q_gen_660
<cons, cons, cons>(q_gen_662, q_gen_619) -> q_gen_661
<z, s, z>(q_gen_663) -> q_gen_662
<s, z, s>(q_gen_666) -> q_gen_665
<cons, cons, cons>(q_gen_636, q_gen_668) -> q_gen_667
<cons, cons, cons>(q_gen_669, q_gen_624) -> q_gen_668
<s, s, s>(q_gen_670) -> q_gen_669
<s, s, s>(q_gen_671) -> q_gen_670
<s, s, z>(q_gen_672) -> q_gen_671
<cons, cons, cons>(q_gen_675, q_gen_674) -> q_gen_673
<nil, cons, cons>(q_gen_650, q_gen_623) -> q_gen_674
<s, z, s>(q_gen_672) -> q_gen_675
<cons, cons, cons>(q_gen_636, q_gen_677) -> q_gen_676
<cons, cons, cons>(q_gen_678, q_gen_631) -> q_gen_677
<s, s, s>(q_gen_679) -> q_gen_678
<z, z, s>(q_gen_627) -> q_gen_679
<cons, cons, cons>(q_gen_675, q_gen_681) -> q_gen_680
<nil, cons, cons>(q_gen_682, q_gen_623) -> q_gen_681
<cons, cons, cons>(q_gen_636, q_gen_684) -> q_gen_683
<cons, cons, cons>(q_gen_685, q_gen_631) -> q_gen_684
<s, s, s>(q_gen_686) -> q_gen_685
<z, s, s>(q_gen_615) -> q_gen_686
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611, q_gen_616, q_gen_617, q_gen_618},
Q_f={},
Delta=
{
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<z>() -> q_gen_611
<cons>(q_gen_611, q_gen_617) -> q_gen_616
<cons>(q_gen_618, q_gen_610) -> q_gen_617
<s>(q_gen_611) -> q_gen_618
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.007385 s (model generation: 0.006328,  model checking: 0.001057):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 1  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 1  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> z
})

-------------------------------------------
Step 1, which took 0.008047 s (model generation: 0.007781,  model checking: 0.000266):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<nil>() -> q_gen_609
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 1  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 1  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> nil
})

-------------------------------------------
Step 2, which took 0.009838 s (model generation: 0.009806,  model checking: 0.000032):

Model:
|_
{
append -> 
{{{
Q={q_gen_612},
Q_f={q_gen_612},
Delta=
{
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<nil>() -> q_gen_609
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 1  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 1  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 1  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((not_null([nil])) -> BOT, {

})

-------------------------------------------
Step 3, which took 0.009072 s (model generation: 0.008911,  model checking: 0.000161):

Model:
|_
{
append -> 
{{{
Q={q_gen_612},
Q_f={q_gen_612},
Delta=
{
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 1  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 1  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> nil  ;  h1 -> z  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 4, which took 0.011617 s (model generation: 0.007374,  model checking: 0.004243):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615},
Q_f={q_gen_612},
Delta=
{
<nil, nil>() -> q_gen_614
<z, z>() -> q_gen_615
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 2  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 2  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> cons(s(z), nil)  ;  x -> z
})

-------------------------------------------
Step 5, which took 0.011477 s (model generation: 0.010728,  model checking: 0.000749):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615},
Q_f={q_gen_612},
Delta=
{
<nil, nil>() -> q_gen_614
<z, z>() -> q_gen_615
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 3  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 3  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> cons(z, nil)
})

-------------------------------------------
Step 6, which took 0.014277 s (model generation: 0.011423,  model checking: 0.002854):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615},
Q_f={q_gen_612},
Delta=
{
<nil, nil>() -> q_gen_614
<z, z>() -> q_gen_615
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 4  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 4  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 7  ;  (not_null([nil])) -> BOT -> 5
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> nil  ;  h1 -> s(z)  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 7, which took 0.010321 s (model generation: 0.010092,  model checking: 0.000229):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615},
Q_f={q_gen_612},
Delta=
{
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 9  ;  () -> not_null([cons(x, ll)]) -> 7  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 5  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 5  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 7  ;  (not_null([nil])) -> BOT -> 6
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> cons(z, cons(z, nil))
})

-------------------------------------------
Step 8, which took 0.009043 s (model generation: 0.007623,  model checking: 0.001420):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615},
Q_f={q_gen_612},
Delta=
{
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 9  ;  () -> not_null([cons(x, ll)]) -> 7  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 6  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 6  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 10  ;  (not_null([nil])) -> BOT -> 7
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(z, nil)  ;  h1 -> z  ;  l2 -> cons(z, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 9, which took 0.015916 s (model generation: 0.013208,  model checking: 0.002708):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<nil>() -> q_gen_626
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 10  ;  () -> not_null([cons(x, ll)]) -> 8  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 7  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 7  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 13  ;  (not_null([nil])) -> BOT -> 8
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(z, nil)  ;  h1 -> z  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 10, which took 0.016057 s (model generation: 0.013134,  model checking: 0.002923):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<nil>() -> q_gen_626
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 11  ;  () -> not_null([cons(x, ll)]) -> 9  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 8  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 8  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 16  ;  (not_null([nil])) -> BOT -> 9
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(z, cons(z, nil))  ;  h1 -> z  ;  l2 -> cons(z, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 11, which took 0.020528 s (model generation: 0.013222,  model checking: 0.007306):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 12  ;  () -> not_null([cons(x, ll)]) -> 10  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 9  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 9  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 19  ;  (not_null([nil])) -> BOT -> 10
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(z, nil)  ;  h1 -> s(z)  ;  l2 -> cons(z, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 12, which took 0.020881 s (model generation: 0.015560,  model checking: 0.005321):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, z, s>(q_gen_615) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 13  ;  () -> not_null([cons(x, ll)]) -> 11  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 10  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 10  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 22  ;  (not_null([nil])) -> BOT -> 11
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(z), nil)  ;  h1 -> s(z)  ;  l2 -> cons(s(z), nil)  ;  t1 -> nil
})

-------------------------------------------
Step 13, which took 0.017517 s (model generation: 0.013319,  model checking: 0.004198):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 14  ;  () -> not_null([cons(x, ll)]) -> 12  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 11  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 11  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 25  ;  (not_null([nil])) -> BOT -> 12
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(z), nil)  ;  h1 -> z  ;  l2 -> cons(s(z), nil)  ;  t1 -> cons(s(z), nil)
})

-------------------------------------------
Step 14, which took 0.031632 s (model generation: 0.017168,  model checking: 0.014464):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<z, s, z>(q_gen_627) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 15  ;  () -> not_null([cons(x, ll)]) -> 13  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 12  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 12  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 28  ;  (not_null([nil])) -> BOT -> 13
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(z, cons(s(z), nil))  ;  h1 -> s(z)  ;  l2 -> cons(z, cons(s(z), nil))  ;  t1 -> nil
})

-------------------------------------------
Step 15, which took 0.031964 s (model generation: 0.013673,  model checking: 0.018291):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<s, z>(q_gen_627) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<z, s, z>(q_gen_627) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 16  ;  () -> not_null([cons(x, ll)]) -> 14  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 13  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 13  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 31  ;  (not_null([nil])) -> BOT -> 14
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(z), cons(s(z), cons(z, cons(z, nil))))  ;  h1 -> z  ;  l2 -> cons(z, cons(z, cons(s(z), nil)))  ;  t1 -> cons(s(z), cons(s(s(z)), nil))
})

-------------------------------------------
Step 16, which took 0.030593 s (model generation: 0.016421,  model checking: 0.014172):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<s, z>(q_gen_627) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<s, z, z>(q_gen_627) -> q_gen_628
<z, s, z>(q_gen_627) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 17  ;  () -> not_null([cons(x, ll)]) -> 15  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 14  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 14  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 34  ;  (not_null([nil])) -> BOT -> 15
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(s(z)), cons(z, cons(z, nil)))  ;  h1 -> s(z)  ;  l2 -> cons(z, cons(s(s(s(s(z)))), cons(z, nil)))  ;  t1 -> cons(s(s(s(z))), cons(z, nil))
})

-------------------------------------------
Step 17, which took 0.048730 s (model generation: 0.024515,  model checking: 0.024215):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<s, z>(q_gen_627) -> q_gen_615
<z, s>(q_gen_627) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, s, z>(q_gen_615) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<s, z, z>(q_gen_627) -> q_gen_628
<z, s, z>(q_gen_627) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 18  ;  () -> not_null([cons(x, ll)]) -> 16  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 15  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 15  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 37  ;  (not_null([nil])) -> BOT -> 16
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(s(z)), cons(z, cons(z, nil)))  ;  h1 -> s(z)  ;  l2 -> cons(z, cons(s(z), cons(z, nil)))  ;  t1 -> cons(s(z), nil)
})

-------------------------------------------
Step 18, which took 0.048559 s (model generation: 0.038002,  model checking: 0.010557):

Model:
|_
{
append -> 
{{{
Q={q_gen_612, q_gen_614, q_gen_615, q_gen_626, q_gen_627, q_gen_628},
Q_f={q_gen_612},
Delta=
{
<cons>(q_gen_627, q_gen_626) -> q_gen_626
<nil>() -> q_gen_626
<s>(q_gen_627) -> q_gen_627
<z>() -> q_gen_627
<cons, cons>(q_gen_615, q_gen_614) -> q_gen_614
<nil, cons>(q_gen_627, q_gen_626) -> q_gen_614
<nil, nil>() -> q_gen_614
<s, s>(q_gen_615) -> q_gen_615
<s, z>(q_gen_627) -> q_gen_615
<z, s>(q_gen_627) -> q_gen_615
<z, z>() -> q_gen_615
<cons, cons, cons>(q_gen_628, q_gen_612) -> q_gen_612
<cons, nil, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, cons, cons>(q_gen_615, q_gen_614) -> q_gen_612
<nil, nil, cons>(q_gen_627, q_gen_626) -> q_gen_612
<nil, nil, nil>() -> q_gen_612
<s, s, s>(q_gen_628) -> q_gen_628
<s, s, z>(q_gen_615) -> q_gen_628
<s, z, s>(q_gen_615) -> q_gen_628
<s, z, z>(q_gen_627) -> q_gen_628
<z, s, z>(q_gen_627) -> q_gen_628
<z, z, s>(q_gen_627) -> q_gen_628
<z, z, z>() -> q_gen_628
}

Datatype: <natlist, natlist, natlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_609, q_gen_610, q_gen_611},
Q_f={q_gen_609},
Delta=
{
<cons>(q_gen_611, q_gen_609) -> q_gen_609
<cons>(q_gen_611, q_gen_610) -> q_gen_609
<nil>() -> q_gen_610
<s>(q_gen_611) -> q_gen_611
<z>() -> q_gen_611
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 19  ;  () -> not_null([cons(x, ll)]) -> 17  ;  (append([l1, l2, _lta]) /\ not_null([l1])) -> not_null([_lta]) -> 16  ;  (append([l1, l2, _mta]) /\ not_null([l2])) -> not_null([_mta]) -> 16  ;  (append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]) -> 40  ;  (not_null([nil])) -> BOT -> 17
}
Sat witness: Yes: ((append([t1, l2, _gta])) -> append([cons(h1, t1), l2, cons(h1, _gta)]), {
_gta -> cons(s(s(z)), cons(z, cons(z, nil)))  ;  h1 -> s(z)  ;  l2 -> cons(z, cons(s(s(z)), cons(z, nil)))  ;  t1 -> cons(s(z), nil)
})

Total time: 40.000176
Reason for stopping: DontKnow. Stopped because: timeout

