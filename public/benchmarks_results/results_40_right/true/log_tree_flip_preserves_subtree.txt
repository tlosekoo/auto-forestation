Solving ../../../../benchmarks/smtlib/true/tree_flip_preserves_subtree.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
elt -> {a, b}  ;  etree -> {leaf, node}  ;  nat -> {s, z}
}
definition:
{
(flip, F:
{() -> flip([leaf, leaf])
(flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)])}
(flip([_iwa, _jwa]) /\ flip([_iwa, _kwa])) -> eq_etree([_jwa, _kwa])
)
(subtree, P:
{() -> subtree([leaf, t])
(subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])
(subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2])
(not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT
(subtree([node(ea, ta1, ta2), leaf])) -> BOT
(subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT}
)
}

properties:
{(flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa])}


over-approximation: {flip}
under-approximation: {}

Clause system for inference is:

{
() -> flip([leaf, leaf]) -> 0  ;  () -> subtree([leaf, t]) -> 0  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 0  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 0  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 0  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 0  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 0  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 0  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 0
}


Solving took 40.001174 seconds.
DontKnow. Stopped because: timeout
Working model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8877, q_gen_8878, q_gen_8884, q_gen_8885, q_gen_8888, q_gen_8889, q_gen_8890, q_gen_8891, q_gen_8892, q_gen_8894, q_gen_8897, q_gen_8898},
Q_f={},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8877
<b, b>() -> q_gen_8878
<node, node>(q_gen_8885, q_gen_8874, q_gen_8874) -> q_gen_8884
<a, a>() -> q_gen_8885
<node, node>(q_gen_8878, q_gen_8892, q_gen_8889) -> q_gen_8888
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8889
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8892
<node, node>(q_gen_8878, q_gen_8892, q_gen_8892) -> q_gen_8894
<node, node>(q_gen_8878, q_gen_8889, q_gen_8892) -> q_gen_8897
<node, node>(q_gen_8878, q_gen_8874, q_gen_8892) -> q_gen_8898
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8875, q_gen_8876, q_gen_8879, q_gen_8880, q_gen_8881, q_gen_8882, q_gen_8883, q_gen_8886, q_gen_8887, q_gen_8893, q_gen_8895, q_gen_8896, q_gen_8899},
Q_f={},
Delta=
{
<leaf>() -> q_gen_8880
<b>() -> q_gen_8881
<a>() -> q_gen_8887
<node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8896
<leaf, leaf>() -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8875
<b, b>() -> q_gen_8876
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8879
<node, node>(q_gen_8883, q_gen_8873, q_gen_8873) -> q_gen_8882
<a, a>() -> q_gen_8883
<leaf, node>(q_gen_8887, q_gen_8880, q_gen_8880) -> q_gen_8886
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8893
<leaf, node>(q_gen_8881, q_gen_8896, q_gen_8896) -> q_gen_8895
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8896) -> q_gen_8899
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004066 s (model generation: 0.003611,  model checking: 0.000455):

Model:
|_
{
flip -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 0  ;  () -> subtree([leaf, t]) -> 3  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 1  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> subtree([leaf, t]), {
t -> leaf
})

-------------------------------------------
Step 1, which took 0.003294 s (model generation: 0.003253,  model checking: 0.000041):

Model:
|_
{
flip -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873},
Q_f={q_gen_8873},
Delta=
{
<leaf, leaf>() -> q_gen_8873
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 1  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> flip([leaf, leaf]), {

})

-------------------------------------------
Step 2, which took 0.003032 s (model generation: 0.002861,  model checking: 0.000171):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873},
Q_f={q_gen_8873},
Delta=
{
<leaf, leaf>() -> q_gen_8873
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 1  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]), {
eb -> b  ;  ta1 -> leaf  ;  ta2 -> leaf  ;  tb1 -> leaf  ;  tb2 -> leaf
})

-------------------------------------------
Step 3, which took 0.003163 s (model generation: 0.003073,  model checking: 0.000090):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876},
Q_f={q_gen_8873},
Delta=
{
<leaf, leaf>() -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 1  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]), {
_gwa -> leaf  ;  _hwa -> leaf  ;  e -> b  ;  t1 -> leaf  ;  t2 -> leaf
})

-------------------------------------------
Step 4, which took 0.038766 s (model generation: 0.003381,  model checking: 0.035385):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876},
Q_f={q_gen_8873},
Delta=
{
<leaf, leaf>() -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 3  ;  () -> subtree([leaf, t]) -> 6  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 2  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 3  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 3  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 3  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 3
}
Sat witness: Yes: (() -> subtree([leaf, t]), {
t -> node(b, leaf, leaf)
})

-------------------------------------------
Step 5, which took 0.178706 s (model generation: 0.004028,  model checking: 0.174678):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 4  ;  () -> subtree([leaf, t]) -> 6  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 3  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 7  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 4  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 4  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 4  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 4
}
Sat witness: Yes: ((subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]), {
eb -> a  ;  ta1 -> leaf  ;  ta2 -> leaf  ;  tb1 -> leaf  ;  tb2 -> leaf
})

-------------------------------------------
Step 6, which took 0.193813 s (model generation: 0.004458,  model checking: 0.189355):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 4  ;  () -> subtree([leaf, t]) -> 6  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 4  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 7  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 7  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 5  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 5  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 5  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 5
}
Sat witness: Yes: ((flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]), {
_gwa -> leaf  ;  _hwa -> leaf  ;  e -> a  ;  t1 -> leaf  ;  t2 -> leaf
})

-------------------------------------------
Step 7, which took 0.176084 s (model generation: 0.004602,  model checking: 0.171482):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 5  ;  () -> subtree([leaf, t]) -> 9  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 5  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 7  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 7  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 6  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 6  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 6  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 6
}
Sat witness: Yes: (() -> subtree([leaf, t]), {
t -> node(a, leaf, leaf)
})

-------------------------------------------
Step 8, which took 0.086268 s (model generation: 0.004707,  model checking: 0.081561):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878},
Q_f={q_gen_8874},
Delta=
{
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 6  ;  () -> subtree([leaf, t]) -> 9  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 6  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 10  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 8  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 7  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 7  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 7  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 7
}
Sat witness: Yes: ((flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]), {
_gwa -> node(b, leaf, leaf)  ;  _hwa -> leaf  ;  e -> b  ;  t1 -> leaf  ;  t2 -> node(b, leaf, leaf)
})

-------------------------------------------
Step 9, which took 0.006236 s (model generation: 0.004977,  model checking: 0.001259):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878, q_gen_8890, q_gen_8891},
Q_f={q_gen_8874},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 6  ;  () -> subtree([leaf, t]) -> 9  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 9  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 10  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 8  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 7  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 7  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 7  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 7
}
Sat witness: Yes: ((flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]), {
_lwa -> node(b, leaf, leaf)  ;  _mwa -> leaf  ;  s -> leaf  ;  t -> leaf
})

-------------------------------------------
Step 10, which took 0.004854 s (model generation: 0.004697,  model checking: 0.000157):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878, q_gen_8890, q_gen_8891},
Q_f={q_gen_8874},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 7  ;  () -> subtree([leaf, t]) -> 9  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 9  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 10  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 8  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 7  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 7  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 10  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 8
}
Sat witness: Yes: ((subtree([node(ea, ta1, ta2), leaf])) -> BOT, {
ea -> b  ;  ta1 -> leaf  ;  ta2 -> leaf
})

-------------------------------------------
Step 11, which took 0.370694 s (model generation: 0.004344,  model checking: 0.366350):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878, q_gen_8889, q_gen_8890, q_gen_8891},
Q_f={q_gen_8874},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8889, q_gen_8889) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8889
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8889
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881, q_gen_8893},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8893
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 8  ;  () -> subtree([leaf, t]) -> 9  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 12  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 10  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 9  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 8  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 8  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 10  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 9
}
Sat witness: Yes: ((flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]), {
_lwa -> leaf  ;  _mwa -> node(b, node(b, leaf, leaf), node(b, leaf, leaf))  ;  s -> leaf  ;  t -> node(b, leaf, leaf)
})

-------------------------------------------
Step 12, which took 0.211671 s (model generation: 0.005771,  model checking: 0.205900):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878, q_gen_8890, q_gen_8891, q_gen_8892},
Q_f={q_gen_8874},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8892, q_gen_8874) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8892, q_gen_8892) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8892
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881, q_gen_8893},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8893
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 9  ;  () -> subtree([leaf, t]) -> 10  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 12  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 13  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 10  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 9  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 9  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 11  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 10
}
Sat witness: Yes: ((flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]), {
_gwa -> leaf  ;  _hwa -> node(b, leaf, leaf)  ;  e -> b  ;  t1 -> node(b, leaf, leaf)  ;  t2 -> leaf
})

-------------------------------------------
Step 13, which took 0.199359 s (model generation: 0.006030,  model checking: 0.193329):

Model:
|_
{
flip -> 
{{{
Q={q_gen_8874, q_gen_8878, q_gen_8890, q_gen_8891, q_gen_8892},
Q_f={q_gen_8874},
Delta=
{
<leaf>() -> q_gen_8890
<b>() -> q_gen_8891
<leaf, leaf>() -> q_gen_8874
<node, leaf>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8874) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8874, q_gen_8892) -> q_gen_8874
<node, node>(q_gen_8878, q_gen_8892, q_gen_8874) -> q_gen_8874
<a, a>() -> q_gen_8878
<b, b>() -> q_gen_8878
<leaf, node>(q_gen_8891, q_gen_8890, q_gen_8890) -> q_gen_8892
<node, node>(q_gen_8878, q_gen_8892, q_gen_8892) -> q_gen_8892
}

Datatype: <etree, etree>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_8873, q_gen_8876, q_gen_8880, q_gen_8881, q_gen_8893},
Q_f={q_gen_8873},
Delta=
{
<leaf>() -> q_gen_8880
<node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8880
<a>() -> q_gen_8881
<b>() -> q_gen_8881
<leaf, leaf>() -> q_gen_8873
<leaf, node>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8873
<node, node>(q_gen_8876, q_gen_8873, q_gen_8873) -> q_gen_8873
<a, a>() -> q_gen_8876
<b, b>() -> q_gen_8876
<node, leaf>(q_gen_8881, q_gen_8880, q_gen_8880) -> q_gen_8893
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> flip([leaf, leaf]) -> 10  ;  () -> subtree([leaf, t]) -> 11  ;  (flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]) -> 15  ;  (flip([t1, _hwa]) /\ flip([t2, _gwa])) -> flip([node(e, t1, t2), node(e, _gwa, _hwa)]) -> 13  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 11  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 10  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 10  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 12  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 11
}
Sat witness: Yes: ((flip([s, _lwa]) /\ flip([t, _mwa]) /\ subtree([s, t])) -> subtree([_lwa, _mwa]), {
_lwa -> node(b, leaf, node(b, leaf, leaf))  ;  _mwa -> leaf  ;  s -> node(b, leaf, leaf)  ;  t -> node(b, leaf, leaf)
})

Total time: 40.001174
Reason for stopping: DontKnow. Stopped because: timeout

