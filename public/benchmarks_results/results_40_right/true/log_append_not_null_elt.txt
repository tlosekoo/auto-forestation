Solving ../../../../benchmarks/smtlib/true/append_not_null_elt.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}
}
definition:
{
(append, F:
{() -> append([nil, l2, l2])
(append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)])}
(append([_ska, _tka, _uka]) /\ append([_ska, _tka, _vka])) -> eq_eltlist([_uka, _vka])
)
(not_null, P:
{() -> not_null([cons(x, ll)])
(not_null([nil])) -> BOT}
)
}

properties:
{(append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]), (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka])}


over-approximation: {append}
under-approximation: {}

Clause system for inference is:

{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 0  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 0  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 0  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 0  ;  (not_null([nil])) -> BOT -> 0
}


Solving took 0.157366 seconds.
Proved
Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<cons>(q_gen_583, q_gen_582) -> q_gen_582
<nil>() -> q_gen_582
<a>() -> q_gen_583
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<a, b>() -> q_gen_570
<b, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, a, a>() -> q_gen_584
<a, b, a>() -> q_gen_584
<b, a, b>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004236 s (model generation: 0.003700,  model checking: 0.000536):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 1  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 1  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> b
})

-------------------------------------------
Step 1, which took 0.003395 s (model generation: 0.003308,  model checking: 0.000087):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<nil>() -> q_gen_564
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 1  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 1  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> nil
})

-------------------------------------------
Step 2, which took 0.003179 s (model generation: 0.003172,  model checking: 0.000007):

Model:
|_
{
append -> 
{{{
Q={q_gen_567},
Q_f={q_gen_567},
Delta=
{
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<nil>() -> q_gen_564
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 1  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 1  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 1  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((not_null([nil])) -> BOT, {

})

-------------------------------------------
Step 3, which took 0.003225 s (model generation: 0.003089,  model checking: 0.000136):

Model:
|_
{
append -> 
{{{
Q={q_gen_567},
Q_f={q_gen_567},
Delta=
{
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 1  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 1  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> nil  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 4, which took 0.004826 s (model generation: 0.003408,  model checking: 0.001418):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570},
Q_f={q_gen_567},
Delta=
{
<nil, nil>() -> q_gen_569
<b, b>() -> q_gen_570
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 2  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 2  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> a
})

-------------------------------------------
Step 5, which took 0.004046 s (model generation: 0.003766,  model checking: 0.000280):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570},
Q_f={q_gen_567},
Delta=
{
<nil, nil>() -> q_gen_569
<b, b>() -> q_gen_570
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 3  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 3  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> cons(a, nil)
})

-------------------------------------------
Step 6, which took 0.006021 s (model generation: 0.004484,  model checking: 0.001537):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570},
Q_f={q_gen_567},
Delta=
{
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 4  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 4  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 7  ;  (not_null([nil])) -> BOT -> 5
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(a, nil)  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> cons(a, nil)
})

-------------------------------------------
Step 7, which took 0.007041 s (model generation: 0.006769,  model checking: 0.000272):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570},
Q_f={q_gen_567},
Delta=
{
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 9  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 5  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 5  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 7  ;  (not_null([nil])) -> BOT -> 6
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> cons(b, nil)  ;  x -> b
})

-------------------------------------------
Step 8, which took 0.009614 s (model generation: 0.008625,  model checking: 0.000989):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570},
Q_f={q_gen_567},
Delta=
{
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 7  ;  () -> not_null([cons(x, ll)]) -> 9  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 6  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 6  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 10  ;  (not_null([nil])) -> BOT -> 7
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(b, nil)  ;  h1 -> b  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 9, which took 0.010968 s (model generation: 0.010150,  model checking: 0.000818):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<nil>() -> q_gen_582
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 8  ;  () -> not_null([cons(x, ll)]) -> 10  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 7  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 7  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 13  ;  (not_null([nil])) -> BOT -> 8
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(b, nil)  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 10, which took 0.007881 s (model generation: 0.006711,  model checking: 0.001170):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<nil>() -> q_gen_582
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 9  ;  () -> not_null([cons(x, ll)]) -> 11  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 8  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 8  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 16  ;  (not_null([nil])) -> BOT -> 9
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(b, nil)  ;  h1 -> a  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 11, which took 0.011492 s (model generation: 0.009075,  model checking: 0.002417):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<nil>() -> q_gen_582
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, b, a>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 10  ;  () -> not_null([cons(x, ll)]) -> 12  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 9  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 9  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 19  ;  (not_null([nil])) -> BOT -> 10
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(a, nil)  ;  h1 -> b  ;  l2 -> cons(a, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 12, which took 0.011337 s (model generation: 0.010033,  model checking: 0.001304):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<nil>() -> q_gen_582
<a>() -> q_gen_583
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, b, a>() -> q_gen_584
<b, a, b>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 11  ;  () -> not_null([cons(x, ll)]) -> 13  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 10  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 10  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 22  ;  (not_null([nil])) -> BOT -> 11
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(a, nil)  ;  h1 -> a  ;  l2 -> cons(a, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 13, which took 0.011956 s (model generation: 0.008825,  model checking: 0.003131):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<nil>() -> q_gen_582
<a>() -> q_gen_583
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, a, a>() -> q_gen_584
<a, b, a>() -> q_gen_584
<b, a, b>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 12  ;  () -> not_null([cons(x, ll)]) -> 14  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 11  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 11  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 25  ;  (not_null([nil])) -> BOT -> 12
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(b, cons(b, nil))  ;  h1 -> b  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 14, which took 0.020267 s (model generation: 0.016484,  model checking: 0.003783):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<cons>(q_gen_583, q_gen_582) -> q_gen_582
<nil>() -> q_gen_582
<a>() -> q_gen_583
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, a, a>() -> q_gen_584
<a, b, a>() -> q_gen_584
<b, a, b>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 13  ;  () -> not_null([cons(x, ll)]) -> 15  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 12  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 12  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 28  ;  (not_null([nil])) -> BOT -> 13
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(a, cons(b, nil))  ;  h1 -> b  ;  l2 -> cons(a, cons(b, nil))  ;  t1 -> nil
})

-------------------------------------------
Step 15, which took 0.015353 s (model generation: 0.011814,  model checking: 0.003539):

Model:
|_
{
append -> 
{{{
Q={q_gen_567, q_gen_569, q_gen_570, q_gen_582, q_gen_583, q_gen_584},
Q_f={q_gen_567},
Delta=
{
<cons>(q_gen_583, q_gen_582) -> q_gen_582
<nil>() -> q_gen_582
<a>() -> q_gen_583
<b>() -> q_gen_583
<cons, cons>(q_gen_570, q_gen_569) -> q_gen_569
<nil, cons>(q_gen_583, q_gen_582) -> q_gen_569
<nil, nil>() -> q_gen_569
<a, a>() -> q_gen_570
<b, a>() -> q_gen_570
<b, b>() -> q_gen_570
<cons, cons, cons>(q_gen_584, q_gen_567) -> q_gen_567
<cons, nil, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, cons, cons>(q_gen_570, q_gen_569) -> q_gen_567
<nil, nil, cons>(q_gen_583, q_gen_582) -> q_gen_567
<nil, nil, nil>() -> q_gen_567
<a, a, a>() -> q_gen_584
<a, b, a>() -> q_gen_584
<b, a, b>() -> q_gen_584
<b, b, b>() -> q_gen_584
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_564, q_gen_565, q_gen_566},
Q_f={q_gen_564},
Delta=
{
<cons>(q_gen_566, q_gen_564) -> q_gen_564
<cons>(q_gen_566, q_gen_565) -> q_gen_564
<nil>() -> q_gen_565
<a>() -> q_gen_566
<b>() -> q_gen_566
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 14  ;  () -> not_null([cons(x, ll)]) -> 16  ;  (append([l1, l2, _wka]) /\ not_null([l1])) -> not_null([_wka]) -> 13  ;  (append([l1, l2, _xka]) /\ not_null([l2])) -> not_null([_xka]) -> 13  ;  (append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]) -> 31  ;  (not_null([nil])) -> BOT -> 14
}
Sat witness: Yes: ((append([t1, l2, _rka])) -> append([cons(h1, t1), l2, cons(h1, _rka)]), {
_rka -> cons(b, cons(a, nil))  ;  h1 -> b  ;  l2 -> cons(b, cons(a, nil))  ;  t1 -> nil
})

Total time: 0.157366
Reason for stopping: Proved

