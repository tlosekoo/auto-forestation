Solving ../../../../benchmarks/smtlib/true/append_not_null_cons_elt.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}
}
definition:
{
(append, F:
{() -> append([nil, l2, l2])
(append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)])}
(append([_be, _ce, _de]) /\ append([_be, _ce, _ee])) -> eq_eltlist([_de, _ee])
)
(not_null, P:
{() -> not_null([cons(x, ll)])
(not_null([nil])) -> BOT}
)
}

properties:
{(append([cons(i, l1), l2, _fe])) -> not_null([_fe])}


over-approximation: {append}
under-approximation: {not_null}

Clause system for inference is:

{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 0  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 0  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 0  ;  (not_null([nil])) -> BOT -> 0
}


Solving took 0.170506 seconds.
Proved
Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<cons>(q_gen_454, q_gen_453) -> q_gen_453
<nil>() -> q_gen_453
<a>() -> q_gen_454
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<a, b>() -> q_gen_439
<b, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, a, a>() -> q_gen_455
<a, b, a>() -> q_gen_455
<b, a, b>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004064 s (model generation: 0.003926,  model checking: 0.000138):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 1  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> b
})

-------------------------------------------
Step 1, which took 0.004163 s (model generation: 0.003318,  model checking: 0.000845):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<nil>() -> q_gen_433
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 1  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> nil
})

-------------------------------------------
Step 2, which took 0.003264 s (model generation: 0.003256,  model checking: 0.000008):

Model:
|_
{
append -> 
{{{
Q={q_gen_436},
Q_f={q_gen_436},
Delta=
{
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<nil>() -> q_gen_433
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 1  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 1  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((not_null([nil])) -> BOT, {

})

-------------------------------------------
Step 3, which took 0.003389 s (model generation: 0.003236,  model checking: 0.000153):

Model:
|_
{
append -> 
{{{
Q={q_gen_436},
Q_f={q_gen_436},
Delta=
{
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 4  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 2  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> nil  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 4, which took 0.003743 s (model generation: 0.003590,  model checking: 0.000153):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439},
Q_f={q_gen_436},
Delta=
{
<nil, nil>() -> q_gen_438
<b, b>() -> q_gen_439
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 4  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 3  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> a
})

-------------------------------------------
Step 5, which took 0.006015 s (model generation: 0.003979,  model checking: 0.002036):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439},
Q_f={q_gen_436},
Delta=
{
<nil, nil>() -> q_gen_438
<b, b>() -> q_gen_439
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 4  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> cons(a, nil)
})

-------------------------------------------
Step 6, which took 0.005598 s (model generation: 0.005110,  model checking: 0.000488):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439},
Q_f={q_gen_436},
Delta=
{
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 7  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 5  ;  (not_null([nil])) -> BOT -> 5
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(a, nil)  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> cons(a, nil)
})

-------------------------------------------
Step 7, which took 0.007416 s (model generation: 0.007327,  model checking: 0.000089):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439},
Q_f={q_gen_436},
Delta=
{
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 6  ;  () -> not_null([cons(x, ll)]) -> 6  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 7  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 8  ;  (not_null([nil])) -> BOT -> 6
}
Sat witness: Yes: ((append([cons(i, l1), l2, _fe])) -> not_null([_fe]), {
_fe -> cons(b, cons(b, nil))  ;  i -> b  ;  l1 -> cons(b, nil)  ;  l2 -> nil
})

-------------------------------------------
Step 8, which took 0.005363 s (model generation: 0.004076,  model checking: 0.001287):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439},
Q_f={q_gen_436},
Delta=
{
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 7  ;  () -> not_null([cons(x, ll)]) -> 7  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 10  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 8  ;  (not_null([nil])) -> BOT -> 7
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(b, nil)  ;  h1 -> b  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 9, which took 0.006539 s (model generation: 0.004713,  model checking: 0.001826):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<nil>() -> q_gen_453
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 8  ;  () -> not_null([cons(x, ll)]) -> 8  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 13  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 9  ;  (not_null([nil])) -> BOT -> 8
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(b, nil)  ;  h1 -> b  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 10, which took 0.011522 s (model generation: 0.010101,  model checking: 0.001421):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<nil>() -> q_gen_453
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 9  ;  () -> not_null([cons(x, ll)]) -> 9  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 16  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 10  ;  (not_null([nil])) -> BOT -> 9
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(b, nil)  ;  h1 -> a  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 11, which took 0.012206 s (model generation: 0.010342,  model checking: 0.001864):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<nil>() -> q_gen_453
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, b, a>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 10  ;  () -> not_null([cons(x, ll)]) -> 10  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 19  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 11  ;  (not_null([nil])) -> BOT -> 10
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(a, nil)  ;  h1 -> b  ;  l2 -> cons(a, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 12, which took 0.018771 s (model generation: 0.017035,  model checking: 0.001736):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<nil>() -> q_gen_453
<a>() -> q_gen_454
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, b, a>() -> q_gen_455
<b, a, b>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 11  ;  () -> not_null([cons(x, ll)]) -> 11  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 22  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 12  ;  (not_null([nil])) -> BOT -> 11
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(a, nil)  ;  h1 -> a  ;  l2 -> cons(a, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 13, which took 0.011659 s (model generation: 0.010197,  model checking: 0.001462):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<nil>() -> q_gen_453
<a>() -> q_gen_454
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, a, a>() -> q_gen_455
<a, b, a>() -> q_gen_455
<b, a, b>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 12  ;  () -> not_null([cons(x, ll)]) -> 12  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 25  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 13  ;  (not_null([nil])) -> BOT -> 12
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(b, cons(b, nil))  ;  h1 -> b  ;  l2 -> cons(b, nil)  ;  t1 -> nil
})

-------------------------------------------
Step 14, which took 0.016614 s (model generation: 0.010526,  model checking: 0.006088):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<cons>(q_gen_454, q_gen_453) -> q_gen_453
<nil>() -> q_gen_453
<a>() -> q_gen_454
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, a, a>() -> q_gen_455
<a, b, a>() -> q_gen_455
<b, a, b>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 13  ;  () -> not_null([cons(x, ll)]) -> 13  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 28  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 14  ;  (not_null([nil])) -> BOT -> 13
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(a, cons(b, nil))  ;  h1 -> b  ;  l2 -> cons(a, cons(b, nil))  ;  t1 -> nil
})

-------------------------------------------
Step 15, which took 0.019532 s (model generation: 0.015336,  model checking: 0.004196):

Model:
|_
{
append -> 
{{{
Q={q_gen_436, q_gen_438, q_gen_439, q_gen_453, q_gen_454, q_gen_455},
Q_f={q_gen_436},
Delta=
{
<cons>(q_gen_454, q_gen_453) -> q_gen_453
<nil>() -> q_gen_453
<a>() -> q_gen_454
<b>() -> q_gen_454
<cons, cons>(q_gen_439, q_gen_438) -> q_gen_438
<nil, cons>(q_gen_454, q_gen_453) -> q_gen_438
<nil, nil>() -> q_gen_438
<a, a>() -> q_gen_439
<b, a>() -> q_gen_439
<b, b>() -> q_gen_439
<cons, cons, cons>(q_gen_455, q_gen_436) -> q_gen_436
<cons, nil, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, cons, cons>(q_gen_439, q_gen_438) -> q_gen_436
<nil, nil, cons>(q_gen_454, q_gen_453) -> q_gen_436
<nil, nil, nil>() -> q_gen_436
<a, a, a>() -> q_gen_455
<a, b, a>() -> q_gen_455
<b, a, b>() -> q_gen_455
<b, b, b>() -> q_gen_455
}

Datatype: <eltlist, eltlist, eltlist>
Convolution form: right
}}}
  ;  not_null -> 
{{{
Q={q_gen_433, q_gen_434, q_gen_435},
Q_f={q_gen_433},
Delta=
{
<cons>(q_gen_435, q_gen_433) -> q_gen_433
<cons>(q_gen_435, q_gen_434) -> q_gen_433
<nil>() -> q_gen_434
<a>() -> q_gen_435
<b>() -> q_gen_435
}

Datatype: <eltlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 14  ;  () -> not_null([cons(x, ll)]) -> 14  ;  (append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]) -> 31  ;  (append([cons(i, l1), l2, _fe])) -> not_null([_fe]) -> 15  ;  (not_null([nil])) -> BOT -> 14
}
Sat witness: Yes: ((append([t1, l2, _ae])) -> append([cons(h1, t1), l2, cons(h1, _ae)]), {
_ae -> cons(b, cons(a, nil))  ;  h1 -> b  ;  l2 -> cons(b, cons(a, nil))  ;  t1 -> nil
})

Total time: 0.170506
Reason for stopping: Proved

