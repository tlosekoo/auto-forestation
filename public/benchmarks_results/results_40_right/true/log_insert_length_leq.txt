Solving ../../../../benchmarks/smtlib/true/insert_length_leq.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([a, y])
() -> leq([b, b])
(leq([b, a])) -> BOT}
)
(insert, F:
{() -> insert([x, nil, cons(x, nil)])
(insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)])
(leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))])}
(insert([_eb, _fb, _gb]) /\ insert([_eb, _fb, _hb])) -> eq_eltlist([_gb, _hb])
)
(length, F:
{() -> length([nil, z])
(length([ll, _ib])) -> length([cons(x, ll), s(_ib)])}
(length([_jb, _kb]) /\ length([_jb, _lb])) -> eq_nat([_kb, _lb])
)
(leqnat, P:
{() -> leqnat([z, s(nn2)])
() -> leqnat([z, z])
(leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)])
(leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2])
(leqnat([s(nn1), z])) -> BOT}
)
}

properties:
{(insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob])}


over-approximation: {insert, length}
under-approximation: {leq, leqnat}

Clause system for inference is:

{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  () -> leq([a, y]) -> 0  ;  () -> leq([b, b]) -> 0  ;  () -> leqnat([z, s(nn2)]) -> 0  ;  () -> leqnat([z, z]) -> 0  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 0  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 0  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 0  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 0  ;  (leq([b, a])) -> BOT -> 0  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 0  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 0  ;  (leqnat([s(nn1), z])) -> BOT -> 0
}


Solving took 0.183267 seconds.
Proved
Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<a>() -> q_gen_1678
<b>() -> q_gen_1678
<cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1681
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<a, b>() -> q_gen_1682
<b, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<a, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<a, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<a>() -> q_gen_1684
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005160 s (model generation: 0.004942,  model checking: 0.000218):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  () -> leq([a, y]) -> 0  ;  () -> leq([b, b]) -> 0  ;  () -> leqnat([z, s(nn2)]) -> 0  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leqnat([z, z]), {

})

-------------------------------------------
Step 1, which took 0.005360 s (model generation: 0.005211,  model checking: 0.000149):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670},
Q_f={q_gen_1670},
Delta=
{
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  () -> leq([a, y]) -> 0  ;  () -> leq([b, b]) -> 0  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leqnat([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 2, which took 0.011864 s (model generation: 0.009906,  model checking: 0.001958):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  () -> leq([a, y]) -> 0  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leq([b, b]), {

})

-------------------------------------------
Step 3, which took 0.009919 s (model generation: 0.009832,  model checking: 0.000087):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leq([a, y]), {
y -> b
})

-------------------------------------------
Step 4, which took 0.009675 s (model generation: 0.009565,  model checking: 0.000110):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 0  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> length([nil, z]), {

})

-------------------------------------------
Step 5, which took 0.007243 s (model generation: 0.007068,  model checking: 0.000175):

Model:
|_
{
insert -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675},
Q_f={q_gen_1675},
Delta=
{
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 1  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 1  ;  (leqnat([s(nn1), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> insert([x, nil, cons(x, nil)]), {
x -> b
})

-------------------------------------------
Step 6, which took 0.006343 s (model generation: 0.006179,  model checking: 0.000164):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675},
Q_f={q_gen_1675},
Delta=
{
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 1  ;  (leq([b, a])) -> BOT -> 1  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 2  ;  (leqnat([s(nn1), z])) -> BOT -> 2
}
Sat witness: Yes: ((leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 7, which took 0.011557 s (model generation: 0.009190,  model checking: 0.002367):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675},
Q_f={q_gen_1675},
Delta=
{
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 1  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 2  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 2  ;  (leqnat([s(nn1), z])) -> BOT -> 2
}
Sat witness: Yes: ((leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]), {
x -> b  ;  y -> b  ;  z -> nil
})

-------------------------------------------
Step 8, which took 0.006983 s (model generation: 0.006862,  model checking: 0.000121):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675},
Q_f={q_gen_1675},
Delta=
{
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 1  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 2  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 2  ;  (leqnat([s(nn1), z])) -> BOT -> 2
}
Sat witness: Yes: ((length([ll, _ib])) -> length([cons(x, ll), s(_ib)]), {
_ib -> z  ;  ll -> nil  ;  x -> b
})

-------------------------------------------
Step 9, which took 0.006130 s (model generation: 0.005594,  model checking: 0.000536):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 1  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 2  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 2  ;  (leqnat([s(nn1), z])) -> BOT -> 2
}
Sat witness: Yes: ((insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]), {
_db -> cons(b, nil)  ;  x -> b  ;  y -> a  ;  z -> nil
})

-------------------------------------------
Step 10, which took 0.012389 s (model generation: 0.011887,  model checking: 0.000502):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, a>() -> q_gen_1673
<b, b>() -> q_gen_1673
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 3  ;  () -> leqnat([z, z]) -> 3  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 2  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 3  ;  (leqnat([s(nn1), z])) -> BOT -> 3
}
Sat witness: Yes: ((leq([b, a])) -> BOT, {

})

-------------------------------------------
Step 11, which took 0.012444 s (model generation: 0.011697,  model checking: 0.000747):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 3  ;  () -> leq([b, b]) -> 3  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 3  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 4  ;  (leqnat([s(nn1), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> leqnat([z, s(nn2)]), {
nn2 -> s(z)
})

-------------------------------------------
Step 12, which took 0.012319 s (model generation: 0.012084,  model checking: 0.000235):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  () -> leq([a, y]) -> 6  ;  () -> leq([b, b]) -> 4  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 4  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 4  ;  (leqnat([s(nn1), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> leq([a, y]), {
y -> a
})

-------------------------------------------
Step 13, which took 0.009900 s (model generation: 0.009305,  model checking: 0.000595):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  () -> leq([a, y]) -> 6  ;  () -> leq([b, b]) -> 4  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 4  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 4  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 4  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 4  ;  (leqnat([s(nn1), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> insert([x, nil, cons(x, nil)]), {
x -> a
})

-------------------------------------------
Step 14, which took 0.015047 s (model generation: 0.014130,  model checking: 0.000917):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<a>() -> q_gen_1678
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<a, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  () -> leq([a, y]) -> 6  ;  () -> leq([b, b]) -> 4  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 4  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 4  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 7  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 5  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 5  ;  (leqnat([s(nn1), z])) -> BOT -> 5
}
Sat witness: Yes: ((leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]), {
x -> a  ;  y -> b  ;  z -> nil
})

-------------------------------------------
Step 15, which took 0.009218 s (model generation: 0.009018,  model checking: 0.000200):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<a>() -> q_gen_1678
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<a, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<a, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  () -> leq([a, y]) -> 6  ;  () -> leq([b, b]) -> 4  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 4  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 4  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 7  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 7  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 5  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 5  ;  (leqnat([s(nn1), z])) -> BOT -> 5
}
Sat witness: Yes: ((length([ll, _ib])) -> length([cons(x, ll), s(_ib)]), {
_ib -> z  ;  ll -> nil  ;  x -> a
})

-------------------------------------------
Step 16, which took 0.009020 s (model generation: 0.008183,  model checking: 0.000837):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<a>() -> q_gen_1678
<b>() -> q_gen_1678
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<a, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<a, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<a>() -> q_gen_1684
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  () -> leq([a, y]) -> 6  ;  () -> leq([b, b]) -> 4  ;  () -> leqnat([z, s(nn2)]) -> 6  ;  () -> leqnat([z, z]) -> 4  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 4  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 7  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 7  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 7  ;  (leq([b, a])) -> BOT -> 5  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 5  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 5  ;  (leqnat([s(nn1), z])) -> BOT -> 5
}
Sat witness: Yes: ((insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]), {
_db -> cons(a, cons(a, nil))  ;  x -> b  ;  y -> a  ;  z -> cons(a, nil)
})

-------------------------------------------
Step 17, which took 0.012216 s (model generation: 0.011129,  model checking: 0.001087):

Model:
|_
{
insert -> 
{{{
Q={q_gen_1676, q_gen_1677, q_gen_1678, q_gen_1681, q_gen_1682},
Q_f={q_gen_1676},
Delta=
{
<nil>() -> q_gen_1677
<a>() -> q_gen_1678
<b>() -> q_gen_1678
<cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1681
<nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1681
<a, a>() -> q_gen_1682
<b, a>() -> q_gen_1682
<b, b>() -> q_gen_1682
<a, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<a, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
<b, cons, cons>(q_gen_1682, q_gen_1681) -> q_gen_1676
<b, nil, cons>(q_gen_1678, q_gen_1677) -> q_gen_1676
}

Datatype: <elt, eltlist, eltlist>
Convolution form: right
}}}
  ;  length -> 
{{{
Q={q_gen_1675, q_gen_1684},
Q_f={q_gen_1675},
Delta=
{
<a>() -> q_gen_1684
<b>() -> q_gen_1684
<cons, s>(q_gen_1684, q_gen_1675) -> q_gen_1675
<nil, z>() -> q_gen_1675
}

Datatype: <eltlist, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_1673, q_gen_1685},
Q_f={q_gen_1673},
Delta=
{
<a, a>() -> q_gen_1673
<a, b>() -> q_gen_1673
<b, b>() -> q_gen_1673
<b, a>() -> q_gen_1685
}

Datatype: <elt, elt>
Convolution form: right
}}}
  ;  leqnat -> 
{{{
Q={q_gen_1670, q_gen_1672},
Q_f={q_gen_1670},
Delta=
{
<s>(q_gen_1672) -> q_gen_1672
<z>() -> q_gen_1672
<s, s>(q_gen_1670) -> q_gen_1670
<z, s>(q_gen_1672) -> q_gen_1670
<z, z>() -> q_gen_1670
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> insert([x, nil, cons(x, nil)]) -> 7  ;  () -> length([nil, z]) -> 5  ;  () -> leq([a, y]) -> 7  ;  () -> leq([b, b]) -> 5  ;  () -> leqnat([z, s(nn2)]) -> 7  ;  () -> leqnat([z, z]) -> 5  ;  (insert([x, l, _nb]) /\ length([_nb, _ob]) /\ length([l, _mb])) -> leqnat([_mb, _ob]) -> 5  ;  (insert([x, z, _db]) /\ not leq([x, y])) -> insert([x, cons(y, z), cons(y, _db)]) -> 7  ;  (length([ll, _ib])) -> length([cons(x, ll), s(_ib)]) -> 7  ;  (leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]) -> 10  ;  (leq([b, a])) -> BOT -> 6  ;  (leqnat([nn1, nn2])) -> leqnat([s(nn1), s(nn2)]) -> 6  ;  (leqnat([s(nn1), s(nn2)])) -> leqnat([nn1, nn2]) -> 6  ;  (leqnat([s(nn1), z])) -> BOT -> 6
}
Sat witness: Yes: ((leq([x, y])) -> insert([x, cons(y, z), cons(x, cons(y, z))]), {
x -> b  ;  y -> b  ;  z -> cons(a, nil)
})

Total time: 0.183267
Reason for stopping: Proved

