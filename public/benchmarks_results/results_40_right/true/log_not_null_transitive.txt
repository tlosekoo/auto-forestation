Solving ../../../../benchmarks/smtlib/true/not_null_transitive.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(not_null, P:
{() -> not_null([cons(x, ll)])
(not_null([nil])) -> BOT}
)
}

properties:
{(not_null([l2])) -> not_null([l2])}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{
() -> not_null([cons(x, ll)]) -> 0  ;  (not_null([l2])) -> not_null([l2]) -> 0  ;  (not_null([nil])) -> BOT -> 0
}


Solving took 0.012899 seconds.
Proved
Model:
|_
{
not_null -> 
{{{
Q={q_gen_6716, q_gen_6717, q_gen_6718},
Q_f={q_gen_6716},
Delta=
{
<cons>(q_gen_6718, q_gen_6716) -> q_gen_6716
<cons>(q_gen_6718, q_gen_6717) -> q_gen_6716
<nil>() -> q_gen_6717
<s>(q_gen_6718) -> q_gen_6718
<z>() -> q_gen_6718
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.003154 s (model generation: 0.003093,  model checking: 0.000061):

Model:
|_
{
not_null -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> not_null([cons(x, ll)]) -> 3  ;  (not_null([l2])) -> not_null([l2]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> z
})

-------------------------------------------
Step 1, which took 0.003255 s (model generation: 0.003246,  model checking: 0.000009):

Model:
|_
{
not_null -> 
{{{
Q={q_gen_6716, q_gen_6718},
Q_f={q_gen_6716},
Delta=
{
<cons>(q_gen_6718, q_gen_6716) -> q_gen_6716
<nil>() -> q_gen_6716
<z>() -> q_gen_6718
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> not_null([cons(x, ll)]) -> 3  ;  (not_null([l2])) -> not_null([l2]) -> 1  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((not_null([nil])) -> BOT, {

})

-------------------------------------------
Step 2, which took 0.003215 s (model generation: 0.003034,  model checking: 0.000181):

Model:
|_
{
not_null -> 
{{{
Q={q_gen_6716, q_gen_6717, q_gen_6718},
Q_f={q_gen_6716},
Delta=
{
<cons>(q_gen_6718, q_gen_6717) -> q_gen_6716
<nil>() -> q_gen_6717
<z>() -> q_gen_6718
}

Datatype: <natlist>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> not_null([cons(x, ll)]) -> 6  ;  (not_null([l2])) -> not_null([l2]) -> 2  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> cons(s(z), nil)  ;  x -> z
})

Total time: 0.012899
Reason for stopping: Proved

