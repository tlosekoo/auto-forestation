Solving ../../../../benchmarks/smtlib/true/leq_leq_eq.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(eqnat, P:
{() -> eqnat([z, z])
(eqnat([x2, y2])) -> eqnat([s(x2), s(y2)])
(eqnat([s(x2), s(y2)])) -> eqnat([x2, y2])
(eqnat([s(x2), z])) -> BOT
(eqnat([z, s(u)])) -> BOT}
)
(leq, P:
{() -> leq([z, y])
(leq([z, x2])) -> leq([s(z), s(x2)])
(leq([s(z), s(x2)])) -> leq([z, x2])
(leq([s(z), z])) -> BOT}
)
}

properties:
{(leq([m, n]) /\ leq([n, m])) -> eqnat([n, m])}


over-approximation: {leq}
under-approximation: {eqnat}

Clause system for inference is:

{
() -> eqnat([z, z]) -> 0  ;  () -> leq([z, y]) -> 0  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 0  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 0  ;  (eqnat([s(x2), z])) -> BOT -> 0  ;  (eqnat([z, s(u)])) -> BOT -> 0  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 0  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 0  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 0  ;  (leq([s(z), z])) -> BOT -> 0
}


Solving took 0.064229 seconds.
Proved
Model:
|_
{
eqnat -> 
{{{
Q={q_gen_5492},
Q_f={q_gen_5492},
Delta=
{
<s, s>(q_gen_5492) -> q_gen_5492
<z, z>() -> q_gen_5492
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491, q_gen_5496},
Q_f={q_gen_5491},
Delta=
{
<s>(q_gen_5496) -> q_gen_5496
<z>() -> q_gen_5496
<s, s>(q_gen_5491) -> q_gen_5491
<z, s>(q_gen_5496) -> q_gen_5491
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.008773 s (model generation: 0.008494,  model checking: 0.000279):

Model:
|_
{
eqnat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 0  ;  () -> leq([z, y]) -> 3  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 1  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 1  ;  (eqnat([s(x2), z])) -> BOT -> 1  ;  (eqnat([z, s(u)])) -> BOT -> 1  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 1  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 1  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 1  ;  (leq([s(z), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leq([z, y]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.006649 s (model generation: 0.006563,  model checking: 0.000086):

Model:
|_
{
eqnat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491},
Q_f={q_gen_5491},
Delta=
{
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 3  ;  () -> leq([z, y]) -> 3  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 1  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 1  ;  (eqnat([s(x2), z])) -> BOT -> 1  ;  (eqnat([z, s(u)])) -> BOT -> 1  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 1  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 1  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 1  ;  (leq([s(z), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> eqnat([z, z]), {

})

-------------------------------------------
Step 2, which took 0.008277 s (model generation: 0.007974,  model checking: 0.000303):

Model:
|_
{
eqnat -> 
{{{
Q={q_gen_5492},
Q_f={q_gen_5492},
Delta=
{
<z, z>() -> q_gen_5492
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491},
Q_f={q_gen_5491},
Delta=
{
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 3  ;  () -> leq([z, y]) -> 3  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 1  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 1  ;  (eqnat([s(x2), z])) -> BOT -> 1  ;  (eqnat([z, s(u)])) -> BOT -> 1  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 1  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 4  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 2  ;  (leq([s(z), z])) -> BOT -> 2
}
Sat witness: Yes: ((leq([z, x2])) -> leq([s(z), s(x2)]), {
x2 -> z  ;  z -> z
})

-------------------------------------------
Step 3, which took 0.010282 s (model generation: 0.009996,  model checking: 0.000286):

Model:
|_
{
eqnat -> 
{{{
Q={q_gen_5492},
Q_f={q_gen_5492},
Delta=
{
<z, z>() -> q_gen_5492
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491},
Q_f={q_gen_5491},
Delta=
{
<s, s>(q_gen_5491) -> q_gen_5491
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 3  ;  () -> leq([z, y]) -> 3  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 1  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 1  ;  (eqnat([s(x2), z])) -> BOT -> 1  ;  (eqnat([z, s(u)])) -> BOT -> 1  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 4  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 4  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 2  ;  (leq([s(z), z])) -> BOT -> 2
}
Sat witness: Yes: ((leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]), {
m -> s(z)  ;  n -> s(z)
})

-------------------------------------------
Step 4, which took 0.009654 s (model generation: 0.009227,  model checking: 0.000427):

Model:
|_
{
eqnat -> 
{{{
Q={q_gen_5492},
Q_f={q_gen_5492},
Delta=
{
<s, s>(q_gen_5492) -> q_gen_5492
<z, z>() -> q_gen_5492
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491},
Q_f={q_gen_5491},
Delta=
{
<s, s>(q_gen_5491) -> q_gen_5491
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 3  ;  () -> leq([z, y]) -> 6  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 2  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 2  ;  (eqnat([s(x2), z])) -> BOT -> 2  ;  (eqnat([z, s(u)])) -> BOT -> 2  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 4  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 4  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 3  ;  (leq([s(z), z])) -> BOT -> 3
}
Sat witness: Yes: (() -> leq([z, y]), {
y -> s(z)
})

-------------------------------------------
Step 5, which took 0.011725 s (model generation: 0.010916,  model checking: 0.000809):

Model:
|_
{
eqnat -> 
{{{
Q={q_gen_5492},
Q_f={q_gen_5492},
Delta=
{
<s, s>(q_gen_5492) -> q_gen_5492
<z, z>() -> q_gen_5492
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq -> 
{{{
Q={q_gen_5491, q_gen_5496},
Q_f={q_gen_5491},
Delta=
{
<z>() -> q_gen_5496
<s, s>(q_gen_5491) -> q_gen_5491
<z, s>(q_gen_5496) -> q_gen_5491
<z, z>() -> q_gen_5491
}

Datatype: <nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> eqnat([z, z]) -> 4  ;  () -> leq([z, y]) -> 9  ;  (eqnat([x2, y2])) -> eqnat([s(x2), s(y2)]) -> 3  ;  (eqnat([s(x2), s(y2)])) -> eqnat([x2, y2]) -> 3  ;  (eqnat([s(x2), z])) -> BOT -> 3  ;  (eqnat([z, s(u)])) -> BOT -> 3  ;  (leq([m, n]) /\ leq([n, m])) -> eqnat([n, m]) -> 5  ;  (leq([z, x2])) -> leq([s(z), s(x2)]) -> 5  ;  (leq([s(z), s(x2)])) -> leq([z, x2]) -> 4  ;  (leq([s(z), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> leq([z, y]), {
y -> s(s(z))
})

Total time: 0.064229
Reason for stopping: Proved

