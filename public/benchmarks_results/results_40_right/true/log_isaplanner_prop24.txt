Solving ../../../../benchmarks/smtlib/true/isaplanner_prop24.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(max, F:
{() -> max([s(u), z, s(u)])
() -> max([z, y, y])
(max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)])}
(max([_lxa, _mxa, _nxa]) /\ max([_lxa, _mxa, _oxa])) -> eq_nat([_nxa, _oxa])
)
}

properties:
{(leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]), (max([i, j, i])) -> leq([j, i])}


over-approximation: {max}
under-approximation: {}

Clause system for inference is:

{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 0  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (max([i, j, i])) -> leq([j, i]) -> 0  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 0
}


Solving took 0.044029 seconds.
Proved
Model:
|_
{
leq -> 
{{{
Q={q_gen_2430, q_gen_2433},
Q_f={q_gen_2430},
Delta=
{
<s>(q_gen_2433) -> q_gen_2433
<z>() -> q_gen_2433
<s, s>(q_gen_2430) -> q_gen_2430
<z, s>(q_gen_2433) -> q_gen_2430
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<s, s>(q_gen_2429) -> q_gen_2429
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, s, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.003430 s (model generation: 0.003282,  model checking: 0.000148):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, i])) -> leq([j, i]) -> 1  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 1
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.003443 s (model generation: 0.003366,  model checking: 0.000077):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427},
Q_f={q_gen_2427},
Delta=
{
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, i])) -> leq([j, i]) -> 1  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 1
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> z
})

-------------------------------------------
Step 2, which took 0.003284 s (model generation: 0.003220,  model checking: 0.000064):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, i])) -> leq([j, i]) -> 1  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 3, which took 0.003144 s (model generation: 0.003054,  model checking: 0.000090):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430},
Q_f={q_gen_2430},
Delta=
{
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, i])) -> leq([j, i]) -> 1  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: ((max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]), {
_kxa -> z  ;  u -> z  ;  x2 -> z
})

-------------------------------------------
Step 4, which took 0.004652 s (model generation: 0.004571,  model checking: 0.000081):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430},
Q_f={q_gen_2430},
Delta=
{
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, i])) -> leq([j, i]) -> 4  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: ((max([i, j, i])) -> leq([j, i]), {
i -> s(z)  ;  j -> z
})

-------------------------------------------
Step 5, which took 0.005733 s (model generation: 0.003406,  model checking: 0.002327):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430, q_gen_2433},
Q_f={q_gen_2430},
Delta=
{
<z>() -> q_gen_2433
<z, s>(q_gen_2433) -> q_gen_2430
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (max([i, j, i])) -> leq([j, i]) -> 4  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 6, which took 0.003662 s (model generation: 0.003447,  model checking: 0.000215):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430, q_gen_2433},
Q_f={q_gen_2430},
Delta=
{
<z>() -> q_gen_2433
<s, s>(q_gen_2430) -> q_gen_2430
<z, s>(q_gen_2433) -> q_gen_2430
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 6  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 2  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (max([i, j, i])) -> leq([j, i]) -> 4  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> s(z)
})

-------------------------------------------
Step 7, which took 0.003686 s (model generation: 0.003429,  model checking: 0.000257):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430, q_gen_2433},
Q_f={q_gen_2430},
Delta=
{
<z>() -> q_gen_2433
<s, s>(q_gen_2430) -> q_gen_2430
<z, s>(q_gen_2433) -> q_gen_2430
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, s, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (max([i, j, i])) -> leq([j, i]) -> 4  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> s(z)
})

-------------------------------------------
Step 8, which took 0.006436 s (model generation: 0.006314,  model checking: 0.000122):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2430, q_gen_2433},
Q_f={q_gen_2430},
Delta=
{
<z>() -> q_gen_2433
<s, s>(q_gen_2430) -> q_gen_2430
<z, s>(q_gen_2433) -> q_gen_2430
<z, z>() -> q_gen_2430
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2427, q_gen_2429},
Q_f={q_gen_2427},
Delta=
{
<s, s>(q_gen_2429) -> q_gen_2429
<z, z>() -> q_gen_2429
<s, s, s>(q_gen_2427) -> q_gen_2427
<s, z, s>(q_gen_2429) -> q_gen_2427
<z, s, s>(q_gen_2429) -> q_gen_2427
<z, z, z>() -> q_gen_2427
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (leq([j, i]) /\ max([i, j, _qxa])) -> eq_nat([_qxa, i]) -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (max([i, j, i])) -> leq([j, i]) -> 4  ;  (max([u, x2, _kxa])) -> max([s(u), s(x2), s(_kxa)]) -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(s(z))
})

Total time: 0.044029
Reason for stopping: Proved

