Solving ../../../../benchmarks/smtlib/true/isaplanner_prop23.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(max, F:
{() -> max([s(u), z, s(u)])
() -> max([z, y, y])
(max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)])}
(max([_eda, _fda, _gda]) /\ max([_eda, _fda, _hda])) -> eq_nat([_gda, _hda])
)
}

properties:
{(max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda])}


over-approximation: {max}
under-approximation: {}

Clause system for inference is:

{
() -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 0  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 0  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 0
}


Solving took 0.026497 seconds.
Proved
Model:
|_
{
max -> 
{{{
Q={q_gen_2420, q_gen_2422},
Q_f={q_gen_2420},
Delta=
{
<s, s>(q_gen_2422) -> q_gen_2422
<z, z>() -> q_gen_2422
<s, s, s>(q_gen_2420) -> q_gen_2420
<s, z, s>(q_gen_2422) -> q_gen_2420
<z, s, s>(q_gen_2422) -> q_gen_2420
<z, z, z>() -> q_gen_2420
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004035 s (model generation: 0.003910,  model checking: 0.000125):

Model:
|_
{
max -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 3  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 1  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 1
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.003570 s (model generation: 0.003503,  model checking: 0.000067):

Model:
|_
{
max -> 
{{{
Q={q_gen_2420},
Q_f={q_gen_2420},
Delta=
{
<z, z, z>() -> q_gen_2420
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 1  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 1
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> z
})

-------------------------------------------
Step 2, which took 0.003397 s (model generation: 0.003234,  model checking: 0.000163):

Model:
|_
{
max -> 
{{{
Q={q_gen_2420, q_gen_2422},
Q_f={q_gen_2420},
Delta=
{
<z, z>() -> q_gen_2422
<s, z, s>(q_gen_2422) -> q_gen_2420
<z, z, z>() -> q_gen_2420
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 1  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 4
}
Sat witness: Yes: ((max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]), {
_dda -> z  ;  u -> z  ;  x2 -> z
})

-------------------------------------------
Step 3, which took 0.004658 s (model generation: 0.004499,  model checking: 0.000159):

Model:
|_
{
max -> 
{{{
Q={q_gen_2420, q_gen_2422},
Q_f={q_gen_2420},
Delta=
{
<z, z>() -> q_gen_2422
<s, s, s>(q_gen_2420) -> q_gen_2420
<s, z, s>(q_gen_2422) -> q_gen_2420
<z, z, z>() -> q_gen_2420
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 6  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 2  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 4
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> s(z)
})

-------------------------------------------
Step 4, which took 0.006646 s (model generation: 0.004307,  model checking: 0.002339):

Model:
|_
{
max -> 
{{{
Q={q_gen_2420, q_gen_2422},
Q_f={q_gen_2420},
Delta=
{
<z, z>() -> q_gen_2422
<s, s, s>(q_gen_2420) -> q_gen_2420
<s, z, s>(q_gen_2422) -> q_gen_2420
<z, s, s>(q_gen_2422) -> q_gen_2420
<z, z, z>() -> q_gen_2420
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (max([a, b, _ida]) /\ max([b, a, _jda])) -> eq_nat([_ida, _jda]) -> 3  ;  (max([u, x2, _dda])) -> max([s(u), s(x2), s(_dda)]) -> 4
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> s(z)
})

Total time: 0.026497
Reason for stopping: Proved

