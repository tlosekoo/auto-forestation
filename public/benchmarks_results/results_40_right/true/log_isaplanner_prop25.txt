Solving ../../../../benchmarks/smtlib/true/isaplanner_prop25.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(max, F:
{() -> max([s(u), z, s(u)])
() -> max([z, y, y])
(max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)])}
(max([_vpa, _wpa, _xpa]) /\ max([_vpa, _wpa, _ypa])) -> eq_nat([_xpa, _ypa])
)
}

properties:
{(leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]), (max([i, j, j])) -> leq([i, j])}


over-approximation: {max}
under-approximation: {}

Clause system for inference is:

{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 0  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (max([i, j, j])) -> leq([i, j]) -> 0  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 0
}


Solving took 0.046701 seconds.
Proved
Model:
|_
{
leq -> 
{{{
Q={q_gen_2443, q_gen_2450},
Q_f={q_gen_2443},
Delta=
{
<s>(q_gen_2450) -> q_gen_2450
<z>() -> q_gen_2450
<s, s>(q_gen_2443) -> q_gen_2443
<z, s>(q_gen_2450) -> q_gen_2443
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<s, s>(q_gen_2442) -> q_gen_2442
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, s, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004762 s (model generation: 0.003506,  model checking: 0.001256):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 0  ;  () -> max([z, y, y]) -> 3  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, j])) -> leq([i, j]) -> 1  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 1
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.004032 s (model generation: 0.003955,  model checking: 0.000077):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440},
Q_f={q_gen_2440},
Delta=
{
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, j])) -> leq([i, j]) -> 1  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 1
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> z
})

-------------------------------------------
Step 2, which took 0.003427 s (model generation: 0.003391,  model checking: 0.000036):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<z, z>() -> q_gen_2442
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, j])) -> leq([i, j]) -> 1  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 3, which took 0.003165 s (model generation: 0.003075,  model checking: 0.000090):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443},
Q_f={q_gen_2443},
Delta=
{
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<z, z>() -> q_gen_2442
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, j])) -> leq([i, j]) -> 1  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 4
}
Sat witness: Yes: ((max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]), {
_upa -> z  ;  u -> z  ;  x2 -> z
})

-------------------------------------------
Step 4, which took 0.003378 s (model generation: 0.003314,  model checking: 0.000064):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443},
Q_f={q_gen_2443},
Delta=
{
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 3  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, j])) -> leq([i, j]) -> 4  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 4
}
Sat witness: Yes: ((max([i, j, j])) -> leq([i, j]), {
i -> s(z)  ;  j -> s(z)
})

-------------------------------------------
Step 5, which took 0.006927 s (model generation: 0.004364,  model checking: 0.002563):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443},
Q_f={q_gen_2443},
Delta=
{
<s, s>(q_gen_2443) -> q_gen_2443
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 3  ;  () -> max([z, y, y]) -> 6  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 2  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 2  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (max([i, j, j])) -> leq([i, j]) -> 4  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 4
}
Sat witness: Yes: (() -> max([z, y, y]), {
y -> s(z)
})

-------------------------------------------
Step 6, which took 0.004313 s (model generation: 0.004048,  model checking: 0.000265):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443},
Q_f={q_gen_2443},
Delta=
{
<s, s>(q_gen_2443) -> q_gen_2443
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, s, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 3  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (max([i, j, j])) -> leq([i, j]) -> 4  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 4
}
Sat witness: Yes: (() -> max([s(u), z, s(u)]), {
u -> s(z)
})

-------------------------------------------
Step 7, which took 0.005350 s (model generation: 0.005181,  model checking: 0.000169):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443},
Q_f={q_gen_2443},
Delta=
{
<s, s>(q_gen_2443) -> q_gen_2443
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<s, s>(q_gen_2442) -> q_gen_2442
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, s, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (max([i, j, j])) -> leq([i, j]) -> 4  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(z)
})

-------------------------------------------
Step 8, which took 0.005554 s (model generation: 0.005360,  model checking: 0.000194):

Model:
|_
{
leq -> 
{{{
Q={q_gen_2443, q_gen_2450},
Q_f={q_gen_2443},
Delta=
{
<z>() -> q_gen_2450
<s, s>(q_gen_2443) -> q_gen_2443
<z, s>(q_gen_2450) -> q_gen_2443
<z, z>() -> q_gen_2443
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_2440, q_gen_2442},
Q_f={q_gen_2440},
Delta=
{
<s, s>(q_gen_2442) -> q_gen_2442
<z, z>() -> q_gen_2442
<s, s, s>(q_gen_2440) -> q_gen_2440
<s, z, s>(q_gen_2442) -> q_gen_2440
<z, s, s>(q_gen_2442) -> q_gen_2440
<z, z, z>() -> q_gen_2440
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> max([s(u), z, s(u)]) -> 6  ;  () -> max([z, y, y]) -> 6  ;  (leq([i, j]) /\ max([i, j, _aqa])) -> eq_nat([_aqa, j]) -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (max([i, j, j])) -> leq([i, j]) -> 7  ;  (max([u, x2, _upa])) -> max([s(u), s(x2), s(_upa)]) -> 5
}
Sat witness: Yes: ((max([i, j, j])) -> leq([i, j]), {
i -> z  ;  j -> s(s(z))
})

Total time: 0.046701
Reason for stopping: Proved

