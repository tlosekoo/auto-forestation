Solving ../../../../benchmarks/smtlib/true/isaplanner_prop32.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(min, F:
{() -> min([s(u), z, z])
() -> min([z, y, z])
(min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)])}
(min([_oy, _py, _qy]) /\ min([_oy, _py, _ry])) -> eq_nat([_qy, _ry])
)
}

properties:
{(min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty])}


over-approximation: {min}
under-approximation: {}

Clause system for inference is:

{
() -> min([s(u), z, z]) -> 0  ;  () -> min([z, y, z]) -> 0  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 0  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 0
}


Solving took 0.025144 seconds.
Proved
Model:
|_
{
min -> 
{{{
Q={q_gen_2849, q_gen_2851},
Q_f={q_gen_2849},
Delta=
{
<s>(q_gen_2851) -> q_gen_2851
<z>() -> q_gen_2851
<s, s, s>(q_gen_2849) -> q_gen_2849
<s, z, z>(q_gen_2851) -> q_gen_2849
<z, s, z>(q_gen_2851) -> q_gen_2849
<z, z, z>() -> q_gen_2849
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.002985 s (model generation: 0.002878,  model checking: 0.000107):

Model:
|_
{
min -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> min([s(u), z, z]) -> 0  ;  () -> min([z, y, z]) -> 3  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 1  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 1
}
Sat witness: Yes: (() -> min([z, y, z]), {
y -> z
})

-------------------------------------------
Step 1, which took 0.004189 s (model generation: 0.003152,  model checking: 0.001037):

Model:
|_
{
min -> 
{{{
Q={q_gen_2849},
Q_f={q_gen_2849},
Delta=
{
<z, z, z>() -> q_gen_2849
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 1  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 1
}
Sat witness: Yes: (() -> min([s(u), z, z]), {
u -> z
})

-------------------------------------------
Step 2, which took 0.003154 s (model generation: 0.003069,  model checking: 0.000085):

Model:
|_
{
min -> 
{{{
Q={q_gen_2849, q_gen_2851},
Q_f={q_gen_2849},
Delta=
{
<z>() -> q_gen_2851
<s, z, z>(q_gen_2851) -> q_gen_2849
<z, z, z>() -> q_gen_2849
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 3  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 1  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 4
}
Sat witness: Yes: ((min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]), {
_ny -> z  ;  u -> z  ;  y1 -> z
})

-------------------------------------------
Step 3, which took 0.003216 s (model generation: 0.003104,  model checking: 0.000112):

Model:
|_
{
min -> 
{{{
Q={q_gen_2849, q_gen_2851},
Q_f={q_gen_2849},
Delta=
{
<z>() -> q_gen_2851
<s, s, s>(q_gen_2849) -> q_gen_2849
<s, z, z>(q_gen_2851) -> q_gen_2849
<z, z, z>() -> q_gen_2849
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> min([s(u), z, z]) -> 3  ;  () -> min([z, y, z]) -> 6  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 2  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 4
}
Sat witness: Yes: (() -> min([z, y, z]), {
y -> s(z)
})

-------------------------------------------
Step 4, which took 0.003219 s (model generation: 0.003071,  model checking: 0.000148):

Model:
|_
{
min -> 
{{{
Q={q_gen_2849, q_gen_2851},
Q_f={q_gen_2849},
Delta=
{
<z>() -> q_gen_2851
<s, s, s>(q_gen_2849) -> q_gen_2849
<s, z, z>(q_gen_2851) -> q_gen_2849
<z, s, z>(q_gen_2851) -> q_gen_2849
<z, z, z>() -> q_gen_2849
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> min([s(u), z, z]) -> 6  ;  () -> min([z, y, z]) -> 6  ;  (min([a, b, _sy]) /\ min([b, a, _ty])) -> eq_nat([_sy, _ty]) -> 3  ;  (min([u, y1, _ny])) -> min([s(u), s(y1), s(_ny)]) -> 4
}
Sat witness: Yes: (() -> min([s(u), z, z]), {
u -> s(z)
})

Total time: 0.025144
Reason for stopping: Proved

