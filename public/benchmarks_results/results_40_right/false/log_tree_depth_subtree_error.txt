Solving ../../../../benchmarks/smtlib/false/tree_depth_subtree_error.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
elt -> {a, b}  ;  etree -> {leaf, node}  ;  nat -> {s, z}
}
definition:
{
(leq_nat, P:
{() -> leq_nat([z, n2])
(leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)])
(leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2])
(leq_nat([s(nn1), z])) -> BOT}
)
(le_nat, P:
{() -> le_nat([z, s(nn2)])
(le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)])
(le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2])
(le_nat([s(nn1), z])) -> BOT
(le_nat([z, z])) -> BOT}
)
(subtree, P:
{() -> subtree([leaf, t])
(subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])
(subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2])
(not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT
(subtree([node(ea, ta1, ta2), leaf])) -> BOT
(subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT}
)
(max, F:
{(le_nat([n, m])) -> max([n, m, m])
(not le_nat([n, m])) -> max([n, m, n])}
(max([_xeb, _yeb, _afb]) /\ max([_xeb, _yeb, _zeb])) -> eq_nat([_zeb, _afb])
)
(height, F:
{() -> height([leaf, z])
(height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)])
(height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)])}
(height([_ffb, _gfb]) /\ height([_ffb, _hfb])) -> eq_nat([_gfb, _hfb])
)
}

properties:
{(height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb])}


over-approximation: {height, leq_nat, max, subtree}
under-approximation: {leq_nat, max}

Clause system for inference is:

{
() -> height([leaf, z]) -> 0  ;  () -> le_nat([z, s(nn2)]) -> 0  ;  () -> leq_nat([z, n2]) -> 0  ;  () -> subtree([leaf, t]) -> 0  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 0  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 0  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 0  ;  (le_nat([n, m])) -> max([n, m, m]) -> 0  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 0  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 0  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 0  ;  (le_nat([s(nn1), z])) -> BOT -> 0  ;  (le_nat([z, z])) -> BOT -> 0  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 0  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 0  ;  (leq_nat([s(nn1), z])) -> BOT -> 0  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 0  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 0  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 0  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 0  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 0
}


Solving took 0.120250 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005631 s (model generation: 0.004891,  model checking: 0.000740):

Model:
|_
{
height -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 0  ;  () -> le_nat([z, s(nn2)]) -> 0  ;  () -> leq_nat([z, n2]) -> 0  ;  () -> subtree([leaf, t]) -> 0  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 0  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 0  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 0  ;  (le_nat([n, m])) -> max([n, m, m]) -> 0  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: ((not le_nat([n, m])) -> max([n, m, n]), {
m -> z  ;  n -> z
})

-------------------------------------------
Step 1, which took 0.010153 s (model generation: 0.009536,  model checking: 0.000617):

Model:
|_
{
height -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723},
Q_f={},
Delta=
{
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={q_gen_10724},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 0  ;  () -> le_nat([z, s(nn2)]) -> 0  ;  () -> leq_nat([z, n2]) -> 0  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> subtree([leaf, t]), {
t -> leaf
})

-------------------------------------------
Step 2, which took 0.010161 s (model generation: 0.010025,  model checking: 0.000136):

Model:
|_
{
height -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723},
Q_f={},
Delta=
{
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={q_gen_10724},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 0  ;  () -> le_nat([z, s(nn2)]) -> 0  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> leq_nat([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 3, which took 0.010324 s (model generation: 0.010167,  model checking: 0.000157):

Model:
|_
{
height -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723},
Q_f={},
Delta=
{
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={q_gen_10724},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 0  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> le_nat([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 4, which took 0.010953 s (model generation: 0.010817,  model checking: 0.000136):

Model:
|_
{
height -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10728},
Q_f={q_gen_10723},
Delta=
{
<z>() -> q_gen_10728
<z, s>(q_gen_10728) -> q_gen_10723
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 1  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> height([leaf, z]), {

})

-------------------------------------------
Step 5, which took 0.011199 s (model generation: 0.010487,  model checking: 0.000712):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10728},
Q_f={q_gen_10723},
Delta=
{
<z>() -> q_gen_10728
<z, s>(q_gen_10728) -> q_gen_10723
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 1  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 1  ;  (leq_nat([s(nn1), z])) -> BOT -> 1  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]), {
eb -> b  ;  ta1 -> leaf  ;  ta2 -> leaf  ;  tb1 -> leaf  ;  tb2 -> leaf
})

-------------------------------------------
Step 6, which took 0.010763 s (model generation: 0.010552,  model checking: 0.000211):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10728},
Q_f={q_gen_10723},
Delta=
{
<z>() -> q_gen_10728
<z, s>(q_gen_10728) -> q_gen_10723
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725, q_gen_10731},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
<node, node>(q_gen_10731, q_gen_10725, q_gen_10725) -> q_gen_10725
<b, b>() -> q_gen_10731
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 1  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 4  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 2  ;  (leq_nat([s(nn1), z])) -> BOT -> 2  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 7, which took 0.009960 s (model generation: 0.009919,  model checking: 0.000041):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10728},
Q_f={q_gen_10723},
Delta=
{
<z>() -> q_gen_10728
<z, s>(q_gen_10728) -> q_gen_10723
<z, z>() -> q_gen_10723
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<s, s>(q_gen_10726) -> q_gen_10726
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725, q_gen_10731},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
<node, node>(q_gen_10731, q_gen_10725, q_gen_10725) -> q_gen_10725
<b, b>() -> q_gen_10731
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 1  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 1  ;  (le_nat([s(nn1), z])) -> BOT -> 1  ;  (le_nat([z, z])) -> BOT -> 4  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 4  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 2  ;  (leq_nat([s(nn1), z])) -> BOT -> 2  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((le_nat([z, z])) -> BOT, {

})

-------------------------------------------
Step 8, which took 0.009090 s (model generation: 0.008781,  model checking: 0.000309):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10727, q_gen_10728},
Q_f={q_gen_10727},
Delta=
{
<z>() -> q_gen_10728
<z, z>() -> q_gen_10723
<z, s>(q_gen_10728) -> q_gen_10727
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<s, s>(q_gen_10726) -> q_gen_10726
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={q_gen_10724},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725, q_gen_10731},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
<node, node>(q_gen_10731, q_gen_10725, q_gen_10725) -> q_gen_10725
<b, b>() -> q_gen_10731
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 1  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 4  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 2  ;  (le_nat([s(nn1), z])) -> BOT -> 2  ;  (le_nat([z, z])) -> BOT -> 4  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 4  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 2  ;  (leq_nat([s(nn1), z])) -> BOT -> 2  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> s(z)
})

-------------------------------------------
Step 9, which took 0.011185 s (model generation: 0.010925,  model checking: 0.000260):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10727, q_gen_10728},
Q_f={q_gen_10727},
Delta=
{
<z>() -> q_gen_10728
<z, z>() -> q_gen_10723
<s, s>(q_gen_10727) -> q_gen_10727
<z, s>(q_gen_10728) -> q_gen_10727
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<s, s>(q_gen_10726) -> q_gen_10726
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724},
Q_f={q_gen_10724},
Delta=
{
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725, q_gen_10731},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
<node, node>(q_gen_10731, q_gen_10725, q_gen_10725) -> q_gen_10725
<b, b>() -> q_gen_10731
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 1  ;  (le_nat([n, m])) -> max([n, m, m]) -> 4  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 4  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 2  ;  (le_nat([s(nn1), z])) -> BOT -> 2  ;  (le_nat([z, z])) -> BOT -> 4  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 4  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 2  ;  (leq_nat([s(nn1), z])) -> BOT -> 2  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((le_nat([n, m])) -> max([n, m, m]), {
m -> s(z)  ;  n -> z
})

-------------------------------------------
Step 10, which took 0.010502 s (model generation: 0.010338,  model checking: 0.000164):

Model:
|_
{
height -> 
{{{
Q={q_gen_10729},
Q_f={q_gen_10729},
Delta=
{
<leaf, z>() -> q_gen_10729
}

Datatype: <etree, nat>
Convolution form: right
}}}
  ;  le_nat -> 
{{{
Q={q_gen_10723, q_gen_10727, q_gen_10728},
Q_f={q_gen_10727},
Delta=
{
<z>() -> q_gen_10728
<z, z>() -> q_gen_10723
<s, s>(q_gen_10727) -> q_gen_10727
<z, s>(q_gen_10728) -> q_gen_10727
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  leq_nat -> 
{{{
Q={q_gen_10726},
Q_f={q_gen_10726},
Delta=
{
<s, s>(q_gen_10726) -> q_gen_10726
<z, z>() -> q_gen_10726
}

Datatype: <nat, nat>
Convolution form: right
}}}
  ;  max -> 
{{{
Q={q_gen_10724, q_gen_10735},
Q_f={q_gen_10724},
Delta=
{
<z, z>() -> q_gen_10735
<z, s, s>(q_gen_10735) -> q_gen_10724
<z, z, z>() -> q_gen_10724
}

Datatype: <nat, nat, nat>
Convolution form: right
}}}
  ;  subtree -> 
{{{
Q={q_gen_10725, q_gen_10731},
Q_f={q_gen_10725},
Delta=
{
<leaf, leaf>() -> q_gen_10725
<node, node>(q_gen_10731, q_gen_10725, q_gen_10725) -> q_gen_10725
<b, b>() -> q_gen_10731
}

Datatype: <etree, etree>
Convolution form: right
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> height([leaf, z]) -> 3  ;  () -> le_nat([z, s(nn2)]) -> 3  ;  () -> leq_nat([z, n2]) -> 3  ;  () -> subtree([leaf, t]) -> 3  ;  (height([t1, _bfb]) /\ height([t1, _efb]) /\ height([t2, _cfb]) /\ not le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_efb)]) -> 1  ;  (height([t1, _bfb]) /\ height([t2, _cfb]) /\ height([t2, _dfb]) /\ le_nat([_bfb, _cfb])) -> height([node(e, t1, t2), s(_dfb)]) -> 1  ;  (height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]) -> 4  ;  (le_nat([n, m])) -> max([n, m, m]) -> 4  ;  (not le_nat([n, m])) -> max([n, m, n]) -> 3  ;  (le_nat([nn1, nn2])) -> le_nat([s(nn1), s(nn2)]) -> 4  ;  (le_nat([s(nn1), s(nn2)])) -> le_nat([nn1, nn2]) -> 2  ;  (le_nat([s(nn1), z])) -> BOT -> 2  ;  (le_nat([z, z])) -> BOT -> 4  ;  (leq_nat([nn1, nn2])) -> leq_nat([s(nn1), s(nn2)]) -> 4  ;  (leq_nat([s(nn1), s(nn2)])) -> leq_nat([nn1, nn2]) -> 2  ;  (leq_nat([s(nn1), z])) -> BOT -> 2  ;  (subtree([ta1, tb1]) /\ subtree([ta2, tb2])) -> subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> subtree([ta2, tb2]) -> 2  ;  (not subtree([ta1, tb1]) /\ subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_elt([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((height([t1, _ifb]) /\ height([t2, _jfb]) /\ subtree([t1, t2])) -> le_nat([_ifb, _jfb]), {
_ifb -> z  ;  _jfb -> z  ;  t1 -> leaf  ;  t2 -> leaf
})

Total time: 0.120250
Reason for stopping: Disproved

