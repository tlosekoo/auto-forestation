Solving ../../../../benchmarks/smtlib/false/cons_is_equal.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: right


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{

}

properties:
{() -> eq_natlist([x, cons(y, x)])}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{
() -> eq_natlist([x, cons(y, x)]) -> 0
}


Solving took 0.019395 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.010049 s (model generation: 0.009754,  model checking: 0.000295):

Model:
|_
{

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> eq_natlist([x, cons(y, x)]) -> 3
}
Sat witness: Yes: (() -> eq_natlist([x, cons(y, x)]), {
x -> nil  ;  y -> z
})

Total time: 0.019395
Reason for stopping: Disproved

