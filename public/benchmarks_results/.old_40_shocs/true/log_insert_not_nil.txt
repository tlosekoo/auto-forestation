Solving ../../../../benchmarks/smtlib/true/insert_not_nil.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elist -> {econs, enil}  ;  elt -> {a, b}
}
definition:
{
(not_nil_elt, P:
{
not_nil_elt(econs(x, ll)) <= True
False <= not_nil_elt(enil)
}
)
(leq_elt, P:
{
leq_elt(a, e2) <= True
leq_elt(b, b) <= True
False <= leq_elt(b, a)
}
)
(insert_elt, F:
{
insert_elt(x, enil, econs(x, enil)) <= True
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu)
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y)
}
eq_elist(_lu, _mu) <= insert_elt(_ju, _ku, _lu) /\ insert_elt(_ju, _ku, _mu)
)
}

properties:
{
not_nil_elt(_nu) <= insert_elt(e, l, _nu)
}


over-approximation: {insert_elt}
under-approximation: {not_nil_elt}

Clause system for inference is:

{
insert_elt(x, enil, econs(x, enil)) <= True
leq_elt(a, e2) <= True
leq_elt(b, b) <= True
not_nil_elt(econs(x, ll)) <= True
not_nil_elt(_nu) <= insert_elt(e, l, _nu)
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu)
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y)
False <= leq_elt(b, a)
False <= not_nil_elt(enil)
}


Solving took 0.033710 seconds.
Yes: |_

insert_elt ->
{
  insert_elt(a, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(a, enil, econs(x_2_0, x_2_1)) <= True
  insert_elt(b, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(b, enil, econs(x_2_0, x_2_1)) <= True
}
;
leq_elt ->
{
  leq_elt(a, a) <= True
  leq_elt(a, b) <= True
  leq_elt(b, b) <= True
}
;
not_nil_elt ->
{
  not_nil_elt(econs(x_0_0, x_0_1)) <= True
}

--
Equality automata are defined for: {elist, elt}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006167 s (model generation: 0.006101,  model checking: 0.000066):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

insert_elt ->
{
  
}
;
leq_elt ->
{
  
}
;
not_nil_elt ->
{
  
}

--
Equality automata are defined for: {elist, elt}
_|


Answer of teacher:


insert_elt(x, enil, econs(x, enil)) <= True -> Yes: {
x -> b
}
leq_elt(a, e2) <= True -> Yes: {
e2 -> b
}
leq_elt(b, b) <= True -> Yes: {

}
not_nil_elt(econs(x, ll)) <= True -> Yes: {

}
not_nil_elt(_nu) <= insert_elt(e, l, _nu) -> No: ()
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu) -> No: ()
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y) -> No: ()
False <= leq_elt(b, a) -> No: ()
False <= not_nil_elt(enil) -> No: ()



-------------------------------------------
Step 1, which took 0.007071 s (model generation: 0.006876,  model checking: 0.000195):

Ground examples at the beginning of this step are:
{
insert_elt(b, enil, econs(b, enil)) <= True
leq_elt(a, b) <= True
leq_elt(b, b) <= True
not_nil_elt(econs(a, enil)) <= True
}

Learner proposed model:
Found Model:
|_

insert_elt ->
{
  insert_elt(b, enil, econs(x_2_0, x_2_1)) <= True
}
;
leq_elt ->
{
  leq_elt(a, b) <= True
  leq_elt(b, b) <= True
}
;
not_nil_elt ->
{
  not_nil_elt(econs(x_0_0, x_0_1)) <= True
}

--
Equality automata are defined for: {elist, elt}
_|


Answer of teacher:


insert_elt(x, enil, econs(x, enil)) <= True -> Yes: {
x -> a
}
leq_elt(a, e2) <= True -> Yes: {
e2 -> a
}
leq_elt(b, b) <= True -> No: ()
not_nil_elt(econs(x, ll)) <= True -> No: ()
not_nil_elt(_nu) <= insert_elt(e, l, _nu) -> No: ()
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu) -> Yes: {
_iu -> econs(_iu_0, _iu_1)  ;  x -> b  ;  y -> a  ;  z -> enil
}
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y) -> Yes: {
x -> b  ;  y -> b
}
False <= leq_elt(b, a) -> No: ()
False <= not_nil_elt(enil) -> No: ()



-------------------------------------------
Step 2, which took 0.009847 s (model generation: 0.009755,  model checking: 0.000092):

Ground examples at the beginning of this step are:
{
insert_elt(a, enil, econs(a, enil)) <= True
insert_elt(b, econs(b, enil), econs(b, econs(b, enil))) <= True
insert_elt(b, enil, econs(b, enil)) <= True
leq_elt(a, a) <= True
leq_elt(a, b) <= True
leq_elt(b, b) <= True
not_nil_elt(econs(a, enil)) <= True
insert_elt(b, econs(a, enil), econs(a, econs(a, enil))) \/ leq_elt(b, a) <= insert_elt(b, enil, econs(a, enil))
}

Learner proposed model:
Found Model:
|_

insert_elt ->
{
  insert_elt(a, enil, econs(x_2_0, x_2_1)) <= True
  insert_elt(b, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(b, enil, econs(x_2_0, x_2_1)) <= True
}
;
leq_elt ->
{
  leq_elt(a, a) <= True
  leq_elt(a, b) <= True
  leq_elt(b, a) <= True
  leq_elt(b, b) <= True
}
;
not_nil_elt ->
{
  not_nil_elt(econs(x_0_0, x_0_1)) <= True
}

--
Equality automata are defined for: {elist, elt}
_|


Answer of teacher:


insert_elt(x, enil, econs(x, enil)) <= True -> No: ()
leq_elt(a, e2) <= True -> No: ()
leq_elt(b, b) <= True -> No: ()
not_nil_elt(econs(x, ll)) <= True -> No: ()
not_nil_elt(_nu) <= insert_elt(e, l, _nu) -> No: ()
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu) -> No: ()
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y) -> Yes: {
x -> a  ;  y -> b
}
False <= leq_elt(b, a) -> Yes: {

}
False <= not_nil_elt(enil) -> No: ()



-------------------------------------------
Step 3, which took 0.010492 s (model generation: 0.010443,  model checking: 0.000049):

Ground examples at the beginning of this step are:
{
insert_elt(a, econs(b, enil), econs(a, econs(b, enil))) <= True
insert_elt(a, enil, econs(a, enil)) <= True
insert_elt(b, econs(b, enil), econs(b, econs(b, enil))) <= True
insert_elt(b, enil, econs(b, enil)) <= True
leq_elt(a, a) <= True
leq_elt(a, b) <= True
leq_elt(b, b) <= True
not_nil_elt(econs(a, enil)) <= True
insert_elt(b, econs(a, enil), econs(a, econs(a, enil))) <= insert_elt(b, enil, econs(a, enil))
False <= leq_elt(b, a)
}

Learner proposed model:
Found Model:
|_

insert_elt ->
{
  insert_elt(a, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(a, enil, econs(x_2_0, x_2_1)) <= True
  insert_elt(b, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(b, enil, econs(x_2_0, x_2_1)) <= True
}
;
leq_elt ->
{
  leq_elt(a, a) <= True
  leq_elt(a, b) <= True
  leq_elt(b, b) <= True
}
;
not_nil_elt ->
{
  not_nil_elt(econs(x_0_0, x_0_1)) <= True
}

--
Equality automata are defined for: {elist, elt}
_|


Answer of teacher:


insert_elt(x, enil, econs(x, enil)) <= True -> No: ()
leq_elt(a, e2) <= True -> No: ()
leq_elt(b, b) <= True -> No: ()
not_nil_elt(econs(x, ll)) <= True -> No: ()
not_nil_elt(_nu) <= insert_elt(e, l, _nu) -> No: ()
insert_elt(x, econs(y, z), econs(y, _iu)) \/ leq_elt(x, y) <= insert_elt(x, z, _iu) -> No: ()
insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y) -> No: ()
False <= leq_elt(b, a) -> No: ()
False <= not_nil_elt(enil) -> No: ()




Total time: 0.033710
Learner time: 0.033175
Teacher time: 0.000402
Reasons for stopping: Yes: |_

insert_elt ->
{
  insert_elt(a, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(a, enil, econs(x_2_0, x_2_1)) <= True
  insert_elt(b, econs(x_1_0, x_1_1), econs(x_2_0, x_2_1)) <= True
  insert_elt(b, enil, econs(x_2_0, x_2_1)) <= True
}
;
leq_elt ->
{
  leq_elt(a, a) <= True
  leq_elt(a, b) <= True
  leq_elt(b, b) <= True
}
;
not_nil_elt ->
{
  not_nil_elt(econs(x_0_0, x_0_1)) <= True
}

--
Equality automata are defined for: {elist, elt}
_|


