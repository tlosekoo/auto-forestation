Solving ../../../../benchmarks/smtlib/true/prefix_nil.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elist -> {econs, enil}  ;  elt -> {a, b}  ;  nat -> {s, z}
}
definition:
{
(prefix_elt, P:
{
prefix_elt(enil, l) <= True
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys)
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys))
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys))
False <= prefix_elt(econs(z, zs), enil)
}
)
}

properties:
{
prefix_elt(enil, l) <= True
}


over-approximation: {}
under-approximation: {prefix_elt}

Clause system for inference is:

{
prefix_elt(enil, l) <= True
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys)
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys))
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys))
False <= prefix_elt(econs(z, zs), enil)
}


Solving took 0.042409 seconds.
Yes: |_

_r_1 ->
{
  _r_1(a, a) <= True
  _r_1(b, b) <= True
}
;
prefix_elt ->
{
  prefix_elt(econs(x_0_0, x_0_1), econs(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ prefix_elt(x_0_1, x_1_1)
  prefix_elt(enil, econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005767 s (model generation: 0.005712,  model checking: 0.000055):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

prefix_elt ->
{
  
}

--
Equality automata are defined for: {elist, elt, nat}
_|


Answer of teacher:


prefix_elt(enil, l) <= True -> Yes: {
l -> enil
}
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys) -> No: ()
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys)) -> No: ()
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys)) -> No: ()
False <= prefix_elt(econs(z, zs), enil) -> No: ()



-------------------------------------------
Step 1, which took 0.005292 s (model generation: 0.005247,  model checking: 0.000045):

Ground examples at the beginning of this step are:
{
prefix_elt(enil, enil) <= True
}

Learner proposed model:
Found Model:
|_

prefix_elt ->
{
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|


Answer of teacher:


prefix_elt(enil, l) <= True -> Yes: {
l -> econs(l_0, l_1)
}
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys) -> Yes: {
ys -> enil  ;  zs -> enil
}
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys)) -> No: ()
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys)) -> No: ()
False <= prefix_elt(econs(z, zs), enil) -> No: ()



-------------------------------------------
Step 2, which took 0.008165 s (model generation: 0.007656,  model checking: 0.000509):

Ground examples at the beginning of this step are:
{
prefix_elt(econs(a, enil), econs(a, enil)) <= True
prefix_elt(enil, econs(a, enil)) <= True
prefix_elt(enil, enil) <= True
}

Learner proposed model:
Found Model:
|_

prefix_elt ->
{
  prefix_elt(econs(x_0_0, x_0_1), econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|


Answer of teacher:


prefix_elt(enil, l) <= True -> No: ()
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys) -> No: ()
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys)) -> Yes: {
ys -> enil  ;  zs -> econs(v0_0, v0_1)
}
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys)) -> Yes: {
y2 -> a  ;  z -> b
}
False <= prefix_elt(econs(z, zs), enil) -> No: ()



-------------------------------------------
Step 3, which took 0.011394 s (model generation: 0.011209,  model checking: 0.000185):

Ground examples at the beginning of this step are:
{
prefix_elt(econs(a, enil), econs(a, enil)) <= True
prefix_elt(enil, econs(a, enil)) <= True
prefix_elt(enil, enil) <= True
prefix_elt(econs(a, enil), enil) <= prefix_elt(econs(a, econs(a, enil)), econs(a, enil))
False <= prefix_elt(econs(b, enil), econs(a, enil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(a) <= True
}
;
prefix_elt ->
{
  prefix_elt(econs(x_0_0, x_0_1), econs(x_1_0, x_1_1)) <= _r_1(x_0_0)
  prefix_elt(econs(x_0_0, x_0_1), enil) <= True
  prefix_elt(enil, econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|


Answer of teacher:


prefix_elt(enil, l) <= True -> No: ()
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys) -> Yes: {
y2 -> b  ;  ys -> enil  ;  zs -> enil
}
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys)) -> Yes: {
y2 -> a  ;  ys -> econs(v1_0, v1_1)  ;  zs -> econs(b, v0_1)
}
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys)) -> Yes: {
y2 -> b  ;  z -> a
}
False <= prefix_elt(econs(z, zs), enil) -> Yes: {

}



-------------------------------------------
Step 4, which took 0.011676 s (model generation: 0.011544,  model checking: 0.000132):

Ground examples at the beginning of this step are:
{
prefix_elt(econs(a, enil), econs(a, enil)) <= True
prefix_elt(econs(b, enil), econs(b, enil)) <= True
prefix_elt(enil, econs(a, enil)) <= True
prefix_elt(enil, enil) <= True
False <= prefix_elt(econs(a, econs(a, enil)), econs(a, enil))
False <= prefix_elt(econs(a, econs(b, enil)), econs(a, econs(a, enil)))
False <= prefix_elt(econs(a, enil), econs(b, enil))
False <= prefix_elt(econs(a, enil), enil)
False <= prefix_elt(econs(b, enil), econs(a, enil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(a, a) <= True
  _r_1(b, b) <= True
}
;
prefix_elt ->
{
  prefix_elt(econs(x_0_0, x_0_1), econs(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ prefix_elt(x_0_1, x_1_1)
  prefix_elt(enil, econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|


Answer of teacher:


prefix_elt(enil, l) <= True -> No: ()
prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys) -> No: ()
prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys)) -> No: ()
eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys)) -> No: ()
False <= prefix_elt(econs(z, zs), enil) -> No: ()




Total time: 0.042409
Learner time: 0.041368
Teacher time: 0.000926
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(a, a) <= True
  _r_1(b, b) <= True
}
;
prefix_elt ->
{
  prefix_elt(econs(x_0_0, x_0_1), econs(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ prefix_elt(x_0_1, x_1_1)
  prefix_elt(enil, econs(x_1_0, x_1_1)) <= True
  prefix_elt(enil, enil) <= True
}

--
Equality automata are defined for: {elist, elt, nat}
_|


