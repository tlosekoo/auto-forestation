Solving ../../../../benchmarks/smtlib/true/length_cons_le.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(le, P:
{
le(z, s(nn2)) <= True
False <= le(n1, z)
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
}
)
(length, F:
{
length(nil, z) <= True
length(cons(x, ll), s(_xr)) <= length(ll, _xr)
}
eq_nat(_zr, _as) <= length(_yr, _as) /\ length(_yr, _zr)
)
}

properties:
{
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs)
}


over-approximation: {length}
under-approximation: {le}

Clause system for inference is:

{
le(z, s(nn2)) <= True
length(nil, z) <= True
False <= le(n1, z)
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs)
length(cons(x, ll), s(_xr)) <= length(ll, _xr)
}


Solving took 0.042380 seconds.
Yes: |_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005624 s (model generation: 0.005512,  model checking: 0.000112):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

le ->
{
  
}
;
length ->
{
  
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> Yes: {

}
length(nil, z) <= True -> Yes: {

}
False <= le(n1, z) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> No: ()
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> No: ()



-------------------------------------------
Step 1, which took 0.005759 s (model generation: 0.005697,  model checking: 0.000062):

Ground examples at the beginning of this step are:
{
le(z, s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
False <= le(n1, z) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> s(v1_0)
}
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> No: ()
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> Yes: {
_xr -> z  ;  ll -> nil
}



-------------------------------------------
Step 2, which took 0.006884 s (model generation: 0.006616,  model checking: 0.000268):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(z, nil), s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
False <= le(n1, z) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> No: ()
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> No: ()



-------------------------------------------
Step 3, which took 0.007688 s (model generation: 0.007633,  model checking: 0.000055):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(z, nil), s(z)) <= True
length(nil, z) <= True
le(z, z) <= le(s(z), s(z))
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
  le(z, z) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
False <= le(n1, z) -> Yes: {
n1 -> z
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> No: ()
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> No: ()



-------------------------------------------
Step 4, which took 0.006977 s (model generation: 0.006892,  model checking: 0.000085):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(z, nil), s(z)) <= True
length(nil, z) <= True
le(s(z), z) <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(s(x_0_0), z) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
False <= le(n1, z) -> Yes: {
n1 -> s(n1_0)
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> Yes: {
_bs -> s(z)  ;  _cs -> s(z)  ;  l -> cons(l_0, l_1)
}
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> No: ()



-------------------------------------------
Step 5, which took 0.009343 s (model generation: 0.009258,  model checking: 0.000085):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(z, nil), s(z)) <= True
length(nil, z) <= True
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
False <= length(cons(z, cons(z, nil)), s(z))
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
False <= le(n1, z) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
le(_bs, _cs) <= length(l, _bs) /\ length(cons(x, l), _cs) -> No: ()
length(cons(x, ll), s(_xr)) <= length(ll, _xr) -> No: ()




Total time: 0.042380
Learner time: 0.041608
Teacher time: 0.000667
Reasons for stopping: Yes: |_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {nat, natlist}
_|


