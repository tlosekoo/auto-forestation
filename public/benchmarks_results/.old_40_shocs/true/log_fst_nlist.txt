Solving ../../../../benchmarks/smtlib/true/fst_nlist.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  nat -> {s, z}  ;  nlist -> {ncons, nnil}  ;  nlpair -> {nlpair}
}
definition:
{
(fst_nlist, F:
{
fst_nlist(nlpair(l1, l2), l1) <= True
}
eq_nlist(_hd, _id) <= fst_nlist(_gd, _hd) /\ fst_nlist(_gd, _id)
)
}

properties:
{
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd)
}


over-approximation: {fst_nlist}
under-approximation: {}

Clause system for inference is:

{
fst_nlist(nlpair(l1, l2), l1) <= True
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd)
}


Solving took 14.631564 seconds.
Yes: |_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1) /\ _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_3(s(x_0_0), z) <= _r_5(x_0_0)
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005521 s (model generation: 0.005490,  model checking: 0.000031):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

fst_nlist ->
{
  
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> nnil
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> No: ()



-------------------------------------------
Step 1, which took 0.005526 s (model generation: 0.005500,  model checking: 0.000026):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(nnil, nnil), nnil) <= True
}

Learner proposed model:
Found Model:
|_

fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= True
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(l1_0, l1_1)
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> nnil  ;  l1 -> ncons(l1_0, l1_1)
}



-------------------------------------------
Step 2, which took 0.010673 s (model generation: 0.010606,  model checking: 0.000067):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(nnil) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= True
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_1(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(_jd_0, _jd_1)  ;  l1 -> nnil
}



-------------------------------------------
Step 3, which took 0.013639 s (model generation: 0.013532,  model checking: 0.000107):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(nnil) <= True
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1)) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_1(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(z, _jd_1)  ;  l1 -> ncons(s(v1_0), l1_1)
}



-------------------------------------------
Step 4, which took 0.021171 s (model generation: 0.020665,  model checking: 0.000506):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1)) <= _r_3(x_0_0)
}
;
_r_2 ->
{
  _r_2(nnil) <= True
}
;
_r_3 ->
{
  _r_3(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(s(v0_0), l1_1)
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(_jd_0, nnil)  ;  l1 -> ncons(z, ncons(v1_0, v1_1))
}



-------------------------------------------
Step 5, which took 0.035425 s (model generation: 0.035022,  model checking: 0.000403):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), s(x_1_0)) <= True
  _r_1(ncons(x_0_0, x_0_1), z) <= _r_2(x_0_1) /\ _r_3(x_0_0)
}
;
_r_2 ->
{
  _r_2(nnil) <= True
}
;
_r_3 ->
{
  _r_3(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(z, ncons(v0_0, v0_1))
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(v3_0), nnil)  ;  l1 -> ncons(l1_0, ncons(v1_0, v1_1))
}



-------------------------------------------
Step 6, which took 0.102989 s (model generation: 0.102559,  model checking: 0.000430):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), s(x_1_0)) <= True
  _r_1(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1)) <= _r_3(x_0_1)
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ _r_2(x_0_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ _r_2(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(l1_0, ncons(v0_0, ncons(v0_0, v0_1)))
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(v0_0), ncons(v0_0, nnil))  ;  l1 -> ncons(z, l1_1)
}



-------------------------------------------
Step 7, which took 0.179516 s (model generation: 0.179104,  model checking: 0.000412):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= True
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_3(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
  _r_2(nnil, s(x_1_0)) <= True
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_1, x_1_0) /\ _r_3(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(s(v1_0), l1_1)  ;  l2 -> ncons(v0_0, v0_1)
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(z, ncons(v0_0, v0_1))  ;  l1 -> ncons(z, nnil)
}



-------------------------------------------
Step 8, which took 0.248322 s (model generation: 0.247702,  model checking: 0.000620):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_3(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= True
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(v0_0), ncons(s(v1_0), v3_1))  ;  l1 -> ncons(z, ncons(v0_0, v0_1))
}



-------------------------------------------
Step 9, which took 0.251823 s (model generation: 0.251316,  model checking: 0.000507):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_3(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
  _r_2(nnil, s(x_1_0)) <= True
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_1, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(l1_0, ncons(s(v0_0), v1_1))  ;  l2 -> nnil
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(_jd_0, ncons(s(v1_0), v1_1))  ;  l1 -> nnil  ;  l2 -> ncons(v0_0, nnil)
}



-------------------------------------------
Step 10, which took 0.482655 s (model generation: 0.481915,  model checking: 0.000740):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), s(x_1_0)) <= True
  _r_1(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
  _r_1(nnil, z) <= True
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1)) <= _r_3(x_0_1)
}
;
_r_3 ->
{
  _r_3(ncons(x_0_0, x_0_1)) <= _r_2(x_0_1)
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ _r_1(x_0_1, x_1_0) /\ _r_2(x_1_1) /\ _r_3(x_0_0) /\ _r_3(x_0_1)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_0) /\ _r_2(x_0_0) /\ _r_3(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(l1_0, ncons(v0_0, nnil))  ;  l2 -> ncons(v0_0, nnil)
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> nnil  ;  l1 -> ncons(l1_0, ncons(v0_0, nnil))
}



-------------------------------------------
Step 11, which took 0.504564 s (model generation: 0.503582,  model checking: 0.000982):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_3(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= True
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
  _r_2(nnil, z) <= True
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0) /\ _r_2(x_0_1, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0) /\ _r_3(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(s(v1_0), ncons(v0_0, v0_1))  ;  l2 -> nnil
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(v0_0), nnil)  ;  l1 -> ncons(z, nnil)
}



-------------------------------------------
Step 12, which took 0.573460 s (model generation: 0.573089,  model checking: 0.000371):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_4(x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_2(x_0_1) /\ _r_3(x_0_0)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1)) <= _r_4(x_0_0)
  _r_2(nnil) <= True
}
;
_r_3 ->
{
  _r_3(z) <= True
}
;
_r_4 ->
{
  _r_4(s(x_0_0)) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_3(x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_0) /\ _r_4(x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(v0_0), _jd_1)  ;  l1 -> nnil
}



-------------------------------------------
Step 13, which took 2.192617 s (model generation: 2.191980,  model checking: 0.000637):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), z) <= True
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> Yes: {
l1 -> ncons(s(s(v0_0)), l1_1)
}
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(z), nnil)  ;  l1 -> ncons(s(s(v1_0)), nnil)
}



-------------------------------------------
Step 14, which took 3.010324 s (model generation: 3.009744,  model checking: 0.000580):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(s(z)), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), s(x_1_0)) <= True
  _r_3(s(x_0_0), z) <= _r_5(x_0_0)
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(s(s(v0_0)), nnil)  ;  l1 -> ncons(s(z), nnil)
}



-------------------------------------------
Step 15, which took 1.653737 s (model generation: 1.648303,  model checking: 0.005434):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(s(z)), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(s(z)), nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_3(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_4(x_0_0) /\ _r_5(x_1_0)
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_4(x_1_0) /\ _r_5(x_0_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_4(x_0_0)
  _r_2(nnil, z) <= True
}
;
_r_3 ->
{
  _r_3(nnil) <= True
}
;
_r_4 ->
{
  _r_4(s(x_0_0)) <= _r_5(x_0_0)
  _r_4(z) <= True
}
;
_r_5 ->
{
  _r_5(s(x_0_0)) <= _r_4(x_0_0)
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0) /\ _r_2(x_0_1, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0) /\ _r_3(x_0_1) /\ _r_3(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0) /\ _r_3(x_1_1)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_3(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(z, nnil)  ;  l1 -> ncons(s(s(z)), nnil)
}



-------------------------------------------
Step 16, which took 2.585196 s (model generation: 2.584504,  model checking: 0.000692):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(s(z)), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(s(z)), nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_3(s(x_0_0), z) <= _r_5(x_0_0)
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> Yes: {
_jd -> ncons(z, ncons(v0_0, ncons(z, nnil)))  ;  l1 -> ncons(z, ncons(v1_0, ncons(s(v1_0), nnil)))
}



-------------------------------------------
Step 17, which took 2.753886 s (model generation: 2.753202,  model checking: 0.000684):

Ground examples at the beginning of this step are:
{
fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(s(z)), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), ncons(z, nnil)), nnil), ncons(s(z), ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(s(z), nnil), ncons(z, nnil)), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(z), nnil)) <= True
fst_nlist(nlpair(ncons(z, ncons(s(z), nnil)), nnil), ncons(z, ncons(s(z), nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, ncons(z, nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil)))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), ncons(z, nnil)), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, ncons(z, nnil))) <= True
fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, nnil)) <= True
fst_nlist(nlpair(nnil, nnil), nnil) <= True
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(s(s(z)), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(s(s(z)), nnil))
False <= fst_nlist(nlpair(ncons(s(z), nnil), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, ncons(s(z), nnil))), nnil), ncons(z, ncons(z, ncons(z, nnil))))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), ncons(s(z), nnil)))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), ncons(z, nnil))
False <= fst_nlist(nlpair(ncons(z, ncons(z, nnil)), nnil), nnil)
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), ncons(z, ncons(z, nnil)))
False <= fst_nlist(nlpair(ncons(z, nnil), nnil), nnil)
False <= fst_nlist(nlpair(nnil, ncons(z, nnil)), ncons(z, ncons(s(z), nnil)))
False <= fst_nlist(nlpair(nnil, nnil), ncons(s(z), nnil))
False <= fst_nlist(nlpair(nnil, nnil), ncons(z, nnil))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1) /\ _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_3(s(x_0_0), z) <= _r_5(x_0_0)
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


Answer of teacher:


fst_nlist(nlpair(l1, l2), l1) <= True -> No: ()
eq_nlist(_jd, l1) <= fst_nlist(nlpair(l1, l2), _jd) -> No: ()




Total time: 14.631564
Learner time: 14.617815
Teacher time: 0.013229
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(ncons(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1) /\ _r_2(x_0_1, x_1_0)
  _r_1(ncons(x_0_0, x_0_1), nnil) <= _r_4(x_0_1)
}
;
_r_2 ->
{
  _r_2(ncons(x_0_0, x_0_1), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_2(ncons(x_0_0, x_0_1), z) <= _r_5(x_0_0)
}
;
_r_3 ->
{
  _r_3(s(x_0_0), s(x_1_0)) <= _r_3(x_0_0, x_1_0)
  _r_3(s(x_0_0), z) <= _r_5(x_0_0)
}
;
_r_4 ->
{
  _r_4(nnil) <= True
}
;
_r_5 ->
{
  _r_5(z) <= True
}
;
fst_nlist ->
{
  fst_nlist(nlpair(x_0_0, x_0_1), ncons(x_1_0, x_1_1)) <= _r_1(x_0_0, x_1_1) /\ _r_2(x_0_0, x_1_0)
  fst_nlist(nlpair(x_0_0, x_0_1), nnil) <= _r_4(x_0_0)
}

--
Equality automata are defined for: {elt, nat, nlist, nlpair}
_|


