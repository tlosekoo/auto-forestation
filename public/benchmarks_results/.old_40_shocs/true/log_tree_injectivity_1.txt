Solving ../../../../benchmarks/smtlib/true/tree_injectivity_1.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}  ;  nat_bin_tree -> {leaf, node}
}
definition:
{

}

properties:
{

}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{

}


Solving took 0.005513 seconds.
Yes: |_



--
Equality automata are defined for: {nat, nat_bin_tree}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005475 s (model generation: 0.005472,  model checking: 0.000003):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_



--
Equality automata are defined for: {nat, nat_bin_tree}
_|


Answer of teacher:







Total time: 0.005513
Learner time: 0.005472
Teacher time: 0.000003
Reasons for stopping: Yes: |_



--
Equality automata are defined for: {nat, nat_bin_tree}
_|


