Solving ../../../../benchmarks/smtlib/true/timbuk_equalNat.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}
}
definition:
{

}

properties:
{

}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{

}


Solving took 0.005798 seconds.
Yes: |_



--
Equality automata are defined for: {nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005778 s (model generation: 0.005775,  model checking: 0.000003):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_



--
Equality automata are defined for: {nat}
_|


Answer of teacher:







Total time: 0.005798
Learner time: 0.005775
Teacher time: 0.000003
Reasons for stopping: Yes: |_



--
Equality automata are defined for: {nat}
_|


