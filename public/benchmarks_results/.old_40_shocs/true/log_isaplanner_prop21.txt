Solving ../../../../benchmarks/smtlib/true/isaplanner_prop21.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(plus, F:
{
plus(n, z, n) <= True
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz)
}
eq_nat(_iz, _jz) <= plus(_gz, _hz, _iz) /\ plus(_gz, _hz, _jz)
)
(leq, P:
{
leq(z, s(nn2)) <= True
leq(z, z) <= True
leq(s(nn1), s(nn2)) <= leq(nn1, nn2)
leq(nn1, nn2) <= leq(s(nn1), s(nn2))
False <= leq(s(nn1), z)
}
)
}

properties:
{
leq(n, _kz) <= plus(n, m, _kz)
}


over-approximation: {plus}
under-approximation: {leq}

Clause system for inference is:

{
leq(z, s(nn2)) <= True
leq(z, z) <= True
plus(n, z, n) <= True
leq(s(nn1), s(nn2)) <= leq(nn1, nn2)
leq(nn1, nn2) <= leq(s(nn1), s(nn2))
False <= leq(s(nn1), z)
leq(n, _kz) <= plus(n, m, _kz)
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz)
}


Solving took 0.057362 seconds.
Yes: |_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005899 s (model generation: 0.005552,  model checking: 0.000347):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

leq ->
{
  
}
;
plus ->
{
  
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> Yes: {

}
leq(z, z) <= True -> Yes: {

}
plus(n, z, n) <= True -> Yes: {
n -> z
}
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> No: ()
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> No: ()



-------------------------------------------
Step 1, which took 0.005689 s (model generation: 0.005627,  model checking: 0.000062):

Ground examples at the beginning of this step are:
{
leq(z, s(z)) <= True
leq(z, z) <= True
plus(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> Yes: {
n -> s(n_0)
}
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> No: ()
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> Yes: {
_fz -> z  ;  mm -> z  ;  n -> z
}



-------------------------------------------
Step 2, which took 0.009045 s (model generation: 0.008970,  model checking: 0.000075):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= True
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> No: ()
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> Yes: {
_fz -> s(_fz_0)  ;  mm -> z  ;  n -> s(n_0)
}



-------------------------------------------
Step 3, which took 0.011826 s (model generation: 0.011385,  model checking: 0.000441):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
leq(s(z), z) <= leq(s(s(z)), s(z))
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= True
  leq(s(x_0_0), z) <= True
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> Yes: {

}
leq(n, _kz) <= plus(n, m, _kz) -> No: ()
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> No: ()



-------------------------------------------
Step 4, which took 0.007422 s (model generation: 0.007279,  model checking: 0.000143):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= leq(s(s(z)), s(z))
False <= leq(s(z), z)
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> Yes: {
_kz -> s(z)  ;  m -> z  ;  n -> s(s(v0_0))
}
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> No: ()



-------------------------------------------
Step 5, which took 0.007821 s (model generation: 0.007063,  model checking: 0.000758):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= leq(s(s(z)), s(z))
False <= leq(s(z), z)
False <= plus(s(s(z)), z, s(z))
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  plus(s(x_0_0), z, s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> Yes: {
_kz -> s(z)  ;  m -> s(m_0)  ;  n -> s(s(v0_0))
}
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> No: ()



-------------------------------------------
Step 6, which took 0.009532 s (model generation: 0.009415,  model checking: 0.000117):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= leq(s(s(z)), s(z))
False <= leq(s(z), z)
False <= plus(s(s(z)), s(z), s(z))
False <= plus(s(s(z)), z, s(z))
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, s(nn2)) <= True -> No: ()
leq(z, z) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(n, _kz) <= plus(n, m, _kz) -> No: ()
plus(n, s(mm), s(_fz)) <= plus(n, mm, _fz) -> No: ()




Total time: 0.057362
Learner time: 0.055291
Teacher time: 0.001943
Reasons for stopping: Yes: |_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= leq(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


