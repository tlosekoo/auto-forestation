Solving ../../../../benchmarks/smtlib/false/isaplanner_prop11.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(drop, F:
{
drop(s(u), nil, nil) <= True
drop(z, l, l) <= True
drop(s(u), cons(x2, x3), _lkb) <= drop(u, x3, _lkb)
}
eq_eltlist(_okb, _pkb) <= drop(_mkb, _nkb, _okb) /\ drop(_mkb, _nkb, _pkb)
)
}

properties:
{
False <= drop(z, _qkb, _qkb)
}


over-approximation: {drop}
under-approximation: {}

Clause system for inference is:

{
drop(s(u), nil, nil) <= True
drop(z, l, l) <= True
drop(s(u), cons(x2, x3), _lkb) <= drop(u, x3, _lkb)
False <= drop(z, _qkb, _qkb)
}


Solving took 0.055599 seconds.
No: Contradictory set of ground constraints:
{
drop(s(z), nil, nil) <= True
drop(z, cons(a, nil), cons(a, nil)) <= True
drop(z, nil, nil) <= True
False <= drop(z, nil, nil)
}
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006754 s (model generation: 0.006687,  model checking: 0.000067):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

drop ->
{
  
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


drop(s(u), nil, nil) <= True -> Yes: {

}
drop(z, l, l) <= True -> Yes: {
l -> nil
}
drop(s(u), cons(x2, x3), _lkb) <= drop(u, x3, _lkb) -> No: ()
False <= drop(z, _qkb, _qkb) -> No: ()



-------------------------------------------
Step 1, which took 0.015179 s (model generation: 0.015057,  model checking: 0.000122):

Ground examples at the beginning of this step are:
{
drop(s(z), nil, nil) <= True
drop(z, nil, nil) <= True
}

Learner proposed model:
Found Model:
|_

drop ->
{
  drop(s(x_0_0), nil, nil) <= True
  drop(z, nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


drop(s(u), nil, nil) <= True -> No: ()
drop(z, l, l) <= True -> Yes: {
l -> cons(l_0, l_1)
}
drop(s(u), cons(x2, x3), _lkb) <= drop(u, x3, _lkb) -> Yes: {
_lkb -> nil  ;  u -> z  ;  x3 -> nil
}
False <= drop(z, _qkb, _qkb) -> Yes: {
_qkb -> nil
}



Ground examples:
{
drop(s(z), nil, nil) <= True
drop(z, cons(a, nil), cons(a, nil)) <= True
drop(z, nil, nil) <= True
False <= drop(z, nil, nil)
}

Learner output:
Contradictory ground constraints

Last ice step stopped before teacher could answer
Total time: 0.055599
Learner time: 0.021744
Teacher time: 0.000189
Reasons for stopping: No: Contradictory set of ground constraints:
{
drop(s(z), nil, nil) <= True
drop(z, cons(a, nil), cons(a, nil)) <= True
drop(z, nil, nil) <= True
False <= drop(z, nil, nil)
}


