Solving ../../../../benchmarks/smtlib/false/timbuk_reverseBUGimplies.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}
}
definition:
{
(append_bug, F:
{
append_bug(nil, l2, nil) <= True
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb)
}
eq_eltlist(_bib, _cib) <= append_bug(_zhb, _aib, _bib) /\ append_bug(_zhb, _aib, _cib)
)
(reverse, F:
{
reverse(nil, nil) <= True
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib)
}
eq_eltlist(_gib, _hib) <= reverse(_fib, _gib) /\ reverse(_fib, _hib)
)
(member, P:
{
member(h1, cons(h1, t1)) <= True
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1)
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1))
False <= member(e, nil)
}
)
}

properties:
{
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib)
}


over-approximation: {append_bug, reverse}
under-approximation: {}

Clause system for inference is:

{
append_bug(nil, l2, nil) <= True
member(h1, cons(h1, t1)) <= True
reverse(nil, nil) <= True
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib)
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb)
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib)
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1)
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1))
False <= member(e, nil)
}


Solving took 0.521170 seconds.
No: Contradictory set of ground constraints:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, cons(b, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(a, cons(b, cons(a, nil))) <= True
member(b, cons(a, cons(a, cons(b, nil)))) <= True
member(b, cons(a, cons(b, nil))) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
False <= append_bug(nil, cons(b, nil), nil)
False <= member(a, cons(a, cons(a, nil))) /\ reverse(cons(a, cons(a, nil)), nil)
False <= member(a, cons(b, cons(b, nil)))
False <= member(a, cons(b, nil))
False <= member(a, nil)
False <= member(b, cons(a, cons(a, nil)))
False <= member(b, cons(a, nil))
False <= member(b, nil)
False <= reverse(cons(b, nil), nil)
}
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.005138 s (model generation: 0.005053,  model checking: 0.000085):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

append_bug ->
{
  
}
;
member ->
{
  
}
;
reverse ->
{
  
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> Yes: {
l2 -> nil
}
member(h1, cons(h1, t1)) <= True -> Yes: {
h1 -> b
}
reverse(nil, nil) <= True -> Yes: {

}
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> No: ()
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> No: ()
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> No: ()
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> No: ()
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> No: ()
False <= member(e, nil) -> No: ()



-------------------------------------------
Step 1, which took 0.006628 s (model generation: 0.006558,  model checking: 0.000070):

Ground examples at the beginning of this step are:
{
append_bug(nil, nil, nil) <= True
member(b, cons(b, nil)) <= True
reverse(nil, nil) <= True
}

Learner proposed model:
Found Model:
|_

append_bug ->
{
  append_bug(nil, nil, nil) <= True
}
;
member ->
{
  member(b, cons(x_1_0, x_1_1)) <= True
}
;
reverse ->
{
  reverse(nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> Yes: {
l2 -> cons(l2_0, l2_1)
}
member(h1, cons(h1, t1)) <= True -> Yes: {
h1 -> a
}
reverse(nil, nil) <= True -> No: ()
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> No: ()
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> Yes: {
_yhb -> nil  ;  l2 -> nil  ;  t1 -> nil
}
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> No: ()
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> No: ()
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> nil
}
False <= member(e, nil) -> No: ()



-------------------------------------------
Step 2, which took 0.007474 s (model generation: 0.007392,  model checking: 0.000082):

Ground examples at the beginning of this step are:
{
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(b, cons(b, nil)) <= True
reverse(nil, nil) <= True
member(b, nil) <= member(b, cons(a, nil))
}

Learner proposed model:
Found Model:
|_

append_bug ->
{
  append_bug(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append_bug(nil, cons(x_1_0, x_1_1), nil) <= True
  append_bug(nil, nil, nil) <= True
}
;
member ->
{
  member(a, cons(x_1_0, x_1_1)) <= True
  member(b, cons(x_1_0, x_1_1)) <= True
  member(b, nil) <= True
}
;
reverse ->
{
  reverse(nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> No: ()
member(h1, cons(h1, t1)) <= True -> No: ()
reverse(nil, nil) <= True -> No: ()
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> Yes: {
_dib -> nil  ;  _eib -> nil  ;  t1 -> nil
}
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> Yes: {
_yhb -> nil  ;  l2 -> cons(l2_0, l2_1)  ;  t1 -> nil
}
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> No: ()
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> No: ()
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> nil
}
False <= member(e, nil) -> Yes: {
e -> b
}



-------------------------------------------
Step 3, which took 0.016105 s (model generation: 0.015945,  model checking: 0.000160):

Ground examples at the beginning of this step are:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
member(a, nil) <= member(a, cons(b, nil))
False <= member(b, cons(a, nil))
False <= member(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(b) <= True
}
;
append_bug ->
{
  append_bug(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append_bug(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append_bug(nil, cons(x_1_0, x_1_1), nil) <= True
  append_bug(nil, nil, nil) <= True
}
;
member ->
{
  member(a, cons(x_1_0, x_1_1)) <= True
  member(a, nil) <= True
  member(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_0)
}
;
reverse ->
{
  reverse(cons(x_0_0, x_0_1), nil) <= True
  reverse(nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> No: ()
member(h1, cons(h1, t1)) <= True -> No: ()
reverse(nil, nil) <= True -> No: ()
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> No: ()
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> No: ()
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> Yes: {
_iib -> nil  ;  e -> b  ;  l1 -> cons(b, l1_1)
}
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(b, t1_1)
}
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> No: ()
False <= member(e, nil) -> Yes: {
e -> a
}



-------------------------------------------
Step 4, which took 0.035340 s (model generation: 0.034849,  model checking: 0.000491):

Ground examples at the beginning of this step are:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(b, cons(a, cons(b, nil))) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
False <= member(a, cons(b, nil))
False <= member(a, nil)
False <= member(b, cons(a, nil))
False <= member(b, nil)
False <= reverse(cons(b, nil), nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= True
}
;
_r_2 ->
{
  _r_2(b) <= True
}
;
_r_3 ->
{
  _r_3(a) <= True
}
;
append_bug ->
{
  append_bug(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append_bug(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append_bug(nil, cons(x_1_0, x_1_1), nil) <= True
  append_bug(nil, nil, nil) <= True
}
;
member ->
{
  member(a, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
  member(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  member(b, cons(x_1_0, x_1_1)) <= _r_2(x_1_0)
}
;
reverse ->
{
  reverse(cons(x_0_0, x_0_1), nil) <= _r_3(x_0_0)
  reverse(nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> No: ()
member(h1, cons(h1, t1)) <= True -> No: ()
reverse(nil, nil) <= True -> No: ()
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> Yes: {
_dib -> nil  ;  _eib -> nil  ;  h1 -> b  ;  t1 -> nil
}
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> No: ()
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> Yes: {
_iib -> nil  ;  e -> b  ;  l1 -> cons(a, cons(v0_0, v0_1))
}
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> cons(a, t1_1)
}
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(a, nil)
}
False <= member(e, nil) -> No: ()



-------------------------------------------
Step 5, which took 0.055475 s (model generation: 0.054702,  model checking: 0.000773):

Ground examples at the beginning of this step are:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(a, cons(b, cons(a, nil))) <= True
member(b, cons(a, cons(b, nil))) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
False <= append_bug(nil, cons(b, nil), nil)
False <= member(a, cons(b, nil))
False <= member(a, nil)
False <= member(b, cons(a, cons(a, nil)))
False <= member(b, cons(a, nil))
False <= member(b, nil)
False <= reverse(cons(b, nil), nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= True
}
;
_r_3 ->
{
  _r_3(a) <= True
}
;
_r_4 ->
{
  _r_4(b) <= True
}
;
append_bug ->
{
  append_bug(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append_bug(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append_bug(nil, cons(x_1_0, x_1_1), nil) <= _r_3(x_1_0)
  append_bug(nil, nil, nil) <= True
}
;
member ->
{
  member(a, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  member(a, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
  member(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  member(b, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
}
;
reverse ->
{
  reverse(cons(x_0_0, x_0_1), nil) <= _r_3(x_0_0)
  reverse(nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


append_bug(nil, l2, nil) <= True -> Yes: {
l2 -> cons(b, l2_1)
}
member(h1, cons(h1, t1)) <= True -> No: ()
reverse(nil, nil) <= True -> No: ()
reverse(cons(h1, t1), _dib) <= append_bug(_eib, cons(h1, nil), _dib) /\ reverse(t1, _eib) -> No: ()
append_bug(cons(h1, t1), l2, cons(h1, _yhb)) <= append_bug(t1, l2, _yhb) -> No: ()
member(e, _iib) <= member(e, l1) /\ reverse(l1, _iib) -> Yes: {
_iib -> nil  ;  e -> a  ;  l1 -> cons(a, cons(v0_0, v0_1))
}
eq_elt(e, h1) \/ member(e, cons(h1, t1)) <= member(e, t1) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(a, cons(b, v0_1))
}
eq_elt(e, h1) \/ member(e, t1) <= member(e, cons(h1, t1)) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> cons(b, nil)
}
False <= member(e, nil) -> No: ()



Ground examples:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, cons(b, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(a, cons(b, cons(a, nil))) <= True
member(b, cons(a, cons(a, cons(b, nil)))) <= True
member(b, cons(a, cons(b, nil))) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
False <= append_bug(nil, cons(b, nil), nil)
False <= member(a, cons(a, cons(a, nil))) /\ reverse(cons(a, cons(a, nil)), nil)
False <= member(a, cons(b, cons(b, nil)))
False <= member(a, cons(b, nil))
False <= member(a, nil)
False <= member(b, cons(a, cons(a, nil)))
False <= member(b, cons(a, nil))
False <= member(b, nil)
False <= reverse(cons(b, nil), nil)
}

Learner output:
Contradictory ground constraints

Last ice step stopped before teacher could answer
Total time: 0.521170
Learner time: 0.124499
Teacher time: 0.001661
Reasons for stopping: No: Contradictory set of ground constraints:
{
append_bug(cons(a, nil), cons(a, nil), cons(a, nil)) <= True
append_bug(cons(a, nil), nil, cons(a, nil)) <= True
append_bug(nil, cons(a, nil), nil) <= True
append_bug(nil, cons(b, nil), nil) <= True
append_bug(nil, nil, nil) <= True
member(a, cons(a, nil)) <= True
member(a, cons(b, cons(a, nil))) <= True
member(b, cons(a, cons(a, cons(b, nil)))) <= True
member(b, cons(a, cons(b, nil))) <= True
member(b, cons(b, nil)) <= True
reverse(cons(a, nil), nil) <= True
reverse(nil, nil) <= True
False <= append_bug(nil, cons(b, nil), nil)
False <= member(a, cons(a, cons(a, nil))) /\ reverse(cons(a, cons(a, nil)), nil)
False <= member(a, cons(b, cons(b, nil)))
False <= member(a, cons(b, nil))
False <= member(a, nil)
False <= member(b, cons(a, cons(a, nil)))
False <= member(b, cons(a, nil))
False <= member(b, nil)
False <= reverse(cons(b, nil), nil)
}


