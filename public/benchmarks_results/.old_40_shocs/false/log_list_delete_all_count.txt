Solving ../../../../benchmarks/smtlib/false/list_delete_all_count.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(delete_one, F:
{
delete_one(x, nil, nil) <= True
delete_one(y, cons(y, r), r) <= True
eq_elt(x, y) \/ delete_one(x, cons(y, r), cons(y, _gab)) <= delete_one(x, r, _gab)
}
eq_eltlist(_jab, _kab) <= delete_one(_hab, _iab, _jab) /\ delete_one(_hab, _iab, _kab)
)
(delete_all, F:
{
delete_all(x, nil, nil) <= True
eq_elt(x, y) \/ delete_all(x, cons(y, r), cons(y, _mab)) <= delete_all(x, r, _mab)
delete_all(y, cons(y, r), _lab) <= delete_all(y, r, _lab)
}
eq_eltlist(_pab, _qab) <= delete_all(_nab, _oab, _pab) /\ delete_all(_nab, _oab, _qab)
)
(count, F:
{
count(x, nil, z) <= True
eq_elt(x, y) \/ count(x, cons(y, r), _sab) <= count(x, r, _sab)
count(y, cons(y, r), s(_rab)) <= count(y, r, _rab)
}
eq_nat(_vab, _wab) <= count(_tab, _uab, _vab) /\ count(_tab, _uab, _wab)
)
}

properties:
{
eq_nat(_xab, s(z)) <= count(x, l, _xab) /\ delete_all(x, l, _zab) /\ delete_one(x, l, _zab)
}


over-approximation: {count, delete_all, delete_one}
under-approximation: {}

Clause system for inference is:

{
count(x, nil, z) <= True
delete_all(x, nil, nil) <= True
delete_one(x, nil, nil) <= True
delete_one(y, cons(y, r), r) <= True
eq_nat(_xab, s(z)) <= count(x, l, _xab) /\ delete_all(x, l, _zab) /\ delete_one(x, l, _zab)
eq_elt(x, y) \/ count(x, cons(y, r), _sab) <= count(x, r, _sab)
count(y, cons(y, r), s(_rab)) <= count(y, r, _rab)
eq_elt(x, y) \/ delete_all(x, cons(y, r), cons(y, _mab)) <= delete_all(x, r, _mab)
delete_all(y, cons(y, r), _lab) <= delete_all(y, r, _lab)
eq_elt(x, y) \/ delete_one(x, cons(y, r), cons(y, _gab)) <= delete_one(x, r, _gab)
}


Solving took 0.031418 seconds.
No: Contradictory set of ground constraints:
{
False <= True
count(a, nil, z) <= True
count(b, cons(a, nil), z) <= True
count(b, cons(b, nil), s(z)) <= True
count(b, nil, z) <= True
delete_all(a, nil, nil) <= True
delete_all(b, cons(a, nil), cons(a, nil)) <= True
delete_all(b, cons(b, nil), nil) <= True
delete_all(b, nil, nil) <= True
delete_one(a, cons(a, nil), nil) <= True
delete_one(a, nil, nil) <= True
delete_one(b, cons(a, nil), cons(a, nil)) <= True
delete_one(b, cons(b, nil), nil) <= True
delete_one(b, nil, nil) <= True
}
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.007741 s (model generation: 0.007647,  model checking: 0.000094):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

count ->
{
  
}
;
delete_all ->
{
  
}
;
delete_one ->
{
  
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


count(x, nil, z) <= True -> Yes: {
x -> b
}
delete_all(x, nil, nil) <= True -> Yes: {
x -> b
}
delete_one(x, nil, nil) <= True -> Yes: {
x -> b
}
delete_one(y, cons(y, r), r) <= True -> Yes: {
r -> nil  ;  y -> b
}
eq_nat(_xab, s(z)) <= count(x, l, _xab) /\ delete_all(x, l, _zab) /\ delete_one(x, l, _zab) -> No: ()
eq_elt(x, y) \/ count(x, cons(y, r), _sab) <= count(x, r, _sab) -> No: ()
count(y, cons(y, r), s(_rab)) <= count(y, r, _rab) -> No: ()
eq_elt(x, y) \/ delete_all(x, cons(y, r), cons(y, _mab)) <= delete_all(x, r, _mab) -> No: ()
delete_all(y, cons(y, r), _lab) <= delete_all(y, r, _lab) -> No: ()
eq_elt(x, y) \/ delete_one(x, cons(y, r), cons(y, _gab)) <= delete_one(x, r, _gab) -> No: ()



-------------------------------------------
Step 1, which took 0.006875 s (model generation: 0.006662,  model checking: 0.000213):

Ground examples at the beginning of this step are:
{
count(b, nil, z) <= True
delete_all(b, nil, nil) <= True
delete_one(b, cons(b, nil), nil) <= True
delete_one(b, nil, nil) <= True
}

Learner proposed model:
Found Model:
|_

count ->
{
  count(b, nil, z) <= True
}
;
delete_all ->
{
  delete_all(b, nil, nil) <= True
}
;
delete_one ->
{
  delete_one(b, cons(x_1_0, x_1_1), nil) <= True
  delete_one(b, nil, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


count(x, nil, z) <= True -> Yes: {
x -> a
}
delete_all(x, nil, nil) <= True -> Yes: {
x -> a
}
delete_one(x, nil, nil) <= True -> Yes: {
x -> a
}
delete_one(y, cons(y, r), r) <= True -> Yes: {
r -> nil  ;  y -> a
}
eq_nat(_xab, s(z)) <= count(x, l, _xab) /\ delete_all(x, l, _zab) /\ delete_one(x, l, _zab) -> Yes: {
_xab -> z  ;  _zab -> nil  ;  l -> nil  ;  x -> b
}
eq_elt(x, y) \/ count(x, cons(y, r), _sab) <= count(x, r, _sab) -> Yes: {
_sab -> z  ;  r -> nil  ;  x -> b  ;  y -> a
}
count(y, cons(y, r), s(_rab)) <= count(y, r, _rab) -> Yes: {
_rab -> z  ;  r -> nil  ;  y -> b
}
eq_elt(x, y) \/ delete_all(x, cons(y, r), cons(y, _mab)) <= delete_all(x, r, _mab) -> Yes: {
_mab -> nil  ;  r -> nil  ;  x -> b  ;  y -> a
}
delete_all(y, cons(y, r), _lab) <= delete_all(y, r, _lab) -> Yes: {
_lab -> nil  ;  r -> nil  ;  y -> b
}
eq_elt(x, y) \/ delete_one(x, cons(y, r), cons(y, _gab)) <= delete_one(x, r, _gab) -> Yes: {
_gab -> nil  ;  r -> nil  ;  x -> b  ;  y -> a
}



Ground examples:
{
False <= True
count(a, nil, z) <= True
count(b, cons(a, nil), z) <= True
count(b, cons(b, nil), s(z)) <= True
count(b, nil, z) <= True
delete_all(a, nil, nil) <= True
delete_all(b, cons(a, nil), cons(a, nil)) <= True
delete_all(b, cons(b, nil), nil) <= True
delete_all(b, nil, nil) <= True
delete_one(a, cons(a, nil), nil) <= True
delete_one(a, nil, nil) <= True
delete_one(b, cons(a, nil), cons(a, nil)) <= True
delete_one(b, cons(b, nil), nil) <= True
delete_one(b, nil, nil) <= True
}

Learner output:
Contradictory ground constraints

Last ice step stopped before teacher could answer
Total time: 0.031418
Learner time: 0.014309
Teacher time: 0.000307
Reasons for stopping: No: Contradictory set of ground constraints:
{
False <= True
count(a, nil, z) <= True
count(b, cons(a, nil), z) <= True
count(b, cons(b, nil), s(z)) <= True
count(b, nil, z) <= True
delete_all(a, nil, nil) <= True
delete_all(b, cons(a, nil), cons(a, nil)) <= True
delete_all(b, cons(b, nil), nil) <= True
delete_all(b, nil, nil) <= True
delete_one(a, cons(a, nil), nil) <= True
delete_one(a, nil, nil) <= True
delete_one(b, cons(a, nil), cons(a, nil)) <= True
delete_one(b, cons(b, nil), nil) <= True
delete_one(b, nil, nil) <= True
}


