Solving ../../../../benchmarks/smtlib/true/cons_injectivity_2.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{

}

properties:
{}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{

}


Solving took 0.003010 seconds.
Proved
Model:
|_
{

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

-------------------
STEPS:
Total time: 0.003010
Reason for stopping: Proved

