Solving ../../../../benchmarks/smtlib/true/isaplanner_prop69.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(plus, F:
{() -> plus([n, z, n])
(plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)])}
(plus([_mr, _nr, _or]) /\ plus([_mr, _nr, _pr])) -> eq_nat([_or, _pr])
)
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
}

properties:
{(plus([n, m, _qr])) -> leq([n, _qr])}


over-approximation: {plus}
under-approximation: {leq}

Clause system for inference is:

{
() -> leq([z, n2]) -> 0  ;  () -> plus([n, z, n]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 0  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 0
}


Solving took 0.067545 seconds.
Proved
Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<s>(q_gen_4080) -> q_gen_4080
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078, q_gen_4087},
Q_f={q_gen_4075},
Delta=
{
<s>(q_gen_4087) -> q_gen_4087
<z>() -> q_gen_4087
<s, s>(q_gen_4078) -> q_gen_4078
<z, s>(q_gen_4087) -> q_gen_4078
<z, z>() -> q_gen_4078
<s, s, s>(q_gen_4075) -> q_gen_4075
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, s>(q_gen_4087) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.003739 s (model generation: 0.003609,  model checking: 0.000130):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 1  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 1
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> z
})

-------------------------------------------
Step 1, which took 0.003347 s (model generation: 0.003311,  model checking: 0.000036):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075},
Q_f={q_gen_4075},
Delta=
{
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 1  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 2, which took 0.005053 s (model generation: 0.004986,  model checking: 0.000067):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076},
Q_f={q_gen_4076},
Delta=
{
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075},
Q_f={q_gen_4075},
Delta=
{
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 1  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 4
}
Sat witness: Yes: ((plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]), {
_lr -> z  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 3, which took 0.003445 s (model generation: 0.003386,  model checking: 0.000059):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076},
Q_f={q_gen_4076},
Delta=
{
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078},
Q_f={q_gen_4075},
Delta=
{
<z, z>() -> q_gen_4078
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 4  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 4
}
Sat witness: Yes: ((plus([n, m, _qr])) -> leq([n, _qr]), {
_qr -> s(z)  ;  m -> s(z)  ;  n -> z
})

-------------------------------------------
Step 4, which took 0.003407 s (model generation: 0.003323,  model checking: 0.000084):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<z>() -> q_gen_4080
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078},
Q_f={q_gen_4075},
Delta=
{
<z, z>() -> q_gen_4078
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 4  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 4
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 5, which took 0.003652 s (model generation: 0.003411,  model checking: 0.000241):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078},
Q_f={q_gen_4075},
Delta=
{
<z, z>() -> q_gen_4078
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 4  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 4
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> s(z)
})

-------------------------------------------
Step 6, which took 0.006322 s (model generation: 0.004437,  model checking: 0.001885):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078},
Q_f={q_gen_4075},
Delta=
{
<z, z>() -> q_gen_4078
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 4  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(s(z))
})

-------------------------------------------
Step 7, which took 0.007442 s (model generation: 0.007343,  model checking: 0.000099):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<s>(q_gen_4080) -> q_gen_4080
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078},
Q_f={q_gen_4075},
Delta=
{
<z, z>() -> q_gen_4078
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 4  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 7
}
Sat witness: Yes: ((plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]), {
_lr -> s(z)  ;  mm -> z  ;  n -> s(z)
})

-------------------------------------------
Step 8, which took 0.006972 s (model generation: 0.006787,  model checking: 0.000185):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<s>(q_gen_4080) -> q_gen_4080
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078, q_gen_4087},
Q_f={q_gen_4075},
Delta=
{
<z>() -> q_gen_4087
<z, z>() -> q_gen_4078
<s, s, s>(q_gen_4075) -> q_gen_4075
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, s>(q_gen_4087) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  () -> plus([n, z, n]) -> 9  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 5  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 5  ;  (leq([s(nn1), z])) -> BOT -> 5  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 5  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 7
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> s(s(z))
})

-------------------------------------------
Step 9, which took 0.007412 s (model generation: 0.007142,  model checking: 0.000270):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<s>(q_gen_4080) -> q_gen_4080
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078, q_gen_4087},
Q_f={q_gen_4075},
Delta=
{
<z>() -> q_gen_4087
<s, s>(q_gen_4078) -> q_gen_4078
<z, z>() -> q_gen_4078
<s, s, s>(q_gen_4075) -> q_gen_4075
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, s>(q_gen_4087) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 7  ;  () -> plus([n, z, n]) -> 9  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 6  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 6  ;  (leq([s(nn1), z])) -> BOT -> 6  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 6  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 10
}
Sat witness: Yes: ((plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]), {
_lr -> s(z)  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 10, which took 0.007655 s (model generation: 0.007221,  model checking: 0.000434):

Model:
|_
{
leq -> 
{{{
Q={q_gen_4076, q_gen_4080},
Q_f={q_gen_4076},
Delta=
{
<s>(q_gen_4080) -> q_gen_4080
<z>() -> q_gen_4080
<s, s>(q_gen_4076) -> q_gen_4076
<z, s>(q_gen_4080) -> q_gen_4076
<z, z>() -> q_gen_4076
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_4075, q_gen_4078, q_gen_4087},
Q_f={q_gen_4075},
Delta=
{
<z>() -> q_gen_4087
<s, s>(q_gen_4078) -> q_gen_4078
<z, s>(q_gen_4087) -> q_gen_4078
<z, z>() -> q_gen_4078
<s, s, s>(q_gen_4075) -> q_gen_4075
<s, z, s>(q_gen_4078) -> q_gen_4075
<z, s, s>(q_gen_4078) -> q_gen_4075
<z, z, s>(q_gen_4087) -> q_gen_4075
<z, z, z>() -> q_gen_4075
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 8  ;  () -> plus([n, z, n]) -> 10  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 7  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 7  ;  (leq([s(nn1), z])) -> BOT -> 7  ;  (plus([n, m, _qr])) -> leq([n, _qr]) -> 7  ;  (plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]) -> 13
}
Sat witness: Yes: ((plus([n, mm, _lr])) -> plus([n, s(mm), s(_lr)]), {
_lr -> s(s(z))  ;  mm -> z  ;  n -> s(z)
})

Total time: 0.067545
Reason for stopping: Proved

