Solving ../../../../benchmarks/smtlib/true/length_cons_le_fun_elt.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(le, P:
{() -> le([z, s(nn2)])
(le([nn1, nn2])) -> le([s(nn1), s(nn2)])
(le([s(nn1), s(nn2)])) -> le([nn1, nn2])
(le([s(nn1), z])) -> BOT
(le([z, z])) -> BOT}
)
(length, F:
{() -> length([nil, z])
(length([ll, _hea])) -> length([cons(x, ll), s(_hea)])}
(length([_iea, _jea]) /\ length([_iea, _kea])) -> eq_nat([_jea, _kea])
)
(fcons, F:
{() -> fcons([x, l, cons(x, l)])}
(fcons([_lea, _mea, _nea]) /\ fcons([_lea, _mea, _oea])) -> eq_eltlist([_nea, _oea])
)
}

properties:
{(fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea])}


over-approximation: {fcons, length}
under-approximation: {le}

Clause system for inference is:

{
() -> fcons([x, l, cons(x, l)]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 0  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 0  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 0  ;  (le([s(nn1), z])) -> BOT -> 0  ;  (le([z, z])) -> BOT -> 0  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 0
}


Solving took 0.241423 seconds.
Proved
Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<cons>(q_gen_4733, q_gen_4732) -> q_gen_4732
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4744
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<cons, a>(q_gen_4733, q_gen_4732) -> q_gen_4745
<cons, b>(q_gen_4733, q_gen_4732) -> q_gen_4745
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<a, b>() -> q_gen_4747
<b, a>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<s>(q_gen_4750) -> q_gen_4750
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.008842 s (model generation: 0.008504,  model checking: 0.000338):

Model:
|_
{
fcons -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 3  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 1
}
Sat witness: Yes: (() -> length([nil, z]), {

})

-------------------------------------------
Step 1, which took 0.007921 s (model generation: 0.007785,  model checking: 0.000136):

Model:
|_
{
fcons -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728},
Q_f={q_gen_4728},
Delta=
{
<nil, z>() -> q_gen_4728
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 0  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 1
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 2, which took 0.010330 s (model generation: 0.009911,  model checking: 0.000419):

Model:
|_
{
fcons -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<z>() -> q_gen_4730
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728},
Q_f={q_gen_4728},
Delta=
{
<nil, z>() -> q_gen_4728
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 1
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> nil  ;  x -> b
})

-------------------------------------------
Step 3, which took 0.009525 s (model generation: 0.009412,  model checking: 0.000113):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<b>() -> q_gen_4733
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<z>() -> q_gen_4730
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728},
Q_f={q_gen_4728},
Delta=
{
<nil, z>() -> q_gen_4728
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 4
}
Sat witness: Yes: ((length([ll, _hea])) -> length([cons(x, ll), s(_hea)]), {
_hea -> z  ;  ll -> nil  ;  x -> b
})

-------------------------------------------
Step 4, which took 0.009345 s (model generation: 0.009077,  model checking: 0.000268):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<b>() -> q_gen_4733
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<z>() -> q_gen_4730
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 4
}
Sat witness: Yes: ((le([nn1, nn2])) -> le([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> s(z)
})

-------------------------------------------
Step 5, which took 0.009598 s (model generation: 0.009145,  model checking: 0.000453):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<b>() -> q_gen_4733
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 3  ;  () -> le([z, s(nn2)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 2  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 3  ;  (le([s(nn1), z])) -> BOT -> 3  ;  (le([z, z])) -> BOT -> 3  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 4
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> s(z)
})

-------------------------------------------
Step 6, which took 0.004311 s (model generation: 0.003369,  model checking: 0.000942):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<b>() -> q_gen_4733
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 6  ;  () -> le([z, s(nn2)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 4  ;  (le([s(nn1), z])) -> BOT -> 4  ;  (le([z, z])) -> BOT -> 4  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 4
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> nil  ;  x -> a
})

-------------------------------------------
Step 7, which took 0.010807 s (model generation: 0.010415,  model checking: 0.000392):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 6  ;  () -> le([z, s(nn2)]) -> 6  ;  () -> length([nil, z]) -> 4  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 4  ;  (le([s(nn1), z])) -> BOT -> 4  ;  (le([z, z])) -> BOT -> 4  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 7
}
Sat witness: Yes: ((length([ll, _hea])) -> length([cons(x, ll), s(_hea)]), {
_hea -> z  ;  ll -> nil  ;  x -> a
})

-------------------------------------------
Step 8, which took 0.012373 s (model generation: 0.010533,  model checking: 0.001840):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, z>() -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 9  ;  () -> le([z, s(nn2)]) -> 7  ;  () -> length([nil, z]) -> 5  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 5  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 5  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 5  ;  (le([s(nn1), z])) -> BOT -> 5  ;  (le([z, z])) -> BOT -> 5  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 7
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(b, nil)  ;  x -> b
})

-------------------------------------------
Step 9, which took 0.012184 s (model generation: 0.011050,  model checking: 0.001134):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, b>() -> q_gen_4745
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, b>() -> q_gen_4747
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735},
Q_f={q_gen_4728},
Delta=
{
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, z>() -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 9  ;  () -> le([z, s(nn2)]) -> 7  ;  () -> length([nil, z]) -> 6  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 6  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 6  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 6  ;  (le([s(nn1), z])) -> BOT -> 6  ;  (le([z, z])) -> BOT -> 6  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 10
}
Sat witness: Yes: ((length([ll, _hea])) -> length([cons(x, ll), s(_hea)]), {
_hea -> s(z)  ;  ll -> cons(a, nil)  ;  x -> b
})

-------------------------------------------
Step 10, which took 0.012881 s (model generation: 0.010950,  model checking: 0.001931):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, b>() -> q_gen_4745
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, b>() -> q_gen_4747
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 12  ;  () -> le([z, s(nn2)]) -> 8  ;  () -> length([nil, z]) -> 7  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 7  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 7  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 7  ;  (le([s(nn1), z])) -> BOT -> 7  ;  (le([z, z])) -> BOT -> 7  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 10
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(a, nil)  ;  x -> a
})

-------------------------------------------
Step 11, which took 0.008780 s (model generation: 0.008328,  model checking: 0.000452):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 12  ;  () -> le([z, s(nn2)]) -> 9  ;  () -> length([nil, z]) -> 8  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 8  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 8  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 8  ;  (le([s(nn1), z])) -> BOT -> 8  ;  (le([z, z])) -> BOT -> 8  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 13
}
Sat witness: Yes: ((length([ll, _hea])) -> length([cons(x, ll), s(_hea)]), {
_hea -> s(z)  ;  ll -> cons(b, nil)  ;  x -> a
})

-------------------------------------------
Step 12, which took 0.013952 s (model generation: 0.006942,  model checking: 0.007010):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 15  ;  () -> le([z, s(nn2)]) -> 10  ;  () -> length([nil, z]) -> 9  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 9  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 9  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 9  ;  (le([s(nn1), z])) -> BOT -> 9  ;  (le([z, z])) -> BOT -> 9  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 13
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(a, nil)  ;  x -> b
})

-------------------------------------------
Step 13, which took 0.016905 s (model generation: 0.014881,  model checking: 0.002024):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<a, b>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 15  ;  () -> le([z, s(nn2)]) -> 11  ;  () -> length([nil, z]) -> 10  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 10  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 10  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 10  ;  (le([s(nn1), z])) -> BOT -> 10  ;  (le([z, z])) -> BOT -> 10  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 16
}
Sat witness: Yes: ((length([ll, _hea])) -> length([cons(x, ll), s(_hea)]), {
_hea -> s(s(z))  ;  ll -> cons(b, cons(b, nil))  ;  x -> b
})

-------------------------------------------
Step 14, which took 0.030219 s (model generation: 0.016687,  model checking: 0.013532):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<a, b>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<s>(q_gen_4750) -> q_gen_4750
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 18  ;  () -> le([z, s(nn2)]) -> 12  ;  () -> length([nil, z]) -> 11  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 11  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 11  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 11  ;  (le([s(nn1), z])) -> BOT -> 11  ;  (le([z, z])) -> BOT -> 11  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 16
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(b, nil)  ;  x -> a
})

-------------------------------------------
Step 15, which took 0.028067 s (model generation: 0.015332,  model checking: 0.012735):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<a, b>() -> q_gen_4747
<b, a>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<s>(q_gen_4750) -> q_gen_4750
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 21  ;  () -> le([z, s(nn2)]) -> 13  ;  () -> length([nil, z]) -> 12  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 12  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 12  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 12  ;  (le([s(nn1), z])) -> BOT -> 12  ;  (le([z, z])) -> BOT -> 12  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 17
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(b, cons(b, nil))  ;  x -> a
})

-------------------------------------------
Step 16, which took 0.015176 s (model generation: 0.010111,  model checking: 0.005065):

Model:
|_
{
fcons -> 
{{{
Q={q_gen_4731, q_gen_4732, q_gen_4733, q_gen_4744, q_gen_4745, q_gen_4746, q_gen_4747},
Q_f={q_gen_4731},
Delta=
{
<cons>(q_gen_4733, q_gen_4732) -> q_gen_4732
<nil>() -> q_gen_4732
<a>() -> q_gen_4733
<b>() -> q_gen_4733
<cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4744
<nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4744
<cons, a>(q_gen_4733, q_gen_4732) -> q_gen_4745
<nil, a>() -> q_gen_4745
<nil, b>() -> q_gen_4745
<a, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<b, cons>(q_gen_4733, q_gen_4732) -> q_gen_4746
<a, a>() -> q_gen_4747
<a, b>() -> q_gen_4747
<b, a>() -> q_gen_4747
<b, b>() -> q_gen_4747
<a, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<a, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
<b, cons, cons>(q_gen_4747, q_gen_4746, q_gen_4745, q_gen_4744) -> q_gen_4731
<b, nil, cons>(q_gen_4733, q_gen_4732) -> q_gen_4731
}

Datatype: <elt, eltlist, eltlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_4729, q_gen_4730},
Q_f={q_gen_4729},
Delta=
{
<s>(q_gen_4730) -> q_gen_4730
<z>() -> q_gen_4730
<s, s>(q_gen_4729) -> q_gen_4729
<z, s>(q_gen_4730) -> q_gen_4729
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_4728, q_gen_4735, q_gen_4750},
Q_f={q_gen_4728},
Delta=
{
<s>(q_gen_4750) -> q_gen_4750
<z>() -> q_gen_4750
<cons, s>(q_gen_4735, q_gen_4728) -> q_gen_4728
<nil, z>() -> q_gen_4728
<a, s>(q_gen_4750) -> q_gen_4735
<a, z>() -> q_gen_4735
<b, s>(q_gen_4750) -> q_gen_4735
<b, z>() -> q_gen_4735
}

Datatype: <eltlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> fcons([x, l, cons(x, l)]) -> 24  ;  () -> le([z, s(nn2)]) -> 14  ;  () -> length([nil, z]) -> 13  ;  (fcons([x, l, _qea]) /\ length([_qea, _rea]) /\ length([l, _pea])) -> le([_pea, _rea]) -> 13  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 13  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 13  ;  (le([s(nn1), z])) -> BOT -> 13  ;  (le([z, z])) -> BOT -> 13  ;  (length([ll, _hea])) -> length([cons(x, ll), s(_hea)]) -> 18
}
Sat witness: Yes: (() -> fcons([x, l, cons(x, l)]), {
l -> cons(b, cons(b, nil))  ;  x -> b
})

Total time: 0.241423
Reason for stopping: Proved

