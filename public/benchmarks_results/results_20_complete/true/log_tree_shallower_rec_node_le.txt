Solving ../../../../benchmarks/smtlib/true/tree_shallower_rec_node_le.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
elt -> {a}  ;  etree -> {leaf, node}  ;  nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(le, P:
{() -> le([z, s(nn2)])
(le([nn1, nn2])) -> le([s(nn1), s(nn2)])
(le([s(nn1), s(nn2)])) -> le([nn1, nn2])
(le([s(nn1), z])) -> BOT
(le([z, z])) -> BOT}
)
(shallower, P:
{() -> shallower([leaf, n])
(shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)])
(shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m])
(not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT
(shallower([node(e, t1, t2), z])) -> BOT}
)
}

properties:
{(shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]), (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n])}


over-approximation: {le, leq}
under-approximation: {le, leq}

Clause system for inference is:

{
() -> le([z, s(nn2)]) -> 0  ;  () -> leq([z, n2]) -> 0  ;  () -> shallower([leaf, n]) -> 0  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 0  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 0  ;  (le([s(nn1), z])) -> BOT -> 0  ;  (le([z, z])) -> BOT -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 0  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 0  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 0  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 0  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 0  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 0
}


Solving took 20.000333 seconds.
DontKnow. Stopped because: timeout
Working model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831, q_gen_9835, q_gen_9840, q_gen_9841},
Q_f={},
Delta=
{
<z>() -> q_gen_9831
<s>(q_gen_9831) -> q_gen_9841
<z, s>(q_gen_9831) -> q_gen_9830
<s, s>(q_gen_9830) -> q_gen_9835
<z, s>(q_gen_9841) -> q_gen_9840
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829, q_gen_9834, q_gen_9838, q_gen_9839},
Q_f={},
Delta=
{
<z>() -> q_gen_9839
<z, z>() -> q_gen_9829
<s, s>(q_gen_9829) -> q_gen_9834
<z, s>(q_gen_9839) -> q_gen_9838
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9832, q_gen_9833, q_gen_9836, q_gen_9837, q_gen_9842, q_gen_9843},
Q_f={},
Delta=
{
<z>() -> q_gen_9837
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9832
<a, z>() -> q_gen_9833
<leaf, s>(q_gen_9837) -> q_gen_9836
<node, s>(q_gen_9843, q_gen_9836, q_gen_9836) -> q_gen_9842
<a, s>(q_gen_9837) -> q_gen_9843
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004423 s (model generation: 0.003763,  model checking: 0.000660):

Model:
|_
{
le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 0  ;  () -> leq([z, n2]) -> 0  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 1  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 1  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 1  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> shallower([leaf, n]), {
n -> z
})

-------------------------------------------
Step 1, which took 0.003302 s (model generation: 0.003261,  model checking: 0.000041):

Model:
|_
{
le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 0  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 1  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 1  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 1  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 2, which took 0.003231 s (model generation: 0.003189,  model checking: 0.000042):

Model:
|_
{
le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 1  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 1  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 1  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 1  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 1
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 3, which took 0.003210 s (model generation: 0.002955,  model checking: 0.000255):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 2  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 2  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 2
}
Sat witness: Yes: ((shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]), {
e -> a  ;  m -> z  ;  t1 -> leaf  ;  t2 -> leaf
})

-------------------------------------------
Step 4, which took 0.004352 s (model generation: 0.003350,  model checking: 0.001002):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 2  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 2  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 2
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 5, which took 0.003844 s (model generation: 0.003741,  model checking: 0.000103):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<s, s>(q_gen_9829) -> q_gen_9829
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 2  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 2  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 2  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 2
}
Sat witness: Yes: ((le([nn1, nn2])) -> le([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> s(z)
})

-------------------------------------------
Step 6, which took 0.008203 s (model generation: 0.004280,  model checking: 0.003923):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<s, s>(q_gen_9830) -> q_gen_9830
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<s, s>(q_gen_9829) -> q_gen_9829
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833},
Q_f={q_gen_9828},
Delta=
{
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 3  ;  () -> shallower([leaf, n]) -> 6  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 3  ;  (le([s(nn1), z])) -> BOT -> 3  ;  (le([z, z])) -> BOT -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 3  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 3  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 3  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 3  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 3
}
Sat witness: Yes: (() -> shallower([leaf, n]), {
n -> s(z)
})

-------------------------------------------
Step 7, which took 0.015331 s (model generation: 0.005445,  model checking: 0.009886):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<s, s>(q_gen_9830) -> q_gen_9830
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829},
Q_f={q_gen_9829},
Delta=
{
<s, s>(q_gen_9829) -> q_gen_9829
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833, q_gen_9837},
Q_f={q_gen_9828},
Delta=
{
<z>() -> q_gen_9837
<leaf, s>(q_gen_9837) -> q_gen_9828
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> leq([z, n2]) -> 6  ;  () -> shallower([leaf, n]) -> 6  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 4  ;  (le([s(nn1), z])) -> BOT -> 4  ;  (le([z, z])) -> BOT -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 4  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 4  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 4  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 4  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(z)
})

-------------------------------------------
Step 8, which took 0.003528 s (model generation: 0.003484,  model checking: 0.000044):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<z>() -> q_gen_9831
<s, s>(q_gen_9830) -> q_gen_9830
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829, q_gen_9839},
Q_f={q_gen_9829},
Delta=
{
<z>() -> q_gen_9839
<s, s>(q_gen_9829) -> q_gen_9829
<z, s>(q_gen_9839) -> q_gen_9829
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833, q_gen_9837},
Q_f={q_gen_9828},
Delta=
{
<z>() -> q_gen_9837
<leaf, s>(q_gen_9837) -> q_gen_9828
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 6  ;  () -> leq([z, n2]) -> 6  ;  () -> shallower([leaf, n]) -> 6  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 4  ;  (le([s(nn1), z])) -> BOT -> 4  ;  (le([z, z])) -> BOT -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 4  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 4  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 4  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 4  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 4  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 4
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> s(z)
})

-------------------------------------------
Step 9, which took 0.013094 s (model generation: 0.004120,  model checking: 0.008974):

Model:
|_
{
le -> 
{{{
Q={q_gen_9830, q_gen_9831},
Q_f={q_gen_9830},
Delta=
{
<s>(q_gen_9831) -> q_gen_9831
<z>() -> q_gen_9831
<s, s>(q_gen_9830) -> q_gen_9830
<z, s>(q_gen_9831) -> q_gen_9830
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  leq -> 
{{{
Q={q_gen_9829, q_gen_9839},
Q_f={q_gen_9829},
Delta=
{
<z>() -> q_gen_9839
<s, s>(q_gen_9829) -> q_gen_9829
<z, s>(q_gen_9839) -> q_gen_9829
<z, z>() -> q_gen_9829
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  shallower -> 
{{{
Q={q_gen_9828, q_gen_9833, q_gen_9837},
Q_f={q_gen_9828},
Delta=
{
<z>() -> q_gen_9837
<leaf, s>(q_gen_9837) -> q_gen_9828
<leaf, z>() -> q_gen_9828
<node, s>(q_gen_9833, q_gen_9828, q_gen_9828) -> q_gen_9828
<a, z>() -> q_gen_9833
}

Datatype: <etree, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_elt, eq_etree, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 6  ;  () -> leq([z, n2]) -> 6  ;  () -> shallower([leaf, n]) -> 6  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 4  ;  (le([s(nn1), z])) -> BOT -> 4  ;  (le([z, z])) -> BOT -> 4  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]) -> 7  ;  (shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> shallower([t2, m]) -> 5  ;  (not shallower([t1, m]) /\ shallower([node(e, t1, t2), s(m)])) -> BOT -> 5  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t1, n]) -> 5  ;  (shallower([node(e, t1, t2), s(n)])) -> shallower([t2, n]) -> 5  ;  (shallower([node(e, t1, t2), z])) -> BOT -> 5
}
Sat witness: Yes: ((shallower([t1, m]) /\ shallower([t2, m])) -> shallower([node(e, t1, t2), s(m)]), {
e -> a  ;  m -> s(z)  ;  t1 -> leaf  ;  t2 -> leaf
})

Total time: 20.000333
Reason for stopping: DontKnow. Stopped because: timeout

