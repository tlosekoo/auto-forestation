Solving ../../../../benchmarks/smtlib/true/tree_strict_subtree_trans.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}  ;  nattree -> {leaf, node}
}
definition:
{
(strict_subtree, P:
{() -> strict_subtree([leaf, node(eb, tb1, tb2)])
(strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])
(strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> strict_subtree([ta2, tb2])
(not strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT
(strict_subtree([leaf, leaf])) -> BOT
(strict_subtree([node(ea, ta1, ta2), leaf])) -> BOT
(strict_subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_nat([ea, eb])) -> BOT}
)
}

properties:
{(strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3])}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{
() -> strict_subtree([leaf, node(eb, tb1, tb2)]) -> 0  ;  (strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3]) -> 0  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 0  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> strict_subtree([ta2, tb2]) -> 0  ;  (not strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 0  ;  (strict_subtree([leaf, leaf])) -> BOT -> 0  ;  (strict_subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 0  ;  (strict_subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_nat([ea, eb])) -> BOT -> 0
}


Solving took 20.012949 seconds.
DontKnow. Stopped because: timeout
Working model:
|_
{
strict_subtree -> 
{{{
Q={q_gen_9886, q_gen_9887, q_gen_9888, q_gen_9889, q_gen_9890, q_gen_9891, q_gen_9892, q_gen_9893, q_gen_9894},
Q_f={},
Delta=
{
<leaf>() -> q_gen_9887
<z>() -> q_gen_9888
<node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9894
<leaf, node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9886
<node, node>(q_gen_9892, q_gen_9891, q_gen_9891, q_gen_9890, q_gen_9886, q_gen_9886, q_gen_9890, q_gen_9886, q_gen_9886) -> q_gen_9889
<leaf, z>() -> q_gen_9890
<z, node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9891
<z, z>() -> q_gen_9892
<leaf, node>(q_gen_9888, q_gen_9894, q_gen_9894) -> q_gen_9893
}

Datatype: <nattree, nattree>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_nattree}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004441 s (model generation: 0.003715,  model checking: 0.000726):

Model:
|_
{
strict_subtree -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nattree, nattree>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_nattree}
_|

Teacher's answer:
New clause system:
{
() -> strict_subtree([leaf, node(eb, tb1, tb2)]) -> 3  ;  (strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3]) -> 1  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 1  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> strict_subtree([ta2, tb2]) -> 1  ;  (not strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 1  ;  (strict_subtree([leaf, leaf])) -> BOT -> 1  ;  (strict_subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 1  ;  (strict_subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_nat([ea, eb])) -> BOT -> 1
}
Sat witness: Yes: (() -> strict_subtree([leaf, node(eb, tb1, tb2)]), {
eb -> z  ;  tb1 -> leaf  ;  tb2 -> leaf
})

-------------------------------------------
Step 1, which took 0.097181 s (model generation: 0.003456,  model checking: 0.093725):

Model:
|_
{
strict_subtree -> 
{{{
Q={q_gen_9886, q_gen_9887, q_gen_9888},
Q_f={q_gen_9886},
Delta=
{
<leaf>() -> q_gen_9887
<z>() -> q_gen_9888
<leaf, node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9886
}

Datatype: <nattree, nattree>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_nattree}
_|

Teacher's answer:
New clause system:
{
() -> strict_subtree([leaf, node(eb, tb1, tb2)]) -> 3  ;  (strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3]) -> 1  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> strict_subtree([ta2, tb2]) -> 2  ;  (not strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (strict_subtree([leaf, leaf])) -> BOT -> 2  ;  (strict_subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (strict_subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_nat([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]), {
eb -> z  ;  ta1 -> leaf  ;  ta2 -> leaf  ;  tb1 -> node(z, leaf, leaf)  ;  tb2 -> node(z, leaf, leaf)
})

-------------------------------------------
Step 2, which took 0.065851 s (model generation: 0.004183,  model checking: 0.061668):

Model:
|_
{
strict_subtree -> 
{{{
Q={q_gen_9886, q_gen_9887, q_gen_9888, q_gen_9890, q_gen_9891, q_gen_9892},
Q_f={q_gen_9886},
Delta=
{
<leaf>() -> q_gen_9887
<z>() -> q_gen_9888
<leaf, node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9886
<node, node>(q_gen_9892, q_gen_9891, q_gen_9891, q_gen_9890, q_gen_9886, q_gen_9886, q_gen_9890, q_gen_9886, q_gen_9886) -> q_gen_9886
<leaf, z>() -> q_gen_9890
<z, node>(q_gen_9888, q_gen_9887, q_gen_9887) -> q_gen_9891
<z, z>() -> q_gen_9892
}

Datatype: <nattree, nattree>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_nattree}
_|

Teacher's answer:
New clause system:
{
() -> strict_subtree([leaf, node(eb, tb1, tb2)]) -> 3  ;  (strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3]) -> 4  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([ta2, tb2])) -> strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)]) -> 4  ;  (strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> strict_subtree([ta2, tb2]) -> 2  ;  (not strict_subtree([ta1, tb1]) /\ strict_subtree([node(eb, ta1, ta2), node(eb, tb1, tb2)])) -> BOT -> 2  ;  (strict_subtree([leaf, leaf])) -> BOT -> 2  ;  (strict_subtree([node(ea, ta1, ta2), leaf])) -> BOT -> 2  ;  (strict_subtree([node(ea, ta1, ta2), node(eb, tb1, tb2)]) /\ not eq_nat([ea, eb])) -> BOT -> 2
}
Sat witness: Yes: ((strict_subtree([t1, t2]) /\ strict_subtree([t2, t3])) -> strict_subtree([t1, t3]), {
t1 -> leaf  ;  t2 -> node(z, leaf, leaf)  ;  t3 -> node(z, node(z, leaf, leaf), node(z, leaf, leaf))
})

Total time: 20.012949
Reason for stopping: DontKnow. Stopped because: timeout

