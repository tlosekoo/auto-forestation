Solving ../../../../benchmarks/smtlib/false/plus_le.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(plus, F:
{() -> plus([n, z, n])
(plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)])}
(plus([_cjb, _djb, _ejb]) /\ plus([_cjb, _djb, _fjb])) -> eq_nat([_ejb, _fjb])
)
(le, P:
{() -> le([z, s(nn2)])
(le([nn1, nn2])) -> le([s(nn1), s(nn2)])
(le([s(nn1), s(nn2)])) -> le([nn1, nn2])
(le([s(nn1), z])) -> BOT
(le([z, z])) -> BOT}
)
}

properties:
{(plus([n, m, _gjb])) -> le([n, _gjb])}


over-approximation: {plus}
under-approximation: {le}

Clause system for inference is:

{
() -> le([z, s(nn2)]) -> 0  ;  () -> plus([n, z, n]) -> 0  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 0  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 0  ;  (le([s(nn1), z])) -> BOT -> 0  ;  (le([z, z])) -> BOT -> 0  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 0  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 0
}


Solving took 0.063843 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.010161 s (model generation: 0.009631,  model checking: 0.000530):

Model:
|_
{
le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 0  ;  () -> plus([n, z, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 1  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 1
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> z
})

-------------------------------------------
Step 1, which took 0.009631 s (model generation: 0.009499,  model checking: 0.000132):

Model:
|_
{
le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_10047},
Q_f={q_gen_10047},
Delta=
{
<z, z, z>() -> q_gen_10047
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 1  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 1
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 2, which took 0.009775 s (model generation: 0.009538,  model checking: 0.000237):

Model:
|_
{
le -> 
{{{
Q={q_gen_10048, q_gen_10049},
Q_f={q_gen_10048},
Delta=
{
<z>() -> q_gen_10049
<z, s>(q_gen_10049) -> q_gen_10048
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_10047},
Q_f={q_gen_10047},
Delta=
{
<z, z, z>() -> q_gen_10047
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 1  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 4
}
Sat witness: Yes: ((plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]), {
_bjb -> z  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 3, which took 0.010177 s (model generation: 0.010016,  model checking: 0.000161):

Model:
|_
{
le -> 
{{{
Q={q_gen_10048, q_gen_10049},
Q_f={q_gen_10048},
Delta=
{
<z>() -> q_gen_10049
<z, s>(q_gen_10049) -> q_gen_10048
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_10047, q_gen_10051},
Q_f={q_gen_10047},
Delta=
{
<z, z>() -> q_gen_10051
<z, s, s>(q_gen_10051) -> q_gen_10047
<z, z, z>() -> q_gen_10047
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 4  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 4
}
Sat witness: Yes: ((plus([n, m, _gjb])) -> le([n, _gjb]), {
_gjb -> z  ;  m -> z  ;  n -> z
})

-------------------------------------------
Step 4, which took 0.010372 s (model generation: 0.010340,  model checking: 0.000032):

Model:
|_
{
le -> 
{{{
Q={q_gen_10048, q_gen_10049},
Q_f={q_gen_10048},
Delta=
{
<z>() -> q_gen_10049
<z, s>(q_gen_10049) -> q_gen_10048
<z, z>() -> q_gen_10048
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  plus -> 
{{{
Q={q_gen_10047, q_gen_10051},
Q_f={q_gen_10047},
Delta=
{
<z, z>() -> q_gen_10051
<z, s, s>(q_gen_10051) -> q_gen_10047
<z, z, z>() -> q_gen_10047
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> le([z, s(nn2)]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 4  ;  (plus([n, m, _gjb])) -> le([n, _gjb]) -> 4  ;  (plus([n, mm, _bjb])) -> plus([n, s(mm), s(_bjb)]) -> 4
}
Sat witness: Yes: ((le([z, z])) -> BOT, {

})

Total time: 0.063843
Reason for stopping: Disproved

