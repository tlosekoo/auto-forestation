Solving ../../../../benchmarks/smtlib/false/max_nat.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, n2])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(max, F:
{(leq([n, m])) -> max([n, m, m])
(not leq([n, m])) -> max([n, m, n])}
(max([_edb, _fdb, _gdb]) /\ max([_edb, _fdb, _hdb])) -> eq_nat([_gdb, _hdb])
)
}

properties:
{(max([i, j, _idb])) -> eq_nat([i, _idb])}


over-approximation: {leq, max}
under-approximation: {leq}

Clause system for inference is:

{
() -> leq([z, n2]) -> 0  ;  (leq([n, m])) -> max([n, m, m]) -> 0  ;  (not leq([n, m])) -> max([n, m, n]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 0
}


Solving took 0.123932 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.011058 s (model generation: 0.009600,  model checking: 0.001458):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 0  ;  (leq([n, m])) -> max([n, m, m]) -> 0  ;  (not leq([n, m])) -> max([n, m, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 1
}
Sat witness: Yes: ((not leq([n, m])) -> max([n, m, n]), {
m -> z  ;  n -> z
})

-------------------------------------------
Step 1, which took 0.009893 s (model generation: 0.009624,  model checking: 0.000269):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983},
Q_f={},
Delta=
{
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984},
Q_f={q_gen_9984},
Delta=
{
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  (leq([n, m])) -> max([n, m, m]) -> 1  ;  (not leq([n, m])) -> max([n, m, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 1
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> z
})

-------------------------------------------
Step 2, which took 0.010422 s (model generation: 0.010057,  model checking: 0.000365):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983},
Q_f={q_gen_9983},
Delta=
{
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  (leq([n, m])) -> max([n, m, m]) -> 1  ;  (not leq([n, m])) -> max([n, m, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 2
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 3, which took 0.010604 s (model generation: 0.010412,  model checking: 0.000192):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983},
Q_f={q_gen_9983},
Delta=
{
<s, s>(q_gen_9983) -> q_gen_9983
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984},
Q_f={},
Delta=
{
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  (leq([n, m])) -> max([n, m, m]) -> 4  ;  (not leq([n, m])) -> max([n, m, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 2
}
Sat witness: Yes: ((leq([n, m])) -> max([n, m, m]), {
m -> z  ;  n -> z
})

-------------------------------------------
Step 4, which took 0.012363 s (model generation: 0.009992,  model checking: 0.002371):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983},
Q_f={q_gen_9983},
Delta=
{
<s, s>(q_gen_9983) -> q_gen_9983
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984},
Q_f={q_gen_9984},
Delta=
{
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 3  ;  (leq([n, m])) -> max([n, m, m]) -> 4  ;  (not leq([n, m])) -> max([n, m, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 3
}
Sat witness: Yes: ((not leq([n, m])) -> max([n, m, n]), {
m -> s(z)  ;  n -> z
})

-------------------------------------------
Step 5, which took 0.008767 s (model generation: 0.008346,  model checking: 0.000421):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983, q_gen_9987},
Q_f={q_gen_9983},
Delta=
{
<z>() -> q_gen_9987
<s, s>(q_gen_9983) -> q_gen_9983
<z, s>(q_gen_9987) -> q_gen_9983
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984, q_gen_9989},
Q_f={q_gen_9984},
Delta=
{
<z>() -> q_gen_9989
<z, s, z>(q_gen_9989) -> q_gen_9984
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  (leq([n, m])) -> max([n, m, m]) -> 4  ;  (not leq([n, m])) -> max([n, m, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 4
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(s(z))
})

-------------------------------------------
Step 6, which took 0.011583 s (model generation: 0.011008,  model checking: 0.000575):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983, q_gen_9987},
Q_f={q_gen_9983},
Delta=
{
<s>(q_gen_9987) -> q_gen_9987
<z>() -> q_gen_9987
<s, s>(q_gen_9983) -> q_gen_9983
<z, s>(q_gen_9987) -> q_gen_9983
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984, q_gen_9989},
Q_f={q_gen_9984},
Delta=
{
<z>() -> q_gen_9989
<z, s, z>(q_gen_9989) -> q_gen_9984
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  (leq([n, m])) -> max([n, m, m]) -> 7  ;  (not leq([n, m])) -> max([n, m, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 5  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 5  ;  (leq([s(nn1), z])) -> BOT -> 5  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 5
}
Sat witness: Yes: ((leq([n, m])) -> max([n, m, m]), {
m -> s(z)  ;  n -> z
})

-------------------------------------------
Step 7, which took 0.012196 s (model generation: 0.010379,  model checking: 0.001817):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983, q_gen_9987},
Q_f={q_gen_9983},
Delta=
{
<s>(q_gen_9987) -> q_gen_9987
<z>() -> q_gen_9987
<s, s>(q_gen_9983) -> q_gen_9983
<z, s>(q_gen_9987) -> q_gen_9983
<z, z>() -> q_gen_9983
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984, q_gen_9989, q_gen_9993},
Q_f={q_gen_9984},
Delta=
{
<z>() -> q_gen_9989
<z, z>() -> q_gen_9993
<z, s, s>(q_gen_9993) -> q_gen_9984
<z, s, z>(q_gen_9989) -> q_gen_9984
<z, z, z>() -> q_gen_9984
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  (leq([n, m])) -> max([n, m, m]) -> 7  ;  (not leq([n, m])) -> max([n, m, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 5  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 5  ;  (leq([s(nn1), z])) -> BOT -> 5  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 8
}
Sat witness: Yes: ((max([i, j, _idb])) -> eq_nat([i, _idb]), {
_idb -> s(z)  ;  i -> z  ;  j -> s(z)
})

-------------------------------------------
Step 8, which took 0.013173 s (model generation: 0.010924,  model checking: 0.002249):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983, q_gen_9986, q_gen_9987, q_gen_9991},
Q_f={q_gen_9983},
Delta=
{
<z>() -> q_gen_9987
<s>(q_gen_9987) -> q_gen_9991
<s, s>(q_gen_9983) -> q_gen_9983
<z, s>(q_gen_9991) -> q_gen_9983
<z, z>() -> q_gen_9983
<z, s>(q_gen_9987) -> q_gen_9986
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984, q_gen_9989, q_gen_9992, q_gen_9993},
Q_f={q_gen_9984},
Delta=
{
<z>() -> q_gen_9989
<z, z>() -> q_gen_9993
<z, s, z>(q_gen_9989) -> q_gen_9984
<z, z, z>() -> q_gen_9984
<z, s, s>(q_gen_9993) -> q_gen_9992
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 6  ;  (leq([n, m])) -> max([n, m, m]) -> 7  ;  (not leq([n, m])) -> max([n, m, n]) -> 9  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 6  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 6  ;  (leq([s(nn1), z])) -> BOT -> 6  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 8
}
Sat witness: Yes: ((not leq([n, m])) -> max([n, m, n]), {
m -> z  ;  n -> s(z)
})

-------------------------------------------
Step 9, which took 0.012743 s (model generation: 0.011840,  model checking: 0.000903):

Model:
|_
{
leq -> 
{{{
Q={q_gen_9983, q_gen_9986, q_gen_9987, q_gen_9991},
Q_f={q_gen_9983},
Delta=
{
<z>() -> q_gen_9987
<s>(q_gen_9987) -> q_gen_9991
<s, s>(q_gen_9983) -> q_gen_9983
<z, s>(q_gen_9991) -> q_gen_9983
<z, z>() -> q_gen_9983
<s, z>(q_gen_9987) -> q_gen_9986
<z, s>(q_gen_9987) -> q_gen_9986
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  max -> 
{{{
Q={q_gen_9984, q_gen_9989, q_gen_9992, q_gen_9993},
Q_f={q_gen_9984},
Delta=
{
<z>() -> q_gen_9989
<z, z>() -> q_gen_9993
<s, z, s>(q_gen_9993) -> q_gen_9984
<z, s, z>(q_gen_9989) -> q_gen_9984
<z, z, z>() -> q_gen_9984
<z, s, s>(q_gen_9993) -> q_gen_9992
}

Datatype: <nat, nat, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, n2]) -> 9  ;  (leq([n, m])) -> max([n, m, m]) -> 7  ;  (not leq([n, m])) -> max([n, m, n]) -> 9  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 7  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 7  ;  (leq([s(nn1), z])) -> BOT -> 7  ;  (max([i, j, _idb])) -> eq_nat([i, _idb]) -> 8
}
Sat witness: Yes: (() -> leq([z, n2]), {
n2 -> s(z)
})

Total time: 0.123932
Reason for stopping: Disproved

