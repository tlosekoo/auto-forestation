Solving ../../../../benchmarks/smtlib/false/append_length_le.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(append, F:
{() -> append([nil, l2, l2])
(append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)])}
(append([_ujb, _vjb, _wjb]) /\ append([_ujb, _vjb, _xjb])) -> eq_natlist([_wjb, _xjb])
)
(le, P:
{() -> le([z, s(nn2)])
(le([nn1, nn2])) -> le([s(nn1), s(nn2)])
(le([s(nn1), s(nn2)])) -> le([nn1, nn2])
(le([s(nn1), z])) -> BOT
(le([z, z])) -> BOT}
)
(length, F:
{() -> length([nil, z])
(length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)])}
(length([_zjb, _akb]) /\ length([_zjb, _bkb])) -> eq_nat([_akb, _bkb])
)
}

properties:
{(append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb])}


over-approximation: {append, length}
under-approximation: {le}

Clause system for inference is:

{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 0  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 0  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 0  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 0  ;  (le([s(nn1), z])) -> BOT -> 0  ;  (le([z, z])) -> BOT -> 0  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 0
}


Solving took 0.067004 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.004582 s (model generation: 0.002854,  model checking: 0.001728):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> length([nil, z]), {

})

-------------------------------------------
Step 1, which took 0.004760 s (model generation: 0.004662,  model checking: 0.000098):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908},
Q_f={q_gen_9908},
Delta=
{
<nil, z>() -> q_gen_9908
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 2, which took 0.006928 s (model generation: 0.005274,  model checking: 0.001654):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<z, s>(q_gen_9910) -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908},
Q_f={q_gen_9908},
Delta=
{
<nil, z>() -> q_gen_9908
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> nil
})

-------------------------------------------
Step 3, which took 0.007868 s (model generation: 0.007646,  model checking: 0.000222):

Model:
|_
{
append -> 
{{{
Q={q_gen_9911},
Q_f={q_gen_9911},
Delta=
{
<nil, nil, nil>() -> q_gen_9911
}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<z, s>(q_gen_9910) -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908},
Q_f={q_gen_9908},
Delta=
{
<nil, z>() -> q_gen_9908
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]), {
_yjb -> z  ;  ll -> nil  ;  x -> z
})

-------------------------------------------
Step 4, which took 0.003326 s (model generation: 0.003246,  model checking: 0.000080):

Model:
|_
{
append -> 
{{{
Q={q_gen_9911},
Q_f={q_gen_9911},
Delta=
{
<nil, nil, nil>() -> q_gen_9911
}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<z, s>(q_gen_9910) -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908, q_gen_9913},
Q_f={q_gen_9908},
Delta=
{
<cons, s>(q_gen_9913, q_gen_9908) -> q_gen_9908
<nil, z>() -> q_gen_9908
<z, z>() -> q_gen_9913
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((le([nn1, nn2])) -> le([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> s(z)
})

-------------------------------------------
Step 5, which took 0.008819 s (model generation: 0.007859,  model checking: 0.000960):

Model:
|_
{
append -> 
{{{
Q={q_gen_9911},
Q_f={q_gen_9911},
Delta=
{
<nil, nil, nil>() -> q_gen_9911
}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<s, s>(q_gen_9909) -> q_gen_9909
<z, s>(q_gen_9910) -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908, q_gen_9913},
Q_f={q_gen_9908},
Delta=
{
<cons, s>(q_gen_9913, q_gen_9908) -> q_gen_9908
<nil, z>() -> q_gen_9908
<z, z>() -> q_gen_9913
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]), {
_tjb -> nil  ;  h1 -> z  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 6, which took 0.008100 s (model generation: 0.007856,  model checking: 0.000244):

Model:
|_
{
append -> 
{{{
Q={q_gen_9911, q_gen_9916, q_gen_9917, q_gen_9918, q_gen_9919},
Q_f={q_gen_9911},
Delta=
{
<nil, nil>() -> q_gen_9916
<nil, z>() -> q_gen_9917
<z, nil>() -> q_gen_9918
<z, z>() -> q_gen_9919
<cons, nil, cons>(q_gen_9919, q_gen_9918, q_gen_9917, q_gen_9916) -> q_gen_9911
<nil, nil, nil>() -> q_gen_9911
}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<s, s>(q_gen_9909) -> q_gen_9909
<z, s>(q_gen_9910) -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908, q_gen_9913},
Q_f={q_gen_9908},
Delta=
{
<cons, s>(q_gen_9913, q_gen_9908) -> q_gen_9908
<nil, z>() -> q_gen_9908
<z, z>() -> q_gen_9913
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 4  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]), {
_ckb -> z  ;  _dkb -> nil  ;  _ekb -> z  ;  l1 -> nil  ;  l2 -> nil
})

-------------------------------------------
Step 7, which took 0.012021 s (model generation: 0.011988,  model checking: 0.000033):

Model:
|_
{
append -> 
{{{
Q={q_gen_9911, q_gen_9916, q_gen_9917, q_gen_9918, q_gen_9919},
Q_f={q_gen_9911},
Delta=
{
<nil, nil>() -> q_gen_9916
<nil, z>() -> q_gen_9917
<z, nil>() -> q_gen_9918
<z, z>() -> q_gen_9919
<cons, nil, cons>(q_gen_9919, q_gen_9918, q_gen_9917, q_gen_9916) -> q_gen_9911
<nil, nil, nil>() -> q_gen_9911
}

Datatype: <natlist, natlist, natlist>
Convolution form: complete
}}}
  ;  le -> 
{{{
Q={q_gen_9909, q_gen_9910},
Q_f={q_gen_9909},
Delta=
{
<z>() -> q_gen_9910
<s, s>(q_gen_9909) -> q_gen_9909
<z, s>(q_gen_9910) -> q_gen_9909
<z, z>() -> q_gen_9909
}

Datatype: <nat, nat>
Convolution form: complete
}}}
  ;  length -> 
{{{
Q={q_gen_9908, q_gen_9913},
Q_f={q_gen_9908},
Delta=
{
<cons, s>(q_gen_9913, q_gen_9908) -> q_gen_9908
<nil, z>() -> q_gen_9908
<z, z>() -> q_gen_9913
}

Datatype: <natlist, nat>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 4  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 5  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((le([z, z])) -> BOT, {

})

Total time: 0.067004
Reason for stopping: Disproved

