Solving ../../../../benchmarks/smtlib/false/member_cons.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 20s
Convolution: complete


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(memberl, P:
{() -> memberl([h1, cons(h1, t1)])
(memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)])
(memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1])
(memberl([e, nil])) -> BOT}
)
(not_null, P:
{() -> not_null([cons(x, ll)])
(not_null([nil])) -> BOT}
)
}

properties:
{(memberl([i1, cons(i2, l)])) -> memberl([i1, l])}


over-approximation: {not_null}
under-approximation: {not_null}

Clause system for inference is:

{
() -> memberl([h1, cons(h1, t1)]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 0  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 0  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 0  ;  (memberl([e, nil])) -> BOT -> 0  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 0  ;  (not_null([nil])) -> BOT -> 0
}


Solving took 0.059674 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.011149 s (model generation: 0.009645,  model checking: 0.001504):

Model:
|_
{
memberl -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, natlist>
Convolution form: complete
}}}
  ;  not_null -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> memberl([h1, cons(h1, t1)]) -> 0  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 1  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 1  ;  (memberl([e, nil])) -> BOT -> 1  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> not_null([cons(x, ll)]), {
ll -> nil  ;  x -> z
})

-------------------------------------------
Step 1, which took 0.009064 s (model generation: 0.008837,  model checking: 0.000227):

Model:
|_
{
memberl -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, natlist>
Convolution form: complete
}}}
  ;  not_null -> 
{{{
Q={q_gen_9996, q_gen_9998},
Q_f={q_gen_9996},
Delta=
{
<cons>(q_gen_9998, q_gen_9996) -> q_gen_9996
<nil>() -> q_gen_9996
<z>() -> q_gen_9998
}

Datatype: <natlist>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> memberl([h1, cons(h1, t1)]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 1  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 1  ;  (memberl([e, nil])) -> BOT -> 1  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 1  ;  (not_null([nil])) -> BOT -> 1
}
Sat witness: Yes: (() -> memberl([h1, cons(h1, t1)]), {
h1 -> z  ;  t1 -> nil
})

-------------------------------------------
Step 2, which took 0.009932 s (model generation: 0.009901,  model checking: 0.000031):

Model:
|_
{
memberl -> 
{{{
Q={q_gen_10000, q_gen_10001, q_gen_9999},
Q_f={q_gen_9999},
Delta=
{
<nil>() -> q_gen_10000
<z>() -> q_gen_10001
<z, cons>(q_gen_10001, q_gen_10000) -> q_gen_9999
}

Datatype: <nat, natlist>
Convolution form: complete
}}}
  ;  not_null -> 
{{{
Q={q_gen_9996, q_gen_9998},
Q_f={q_gen_9996},
Delta=
{
<cons>(q_gen_9998, q_gen_9996) -> q_gen_9996
<nil>() -> q_gen_9996
<z>() -> q_gen_9998
}

Datatype: <natlist>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> memberl([h1, cons(h1, t1)]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 1  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 1  ;  (memberl([e, nil])) -> BOT -> 1  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 1  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((not_null([nil])) -> BOT, {

})

-------------------------------------------
Step 3, which took 0.010048 s (model generation: 0.009595,  model checking: 0.000453):

Model:
|_
{
memberl -> 
{{{
Q={q_gen_10000, q_gen_10001, q_gen_9999},
Q_f={q_gen_9999},
Delta=
{
<nil>() -> q_gen_10000
<z>() -> q_gen_10001
<z, cons>(q_gen_10001, q_gen_10000) -> q_gen_9999
}

Datatype: <nat, natlist>
Convolution form: complete
}}}
  ;  not_null -> 
{{{
Q={q_gen_9996, q_gen_9997, q_gen_9998},
Q_f={q_gen_9996},
Delta=
{
<cons>(q_gen_9998, q_gen_9997) -> q_gen_9996
<nil>() -> q_gen_9997
<z>() -> q_gen_9998
}

Datatype: <natlist>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> memberl([h1, cons(h1, t1)]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 1  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 1  ;  (memberl([e, nil])) -> BOT -> 1  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((memberl([i1, cons(i2, l)])) -> memberl([i1, l]), {
i1 -> z  ;  i2 -> z  ;  l -> nil
})

-------------------------------------------
Step 4, which took 0.010329 s (model generation: 0.010292,  model checking: 0.000037):

Model:
|_
{
memberl -> 
{{{
Q={q_gen_10000, q_gen_10001, q_gen_10002},
Q_f={q_gen_10002},
Delta=
{
<nil>() -> q_gen_10000
<z>() -> q_gen_10001
<z, cons>(q_gen_10001, q_gen_10000) -> q_gen_10002
<z, nil>() -> q_gen_10002
}

Datatype: <nat, natlist>
Convolution form: complete
}}}
  ;  not_null -> 
{{{
Q={q_gen_9996, q_gen_9997, q_gen_9998},
Q_f={q_gen_9996},
Delta=
{
<cons>(q_gen_9998, q_gen_9997) -> q_gen_9996
<nil>() -> q_gen_9997
<z>() -> q_gen_9998
}

Datatype: <natlist>
Convolution form: complete
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> memberl([h1, cons(h1, t1)]) -> 3  ;  () -> not_null([cons(x, ll)]) -> 3  ;  (memberl([e, t1]) /\ not eq_nat([e, h1])) -> memberl([e, cons(h1, t1)]) -> 1  ;  (memberl([e, cons(h1, t1)]) /\ not eq_nat([e, h1])) -> memberl([e, t1]) -> 1  ;  (memberl([e, nil])) -> BOT -> 4  ;  (memberl([i1, cons(i2, l)])) -> memberl([i1, l]) -> 4  ;  (not_null([nil])) -> BOT -> 4
}
Sat witness: Yes: ((memberl([e, nil])) -> BOT, {
e -> z
})

Total time: 0.059674
Reason for stopping: Disproved

