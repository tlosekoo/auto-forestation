Solving ../../benchmarks/smtlib/false/timbuk_equal.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}
}
definition:
{

}

properties:
{
eq_nat(i, j) <= True
}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{
eq_nat(i, j) <= True
}


Solving took 0.012274 seconds.
No: Contradictory set of ground constraints:
{
False <= True
}
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006583 s (model generation: 0.006534,  model checking: 0.000049):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_



--
Equality automata are defined for: {nat}
_|


Answer of teacher:


eq_nat(i, j) <= True -> Yes: {
i -> z  ;  j -> s(j_0)
}



Ground examples:
{
False <= True
}

Learner output:
Contradictory ground constraints

Last ice step stopped before teacher could answer
Total time: 0.012274
Learner time: 0.006534
Teacher time: 0.000049
Reasons for stopping: No: Contradictory set of ground constraints:
{
False <= True
}


