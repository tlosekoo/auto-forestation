Solving ../../benchmarks/smtlib/true/fst_nat.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elist -> {econs, enil}  ;  elt -> {a, b}  ;  nat -> {s, z}  ;  npair -> {npair}
}
definition:
{
(fst_nat, F:
{
fst_nat(npair(n1, n2), n1) <= True
}
eq_nat(_ld, _md) <= fst_nat(_kd, _ld) /\ fst_nat(_kd, _md)
)
}

properties:
{
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd)
}


over-approximation: {fst_nat}
under-approximation: {}

Clause system for inference is:

{
fst_nat(npair(n1, n2), n1) <= True
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd)
}


Solving took 0.062649 seconds.
Yes: |_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(s(x_0_0), z) <= _r_2(x_0_0)
}
;
_r_2 ->
{
  _r_2(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006258 s (model generation: 0.006235,  model checking: 0.000023):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

fst_nat ->
{
  
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> Yes: {
n1 -> z
}
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> No: ()



-------------------------------------------
Step 1, which took 0.006534 s (model generation: 0.006509,  model checking: 0.000025):

Ground examples at the beginning of this step are:
{
fst_nat(npair(z, z), z) <= True
}

Learner proposed model:
Found Model:
|_

fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), z) <= True
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> Yes: {
n1 -> s(n1_0)
}
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> Yes: {
_nd -> z  ;  n1 -> s(n1_0)
}



-------------------------------------------
Step 2, which took 0.009035 s (model generation: 0.008900,  model checking: 0.000135):

Ground examples at the beginning of this step are:
{
fst_nat(npair(s(z), z), s(z)) <= True
fst_nat(npair(z, z), z) <= True
False <= fst_nat(npair(s(z), z), z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= True
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_1(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> No: ()
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> Yes: {
_nd -> s(_nd_0)  ;  n1 -> z
}



-------------------------------------------
Step 3, which took 0.011718 s (model generation: 0.011640,  model checking: 0.000078):

Ground examples at the beginning of this step are:
{
fst_nat(npair(s(z), z), s(z)) <= True
fst_nat(npair(z, z), z) <= True
False <= fst_nat(npair(s(z), z), z)
False <= fst_nat(npair(z, z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0)) <= True
}
;
_r_2 ->
{
  _r_2(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= _r_1(x_0_0)
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> No: ()
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> Yes: {
_nd -> s(z)  ;  n1 -> s(s(v1_0))
}



-------------------------------------------
Step 4, which took 0.012493 s (model generation: 0.012395,  model checking: 0.000098):

Ground examples at the beginning of this step are:
{
fst_nat(npair(s(z), z), s(z)) <= True
fst_nat(npair(z, z), z) <= True
False <= fst_nat(npair(s(s(z)), z), s(z))
False <= fst_nat(npair(s(z), z), z)
False <= fst_nat(npair(z, z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0)) <= _r_2(x_0_0)
}
;
_r_2 ->
{
  _r_2(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= _r_1(x_0_0)
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> Yes: {
n1 -> s(s(v0_0))
}
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> Yes: {
_nd -> s(s(v0_0))  ;  n1 -> s(z)
}



-------------------------------------------
Step 5, which took 0.015544 s (model generation: 0.015454,  model checking: 0.000090):

Ground examples at the beginning of this step are:
{
fst_nat(npair(s(s(z)), z), s(s(z))) <= True
fst_nat(npair(s(z), z), s(z)) <= True
fst_nat(npair(z, z), z) <= True
False <= fst_nat(npair(s(s(z)), z), s(z))
False <= fst_nat(npair(s(z), z), s(s(z)))
False <= fst_nat(npair(s(z), z), z)
False <= fst_nat(npair(z, z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(s(x_0_0), z) <= _r_2(x_0_0)
}
;
_r_2 ->
{
  _r_2(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


Answer of teacher:


fst_nat(npair(n1, n2), n1) <= True -> No: ()
eq_nat(_nd, n1) <= fst_nat(npair(n1, n2), _nd) -> No: ()




Total time: 0.062649
Learner time: 0.061133
Teacher time: 0.000449
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(s(x_0_0), z) <= _r_2(x_0_0)
}
;
_r_2 ->
{
  _r_2(z) <= True
}
;
fst_nat ->
{
  fst_nat(npair(x_0_0, x_0_1), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  fst_nat(npair(x_0_0, x_0_1), z) <= _r_2(x_0_0)
}

--
Equality automata are defined for: {elist, elt, nat, npair}
_|


