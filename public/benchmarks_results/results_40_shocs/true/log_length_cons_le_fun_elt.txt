Solving ../../benchmarks/smtlib/true/length_cons_le_fun_elt.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(le, P:
{
le(z, s(nn2)) <= True
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
}
)
(length, F:
{
length(nil, z) <= True
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa)
}
eq_nat(_mwa, _nwa) <= length(_lwa, _mwa) /\ length(_lwa, _nwa)
)
(fcons, F:
{
fcons(x, l, cons(x, l)) <= True
}
eq_eltlist(_qwa, _rwa) <= fcons(_owa, _pwa, _qwa) /\ fcons(_owa, _pwa, _rwa)
)
}

properties:
{
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa)
}


over-approximation: {fcons, length}
under-approximation: {le}

Clause system for inference is:

{
fcons(x, l, cons(x, l)) <= True
le(z, s(nn2)) <= True
length(nil, z) <= True
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa)
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa)
}


Solving took 0.126214 seconds.
Yes: |_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(nil, cons(x_1_0, x_1_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006337 s (model generation: 0.006247,  model checking: 0.000090):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

fcons ->
{
  
}
;
le ->
{
  
}
;
length ->
{
  
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> Yes: {
l -> nil  ;  x -> b
}
le(z, s(nn2)) <= True -> Yes: {

}
length(nil, z) <= True -> Yes: {

}
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 1, which took 0.007449 s (model generation: 0.007368,  model checking: 0.000081):

Ground examples at the beginning of this step are:
{
fcons(b, nil, cons(b, nil)) <= True
le(z, s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

fcons ->
{
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> Yes: {
l -> nil  ;  x -> a
}
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> s(v1_0)
}
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> Yes: {
_kwa -> z  ;  ll -> nil
}



-------------------------------------------
Step 2, which took 0.008509 s (model generation: 0.008336,  model checking: 0.000173):

Ground examples at the beginning of this step are:
{
fcons(a, nil, cons(a, nil)) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

fcons ->
{
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> Yes: {
l -> cons(l_0, l_1)  ;  x -> b
}
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 3, which took 0.009231 s (model generation: 0.009161,  model checking: 0.000070):

Ground examples at the beginning of this step are:
{
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
le(z, z) <= le(s(z), s(z))
}

Learner proposed model:
Found Model:
|_

fcons ->
{
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
  le(z, z) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> Yes: {
l -> cons(l_0, l_1)  ;  x -> a
}
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> Yes: {

}
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 4, which took 0.009856 s (model generation: 0.009737,  model checking: 0.000119):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
le(s(z), z) <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(s(x_0_0), z) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> Yes: {
_swa -> s(z)  ;  _twa -> s(z)  ;  _uwa -> cons(_uwa_0, _uwa_1)  ;  l -> cons(l_0, l_1)  ;  x -> b
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> Yes: {

}
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 5, which took 0.014734 s (model generation: 0.014594,  model checking: 0.000140):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= fcons(b, cons(a, nil), cons(a, nil))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(b) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_2_0)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> Yes: {
_swa -> s(z)  ;  _twa -> s(z)  ;  _uwa -> cons(_uwa_0, _uwa_1)  ;  l -> cons(l_0, l_1)  ;  x -> a
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 6, which took 0.016654 s (model generation: 0.016499,  model checking: 0.000155):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= fcons(a, cons(a, nil), cons(a, nil))
False <= fcons(b, cons(a, nil), cons(a, nil))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> Yes: {
_swa -> s(z)  ;  _twa -> s(z)  ;  _uwa -> cons(_uwa_0, cons(v0_0, v0_1))  ;  l -> cons(l_0, l_1)  ;  x -> b
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 7, which took 0.015971 s (model generation: 0.015644,  model checking: 0.000327):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= fcons(a, cons(a, nil), cons(a, nil))
False <= fcons(b, cons(a, nil), cons(a, cons(a, nil))) /\ length(cons(a, cons(a, nil)), s(z))
False <= fcons(b, cons(a, nil), cons(a, nil))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> Yes: {
_swa -> s(s(z))  ;  _twa -> s(s(z))  ;  _uwa -> cons(_uwa_0, cons(v0_0, nil))  ;  l -> cons(l_0, cons(v3_0, nil))  ;  x -> b
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 8, which took 0.017508 s (model generation: 0.017343,  model checking: 0.000165):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= fcons(a, cons(a, nil), cons(a, nil))
le(s(s(z)), s(s(z))) <= fcons(b, cons(a, cons(a, nil)), cons(a, cons(a, nil))) /\ length(cons(a, cons(a, nil)), s(s(z)))
False <= fcons(b, cons(a, nil), cons(a, cons(a, nil))) /\ length(cons(a, cons(a, nil)), s(z))
False <= fcons(b, cons(a, nil), cons(a, nil))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(nil, cons(x_1_0, x_1_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> Yes: {
l -> cons(l_0, cons(v0_0, v0_1))  ;  x -> b
}
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()



-------------------------------------------
Step 9, which took 0.019739 s (model generation: 0.019428,  model checking: 0.000311):

Ground examples at the beginning of this step are:
{
fcons(a, cons(a, nil), cons(a, cons(a, nil))) <= True
fcons(a, nil, cons(a, nil)) <= True
fcons(b, cons(a, cons(a, nil)), cons(b, cons(a, cons(a, nil)))) <= True
fcons(b, cons(a, nil), cons(b, cons(a, nil))) <= True
fcons(b, nil, cons(b, nil)) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= fcons(a, cons(a, nil), cons(a, nil))
le(s(s(z)), s(s(z))) <= fcons(b, cons(a, cons(a, nil)), cons(a, cons(a, nil))) /\ length(cons(a, cons(a, nil)), s(s(z)))
False <= fcons(b, cons(a, nil), cons(a, cons(a, nil))) /\ length(cons(a, cons(a, nil)), s(z))
False <= fcons(b, cons(a, nil), cons(a, nil))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(nil, cons(x_1_0, x_1_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


fcons(x, l, cons(x, l)) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(_swa, _twa) <= fcons(x, l, _uwa) /\ length(_uwa, _twa) /\ length(l, _swa) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_kwa)) <= length(ll, _kwa) -> No: ()




Total time: 0.126214
Learner time: 0.124357
Teacher time: 0.001631
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= _r_1(x_0_1, x_1_1)
  _r_1(nil, cons(x_1_0, x_1_1)) <= True
}
;
fcons ->
{
  fcons(a, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(a, nil, cons(x_2_0, x_2_1)) <= True
  fcons(b, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= _r_1(x_1_1, x_2_1)
  fcons(b, nil, cons(x_2_0, x_2_1)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


