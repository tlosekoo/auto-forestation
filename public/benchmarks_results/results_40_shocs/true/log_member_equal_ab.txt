Solving ../../benchmarks/smtlib/true/member_equal_ab.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}
}
definition:
{
(memberl, P:
{
memberl(h1, cons(h1, t1)) <= True
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1)
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1))
False <= memberl(e, nil)
}
)
}

properties:
{
memberl(i, l2) <= memberl(i, l2)
}


over-approximation: {}
under-approximation: {}

Clause system for inference is:

{
memberl(h1, cons(h1, t1)) <= True
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1)
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1))
False <= memberl(e, nil)
memberl(i, l2) <= memberl(i, l2)
}


Solving took 0.148882 seconds.
Yes: |_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= _r_1(x_0_1)
  _r_1(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= _r_2(x_0_1)
  _r_2(cons(x_0_0, x_0_1)) <= _r_3(x_0_0)
}
;
_r_3 ->
{
  _r_3(b) <= True
}
;
_r_4 ->
{
  _r_4(a) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(a, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006659 s (model generation: 0.006608,  model checking: 0.000051):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

memberl ->
{
  
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> Yes: {
h1 -> b
}
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> No: ()
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> No: ()
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 1, which took 0.007217 s (model generation: 0.006845,  model checking: 0.000372):

Ground examples at the beginning of this step are:
{
memberl(b, cons(b, nil)) <= True
}

Learner proposed model:
Found Model:
|_

memberl ->
{
  memberl(b, cons(x_1_0, x_1_1)) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> Yes: {
h1 -> a
}
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> No: ()
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> nil
}
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 2, which took 0.008355 s (model generation: 0.008313,  model checking: 0.000042):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(b, cons(b, nil)) <= True
memberl(b, nil) <= memberl(b, cons(a, nil))
}

Learner proposed model:
Found Model:
|_

memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= True
  memberl(b, cons(x_1_0, x_1_1)) <= True
  memberl(b, nil) <= True
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> No: ()
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> nil
}
False <= memberl(e, nil) -> Yes: {
e -> b
}
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 3, which took 0.009314 s (model generation: 0.009157,  model checking: 0.000157):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(b, cons(b, nil)) <= True
memberl(a, nil) <= memberl(a, cons(b, nil))
False <= memberl(b, cons(a, nil))
False <= memberl(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(b) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= True
  memberl(a, nil) <= True
  memberl(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(b, t1_1)
}
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> No: ()
False <= memberl(e, nil) -> Yes: {
e -> a
}
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 4, which took 0.016082 s (model generation: 0.015450,  model checking: 0.000632):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(b, cons(a, cons(b, nil))) <= True
memberl(b, cons(b, nil)) <= True
False <= memberl(a, cons(b, nil))
False <= memberl(a, nil)
False <= memberl(b, cons(a, nil))
False <= memberl(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= True
}
;
_r_2 ->
{
  _r_2(a) <= True
}
;
_r_3 ->
{
  _r_3(b) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_2(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> cons(a, t1_1)
}
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(a, nil)
}
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 5, which took 0.026130 s (model generation: 0.025627,  model checking: 0.000503):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(a, cons(b, cons(a, nil))) <= True
memberl(b, cons(a, cons(b, nil))) <= True
memberl(b, cons(b, nil)) <= True
False <= memberl(a, cons(b, nil))
False <= memberl(a, nil)
False <= memberl(b, cons(a, cons(a, nil)))
False <= memberl(b, cons(a, nil))
False <= memberl(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= True
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_3 ->
{
  _r_3(a) <= True
}
;
_r_4 ->
{
  _r_4(b) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(a, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> Yes: {
e -> b  ;  h1 -> a  ;  t1 -> cons(a, cons(b, v0_1))
}
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> cons(b, nil)
}
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 6, which took 0.033128 s (model generation: 0.032605,  model checking: 0.000523):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(a, cons(b, cons(a, nil))) <= True
memberl(b, cons(a, cons(a, cons(b, nil)))) <= True
memberl(b, cons(a, cons(b, nil))) <= True
memberl(b, cons(b, nil)) <= True
False <= memberl(a, cons(b, cons(b, nil)))
False <= memberl(a, cons(b, nil))
False <= memberl(a, nil)
False <= memberl(b, cons(a, cons(a, nil)))
False <= memberl(b, cons(a, nil))
False <= memberl(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= _r_1(x_0_1)
  _r_1(cons(x_0_0, x_0_1)) <= _r_3(x_0_0)
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_3 ->
{
  _r_3(b) <= True
}
;
_r_4 ->
{
  _r_4(a) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  memberl(a, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> Yes: {
e -> a  ;  h1 -> b  ;  t1 -> cons(b, cons(a, v0_1))
}
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> No: ()
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()



-------------------------------------------
Step 7, which took 0.041842 s (model generation: 0.041331,  model checking: 0.000511):

Ground examples at the beginning of this step are:
{
memberl(a, cons(a, nil)) <= True
memberl(a, cons(b, cons(a, nil))) <= True
memberl(a, cons(b, cons(b, cons(a, nil)))) <= True
memberl(b, cons(a, cons(a, cons(b, nil)))) <= True
memberl(b, cons(a, cons(b, nil))) <= True
memberl(b, cons(b, nil)) <= True
False <= memberl(a, cons(b, cons(b, nil)))
False <= memberl(a, cons(b, nil))
False <= memberl(a, nil)
False <= memberl(b, cons(a, cons(a, nil)))
False <= memberl(b, cons(a, nil))
False <= memberl(b, nil)
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= _r_1(x_0_1)
  _r_1(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= _r_2(x_0_1)
  _r_2(cons(x_0_0, x_0_1)) <= _r_3(x_0_0)
}
;
_r_3 ->
{
  _r_3(b) <= True
}
;
_r_4 ->
{
  _r_4(a) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(a, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


Answer of teacher:


memberl(h1, cons(h1, t1)) <= True -> No: ()
eq_elt(e, h1) \/ memberl(e, cons(h1, t1)) <= memberl(e, t1) -> No: ()
eq_elt(e, h1) \/ memberl(e, t1) <= memberl(e, cons(h1, t1)) -> No: ()
False <= memberl(e, nil) -> No: ()
memberl(i, l2) <= memberl(i, l2) -> No: ()




Total time: 0.148882
Learner time: 0.145936
Teacher time: 0.002791
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(cons(x_0_0, x_0_1)) <= _r_1(x_0_1)
  _r_1(cons(x_0_0, x_0_1)) <= _r_4(x_0_0)
}
;
_r_2 ->
{
  _r_2(cons(x_0_0, x_0_1)) <= _r_2(x_0_1)
  _r_2(cons(x_0_0, x_0_1)) <= _r_3(x_0_0)
}
;
_r_3 ->
{
  _r_3(b) <= True
}
;
_r_4 ->
{
  _r_4(a) <= True
}
;
memberl ->
{
  memberl(a, cons(x_1_0, x_1_1)) <= _r_1(x_1_1)
  memberl(a, cons(x_1_0, x_1_1)) <= _r_4(x_1_0)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_2(x_1_1)
  memberl(b, cons(x_1_0, x_1_1)) <= _r_3(x_1_0)
}

--
Equality automata are defined for: {elt, eltlist}
_|


