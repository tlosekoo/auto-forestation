Solving ../../benchmarks/smtlib/true/plus_succ_le_2.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(plus, F:
{
plus(n, z, n) <= True
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt)
}
eq_nat(_nt, _ot) <= plus(_lt, _mt, _nt) /\ plus(_lt, _mt, _ot)
)
(le, P:
{
le(z, s(nn2)) <= True
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
}
)
}

properties:
{
le(n, _pt) <= plus(n, s(m), _pt)
}


over-approximation: {plus}
under-approximation: {le}

Clause system for inference is:

{
le(z, s(nn2)) <= True
plus(n, z, n) <= True
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt)
le(n, _pt) <= plus(n, s(m), _pt)
}


Solving took 0.144059 seconds.
Yes: |_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006386 s (model generation: 0.006333,  model checking: 0.000053):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

le ->
{
  
}
;
plus ->
{
  
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> Yes: {

}
plus(n, z, n) <= True -> Yes: {
n -> z
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 1, which took 0.006961 s (model generation: 0.006873,  model checking: 0.000088):

Ground examples at the beginning of this step are:
{
le(z, s(z)) <= True
plus(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> Yes: {
n -> s(n_0)
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> s(v1_0)
}
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> Yes: {
_kt -> z  ;  mm -> z  ;  n -> z
}
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 2, which took 0.008795 s (model generation: 0.008734,  model checking: 0.000061):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> Yes: {
_kt -> s(_kt_0)  ;  mm -> z  ;  n -> s(n_0)
}
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 3, which took 0.009481 s (model generation: 0.009416,  model checking: 0.000065):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
le(z, z) <= le(s(z), s(z))
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
  le(z, z) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> Yes: {

}
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 4, which took 0.009285 s (model generation: 0.009182,  model checking: 0.000103):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
le(s(z), z) <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(s(x_0_0), z) <= True
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> Yes: {

}
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> Yes: {
_pt -> s(z)  ;  n -> s(z)
}



-------------------------------------------
Step 5, which took 0.009499 s (model generation: 0.009369,  model checking: 0.000130):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> Yes: {
_kt -> s(z)  ;  mm -> z  ;  n -> s(s(z))
}
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 6, which took 0.010031 s (model generation: 0.009871,  model checking: 0.000160):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
plus(s(s(z)), s(z), s(s(z))) <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_1_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> Yes: {
_pt -> s(s(z))  ;  m -> z  ;  n -> s(s(z))
}



-------------------------------------------
Step 7, which took 0.015883 s (model generation: 0.015705,  model checking: 0.000178):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
le(s(s(z)), s(s(z))) <= plus(s(s(z)), s(z), s(s(z)))
plus(s(s(z)), s(z), s(s(z))) <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0)) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= _r_1(x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= _r_1(x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= True
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v1_0)  ;  nn2 -> s(z)
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 8, which took 0.013887 s (model generation: 0.013331,  model checking: 0.000556):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(s(z)))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
False <= plus(s(s(z)), s(z), s(s(z)))
False <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> Yes: {
n -> s(s(v0_0))
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 9, which took 0.014959 s (model generation: 0.014530,  model checking: 0.000429):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(s(z)), z, s(s(z))) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(s(z)))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
False <= plus(s(s(z)), s(z), s(s(z)))
False <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= True
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= _r_1(x_0_0, x_1_0) /\ le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> Yes: {
_kt -> s(s(v1_0))  ;  mm -> z  ;  n -> s(s(v0_0))
}
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 10, which took 0.017723 s (model generation: 0.017518,  model checking: 0.000205):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(s(z)), s(z), s(s(s(z)))) <= True
plus(s(s(z)), z, s(s(z))) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(s(z)))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
False <= plus(s(s(z)), s(z), s(s(z)))
False <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= True
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> Yes: {
_kt -> s(s(z))  ;  mm -> z  ;  n -> s(s(s(z)))
}
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()



-------------------------------------------
Step 11, which took 0.020944 s (model generation: 0.020774,  model checking: 0.000170):

Ground examples at the beginning of this step are:
{
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
plus(s(s(z)), s(z), s(s(s(z)))) <= True
plus(s(s(z)), z, s(s(z))) <= True
plus(s(z), s(z), s(s(z))) <= True
plus(s(z), z, s(z)) <= True
plus(z, s(z), s(z)) <= True
plus(z, z, z) <= True
False <= le(s(s(z)), s(s(z)))
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
plus(s(s(s(z))), s(z), s(s(s(z)))) <= plus(s(s(s(z))), z, s(s(z)))
False <= plus(s(s(z)), s(z), s(s(z)))
False <= plus(s(s(z)), z, s(z))
False <= plus(s(z), s(z), s(z))
}

Learner proposed model:
Found Model:
|_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


le(z, s(nn2)) <= True -> No: ()
plus(n, z, n) <= True -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
plus(n, s(mm), s(_kt)) <= plus(n, mm, _kt) -> No: ()
le(n, _pt) <= plus(n, s(m), _pt) -> No: ()




Total time: 0.144059
Learner time: 0.141636
Teacher time: 0.002198
Reasons for stopping: Yes: |_

_r_1 ->
{
  _r_1(s(x_0_0), s(x_1_0)) <= _r_1(x_0_0, x_1_0)
  _r_1(z, z) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
plus ->
{
  plus(s(x_0_0), s(x_1_0), s(x_2_0)) <= le(x_0_0, x_2_0)
  plus(s(x_0_0), z, s(x_2_0)) <= _r_1(x_0_0, x_2_0)
  plus(z, s(x_1_0), s(x_2_0)) <= True
  plus(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


