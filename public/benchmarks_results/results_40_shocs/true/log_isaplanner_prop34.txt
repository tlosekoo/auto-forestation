Solving ../../benchmarks/smtlib/true/isaplanner_prop34.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{
leq(z, n2) <= True
leq(s(nn1), s(nn2)) <= leq(nn1, nn2)
leq(nn1, nn2) <= leq(s(nn1), s(nn2))
False <= leq(s(nn1), z)
}
)
(min, F:
{
min(s(u), z, z) <= True
min(z, y, z) <= True
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna)
}
eq_nat(_qna, _rna) <= min(_ona, _pna, _qna) /\ min(_ona, _pna, _rna)
)
}

properties:
{
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna)
leq(j, i) <= min(i, j, j)
}


over-approximation: {min}
under-approximation: {}

Clause system for inference is:

{
leq(z, n2) <= True
min(s(u), z, z) <= True
min(z, y, z) <= True
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna)
leq(s(nn1), s(nn2)) <= leq(nn1, nn2)
leq(nn1, nn2) <= leq(s(nn1), s(nn2))
False <= leq(s(nn1), z)
leq(j, i) <= min(i, j, j)
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna)
}


Solving took 0.039281 seconds.
Yes: |_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), s(x_1_0), s(x_2_0)) <= min(x_0_0, x_1_0, x_2_0)
  min(s(x_0_0), z, z) <= True
  min(z, s(x_1_0), z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.006133 s (model generation: 0.006021,  model checking: 0.000112):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

leq ->
{
  
}
;
min ->
{
  
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, n2) <= True -> Yes: {
n2 -> z
}
min(s(u), z, z) <= True -> Yes: {

}
min(z, y, z) <= True -> Yes: {
y -> z
}
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna) -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(j, i) <= min(i, j, j) -> No: ()
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna) -> No: ()



-------------------------------------------
Step 1, which took 0.006990 s (model generation: 0.006901,  model checking: 0.000089):

Ground examples at the beginning of this step are:
{
leq(z, z) <= True
min(s(z), z, z) <= True
min(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), z, z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, n2) <= True -> Yes: {
n2 -> s(n2_0)
}
min(s(u), z, z) <= True -> No: ()
min(z, y, z) <= True -> Yes: {
y -> s(y_0)
}
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna) -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(j, i) <= min(i, j, j) -> Yes: {
i -> s(i_0)  ;  j -> z
}
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna) -> Yes: {
_nna -> z  ;  u -> z  ;  y1 -> z
}



-------------------------------------------
Step 2, which took 0.008266 s (model generation: 0.008170,  model checking: 0.000096):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
min(s(z), s(z), s(z)) <= True
min(s(z), z, z) <= True
min(z, s(z), z) <= True
min(z, z, z) <= True
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= True
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  min(s(x_0_0), z, z) <= True
  min(z, s(x_1_0), z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, n2) <= True -> No: ()
min(s(u), z, z) <= True -> No: ()
min(z, y, z) <= True -> No: ()
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna) -> Yes: {
_tna -> s(z)  ;  i -> s(i_0)  ;  j -> s(s(v1_0))
}
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
False <= leq(s(nn1), z) -> No: ()
leq(j, i) <= min(i, j, j) -> No: ()
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna) -> No: ()



-------------------------------------------
Step 3, which took 0.008473 s (model generation: 0.008333,  model checking: 0.000140):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
min(s(z), s(z), s(z)) <= True
min(s(z), z, z) <= True
min(z, s(z), z) <= True
min(z, z, z) <= True
leq(s(z), z) <= leq(s(s(z)), s(z))
False <= leq(s(s(z)), s(z)) /\ min(s(z), s(s(z)), s(z))
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), s(x_1_0), s(x_2_0)) <= True
  min(s(x_0_0), z, z) <= True
  min(z, s(x_1_0), z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, n2) <= True -> No: ()
min(s(u), z, z) <= True -> No: ()
min(z, y, z) <= True -> No: ()
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna) -> Yes: {
_tna -> s(s(v0_0))  ;  i -> s(z)  ;  j -> s(z)
}
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(j, i) <= min(i, j, j) -> Yes: {
i -> s(z)  ;  j -> s(s(v0_0))
}
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna) -> No: ()



-------------------------------------------
Step 4, which took 0.009326 s (model generation: 0.009127,  model checking: 0.000199):

Ground examples at the beginning of this step are:
{
leq(s(z), s(z)) <= True
leq(z, s(z)) <= True
leq(z, z) <= True
min(s(z), s(z), s(z)) <= True
min(s(z), z, z) <= True
min(z, s(z), z) <= True
min(z, z, z) <= True
leq(s(z), z) <= leq(s(s(z)), s(z))
False <= leq(s(s(z)), s(z)) /\ min(s(z), s(s(z)), s(z))
leq(s(s(z)), s(z)) <= min(s(z), s(s(z)), s(s(z)))
False <= min(s(z), s(z), s(s(z)))
}

Learner proposed model:
Found Model:
|_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), s(x_1_0), s(x_2_0)) <= min(x_0_0, x_1_0, x_2_0)
  min(s(x_0_0), z, z) <= True
  min(z, s(x_1_0), z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


Answer of teacher:


leq(z, n2) <= True -> No: ()
min(s(u), z, z) <= True -> No: ()
min(z, y, z) <= True -> No: ()
eq_nat(_tna, j) <= leq(j, i) /\ min(i, j, _tna) -> No: ()
leq(s(nn1), s(nn2)) <= leq(nn1, nn2) -> No: ()
leq(nn1, nn2) <= leq(s(nn1), s(nn2)) -> No: ()
False <= leq(s(nn1), z) -> No: ()
leq(j, i) <= min(i, j, j) -> No: ()
min(s(u), s(y1), s(_nna)) <= min(u, y1, _nna) -> No: ()




Total time: 0.039281
Learner time: 0.038552
Teacher time: 0.000636
Reasons for stopping: Yes: |_

leq ->
{
  leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
  leq(z, s(x_1_0)) <= True
  leq(z, z) <= True
}
;
min ->
{
  min(s(x_0_0), s(x_1_0), s(x_2_0)) <= min(x_0_0, x_1_0, x_2_0)
  min(s(x_0_0), z, z) <= True
  min(z, s(x_1_0), z) <= True
  min(z, z, z) <= True
}

--
Equality automata are defined for: {nat}
_|


