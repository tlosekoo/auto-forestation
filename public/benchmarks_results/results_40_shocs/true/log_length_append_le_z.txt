Solving ../../benchmarks/smtlib/true/length_append_le_z.smt2...

Inference procedure has parameters:
Timeout: Some(40.) (sec)
Approximation method: remove functionnality constraint where possible



Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(le, P:
{
le(z, s(nn2)) <= True
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
}
)
(length, F:
{
length(nil, z) <= True
length(cons(x, ll), s(_uda)) <= length(ll, _uda)
}
eq_nat(_wda, _xda) <= length(_vda, _wda) /\ length(_vda, _xda)
)
(append, F:
{
append(nil, l2, l2) <= True
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda)
}
eq_eltlist(_bea, _cea) <= append(_zda, _aea, _bea) /\ append(_zda, _aea, _cea)
)
}

properties:
{
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea)
}


over-approximation: {append, length}
under-approximation: {}

Clause system for inference is:

{
append(nil, l2, l2) <= True
le(z, s(nn2)) <= True
length(nil, z) <= True
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea)
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda)
le(s(nn1), s(nn2)) <= le(nn1, nn2)
le(nn1, nn2) <= le(s(nn1), s(nn2))
False <= le(s(nn1), z)
False <= le(z, z)
length(cons(x, ll), s(_uda)) <= length(ll, _uda)
}


Solving took 0.061210 seconds.
Yes: |_

append ->
{
  append(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.007134 s (model generation: 0.006847,  model checking: 0.000287):

Ground examples at the beginning of this step are:
{

}

Learner proposed model:
Found Model:
|_

append ->
{
  
}
;
le ->
{
  
}
;
length ->
{
  
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> Yes: {
l2 -> nil
}
le(z, s(nn2)) <= True -> Yes: {

}
length(nil, z) <= True -> Yes: {

}
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> No: ()



-------------------------------------------
Step 1, which took 0.007030 s (model generation: 0.006943,  model checking: 0.000087):

Ground examples at the beginning of this step are:
{
append(nil, nil, nil) <= True
le(z, s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

append ->
{
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> Yes: {
l2 -> cons(l2_0, l2_1)
}
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> Yes: {
_yda -> nil  ;  l2 -> nil  ;  t1 -> nil
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> Yes: {
nn1 -> z  ;  nn2 -> s(v1_0)
}
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> Yes: {
_uda -> z  ;  ll -> nil
}



-------------------------------------------
Step 2, which took 0.009694 s (model generation: 0.009365,  model checking: 0.000329):

Ground examples at the beginning of this step are:
{
append(cons(a, nil), nil, cons(a, nil)) <= True
append(nil, cons(a, nil), cons(a, nil)) <= True
append(nil, nil, nil) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
}

Learner proposed model:
Found Model:
|_

append ->
{
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> Yes: {
_yda -> cons(_yda_0, _yda_1)  ;  l2 -> cons(l2_0, l2_1)  ;  t1 -> nil
}
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> z  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> No: ()



-------------------------------------------
Step 3, which took 0.012484 s (model generation: 0.012283,  model checking: 0.000201):

Ground examples at the beginning of this step are:
{
append(cons(a, nil), cons(a, nil), cons(a, cons(a, nil))) <= True
append(cons(a, nil), nil, cons(a, nil)) <= True
append(nil, cons(a, nil), cons(a, nil)) <= True
append(nil, nil, nil) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
le(z, z) <= le(s(z), s(z))
}

Learner proposed model:
Found Model:
|_

append ->
{
  append(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= True
  le(z, s(x_1_0)) <= True
  le(z, z) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> Yes: {
nn1 -> s(v0_0)  ;  nn2 -> z
}
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> Yes: {

}
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> No: ()



-------------------------------------------
Step 4, which took 0.012326 s (model generation: 0.012105,  model checking: 0.000221):

Ground examples at the beginning of this step are:
{
append(cons(a, nil), cons(a, nil), cons(a, cons(a, nil))) <= True
append(cons(a, nil), nil, cons(a, nil)) <= True
append(nil, cons(a, nil), cons(a, nil)) <= True
append(nil, nil, nil) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
le(s(z), z) <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

append ->
{
  append(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(s(x_0_0), z) <= True
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> Yes: {

}
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> No: ()



-------------------------------------------
Step 5, which took 0.012393 s (model generation: 0.012183,  model checking: 0.000210):

Ground examples at the beginning of this step are:
{
append(cons(a, nil), cons(a, nil), cons(a, cons(a, nil))) <= True
append(cons(a, nil), nil, cons(a, nil)) <= True
append(nil, cons(a, nil), cons(a, nil)) <= True
append(nil, nil, nil) <= True
le(s(z), s(s(z))) <= True
le(z, s(z)) <= True
length(cons(a, nil), s(z)) <= True
length(nil, z) <= True
False <= le(s(s(z)), s(z))
False <= le(s(z), s(z))
False <= le(s(z), z)
False <= le(z, z)
}

Learner proposed model:
Found Model:
|_

append ->
{
  append(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


Answer of teacher:


append(nil, l2, l2) <= True -> No: ()
le(z, s(nn2)) <= True -> No: ()
length(nil, z) <= True -> No: ()
le(z, _dea) <= append(l1, l2, _eea) /\ le(z, _fea) /\ length(_eea, _dea) /\ length(l1, _fea) -> No: ()
append(cons(h1, t1), l2, cons(h1, _yda)) <= append(t1, l2, _yda) -> No: ()
le(s(nn1), s(nn2)) <= le(nn1, nn2) -> No: ()
le(nn1, nn2) <= le(s(nn1), s(nn2)) -> No: ()
False <= le(s(nn1), z) -> No: ()
False <= le(z, z) -> No: ()
length(cons(x, ll), s(_uda)) <= length(ll, _uda) -> No: ()




Total time: 0.061210
Learner time: 0.059726
Teacher time: 0.001334
Reasons for stopping: Yes: |_

append ->
{
  append(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(cons(x_0_0, x_0_1), nil, cons(x_2_0, x_2_1)) <= True
  append(nil, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
  append(nil, nil, nil) <= True
}
;
le ->
{
  le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
  le(z, s(x_1_0)) <= True
}
;
length ->
{
  length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  length(nil, z) <= True
}

--
Equality automata are defined for: {elt, eltlist, nat}
_|


