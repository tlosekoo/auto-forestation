Solving ../../../../benchmarks/smtlib/false/list_delete_all_count.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: left


Learning problem is:

env: {
elt -> {a, b}  ;  eltlist -> {cons, nil}  ;  nat -> {s, z}
}
definition:
{
(delete_one, F:
{() -> delete_one([x, nil, nil])
() -> delete_one([y, cons(y, r), r])
(delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)])}
(delete_one([_tza, _uza, _vza]) /\ delete_one([_tza, _uza, _wza])) -> eq_eltlist([_vza, _wza])
)
(delete_all, F:
{() -> delete_all([x, nil, nil])
(delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)])
(delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza])}
(delete_all([_zza, _aab, _bab]) /\ delete_all([_zza, _aab, _cab])) -> eq_eltlist([_bab, _cab])
)
(count, F:
{() -> count([x, nil, z])
(count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab])
(count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)])}
(count([_fab, _gab, _hab]) /\ count([_fab, _gab, _iab])) -> eq_nat([_hab, _iab])
)
}

properties:
{(count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)])}


over-approximation: {count, delete_all, delete_one}
under-approximation: {}

Clause system for inference is:

{
() -> count([x, nil, z]) -> 0  ;  () -> delete_all([x, nil, nil]) -> 0  ;  () -> delete_one([x, nil, nil]) -> 0  ;  () -> delete_one([y, cons(y, r), r]) -> 0  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 0  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 0  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 0  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 0  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 0  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 0
}


Solving took 0.116232 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.010554 s (model generation: 0.009478,  model checking: 0.001076):

Model:
|_
{
count -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 0  ;  () -> delete_all([x, nil, nil]) -> 0  ;  () -> delete_one([x, nil, nil]) -> 0  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 1  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 1
}
Sat witness: Yes: (() -> delete_one([y, cons(y, r), r]), {
r -> nil  ;  y -> b
})

-------------------------------------------
Step 1, which took 0.010331 s (model generation: 0.009887,  model checking: 0.000444):

Model:
|_
{
count -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 0  ;  () -> delete_all([x, nil, nil]) -> 0  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 1  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 1
}
Sat witness: Yes: (() -> delete_one([x, nil, nil]), {
x -> b
})

-------------------------------------------
Step 2, which took 0.010454 s (model generation: 0.010241,  model checking: 0.000213):

Model:
|_
{
count -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 0  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 1  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 1
}
Sat witness: Yes: (() -> delete_all([x, nil, nil]), {
x -> b
})

-------------------------------------------
Step 3, which took 0.010107 s (model generation: 0.009908,  model checking: 0.000199):

Model:
|_
{
count -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641},
Q_f={q_gen_13641},
Delta=
{
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 1  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 1
}
Sat witness: Yes: (() -> count([x, nil, z]), {
x -> b
})

-------------------------------------------
Step 4, which took 0.014715 s (model generation: 0.014263,  model checking: 0.000452):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642},
Q_f={q_gen_13642},
Delta=
{
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641},
Q_f={q_gen_13641},
Delta=
{
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 1  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]), {
_sza -> nil  ;  r -> nil  ;  x -> b  ;  y -> a
})

-------------------------------------------
Step 5, which took 0.010224 s (model generation: 0.010102,  model checking: 0.000122):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642},
Q_f={q_gen_13642},
Delta=
{
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641},
Q_f={q_gen_13641},
Delta=
{
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639, q_gen_13644, q_gen_13645},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<nil, nil>() -> q_gen_13644
<a, a>() -> q_gen_13645
<b, cons, cons>(q_gen_13645, q_gen_13644) -> q_gen_13637
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 1  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 4  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]), {
_xza -> nil  ;  r -> nil  ;  y -> b
})

-------------------------------------------
Step 6, which took 0.005032 s (model generation: 0.004898,  model checking: 0.000134):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642},
Q_f={q_gen_13642},
Delta=
{
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641, q_gen_13647, q_gen_13648},
Q_f={q_gen_13641},
Delta=
{
<nil>() -> q_gen_13647
<b>() -> q_gen_13648
<b, cons, nil>(q_gen_13648, q_gen_13647) -> q_gen_13641
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639, q_gen_13644, q_gen_13645},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<nil, nil>() -> q_gen_13644
<a, a>() -> q_gen_13645
<b, cons, cons>(q_gen_13645, q_gen_13644) -> q_gen_13637
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 1  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 4  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 4  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]), {
_yza -> nil  ;  r -> nil  ;  x -> b  ;  y -> a
})

-------------------------------------------
Step 7, which took 0.008554 s (model generation: 0.008336,  model checking: 0.000218):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642},
Q_f={q_gen_13642},
Delta=
{
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641, q_gen_13647, q_gen_13648, q_gen_13650, q_gen_13651},
Q_f={q_gen_13641},
Delta=
{
<nil>() -> q_gen_13647
<b>() -> q_gen_13648
<nil, nil>() -> q_gen_13650
<a, a>() -> q_gen_13651
<b, cons, cons>(q_gen_13651, q_gen_13650) -> q_gen_13641
<b, cons, nil>(q_gen_13648, q_gen_13647) -> q_gen_13641
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639, q_gen_13644, q_gen_13645},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<nil, nil>() -> q_gen_13644
<a, a>() -> q_gen_13645
<b, cons, cons>(q_gen_13645, q_gen_13644) -> q_gen_13637
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 1  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 4  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 4  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 4  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]), {
_dab -> z  ;  r -> nil  ;  y -> b
})

-------------------------------------------
Step 8, which took 0.011700 s (model generation: 0.011149,  model checking: 0.000551):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642, q_gen_13653, q_gen_13654},
Q_f={q_gen_13642},
Delta=
{
<nil>() -> q_gen_13653
<b, z>() -> q_gen_13654
<b, cons, s>(q_gen_13654, q_gen_13653) -> q_gen_13642
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641, q_gen_13647, q_gen_13648, q_gen_13650, q_gen_13651},
Q_f={q_gen_13641},
Delta=
{
<nil>() -> q_gen_13647
<b>() -> q_gen_13648
<nil, nil>() -> q_gen_13650
<a, a>() -> q_gen_13651
<b, cons, cons>(q_gen_13651, q_gen_13650) -> q_gen_13641
<b, cons, nil>(q_gen_13648, q_gen_13647) -> q_gen_13641
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639, q_gen_13644, q_gen_13645},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<nil, nil>() -> q_gen_13644
<a, a>() -> q_gen_13645
<b, cons, cons>(q_gen_13645, q_gen_13644) -> q_gen_13637
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 1  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 4  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 4  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 4  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 4  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]), {
_eab -> z  ;  r -> nil  ;  x -> b  ;  y -> a
})

-------------------------------------------
Step 9, which took 0.011740 s (model generation: 0.011381,  model checking: 0.000359):

Model:
|_
{
count -> 
{{{
Q={q_gen_13642, q_gen_13653, q_gen_13654, q_gen_13656},
Q_f={q_gen_13642},
Delta=
{
<nil>() -> q_gen_13653
<a>() -> q_gen_13656
<b, z>() -> q_gen_13654
<b, cons, s>(q_gen_13654, q_gen_13653) -> q_gen_13642
<b, cons, z>(q_gen_13656, q_gen_13653) -> q_gen_13642
<b, nil, z>() -> q_gen_13642
}

Datatype: <elt, eltlist, nat>
Convolution form: left
}}}
  ;  delete_all -> 
{{{
Q={q_gen_13641, q_gen_13647, q_gen_13648, q_gen_13650, q_gen_13651},
Q_f={q_gen_13641},
Delta=
{
<nil>() -> q_gen_13647
<b>() -> q_gen_13648
<nil, nil>() -> q_gen_13650
<a, a>() -> q_gen_13651
<b, cons, cons>(q_gen_13651, q_gen_13650) -> q_gen_13641
<b, cons, nil>(q_gen_13648, q_gen_13647) -> q_gen_13641
<b, nil, nil>() -> q_gen_13641
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}
  ;  delete_one -> 
{{{
Q={q_gen_13637, q_gen_13638, q_gen_13639, q_gen_13644, q_gen_13645},
Q_f={q_gen_13637},
Delta=
{
<nil>() -> q_gen_13638
<b>() -> q_gen_13639
<nil, nil>() -> q_gen_13644
<a, a>() -> q_gen_13645
<b, cons, cons>(q_gen_13645, q_gen_13644) -> q_gen_13637
<b, cons, nil>(q_gen_13639, q_gen_13638) -> q_gen_13637
<b, nil, nil>() -> q_gen_13637
}

Datatype: <elt, eltlist, eltlist>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_elt, eq_eltlist, eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> count([x, nil, z]) -> 3  ;  () -> delete_all([x, nil, nil]) -> 3  ;  () -> delete_one([x, nil, nil]) -> 3  ;  () -> delete_one([y, cons(y, r), r]) -> 3  ;  (count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]) -> 4  ;  (count([x, r, _eab]) /\ not eq_elt([x, y])) -> count([x, cons(y, r), _eab]) -> 4  ;  (count([y, r, _dab])) -> count([y, cons(y, r), s(_dab)]) -> 4  ;  (delete_all([x, r, _yza]) /\ not eq_elt([x, y])) -> delete_all([x, cons(y, r), cons(y, _yza)]) -> 4  ;  (delete_all([y, r, _xza])) -> delete_all([y, cons(y, r), _xza]) -> 4  ;  (delete_one([x, r, _sza]) /\ not eq_elt([x, y])) -> delete_one([x, cons(y, r), cons(y, _sza)]) -> 4
}
Sat witness: Yes: ((count([x, l, _lab]) /\ delete_all([x, l, _kab]) /\ delete_one([x, l, _kab])) -> eq_nat([_lab, s(z)]), {
_kab -> nil  ;  _lab -> z  ;  l -> nil  ;  x -> b
})

Total time: 0.116232
Reason for stopping: Disproved

