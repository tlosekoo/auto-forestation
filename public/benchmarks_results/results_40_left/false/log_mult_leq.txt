Solving ../../../../benchmarks/smtlib/false/mult_leq.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: left


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(leq, P:
{() -> leq([z, s(nn2)])
() -> leq([z, z])
(leq([nn1, nn2])) -> leq([s(nn1), s(nn2)])
(leq([s(nn1), s(nn2)])) -> leq([nn1, nn2])
(leq([s(nn1), z])) -> BOT}
)
(plus, F:
{() -> plus([n, z, n])
(plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)])}
(plus([_zfb, _agb, _bgb]) /\ plus([_zfb, _agb, _cgb])) -> eq_nat([_bgb, _cgb])
)
(mult, F:
{() -> mult([n, z, z])
(mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb])}
(mult([_fgb, _ggb, _hgb]) /\ mult([_fgb, _ggb, _igb])) -> eq_nat([_hgb, _igb])
)
}

properties:
{(mult([n, m, _jgb])) -> leq([n, _jgb])}


over-approximation: {mult, plus}
under-approximation: {leq}

Clause system for inference is:

{
() -> leq([z, s(nn2)]) -> 0  ;  () -> leq([z, z]) -> 0  ;  () -> mult([n, z, z]) -> 0  ;  () -> plus([n, z, n]) -> 0  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 0  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 0  ;  (leq([s(nn1), z])) -> BOT -> 0  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 0  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 0  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 0
}


Solving took 0.154188 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.009998 s (model generation: 0.009484,  model checking: 0.000514):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 0  ;  () -> leq([z, z]) -> 0  ;  () -> mult([n, z, z]) -> 0  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 1  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 1
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> z
})

-------------------------------------------
Step 1, which took 0.009689 s (model generation: 0.009518,  model checking: 0.000171):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677},
Q_f={q_gen_13677},
Delta=
{
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 0  ;  () -> leq([z, z]) -> 0  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 1  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 1
}
Sat witness: Yes: (() -> mult([n, z, z]), {
n -> z
})

-------------------------------------------
Step 2, which took 0.009443 s (model generation: 0.009337,  model checking: 0.000106):

Model:
|_
{
leq -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678},
Q_f={q_gen_13678},
Delta=
{
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677},
Q_f={q_gen_13677},
Delta=
{
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 0  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 1  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 1
}
Sat witness: Yes: (() -> leq([z, z]), {

})

-------------------------------------------
Step 3, which took 0.010162 s (model generation: 0.009545,  model checking: 0.000617):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679},
Q_f={q_gen_13679},
Delta=
{
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678},
Q_f={q_gen_13678},
Delta=
{
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677},
Q_f={q_gen_13677},
Delta=
{
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 1  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 1
}
Sat witness: Yes: (() -> leq([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 4, which took 0.009057 s (model generation: 0.008855,  model checking: 0.000202):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678},
Q_f={q_gen_13678},
Delta=
{
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677},
Q_f={q_gen_13677},
Delta=
{
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 1  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: ((plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]), {
_yfb -> z  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 5, which took 0.010614 s (model generation: 0.010359,  model checking: 0.000255):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678},
Q_f={q_gen_13678},
Delta=
{
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 1  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 1  ;  (leq([s(nn1), z])) -> BOT -> 1  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 1  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: ((mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]), {
_dgb -> z  ;  _egb -> z  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 6, which took 0.010928 s (model generation: 0.010597,  model checking: 0.000331):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 3  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 2  ;  (leq([s(nn1), z])) -> BOT -> 2  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 2  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: ((leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> z
})

-------------------------------------------
Step 7, which took 0.010955 s (model generation: 0.010259,  model checking: 0.000696):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 3  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 3  ;  (leq([s(nn1), z])) -> BOT -> 3  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 3  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: (() -> plus([n, z, n]), {
n -> s(z)
})

-------------------------------------------
Step 8, which took 0.011101 s (model generation: 0.010579,  model checking: 0.000522):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 3  ;  () -> leq([z, z]) -> 3  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 4  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: (() -> mult([n, z, z]), {
n -> s(z)
})

-------------------------------------------
Step 9, which took 0.010626 s (model generation: 0.010470,  model checking: 0.000156):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<s, z, z>(q_gen_13685) -> q_gen_13678
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 6  ;  () -> leq([z, z]) -> 4  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 4  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 4
}
Sat witness: Yes: (() -> leq([z, s(nn2)]), {
nn2 -> s(z)
})

-------------------------------------------
Step 10, which took 0.010492 s (model generation: 0.009835,  model checking: 0.000657):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<s>(q_gen_13681) -> q_gen_13681
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<s, z, z>(q_gen_13685) -> q_gen_13678
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683},
Q_f={q_gen_13677},
Delta=
{
<z, z>() -> q_gen_13683
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 6  ;  () -> leq([z, z]) -> 4  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 4  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 4  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 7
}
Sat witness: Yes: ((plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]), {
_yfb -> s(z)  ;  mm -> z  ;  n -> s(z)
})

-------------------------------------------
Step 11, which took 0.012053 s (model generation: 0.011666,  model checking: 0.000387):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<s>(q_gen_13681) -> q_gen_13681
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<s, z, z>(q_gen_13685) -> q_gen_13678
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683, q_gen_13693},
Q_f={q_gen_13677},
Delta=
{
<z>() -> q_gen_13693
<z, z>() -> q_gen_13683
<s, s, s>(q_gen_13677) -> q_gen_13677
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, s>(q_gen_13693) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 6  ;  () -> leq([z, z]) -> 4  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 4  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 7  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 7
}
Sat witness: Yes: ((mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]), {
_dgb -> z  ;  _egb -> s(z)  ;  mm -> z  ;  n -> z
})

-------------------------------------------
Step 12, which took 0.012131 s (model generation: 0.011982,  model checking: 0.000149):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<s>(q_gen_13681) -> q_gen_13681
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685, q_gen_13695},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<z, z>() -> q_gen_13695
<s, z, z>(q_gen_13685) -> q_gen_13678
<z, s, s>(q_gen_13695) -> q_gen_13678
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683, q_gen_13693},
Q_f={q_gen_13677},
Delta=
{
<z>() -> q_gen_13693
<z, z>() -> q_gen_13683
<s, s, s>(q_gen_13677) -> q_gen_13677
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, s>(q_gen_13693) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 6  ;  () -> leq([z, z]) -> 4  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 4  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 7  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 7  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 7
}
Sat witness: Yes: ((mult([n, m, _jgb])) -> leq([n, _jgb]), {
_jgb -> z  ;  m -> z  ;  n -> s(z)
})

-------------------------------------------
Step 13, which took 0.009792 s (model generation: 0.009756,  model checking: 0.000036):

Model:
|_
{
leq -> 
{{{
Q={q_gen_13679, q_gen_13681},
Q_f={q_gen_13679},
Delta=
{
<s>(q_gen_13681) -> q_gen_13681
<z>() -> q_gen_13681
<s, s>(q_gen_13679) -> q_gen_13679
<s, z>(q_gen_13681) -> q_gen_13679
<z, s>(q_gen_13681) -> q_gen_13679
<z, z>() -> q_gen_13679
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  mult -> 
{{{
Q={q_gen_13678, q_gen_13685, q_gen_13695},
Q_f={q_gen_13678},
Delta=
{
<z>() -> q_gen_13685
<z, z>() -> q_gen_13695
<s, z, z>(q_gen_13685) -> q_gen_13678
<z, s, s>(q_gen_13695) -> q_gen_13678
<z, s, z>(q_gen_13685) -> q_gen_13678
<z, z, z>() -> q_gen_13678
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}
  ;  plus -> 
{{{
Q={q_gen_13677, q_gen_13683, q_gen_13693},
Q_f={q_gen_13677},
Delta=
{
<z>() -> q_gen_13693
<z, z>() -> q_gen_13683
<s, s, s>(q_gen_13677) -> q_gen_13677
<s, z, s>(q_gen_13683) -> q_gen_13677
<z, s, s>(q_gen_13683) -> q_gen_13677
<z, z, s>(q_gen_13693) -> q_gen_13677
<z, z, z>() -> q_gen_13677
}

Datatype: <nat, nat, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> leq([z, s(nn2)]) -> 6  ;  () -> leq([z, z]) -> 4  ;  () -> mult([n, z, z]) -> 6  ;  () -> plus([n, z, n]) -> 6  ;  (leq([nn1, nn2])) -> leq([s(nn1), s(nn2)]) -> 4  ;  (leq([s(nn1), s(nn2)])) -> leq([nn1, nn2]) -> 4  ;  (leq([s(nn1), z])) -> BOT -> 7  ;  (mult([n, m, _jgb])) -> leq([n, _jgb]) -> 7  ;  (mult([n, mm, _dgb]) /\ plus([n, _dgb, _egb])) -> mult([n, s(mm), _egb]) -> 7  ;  (plus([n, mm, _yfb])) -> plus([n, s(mm), s(_yfb)]) -> 7
}
Sat witness: Yes: ((leq([s(nn1), z])) -> BOT, {
nn1 -> z
})

Total time: 0.154188
Reason for stopping: Disproved

