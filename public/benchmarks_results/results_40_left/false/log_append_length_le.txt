Solving ../../../../benchmarks/smtlib/false/append_length_le.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: left


Learning problem is:

env: {
nat -> {s, z}  ;  natlist -> {cons, nil}
}
definition:
{
(append, F:
{() -> append([nil, l2, l2])
(append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)])}
(append([_ujb, _vjb, _wjb]) /\ append([_ujb, _vjb, _xjb])) -> eq_natlist([_wjb, _xjb])
)
(le, P:
{() -> le([z, s(nn2)])
(le([nn1, nn2])) -> le([s(nn1), s(nn2)])
(le([s(nn1), s(nn2)])) -> le([nn1, nn2])
(le([s(nn1), z])) -> BOT
(le([z, z])) -> BOT}
)
(length, F:
{() -> length([nil, z])
(length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)])}
(length([_zjb, _akb]) /\ length([_zjb, _bkb])) -> eq_nat([_akb, _bkb])
)
}

properties:
{(append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb])}


over-approximation: {append, length}
under-approximation: {le}

Clause system for inference is:

{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 0  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 0  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 0  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 0  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 0  ;  (le([s(nn1), z])) -> BOT -> 0  ;  (le([z, z])) -> BOT -> 0  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 0
}


Solving took 0.031044 seconds.
Disproved
-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.003172 s (model generation: 0.002977,  model checking: 0.000195):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 0  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> length([nil, z]), {

})

-------------------------------------------
Step 1, which took 0.002980 s (model generation: 0.002941,  model checking: 0.000039):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590},
Q_f={q_gen_13590},
Delta=
{
<nil, z>() -> q_gen_13590
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 0  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> le([z, s(nn2)]), {
nn2 -> z
})

-------------------------------------------
Step 2, which took 0.003097 s (model generation: 0.003009,  model checking: 0.000088):

Model:
|_
{
append -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<z, s>(q_gen_13592) -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590},
Q_f={q_gen_13590},
Delta=
{
<nil, z>() -> q_gen_13590
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 1
}
Sat witness: Yes: (() -> append([nil, l2, l2]), {
l2 -> nil
})

-------------------------------------------
Step 3, which took 0.003003 s (model generation: 0.002942,  model checking: 0.000061):

Model:
|_
{
append -> 
{{{
Q={q_gen_13593},
Q_f={q_gen_13593},
Delta=
{
<nil, nil, nil>() -> q_gen_13593
}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<z, s>(q_gen_13592) -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590},
Q_f={q_gen_13590},
Delta=
{
<nil, z>() -> q_gen_13590
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 1  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 1  ;  (le([s(nn1), z])) -> BOT -> 1  ;  (le([z, z])) -> BOT -> 1  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]), {
_yjb -> z  ;  ll -> nil  ;  x -> z
})

-------------------------------------------
Step 4, which took 0.003265 s (model generation: 0.003192,  model checking: 0.000073):

Model:
|_
{
append -> 
{{{
Q={q_gen_13593},
Q_f={q_gen_13593},
Delta=
{
<nil, nil, nil>() -> q_gen_13593
}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<z, s>(q_gen_13592) -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590, q_gen_13595, q_gen_13596},
Q_f={q_gen_13590},
Delta=
{
<nil>() -> q_gen_13595
<cons, s>(q_gen_13596, q_gen_13595) -> q_gen_13590
<nil, z>() -> q_gen_13590
<z, z>() -> q_gen_13596
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 1  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((le([nn1, nn2])) -> le([s(nn1), s(nn2)]), {
nn1 -> z  ;  nn2 -> s(z)
})

-------------------------------------------
Step 5, which took 0.003413 s (model generation: 0.003260,  model checking: 0.000153):

Model:
|_
{
append -> 
{{{
Q={q_gen_13593},
Q_f={q_gen_13593},
Delta=
{
<nil, nil, nil>() -> q_gen_13593
}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<s, s>(q_gen_13591) -> q_gen_13591
<z, s>(q_gen_13592) -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590, q_gen_13595, q_gen_13596},
Q_f={q_gen_13590},
Delta=
{
<nil>() -> q_gen_13595
<cons, s>(q_gen_13596, q_gen_13595) -> q_gen_13590
<nil, z>() -> q_gen_13590
<z, z>() -> q_gen_13596
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 1  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]), {
_tjb -> nil  ;  h1 -> z  ;  l2 -> nil  ;  t1 -> nil
})

-------------------------------------------
Step 6, which took 0.003285 s (model generation: 0.003221,  model checking: 0.000064):

Model:
|_
{
append -> 
{{{
Q={q_gen_13593, q_gen_13599, q_gen_13600},
Q_f={q_gen_13593},
Delta=
{
<nil, nil>() -> q_gen_13599
<z, z>() -> q_gen_13600
<cons, nil, cons>(q_gen_13600, q_gen_13599) -> q_gen_13593
<nil, nil, nil>() -> q_gen_13593
}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<s, s>(q_gen_13591) -> q_gen_13591
<z, s>(q_gen_13592) -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590, q_gen_13595, q_gen_13596},
Q_f={q_gen_13590},
Delta=
{
<nil>() -> q_gen_13595
<cons, s>(q_gen_13596, q_gen_13595) -> q_gen_13590
<nil, z>() -> q_gen_13590
<z, z>() -> q_gen_13596
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 4  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 2  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]), {
_ckb -> z  ;  _dkb -> nil  ;  _ekb -> z  ;  l1 -> nil  ;  l2 -> nil
})

-------------------------------------------
Step 7, which took 0.004252 s (model generation: 0.004243,  model checking: 0.000009):

Model:
|_
{
append -> 
{{{
Q={q_gen_13593, q_gen_13599, q_gen_13600},
Q_f={q_gen_13593},
Delta=
{
<nil, nil>() -> q_gen_13599
<z, z>() -> q_gen_13600
<cons, nil, cons>(q_gen_13600, q_gen_13599) -> q_gen_13593
<nil, nil, nil>() -> q_gen_13593
}

Datatype: <natlist, natlist, natlist>
Convolution form: left
}}}
  ;  le -> 
{{{
Q={q_gen_13591, q_gen_13592},
Q_f={q_gen_13591},
Delta=
{
<z>() -> q_gen_13592
<s, s>(q_gen_13591) -> q_gen_13591
<z, s>(q_gen_13592) -> q_gen_13591
<z, z>() -> q_gen_13591
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  length -> 
{{{
Q={q_gen_13590, q_gen_13595, q_gen_13596},
Q_f={q_gen_13590},
Delta=
{
<nil>() -> q_gen_13595
<cons, s>(q_gen_13596, q_gen_13595) -> q_gen_13590
<nil, z>() -> q_gen_13590
<z, z>() -> q_gen_13596
}

Datatype: <natlist, nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat, eq_natlist}
_|

Teacher's answer:
New clause system:
{
() -> append([nil, l2, l2]) -> 3  ;  () -> le([z, s(nn2)]) -> 3  ;  () -> length([nil, z]) -> 3  ;  (append([l1, l2, _dkb]) /\ length([_dkb, _ekb]) /\ length([l1, _ckb])) -> le([_ckb, _ekb]) -> 4  ;  (append([t1, l2, _tjb])) -> append([cons(h1, t1), l2, cons(h1, _tjb)]) -> 4  ;  (le([nn1, nn2])) -> le([s(nn1), s(nn2)]) -> 4  ;  (le([s(nn1), s(nn2)])) -> le([nn1, nn2]) -> 2  ;  (le([s(nn1), z])) -> BOT -> 2  ;  (le([z, z])) -> BOT -> 5  ;  (length([ll, _yjb])) -> length([cons(x, ll), s(_yjb)]) -> 4
}
Sat witness: Yes: ((le([z, z])) -> BOT, {

})

Total time: 0.031044
Reason for stopping: Disproved

