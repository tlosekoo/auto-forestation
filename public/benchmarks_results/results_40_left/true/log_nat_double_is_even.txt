Solving ../../../../benchmarks/smtlib/true/nat_double_is_even.smt2...

Inference procedure has parameters:
Ice fuel: 200
Timeout: 40s
Convolution: left


Learning problem is:

env: {
nat -> {s, z}
}
definition:
{
(double, F:
{() -> double([z, z])
(double([nn, _ej])) -> double([s(nn), s(s(_ej))])}
(double([_fj, _gj]) /\ double([_fj, _hj])) -> eq_nat([_gj, _hj])
)
(is_even, P:
{() -> is_even([z])
(is_even([n3])) -> is_even([s(s(n3))])
(is_even([s(s(n3))])) -> is_even([n3])
(is_even([s(z)])) -> BOT}
)
}

properties:
{(double([n, _ij])) -> is_even([_ij])}


over-approximation: {double}
under-approximation: {is_even}

Clause system for inference is:

{
() -> double([z, z]) -> 0  ;  () -> is_even([z]) -> 0  ;  (double([n, _ij])) -> is_even([_ij]) -> 0  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 0  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 0  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 0  ;  (is_even([s(z)])) -> BOT -> 0
}


Solving took 0.047189 seconds.
Proved
Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8954, q_gen_8955, q_gen_8960},
Q_f={q_gen_8950},
Delta=
{
<s>(q_gen_8960) -> q_gen_8955
<z>() -> q_gen_8955
<s>(q_gen_8955) -> q_gen_8960
<s, s>(q_gen_8954) -> q_gen_8950
<z, s>(q_gen_8960) -> q_gen_8950
<z, z>() -> q_gen_8950
<s, s>(q_gen_8950) -> q_gen_8954
<z, s>(q_gen_8955) -> q_gen_8954
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

-------------------
STEPS:
-------------------------------------------
Step 0, which took 0.003686 s (model generation: 0.003638,  model checking: 0.000048):

Model:
|_
{
double -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 0  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 1  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 1  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 1  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 1  ;  (is_even([s(z)])) -> BOT -> 1
}
Sat witness: Yes: (() -> is_even([z]), {

})

-------------------------------------------
Step 1, which took 0.003241 s (model generation: 0.003211,  model checking: 0.000030):

Model:
|_
{
double -> 
{{{
Q={},
Q_f={},
Delta=
{

}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949},
Q_f={q_gen_8949},
Delta=
{
<z>() -> q_gen_8949
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 3  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 1  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 1  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 1  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 1  ;  (is_even([s(z)])) -> BOT -> 1
}
Sat witness: Yes: (() -> double([z, z]), {

})

-------------------------------------------
Step 2, which took 0.003385 s (model generation: 0.003301,  model checking: 0.000084):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950},
Q_f={q_gen_8950},
Delta=
{
<z, z>() -> q_gen_8950
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949},
Q_f={q_gen_8949},
Delta=
{
<z>() -> q_gen_8949
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 3  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 1  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 1  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 4  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 2  ;  (is_even([s(z)])) -> BOT -> 2
}
Sat witness: Yes: ((is_even([n3])) -> is_even([s(s(n3))]), {
n3 -> z
})

-------------------------------------------
Step 3, which took 0.003300 s (model generation: 0.003245,  model checking: 0.000055):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950},
Q_f={q_gen_8950},
Delta=
{
<z, z>() -> q_gen_8950
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8949) -> q_gen_8949
<z>() -> q_gen_8949
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 3  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 1  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 4  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 4  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 2  ;  (is_even([s(z)])) -> BOT -> 2
}
Sat witness: Yes: ((double([nn, _ej])) -> double([s(nn), s(s(_ej))]), {
_ej -> z  ;  nn -> z
})

-------------------------------------------
Step 4, which took 0.004088 s (model generation: 0.004058,  model checking: 0.000030):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8955},
Q_f={q_gen_8950},
Delta=
{
<z>() -> q_gen_8955
<s, s>(q_gen_8950) -> q_gen_8950
<z, s>(q_gen_8955) -> q_gen_8950
<z, z>() -> q_gen_8950
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8949) -> q_gen_8949
<z>() -> q_gen_8949
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 3  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 2  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 4  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 4  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 2  ;  (is_even([s(z)])) -> BOT -> 5
}
Sat witness: Yes: ((is_even([s(z)])) -> BOT, {

})

-------------------------------------------
Step 5, which took 0.003560 s (model generation: 0.003486,  model checking: 0.000074):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8955},
Q_f={q_gen_8950},
Delta=
{
<z>() -> q_gen_8955
<s, s>(q_gen_8950) -> q_gen_8950
<z, s>(q_gen_8955) -> q_gen_8950
<z, z>() -> q_gen_8950
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 3  ;  () -> is_even([z]) -> 3  ;  (double([n, _ij])) -> is_even([_ij]) -> 5  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 4  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 4  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 3  ;  (is_even([s(z)])) -> BOT -> 5
}
Sat witness: Yes: ((double([n, _ij])) -> is_even([_ij]), {
_ij -> s(z)  ;  n -> s(z)
})

-------------------------------------------
Step 6, which took 0.005560 s (model generation: 0.005382,  model checking: 0.000178):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8954, q_gen_8955},
Q_f={q_gen_8950},
Delta=
{
<z>() -> q_gen_8955
<s, s>(q_gen_8954) -> q_gen_8950
<z, z>() -> q_gen_8950
<s, s>(q_gen_8950) -> q_gen_8954
<z, s>(q_gen_8955) -> q_gen_8954
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 4  ;  () -> is_even([z]) -> 4  ;  (double([n, _ij])) -> is_even([_ij]) -> 5  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 7  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 5  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 4  ;  (is_even([s(z)])) -> BOT -> 5
}
Sat witness: Yes: ((double([nn, _ej])) -> double([s(nn), s(s(_ej))]), {
_ej -> s(s(z))  ;  nn -> s(z)
})

-------------------------------------------
Step 7, which took 0.004876 s (model generation: 0.004027,  model checking: 0.000849):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8954, q_gen_8955, q_gen_8960},
Q_f={q_gen_8950},
Delta=
{
<z>() -> q_gen_8955
<s>(q_gen_8955) -> q_gen_8960
<s, s>(q_gen_8954) -> q_gen_8950
<z, s>(q_gen_8960) -> q_gen_8950
<z, z>() -> q_gen_8950
<s, s>(q_gen_8950) -> q_gen_8954
<z, s>(q_gen_8955) -> q_gen_8954
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 5  ;  () -> is_even([z]) -> 5  ;  (double([n, _ij])) -> is_even([_ij]) -> 6  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 10  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 6  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 5  ;  (is_even([s(z)])) -> BOT -> 6
}
Sat witness: Yes: ((double([nn, _ej])) -> double([s(nn), s(s(_ej))]), {
_ej -> s(s(z))  ;  nn -> z
})

-------------------------------------------
Step 8, which took 0.004530 s (model generation: 0.004387,  model checking: 0.000143):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8953, q_gen_8955, q_gen_8956},
Q_f={q_gen_8950, q_gen_8953},
Delta=
{
<s>(q_gen_8955) -> q_gen_8955
<z>() -> q_gen_8955
<z, z>() -> q_gen_8950
<s, s>(q_gen_8953) -> q_gen_8953
<z, s>(q_gen_8955) -> q_gen_8953
<s, s>(q_gen_8950) -> q_gen_8956
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 6  ;  () -> is_even([z]) -> 6  ;  (double([n, _ij])) -> is_even([_ij]) -> 9  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 10  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 7  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 6  ;  (is_even([s(z)])) -> BOT -> 7
}
Sat witness: Yes: ((double([n, _ij])) -> is_even([_ij]), {
_ij -> s(z)  ;  n -> z
})

-------------------------------------------
Step 9, which took 0.005294 s (model generation: 0.004645,  model checking: 0.000649):

Model:
|_
{
double -> 
{{{
Q={q_gen_8950, q_gen_8953, q_gen_8954, q_gen_8955},
Q_f={q_gen_8950, q_gen_8953},
Delta=
{
<s>(q_gen_8955) -> q_gen_8955
<z>() -> q_gen_8955
<z, z>() -> q_gen_8950
<s, s>(q_gen_8953) -> q_gen_8953
<s, s>(q_gen_8954) -> q_gen_8953
<s, s>(q_gen_8950) -> q_gen_8954
<z, s>(q_gen_8955) -> q_gen_8954
}

Datatype: <nat, nat>
Convolution form: left
}}}
  ;  is_even -> 
{{{
Q={q_gen_8949, q_gen_8952},
Q_f={q_gen_8949},
Delta=
{
<s>(q_gen_8952) -> q_gen_8949
<z>() -> q_gen_8949
<s>(q_gen_8949) -> q_gen_8952
}

Datatype: <nat>
Convolution form: left
}}}

}
--
Equality automata are defined for: {eq_nat}
_|

Teacher's answer:
New clause system:
{
() -> double([z, z]) -> 7  ;  () -> is_even([z]) -> 7  ;  (double([n, _ij])) -> is_even([_ij]) -> 12  ;  (double([nn, _ej])) -> double([s(nn), s(s(_ej))]) -> 10  ;  (is_even([n3])) -> is_even([s(s(n3))]) -> 8  ;  (is_even([s(s(n3))])) -> is_even([n3]) -> 7  ;  (is_even([s(z)])) -> BOT -> 8
}
Sat witness: Yes: ((double([n, _ij])) -> is_even([_ij]), {
_ij -> s(s(s(z)))  ;  n -> s(s(s(z)))
})

Total time: 0.047189
Reason for stopping: Proved

