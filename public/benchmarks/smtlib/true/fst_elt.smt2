(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0) (epair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(epair (first elt) (second elt))
		)
	)
)

(define-fun fst_elt ((ee epair)) elt
	(
		match ee
		(
			((epair e1 e2) e1)
		)
	)
)


(assert (forall ((e1 elt) (e2 elt))
				(= (fst_elt (epair e1 e2)) e1)
		)
)


(check-sat)
