(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
;;			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec le ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
;;						((s m) (and (shallower t1 m) (shallower t2 m)))
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)

(assert 
	(forall 
		((n nat) (t1 etree)(t2 etree)(e elt))
		(=> (shallower (node e t1 t2) (s n)) (shallower t1 n)))
)

(assert 
	(forall 
		((n nat) (t1 etree)(t2 etree)(e elt))
		(=> (shallower (node e t1 t2) (s n)) (shallower t2 n)))
)


(check-sat)
