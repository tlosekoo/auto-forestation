(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0) (npair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(npair (first nat) (second nat))
		)
	)
)

(define-fun fst_nat ((p npair)) nat
	(
		match p
		(
			((npair n1 n2) n1)
		)
	)
)


(assert (forall ((n1 nat) (n2 nat))
				(= (fst_nat (npair n1 n2)) n1)
		)
)


(check-sat)
