
(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec take ((x nat) (y eltlist)) eltlist
  (match x
    ((z nil)
     ((s u)
      (match y
        ((nil nil)
         ((cons x2 x3) (cons x2 (take u x3)))))))))


(assert (forall ((n nat)(x elt)(xs eltlist)) 
      (= (take (s n) (cons x xs)) (cons x (take n xs)))))
      
(check-sat)
