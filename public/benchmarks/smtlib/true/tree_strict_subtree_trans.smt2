(set-logic HORN)


(declare-datatypes ((nat 0) (nattree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(leaf)
			(node (hd nat) (ls nattree) (rs nattree))
		)
	)
)



(define-fun-rec strict_subtree ((s nattree) (t nattree)) Bool
	(
		match s
		(
			(leaf 
				(
					match t
					(
						(leaf false)
						((node eb tb1 tb2) true)
					)
				)
			)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (strict_subtree ta1 tb1) (strict_subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)


(assert (forall ((t1 nattree) (t2 nattree) (t3 nattree))
			(=> (and (strict_subtree t1 t2) (strict_subtree t2 t3))
				(strict_subtree t1 t3))))


(check-sat)
