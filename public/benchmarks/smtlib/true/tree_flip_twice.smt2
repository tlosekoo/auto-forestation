(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec flip ((t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)



(assert 
	(forall ((t etree))
		(= (flip (flip t)) t)
	)
)

(check-sat)
