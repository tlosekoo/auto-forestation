(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (nlist 0) (elist 0) (etree 0) (epair 0) (npair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nnil)
			(ncons (nhd nat) (ntl nlist))
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
		(
			(epairc (efst elt) (esnd elt))
		)
		(
			(npairc (nfst nat) (nsnd nat))
		)
	)
)
(define-fun swap_elt ((p epair)) epair
	(
		match p 
		(
			((epairc fst snd) (epairc snd fst))
		)
	)
)


(assert
	(forall ((p epair))
		(= p (swap_elt (swap_elt p)))
	)
)

(check-sat)