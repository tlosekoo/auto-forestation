(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)


(define-fun-rec length_elt ((l elist)) nat
	(
		match l 
		(
			(  enil    z)
			( (econs x ll) (s (length_elt ll)))
		)
	)
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(define-fun-rec delete_all_elt ((x elt) (l elist)) elist
  (match l
  (
    (enil enil)
	((econs h t) (ite (= x h) (delete_all_elt x t) (econs h (delete_all_elt x t))))
  )
  )
)

(define-fun-rec count_elt ((x elt) (l elist)) nat
	(
		match l
		(
			(enil z)
			((econs h1 t1) (ite (= h1 x) (s (count_elt x t1)) (count_elt x t1)))
		)
	)
)


(assert 
	(forall ((l elist) (e elt))
				(= (length_elt (delete_all_elt e l)) (minus (length_elt l) (count_elt e l)))
	)
)


(check-sat)
