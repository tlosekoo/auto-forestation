(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)

(define-fun-rec take_elt ((x nat) (y elist)) elist
  (match x
    ((z enil)
     ((s u)
      (match y
        ((enil enil)
         ((econs x2 x3) (econs x2 (take_elt u x3)))))))))



(define-fun-rec length_elt ((l elist)) nat
	(
		match l 
		(
			(  enil    z)
			( (econs x ll) (s (length_elt ll)))
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(assert 
	(forall ((l elist) (n nat))
		(=> (leq_nat n (length_elt l))
				(= (length_elt (take_elt n l)) n))
	)
)



(check-sat)
