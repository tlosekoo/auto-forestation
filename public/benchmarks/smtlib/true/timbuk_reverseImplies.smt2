(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)


(define-fun-rec leq ((x elt)(y elt)) Bool
	(
		match x
		(
			(a true)
			(b 
				(
					match y 
					(
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec reverse ((l1 eltlist)) eltlist
	(
		match l1
		(
			(nil nil)
			((cons h1 t1) (append (reverse t1) (cons h1 nil)))
		)
	)
)

(define-fun-rec insert ((x elt) (l eltlist)) eltlist
	(
		match l
		(
			(nil (cons x nil))
			((cons y z) (ite (leq x y) (cons x (cons y z)) (cons y (insert x z))))
		)
	)
)

(define-fun-rec sort ((l eltlist)) eltlist
	(
		match l
		(
			(nil nil)
			((cons y z) (insert y (sort z))
		)
	)
))

(define-fun-rec sorted ((l eltlist)) Bool
	(
		match l 
		(
			(  nil    true)
			( (cons x xs) 
				(
					match xs 
					(
						(nil true)
						((cons y xs) (ite (leq x y) (sorted (cons y xs)) false))
					)
				)
			)
		)
	)
)


(assert
	(forall ((l eltlist)) 
		(=> (sorted l) (sorted (reverse (reverse l))))
	)
)

(check-sat)