(set-logic HORN)

(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)

(assert
	(forall ((n nat) (l natlist)) (=> (= (cons n l) l) false))
)

(check-sat)
