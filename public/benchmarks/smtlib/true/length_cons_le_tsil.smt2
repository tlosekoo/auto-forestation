(set-logic HORN)

; better not use right-convolution!
(declare-datatypes ((nat 0) (nattsil 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(snoc (tl nattsil) (hd nat))
		)
	)
)

(define-fun-rec le ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec length ((l nattsil)) nat
	(
		match l 
		(
			(  nil    z)
			( (snoc ll x) (s (length ll)))
		)
	)
)




(assert (forall ((x nat) (l nattsil))
			(le (length l) (length (snoc l x)))))

(check-sat)

