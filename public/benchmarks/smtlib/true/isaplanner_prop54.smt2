(set-logic HORN)


(declare-datatypes ((nat 0))
	(
		(
			(z) 
			(s (pred nat))
		)
	)
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(assert (forall ((n nat)(m nat))
			(= (minus (plus m n) n) m)))

(check-sat)
