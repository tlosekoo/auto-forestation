(set-logic HORN)

(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)


(assert
	(forall ((l1 natlist)(l2 natlist)(n1 nat)(n2 nat)) 
		(=> (= (cons n1 l1) (cons n2 l2)) 
			(= l1 l2))))
(assert
	(forall ((l1 natlist)(l2 natlist)(n1 nat)(n2 nat)) 
		(=> (= (cons n1 l1) (cons n2 l2)) 
			(= n1 n2))))


(check-sat)
