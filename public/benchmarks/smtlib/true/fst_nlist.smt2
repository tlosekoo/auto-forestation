(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (nlist 0) (nlpair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nnil)
			(ncons (nhd nat) (ntl nlist))
		)
		(
			(nlpair (first nlist) (second nlist))
		)
	)
)

(define-fun fst_nlist ((p nlpair)) nlist
	(
		match p
		(
			((nlpair l1 l2) l1)
		)
	)
)


(assert (forall ((l1 nlist) (l2 nlist))
				(= (fst_nlist (nlpair l1 l2)) l1)
		)
)


(check-sat)
