(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)

(define-fun-rec prefix_elt ((p elist) (l elist)) Bool
  (match p
  	(
    	(enil true)
		((econs z zs)
      	   (match l
			  (
        		(enil false)
	    		((econs y2 ys)
		          (ite
        		    (= z y2) (prefix_elt zs ys) false))
			  )
			)
		)
	)
  )
)

(assert 
	(forall ((l elist))
		(prefix_elt enil l)
	)
)


(check-sat)
