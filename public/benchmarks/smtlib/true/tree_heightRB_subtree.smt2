(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (subtree ta1 tb1) (subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)

(define-fun-rec height_rb ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (height_rb t2))
			)
		)
	)
)

(assert (forall ((t1 etree) (t2 etree))
		(=> (subtree t1 t2)
			(leq_nat (height_rb t1) (height_rb t2)))))


(check-sat)
