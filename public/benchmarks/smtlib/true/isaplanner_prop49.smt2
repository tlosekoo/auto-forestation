(set-logic HORN)


(declare-datatypes ((elt 0) (eltlist 0)) 
	(
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec butlast ((x eltlist)) eltlist
  (match x
    ((nil nil)
     ((cons y z)
      (match z
        ((nil nil)
         ((cons x2 x3) (cons y (butlast (cons x2 x3))))))))))


(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)
(define-fun butlastconcat ((x eltlist) (y eltlist)) eltlist
  (match y
    ((nil (butlast x))
     ((cons z x2) (append x (butlast (cons z x2)))))))


(assert (forall ((xs eltlist) (ys eltlist))
      (= (butlast (append xs ys)) (butlastconcat xs ys))))

(check-sat)
