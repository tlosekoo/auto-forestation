(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
	)
)


(define-fun-rec mem_elt ((e elt) (l elist)) Bool 
	(
		match l
		(
			(enil false)
			((econs h t) (ite (= e h) true (mem_elt e t)))
		)
	)
)


(define-fun-rec count_elt ((x elt) (l elist)) nat
	(
		match l
		(
			(enil z)
			((econs h1 t1) (ite (= h1 x) (s (count_elt x t1)) (count_elt x t1)))
		)
	)
)


(assert 
	(forall ((l elist) (e elt))
		(=> 
			(and (mem_elt e l) (= (count_elt e l) z))
			false
		)
	)
)


(check-sat)
