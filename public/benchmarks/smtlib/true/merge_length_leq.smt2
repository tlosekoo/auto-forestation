(set-logic HORN)


(declare-datatypes ((nat 0)(elt 0) (eltlist 0)) 
	(
		(
			(z)
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec leq ((n1 elt) (n2 elt)) Bool
	(
		match n1
		(
			(a true)
			(b (
					match n2
					(
						(a false)
						(b true)
					)
			    )
			)
		)
	)
)

(define-fun-rec leqnat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leqnat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec merge ((x eltlist) (y eltlist)) eltlist
  (match x
    ((nil y)
     ((cons z xs)
      (match y
        ((nil (cons z xs))
         ((cons y2 ys)
          (ite
            (leq z y2) 
			(cons z (merge xs (cons y2 ys)))
            (cons y2 (merge (cons z xs) ys))))))))))


(define-fun-rec append ((l1 eltlist) (l2 eltlist)) eltlist 
	(
		match l1
		(
			(nil l2)
			((cons h1 t1) (cons h1 (append t1 l2)))
		)
	)
)

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)


(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)

(assert (forall ((l1 eltlist) (l2 eltlist))
			(leqnat (length l1) (length (merge l1 l2)))))

(assert (forall ((l1 eltlist) (l2 eltlist))
			(leqnat (length l2) (length (merge l1 l2)))))

(check-sat)
