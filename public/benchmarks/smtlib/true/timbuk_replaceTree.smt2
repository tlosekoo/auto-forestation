(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec member ((e elt)(t etree)) Bool
	(
		match t
		(
			(leaf false)
			((node e2 t1 t2) (ite (= e e2) true (ite (member e t1) true (member e t2))))
		)
	)
)

(define-fun-rec replace ((e1 elt) (e2 elt) (t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e3 t1 t2) (ite (= e1 e3) (node e2 (replace e1 e2 t1)
											         (replace e1 e2 t2))
											(node e3 (replace e1 e2 t1)
													 (replace e1 e2 t2))))
		)
	)
)

(assert (forall ((t1 etree))
			(=> (member a (replace a b t1)) false)))


(check-sat)
