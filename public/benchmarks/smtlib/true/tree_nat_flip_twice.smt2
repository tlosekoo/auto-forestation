(set-logic HORN)


(declare-datatypes ((nat 0) (nattree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(leaf)
			(node (hd nat) (ls nattree) (rs nattree))
		)
	)
)

(define-fun-rec flip ((t nattree)) nattree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)



(assert 
	(forall ((t nattree))
		(= (flip (flip t)) t)
	)
)

(check-sat)
