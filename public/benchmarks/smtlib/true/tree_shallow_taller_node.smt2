(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						(z false)
						((s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec max ((n nat)(m nat)) nat
	(ite (le_nat n m) m n))


(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)


(define-fun-rec taller ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf 
				(
					match n 
					(
						(z true)
						((s m) false)
					)
				)
			)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z true)
						((s m) (ite (taller t1 m) true (ite (taller t2 m) true false)))
					)
				)
			)
		)
	)
)

(assert (forall ((e elt)(t1 etree) (t2 etree) (n1 nat) (n2 nat))
			(=> (and (taller t1 n1) (taller t2 n2))
				(taller (node e t1 t2) (s (max n1 n2))))))

(assert (forall ((e elt)(t1 etree) (t2 etree) (n1 nat) (n2 nat))
			(=> (and  (shallower t1 n1) (shallower t2 n2))
				(shallower (node e t1 t2) (s (max n1 n2))))))

(check-sat)
