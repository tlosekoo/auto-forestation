(set-logic HORN)


(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)

(define-fun-rec mult ((n nat) (m nat)) nat 
	(
		match m
		(
			(z z)
			((s mm) (plus n (mult n mm)))
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z true)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec  max_ite
  ((x nat) (y nat)) nat
  (ite (leq_nat x y) y x)
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(define-fun-rec  min_ite
  ((x nat) (y nat)) nat
  (ite (leq_nat x y) x y)
)



(assert
	(forall ((n1 nat) (n2 nat)) 
		(= (min_ite n1 n2) (minus (plus n1 n2) (max_ite n1 n2)))
		)
)

(check-sat)