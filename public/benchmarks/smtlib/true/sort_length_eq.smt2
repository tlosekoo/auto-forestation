(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec leq ((x elt)(y elt)) Bool
	(
		match x
		(
			(a true)
			(b 
				(
					match y 
					(
						(a false)
						(b true)
					)
				)
			)
		)
	)
)

(define-fun-rec insert ((x elt) (l eltlist)) eltlist
	(
		match l
		(
			(nil (cons x nil))
			((cons y z) (ite (leq x y) (cons x (cons y z)) (cons y (insert x z))))
		)
	)
)

(define-fun-rec sort ((l eltlist)) eltlist
	(
		match l
		(
			(nil nil)
			((cons y z) (insert y (sort z))
		)
	)
))

(define-fun-rec length ((l eltlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)


(assert
	(forall ((l eltlist)) 
		(= (length l) (length (sort l)))
	)
)

(check-sat)