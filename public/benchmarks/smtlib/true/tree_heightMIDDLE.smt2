(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0) (direction 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
		(
			(left)
			(right)
		)
	)
)

(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec height_mid ((t etree)(d direction)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(
					match d
					(
						(left (s (height_mid t1 right)))
						(right (s (height_mid t2 left)))
					)
				)
			)
		)
	)
)

(define-fun-rec flip ((t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)

(assert (forall ((t1 etree))
		(= (height_mid t1 left) (height_mid (flip t1) right))))


(check-sat)
