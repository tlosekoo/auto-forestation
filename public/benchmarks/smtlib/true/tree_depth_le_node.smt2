(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec le ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						( z false)
						( ( s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le nn1 nn2))
					)
				)
			)
		)
	)
)

(define-fun-rec leq ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec max ((n nat)(m nat)) nat
	(ite (leq n m) m n))


(define-fun-rec depth ((t etree)) nat
	(
		match t
		(
			(leaf z)
			((node e t1 t2) (s (max (depth t1) (depth t2))))
		)
	)
)

(assert (forall ((e elt) (t1 etree) (t2 etree))
			(le (depth t1) (depth (node e t1 t2)))))


(check-sat)
