(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)

(define-fun-rec flip ((t etree)) etree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (flip t2) (flip t1)))
		)
	)
)


(define-fun-rec memt_elt ((e elt)(t etree)) Bool
	(
		match t
		(
			(leaf false)
			((node e2 t1 t2) (ite (= e e2) true (ite (memt_elt e t1) true (memt_elt e t2))))
		)
	)
)

(assert 
	(forall ((e elt) (t etree))
		(=> (memt_elt e t) (memt_elt e (flip t)))
	)
)

(check-sat)
