(set-logic HORN)

(declare-datatypes ((nat 0))
	(
		(
			(z) 
			(s (pred nat))
		)
  )
)

(define-fun-rec
  max
  ((x nat) (y nat)) nat
  (match x
    ((z y)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (s (max u x2)))))))))

(assert
    (forall ((a nat) (b nat) (c nat))
      (= (max (max a b) c) (max a (max b c))))
)

(check-sat)