(set-logic HORN)


(declare-datatypes ((nat 0))
	(
		(
			(z) 
			(s (pred nat))
		)
	)
)

(define-fun-rec minus ((x nat) (y nat)) nat
  (match x
    ((z z)
     ((s u)
      (match y
        ((z (s u))
         ((s x2) (minus u x2))))))))

(assert (forall ((n nat)(m nat)(k nat))
			(= (minus (minus (s m) n) (s k)) (minus (minus m n) k))))

(check-sat)
