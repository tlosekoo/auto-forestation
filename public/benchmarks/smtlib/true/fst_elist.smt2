(set-logic HORN)

(declare-datatypes ((nat 0) (elt 0) (elist 0) (elpair 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(enil)
			(econs (ehd elt) (etl elist))
		)
		(
			(elpair (first elist) (second elist))
		)
	)
)

(define-fun fst_elist ((p elpair)) elist
	(
		match p
		(
			((elpair l1 l2) l1)
		)
	)
)


(assert (forall ((l1 elist) (l2 elist))
				(= (fst_elist (elpair l1 l2)) l1)
		)
)


(check-sat)
