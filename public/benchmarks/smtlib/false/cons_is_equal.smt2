(set-logic HORN)

(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)


(assert
	(forall ((x natlist)(y nat)) (= x (cons y x)))
)

(check-sat)
