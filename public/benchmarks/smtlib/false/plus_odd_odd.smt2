(set-logic HORN)

(declare-datatypes ((nat 0)) (((z) (s (pred nat)))))

(define-fun-rec is_odd ((n nat)) Bool 
	(
		match n
		(
			(z false)
			((s nn) (
						match nn
						(
							(z true)
							((s n3) (is_odd n3))
						)
					)
			)
		)
	)
)

(define-fun-rec plus ((n nat) (m nat)) nat 
	(
		match m
		(
			(z n)
			((s mm) (s (plus n mm)))
		)
	)
)


; Property (false) 
; odd x /\ odd y --> odd(plus(x,y))
(assert (forall ((x nat)(y nat)) 
    (=> (and (is_odd x) (is_odd y))
        (is_odd (plus x y)))))

(check-sat)