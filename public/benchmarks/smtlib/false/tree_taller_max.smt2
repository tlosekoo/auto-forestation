(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						(z false)
						((s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)



(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (subtree ta1 tb1) (subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)

(define-fun max ((n nat)(m nat)) nat
	(ite (le_nat n m) m n))

(define-fun min ((n nat)(m nat)) nat
	(ite (le_nat n m) n m))

(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(ite (le_nat (height t1) (height t2)) (s (height t2)) (s (height t1)))
				;;(s (max (height t1) (height t2)))
			)
		)
	)
)

(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)


(define-fun-rec taller ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf 
				(
					match n 
					(
						(z true)
						((s m) false)
					)
				)
			)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z true)
						((s m) (ite (taller t1 m) true (ite (taller t2 m) true false)))
					)
				)
			)
		)
	)
)


(assert (forall ((e elt) (t1 etree) (n1 nat) (t2 etree) (n2 nat) (n3 nat))
			(=> (and (taller t1 n1) (taller t2 n2))
				(taller (node e t1 t2) (s(s(max n1 n2)))))))


(check-sat)
