(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec le_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z 
				(
					match n2
					(
						(z false)
						((s nn2) true)
					)
				)
			)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (le_nat nn1 nn2))
					)
				)
			)
		)
	)
)



(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (subtree ta1 tb1) (subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)

(define-fun-rec max ((n nat)(m nat)) nat
	(ite (le_nat n m) m n))


(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(ite (le_nat (height t1) (height t2)) (s (height t2)) (s (height t1)))
			)
		)
	)
)

(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)

(assert (forall ((t1 etree) (t2 etree) (n1 nat) (n2 nat))
			(=> (and (subtree t1 t2) (shallower t1 n1) (shallower t2 n2))
				(leq_nat n1 n2))))


(check-sat)
