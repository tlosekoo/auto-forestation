(set-logic HORN)


(declare-datatypes ((nat 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
	)
)

(assert (forall ((i nat) (j nat))
		(= i j)))

(check-sat)
