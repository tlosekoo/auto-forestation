(set-logic HORN)


(declare-datatypes ((nat 0)(elt 0) (eltlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a) 
			(b)
		)
		(
			(nil)
			(cons (hd elt) (tl eltlist))
		)
	)
)

(define-fun-rec drop ((x nat) (l eltlist)) eltlist
  (match x
    (
		(z l)
     	((s u)
	      (match l
		  	(
		        (nil nil)
		        ((cons x2 x3) (drop u x3))
			)
		  )
		)
	)
  )
)

(assert (forall ((l1 eltlist))
			(=> (= l1 (drop z l1)) false)))

(check-sat)
