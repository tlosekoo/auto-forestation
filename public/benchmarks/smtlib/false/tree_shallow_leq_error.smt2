(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)



(define-fun-rec leq_nat ((n1 nat) (n2 nat)) Bool
	(
		match n1
		(
			(z true)
			( (s nn1) 
				(
					match n2
					( 
						(z false)
						((s nn2) (leq_nat nn1 nn2))
					)
				)
			)
		)
	)
)


(define-fun-rec max ((n nat)(m nat)) nat
	(ite (leq_nat n m) m n))


(define-fun-rec height ((t etree)) nat
	(
		match t
		(
			(leaf z)
			(
				(node e t1 t2) 
				(s (ite (leq_nat (height t1) (height t2)) (height t2) (height t1)))
			)
		)
	)
)

(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)

(define-fun-rec height_rel ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf 
				(
					match n
					(
						(z true)
						((s nn) false)
					)
				)
			)
			(
				(node e t1 t2) 
				(
					match n
					(
						(z false)
						((s nn) (ite (height_rel t1 nn) (shallower t1 nn) (ite (height_rel t2 nn) (shallower t2 nn) false)))
					)
				)
			)
		)
	)
)



(assert (forall ((e elt) (t1 etree) (t2 etree) (n nat) (m nat) (u nat))
			(=> (and (height_rel (node e t1 t2) u) 
					 (height_rel t1 n) 
					(height_rel t2 m))
				(= u (s (max n m)))
			)
)
)


(check-sat)
