open Misc
module Map_char = My_map.Make (Char)
module Map_char_int = My_map.Make2 (Char) (Int)
module Map_int = My_map.Make (Int)
module Set_char = My_set.Make (Char)
module Set_int = My_set.Make (Int)

let pp_int c i = Format.fprintf c "%n" i
let pp_char c ch = Format.fprintf c "%c" ch
let union_int = Map_char.union_if_compatible Int.equal
let x = 'x'
let l = 'l'
let n = 'n'
let map_1 = Map_char.of_list [(x, 1); (l, 1)]
let map_2 = Map_char.of_seq (List.to_seq [(x, 1); (n, 3)])
let map_3 = Map_char.of_seq (List.to_seq [(x, 1); (l, 3)])
let map_12 = Map_char.of_list [(x, 1); (l, 1); (n, 3)]
let map_1x = Map_char.of_seq (List.to_seq [(x, 1)])
let map_1l = Map_char.of_seq (List.to_seq [(l, 1)])

let%expect_test "pp map 12" =
  Map_char.pp pp_int Format.std_formatter map_12;
  [%expect "
 {
 l -> 1  ;  n -> 3  ;  x -> 1
 }"]

let%test "make_union 1" = Map_char.equal Int.equal map_12 (union_int map_1 map_2)

let%test "make_union 2" =
  try
    let _ = union_int map_1 map_3 in
    false
  with
  | _ -> true

let%test "project_in" =
  Map_char.equal Int.equal map_1x (Map_char.project_in (Set_char.of_list [x; n]) map_1)

let%test "project_out" =
  Map_char.equal Int.equal map_1l (Map_char.project_out (Set_char.of_list [x; n]) map_1)

let%test "update as in set success" =
  try
    Map_char.equal Int.equal (Map_char.add_compatible_binding Int.equal l 1 map_1) map_1 = true
  with
  | _ -> false

let%test "update as in set success 1" =
  try
    Map_char.equal Int.equal (Map_char.add_compatible_binding Int.equal 'o' 1 map_1) map_1 = false
  with
  | _ -> false

let%test "update as in set failure" =
  try Map_char.equal Int.equal (Map_char.add_compatible_binding Int.equal l 8 map_1) map_1 with
  | _ -> true

let%test "domain" = Set_char.equal (Map_char.domain map_12) (Set_char.of_list [x; l; n])

let%test "list_union 1" =
  Map_char.equal Int.equal (Map_char.list_union_if_compatible Int.equal []) Map_char.empty

let%test "list_union 2" =
  Map_char_int.equal (Map_char.list_union_if_compatible Int.equal [map_1]) map_1

let%test "list_union 3" =
  Map_char.equal Int.equal (Map_char.list_union_if_compatible Int.equal [map_1; map_2]) map_12

let%test "list_union fail" =
  try
    let _ = Map_char.list_union_if_compatible Int.equal [map_1; map_3] in
    false
  with
  | _ -> true

let%test "contains 1" = Map_char.contains_at Int.equal map_1 x 1 = true
let%test "contains 2" = Map_char.contains_at Int.equal map_1 x 2 = false
let%test "contains 3" = Map_char_int.contains_at map_1 n 1 = false
let%test "is subobject" = Map_char.is_subobject Int.equal map_1 map_12 = true
let%test "is subobject" = Map_char.is_subobject Int.equal map_12 map_1 = false
let%test "is subobject" = Map_char.is_subobject Int.equal map_1 map_1 = true
let m1 = Map_char.of_list [('a', 1); ('b', 2); ('c', 4); ('d', 3); ('e', 5)]
let m1' = Map_char.of_list [('a', 4)]
let m1'' = Map_char.of_list [('d', 3); ('e', 5); ('f', 6)]
let m11'' = Map_char.of_list [('a', 1); ('b', 2); ('c', 4); ('d', 3); ('e', 5); ('f', 6)]
let m2 = Map_int.of_list [(1, 'a'); (2, 'b'); (3, 'c'); (4, 'd'); (7, 'g')]
let mc = Map_char.of_list [('a', 'b'); ('b', 'c'); ('c', 'd'); ('d', 'e')]

let%expect_test "domain" =
  Set_char.pp Format.std_formatter (Map_char_int.domain m1);
  [%expect "{a, b, c, d, e}"]

let%expect_test "codomain" =
  Set_int.pp Format.std_formatter (Map_char_int.codomain m1);
  [%expect "{1, 2, 3, 4, 5}"]

module CCharInt = My_map.ComposeMaker (Char) (Int)

let%expect_test "composition" =
  Map_char.pp_param ~sep:"  ;  " ~pp_value:Printing.pp_char Format.std_formatter
    (CCharInt.compose m1 m2);
  [%expect "
    {
    a -> a  ;  b -> b  ;  c -> d  ;  d -> c
    }"]

let%expect_test "auto composition identity" =
  Map_char.pp Printing.pp_char Format.std_formatter
    (Map_char.auto_compose_considering_undefined_is_identity mc mc);
  [%expect "
    {
    a -> c  ;  b -> d  ;  c -> e  ;  d -> e
    }"]

let%expect_test "auto composition identity" =
  Map_char.pp_param ~sep:"  ;  " ~pp_value:Printing.pp_char Format.std_formatter
    (Map_char.auto_compose mc mc);
  [%expect "
    {
    a -> c  ;  b -> d  ;  c -> e
    }"]

let%expect_test "auto composition identity" =
  Map_char.pp Printing.pp_char Format.std_formatter
    (Others.fixpoint_from (Map_char.equal Char.equal)
       (Map_char.auto_compose_considering_undefined_is_identity mc)
       mc);
  [%expect "
    {
    a -> e  ;  b -> e  ;  c -> e  ;  d -> e
    }"]

let%test "reversing" = Map_char_int.reverse map_12 = None

let%expect_test "reversing" =
  Printing.pp_opt (Map_int.pp Char.pp) Format.std_formatter (Map_char_int.reverse map_3);
  [%expect "
    Some({
    1 -> x  ;  3 -> l
    })"]

let xl = Set_char.of_list [x; l]
let xn = Set_char.of_list [x; n]

let%expect_test "map a set 1" =
  Set_int.pp Format.std_formatter (Map_char_int.map_a_set map_12 xl);
  [%expect "{1}"]

let%expect_test "map a set 2" =
  Set_int.pp Format.std_formatter (Map_char_int.map_a_set map_12 xn);
  [%expect "{1, 3}"]

let%expect_test "to_function" =
  let f = Map_char.to_function_with_not_mapped_is_identity mc in
  Set_char.pp Format.std_formatter (Set_char.map f (Set_char.of_list ['a'; 'c'; 'z']));
  [%expect "{b, d, z}"]

let%expect_test "pp" =
  Map_char.pp Int.pp Format.std_formatter m1;
  [%expect "
    {
    a -> 1  ;  b -> 2  ;  c -> 4  ;  d -> 3  ;  e -> 5
    }"]

let%test "meet works 1" =
  let meet = Map_char_int.union_if_compatible_opt m1 m1' in
  (Option.equal (Map_char.equal Int.equal)) meet None

let%test "meet works 2" =
  let meet = Map_char_int.union_if_compatible_opt m1 m1'' in
  (Option.equal (Map_char.equal Int.equal)) meet (Some m11'')

let%expect_test "find_map 1" =
  let found_mapped = Map_char.find_map m1 (fun c i -> if c = 'b' then Some (i * 7) else None) in
  Printing.pp_opt Int.pp Format.std_formatter found_mapped;
  [%expect "Some(14)"]

let%expect_test "find_map 2" =
  let found_mapped = Map_char.find_map m1 (fun c i -> if c = 'z' then Some (i * 7) else None) in
  Printing.pp_opt Int.pp Format.std_formatter found_mapped;
  [%expect "None"]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
