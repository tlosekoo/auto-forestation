open Misc

let log = Time_logger.create ()

let%expect_test "time_logger" =
  Time_logger.start_time_stamp log "A";
  System.sleepf 0.1;
  Time_logger.start_time_stamp log "B";
  System.sleepf 0.2;
  Time_logger.end_time_stamp log "B";
  Time_logger.start_time_stamp log "C";
  System.sleepf 0.1;
  Time_logger.start_time_stamp log "A";
  Time_logger.end_time_stamp log "A";
  System.sleepf 0.4;
  Time_logger.end_time_stamp log "C";
  System.sleepf 0.5;
  Time_logger.start_time_stamp log "B";
  System.sleepf 0.2;
  Time_logger.end_time_stamp log "B";
  Time_logger.start_time_stamp log "B";
  System.sleepf 0.2;
  Time_logger.end_time_stamp log "B";
  Time_logger.end_time_stamp log "A";
  let html_of_log = Time_logger.to_html log in

  Format.fprintf Format.std_formatter "%s" html_of_log;
  (* Format.fprintf Format.std_formatter "%s" html_of_log; *)
  [%expect
    {|
    <ul> <li>program:   0.</li><li>C:   0.511711120605</li><li>B:   0.618241071701</li><li>A:   1.73829889297</li></ul>


    <ul><li>program:   0.
    <ul><li>A:   1.73829889297
    <ul><li>B:   0.205893039703</li></ul>
    <ul><li>C:   0.511711120605
    <ul><li>A:   0.</li></ul></li></ul>
    <ul><li>B:   0.206126928329</li></ul>
    <ul><li>B:   0.206221103668</li></ul></li></ul></li></ul> |}]

let%expect_test "time_logger" =
  Time_logger.start_time_stamp log "A";
  System.sleepf 0.1;
  Time_logger.start_time_stamp log "B";
  System.sleepf 0.2;
  Time_logger.end_time_stamp log "B";
  Time_logger.start_time_stamp log "C";
  System.sleepf 0.1;
  Time_logger.start_time_stamp log "A";
  Time_logger.end_time_stamp log "A";
  System.sleepf 0.4;
  Time_logger.end_time_stamp log "C";
  System.sleepf 0.5;
  Time_logger.end_time_stamp log "AAA";
  let html_of_log = Time_logger.to_html log in

  Format.fprintf Format.std_formatter "%s" html_of_log;
  (* Format.fprintf Format.std_formatter "%s" html_of_log; *)
  [%expect.unreachable]
[@@expect.uncaught_exn
  {|
  (* CR expect_test_collector: This test expectation appears to contain a backtrace.
     This is strongly discouraged as backtraces are fragile.
     Please change this test to not include a backtrace. *)

  (Failure "Malformed log. A has wrong ending point AAA.")
  Raised at Stdlib.failwith in file "stdlib.ml", line 29, characters 17-33
  Called from Misc__Time_logger.Type_logger_printer.build_tree_until_closing in file "lib/time_logger.ml", line 56, characters 14-89
  Called from Misc__Time_logger.Type_logger_printer.from_log_to_durations in file "lib/time_logger.ml", line 65, characters 6-70
  Called from Misc__Time_logger.to_html in file "lib/time_logger.ml", line 137, characters 2-51
  Called from Misc_test__Time_logger_test.(fun) in file "test/time_logger_test.ml", line 56, characters 20-43
  Called from Expect_test_collector.Make.Instance_io.exec in file "collector/expect_test_collector.ml", line 234, characters 12-19 |}]
