open Misc

let plus_1_if_under_20 n =
  if n < 20 then
    n + 1
  else
    n

let%test "fixpoint_from" = Others.fixpoint_from ( = ) plus_1_if_under_20 5 = 20
let%test "fixpoint_from" = Others.fixpoint_from ( = ) plus_1_if_under_20 25 = 25
let ord = Others.lexicographical_order Int.compare String.compare
let%test "ord1" = ord (4, "oui") (5, "oui") = -1
let%test "ord1" = ord (5, "oui") (5, "oui") = 0
let%test "ord1" = ord (5, "oui") (5, "non") = 1
let pp_i i = Format.fprintf Format.std_formatter "%d" i

let%expect_test "iter" =
  Others.iter pp_i 5;
  [%expect {| 01234 |}]

let%test "eq_from_cmp false" = (Others.eq_from_cmp Int.compare) 0 1 = false
let%test "eq_from_cmp true" = (Others.eq_from_cmp Int.compare) 1 1 = true
let%test "forn" = Others.forn (( + ) 4) 0 7 = 0 + (4 * 7)

module Propagator_char = Accessible_computation.Propagator (Char)
module Set_char = My_set.Make2 (Char)

let%expect_test "accessible_computations" =
  let c1 = (Set_char.empty, Set_char.of_list ['a'; 'b']) in
  let c2 = (Set_char.singleton 'b', Set_char.of_list ['c']) in
  let c3 = (Set_char.of_list ['b'; 'c'], Set_char.of_list ['a'; 'd'; 'e'; 'f']) in
  let c4 = (Set_char.of_list ['f'; 'g'], Set_char.of_list ['h'; 'z']) in
  let c5 = (Set_char.of_list ['x'], Set_char.of_list ['y']) in
  let constraints = Propagator_char.Set_constraints.of_list [c1; c2; c3; c4; c5] in
  let initially_accessible = Set_char.singleton 'x' in
  let unused_constraints, accessible_chars =
    Propagator_char.propagate (constraints, initially_accessible)
  in
  let pp_constraints =
    Propagator_char.Set_constraints.pp_param ~opening:"" ~closing:"" ~sep:"\n"
      ~pp_element:Propagator_char.Constraint_on_X.pp
  in
  Format.fprintf Format.std_formatter
    "All constraints:\n%a\n\nInitially accessible chars:\n%a\n\nAccessible chars:\n%a\n\nUnused constraints:\n%a\n\n"
    pp_constraints constraints Set_char.pp initially_accessible Set_char.pp accessible_chars
    pp_constraints unused_constraints;
  [%expect
    "
    All constraints:
    {} => {a, b}
    {b} => {c}
    {b, c} => {a, d, e, f}
    {f, g} => {h, z}
    {x} => {y}

    Initially accessible chars:
    {x}

    Accessible chars:
    {a, b, c, d, e, f, x, y}

    Unused constraints:
    {f, g} => {h, z}"]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
