open Misc
include Pair.Make (Int) (Char)

let%expect_test "pp" =
  Format.fprintf Format.std_formatter "%a" pp (78, 'h');
  [%expect "(78, h)"]

module PairCharInt = Pair.Make (Char) (Int)

let%expect_test "pair cmp" =
  let l = [('b', 3); ('a', 2); ('b', 2); ('a', 3)] in
  let sorted_list = List.sort (Pair.compare Char.compare Int.compare) l in
  Format.fprintf Format.std_formatter "%a"
    (Printing.pp_list_and_brackets PairCharInt.pp)
    sorted_list;
  [%expect "[(a, 2), (a, 3), (b, 2), (b, 3)]"]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
