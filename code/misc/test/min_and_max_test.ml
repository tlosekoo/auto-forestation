open Misc

let max_int = Min_and_max.generate_max Int.compare
let min_int = Min_and_max.generate_min Int.compare
let%test "max int" = max_int 4 7 = 7
let%test "min int" = min_int 4 7 = 4
let%test "max in list empty" = Min_and_max.max_in_list Int.compare [] = None
let%test "max in list some" = Min_and_max.max_in_list Int.compare [-2; 7] = Some 7
let%test "min in list some" = Min_and_max.min_in_list Int.compare [-2; 7] = Some (-2)
let%test "max in positive list empty" = Min_and_max.max_pilist [] = 0
let%test "max in positive list some" = Min_and_max.max_pilist [2; 7] = 7

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
