open Misc
module Set_int = My_set.Make2 (Int)
module List_int = List_maker.Make (Int)
module Set_set_int = My_set.Make (My_set.Make (Int))
module Set_list_int = My_set.Make (List_maker.Make (Int))

let s = Set_int.of_list [1; 2; 3]
let s2 = Set_int.of_list [4; 7]

let%expect_test "pp" =
  Set_int.pp_param ~sep:" ; " Format.std_formatter s;
  [%expect {| {1 ; 2 ; 3} |}]

let%expect_test "pp 2" =
  Set_int.pp Format.std_formatter s;
  [%expect {| {1, 2, 3} |}]

let%expect_test "pp 3" =
  Set_int.pp Format.std_formatter s;
  [%expect {| {1, 2, 3} |}]

let pp_int_doubling c i = Format.fprintf c "%d%d" i i

let%expect_test "pp_param" =
  Set_int.pp_param ~opening:"LEFT" ~closing:"RIGHT" ~sep:" | " ~pp_element:pp_int_doubling
    Format.std_formatter s;
  [%expect {| LEFT11 | 22 | 33RIGHT |}]

let%expect_test "cartesian" =
  Set_list_int.pp Format.std_formatter (Set_int.cartesian_product [s; s2; s2]);
  [%expect
    "{[1, 4, 4], [1, 4, 7], [1, 7, 4], [1, 7, 7], [2, 4, 4], [2, 4, 7], [2, 7, 4], [2, 7, 7], [3, 4, 4], [3, 4, 7], [3, 7, 4], [3, 7, 7]}"]

let%expect_test "unordered cartesian" =
  Set_set_int.pp Format.std_formatter
    (Set_int.unordered_cartesian_product (Set_set_int.of_list [s; s2; s2]));
  [%expect "{{1, 4}, {1, 7}, {2, 4}, {2, 7}, {3, 4}, {3, 7}}"]

let%expect_test "preorder" =
  let s = Set_int.of_list [1; 2; 3; 4; 5; 6; 7] in
  let same_parity i1 i2 = i1 mod 2 = i2 mod 2 in
  let preordered_s = Set_int.split_into_equivalence_classes same_parity s in
  (Printing.pp_list_and_brackets Set_int.pp) Format.std_formatter preordered_s;
  [%expect "[{1, 3, 5, 7}, {2, 4, 6}]"]

(* module SetSetInt = My_set.Make2( *)

let%expect_test "seq union" =
  let sets =
    Set_set_int.of_list
      [Set_int.of_list [1; 2; 3]; Set_int.of_list [4; 7]; Set_int.singleton 1000; Set_int.empty]
  in
  let union_sets = sets |> Set_set_int.to_seq |> Set_int.seq_union in
  Set_int.pp Format.std_formatter union_sets;
  [%expect "{1, 2, 3, 4, 7, 1000}"]

let%expect_test "subsets" =
  let subsets = Set_int.subsets (Set_int.of_list [1; 2; 3]) in
  Set_set_int.pp Format.std_formatter subsets;
  [%expect "{{}, {1}, {1, 2}, {1, 2, 3}, {1, 3}, {2}, {2, 3}, {3}}"]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
