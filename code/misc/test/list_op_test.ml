open Misc

let int_eq x y = Int.compare x y = 0
let%test "all_equal true" = List_op.all_equal int_eq [5; 5; 5] = true
let%test "all_equal false" = List_op.all_equal int_eq [1; 5; 6] = false
let%test "all_uniq false" = List_op.all_uniq Int.compare [1; 5; 6; 5] = false
let%test "all_uniq true" = List_op.all_uniq Int.compare [1; 5; 6] = true
let%test "cartesian product of [] is [[]]" = List_op.cartesian_product_list [] = [[]]

module PC = List_maker.Make (List_maker.Make (Int))

let%expect_test "cartesian product 1" =
  let cp = List_op.cartesian_product_list [[4; 8]; [7; 6]] in
  Format.fprintf Format.std_formatter "%a" PC.pp cp;
  [%expect "[[4, 7], [8, 7], [4, 6], [8, 6]]"]

let%expect_test "cartesian product 2" =
  let cp = List_op.cartesian_product_list [[]; [7; 6]] in
  Format.fprintf Format.std_formatter "%a" PC.pp cp;
  [%expect "[]"]

let%expect_test "cartesian product 3" =
  let cp = List_op.cartesian_product_list [[4]; [7; 6]] in
  Format.fprintf Format.std_formatter "%a" PC.pp cp;
  [%expect "[[4, 7], [4, 6]]"]

let%expect_test "cartesian product 4" =
  let cp = List_op.cartesian_product_list [[1; 2]; [4]; [7; 6]] in
  Format.fprintf Format.std_formatter "%a" PC.pp cp;
  [%expect "[[1, 4, 7], [2, 4, 7], [1, 4, 6], [2, 4, 6]]"]

let%test "map ll" = List_op.map_ll (( + ) 1) [[3; 6]; [9]] = [[4; 7]; [10]]

let%test "list op to op list none" =
  List_op.convert_list_of_opt_into_opt_list [Some 7; Some 4; None] = None

let%test "list op to op list some" =
  List_op.convert_list_of_opt_into_opt_list [Some 7; Some 4] = Some [7; 4]

let%test "diff of [1;5;3] and [3;1;1;9] is [5]" =
  List_op.list_diff int_eq [1; 5; 3] [3; 1; 1; 9] = [5]

let%test "list union" =
  List.sort Int.compare (List_op.list_union Int.compare [[4; 5]; [4; 3]; [9]; []; [4]])
  = List.sort Int.compare [4; 5; 3; 9]

let%test "map uniq" = List_op.list_map_uniq Int.compare (( + ) 1) [4; 3; 4] = [4; 5]
let%test "list included true" = List_op.list_included int_eq [4; 5; 7; 4] [7; 5; 4] = true
let%test "list included false" = List_op.list_included int_eq [4; 5; 7; 4] [5; 4] = false
let%test "list equal true" = List_op.eq_lists_as_sets int_eq [4; 5; 7; 4] [7; 5; 4] = true
let%test "list equal false" = List_op.eq_lists_as_sets int_eq [4; 5; 7; 4] [5; 4] = false
let%test "list equal false" = List_op.eq_lists_as_sets int_eq [4] [5; 4] = false

let%test "list index sorting" =
  List_op.indexed_map_into_list Char.compare [(1, 'a'); (3, 'b'); (2, 'c'); (1, 'd')]
  = ['a'; 'd'; 'c'; 'b']

let%test "partition_on_first_element" =
  List_op.partition_on_first_element Int.compare [(1, 5); (1, 3); (2, 3)] = [(1, [5; 3]); (2, [3])]

let%test "split 1" =
  List_op.split_before_during_after_first_match (Int.equal 7) [1; 3; 5; 7; 9; 8; 6; 4; 2; 0]
  = ([1; 3; 5], Some 7, [9; 8; 6; 4; 2; 0])

let%test "split 2" =
  List_op.split_before_during_after_first_match (Int.equal 10) [1; 3; 5; 7; 9; 8; 6; 4; 2; 0]
  = ([1; 3; 5; 7; 9; 8; 6; 4; 2; 0], None, [])

let%expect_test "equivalence classes" =
  let partiy_equivalence i1 i2 = Int.equal (i1 mod 2) (i2 mod 2) in
  let equivalence_classes =
    List_op.split_into_equivalence_classes partiy_equivalence [1; 3; 4; 5; 2; 8; 7; 9; 4; 2; 0]
  in
  Printing.pp_list
    (Printing.pp_list_and_brackets Printing.pp_int)
    Format.std_formatter equivalence_classes;
  [%expect "[9, 7, 5, 3, 1], [0, 2, 4, 8, 2, 4]"]

(* let%test "preorder empty" = List_op.preorder Int.compare [] = [] *)

(* let%test "transpose" = List_op.transpose [] = [] *)

(* let%test "transpose" = List_op.transpose [[1;2;3] ; [4;5]] = [[1;4] ; [2;5] ; [3]] *)
(* let%expect_test "transpose" =
   let l = List_op.transpose [[1; 2; 3]; [4; 5]] in
   Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Int.pp) Format.std_formatter l;
   [%expect ""] *)

let%expect_test "two_among" =
  let l = ['a'; 'b'; 'c'; 'd'] in
  let ul = List_op.two_among l in
  Format.fprintf Format.std_formatter "%a"
    (Printing.pp_list_and_brackets_sep "," (Pair.pp_param ~pp_fst:Char.pp ~pp_snd:Char.pp))
    ul;
  [%expect "[(a, b),(a, c),(a, d),(b, c),(b, d),(c, d)]"]

let%expect_test "join_list_of_relations 1" =
  let r1 = [('a', 1); ('b', 2); ('c', 3); ('c', 5)] in
  let r2 = [('a', 1); ('b', 22); ('c', 33); ('d', 0)] in
  let join = List_op.join_list_of_relations Char.compare [r1; r2] in
  let pp_result c =
    Format.fprintf c "%a"
      (Printing.pp_assoclist_long Char.pp
         (Printing.pp_list_and_brackets_sep "\n" (Printing.pp_list_and_brackets Int.pp)))
  in
  pp_result Format.std_formatter join;
  [%expect
    "
    {
    a -> [[1, 1]]
    b -> [[2, 22]]
    c -> [[3, 33]
    [5, 33]]
    d -> []
    }"]

let%expect_test "join_list_of_relations 1" =
  let r1 = [('a', 1); ('b', 2); ('c', 3); ('c', 5)] in
  let r2 = [('a', 1); ('b', 22); ('c', 33); ('d', 0)] in
  let r3 = [('a', 111); ('b', 222); ('d', 0)] in
  let join = List_op.join_list_of_relations Char.compare [r1; r2; r3] in
  let pp_result c =
    Format.fprintf c "%a"
      (Printing.pp_assoclist_long Char.pp
         (Printing.pp_list_and_brackets_sep "\n" (Printing.pp_list_and_brackets Int.pp)))
  in
  pp_result Format.std_formatter join;
  [%expect "
    {
    a -> [[1, 1, 111]]
    b -> [[2, 22, 222]]
    c -> []
    d -> []
    }"]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
