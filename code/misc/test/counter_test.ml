open Misc

let c = Counter.get_new_counter ()
let%test "some new counter" = Counter.get_value_counter c = 0
let _ = Counter.get_and_incr_counter c
let _ = Counter.get_and_incr_counter c
let _ = Counter.get_and_incr_counter c
let%test "get and inct" = Counter.get_and_incr_counter c = 3
let () = Counter.reset c
let%test "after reset" = Counter.get_value_counter c = 0
let _ = Counter.get_and_incr_counter c

let%expect_test "pp" =
  Counter.pp Format.std_formatter c;
  [%expect {| count(1) |}]

(* *)
let%test "ok" =
  let _ = Printing.pp_ok () in
  true
