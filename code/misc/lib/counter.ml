type counter = int ref

let get_new_counter () : counter = ref 0
let set_value_counter (c : counter) (v : int) : unit = c := v
let get_value_counter (c : counter) = !c
let incr_counter (c : counter) : unit = set_value_counter c (get_value_counter c + 1)

let get_and_incr_counter (c : counter) =
  let cpt = get_value_counter c in
  let () = incr_counter c in
  cpt

let reset (c : counter) : unit = set_value_counter c 0

let pp (c : Format.formatter) (co : counter) : unit =
  Format.fprintf c "count(%d)" (get_value_counter co)
