(** The time at which a computation is supposed to stop, if any  *)
type time_limit = float option

(** Determines if time is up. *)
val is_time_up : time_limit -> bool

(** Raises a TimeUp exception if time is up. *)
val raise_time_up : time_limit -> unit

exception TimeUp
