let rec fixpoint_from (eq : 'a -> 'a -> bool) (f : 'a -> 'a) (e : 'a) : 'a =
  if eq (f e) e then
    e
  else
    fixpoint_from eq f (f e)

let lexicographical_order (cmp1 : 'a -> 'a -> int) (cmp2 : 'b -> 'b -> int) :
    'a * 'b -> 'a * 'b -> int =
 fun (a1, b1) (a2, b2) ->
  let compare_fst = cmp1 a1 a2 in
  if compare_fst <> 0 then
    compare_fst
  else
    cmp2 b1 b2

let iter (f : int -> unit) (n : int) : unit =
  let _ = List.init n f in
  ()

let eq_from_cmp (cmp : 'a -> 'a -> int) (x : 'a) (y : 'a) = cmp x y = 0

let rec forn (f : 'a -> 'a) (x : 'a) (n : int) : 'a =
  if n <= 0 then
    x
  else
    forn f (f x) (n - 1)
