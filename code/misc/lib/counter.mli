(** A counter is an integer reference *)
type counter

(** Creates a new counter *)
val get_new_counter : unit -> counter

(** resets the counter, i.e. sets its value to '0'. *)
val reset : counter -> unit

(** get_value_counter c' returns the current value of 'c'. *)
val get_value_counter : counter -> int

(** returns the counter value and increment it. *)
val get_and_incr_counter : counter -> int

(** Pretty-printer *)
val pp : Format.formatter -> counter -> unit
