module type S = sig
  include Map.S

  (* module Set_key : My_set.S with type elt = key *)
  type set_key

  exception Tried_adding_incompatible_binding

  (** 'of_list l' constructs a map containing every pair '(k,v)' of 'l' for which '(k,v)' is the last item of 'l' whose key is 'k'. *)
  val of_list : (key * 'a) list -> 'a t

  (** 'to_list m' constructs a list of every pair '(k,v)' of 'l' *)
  val to_list : 'a t -> (key * 'a) list

  (** 'add_compatible_binding eq k v m' adds pair '(k,v)' to 'm' if 'm[k]' does not exists or is equal to 'v'. Otherwise, raises an exception. *)
  val add_compatible_binding : ('a -> 'a -> bool) -> key -> 'a -> 'a t -> 'a t

  (** 'of_list_if_compatible l' constructs a map containing exactly every pair '(k,v)' of 'l', if possible.
		 If some key is defined on two different values, raises an exception. *)
  val of_list_if_compatible : ('a -> 'a -> bool) -> (key * 'a) list -> 'a t

  (** 'of_seq_if_compatible s' constructs a map containing exactly every pair '(k,v)' of 's', if possible.
		 If some key is defined on two different values, raises an exception. *)
  val of_seq_if_compatible : ('a -> 'a -> bool) -> (key * 'a) Seq.t -> 'a t

  (** Given an equality function, creates a union function. This function make the union of two maps as if they were sets,
		   but returns None if some key has two different values. *)
  val union_if_compatible : ('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t

  (** 'list_union_if_compatible eq ms' computes the smallest map containing every map of 'ms'. Raises an exception if two maps have incompatible entries. *)
  val list_union_if_compatible : ('a -> 'a -> bool) -> 'a t list -> 'a t

  (** 'seq_union_if_compatible eq ms' computes the smallest map containing every map of 'ms'. Raises an exception if two maps have incompatible entries. *)
  val seq_union_if_compatible : ('a -> 'a -> bool) -> 'a t Seq.t -> 'a t

  (** 'contains_at eq m k v' returns true iff m[k] = v' *)
  val contains_at : ('a -> 'a -> bool) -> 'a t -> key -> 'a -> bool

  (** 'is_subobject eq sub sup' returns true iff every '(v,k)' of 'sub' appears in 'sup'. *)
  val is_subobject : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool

  (** Pretty-printer *)
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_key:(Format.formatter -> key -> unit) ->
    ?sep_key_value:string ->
    pp_value:(Format.formatter -> 'a -> unit) ->
    Format.formatter ->
    'a t ->
    unit

  (** 'domain m' returns the set of keys on which 'm' is defined. *)
  val domain : 'a t -> set_key

  (** Given a map and a set of keys, returns the map whose bindings for those keys have been removed. *)
  val project_out : set_key -> 'a t -> 'a t

  (** Given a map and a set of keys, returns the map whose domain is restricted to those keys. *)
  val project_in : set_key -> 'a t -> 'a t

  (** Composition of partial functions *)
  val auto_compose : key t -> 'a t -> 'a t

  (** Composition of partial functions, but where undefined is considered as identity *)
  val auto_compose_considering_undefined_is_identity : key t -> key t -> key t

  (** Computes the reverse map if it is well defined (no key duplicates) *)
  val swap : key t -> key t option

  (* Transforms the map into its functional extension *)
  val to_function_with_not_mapped_is_identity : key t -> key -> key

  (* `find_map map f` return `Some(v)` if a binding `(key, value)` is found in `map` such that `(f key value) = Some(v)`,
     and otherwise `None`. *)
  val find_map : 'a t -> (key -> 'a -> 'result option) -> 'result option
end

module Make : functor (Keys : Ordered_and_printable.OP) ->
  S with type key := Keys.t and type set_key := My_set.Make(Keys).t

module type S2 = sig
  (* type key *)
  type +!'a map

  include S with type 'a t := 'a map

  type value
  type set_value
  type reversed_map
  type t = value map

  (* Previously defined functions, but without the comparison function needed *)
  val compare : t -> t -> int
  val equal : t -> t -> bool
  val union_if_compatible : t -> t -> t
  val union_if_compatible_opt : t -> t -> t option
  val add_compatible_binding : key -> value -> t -> t
  val add_compatible_binding_opt : key -> value -> t -> t option
  val of_list_if_compatible : (key * value) list -> t
  val of_seq_if_compatible : (key * value) Seq.t -> t
  val list_union_if_compatible : t list -> t
  val seq_union_if_compatible : t Seq.t -> t
  val contains_at : t -> key -> value -> bool
  val is_subobject : t -> t -> bool
  val pp : Format.formatter -> t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_key:(Format.formatter -> key -> unit) ->
    ?sep_key_value:string ->
    ?pp_value:(Format.formatter -> value -> unit) ->
    Format.formatter ->
    t ->
    unit

  (* New functions *)

  (* Computes the inverse map. None if undefined. *)
  val reverse : t -> reversed_map option
  val codomain : t -> set_value
  val map_a_set : t -> set_key -> set_value
end

module Make2 : functor (Keys : Ordered_and_printable.OP) (Values : Ordered_and_printable.OP) ->
  S2
    with type 'a map := 'a Make(Keys).t
     and type key = Keys.t
     and type value = Values.t
     and type set_key = My_set.Make(Keys).t
     and type set_value = My_set.Make(Values).t
     and type reversed_map = Keys.t Make(Values).t

module type SCompose = sig
  type map12
  type 'a map23
  type 'a map13

  val compose : map12 -> 'a map23 -> 'a map13
end

module ComposeMaker : functor
  (Keys1 : Ordered_and_printable.OP)
  (Keys2 : Ordered_and_printable.OP)
  ->
  SCompose
    with type map12 := Keys2.t Make(Keys1).t
     and type 'a map23 := 'a Make(Keys2).t
     and type 'a map13 := 'a Make(Keys1).t
