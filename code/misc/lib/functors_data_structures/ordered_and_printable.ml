module type OP = sig
  include Set.OrderedType

  val equal : t -> t -> bool
  val pp : Format.formatter -> t -> unit
end
