val pp_param :
  pp_fst:(Format.formatter -> 'a -> unit) ->
  pp_snd:(Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  'a * 'b ->
  unit

val create_from_fst_then_snd : 'a -> 'b -> 'a * 'b
val create_from_snd_then_fst : 'a -> 'b -> 'b * 'a
val fst : 'a * 'b -> 'a
val snd : 'a * 'b -> 'b
val map_fst : ('a -> 'b) -> 'a * 'c -> 'b * 'c
val map_snd : ('a -> 'b) -> 'c * 'a -> 'c * 'b
val swap : 'a * 'b -> 'b * 'a
val map : ('a -> 'b) -> ('c -> 'd) -> 'a * 'c -> 'b * 'd
val compare : ('a -> 'a -> int) -> ('b -> 'b -> int) -> 'a * 'b -> 'a * 'b -> int

module type S = sig
  type t_fst
  type t_snd

  include Ordered_and_printable.OP with type t = t_fst * t_snd
end

module Make : functor (FST : Ordered_and_printable.OP) (SND : Ordered_and_printable.OP) ->
  S with type t_fst = FST.t and type t_snd = SND.t
