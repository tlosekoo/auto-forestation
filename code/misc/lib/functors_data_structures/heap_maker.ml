module PriorityHeap (X : Ordered_and_printable.OP) = struct
  type t =
    | Leaf
    | Node of t * X.t * t * Int.t
  [@@deriving equal, compare]

  let empty = Leaf

  let is_empty h =
    match h with
    | Leaf -> true
    | _ -> false

  let singleton (k : X.t) = Node (Leaf, k, Leaf, 1)

  let rec cardinal (heap : t) : int =
    match heap with
    | Leaf -> 0
    | Node (left, _elt, right, _priority) -> 1 + cardinal left + cardinal right

  let pp c heap = Format.fprintf c "Heap has size %d.." (cardinal heap)

  let rank = function
    | Leaf -> 0
    | Node (_, _, _, r) -> r

  let rec merge t1 t2 =
    match (t1, t2) with
    | Leaf, t
    | t, Leaf ->
        t
    | Node (l, k1, r, _), Node (_, k2, _, _) ->
        if X.compare k1 k2 > 0 then
          merge t2 t1 (* switch merge if necessary *)
        else
          let merged = merge r t2 in
          (* always merge with right *)
          let rank_left = rank l and rank_right = rank merged in
          if rank_left >= rank_right then
            Node (l, k1, merged, rank_right + 1)
          else
            Node (merged, k1, l, rank_left + 1)
  (* left becomes right due to being shorter *)

  let insert x t = merge (singleton x) t

  let get_min = function
    | Leaf -> failwith "empty"
    | Node (_, k, _, _) -> k

  let delete_min = function
    | Leaf -> failwith "empty"
    | Node (l, _, r, _) -> merge l r

  let of_list = List.fold_left (fun t x -> insert x t) empty
end
