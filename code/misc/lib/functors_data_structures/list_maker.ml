module type S = sig
  type x

  include Ordered_and_printable.OP with type t = x list

  val pp_param : opening:string -> closing:string -> sep:string -> Format.formatter -> t -> unit
end

module Make (X : Ordered_and_printable.OP) = struct
  type x = X.t
  type t = X.t Stdlib.List.t

  let compare = Stdlib.List.compare X.compare
  let equal = Stdlib.List.equal X.equal
  let pp = Printing.pp_list_and_brackets X.pp

  let pp_param ~(opening : string) ~(closing : string) ~(sep : string) =
    Printing.pp_list_sep_surrounded sep opening closing X.pp
end
