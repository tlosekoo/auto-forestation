let pp_param ~pp_fst ~pp_snd c (a, b) = Format.fprintf c "(%a, %a)" pp_fst a pp_snd b
let create_from_fst_then_snd a b = (a, b)
let create_from_snd_then_fst b a = (a, b)
let fst = fst
let snd = snd
let map_fst f (a, b) = (f a, b)
let map_snd f (a, b) = (a, f b)
let swap (a, b) = (b, a)
let map f g (a, b) = (f a, g b)

let compare (cmp_a : 'a -> 'a -> int) (cmp_b : 'b -> 'b -> int) (a1, b1) (a2, b2) =
  let cmpa = cmp_a a1 a2 in
  if cmpa = 0 then
    cmp_b b1 b2
  else
    cmpa

module type S = sig
  type t_fst
  type t_snd

  include Ordered_and_printable.OP with type t = t_fst * t_snd
end

module Make (FST : Ordered_and_printable.OP) (SND : Ordered_and_printable.OP) = struct
  type t_fst = FST.t
  type t_snd = SND.t
  type t = FST.t * SND.t [@@deriving compare, equal]

  let pp = pp_param ~pp_fst:FST.pp ~pp_snd:SND.pp
end

(* val create_from_fst_then_snd : t_fst -> t_snd -> t
   val create_from_snd_then_fst : t_snd -> t_fst -> t

   val fst : t -> t_fst
   val snd : t -> t_snd *)
