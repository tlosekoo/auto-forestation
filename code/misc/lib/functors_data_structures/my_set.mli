module type S = sig
  include Set.S

  val of_list : elt list -> t
  val to_list : t -> elt list
  val pp : Format.formatter -> t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_element:(Format.formatter -> elt -> unit) ->
    Format.formatter ->
    t ->
    unit

  val list_union : t list -> t
  val seq_union : t Seq.t -> t
  val find_opt_predicate : (elt -> bool) -> t -> elt option
end

module type S2 = sig
  (* type set *)

  include S

  (* include S with type t := set *)
  (* module SetX : S with type elt := t *)
  module ListX : List_maker.S with type x = elt
  module ListSetX : List_maker.S with type x = t
  module SetListX : S with type elt := elt list
  module SetSetX : S with type elt := t

  (* type set_list_x *)
  (* type set_set_x *)
  (* type t = set *)

  val cartesian_product : ListSetX.t -> SetListX.t
  val unordered_cartesian_product : SetSetX.t -> SetSetX.t
  val split_into_equivalence_classes : (elt -> elt -> bool) -> t -> t list
  val subsets : t -> SetSetX.t
  (* val cartesian_product : t list -> set_list_x
     val cartesian_product : t list -> set_list_x
     val preorder : (elt -> elt -> int) -> t -> t list *)
  (* val set_union : set_set_x -> t *)
end

module Make : functor (X : Ordered_and_printable.OP) -> S with type elt := X.t

module Make2 : functor (X : Ordered_and_printable.OP) ->
  S2
    with type elt = X.t
    with type t = Make(X).t
    with module ListX := List_maker.Make(X)
     and module SetListX := Make(List_maker.Make(X))
     and module ListSetX := List_maker.Make(Make(X))
     and module SetSetX := Make(Make(X))
(* S2 with type elt = X.t and type set = Make(X).t and type set_list_x = Make(List_maker.Make(X)).t *)
