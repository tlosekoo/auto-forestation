open Ordered_and_printable

module type S = sig
  include Set.S

  val of_list : elt list -> t

  (** 'to_list s' returns a list containing exactly the elements of 's'. Order unspecified *)
  val to_list : t -> elt list

  val pp : Format.formatter -> t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_element:(Format.formatter -> elt -> unit) ->
    Format.formatter ->
    t ->
    unit

  (** 'list_union sl' returns the union of every set 's' of 'l'. *)
  val list_union : t List.t -> t

  (** 'seq_union ss' returns the union of every set 's' of 'ss'. *)
  val seq_union : t Seq.t -> t

  (** As find_first_opt, but checks every element *)
  val find_opt_predicate : (elt -> bool) -> t -> elt option
end

module Make (X : OP) = (* S with type elt = X.t  *) struct
  include Set.Make (X)

  (* type elt = X.t *)
  (* type t = Set.Make(X).t *)

  (** 'of_list l' returns the set containing exactly the elements of 'l' *)
  let of_list (l : elt list) = l |> List.to_seq |> of_seq

  let to_list (s : t) = fold List.cons s []

  (** 'list_union sl' returns the union of every set 's' of 'l'. *)
  let list_union (sets : t List.t) = List.fold_left union empty sets

  (** 'seq_union ss' returns the union of every set 's' of 'ss'. *)
  let seq_union (sets : t Seq.t) = Seq.fold_left union empty sets

  let pp_param
      ?(opening = "{")
      ?(closing = "}")
      ?(sep = ", ")
      ?(pp_element = X.pp)
      (c : Format.formatter)
      (set : t) : unit =
    Printing.pp_list_sep_surrounded sep opening closing pp_element c (List.of_seq (to_seq set))

  let pp : Format.formatter -> t -> unit = pp_param

  (** As find_first_opt, but checks every element *)
  let find_opt_predicate (f : elt -> bool) (m : t) : elt option =
    fold
      (fun e found_opt ->
        match found_opt with
        | None -> if f e then Some e else found_opt
        | Some _ -> found_opt)
      m None
end

module CartesianProductMaker (X : Ordered_and_printable.OP) = struct
  module SetX = Make (X)

  (* include Make (X) *)
  module SetListX = Make (List_maker.Make (X))

  let cartesian_product (ls : SetX.t list) : SetListX.t =
    ls |> List.map SetX.to_list |> List_op.cartesian_product_list |> SetListX.of_list
end

module EquivalenceClassesMaker (X : Ordered_and_printable.OP) = struct
  module SetX = Make (X)

  let split_into_equivalence_classes (eq : SetX.elt -> SetX.elt -> bool) (s : SetX.t) : SetX.t list
      =
    s |> SetX.to_list |> List_op.split_into_equivalence_classes eq |> List.map SetX.of_list
end

module UnorderedCartesianProductMaker (X : Ordered_and_printable.OP) = struct
  module SetX = Make (X)
  module SetSetX = Make (Make (X))

  let unordered_cartesian_product_one_step (s : SetX.t) (ss : SetSetX.t) : SetSetX.t =
    SetX.fold
      (fun x current_cartesian_product ->
        SetSetX.union current_cartesian_product (SetSetX.map (SetX.add x) ss))
      s SetSetX.empty

  let rec unordered_cartesian_product_with_accumulator (acc : SetSetX.t) (ss : SetSetX.t) :
      SetSetX.t =
    match SetSetX.choose_opt ss with
    | None -> acc
    | Some s ->
        let remaining_ss = SetSetX.remove s ss in
        let updated_acc = unordered_cartesian_product_one_step s acc in
        unordered_cartesian_product_with_accumulator updated_acc remaining_ss

  let unordered_cartesian_product (ss : SetSetX.t) : SetSetX.t =
    unordered_cartesian_product_with_accumulator (SetSetX.singleton SetX.empty) ss
end

module type S2 = sig
  include S

  module ListX : List_maker.S with type x = elt
  module ListSetX : List_maker.S with type x = t
  module SetListX : S with type elt := elt list
  module SetSetX : S with type elt := t

  val cartesian_product : ListSetX.t -> SetListX.t
  val unordered_cartesian_product : SetSetX.t -> SetSetX.t
  val split_into_equivalence_classes : (elt -> elt -> bool) -> t -> t list
  val subsets : t -> SetSetX.t
end

module Make2 (X : Ordered_and_printable.OP) = struct
  include Make (X)
  include CartesianProductMaker (X)
  include UnorderedCartesianProductMaker (X)
  include EquivalenceClassesMaker (X)

  module ListX = List_maker.Make (X)
  module SetListX = Make (List_maker.Make (X))
  module SetSetX = Make (Make (X))

  let rec subsets_acc (subsets : SetSetX.t) (s : t) : SetSetX.t =
    match choose_opt s with
    | None -> subsets
    | Some e ->
        let new_subsets = SetSetX.map (add e) subsets in
        let updated_subsets = SetSetX.union subsets new_subsets in
        let s_without_e = remove e s in
        subsets_acc updated_subsets s_without_e

  let subsets : t -> SetSetX.t = subsets_acc (SetSetX.singleton empty)

  (* type set = Make(X).t *)
  (* type set_list_x = Make(List_maker.Make(X)).t *)
end
