module type S = sig
  include Map.S

  (* module Set_key : My_set.S with type elt = key *)
  type set_key

  exception Tried_adding_incompatible_binding

  (** 'of_list l' constructs a map containing every pair '(k,v)' of 'l' for which '(k,v)' is the last item of 'l' whose key is 'k'. *)
  val of_list : (key * 'a) list -> 'a t

  (** 'to_list m' constructs a list of every pair '(k,v)' of 'l' *)
  val to_list : 'a t -> (key * 'a) list

  (** 'add_compatible_binding eq k v m' adds pair '(k,v)' to 'm' if 'm[k]' does not exists or is equal to 'v'. Otherwise, raises an exception. *)
  val add_compatible_binding : ('a -> 'a -> bool) -> key -> 'a -> 'a t -> 'a t

  (** 'of_list_if_compatible l' constructs a map containing exactly every pair '(k,v)' of 'l', if possible.
       If some key is defined on two different values, raises an exception. *)
  val of_list_if_compatible : ('a -> 'a -> bool) -> (key * 'a) list -> 'a t

  (** 'of_seq_if_compatible s' constructs a map containing exactly every pair '(k,v)' of 's', if possible.
       If some key is defined on two different values, raises an exception. *)
  val of_seq_if_compatible : ('a -> 'a -> bool) -> (key * 'a) Seq.t -> 'a t

  (** Given an equality function, creates a union function. This function make the union of two maps as if they were sets,
         but returns None if some key has two different values. *)
  val union_if_compatible : ('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t

  (** 'list_union_if_compatible eq ms' computes the smallest map containing every map of 'ms'. Raises an exception if two maps have incompatible entries. *)
  val list_union_if_compatible : ('a -> 'a -> bool) -> 'a t list -> 'a t

  (** 'seq_union_if_compatible eq ms' computes the smallest map containing every map of 'ms'. Raises an exception if two maps have incompatible entries. *)
  val seq_union_if_compatible : ('a -> 'a -> bool) -> 'a t Seq.t -> 'a t

  (** 'contains_at eq m k v' returns true iff m[k] = v' *)
  val contains_at : ('a -> 'a -> bool) -> 'a t -> key -> 'a -> bool

  (** 'is_subobject eq sub sup' returns true iff every '(v,k)' of 'sub' appears in 'sup'. *)
  val is_subobject : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool

  (** Pretty-printer *)
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_key:(Format.formatter -> key -> unit) ->
    ?sep_key_value:string ->
    pp_value:(Format.formatter -> 'a -> unit) ->
    Format.formatter ->
    'a t ->
    unit

  (** 'domain m' returns the set of keys on which 'm' is defined. *)
  val domain : 'a t -> set_key

  (** Given a map and a set of keys, returns the map whose bindings for those keys have been removed. *)
  val project_out : set_key -> 'a t -> 'a t

  (** Given a map and a set of keys, returns the map whose domain is restricted to those keys. *)
  val project_in : set_key -> 'a t -> 'a t

  (** Composition of partial functions *)
  val auto_compose : key t -> 'a t -> 'a t

  (** Composition of partial functions, but where undefined is considered as identity *)
  val auto_compose_considering_undefined_is_identity : key t -> key t -> key t

  (** Computes the reverse map if it is well defined (no key duplicates) *)
  val swap : key t -> key t option

  (* Transforms the map into its functional extension *)
  val to_function_with_not_mapped_is_identity : key t -> key -> key
  val find_map : 'a t -> (key -> 'a -> 'result option) -> 'result option
end

module Make (Keys : Ordered_and_printable.OP) =
(* : S with type key = Keys.t and module Set_key = My_set.Make(Keys)  *)
struct
  include Map.Make (Keys)
  module Set_key = My_set.Make (Keys)

  type set_key = Set_key.t

  let of_list (l : (key * 'a) list) : 'a t = List.fold_left (fun m (k, v) -> add k v m) empty l
  let to_list (m : 'a t) : (key * 'a) list = fold (fun k v -> List.cons (k, v)) m []

  exception Tried_adding_incompatible_binding

  let assert_unicity_of_definition (eq_v : 'a -> 'a -> bool) (_k : Keys.t) (v1 : 'a) (v2 : 'a) :
      'a option =
    if eq_v v1 v2 then
      Some v1
    else
      raise Tried_adding_incompatible_binding

  let union_if_compatible (eq : 'a -> 'a -> bool) = union (assert_unicity_of_definition eq)

  let add_compatible_binding (eq : 'a -> 'a -> bool) (k : key) (v : 'a) (map : 'a t) : 'a t =
    let update_existing_value (val_opt : 'a option) =
      match val_opt with
      | None -> Some v
      | Some v' -> assert_unicity_of_definition eq k v v'
    in
    update k update_existing_value map

  let contains_at (eq_a : 'a -> 'a -> bool) (m : 'a t) (k : key) (v : 'a) : bool =
    Option.fold ~none:false ~some:(eq_a v) (find_opt k m)

  let of_list_if_compatible (eq_a : 'a -> 'a -> bool) =
    List.fold_left (fun m (k, v) -> add_compatible_binding eq_a k v m) empty

  let of_seq_if_compatible (eq_a : 'a -> 'a -> bool) =
    Seq.fold_left (fun m (k, v) -> add_compatible_binding eq_a k v m) empty

  let list_union_if_compatible (eq_a : 'a -> 'a -> bool) =
    let union = union_if_compatible eq_a in
    List.fold_left union empty

  let seq_union_if_compatible (eq_a : 'a -> 'a -> bool) =
    let union = union_if_compatible eq_a in
    Seq.fold_left union empty

  let is_subobject (eq_a : 'a -> 'a -> bool) (sub_object : 'a t) (sup_object : 'a t) : bool =
    for_all (contains_at eq_a sup_object) sub_object

  let pp_param
      ?(opening = "{")
      ?(closing = "}")
      ?(sep = "  ;  ")
      ?(pp_key = Keys.pp)
      ?(sep_key_value = " -> ")
      ~(pp_value : Format.formatter -> 'a -> unit)
      (c : Format.formatter)
      (map : 'a t) : unit =
    Format.fprintf c "%s\n%a\n%s" opening
      (Printing.pp_list_sep sep (Printing.pp_binding ~sep_key_value pp_key pp_value))
      (bindings map) closing

  let pp (pp_val : Format.formatter -> 'a -> unit) : Format.formatter -> 'a t -> unit =
    pp_param ~pp_value:pp_val

  let domain (m : 'a t) : Set_key.t = to_seq m |> Seq.map fst |> Set_key.of_seq

  let project_out (keys : Set_key.t) (map : 'a t) : 'a t =
    filter (fun k _v -> not (Set_key.mem k keys)) map

  let project_in (keys : Set_key.t) (map : 'a t) : 'a t =
    filter (fun k _v -> Set_key.mem k keys) map

  type tt = Keys.t t

  let auto_compose (m1 : tt) (m2 : 'a t) : 'a t =
    merge
      (fun _key value_1_opt _value_2_opt ->
        match value_1_opt with
        | None -> None
        | Some value_1 -> find_opt value_1 m2)
      m1 m2

  let auto_compose_considering_undefined_is_identity (m1 : tt) (m2 : tt) : tt =
    merge
      (fun _key value_1_opt value_2_opt ->
        match value_1_opt with
        | None -> value_2_opt
        | Some value_1 -> (
            match find_opt value_1 m2 with
            | None -> value_1_opt
            | Some value_2 -> Some value_2))
      m1 m2

  let swap (m : tt) : tt option =
    let pairs = m |> to_list |> List.map Pair.swap in
    let keys = List.map fst pairs in
    let all_keys_uniq = List_op.all_uniq Keys.compare keys in
    if all_keys_uniq then
      Some (of_list pairs)
    else
      None

  let to_function_with_not_mapped_is_identity (m : tt) (k : Keys.t) : Keys.t =
    Option.value ~default:k (find_opt k m)

  let find_map (map : 'value t) (f : Keys.t -> 'value -> 'result option) : 'result option =
    map |> to_seq |> Seq.find_map (fun (key, value) -> f key value)
end

module type SCompose = sig
  type map12
  type 'a map23
  type 'a map13

  val compose : map12 -> 'a map23 -> 'a map13
end

(* Generates the function to compose two maps *)
module ComposeMaker (Keys1 : Ordered_and_printable.OP) (Keys2 : Ordered_and_printable.OP) = struct
  module M1 = Make (Keys1)
  module M2 = Make (Keys2)

  type map12 = Keys2.t M1.t
  type 'a map23 = 'a M2.t
  type 'a map13 = 'a M1.t

  let compose (m1 : map12) (m2 : 'a map23) : 'a map13 =
    M1.fold
      (fun key value_1 composed_map ->
        let value_2_opt = M2.find_opt value_1 m2 in
        match value_2_opt with
        | None -> composed_map
        | Some value_2 -> M1.add key value_2 composed_map)
      m1 M1.empty
end

(* Generates the function to reverse a map *)
module ReverserMaker (Keys : Ordered_and_printable.OP) (Values : Ordered_and_printable.OP) = struct
  (* module M1 = Make (Keys)
     module M2 = Make (Values) *)
end

module type S2 = sig
  (* type key *)
  type +!'a map

  include S with type 'a t := 'a map

  type value
  type set_value
  type reversed_map
  type t = value map

  (* Previously defined functions, but without the comparison function needed *)
  val compare : t -> t -> int
  val equal : t -> t -> bool
  val union_if_compatible : t -> t -> t
  val union_if_compatible_opt : t -> t -> t option
  val add_compatible_binding : key -> value -> t -> t
  val add_compatible_binding_opt : key -> value -> t -> t option
  val of_list_if_compatible : (key * value) list -> t
  val of_seq_if_compatible : (key * value) Seq.t -> t
  val list_union_if_compatible : t list -> t
  val seq_union_if_compatible : t Seq.t -> t
  val contains_at : t -> key -> value -> bool
  val is_subobject : t -> t -> bool
  val pp : Format.formatter -> t -> unit

  val pp_param :
    ?opening:string ->
    ?closing:string ->
    ?sep:string ->
    ?pp_key:(Format.formatter -> key -> unit) ->
    ?sep_key_value:string ->
    ?pp_value:(Format.formatter -> value -> unit) ->
    Format.formatter ->
    t ->
    unit

  (* New functions *)

  (* Computes the inverse map. None if undefined. *)
  val reverse : t -> reversed_map option
  val codomain : t -> set_value
  val map_a_set : t -> set_key -> set_value
end

(* Generates a map including functions requiring that values are ordered *)
module Make2 (Keys : Ordered_and_printable.OP) (Values : Ordered_and_printable.OP) = struct
  type value = Values.t

  (* module Set_keys = My_set.Make (Keys) *)
  module Set_values = My_set.Make (Values)

  type set_value = Set_values.t

  module M : module type of Make (Keys) with type 'a t := 'a Make(Keys).t = Make (Keys)
  module Reversed_map = Make (Values)

  type reversed_map = Keys.t Reversed_map.t

  include M

  type t = value Make(Keys).t
  (* module Reverser = ReverserMaker (Keys) (Values) *)
  (* include Reverser *)

  let compare = M.compare Values.compare
  let equal = M.equal Values.equal

  let assert_unicity_of_definition : key -> value -> value -> value option =
    M.assert_unicity_of_definition Values.equal

  let union_if_compatible : t -> t -> t = M.union assert_unicity_of_definition

  let union_if_compatible_opt (b1 : t) (b2 : t) : t option =
    try Some (union_if_compatible b1 b2) with
    | Tried_adding_incompatible_binding -> None

  let add_compatible_binding : key -> value -> t -> t = M.add_compatible_binding Values.equal

  let add_compatible_binding_opt (k : key) (v : value) (c : t) : t option =
    try Some (add_compatible_binding k v c) with
    | Tried_adding_incompatible_binding -> None

  let contains_at = M.contains_at Values.equal
  let of_list_if_compatible = M.of_list_if_compatible Values.equal
  let of_seq_if_compatible = M.of_seq_if_compatible Values.equal
  let list_union_if_compatible = M.list_union_if_compatible Values.equal
  let seq_union_if_compatible = M.seq_union_if_compatible Values.equal
  let is_subobject = M.is_subobject Values.equal
  let codomain (m : t) : set_value = m |> to_seq |> Seq.map snd |> Set_values.of_seq
  let map_a_set (m : t) (keys : set_key) : set_value = m |> project_in keys |> codomain

  let pp_param
      ?(opening = "{")
      ?(closing = "}")
      ?(sep = "  ;  ")
      ?(pp_key = Keys.pp)
      ?(sep_key_value = " -> ")
      ?(pp_value = Values.pp)
      (c : Format.formatter)
      (map : t) : unit =
    Format.fprintf c "%s\n%a\n%s" opening
      (Printing.pp_list_sep sep (Printing.pp_binding ~sep_key_value pp_key pp_value))
      (bindings map) closing

  let pp : Format.formatter -> t -> unit = pp_param ~pp_value:Values.pp

  let reverse (m : t) : reversed_map option =
    let pairs = m |> to_list |> List.map Pair.swap in
    let values = List.map fst pairs in
    let all_values_uniq = List_op.all_uniq Values.compare values in
    if all_values_uniq then
      Some (Reversed_map.of_list pairs)
    else
      None
  end
