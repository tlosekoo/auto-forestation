module type S = sig
  type x

  include Ordered_and_printable.OP with type t = x list

  val pp_param :
    opening:string -> closing:string -> sep:string -> Format.formatter -> x list -> unit
end

module Make : functor (X : Ordered_and_printable.OP) -> S with type x = X.t
