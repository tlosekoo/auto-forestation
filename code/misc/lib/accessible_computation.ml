module Propagator (X : Ordered_and_printable.OP) = struct
  module Set_X = My_set.Make (X)

  (* A constraint is an implication. If every element of the left part is reached, then all elements of the right-part become reachable. *)
  module Constraint_on_X = struct
    include Pair.Make (My_set.Make (X)) (My_set.Make (X))

    let pp c (preconditions, postconditions) =
      Format.fprintf c "%a => %a" Set_X.pp preconditions Set_X.pp postconditions
  end

  module Set_constraints = My_set.Make (Constraint_on_X)

  let propagate_one_step ((constraints, currently_accessible) : Set_constraints.t * Set_X.t) =
    let usable_constraints_and_their_conclusion =
      Set_constraints.filter
        (fun (head, _body) -> Set_X.subset head currently_accessible)
        constraints
    in
    let unused_constraints =
      Set_constraints.diff constraints usable_constraints_and_their_conclusion
    in
    let new_accessible_predicates =
      usable_constraints_and_their_conclusion |> Set_constraints.to_list |> List.map snd
      |> Set_X.list_union |> Set_X.union currently_accessible
    in
    (unused_constraints, new_accessible_predicates)

  let propagate : Set_constraints.t * Set_X.t -> Set_constraints.t * Set_X.t =
    let eq = Others.eq_from_cmp (Pair.compare compare Set_X.compare) in
    Others.fixpoint_from eq propagate_one_step
end
