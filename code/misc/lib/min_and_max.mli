(** This module defines some generic min/max functions and their application on integer list *)

(** 'generate_max cmp' returns the 'max' function that is compatible with 'cmp'  *)
val generate_max : ('a -> 'a -> int) -> 'a -> 'a -> 'a

(** 'generate_min cmp' returns the 'min' function that is compatible with 'cmp'  *)
val generate_min : ('a -> 'a -> int) -> 'a -> 'a -> 'a

(** 'max_in_list l cmp' returns the largest element in the list, or empty if there is none *)
val max_in_list : ('a -> 'a -> int) -> 'a list -> 'a option

(** 'min_in_list l cmp' returns the smallest element in the list, or empty if there is none *)
val min_in_list : ('a -> 'a -> int) -> 'a list -> 'a option

(** 'max_pilist l' returns the maximal element of 'l', or '0' if there are none. *)
val max_pilist : int list -> int

(** 'min_pilist l' returns the minimal element of 'l', or 'Int.max_int' if there are none. *)
val min_pilist : int list -> int
