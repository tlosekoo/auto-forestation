let all_equal (eq : 'a -> 'a -> bool) (l : 'a list) : bool =
  match l with
  | [] -> true
  | h :: t -> List.for_all (eq h) t

let all_uniq (cmp : 'a -> 'a -> int) (l : 'a list) : bool =
  List.compare_lengths (List.sort_uniq cmp l) l = 0

let cartesian_product (l : 'a list) (l' : 'b list) : ('a * 'b) list =
  List.concat (List.map (fun e -> List.map (fun e' -> (e, e')) l') l)

let cartesian_product_one_step (l : 'a list) (ll : 'a list list) : 'a list list =
  List.fold_left
    (fun current_cartesian_product x ->
      List.append current_cartesian_product (List.map (List.cons x) ll))
    [] l

let rec cartesian_product_list_with_accumulator (acc : 'a list list) (l : 'a list list) :
    'a list list =
  match l with
  | [] -> acc
  (* | [h] -> cartesian_product_one_step h acc  *)
  | h :: t ->
      let updated_acc = cartesian_product_one_step h acc in
      cartesian_product_list_with_accumulator updated_acc t

let cartesian_product_list (l : 'a list list) : 'a list list =
  List.map List.rev (cartesian_product_list_with_accumulator [[]] l)

(* let rec cartesian_product_list (l : 'a list list) : 'a list list =
   match l with
   | [] -> [[]]
   | [h] -> List.map (fun x -> [x]) h
   | h :: t ->
       let cartesian_tail = cartesian_product_list t in
       cartesian_product_one_step h cartesian_tail *)

(* Cartesian product without symetric bindings and without identity. See tests. *)
let rec two_among (l : 'a list) : ('a * 'a) list =
  match l with
  | [] -> []
  | h :: t ->
      let two_among_rec = two_among t in
      let new_bindings = List.map (Pair.create_from_fst_then_snd h) t in
      new_bindings @ two_among_rec

let map_ll (f : 'a -> 'b) = List.map (List.map f)
(*
   let map_ll2 (f : 'a -> 'b -> 'c) = List.map (List.map2 f)
*)

let convert_list_of_opt_into_opt_list (l_of_opt : 'a option list) : 'a list option =
  try Some (List.map Option.get l_of_opt) with
  | Invalid_argument _ -> None

let list_diff (eq : 'a -> 'a -> bool) (l1 : 'a list) (l2 : 'a list) : 'a list =
  List.filter (fun e1 -> not (List.exists (eq e1) l2)) l1

(* let list_union (cmp : 'a -> 'a -> int) (ls : 'a list list) : 'a list =
   let flattened = List.flatten ls in
   let flattened_and_uniq = List.sort_uniq cmp flattened in
   flattened_and_uniq *)

let list_union (cmp : 'a -> 'a -> int) (ls : 'a list list) : 'a list =
  ls |> List.flatten |> List.sort_uniq cmp

(* where is the equality predicate? <- Obsolete *)
let list_included (eq : 'a -> 'a -> bool) (l1 : 'a list) (l2 : 'a list) : bool =
  List.for_all (fun e1 -> List.exists (eq e1) l2) l1

let eq_lists_as_sets (eq : 'a -> 'a -> bool) (l1 : 'a list) (l2 : 'a list) : bool =
  list_included eq l1 l2 && list_included eq l2 l1

let list_map_uniq (cmp : 'b -> 'b -> int) (f : 'a -> 'b) (l : 'a list) : 'b list =
  List.sort_uniq cmp (List.map f l)

let indexed_map_into_list (cmp_a : 'a -> 'a -> int) (l : (int * 'a) list) : 'a list =
  let order = Others.lexicographical_order Int.compare cmp_a in
  let sorted_list = List.sort (fun p1 p2 -> order p1 p2) l in
  let projection = List.map snd sorted_list in
  projection

let rec split_before_during_after_first_match (f : 'a -> bool) (l : 'a list) :
    'a list * 'a option * 'a list =
  match l with
  | [] -> ([], None, [])
  | h :: t ->
      if f h then
        ([], Some h, t)
      else
        let before, during, after = split_before_during_after_first_match f t in
        (h :: before, during, after)

(* let rec split_by_consecutive_equiv (eq_a : 'a -> 'a -> bool) (al : 'a list) : 'a list list =
   match al with
   | [] -> []
   (* | [h] -> [[h]] *)
   | h :: t -> (
       let recurs = split_by_consecutive_equiv eq_a t in
       match recurs with
       | [] -> [[h]]
       | current_list :: other_lists -> (
           match current_list with
           | [] -> [h] :: other_lists
           | h_cl :: _ ->
               if eq_a h h_cl then
                 (h :: current_list) :: other_lists
               else
                 [h] :: recurs)) *)

let rec add_in_equivalence_class
    (eq : 'a -> 'a -> bool)
    (equivalence_classes : 'a list list)
    (e : 'a) : 'a list list =
  match equivalence_classes with
  | [] -> [[e]]
  | l :: other_equivalence_classes ->
      let hd_of_l = List.hd l in
      (* must be defined *)
      if eq hd_of_l e then
        (e :: l) :: other_equivalence_classes
      else
        l :: add_in_equivalence_class eq other_equivalence_classes e

let split_into_equivalence_classes (eq : 'a -> 'a -> bool) (l : 'a list) : 'a list list =
  List.fold_left (add_in_equivalence_class eq) [] l

(* let preorder (cmp_a : 'a -> 'a -> int) (al : 'a list) : 'a list list =
   let ordered_al = List.stable_sort cmp_a al in
   (* List.filter
      (fun l -> not (l = [])) *)
   split_by_consecutive_equiv (Others.eq_from_cmp cmp_a) ordered_al *)

let filter_opt (l : 'a option list) : 'a list = List.filter_map (fun x -> x) l

module Relation_as_indexed_list = struct
  type ('a, 'b) t = ('a * 'b List.t) List.t [@@deriving compare, equal]
end

type ('a, 'b) relation_as_indexed_list = ('a * 'b List.t) List.t

let partition_on_first_element (cmp_a : 'a -> 'a -> int) (l : ('a * 'b) list) :
    ('a, 'b) Relation_as_indexed_list.t =
  let a_elements = List.sort_uniq cmp_a (List.map fst l) in
  let list_of_b_elements_for_each_a =
    List.map
      (fun a_ref ->
        List.filter_map
          (fun (a, b) ->
            if a = a_ref then
              Some b
            else
              None)
          l)
      a_elements
  in
  let result = List.combine a_elements list_of_b_elements_for_each_a in
  result
(*
   let rec join_two_lists_ordered_by_first_element
       (cmp_a : 'a -> 'a -> int)
       (l1 : ('a, 'b) Relation_as_indexed_list.t)
       (l2 : ('a, 'c) Relation_as_indexed_list.t) : ('a, 'b * 'c) Relation_as_indexed_list.t =
     match (l1, l2) with
     | [], _ -> []
     | _, [] -> []
     | (a1, b) :: t1, (a2, c) :: t2 ->
         let cmp_a1_a2 = cmp_a a1 a2 in
         if cmp_a1_a2 < 0 then
           join_two_lists_ordered_by_first_element cmp_a t1 l2
         else if cmp_a1_a2 > 0 then
           join_two_lists_ordered_by_first_element cmp_a l1 t2
         else
           let cartesian = (a1, cartesian_product b c) in
           let join_rec = join_two_lists_ordered_by_first_element cmp_a t1 t2 in
           List.cons cartesian join_rec

   let join (cmp_a : 'a -> 'a -> int) (l1 : ('a * 'b) list) (l2 : ('a * 'c) list) :
       ('a * ('b * 'c) list) list =
     let l1_ordered_by_a = partition_on_first_element cmp_a l1 in
     let l2_ordered_by_a = partition_on_first_element cmp_a l2 in
     join_two_lists_ordered_by_first_element cmp_a l1_ordered_by_a l2_ordered_by_a *)

let join_list_of_relations (cmp_a : 'a -> 'a -> int) (ll : ('a * 'b) list list) :
    ('a, 'b list) Relation_as_indexed_list.t =
  let ll_as_relations_as_index_lists = List.map (partition_on_first_element cmp_a) ll in
  let number_of_relations = List.length ll_as_relations_as_index_lists in
  let indexed_list_of_relations =
    ll_as_relations_as_index_lists |> List.flatten |> partition_on_first_element cmp_a
  in
  let join =
    List.map
      (fun (a, l) ->
        if List.length l = number_of_relations then
          (a, cartesian_product_list l)
        else
          (a, []))
      indexed_list_of_relations
  in
  join

(* let split_according_to (f : 'a -> 'b) (cmp : 'b -> 'b -> int) (l : 'a list) : 'a list list =  *)

(* let rec join_list_lists_ordered_by_first_element
    (cmp_a : 'a -> 'a -> int)
    (ll : ('a * 'b list) list list) : ('a * ('b list list)) list = *)
(* let rec join_list_lists_ordered_by_first_element
       (cmp_a : 'a -> 'a -> int)
       (ll : ('a, 'b) list_indexed_by_first_element list) : ('a, 'b list) list_indexed_by_first_element
       =
     if List.exists (fun l -> l = []) ll then
       []
     else if ll = [] then
       []
     else
       let heads = List.map List.hd ll in
       let heads_a = List.map fst heads in
       let first_head_a = List.hd heads_a in

       let all_heads_equal = all_equal (Others.eq_from_cmp cmp_a) heads_a in
       if all_heads_equal then
         let all_tails = List.map List.tl ll in
         let heads_b = List.map snd heads in
         let cartesian = cartesian_product_list heads_b in
         let join_rec = join_list_lists_ordered_by_first_element cmp_a all_tails in
         List.cons (first_head_a, cartesian) join_rec
       else
         ()

   let join_list (cmp_a : 'a -> 'a -> int) (ll : ('a * 'b) list list) : ('a * ('b * 'c) list) list =
     let lists_ordered_by_a = List.map (partition_on_first_element cmp_a) ll in
     List.fold_left (join_lists_ordered_by_first_element cmp_a) [[]] lists_ordered_by_a *)
