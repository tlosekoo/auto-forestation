(** This module provides some additional operations on lists *)

(** 'all_equal eq l' returns true iff all elements of 'l' are equal (w.r.t 'eq') *)
val all_equal : ('a -> 'a -> bool) -> 'a list -> bool

(** 'all_uniq cmp l' returns true iff all elements of 'l' are pairwise different*)
val all_uniq : ('a -> 'a -> int) -> 'a list -> bool

(** 'combine_lists l' returns the combination of every elements of 'l'. When seeing 'l' as a square matrix, it is its transposition *)
(* val combine_lists : 'a list list -> 'a list list *)

(** 'cartesian_product al bl' returns the cartesian product between lists 'al' and 'bl'. *)
val cartesian_product : 'a list -> 'b list -> ('a * 'b) list

(** 'cartesian_product_list ll' returns the cartesian product between every list of 'll'. *)
val cartesian_product_list : 'a list list -> 'a list list

(** 'map_ll f_ab a_ll'  is a map of map. *)
val map_ll : ('a -> 'b) -> 'a list list -> 'b list list
(*
   (** 'map_ll2 f_abc a_ll'  is a map of map2. *)
   val map_ll2 : ('a -> 'b -> 'c) -> 'a list list -> ('b -> 'c) list list *)

(** 'convert_list_of_opt_into_opt_list l_opts' returns 'Some(l)' where 'l' is the list 'l_opts' whose elements have all been Option.gotten. 
	If there is at least one 'None' in 'l_opts', returns 'None'. *)
val convert_list_of_opt_into_opt_list : 'a option list -> 'a list option

(** 'list_diff l1 l2' returns the list 'l' whose elemnts are those appearing in 'l1' and not in 'l2' *)
val list_diff : ('a -> 'a -> bool) -> 'a list -> 'a list -> 'a list

(** 'list_union cmp ll' returns the list 'l' whose elements are those appearing in at least one element of 'll' (according to 'cmp') *)
val list_union : ('a -> 'a -> int) -> 'a list list -> 'a list

(** 'list_map_uniq cmp f l' maps 'l' with 'f' and then uniq_sorts the resulting list. *)
val list_map_uniq : ('b -> 'b -> int) -> ('a -> 'b) -> 'a list -> 'b list

(** 'list_included l1 l2' returns true iff 'l1' is included in 'l2' (when seen as sets) *)
val list_included : ('a -> 'a -> bool) -> 'a list -> 'a list -> bool

(** 'eq_lists_as_sets l1 l2' returns true iff 'l1' is included in 'l2' and vice-versa *)
val eq_lists_as_sets : ('a -> 'a -> bool) -> 'a list -> 'a list -> bool

(** Sorts on the first elements (and on second arguments if the first is equal) and projects on second argument *)
val indexed_map_into_list : ('a -> 'a -> int) -> (int * 'a) list -> 'a list

(** 'split_before_during_after_first_match p l` returns a triplet `(lb, Some e, la)` with `l = lb ++ [e] ++ la` with `e` the first element of `l` that evaluates to `true` with predicate `p`, if any. 
	If no such `e` exists, then returns (l, None, [])`. *)
val split_before_during_after_first_match : ('a -> bool) -> 'a list -> 'a list * 'a option * 'a list

val split_into_equivalence_classes : ('a -> 'a -> bool) -> 'a list -> 'a list list

(** Transforms '[(Some 4), None]' into '[4]'. *)
val filter_opt : 'a option list -> 'a list

(* Cartesian product without symetric bindings and without identity. See tests. *)
val two_among : 'a list -> ('a * 'a) list

type ('a, 'b) relation_as_indexed_list = ('a * 'b List.t) List.t

(** partition_on_first_element Int.compare [(1, 5) ; (1, 3) ; (2, 3)] returns [ (1, [5;3]) ; (2, [3]) ] *)
val partition_on_first_element :
  ('a -> 'a -> int) -> ('a * 'b) list -> ('a, 'b) relation_as_indexed_list

(* Performs the relational join operation. See tests for examples *)
val join_list_of_relations :
  ('a -> 'a -> int) -> ('a * 'b) list list -> ('a, 'b list) relation_as_indexed_list
