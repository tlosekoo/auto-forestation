type ('yes, 'maybe, 'no) t =
  | Yes of 'yes
  | Maybe of 'maybe
  | No of 'no
[@@deriving compare, equal]

let pp
    (pp_yes : Format.formatter -> 'yes -> unit)
    (pp_maybe : Format.formatter -> 'maybe -> unit)
    (pp_no : Format.formatter -> 'no -> unit)
    (c : Format.formatter)
    (tt : ('yes, 'maybe, 'no) t) : unit =
  match tt with
  | Yes yw -> Format.fprintf c "Yes: %a" pp_yes yw
  | Maybe mw -> Format.fprintf c "Maybe: %a" pp_maybe mw
  | No nw -> Format.fprintf c "No: %a" pp_no nw

let map (fy : 'yes -> 'yess) (fm : 'maybe -> 'maybee) (fn : 'no -> 'noo) (e : ('yes, 'maybe, 'no) t)
    : ('yess, 'maybee, 'noo) t =
  match e with
  | Yes yw -> Yes (fy yw)
  | Maybe mw -> Maybe (fm mw)
  | No nw -> No (fn nw)

let map_yes f =
  let id x = x in
  map f id id

let map_maybe f =
  let id x = x in
  map id f id

let map_no f =
  let id x = x in
  map id id f

let yes yw = Yes yw
let maybe mw = Maybe mw
let no nw = No nw

let get_yes v =
  match v with
  | Yes yw -> Some yw
  | _ -> None

let get_maybe v =
  match v with
  | Maybe mw -> Some mw
  | _ -> None

let get_no v =
  match v with
  | No nw -> Some nw
  | _ -> None

let is_yes (v : _ t) : bool = Option.is_some (get_yes v)
let is_no (v : _ t) : bool = Option.is_some (get_no v)
let is_maybe (v : _ t) : bool = Option.is_some (get_maybe v)
