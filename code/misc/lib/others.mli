(** 'fixpoint_from f eq f e' iterates 'f', starting from 'e', until 'eq f(x) x'. Does not necessarily terminates *)
val fixpoint_from : ('a -> 'a -> bool) -> ('a -> 'a) -> 'a -> 'a

(* Given two orders, compute the lexical ordering of pairs. *)
val lexicographical_order : ('a -> 'a -> int) -> ('b -> 'b -> int) -> 'a * 'b -> 'a * 'b -> int

(** 'iter f n' calls, in order, 'f i' for 'i <= 0 < n'. *)
val iter : (int -> unit) -> int -> unit

(** generates the equality predicate associated with a comparison predicate *)
val eq_from_cmp : ('a -> 'a -> int) -> 'a -> 'a -> bool

val forn : ('a -> 'a) -> 'a -> int -> 'a
