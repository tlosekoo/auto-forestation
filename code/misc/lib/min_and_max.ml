(*   *)
(*   *)
(* Min/Max functions *)
let generate_max (compare : 'a -> 'a -> int) x y =
  if compare x y < 0 then
    y
  else
    x

let generate_min (compare : 'a -> 'a -> int) x y =
  if compare x y < 0 then
    x
  else
    y

let max_in_list (compare : 'a -> 'a -> int) (l : 'a list) : 'a option =
  if l = [] then
    None
  else
    let max_fun = generate_max compare in
    let first_element = List.hd l in
    let max = List.fold_left max_fun first_element l in
    Some max

let min_in_list (compare : 'a -> 'a -> int) (l : 'a list) : 'a option =
  if l = [] then
    None
  else
    let min_fun = generate_min compare in
    let first_element = List.hd l in
    let min = List.fold_left min_fun first_element l in
    Some min

let max_pilist l =
  let max_opt = max_in_list Int.compare l in
  Option.value ~default:0 max_opt

let min_pilist l =
  let min_opt = min_in_list Int.compare l in
  Option.value ~default:Int.max_int min_opt
