include Stdlib.Bool

let equal = Others.eq_from_cmp Stdlib.Bool.compare
let pp (c : Format.formatter) = Format.fprintf c "%b"
