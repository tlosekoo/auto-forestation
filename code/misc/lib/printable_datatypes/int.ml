include Stdlib.Int

let equal = Others.eq_from_cmp Stdlib.Int.compare
let pp (c : Format.formatter) = Format.fprintf c "%d"
