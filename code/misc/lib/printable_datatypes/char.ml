include Stdlib.Char

let equal = Others.eq_from_cmp Stdlib.Char.compare
let pp (c : Format.formatter) = Format.fprintf c "%c"
