include Stdlib.Float

let equal = Others.eq_from_cmp Stdlib.Float.compare
let pp (c : Format.formatter) = Format.fprintf c "%f"
