(** 'sleepf f' sleeps for 'f' seconds *)
val sleepf : float -> unit
