module Map_string = My_map.Make (String)

type time_stamp =
  | Start of string * float
  | End of string * float

type log = time_stamp list
type t = log ref

let start_time_stamp (time_logger : t) (entry : string) =
  let current_time = Unix.gettimeofday () in
  time_logger := Start (entry, current_time) :: !time_logger

let end_time_stamp (time_logger : t) (entry : string) =
  let current_time = Unix.gettimeofday () in
  time_logger := End (entry, current_time) :: !time_logger

let create () : t = ref []

module Type_logger_printer = struct
  type labeled_duration = string * float

  type duration_tree =
    | Leaf of labeled_duration
    | Node of labeled_duration * duration_tree list

  module Map_string_float = My_map.Make2 (String) (Float)

  type sum_of_times_for_each_label = labeled_duration list

  let rec build_tree_until_closing
      (log : log)
      (starting_entry : string)
      (starting_time : float)
      (current_subtrees : duration_tree list) : duration_tree * log =
    match log with
    | [] -> failwith ("Malformed log. " ^ starting_entry ^ " has no ending point")
    | time_stamp :: rest_log -> (
        match time_stamp with
        | End (ending_entry, ending_time) ->
            if String.equal starting_entry ending_entry then
              let elapsed_time = ending_time -. starting_time in
              let labeled_duration =
                if current_subtrees = [] then
                  Leaf (starting_entry, elapsed_time)
                else
                  Node ((starting_entry, elapsed_time), List.rev current_subtrees)
              in
              (labeled_duration, rest_log)
            else
              failwith
                ("Malformed log. " ^ starting_entry ^ " has wrong ending point " ^ ending_entry
               ^ ".")
        | Start (next_starting_entry, next_starting_time) ->
            let sub_durationtree, rest_rest_log =
              build_tree_until_closing rest_log next_starting_entry next_starting_time []
            in
            let updated_current_subtrees = sub_durationtree :: current_subtrees in
            build_tree_until_closing rest_rest_log starting_entry starting_time
              updated_current_subtrees)

  let from_log_to_durations (log : log) : duration_tree =
    let logs_with_ending_point = List.rev (End ("program", 0.0) :: log) in
    let duration_tree, rest_log =
      build_tree_until_closing logs_with_ending_point "program" 0.0 []
    in
    if rest_log = [] then
      duration_tree
    else
      failwith "?"

  let add_duration_to_sum_of_durations
      ((label, duration) : labeled_duration)
      (times_for_each_label : Map_string_float.t) : Map_string_float.t =
    match Map_string_float.find_opt label times_for_each_label with
    | None -> Map_string_float.add label duration times_for_each_label
    | Some previous_duration ->
        Map_string_float.add label (previous_duration +. duration) times_for_each_label

  let rec from_duration_tree_to_sum_of_times_for_each_label_aux
      (current_sum_of_times_for_each_label : Map_string_float.t)
      (duration_tree : duration_tree) : Map_string_float.t =
    match duration_tree with
    | Leaf labeled_duration ->
        add_duration_to_sum_of_durations labeled_duration current_sum_of_times_for_each_label
    | Node (labeled_duration, duration_subtrees) ->
        let recursively_updated_sum_of_duration =
          List.fold_left from_duration_tree_to_sum_of_times_for_each_label_aux
            current_sum_of_times_for_each_label duration_subtrees
        in
        add_duration_to_sum_of_durations labeled_duration recursively_updated_sum_of_duration

  let from_duration_tree_to_sum_of_times_for_each_label (duration_tree : duration_tree) :
      sum_of_times_for_each_label =
    let sum_of_duration =
      from_duration_tree_to_sum_of_times_for_each_label_aux Map_string_float.empty duration_tree
    in
    Map_string_float.to_list sum_of_duration

  (* to html *)
  let labeled_duration_to_str (entry : string) (duration : float) : string =
    entry ^ ":   " ^ Float.to_string duration

  let from_duration_leaf_to_html (entry : string) (duration : float) : string =
    "<li>" ^ labeled_duration_to_str entry duration ^ "</li>"

  let rec from_duration_tree_to_html (durations : duration_tree) : string =
    let duration_item_as_html =
      match durations with
      | Leaf (entry, duration) -> from_duration_leaf_to_html entry duration
      | Node ((entry, duration), sub_durationtree) ->
          let subtrees_as_html = List.map from_duration_tree_to_html sub_durationtree in
          let sublist = String.concat "\n" subtrees_as_html in
          "<li>" ^ labeled_duration_to_str entry duration ^ "\n" ^ sublist ^ "</li>"
    in
    "<ul>" ^ duration_item_as_html ^ "</ul>"

  let from_durations_sum_to_html (duration_sum : sum_of_times_for_each_label) : string =
    (* ugly double map swap *)
    let list_of_duration_sums =
      duration_sum |> List.map Pair.swap
      |> List.sort (Others.lexicographical_order Float.compare String.compare)
      |> List.map Pair.swap
      |> List.map (fun (entry, duration_sum) -> from_duration_leaf_to_html entry duration_sum)
      |> List.fold_left ( ^ ) " "
    in
    "<ul>" ^ list_of_duration_sums ^ "</ul>"

  let from_durations_to_html (durations : duration_tree) : string =
    let duration_sum = from_duration_tree_to_sum_of_times_for_each_label durations in
    let html_duration_tree = from_duration_tree_to_html durations in
    let html_duration_sum = from_durations_sum_to_html duration_sum in
    html_duration_sum ^ "\n\n\n" ^ html_duration_tree
end

let to_html (log : t) : string =
  !log |> Type_logger_printer.from_log_to_durations |> Type_logger_printer.from_durations_to_html
