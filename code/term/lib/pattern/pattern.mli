(** A pattern is either a variable or a typed symbol applied to other patterns *)
include Misc.Ordered_and_printable.OP

(** Creates a pattern that is the application of a typed symbol on the list of its children.
   	Arity and type checking are done. *)
val create : Typed_function.t -> t list -> t

(** Creates a pattern that is a variable *)
val from_var : Typed_symbol.t -> t

(** Returns he depth of the pattern seen as a tree. The depth of a single node (variable or 0-ary constructor) is 0, not 1. *)
val depth : t -> int

(** Returns the root symbol of the pattern, if any *)
val get_root_symbol_opt : t -> Typed_function.t option

(** Returns the root symbol of th pattern. Raise an exception if the pattern is a variable. *)
val get_root_symbol : t -> Typed_function.t

(** Returns the subterms of a pattern. A variable has no subterm. *)
val get_children : t -> t list

(** returns the variable if the pattern is one *)
val get_variable_opt : t -> Typed_symbol.t option

(** get all variables from a padding *)
val get_variables : t -> Misc.My_set.Make(Typed_symbol).t

(** returns the list of variables present in the pattern. Can contain duplicates. *)
val get_variables_dfs_ordered : t -> Typed_symbol.t list

(** returns the list of variables present in the pattern. Does not contain duplicates. *)
val get_variables_dfs_ordered_no_duplicate : t -> Typed_symbol.t list

(** Returns true iff the pattern does not have any variable *)
val is_closed : t -> bool

(** Returns true iff the pattern is a variable *)
val is_variable : t -> bool

(** Returns the type of the pattern. If the pattern is a variable, returns the variable datatype. *)
val get_type : t -> Datatype.t

(** compare_shape t1 t2' compares the shapes of the terms, i.e. compare the patterns by abstracting over variables.
   That is, for two terms 't1' and 't2', 'compare_shape t1 t2' returns 0 iff 't1' and 't2' differ only by variable names. *)
val compare_shape : t -> t -> int

(** compare_shape t1 t2' compares the shapes of the terms, i.e. compare the patterns by comparing variables using `cmp_var`. *)
val compare_shape_with_variable_comparison : (Typed_symbol.t -> Typed_symbol.t -> int) -> t -> t -> int

(** 'map_variable f p' returns the pattern 'p' to which the function 'f' has been applied to every variable *)
val map_variable_to_variable : (Typed_symbol.t -> Typed_symbol.t) -> t -> t

val map_variable_to_pattern : (Typed_symbol.t -> t) -> t -> t
