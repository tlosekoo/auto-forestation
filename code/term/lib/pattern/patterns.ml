include Misc.List_maker.Make (Pattern)

let pp_without_brackets = pp_param ~opening:"" ~closing:"" ~sep:", "
let get_type (ps : t) = ps |> List.map Pattern.get_type

let get_root_symbols (ps : t) : Typed_functions.t Option.t =
  Misc.List_op.convert_list_of_opt_into_opt_list (List.map Pattern.get_root_symbol_opt ps)

let get_root_symbols_opt (ps : t) : Typed_function.t Option.t List.t =
  List.map Pattern.get_root_symbol_opt ps

let arity : t -> int = List.length
let depth (patterns : t) : int = List.fold_left Int.max 0 (List.map Pattern.depth patterns)
