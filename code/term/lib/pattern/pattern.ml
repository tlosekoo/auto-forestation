open Aliases_symbol

type t =
  | Var of Typed_symbol.t
  | Pattern of Typed_function.t * t List.t
[@@deriving compare, equal]

let rec pp_parameterized ~(complete : bool) (c : Format.formatter) (p : t) : unit =
  let pp_typed_function = Typed_function.pp_parameterized ~complete in
  let pp_pattern = pp_parameterized ~complete in
  let pp_variable = Typed_symbol.pp_parameterized ~complete in
  match p with
  | Var s -> pp_variable c s
  | Pattern (ts, children) ->
      pp_typed_function c ts;
      if List.length children > 0 then
        Format.fprintf c "(%a)" (Printing.pp_list pp_pattern) children

let pp = pp_parameterized ~complete:false

let get_type (p : t) : Datatype.t =
  match p with
  | Var v -> Typed_symbol.get_type v
  | Pattern (rs, _) -> Typed_function.get_output_type rs

let typecheck (ts : Typed_function.t) (args : t list) : unit =
  let input_type_rs = Typed_function.get_input_types ts in
  let output_type_args = List.map get_type args in
  let typechecks = List.for_all2 Datatype.equal input_type_rs output_type_args in

  if not typechecks then
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "pattern typechecking: failed to build a pattern with (root symbol, input type or root symbol, output_type_args) = (%a, %a, %a)"
           (Typed_function.pp_parameterized ~complete:true)
           ts
           (Printing.pp_list_and_brackets Datatype.pp)
           input_type_rs
           (Printing.pp_list_and_brackets Datatype.pp)
           output_type_args)
    in
    raise (Invalid_argument error_message)

let create (ts : Typed_function.t) (args : t list) : t =
  let () = typecheck ts args in
  Pattern (ts, args)

let from_var (v : Typed_symbol.t) : t = Var v

let rec map_variable_to_variable (f : Typed_symbol.t -> Typed_symbol.t) (p : t) : t =
  match p with
  | Var x -> Var (f x)
  | Pattern (root, children) -> Pattern (root, List.map (map_variable_to_variable f) children)

let rec map_variable_to_pattern (f : Typed_symbol.t -> t) (p : t) : t =
  match p with
  | Var x -> f x
  | Pattern (root, children) -> Pattern (root, List.map (map_variable_to_pattern f) children)

let rec depth_aux t =
  match t with
  | Pattern (_root, children) -> 1 + Misc.Min_and_max.max_pilist (List.map depth_aux children)
  | Var _symbol -> 1

let depth t =
  match t with
  | Pattern (_root, children) -> Misc.Min_and_max.max_pilist (List.map depth_aux children)
  | Var _symbol -> 0

let get_root_symbol_opt (p : t) : Typed_function.t option =
  match p with
  | Pattern (root, _children) -> Some root
  | Var _symbol -> None

let get_root_symbol (p : t) : Typed_function.t =
  let root_symbol_option = get_root_symbol_opt p in
  if Option.is_some root_symbol_option then
    Option.get root_symbol_option
  else
    raise (Invalid_argument "tried to get root symbol of a variable")

let get_children (p : t) : t list =
  match p with
  | Pattern (_root, children) -> children
  | Var _symbol -> []

let get_variable_opt (p : t) : Typed_symbol.t option =
  match p with
  | Pattern (_root, _children) -> None
  | Var x -> Some x

let rec get_variables_dfs_ordered (p : t) : Typed_symbol.t list =
  match p with
  | Pattern (_root, children) -> List.concat (List.map get_variables_dfs_ordered children)
  | Var x -> [x]

let rec get_variables_dfs_ordered_no_duplicate_aux
    ((already_seen, vars_reverse_dfs_order) :
      Aliases_symbol.Set_typed_symbol.t * Typed_symbol.t list)
    (p : t) : Aliases_symbol.Set_typed_symbol.t * Typed_symbol.t list =
  match p with
  | Pattern (_root, children) ->
      List.fold_left get_variables_dfs_ordered_no_duplicate_aux
        (already_seen, vars_reverse_dfs_order)
        children
  | Var x ->
      if Aliases_symbol.Set_typed_symbol.mem x already_seen then
        (already_seen, vars_reverse_dfs_order)
      else
        (Aliases_symbol.Set_typed_symbol.add x already_seen, x :: vars_reverse_dfs_order)

let get_variables_dfs_ordered_no_duplicate (p : t) : Typed_symbol.t list =
  List.rev
    (snd (get_variables_dfs_ordered_no_duplicate_aux (Aliases_symbol.Set_typed_symbol.empty, []) p))

let get_variables (p : t) : Set_typed_symbol.t =
  p |> get_variables_dfs_ordered |> Set_typed_symbol.of_list

let is_closed (p : t) : bool = p |> get_variables |> Set_typed_symbol.is_empty

let is_variable (p : t) : bool =
  match p with
  | Var _ -> true
  | _ -> false

let rec compare_shape_with_variable_comparison
    (cmp_var : Typed_symbol.t -> Typed_symbol.t -> int)
    (p1 : t)
    (p2 : t) : int =
  match (p1, p2) with
  | Var v1, Var v2 -> cmp_var v1 v2
  | Var _, Pattern (_, _) -> -1
  | Pattern (_, _), Var _ -> 1
  | Pattern (ts_1, children_1), Pattern (ts_2, children_2) ->
      let compare_symbols = Typed_function.compare ts_1 ts_2 in
      if compare_symbols <> 0 then
        compare_symbols
      else
        List.compare (compare_shape_with_variable_comparison cmp_var) children_1 children_2

let compare_shape = compare_shape_with_variable_comparison (fun _ _ -> 0)
