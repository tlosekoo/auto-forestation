open Aliases

(* Datatype names *)
let nat = Datatype.create "nat"
let natlist = Datatype.create "natlist"
let nattsil = Datatype.create "nattsil"
let pair_nat_nat = Datatype.create "pair_nat_nat"

(* let nat_tree = Datatype.create "nat_tree" *)
let elt = Datatype.create "elt"
let elt_tree = Datatype.create "elt_tree"

(* custom dataype *)
let custom = Datatype.create "custom"
let custom_a_symbol = Typed_function.create (Symbol.create "c_a") custom []
let custom_b_symbol = Typed_function.create (Symbol.create "c_b") custom []
let custom_c_symbol = Typed_function.create (Symbol.create "c_c") custom []
let custom_f_symbol = Typed_function.create (Symbol.create "c_f") custom [custom]
let custom_g_symbol = Typed_function.create (Symbol.create "c_g") custom [custom; custom]

(* Datatypes constructors *)
let z_symbol = Typed_function.create (Symbol.create "z") nat []
let s_symbol = Typed_function.create (Symbol.create "s") nat [nat]
let a_symbol = Typed_function.create (Symbol.create "a") elt []
let b_symbol = Typed_function.create (Symbol.create "b") elt []
let nil_symbol = Typed_function.create (Symbol.create "nil") natlist []
let cons_symbol = Typed_function.create (Symbol.create "cons") natlist [nat; natlist]
let lin_symbol = Typed_function.create (Symbol.create "lin") nattsil []
let snoc_symbol = Typed_function.create (Symbol.create "snoc") nattsil [nattsil; nat]
let pair_symbol = Typed_function.create (Symbol.create "pair") pair_nat_nat [nat; nat]

(* let nat_leaf_symbol = Typed_function.create (Symbol.create "nleaf") nat_tree [] *)
(* let nat_node_symbol =
   Typed_function.create (Symbol.create "nnode") nat_tree [nat_tree; nat; nat_tree] *)

let elt_leaf_symbol = Typed_function.create (Symbol.create "leaf") elt_tree []

let elt_node_symbol =
  Typed_function.create (Symbol.create "node") elt_tree [elt_tree; elt; elt_tree]

let some_datatypes = Set_datatype.of_list [nat; elt; natlist; pair_nat_nat; elt_tree]

let every_defined_typed_functions =
  Set_typed_function.of_list
    [
      z_symbol;
      s_symbol;
      a_symbol;
      b_symbol;
      nil_symbol;
      cons_symbol;
      pair_symbol;
      lin_symbol;
      snoc_symbol;
      elt_leaf_symbol;
      elt_node_symbol;
      custom_a_symbol;
      custom_b_symbol;
      custom_c_symbol;
      custom_f_symbol;
      custom_g_symbol;
    ]

(* let some_typed_functions =
   Set_typed_function.of_list
     [
       z_symbol;
       s_symbol;
       a_symbol;
       b_symbol;
       nil_symbol;
       cons_symbol;
       pair_symbol;
       elt_leaf_symbol;
       elt_node_symbol;
     ] *)

let testing_type_env = Datatype_environment.build every_defined_typed_functions
