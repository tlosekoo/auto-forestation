open Aliases
open Database_type_environment

(* GENERATE THINGS *)
let rec generate_tuple_of_symbols_of_size_less_than_n_and_n
    (all_function : Typed_function.t list)
    (n : int) : Set_typed_functions.t * Set_typed_functions.t =
  if n = 0 then
    (Set_typed_functions.empty, Set_typed_functions.singleton [])
  else
    let functions_lesser_size_than_n_minus_one, functions_size_n_minus_one =
      generate_tuple_of_symbols_of_size_less_than_n_and_n all_function (n - 1)
    in
    let functions_size_n =
      functions_size_n_minus_one |> Set_typed_functions.to_list
      |> List.map (fun functions -> List.map (fun f -> f :: functions) all_function)
      |> List.flatten |> Set_typed_functions.of_list
    in
    let functions_lesser_size_than_n =
      Set_typed_functions.union functions_lesser_size_than_n_minus_one functions_size_n_minus_one
    in
    (functions_lesser_size_than_n, functions_size_n)

let generate_tuple_of_symbols_of_size_leq_than_n
    (considered_functions : Typed_function.t list)
    (n : int) : Set_typed_functions.t =
  let functions_size_less_than_n, functions_size_n =
    generate_tuple_of_symbols_of_size_less_than_n_and_n considered_functions n
  in
  Set_typed_functions.union functions_size_less_than_n functions_size_n

let generate_every_pattern_of_depth_0 (env : Datatype_environment.t) (vars : Set_typed_symbol.t) :
    Set_pattern.t =
  let every_constructor = Datatype_environment.get_constructors env in
  let constructors_with_no_argument =
    Set_typed_function.filter (fun rs -> Typed_function.get_arity rs = 0) every_constructor
  in
  let closed_patterns_of_depth_0 =
    constructors_with_no_argument |> Set_typed_function.to_list
    |> List.map (fun rs -> Pattern.create rs [])
    |> Set_pattern.of_list
  in
  let vars_as_patterns =
    Set_typed_symbol.fold (fun v -> Set_pattern.add (Pattern.from_var v)) vars Set_pattern.empty
  in
  let patterns_of_depth_0 = Set_pattern.union vars_as_patterns closed_patterns_of_depth_0 in

  patterns_of_depth_0

let generate_every_pattern_of_depth_plus_one (env : Datatype_environment.t) (from : Set_pattern.t) :
    Set_pattern.t =
  let every_constructor = Datatype_environment.get_constructors env in
  let every_constructor_with_args =
    Set_typed_function.filter (fun rs -> Typed_function.get_arity rs > 0) every_constructor
  in

  let build_every_term_for_one_constructor (ts : Typed_function.t) =
    let input_types = Typed_function.get_input_types ts in
    (* let arity = Typed_function.get_arity rs in *)
    let list_of_parameters_for_each_arg_position =
      List.map
        (fun input_type ->
          let patterns_of_the_correct_type =
            Set_pattern.filter (fun p -> Datatype.equal (Pattern.get_type p) input_type) from
          in
          Set_pattern.to_list patterns_of_the_correct_type)
        input_types
    in

    let args = Misc.List_op.cartesian_product_list list_of_parameters_for_each_arg_position in

    let new_patterns_for_this_constructor =
      args |> List.map (Pattern.create ts) |> Set_pattern.of_list
    in
    new_patterns_for_this_constructor
  in
  let new_patterns_for_each_constructor =
    every_constructor_with_args |> Set_typed_function.to_list
    |> List.map build_every_term_for_one_constructor
    |> Set_pattern.list_union
  in
  new_patterns_for_each_constructor

let generate_every_pattern_of_depth_leq_than
    (env : Datatype_environment.t)
    (vars : Set_typed_symbol.t)
    (depth : int) : Set_pattern.t =
  let patterns_of_depth_0 = generate_every_pattern_of_depth_0 env vars in
  let accumulator patterns =
    Set_pattern.union patterns (generate_every_pattern_of_depth_plus_one env patterns)
  in
  Misc.Others.forn accumulator patterns_of_depth_0 depth

let build_every_tuple_of_patterns_of_some_arity (database_of_terms : Set_pattern.t) (arity : int) :
    Set_patterns.t =
  let replicatas_of_patterns =
    List.init arity (fun _ -> database_of_terms |> Set_pattern.to_list)
  in
  let tuples = Misc.List_op.cartesian_product_list replicatas_of_patterns in
  let convoluted_terms = Set_patterns.of_list tuples in
  convoluted_terms

let max_arity = 3
let max_depth_patterns = 2
let keep_of_each_type = 50

let patterns_of_small_depth () =
  generate_every_pattern_of_depth_leq_than testing_type_env Set_typed_symbol.empty
    max_depth_patterns

let build_every_tuple_of_patterns_term_of_small_depth () =
  let convoluted_terms_of_each_arity =
    List.init max_arity (build_every_tuple_of_patterns_of_some_arity (patterns_of_small_depth ()))
  in

  let every_tuple_of_terms = Set_patterns.list_union convoluted_terms_of_each_arity in
  every_tuple_of_terms

let sample_from_elements (how_many_i_want : int) (l : 'a list) : 'a list =
  let how_many_there_is = List.length l in
  if how_many_there_is = 0 || how_many_i_want = 0 then
    []
  else if how_many_i_want > how_many_there_is then
    l
  else
    let one_every = how_many_there_is / how_many_i_want in
    List.filteri (fun i _ -> i mod one_every = 0) l

let sample_from_patterns_equitable_on_type (terms : Set_patterns.t) (of_every_type : int) :
    Set_patterns.t =
  let types_and_terms =
    terms |> Set_patterns.to_list
    |> List.map (fun t -> (Patterns.get_type t, t))
    |> Misc.List_op.partition_on_first_element Datatypes.compare
  in
  types_and_terms |> List.map snd
  |> List.map (sample_from_elements of_every_type)
  |> List.flatten |> Set_patterns.of_list

let many_tuple_of_patterns () =
  sample_from_patterns_equitable_on_type
    (build_every_tuple_of_patterns_term_of_small_depth ())
    keep_of_each_type

let max_arity_functions = 2

let some_typed_functions () =
  generate_tuple_of_symbols_of_size_leq_than_n
    (Set_typed_function.to_list (Datatype_environment.get_constructors testing_type_env))
    max_arity_functions
