open Aliases
open Database_type_environment
open Database_pattern

let c_ = []
let c_nat = [nat]

(* let c_nattree = [nat_tree] *)
let c_nat_natlist = [nat; natlist]

(* let c_nat_nattree = [nat; nat_tree] *)
let c_natlist = [natlist]
let c_natlist_nat = [natlist; nat]
let c_nattsil_nat = [nattsil; nat]
let c_natlist_natlist = [natlist; natlist]
let c_nat_nat = [nat; nat]
let c_natlist_pair = [natlist; pair_nat_nat]
let c_nat_natlist_pair = [nat; natlist; pair_nat_nat]
let c_nat_pair = [nat; pair_nat_nat]
let c_pair = [pair_nat_nat]

let some_convoluted_datatypes =
  Set_datatypes.of_list [c_nat; c_nat_natlist; c_natlist; c_natlist_nat; c_nattsil_nat; c_nat_nat]

let cons_s = [cons_symbol; s_symbol]
let nil_z = [nil_symbol; z_symbol]
let snoc_s = [snoc_symbol; s_symbol]
let lin_z = [lin_symbol; z_symbol]
let s_cons = [s_symbol; cons_symbol]
let z_nil_pair = [z_symbol; nil_symbol; pair_symbol]

(* patterns *)
(* tuple of patterns *)
let patterns_1 = []
let patterns_2 = [p_nil]
let patterns_3 = [p_consznil]
let patterns_4 = [p_consznil; p_consznil; p_consznil]
let patterns_consznil_sz = [p_consznil; p_sz]
let patterns_nil = [p_nil]
let patterns_z_z = [p_z; p_z]
let patterns_z = [p_z]
let patterns_sz = [p_sz]
let patterns_nil_sz = [p_nil; p_sz]
let patterns_nil_z = [p_nil; p_z]
let patterns_consznil_z = [p_consznil; p_z]
let patterns_conssznil_z = [p_conssznil; p_sz]
let patterns_consznil_conssznil = [p_consznil; p_conssznil]
