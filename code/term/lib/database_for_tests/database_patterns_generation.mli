val many_tuple_of_patterns : unit -> Aliases.Set_patterns.t
val some_typed_functions : unit -> Aliases.Set_typed_functions.t
val patterns_of_small_depth : unit -> Aliases.Set_pattern.t

val generate_every_pattern_of_depth_leq_than :
  Datatype_environment.t -> Aliases.Set_typed_symbol.t -> int -> Aliases.Set_pattern.t
