open Aliases
open Database_pattern
open Database_type_environment

(* Substitutions *)
let sub_empty : Substitution.t = Substitution.empty
let sub_n_to_z = Substitution.add n p_z sub_empty
let sub_n_to_sn = Substitution.add n p_sn sub_empty
let sub_n_to_sn_and_x_to_ssz = Substitution.add x p_ssz sub_n_to_sn
let sub_n_to_sx = Substitution.add n p_sx sub_empty
let sub_x_to_sz = Substitution.add x p_sz sub_empty
let sub_n_to_sz = Substitution.add n p_sz sub_empty
let sub_n_to_ssz = Substitution.add n p_ssz sub_empty
let sub_l_to_nil = Substitution.add l p_nil sub_empty
let sub_x_to_z_and_n_to_sx = Substitution.add x p_z sub_n_to_sx
let sub_x_to_z = Substitution.add x p_z sub_empty
let sub_n_to_sx_and_x_to_z = Substitution.add n p_sx sub_x_to_z
let sub_n_to_sx_and_l_to_nil = Substitution.add l p_nil sub_n_to_sx
let sub_n_to_z_and_l_to_nil = Substitution.add l p_nil sub_n_to_z
let sub_n_to_sz_and_x_to_z = Substitution.add n p_sz sub_x_to_z
let sub_n_to_sz_and_x_to_sz = Substitution.add n p_sz sub_x_to_sz
let sub_n_to_sssx = Substitution.add n p_sssx sub_empty

let some_non_circular_substitution =
  [
    sub_empty;
    sub_n_to_z;
    sub_n_to_sx;
    sub_n_to_ssz;
    sub_x_to_sz;
    sub_l_to_nil;
    sub_x_to_z_and_n_to_sx;
    sub_x_to_z;
    sub_n_to_sx_and_x_to_z;
    sub_n_to_sx_and_l_to_nil;
    sub_n_to_z_and_l_to_nil;
    sub_n_to_sssx;
    sub_n_to_sz_and_x_to_sz;
  ]

let some_substitution = [sub_n_to_sn; sub_n_to_sn_and_x_to_ssz] @ some_non_circular_substitution

let constraints_of_sub_n_to_sx_and_l_to_nil =
  Map_typedSymbol.of_list [(n, s_symbol); (l, nil_symbol)]

let constraints_of_sub_n_to_z_and_l_to_nil =
  Map_typedSymbol.of_list [(n, z_symbol); (l, nil_symbol)]

let constraints_of_sub_n_to_sx_and_x_to_z = Map_typedSymbol.of_list [(n, s_symbol); (x, z_symbol)]
let constraints_12 = None
let constraints_13 = Some (Map_typedSymbol.of_list [(n, s_symbol); (l, nil_symbol); (x, z_symbol)])
