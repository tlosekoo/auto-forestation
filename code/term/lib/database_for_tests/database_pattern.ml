open Aliases
open Database_type_environment

(* Variables *)
let x = Typed_symbol.create (Symbol.create "x") nat
let x1 = Typed_symbol.create (Symbol.create "x1") nat
let x2 = Typed_symbol.create (Symbol.create "x2") nat
let x3 = Typed_symbol.create (Symbol.create "x3") nat
let y = Typed_symbol.create (Symbol.create "y") nat
let y1 = Typed_symbol.create (Symbol.create "y1") nat
let y2 = Typed_symbol.create (Symbol.create "y2") nat
let y3 = Typed_symbol.create (Symbol.create "y3") nat
let n = Typed_symbol.create (Symbol.create "n") nat
let n1 = Typed_symbol.create (Symbol.create "n1") nat
let n2 = Typed_symbol.create (Symbol.create "n2") nat
let n3 = Typed_symbol.create (Symbol.create "n3") nat
let n4 = Typed_symbol.create (Symbol.create "n4") nat
let n5 = Typed_symbol.create (Symbol.create "n5") nat
let n6 = Typed_symbol.create (Symbol.create "n6") nat
let n7 = Typed_symbol.create (Symbol.create "n7") nat
let n8 = Typed_symbol.create (Symbol.create "n8") nat
let n9 = Typed_symbol.create (Symbol.create "n9") nat
let t = Typed_symbol.create (Symbol.create "t") elt_tree
let t1 = Typed_symbol.create (Symbol.create "t1") elt_tree
let t2 = Typed_symbol.create (Symbol.create "t2") elt_tree
let t3 = Typed_symbol.create (Symbol.create "t3") elt_tree
let t4 = Typed_symbol.create (Symbol.create "t4") elt_tree
let t5 = Typed_symbol.create (Symbol.create "t5") elt_tree
let t6 = Typed_symbol.create (Symbol.create "t6") elt_tree
let t7 = Typed_symbol.create (Symbol.create "t7") elt_tree
let t8 = Typed_symbol.create (Symbol.create "t8") elt_tree
let t9 = Typed_symbol.create (Symbol.create "t9") elt_tree
let l = Typed_symbol.create (Symbol.create "l") natlist
let l1 = Typed_symbol.create (Symbol.create "l1") natlist
let l2 = Typed_symbol.create (Symbol.create "l2") natlist
let l3 = Typed_symbol.create (Symbol.create "l3") natlist
let e = Typed_symbol.create (Symbol.create "e") elt
let e1 = Typed_symbol.create (Symbol.create "e1") elt
let e2 = Typed_symbol.create (Symbol.create "e2") elt
let e3 = Typed_symbol.create (Symbol.create "e3") elt
let some_variables = Set_typed_symbol.of_list [x; l; e]

(* Patterns *)
let p_z = Pattern.create z_symbol []
let p_e = Pattern.from_var e
let p_a = Pattern.create a_symbol []
let p_b = Pattern.create b_symbol []
let p_custom_a = Pattern.create custom_a_symbol []
let p_custom_b = Pattern.create custom_b_symbol []
let p_custom_c = Pattern.create custom_c_symbol []
let p_nil = Pattern.create nil_symbol []
let p_lin = Pattern.create lin_symbol []
let p_x = Pattern.from_var x
let p_y = Pattern.from_var y
let p_n = Pattern.from_var n
let p_n1 = Pattern.from_var n1
let p_n2 = Pattern.from_var n2
let p_n3 = Pattern.from_var n3
let p_n4 = Pattern.from_var n4
let p_n5 = Pattern.from_var n5
let p_n6 = Pattern.from_var n6
let p_n7 = Pattern.from_var n7
let p_n8 = Pattern.from_var n8
let p_n9 = Pattern.from_var n9
let p_l = Pattern.from_var l
let p_t = Pattern.from_var t
let p_t1 = Pattern.from_var t1
let p_t2 = Pattern.from_var t2
let p_t3 = Pattern.from_var t3
let p_t4 = Pattern.from_var t4
let p_t5 = Pattern.from_var t5
let p_t6 = Pattern.from_var t6
let p_t7 = Pattern.from_var t7
let p_t8 = Pattern.from_var t8
let p_t9 = Pattern.from_var t9
let p_sx = Pattern.create s_symbol [p_x]
let p_sy = Pattern.create s_symbol [p_y]
let p_cons_sx_l = Pattern.create cons_symbol [p_sx; p_l]
let p_ssx = Pattern.create s_symbol [p_sx]
let p_znil = Pattern.create cons_symbol [p_z; p_nil]
let p_1 = Pattern.create cons_symbol [p_x; p_znil]
let p_2 = Pattern.create cons_symbol [p_z; p_l]
let p_3 = Pattern.create pair_symbol [p_z; p_z]
let p_4 = Pattern.create cons_symbol [p_x; p_l]
let p_5 = Pattern.create cons_symbol [p_ssx; p_l]
let p_sz = Pattern.create s_symbol [p_z]
let p_ssz = Pattern.create s_symbol [p_sz]
let p_sssx = Pattern.create s_symbol [p_ssx]
let p_sn = Pattern.create s_symbol [p_n]
let p_sn1 = Pattern.create s_symbol [p_n1]
let p_sn2 = Pattern.create s_symbol [p_n2]
let p_sn3 = Pattern.create s_symbol [p_n3]
let p_consznil = Pattern.create cons_symbol [p_z; p_nil]
let p_conssxnil = Pattern.create cons_symbol [p_sx; p_nil]
let p_conssznil = Pattern.create cons_symbol [p_sz; p_nil]
let p_conssxl = Pattern.create cons_symbol [p_sx; p_l]
let p_consnnil = Pattern.create cons_symbol [p_n; p_nil]
let p_consnl = Pattern.create cons_symbol [p_n; p_l]
let p_consxl = Pattern.create cons_symbol [p_x; p_l]
let p_conszl = Pattern.create cons_symbol [p_z; p_l]
let p_pairnn = Pattern.create pair_symbol [p_n; p_n]
let p_pairnz = Pattern.create pair_symbol [p_n; p_z]
let p_pairzn = Pattern.create pair_symbol [p_z; p_n]
let p_pairzz = Pattern.create pair_symbol [p_z; p_z]
let p_snoclinz = Pattern.create snoc_symbol [p_lin; p_z]
let p_snoclinn = Pattern.create snoc_symbol [p_lin; p_n]
let p_ca = Pattern.create custom_a_symbol []
let p_cb = Pattern.create custom_b_symbol []
let p_cfa = Pattern.create custom_f_symbol [p_ca]
let p_cfb = Pattern.create custom_f_symbol [p_cb]
let p_cffa = Pattern.create custom_f_symbol [p_cfa]
let p_cfffa = Pattern.create custom_f_symbol [p_cffa]
let p_cffffa = Pattern.create custom_f_symbol [p_cfffa]
let p_cfffffa = Pattern.create custom_f_symbol [p_cffffa]
let p_cffffffa = Pattern.create custom_f_symbol [p_cfffffa]
let p_cfffffffa = Pattern.create custom_f_symbol [p_cffffffa]
let p_cffffffffa = Pattern.create custom_f_symbol [p_cfffffffa]
let p_cfffffffffa = Pattern.create custom_f_symbol [p_cffffffffa]
let p_cffffffffffa = Pattern.create custom_f_symbol [p_cfffffffffa]
let p_cfffffffffffa = Pattern.create custom_f_symbol [p_cffffffffffa]
let p_cffffffffffffa = Pattern.create custom_f_symbol [p_cfffffffffffa]
let p_cfffffffffffffa = Pattern.create custom_f_symbol [p_cffffffffffffa]
let p_cffffffffffffffa = Pattern.create custom_f_symbol [p_cfffffffffffffa]
let p_conszconsznil = Pattern.create cons_symbol [p_z; p_consznil]
let p_leaf = Pattern.create elt_leaf_symbol []
let p_nodeleafaleaf = Pattern.create elt_node_symbol [p_leaf; p_a; p_leaf]
let p_nodeleafeleaf = Pattern.create elt_node_symbol [p_leaf; p_e; p_leaf]
let p_nodeleafe_b_leafaleaf = Pattern.create elt_node_symbol [p_nodeleafeleaf; p_b; p_nodeleafaleaf]
let p_leaf = Pattern.create elt_leaf_symbol []
let p_nodeleafaleaf = Pattern.create elt_node_symbol [p_leaf; p_a; p_leaf]
let p_nodeleafbleaf = Pattern.create elt_node_symbol [p_leaf; p_b; p_leaf]
let p_node_nodeleafaleaf_a_leaf = Pattern.create elt_node_symbol [p_nodeleafaleaf; p_a; p_leaf]
let p_node_leaf_a_nodeleafaleaf = Pattern.create elt_node_symbol [p_leaf; p_a; p_nodeleafaleaf]
let p_node_t1_e_t2 = Pattern.create elt_node_symbol [p_t1; p_e; p_t2]

let some_patterns =
  Set_pattern.of_list
    [
      p_x;
      p_l;
      p_n;
      p_z;
      p_nil;
      p_sx;
      p_ssx;
      p_znil;
      p_1;
      p_2;
      p_3;
      p_4;
      p_5;
      p_nodeleafe_b_leafaleaf;
      p_nodeleafeleaf;
    ]

(* let conspair_symbol =
   Typed_function.create (Symbol.create "consp") pairnnlist [pair_nat_nat; pairnnlist] *)

(* let nilpair_symbol = Typed_function.create (Symbol.create "nilp") pairnnlist [] *)
(* let p_subsubgros = Pattern.create nilpair_symbol []
   let p_subgros = Pattern.create conspair_symbol [p_pairnn; p_subsubgros]
   let p_gros = Pattern.create conspair_symbol [p_pairzn; p_subgros] *)
