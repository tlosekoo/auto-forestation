open Database_type_environment
open Database_lists

let relation_n = Typed_relation_symbol.create (Symbol.create "r_n") c_nat
let relation_n_n = Typed_relation_symbol.create (Symbol.create "r_n_n") c_nat_nat
let relation_l_n = Typed_relation_symbol.create (Symbol.create "r_l_n") c_natlist_nat
let relation_rl_n = Typed_relation_symbol.create (Symbol.create "r_rl_n") c_nattsil_nat
let relation_n_l = Typed_relation_symbol.create (Symbol.create "r_n_l") c_nat_natlist
let relation_ = Typed_relation_symbol.create (Symbol.create "r") []
let relation0_n = Typed_relation_symbol.create (Symbol.create "r0_n") c_nat
let relation1_n = Typed_relation_symbol.create (Symbol.create "r1_n") c_nat
let relation2_n = Typed_relation_symbol.create (Symbol.create "r2_n") c_nat
let relation_l_l = Typed_relation_symbol.create (Symbol.create "r_l_l") c_natlist_natlist
let relation_e_n = Typed_relation_symbol.create (Symbol.create "re_n") c_nat
let relation_o_n = Typed_relation_symbol.create (Symbol.create "ro_n") c_nat
let relation_le_l = Typed_relation_symbol.create (Symbol.create "rle_l") c_natlist
let relation_lo_l = Typed_relation_symbol.create (Symbol.create "rlo_l") c_natlist
let relation_l = Typed_relation_symbol.create (Symbol.create "r_l") c_natlist
let relation_t_n = Typed_relation_symbol.create (Symbol.create "r_t_n") [elt_tree; nat]
let relation_c = Typed_relation_symbol.create (Symbol.create "r_c") [custom]
let relation2_c = Typed_relation_symbol.create (Symbol.create "r2_c") [custom]

let some_states =
  [
    relation_n;
    relation_n_n;
    relation_l_n;
    relation_l_n;
    relation_n_l;
    relation_;
    relation_l_l;
    relation_e_n;
    relation_o_n;
    relation_le_l;
    relation_lo_l;
  ]

(* Named relation symbols *)
let length_symbol = Symbol.create "length"
let odd_symbol = Symbol.create "odd"
let even_symbol = Symbol.create "even"
let isnat_symbol = Symbol.create "isnat"
let fcons_symbol = Symbol.create "r_fcons"
let le_symbol = Symbol.create "le"
let plus_symbol = Symbol.create "plus"
let shallower_symbol = Symbol.create "shallower"
let height_symbol = Symbol.create "height"

(* Named relations *)
let relation_length = Typed_relation_symbol.create length_symbol [natlist; nat]
let relation_odd = Typed_relation_symbol.create odd_symbol [nat]
let relation_even = Typed_relation_symbol.create even_symbol [nat]
let relation_isnat = Typed_relation_symbol.create isnat_symbol [nat]
let relation_fcons = Typed_relation_symbol.create fcons_symbol [nat; natlist; natlist]
let relation_le = Typed_relation_symbol.create le_symbol [nat; nat]
let relation_plus = Typed_relation_symbol.create plus_symbol [nat; nat; nat]
let relation_shallower = Typed_relation_symbol.create shallower_symbol [elt_tree; nat]
let relation_height = Typed_relation_symbol.create height_symbol [elt_tree; nat]
