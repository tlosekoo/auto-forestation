module Set_typed_function = Misc.My_set.Make2 (Typed_function)
module Set_set_typed_function = Misc.My_set.Make2 (Misc.My_set.Make2 (Typed_function))
module Set_typed_functions = Misc.My_set.Make2 (Typed_functions)
module Set_typed_symbol = Misc.My_set.Make2 (Typed_symbol)
module Set_typed_relation_symbol = Misc.My_set.Make2 (Typed_relation_symbol)
module Map_typedSymbol = Misc.My_map.Make (Typed_symbol)
module Map_typedFunction = Misc.My_map.Make (Typed_function)
module Map_typedSymbol_typedSymbol = Misc.My_map.Make2 (Typed_symbol) (Typed_symbol)
module Map_typedSymbol_typedFunction = Misc.My_map.Make2 (Typed_symbol) (Typed_function)
module Map_typedRelationSymbol = Misc.My_map.Make (Typed_relation_symbol)

module Map_typedRelationSymbol_typedRelationFunction =
  Misc.My_map.Make2 (Typed_relation_symbol) (Typed_relation_symbol)

(* type typed_symbol_renaming = Map_typedSymbol_typedSymbol.t *)
