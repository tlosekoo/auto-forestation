(** A typed function is the data of 
- a symbol, 
- an output datatype  
- an input list of datatypes
 *)

include Misc.Ordered_and_printable.OP

(** 'pp_parameterized ~complete:true' is prints more details and 'pp_parameterized ~complete:false' is 'pp'. *)
val pp_parameterized : complete:bool -> Format.formatter -> t -> unit

(** Creates a symbol from its symbol and i/o datatypes *)
val create : Symbol.t -> Datatype.t -> Datatypes.t -> t

(** Returns the arity of a typed symbol, i.e. the size of its convoluted input datatype *)
val get_arity : t -> int

(** Returns the symbol of the typed symbol *)
val get_symbol : t -> Symbol.t

(** Returns the output type *)
val get_output_type : t -> Datatype.t

(** Returns the input types (i.e. the types of the arguments of the symbol*)
val get_input_types : t -> Datatypes.t

(** Returns the string representation of some typed symbol *)
val to_string : t -> string
