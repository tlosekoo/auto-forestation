(* A symbol is only the data of a Label, here a string. *)
include Misc.Ordered_and_printable.OP

(** From a name creates a symbol. This name should only use lowercase letters, numbers, and '_'. 
	It should not start with a digit. 
	It should not be the padding symbol's name. 
	In such cases, raise an exception.
	*)
val create : string -> t

(** Get the name of the symbol *)
val to_string : t -> string
