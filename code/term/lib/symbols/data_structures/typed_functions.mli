(** List of typed functions *)
type t = Typed_function.t List.t

include Misc.Ordered_and_printable.OP with type t := t

(** Returns the type of a term whose root symbol is this convoluted symbol *)
val get_output_types : t -> Datatypes.t

(** Returns the type of the subterms of a term whose root symbol is this convoluted symbol (depends on the convolution form) *)
val get_inputs_types : t -> Datatypes.t list

(** 'pp_parameterized ~complete:true' is 'pp_complete' and 'pp_parameterized ~complete:false' is 'pp'. *)
val pp_parameterized : complete:bool -> Format.formatter -> t -> unit

(** Simple pretty-printer *)
val pp : Format.formatter -> t -> unit
