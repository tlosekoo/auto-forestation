type t = {
  symbol : Symbol.t;
  datatypes : Datatypes.t;
}
[@@deriving compare, equal]

let create (symbol : Symbol.t) (datatypes : Datatypes.t) = {symbol; datatypes}
let get_symbol (ts : t) : Symbol.t = ts.symbol
let get_types (ts : t) : Datatypes.t = ts.datatypes
let get_arity (ts : t) : int = List.length (get_types ts)
let to_string (ts : t) : string = ts |> get_symbol |> Symbol.to_string

let pp_parameterized ~(complete : bool) (c : Format.formatter) (ts : t) : unit =
  if complete then
    Format.fprintf c "%a : %a" Symbol.pp (get_symbol ts) Datatypes.pp (get_types ts)
  else
    Symbol.pp c (get_symbol ts)

let pp = pp_parameterized ~complete:false

let from_function (rs : Typed_function.t) : t =
  let output_type = Typed_function.get_output_type rs in
  let input_types = Typed_function.get_input_types rs in
  let predicate_type = List.append input_types [output_type] in
  let name = Typed_function.get_symbol rs in
  let predicate_of_rs = create name predicate_type in
  predicate_of_rs
