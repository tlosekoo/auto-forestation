type t = {
  symbol : Symbol.t;
  output_type : Datatype.t;
  input_types : Datatypes.t;
}
[@@deriving compare, equal]

let create (symbol : Symbol.t) (output_type : Datatype.t) (input_types : Datatypes.t) =
  {symbol; output_type; input_types}

let get_symbol (ts : t) : Symbol.t = ts.symbol
let get_output_type (ts : t) : Datatype.t = ts.output_type
let get_input_types (ts : t) : Datatypes.t = ts.input_types
let get_arity (ts : t) : int = List.length (get_input_types ts)
let to_string (ts : t) : string = ts |> get_symbol |> Symbol.to_string

let pp_parameterized ~(complete : bool) (c : Format.formatter) (ts : t) : unit =
  if complete then
    Format.fprintf c "%a : %a -> %a" Symbol.pp (get_symbol ts) Datatypes.pp (get_input_types ts)
      Datatype.pp (get_output_type ts)
  else
    Symbol.pp c (get_symbol ts)

let pp = pp_parameterized ~complete:false
