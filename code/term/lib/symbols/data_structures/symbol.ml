include Misc.String

let is_char_allowed ~(as_head : bool) (c : char) : bool =
  let char_code = Char.code c in
  let char_is_a_lowercase_letter = char_code >= 97 && char_code <= 97 + 25 in
  let char_is_underscore = Char.equal c '_' in
  let char_is_number = char_code >= 48 && char_code <= 57 in
  let char_ok =
    if as_head then
      char_is_a_lowercase_letter || char_is_underscore
    else
      char_is_a_lowercase_letter || char_is_underscore || char_is_number
  in
  char_ok

let check_that_name_is_allowed (s : string) : unit =
  if String.length s = 0 then
    ()
  else
    let head = String.get s 0 in
    let head_ok = is_char_allowed ~as_head:true head in
    let body_ok = String.for_all (is_char_allowed ~as_head:false) s in
    if head_ok && body_ok then
      ()
    else
      raise
        (Invalid_argument
           ("Cannot create symbol " ^ s
          ^ "because only allowed characters are lowercase letters, numbers, and '_'. Name cannot start by a number either."
           ))

let create (c : string) : t =
  let () = check_that_name_is_allowed c in
  c

let to_string (s : t) = s
