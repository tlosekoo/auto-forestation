include Misc.List_maker.Make (Typed_function)

let get_output_types = List.map Typed_function.get_output_type
let get_inputs_types = List.map Typed_function.get_input_types

let pp_parameterized ~(complete : bool) (c : Format.formatter) (typed_functions : t) : unit =
  if complete then
    Format.fprintf c "<%a>: %a"
      (Printing.pp_list Typed_function.pp)
      typed_functions Datatypes.pp
      (get_output_types typed_functions)
  else
    Format.fprintf c "<%a>" (Printing.pp_list Typed_function.pp) typed_functions

let pp = pp_parameterized ~complete:false
