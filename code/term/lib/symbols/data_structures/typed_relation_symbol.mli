(** A typed relation symbol is the data of 
- a symbol, 
- a tuple of datatypes
 *)

include Misc.Ordered_and_printable.OP

(** 'pp_parameterized ~complete:true' is prints more details and 'pp_parameterized ~complete:false' is 'pp'. *)
val pp_parameterized : complete:bool -> Format.formatter -> t -> unit

(** Creates a symbol from its symbol and i/o datatypes *)
val create : Symbol.t -> Datatypes.t -> t

(** Returns the arity of a typed symbol, i.e. the size of its convoluted input datatype *)
val get_arity : t -> int

(** Returns the symbol of the typed symbol *)
val get_symbol : t -> Symbol.t

(** Returns the datatype *)
val get_types : t -> Datatypes.t

(** Returns the string representation of some typed symbol *)
val to_string : t -> string

(** Transform a function of arity n into a predicate of arity n+1 by appending the function's return type to its inputs *)
val from_function : Typed_function.t -> t
