type t = {
  symbol : Symbol.t;
  datatype : Datatype.t;
}
[@@deriving compare, equal]

let create (symbol : Symbol.t) (datatype : Datatype.t) = {symbol; datatype}
let get_symbol (ts : t) : Symbol.t = ts.symbol
let get_type (ts : t) : Datatype.t = ts.datatype
let to_string (ts : t) : string = ts |> get_symbol |> Symbol.to_string

let pp_parameterized ~(complete : bool) (c : Format.formatter) (ts : t) : unit =
  if complete then
    Format.fprintf c "%a : %a" Symbol.pp (get_symbol ts) Datatype.pp (get_type ts)
  else
    Symbol.pp c (get_symbol ts)

let pp = pp_parameterized ~complete:false
