(** A typed relation symbol is the data of 
- a symbol, 
- a datatype
 *)

include Misc.Ordered_and_printable.OP

(** 'pp_parameterized ~complete:true' is prints more details and 'pp_parameterized ~complete:false' is 'pp'. *)
val pp_parameterized : complete:bool -> Format.formatter -> t -> unit

(** Creates a symbol from its symbol and datatype *)
val create : Symbol.t -> Datatype.t -> t

(** Returns the symbol of the typed symbol *)
val get_symbol : t -> Symbol.t

(** Returns the datatype *)
val get_type : t -> Datatype.t

(** Returns the string representation of some typed symbol *)
val to_string : t -> string
