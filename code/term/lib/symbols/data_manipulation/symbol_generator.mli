(** This file implements the generation of new variables. *)

(** Given a datatype, generates a typed symbol with a fresh name *)
val generate_fresh_typed_symbol : ?prefix:string -> Datatype.t -> Typed_symbol.t

(** Given a tuple of datatypes, generates a typed symbol with a fresh name *)
val generate_fresh_typed_relation_symbol : ?prefix:string -> Datatypes.t -> Typed_relation_symbol.t

(** Given a typed symbol and a list of datatypes, generate one child typed symbol for each child datatype. 
    The names of the typed syùbol are determined by the name of the input typed symbol. *)
val generate_children : Typed_symbol.t -> Datatype.t list -> Typed_symbol.t list

val reset_counter : unit -> unit
