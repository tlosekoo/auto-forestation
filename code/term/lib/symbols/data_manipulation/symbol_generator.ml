(* Variable generator : this file generates variables of the form '_a', '_b', ..., '_aa'.. *)

module Counter = Misc.Counter

let ascii_value_of_a = 97
let length_alphabet = 26
let cpt_variable_generator : Counter.counter list ref = ref [Counter.get_new_counter ()]

let generate_name_from_counters () : string =
  let cpt_values = List.map Counter.get_value_counter !cpt_variable_generator in
  let ord_chars = List.map (( + ) ascii_value_of_a) cpt_values in
  let chars = List.map Char.chr ord_chars in
  List.fold_left (fun s c -> s ^ String.make 1 c) "" chars

let rec increment_counters (current_value : Counter.counter list) : Counter.counter list =
  match current_value with
  | [] -> [Counter.get_new_counter ()]
  | e :: l ->
      if Counter.get_value_counter e = length_alphabet - 1 then
        let () = Counter.reset e in
        e :: increment_counters l
      else
        let _ = Counter.get_and_incr_counter e in
        e :: l

let increment_cpt_variable_generator () : unit =
  cpt_variable_generator := increment_counters !cpt_variable_generator

let generate_fresh_typed_symbol ?(prefix = "_") (datatype : Datatype.t) : Typed_symbol.t =
  (* let () =
       Format.fprintf Format.std_formatter "\n\ngenerating new variable! with counter: %a\n\n"
         (Printing.pp_list_and_brackets Counter.pp)
         !cpt_variable_generator
     in *)
  let name = prefix ^ generate_name_from_counters () in
  let () = increment_cpt_variable_generator () in
  Typed_symbol.create (Symbol.create name) datatype

(* Generates variables for children. Given a reference variable 'x', its ith children is 'xi' (starting by 0) *)
let new_symbol_for_ith_child (var : Typed_symbol.t) (i : int) (datatype_child : Datatype.t) :
    Typed_symbol.t =
  let new_name = Typed_symbol.to_string var ^ "_" ^ string_of_int i in
  Typed_symbol.create (Symbol.create new_name) datatype_child

let generate_children (var : Typed_symbol.t) (datatype_children : Datatype.t list) :
    Typed_symbol.t list =
  List.mapi (new_symbol_for_ith_child var) datatype_children

let generate_fresh_typed_relation_symbol ?(prefix = "_") (datatype : Datatypes.t) :
    Typed_relation_symbol.t =
  let name = prefix ^ generate_name_from_counters () in
  let () = increment_cpt_variable_generator () in
  Typed_relation_symbol.create (Symbol.create name) datatype

let reset_counter () = cpt_variable_generator := [Counter.get_new_counter ()]
