include List
include Misc.List_maker.Make (Datatype)

let pp (c : Format.formatter) (cd : t) =
  Format.fprintf c "<%a>" (Printing.pp_list_sep " * " Datatype.pp) cd

let pp_no_special_char (c : Format.formatter) (cd : t) =
  Format.fprintf c "%a" (Printing.pp_list_sep "_x_" Datatype.pp) cd

let to_string_no_special_char (cd : t) =
  List.fold_left
    (fun str_previous_datatypes datatype ->
      str_previous_datatypes ^ "_x_" ^ Datatype.to_string datatype)
    "" cd
