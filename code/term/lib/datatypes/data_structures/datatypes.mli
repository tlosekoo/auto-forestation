(** Datatypes are just a list of datatype. *)

type t = Datatype.t List.t

include Misc.List_maker.S with type x := Datatype.t and type t := t

(** Pretty-print a convoluted datatype using only letters and "_". *)
val pp_no_special_char : Format.formatter -> t -> unit

val to_string_no_special_char : t -> string
