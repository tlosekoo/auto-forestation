open Aliases
module Map_datatype_to_typed_function = Misc.My_map.Make2 (Datatype) (Typed_function)
module Map_datatype = Misc.My_map.Make (Datatype)
(* Not optimized. Is it a problem? *)

type partial_default_values = Pattern.t Option.t Map_datatype.t

include Misc.My_map.Make2 (Datatype) (Pattern)

let build_default_value
    (map_default_constructor : partial_default_values)
    (constructor : Typed_function.t) : Pattern.t option =
  let subtypes = Typed_function.get_input_types constructor in
  let subvalues_opt_list =
    List.map (fun subtype -> Map_datatype.find subtype map_default_constructor) subtypes
  in
  let subvalues_list_opt = Misc.List_op.convert_list_of_opt_into_opt_list subvalues_opt_list in
  Option.map (Pattern.create constructor) subvalues_list_opt

let use_constructor_with_already_defined_subtypes_defaults
    (datatypes_definition : Datatype_environment_core.all_datatype_definition)
    (partial_map : partial_default_values)
    (datatype : Datatype.t) : Pattern.t option =
  let constructors =
    Datatype_environment_core.Map_datatype_to_set_typed_function.find datatype datatypes_definition
  in
  let constructors_with_every_subtype_defined =
    Set_typed_function.filter
      (fun constructor ->
        let subtypes = Typed_function.get_input_types constructor in
        List.for_all
          (fun subtype -> Option.is_some (Map_datatype.find subtype partial_map))
          subtypes)
      constructors
  in
  let constructor_opt = Set_typed_function.choose_opt constructors_with_every_subtype_defined in
  let pattern_opt = Option.bind constructor_opt (build_default_value partial_map) in
  pattern_opt

let step_augment_default_values
    (datatypes_definition : Datatype_environment_core.all_datatype_definition)
    (partial_map : partial_default_values) : partial_default_values =
  partial_map |> Map_datatype.to_list
  |> List.map (fun (datatype, pattern_opt) ->
         ( datatype,
           match pattern_opt with
           | Some _ -> pattern_opt
           | None ->
               use_constructor_with_already_defined_subtypes_defaults datatypes_definition
                 partial_map datatype ))
  |> Map_datatype.of_list

let build_default_values_rec
    (datatypes_definition : Datatype_environment_core.all_datatype_definition) =
  Misc.Others.fixpoint_from
    (Map_datatype.equal (Option.equal Pattern.equal))
    (step_augment_default_values datatypes_definition)

let build_default_values (datatypes_definition : Datatype_environment_core.all_datatype_definition)
    : t =
  let datatypes_to_build_default_value_for =
    Datatype_environment_core.Map_datatype_to_set_typed_function.domain datatypes_definition
    |> Set_datatype.to_list
    |> List.map (fun d -> (d, None))
  in
  let partial_map = Map_datatype.of_list datatypes_to_build_default_value_for in
  let default_values_opt = build_default_values_rec datatypes_definition partial_map in
  default_values_opt |> Map_datatype.to_list |> List.map (Misc.Pair.map_snd Option.get) |> of_list

(* We suppose that every datatype has at least one value *)
let groundify_variable (default_values : t) (var : Typed_symbol.t) : Pattern.t =
  let datatype_var = Typed_symbol.get_type var in
  find datatype_var default_values
