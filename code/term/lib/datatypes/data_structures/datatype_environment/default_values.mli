(* Searches for a smallest default value for every datatype *)

type t = Misc.My_map.Make2(Datatype)(Pattern).t

include Misc.Ordered_and_printable.OP with type t := t

val build_default_values : Datatype_environment_core.all_datatype_definition -> t
val groundify_variable : t -> Typed_symbol.t -> Pattern.t
