module Map_datatype_to_typed_function = Misc.My_map.Make2 (Datatype) (Typed_function)
module Map_datatype = Misc.My_map.Make (Datatype)
(* Not optimized. Is it a problem? *)

type partial_default_values = Pattern.t Option.t Map_datatype.t

include Misc.My_map.Make2 (Datatype) (Pattern)
