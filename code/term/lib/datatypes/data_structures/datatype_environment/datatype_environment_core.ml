module Set_list_typed_function = Misc.My_set.Make2 (Misc.List_maker.Make (Typed_function))

module Map_datatype_to_set_typed_function =
  Misc.My_map.Make2 (Datatype) (Misc.My_set.Make2 (Typed_function))

module Set_datatype = Misc.My_set.Make2 (Datatype)
module Datatype_definition = Misc.Pair.Make (Datatype) (Misc.My_set.Make2 (Typed_function))
module Set_typed_functions = Misc.My_set.Make2 (Typed_functions)
module Set_typed_function = Misc.My_set.Make2 (Typed_function)

type datatype_definition = Datatype_definition.t
type all_datatype_definition = Map_datatype_to_set_typed_function.t

(* include Map_datatype_to_set_typed_function *)
type t = {
  map_datatype_constructors : Map_datatype_to_set_typed_function.t;
  default_values : Default_values_core.t;
}
[@@deriving equal, compare]

let pp (formatter : Format.formatter) (env : t) =
  Map_datatype_to_set_typed_function.pp formatter env.map_datatype_constructors

let pp_default_values (formatter : Format.formatter) (env : t) =
  Default_values_core.pp formatter env.default_values

let pp_param ~(see_default_values : bool) (formatter : Format.formatter) (env : t) =
  pp formatter env;
  if see_default_values then
    pp_default_values formatter env

let get_map_datatype_constructors (env : t) = env.map_datatype_constructors
let get_default_values (env : t) = env.default_values

let get_constructors (env : t) : Aliases_symbol.Set_typed_function.t =
  env.map_datatype_constructors |> Map_datatype_to_set_typed_function.codomain
  |> Aliases_symbol.Set_set_typed_function.to_seq |> Aliases_symbol.Set_typed_function.seq_union

let get_datatypes env =
  Map_datatype_to_set_typed_function.domain (get_map_datatype_constructors env)

let get_constructors_of_datatype (env : t) datatype =
  Map_datatype_to_set_typed_function.find datatype (get_map_datatype_constructors env)
