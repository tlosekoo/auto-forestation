open Datatype_environment_core

let build_datatype_definition
    (symbols : Aliases_symbol.Set_typed_function.t)
    (datatype : Datatype.t) : Datatype_definition.t =
  let symbols_of_this_datatype =
    Aliases_symbol.Set_typed_function.filter
      (fun rs -> rs |> Typed_function.get_output_type |> Datatype.equal datatype)
      symbols
  in
  (datatype, symbols_of_this_datatype)

let build (constructor_symbols : Aliases_symbol.Set_typed_function.t) : t =
  let map_datatype_constructors =
    constructor_symbols |> Aliases_symbol.Set_typed_function.to_seq
    |> Seq.map Typed_function.get_output_type
    |> Seq.map (build_datatype_definition constructor_symbols)
    |> Map_datatype_to_set_typed_function.of_seq
  in
  let default_values = Default_values.build_default_values map_datatype_constructors in
  {map_datatype_constructors; default_values}

let get_reachable_datatypes_one_step (datatype_env : t) (datatypes : Set_datatype.t) :
    Set_datatype.t =
  let definition_of_these_datatypes =
    Map_datatype_to_set_typed_function.project_in datatypes datatype_env.map_datatype_constructors
  in
  let symbols_of_these_datatypes =
    Map_datatype_to_set_typed_function.map_a_set definition_of_these_datatypes datatypes
    |> Aliases_symbol.Set_set_typed_function.to_seq |> Aliases_symbol.Set_typed_function.seq_union
  in
  let datatypes_needed_by_these_symbols =
    symbols_of_these_datatypes |> Aliases_symbol.Set_typed_function.to_seq
    |> Seq.map (fun ts -> ts |> Typed_function.get_input_types |> Set_datatype.of_list)
    |> Set_datatype.seq_union
  in
  datatypes_needed_by_these_symbols

(* Given a datatype T (and a type environement), returns the set of datatypes required by this datatype (for example,
   'natlist' required 'natlist' and 'nat'. *)
let get_type_dependencies (datatype_env : t) (datatype : Datatype.t) : Set_datatype.t =
  let accumator_of_reachable_datatypes datatypes =
    Set_datatype.union datatypes (get_reachable_datatypes_one_step datatype_env datatypes)
  in
  Misc.Others.fixpoint_from Set_datatype.equal accumator_of_reachable_datatypes
    (Set_datatype.singleton datatype)

let extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
    (datatype_env : t)
    (datatypes : Datatypes.t) : Set_typed_functions.t =
  datatypes
  |> List.map (get_constructors_of_datatype datatype_env)
  |> Aliases_symbol.Set_typed_function.cartesian_product |> Set_list_typed_function.to_seq
  |> Set_typed_functions.of_seq

let groundify_variable (env : t) = Default_values.groundify_variable env.default_values
let groundify_pattern (env : t) = Pattern.map_variable_to_pattern (groundify_variable env)

let add_datatype_definition (env : t) (datatype : Datatype.t) (constructors : Set_typed_function.t)
    : t =
  let map_datatype_constructors =
    Map_datatype_to_set_typed_function.add_compatible_binding datatype constructors
      (get_map_datatype_constructors env)
  in
  {env with map_datatype_constructors}
