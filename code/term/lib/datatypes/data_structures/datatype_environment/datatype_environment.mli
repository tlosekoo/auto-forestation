(** This file implements the definition of datatypes. For a given datatype, it can return its constructors. *)

type t

include Misc.Ordered_and_printable.OP with type t := t

val pp_param : see_default_values:bool -> Format.formatter -> t -> unit

(** Returns every defined type in a datatype environment *)
val get_datatypes : t -> Misc.My_set.Make(Datatype).t

(** Returns every constructor of a datatype environment *)
val get_constructors : t -> Misc.My_set.Make(Typed_function).t

(** Retrieves the constructors of some defined datatype. Raises an error if datatype is not defined. *)
val get_constructors_of_datatype : t -> Datatype.t -> Misc.My_set.Make(Typed_function).t

(** From a list of constructors, extract their types and the definition of their types *)
val build : Misc.My_set.Make(Typed_function).t -> t

(** 'get_type_dependencies env dt' returns a list of all the datatypes referenced by 'dt' (and those referenced by those referenced by 'dt', etc.) *)
val get_type_dependencies : t -> Datatype.t -> Misc.My_set.Make(Datatype).t

(** Given a tuple of datatypes 'T' and a datatype environment, returns the set of tuple of symbols whose output type is exactly 'T'. *)
val extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes :
  t -> Datatypes.t -> Misc.My_set.Make(Typed_functions).t

val add_datatype_definition : t -> Datatype.t -> Misc.My_set.Make(Typed_function).t -> t
val groundify_variable : t -> Typed_symbol.t -> Pattern.t
val groundify_pattern : t -> Pattern.t -> Pattern.t
