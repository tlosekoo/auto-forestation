include Symbol

let create (name : string) : t = Symbol.create name
let to_string (datatype : t) : string = to_string datatype
