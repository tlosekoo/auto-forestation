(** A type is simply represented by its name. *)
include Misc.Ordered_and_printable.OP

(** Given a name, creates the corresponding datatype (if name is that of padding representation, creates padding instead) *)
val create : string -> t

(** The string representation of a type. If the type is padding, then it returns the string representation of padding. Else, it returns the type's name. *)
val to_string : t -> string
