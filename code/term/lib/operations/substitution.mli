(** A substitution is the data of a set of mapping '(v, p)' with 'v' a variable and 'p' a pattern. *)
(* type t = Pattern.t Misc.My_map.Make(Variable).t *)

include module type of Aliases_pattern.Map_typedSymbol_pattern
(* with type 'a t := 'a Misc.My_map.Make(Variable).t *)

(* type t = Pattern.t Misc.My_map.Make(Variable).t *)
type binding = Typed_symbol.t * Pattern.t

val equal_binding : binding -> binding -> bool
val compare_binding : binding -> binding -> int

(** From a sequence of bindings, creates a substitution. It is checked that, for each binding, the pattern's type is a subtype of the variable's type.
   	It is also checked that no variable is defined twice *)
val create_of_seq : binding Seq.t -> t

(** From a list of bindings, creates a substitution. It is checked that, for each binding, the pattern's type is a subtype of the variable's type.
   				It is also checked that no variable is defined twice *)
val create_of_list : binding List.t -> t

(** Adds a binding to a substitution. Duplication of definiton and type checking are done. *)
val add : Typed_symbol.t -> Pattern.t -> t -> t

(** Given a variable and substitution, returns the variable if no binding for this variable
   	is in the substitution, or else returns the associated pattern *)
val apply_on_var : t -> Typed_symbol.t -> Pattern.t

(** Canonically extends the substitution application on patterns *)
val apply_on_pattern : t -> Pattern.t -> Pattern.t

(** Computes the fixpoint of the substitution, if any. Otherwise this function loops.
   	Is used to transform a shallow substitution into a substitution that does not
   	have for image any variable from the domain of the substitution *)
val resolve_links_substitution : t -> t

(** 'is_defined_on v sub' checks whether 'sub' constains an entry for 'v'. *)
(* val is_defined_on : Variable.t -> t -> bool *)

(** 'compose s1 s2' corresponds to applying 's1' then 's2'. *)
(* val compose : t -> t -> t *)

(** 'compose s1 s2' corresponds to applying 's1' then 's2'. *)
val compose_with_undefined_is_injection : t -> t -> t
