(** This file implements a dumb unification algorithm *)

module Pattern_pair = struct
  (* for comparison generation *)

  (* The boolean indicates whether this element has already been handled and is now in normal form. It is true iff the pair should not be used anymore. *)
  type t = (Pattern.t * Pattern.t) * Bool.t [@@deriving compare, equal]
end

module Unif = Set.Make (Pattern_pair)

type unification_problem = Unif.t
type pair = Unif.elt

let add_false_to_pair_of_pattern (p1p2 : Pattern.t * Pattern.t) = (p1p2, false)

let _pp_unif_elts =
  Printing.pp_list_and_brackets
    (Printing.pp_pair (Printing.pp_pair Pattern.pp Pattern.pp) Printing.pp_bool)

let is_same_type ((p1, p2) : Pattern.t * Pattern.t) : bool =
  Datatype.equal (Pattern.get_type p1) (Pattern.get_type p2)

let check_if_same_type (pairs : (Pattern.t * Pattern.t) list) : unit =
  let same_type = List.for_all is_same_type pairs in
  if same_type then
    ()
  else
    let diff_type_p1, diff_type_p2 = List.find (fun p -> not (is_same_type p)) pairs in
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Cannot create a unification pair between two patterns of different type (%a and %a)."
           Pattern.pp diff_type_p1 Pattern.pp diff_type_p2)
    in
    raise (Invalid_argument error_message)

let of_list (pairs : (Pattern.t * Pattern.t) list) : Unif.t =
  let () = check_if_same_type pairs in
  Unif.of_list (List.map add_false_to_pair_of_pattern pairs)

let check_eq_for_deletion_rule (((p1, p2), _is_nf) : pair) : bool = Pattern.equal p1 p2

let check_eq_for_decompose_rule (((p1, p2), _is_nf) : pair) : bool =
  if Pattern.is_variable p1 || Pattern.is_variable p2 then
    false
  else
    let root_symbol_p1 = Pattern.get_root_symbol p1 in
    let root_symbol_p2 = Pattern.get_root_symbol p2 in
    Typed_function.equal root_symbol_p1 root_symbol_p2

let check_eq_for_exchange_rule (((p1, p2), _is_nf) : pair) : bool =
  Pattern.is_variable p2 && not (Pattern.is_variable p1)

let check_eq_for_elimination_rule (((p1, p2), is_nf) : pair) : bool =
  if is_nf || not (Pattern.is_variable p1) then
    false
  else
    let p1_as_var = Option.get (Pattern.get_variable_opt p1) in
    let variables_of_p2 = Pattern.get_variables p2 in
    let p1_appearing_in_p2 = not (Aliases_symbol.Set_typed_symbol.mem p1_as_var variables_of_p2) in
    p1_appearing_in_p2

let check_eq_for_conflict_rule (((p1, p2), _is_nf) : pair) : bool =
  if Pattern.is_variable p1 || Pattern.is_variable p2 then
    false
  else
    let root_symbol_p1 = Pattern.get_root_symbol p1 in
    let root_symbol_p2 = Pattern.get_root_symbol p2 in
    not (Typed_function.equal root_symbol_p1 root_symbol_p2)

let check_eq_for_checking_rule (((p1, p2), _is_nf) : pair) : bool =
  if Pattern.is_variable p1 then
    let p1_as_var = Option.get (Pattern.get_variable_opt p1) in
    let variables_of_p2 = Pattern.get_variables p2 in
    Aliases_symbol.Set_typed_symbol.mem p1_as_var variables_of_p2
  else
    false

let apply_deletion_rule (u : unification_problem) : unification_problem =
  let pairs_to_eliminate = Unif.filter check_eq_for_deletion_rule u in
  (* let () = Format.fprintf Format.std_formatter "\nThe following pairs have been selected to be deleted: %a" pp_unif_elts (Unif.elements pairs_to_eliminate) in *)
  let pairs_left = Unif.diff u pairs_to_eliminate in
  pairs_left

let rec apply_decompose_rule (u : unification_problem) : unification_problem =
  let pair_to_decompose_opt = Unif.find_first_opt check_eq_for_decompose_rule u in
  if Option.is_none pair_to_decompose_opt then
    u
  else
    let pair_to_decompose = Option.get pair_to_decompose_opt in
    let (p1, p2), _ = pair_to_decompose in
    let children_p1 = Pattern.get_children p1 in
    let children_p2 = Pattern.get_children p2 in
    let new_pairs = List.map2 (fun p1 p2 -> ((p1, p2), false)) children_p1 children_p2 in
    let to_add = Unif.of_list new_pairs in
    let new_unif = Unif.union to_add (Unif.remove pair_to_decompose u) in
    apply_decompose_rule new_unif

let apply_exchange_rule (u : unification_problem) : unification_problem =
  let pairs_to_exchange = Unif.filter check_eq_for_exchange_rule u in
  let exchanged_pairs =
    Unif.map (fun (p1p2, _) -> (Misc.Pair.swap p1p2, false)) pairs_to_exchange
  in
  Unif.union (Unif.diff u pairs_to_exchange) exchanged_pairs

let apply_eliminate_rule (u : unification_problem) : unification_problem =
  let pair_to_eliminate_opt = Unif.find_first_opt check_eq_for_elimination_rule u in
  if Option.is_none pair_to_eliminate_opt then
    u
  else
    let pair_to_eliminate = Option.get pair_to_eliminate_opt in
    let (p1, p2), _ = pair_to_eliminate in
    let p1_as_var = Option.get (Pattern.get_variable_opt p1) in
    let subtitution_p1_for_p2 = Substitution.add p1_as_var p2 Substitution.empty in
    let unif_without_this_pair = Unif.remove pair_to_eliminate u in
    let unif_after_applying_substitution =
      Unif.map
        (fun ((p1, p2), nf) ->
          ( ( Substitution.apply_on_pattern subtitution_p1_for_p2 p1,
              Substitution.apply_on_pattern subtitution_p1_for_p2 p2 ),
            nf ))
        unif_without_this_pair
    in

    let new_pair_in_normal_form = ((p1, p2), true) in

    Unif.add new_pair_in_normal_form unif_after_applying_substitution

let is_conflict_rule_appliable (u : unification_problem) : bool =
  Unif.exists check_eq_for_conflict_rule u

let is_checking_rule_appliable (u : unification_problem) : bool =
  Unif.exists check_eq_for_checking_rule u

let apply_every_simplification (p : unification_problem) =
  (* let () = Format.fprintf Format.std_formatter "\nApplying every simplification on %a!" pp_unif_elts (Unif.elements p) in *)
  let p1 = apply_eliminate_rule p in
  (* let () = Format.fprintf Format.std_formatter "\nAfter Applying elminate %a!" pp_unif_elts (Unif.elements p1) in *)
  let p2 = apply_decompose_rule p1 in
  (* let () = Format.fprintf Format.std_formatter "\nAfter Applying decompose %a!" pp_unif_elts (Unif.elements p2) in *)
  let p3 = apply_exchange_rule p2 in
  (* let () = Format.fprintf Format.std_formatter "\nAfter Applying echange %a!" pp_unif_elts (Unif.elements p3) in *)
  let p4 = apply_deletion_rule p3 in
  (* let () = Format.fprintf Format.std_formatter "\nAfter Applying deletion %a!" pp_unif_elts (Unif.elements p4) in *)
  p4

let check_for_immediate_unsat (p : unification_problem) =
  is_checking_rule_appliable p || is_conflict_rule_appliable p

let eq_problems_opt = Option.equal Unif.equal

let solved_unification_problem_into_subsitution (solved_p : unification_problem) : Substitution.t =
  Unif.fold
    (fun ((p1, p2), _) -> Substitution.add (Option.get (Pattern.get_variable_opt p1)) p2)
    solved_p Substitution.empty

let solve (p : unification_problem) : Substitution.t option =
  let fun_to_iterate (p_opt : unification_problem option) =
    if Option.is_none p_opt then
      None
    else
      let p = Option.get p_opt in
      if check_for_immediate_unsat p then
        None
      else
        Some (apply_every_simplification p)
  in

  let fixpoint = Misc.Others.fixpoint_from eq_problems_opt fun_to_iterate (Some p) in
  let sub_opt = Option.map solved_unification_problem_into_subsitution fixpoint in
  let sub_opt_resolved = Option.map Substitution.resolve_links_substitution sub_opt in
  sub_opt_resolved
