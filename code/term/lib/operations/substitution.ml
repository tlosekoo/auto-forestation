open Aliases_pattern
include Map_typedSymbol_pattern

(* type t = Pattern.t Misc.My_map.Make2(Variable)(Pattern).t *)
type binding = Typed_symbol.t * Pattern.t [@@deriving compare, equal]

let check_binding_datatype (var : Typed_symbol.t) (pattern : Pattern.t) : unit =
  let datatype_var = Typed_symbol.get_type var in
  let datatype_pattern = Pattern.get_type pattern in
  let pattern_is_same_type_as_var = Datatype.equal datatype_pattern datatype_var in
  if not pattern_is_same_type_as_var then
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Cannot use binding (%a, %a) because pattern %a is not a subtype of the var %a"
           Typed_symbol.pp var Pattern.pp pattern Pattern.pp pattern
           (Typed_symbol.pp_parameterized ~complete:true)
           var)
    in
    raise (Invalid_argument error_message)

let check_substitution_datatype = iter check_binding_datatype

let create_of_seq s =
  let sub = of_seq_if_compatible s in
  let () = check_substitution_datatype sub in
  sub

let create_of_list s =
  let sub = of_list_if_compatible s in
  let () = check_substitution_datatype sub in
  sub

let add k p m =
  let () = check_binding_datatype k p in
  add_compatible_binding k p m

let apply_on_var (s : t) (x : Typed_symbol.t) : Pattern.t =
  let pattern_opt = find_opt x s in
  Option.value ~default:(Pattern.from_var x) pattern_opt

let rec apply_on_pattern (s : t) (p : Pattern.t) : Pattern.t =
  match Pattern.get_variable_opt p with
  | Some var -> apply_on_var s var
  | None ->
      let symbol = Pattern.get_root_symbol p in
      let subterms = Pattern.get_children p in
      let subterms_after_substitution = List.map (apply_on_pattern s) subterms in
      Pattern.create symbol subterms_after_substitution

let resolve_links_substitution_one_step (sub : t) : t = map (apply_on_pattern sub) sub

let resolve_links_substitution (sub : t) : t =
  Misc.Others.fixpoint_from equal resolve_links_substitution_one_step sub

let compose_with_undefined_is_injection (s1 : t) (s2 : t) : t =
  Map_typedSymbol_pattern.merge
    (fun _key p1_opt p2_opt ->
      match p1_opt with
      | None -> p2_opt
      | Some p1 -> Some (apply_on_pattern s2 p1))
    s1 s2

(* let compose (s1 : t) (s2 : t) : t = Map_variable_pattern.map (apply_on_pattern s2) s1 *)
