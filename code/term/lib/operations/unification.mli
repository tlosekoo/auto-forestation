(** This module implements a stupid syntactic unification algorithm *)

(** A unification problem is the data of a list of pairs of patterns. Each pair means that these two patterns must be equal. *)
type unification_problem

(** Generates a unification problem from a list of [pairs of patterns that should be equal] *)
val of_list : (Pattern.t * Pattern.t) list -> unification_problem

(** If the unification problem is unifiable then returns a Most General Unifier and else returns None *)
val solve : unification_problem -> Substitution.t option
