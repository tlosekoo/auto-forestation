open Term
open Aliases

let%test "get datatypes" =
  let datatypes = Datatype_environment.get_datatypes Database_for_tests.testing_type_env in
  let expected_datatypes =
    Set_datatype.of_list
      [
        Database_for_tests.nat;
        Database_for_tests.elt;
        Database_for_tests.natlist;
        Database_for_tests.pair_nat_nat;
        Database_for_tests.nattsil;
        Database_for_tests.elt_tree;
        Database_for_tests.custom;
      ]
  in
  (* Printing.pp_list_and_brackets Datatype.pp Format.std_formatter datatypes ; *)
  Set_datatype.equal datatypes expected_datatypes

let%test "get symbols" =
  let symbols = Datatype_environment.get_constructors Database_for_tests.testing_type_env in
  let expected_symbols =
    Set_typed_function.of_list
      [
        Database_for_tests.a_symbol;
        Database_for_tests.b_symbol;
        Database_for_tests.custom_a_symbol;
        Database_for_tests.custom_b_symbol;
        Database_for_tests.custom_c_symbol;
        Database_for_tests.custom_f_symbol;
        Database_for_tests.custom_g_symbol;
        Database_for_tests.s_symbol;
        Database_for_tests.cons_symbol;
        Database_for_tests.snoc_symbol;
        Database_for_tests.pair_symbol;
        Database_for_tests.z_symbol;
        Database_for_tests.nil_symbol;
        Database_for_tests.lin_symbol;
        Database_for_tests.elt_leaf_symbol;
        Database_for_tests.elt_node_symbol;
      ]
  in
  (* Printing.pp_list_and_brackets Datatype.pp Format.std_formatter datatypes ; *)
  Set_typed_function.equal symbols expected_symbols

let%expect_test "pp" =
  Datatype_environment.pp Format.std_formatter Database_for_tests.testing_type_env;
  [%expect
    "
      {
      custom -> {c_a, c_b, c_c, c_f, c_g}  ;  elt -> {a, b}  ;  elt_tree -> {leaf, node}  ;  nat -> {s, z}  ;  natlist -> {cons, nil}  ;  nattsil -> {lin, snoc}  ;  pair_nat_nat -> {pair}
      }"]

let%test "get constructors of some datatype 1" =
  let constructors =
    Datatype_environment.get_constructors_of_datatype Database_for_tests.testing_type_env
      Database_for_tests.nat
  in
  let expected_constructors =
    Set_typed_function.of_list [Database_for_tests.s_symbol; Database_for_tests.z_symbol]
  in
  Set_typed_function.equal constructors expected_constructors

let%test "get constructors of some datatype 3" =
  try
    let _constructors =
      Datatype_environment.get_constructors_of_datatype Database_for_tests.testing_type_env
        (Datatype.create "hehe")
    in
    false
  with
  | _ -> true

let%test "type dependencies 2" =
  let dependencies =
    Datatype_environment.get_type_dependencies Database_for_tests.testing_type_env
      Database_for_tests.nat
  in
  let expected_dependencies = Set_datatype.of_list [Database_for_tests.nat] in
  Set_datatype.equal dependencies expected_dependencies

let%test "type dependencies 3" =
  let dependencies =
    Datatype_environment.get_type_dependencies Database_for_tests.testing_type_env
      Database_for_tests.natlist
  in
  let expected_dependencies =
    Set_datatype.of_list [Database_for_tests.nat; Database_for_tests.natlist]
  in
  Set_datatype.equal dependencies expected_dependencies

(* let%test "needed_types" =
   let types_needed_for_nat_natlist =
     Datatype_environment.get_all_types_needed_for_some_convoluted_type
       Database_for_tests.some_type_env Convolution.Right Database_for_tests.c_nat_natlist
   in
   let expected =
     Set_datatypes.of_list
       [Database_for_tests.c_nat; Database_for_tests.c_nat_natlist; Database_for_tests.c_natlist]
   in
   Set_datatypes.equal expected types_needed_for_nat_natlist *)

let%expect_test "default values" =
  Datatype_environment.pp_param ~see_default_values:true Format.std_formatter
    Database_for_tests.testing_type_env;
  [%expect
    "
    {
    custom -> {c_a, c_b, c_c, c_f, c_g}  ;  elt -> {a, b}  ;  elt_tree -> {leaf, node}  ;  nat -> {s, z}  ;  natlist -> {cons, nil}  ;  nattsil -> {lin, snoc}  ;  pair_nat_nat -> {pair}
    }{
    custom -> c_a  ;  elt -> a  ;  elt_tree -> leaf  ;  nat -> z  ;  natlist -> nil  ;  nattsil -> lin  ;  pair_nat_nat -> pair(z, z)
    }"]

let%expect_test "groundify patterns" =
  let some_pattern = Database_for_tests.p_cons_sx_l in
  let ground_pattern =
    Datatype_environment.groundify_pattern Database_for_tests.testing_type_env some_pattern
  in
  Format.fprintf Format.std_formatter "Groundifying %a gives %a\n" Pattern.pp some_pattern
    Pattern.pp ground_pattern;
  [%expect "
    Groundifying cons(s(x), l) gives cons(s(z), nil)"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
