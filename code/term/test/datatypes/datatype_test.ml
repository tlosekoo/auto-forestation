open Term

let%test "create some type" =
  try
    let _ = Datatype.create "hey" in
    true
  with
  | _____ -> false

let%test "create some type 2" =
  try
    let _ = Datatype.create "hey yo" in
    false
  with
  | _____ -> true

let%test "equal 2" = Datatype.equal Database_for_tests.nat Database_for_tests.nat = true
let%test "equal 4" = Datatype.equal Database_for_tests.natlist Database_for_tests.nat = false

let%expect_test "pp" =
  Datatype.pp Format.std_formatter Database_for_tests.natlist;
  [%expect {| natlist |}]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
