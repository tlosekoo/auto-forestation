open Term

let%expect_test "pp" =
  Datatypes.pp Format.std_formatter Database_for_tests.c_nat_natlist_pair;
  [%expect {| <nat * natlist * pair_nat_nat> |}]

let%expect_test "pp no special chars" =
  Datatypes.pp_no_special_char Format.std_formatter Database_for_tests.c_nat_natlist_pair;
  [%expect {| nat_x_natlist_x_pair_nat_nat |}]

let%test "get datatypes 1" =
  Database_for_tests.c_nat_natlist = [Database_for_tests.nat; Database_for_tests.natlist]

let%test "get datatypes 3" =
  Database_for_tests.c_nat_natlist_pair
  = [Database_for_tests.nat; Database_for_tests.natlist; Database_for_tests.pair_nat_nat]

let%test "get number of convoluted datatypes 1" = List.length Database_for_tests.c_nat_natlist = 2

let%test "get number of convoluted datatypes 3" =
  List.length Database_for_tests.c_nat_natlist_pair = 3

let%test "pairwise equal 1" =
  Datatypes.equal Database_for_tests.c_nat Database_for_tests.c_nat_nat = false

let%test "pairwise equal 2" =
  Datatypes.equal Database_for_tests.c_natlist_nat Database_for_tests.c_natlist_nat = true

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
