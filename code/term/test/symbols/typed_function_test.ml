open Term

(*  *)
(*  *)
(* Testing Typed_function *)

let symbols = Database_for_tests.some_typed_functions
let%test "arity(cons) = 2" = Typed_function.get_arity Database_for_tests.cons_symbol = 2

let%expect_test "print(cons)" =
  Format.fprintf Format.std_formatter "%a" Typed_function.pp Database_for_tests.cons_symbol;
  [%expect {| cons |}]

let%expect_test "print_complete(cons)" =
  Format.fprintf Format.std_formatter "%a"
    (Typed_function.pp_parameterized ~complete:true)
    Database_for_tests.cons_symbol;
  [%expect {| cons : <nat * natlist> -> natlist |}]

let%expect_test "print_complete(snoc)" =
  Format.fprintf Format.std_formatter "%a"
    (Typed_function.pp_parameterized ~complete:true)
    Database_for_tests.snoc_symbol;
  [%expect {| snoc : <nattsil * nat> -> nattsil |}]

let%test "cons is of correct output type" =
  Typed_function.get_output_type Database_for_tests.cons_symbol = Datatype.create "natlist"

let%test "cons is of correct input type" =
  Typed_function.get_input_types Database_for_tests.cons_symbol
  = [Datatype.create "nat"; Datatype.create "natlist"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
