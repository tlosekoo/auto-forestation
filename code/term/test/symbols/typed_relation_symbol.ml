open Term

let%expect_test "pp" =
  Typed_relation_symbol.pp Format.std_formatter Database_for_tests.relation_l_n;
  [%expect {| r_l_n |}]

let%expect_test "pp complete" =
  Typed_relation_symbol.pp_parameterized ~complete:true Format.std_formatter
    Database_for_tests.relation_l_n;
  [%expect {| r_l_n : <natlist * nat> |}]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
