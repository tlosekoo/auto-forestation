open Term

let%test "create success 1" =
  try
    let _ = Symbol.create "plop" in
    true
  with
  | _ -> false

let%test "create success 2" =
  try
    let _ = Symbol.create "pl0p" in
    true
  with
  | _ -> false

let%test "create failure 1" =
  try
    let _ = Symbol.create "Plop" in
    false
  with
  | _ -> true

let%test "create failure 3" =
  try
    let _ = Symbol.create "o  o" in
    false
  with
  | _ -> true

let%expect_test "pp" =
  Symbol.pp Format.std_formatter (Symbol.create "heh");
  [%expect {| heh |}]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
