open Term
open Aliases

let%test "create success" =
  let _ = Pattern.create Database_for_tests.s_symbol [Database_for_tests.p_n] in
  true

let%test "create failure wrong number of parameters 1" =
  try
    let _ = Pattern.create Database_for_tests.s_symbol [] in
    false
  with
  | _ -> true

let%test "create failure wrong number of parameters 2" =
  try
    let _ =
      Pattern.create Database_for_tests.s_symbol [Database_for_tests.p_n; Database_for_tests.p_n]
    in
    false
  with
  | _ -> true

let%test "create failure wrong type of parameters" =
  try
    let _ = Pattern.create Database_for_tests.s_symbol [Database_for_tests.p_l] in
    false
  with
  | _ -> true

let%test "creates from var ok" =
  let _ = Pattern.from_var Database_for_tests.l in
  true

let%expect_test "pp" =
  Format.fprintf Format.std_formatter "%a" Pattern.pp Database_for_tests.p_5;
  [%expect {| cons(s(s(x)), l) |}]

let%test "depth 1" = Pattern.depth Database_for_tests.p_ssx = 2
let%test "depth 2" = Pattern.depth Database_for_tests.p_a = 0
let%test "depth 2bis" = Pattern.depth Database_for_tests.p_nil = 0
let%test "depth 3" = Pattern.depth Database_for_tests.p_x = 0

let%test "root symbol ok" =
  Typed_function.equal
    (Pattern.get_root_symbol Database_for_tests.p_ssx)
    Database_for_tests.s_symbol

let%test "root symbol on variable" =
  try
    let _ = Pattern.get_root_symbol Database_for_tests.p_x in
    false
  with
  | _ -> true

let%test "root symbol opt ok" =
  Option.equal Typed_function.equal
    (Pattern.get_root_symbol_opt Database_for_tests.p_ssx)
    (Some Database_for_tests.s_symbol)

let%test "root symbol opt on variable" =
  Option.equal Typed_function.equal (Pattern.get_root_symbol_opt Database_for_tests.p_x) None

let%test "get children 1" =
  List.equal Pattern.equal (Pattern.get_children Database_for_tests.p_ssx) [Database_for_tests.p_sx]

let%test "get children 2" =
  List.equal Pattern.equal (Pattern.get_children Database_for_tests.p_x) []

let%test "get children 3" =
  List.equal Pattern.equal (Pattern.get_children Database_for_tests.p_nil) []

let%test "get vars 1" =
  Set_typed_symbol.equal (Pattern.get_variables Database_for_tests.p_znil) Set_typed_symbol.empty

let%test "get vars 2" =
  Set_typed_symbol.equal
    (Pattern.get_variables Database_for_tests.p_ssx)
    (Set_typed_symbol.of_list [Database_for_tests.x])

let%test "get vars 3" =
  Set_typed_symbol.equal
    (Pattern.get_variables Database_for_tests.p_4)
    (Set_typed_symbol.of_list [Database_for_tests.x; Database_for_tests.l])

let%test "get var opt 1" =
  Option.equal Typed_symbol.equal
    (Pattern.get_variable_opt Database_for_tests.p_x)
    (Some Database_for_tests.x)

let%test "get var opt 2" =
  Option.equal Typed_symbol.equal (Pattern.get_variable_opt Database_for_tests.p_sx) None
(* let%test "get symbols from pattern 1" =  Pattern.get_symbols_from_pattern Database_for_tests.p_x = []

   let%test "get symbols from pattern 2" =
     Pattern.get_symbols_from_pattern Database_for_tests.p_ssx = [Database_for_tests.s_symbol]

   let%test "get symbols from pattern 3" =
     List.sort Typed_function.compare (Pattern.get_symbols_from_pattern Database_for_tests.p_znil)
     = List.sort Typed_function.compare
         [Database_for_tests.z_symbol; Database_for_tests.nil_symbol; Database_for_tests.cons_symbol]

   let%test "get symbols from patterns 1" = Pattern.get_symbols_from_patterns [] = []

   let%test "get symbols from patterns 2" =
   Pattern.get_symbols_from_patterns [Database_for_tests.p_ssx] = [Database_for_tests.s_symbol]
*)

let%test "closed 1" = Pattern.is_closed Database_for_tests.p_x = false
let%test "closed 2" = Pattern.is_closed Database_for_tests.p_ssx = false
let%test "closed 3" = Pattern.is_closed Database_for_tests.p_znil = true
let%test "closed 4" = Pattern.is_closed Database_for_tests.p_nil = true
let%test "is_variable 1" = Pattern.is_variable Database_for_tests.p_x = true
let%test "is_variable 2" = Pattern.is_variable Database_for_tests.p_ssx = false
let%test "is_variable 3" = Pattern.is_variable Database_for_tests.p_znil = false
let%test "is_variable 4" = Pattern.is_variable Database_for_tests.p_nil = false

let%test "get type 1" =
  Datatype.equal (Pattern.get_type Database_for_tests.p_ssx) Database_for_tests.nat

let%test "get type 2" =
  Datatype.equal (Pattern.get_type Database_for_tests.p_x) Database_for_tests.nat

let%test "get type 3" =
  Datatype.equal (Pattern.get_type Database_for_tests.p_l) Database_for_tests.natlist

let%test "get type 4" =
  Datatype.equal (Pattern.get_type Database_for_tests.p_znil) Database_for_tests.natlist

let%test "compare_shape 1" = Pattern.compare_shape Database_for_tests.p_x Database_for_tests.p_n = 0

let%test "compare_shape 2" =
  Pattern.compare_shape Database_for_tests.p_ssx Database_for_tests.p_ssz <> 0

let%test "compare_shape 3" =
  Pattern.compare_shape Database_for_tests.p_sn Database_for_tests.p_sz <> 0

let%test "compare_shape 4" =
  Pattern.compare_shape Database_for_tests.p_sn Database_for_tests.p_sx = 0

let%test "compare_shape 5" =
  Pattern.compare_shape Database_for_tests.p_conssxl Database_for_tests.p_consxl <> 0

let%test "compare_shape 6" =
  Pattern.compare_shape Database_for_tests.p_conssxl Database_for_tests.p_conssxl = 0

let%test "compare_shape 7" =
  Pattern.compare_shape Database_for_tests.p_consxl Database_for_tests.p_consnl = 0

let%test "compare_shape 8" =
  Pattern.compare_shape Database_for_tests.p_consxl Database_for_tests.p_conszl <> 0

let%test "get vars dfs 1" =
  Pattern.get_variables_dfs_ordered Database_for_tests.p_conssxl
  = [Database_for_tests.x; Database_for_tests.l]

let%test "get vars dfs 2" =
  Pattern.get_variables_dfs_ordered Database_for_tests.p_pairnn
  = [Database_for_tests.n; Database_for_tests.n]

let%test "get vars dfs 2 no duplicate" =
  Pattern.get_variables_dfs_ordered_no_duplicate Database_for_tests.p_pairnn
  = [Database_for_tests.n]

let%test "get vars dfs 3" = Pattern.get_variables_dfs_ordered Database_for_tests.p_pairzz = []

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
