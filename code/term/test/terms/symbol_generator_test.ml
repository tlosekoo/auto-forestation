open Term

let%test "generates of correct type" =
  let type_of_v_input = Database_for_tests.nat in
  let v = Symbol_generator.generate_fresh_typed_symbol type_of_v_input in
  let type_of_v = Typed_symbol.get_type v in
  type_of_v = type_of_v_input

let%test "generates different" =
  let type_of_v_input = Database_for_tests.nat in
  let v1 = Symbol_generator.generate_fresh_typed_symbol type_of_v_input in
  let v2 = Symbol_generator.generate_fresh_typed_symbol type_of_v_input in
  (not (v1 = v2)) && not (Typed_symbol.to_string v1 = Typed_symbol.to_string v2)

let%expect_test "var children" =
  let v = Typed_symbol.create (Symbol.create "hey") Database_for_tests.nat in
  let datatypes_children =
    [Database_for_tests.nat; Database_for_tests.natlist; Database_for_tests.pair_nat_nat]
  in
  Format.fprintf Format.std_formatter "%a"
    (Printing.pp_list_and_brackets (Typed_symbol.pp_parameterized ~complete:true))
    (Symbol_generator.generate_children v datatypes_children);
  [%expect {| [hey_0 : nat, hey_1 : natlist, hey_2 : pair_nat_nat] |}]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
