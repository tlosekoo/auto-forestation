open Term
(*  *)
(*  *)
(* Testing Patterns. *)

let%test "depth 1" = Patterns.depth Database_for_tests.patterns_consznil_sz = 1
let%test "depth 2" = Patterns.depth Database_for_tests.patterns_1 = 0
let%test "depth 3" = Patterns.depth Database_for_tests.patterns_2 = 0

let%expect_test "pp patterns <cons(z,nil), s(z)>" =
  Patterns.pp Format.std_formatter Database_for_tests.patterns_consznil_sz;
  [%expect "[cons(z, nil), s(z)]"]

let%expect_test "pp right convoluted <cons(z,nil), cons(s(z), nil)>" =
  Patterns.pp Format.std_formatter Database_for_tests.patterns_consznil_conssznil;
  [%expect "[cons(z, nil), cons(s(z), nil)]"]

let%test "get_root_symbol 1" =
  Option.equal Typed_functions.equal
    (Patterns.get_root_symbols Database_for_tests.patterns_1)
    (Some [])

let%test "get_root_symbol 2" =
  Option.equal Typed_functions.equal
    (Patterns.get_root_symbols Database_for_tests.patterns_2)
    (Some [Database_for_tests.nil_symbol])

let%test "get_root_symbol 3" =
  Option.equal Typed_functions.equal
    (Patterns.get_root_symbols Database_for_tests.patterns_3)
    (Some [Database_for_tests.cons_symbol])

let%test "get_root_symbol 4" =
  Option.equal Typed_functions.equal
    (Patterns.get_root_symbols Database_for_tests.patterns_consznil_sz)
    (Some Database_for_tests.cons_s)

let%test "get arity" = Patterns.arity Database_for_tests.patterns_consznil_sz = 2
let%test "arity 2" = Patterns.arity Database_for_tests.patterns_4 = 3
let%test "arity 3" = Patterns.arity Database_for_tests.patterns_1 = 0
let%test "get_type 1" = Datatypes.equal (Patterns.get_type Database_for_tests.patterns_1) []

let%test "get_type 2" =
  Datatypes.equal (Patterns.get_type Database_for_tests.patterns_2) [Database_for_tests.natlist]

let%test "get_type 3" =
  Datatypes.equal
    (Patterns.get_type Database_for_tests.patterns_consznil_sz)
    Database_for_tests.c_natlist_nat

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
