open Term
open Aliases

let a = Database_for_tests.c_nat
let many_tuple_of_patterns = Database_for_tests.many_tuple_of_patterns ()

let%test "pattern generation 1" =
  let some_vars = Set_typed_symbol.of_list [Database_for_tests.n; Database_for_tests.l] in
  let patterns_of_depth_0 =
    Database_for_tests.generate_every_pattern_of_depth_leq_than Database_for_tests.testing_type_env
      some_vars 0
  in
  let expected_patterns_of_depth_0 =
    Set_pattern.of_list
      [
        Database_for_tests.p_l;
        Database_for_tests.p_n;
        Database_for_tests.p_a;
        Database_for_tests.p_b;
        Database_for_tests.p_custom_a;
        Database_for_tests.p_custom_b;
        Database_for_tests.p_custom_c;
        Database_for_tests.p_z;
        Database_for_tests.p_nil;
        Database_for_tests.p_lin;
        Database_for_tests.p_leaf;
      ]
  in

  (* Set_pattern.pp Format.std_formatter patterns_of_depth_0; *)
  Set_pattern.equal patterns_of_depth_0 expected_patterns_of_depth_0

let%test "pattern generation 2" =
  let some_vars = Set_typed_symbol.of_list [Database_for_tests.n; Database_for_tests.l] in
  let patterns_of_depth_0_and_1 =
    Database_for_tests.generate_every_pattern_of_depth_leq_than Database_for_tests.testing_type_env
      some_vars 1
  in
  (* Not custom datatypes, too tedious to write *)
  let patterns_of_depth_0_and_1 =
    Set_pattern.filter
      (fun p -> not (Datatype.equal Database_for_tests.custom (Pattern.get_type p)))
      patterns_of_depth_0_and_1
  in
  let expected_patterns_of_depth_0 =
    Set_pattern.of_list
      [
        Database_for_tests.p_l;
        Database_for_tests.p_n;
        Database_for_tests.p_a;
        Database_for_tests.p_b;
        Database_for_tests.p_z;
        Database_for_tests.p_nil;
        Database_for_tests.p_lin;
        Database_for_tests.p_leaf;
      ]
  in
  let expected_patterns_of_depth_1 =
    Set_pattern.of_list
      [
        Database_for_tests.p_sz;
        Database_for_tests.p_sn;
        Database_for_tests.p_consznil;
        Database_for_tests.p_consnnil;
        Database_for_tests.p_conszl;
        Database_for_tests.p_consnl;
        Database_for_tests.p_snoclinz;
        Database_for_tests.p_snoclinn;
        Database_for_tests.p_pairnn;
        Database_for_tests.p_pairzn;
        Database_for_tests.p_pairnz;
        Database_for_tests.p_pairzz;
        Database_for_tests.p_nodeleafaleaf;
        Database_for_tests.p_nodeleafbleaf;
      ]
  in
  let expected_patterns_of_depth_0_and_1 =
    Set_pattern.union expected_patterns_of_depth_0 expected_patterns_of_depth_1
  in
  (* Set_pattern.pp Format.std_formatter patterns_of_depth_0_and_1; *)
  Set_pattern.equal patterns_of_depth_0_and_1 expected_patterns_of_depth_0_and_1

let%expect_test "sub_n_to_sn_and_x_to_ssz" =
  Substitution.pp Format.std_formatter Database_for_tests.sub_n_to_sn_and_x_to_ssz;
  [%expect "
    {
    n -> s(n)  ;  x -> s(s(z))
    }"]

let%expect_test "all tuples of functions" =
  Format.fprintf Format.std_formatter "some_typed_functions:\n\n%a\n\n" Set_typed_functions.pp
    (Database_for_tests.some_typed_functions ());
  [%expect
    "
    some_typed_functions:

    {<>, <a>, <a, a>, <a, b>, <a, c_a>, <a, c_b>, <a, c_c>, <a, c_f>, <a, c_g>, <a, cons>, <a, leaf>, <a, lin>, <a, nil>, <a, node>, <a, pair>, <a, s>, <a, snoc>, <a, z>, <b>, <b, a>, <b, b>, <b, c_a>, <b, c_b>, <b, c_c>, <b, c_f>, <b, c_g>, <b, cons>, <b, leaf>, <b, lin>, <b, nil>, <b, node>, <b, pair>, <b, s>, <b, snoc>, <b, z>, <c_a>, <c_a, a>, <c_a, b>, <c_a, c_a>, <c_a, c_b>, <c_a, c_c>, <c_a, c_f>, <c_a, c_g>, <c_a, cons>, <c_a, leaf>, <c_a, lin>, <c_a, nil>, <c_a, node>, <c_a, pair>, <c_a, s>, <c_a, snoc>, <c_a, z>, <c_b>, <c_b, a>, <c_b, b>, <c_b, c_a>, <c_b, c_b>, <c_b, c_c>, <c_b, c_f>, <c_b, c_g>, <c_b, cons>, <c_b, leaf>, <c_b, lin>, <c_b, nil>, <c_b, node>, <c_b, pair>, <c_b, s>, <c_b, snoc>, <c_b, z>, <c_c>, <c_c, a>, <c_c, b>, <c_c, c_a>, <c_c, c_b>, <c_c, c_c>, <c_c, c_f>, <c_c, c_g>, <c_c, cons>, <c_c, leaf>, <c_c, lin>, <c_c, nil>, <c_c, node>, <c_c, pair>, <c_c, s>, <c_c, snoc>, <c_c, z>, <c_f>, <c_f, a>, <c_f, b>, <c_f, c_a>, <c_f, c_b>, <c_f, c_c>, <c_f, c_f>, <c_f, c_g>, <c_f, cons>, <c_f, leaf>, <c_f, lin>, <c_f, nil>, <c_f, node>, <c_f, pair>, <c_f, s>, <c_f, snoc>, <c_f, z>, <c_g>, <c_g, a>, <c_g, b>, <c_g, c_a>, <c_g, c_b>, <c_g, c_c>, <c_g, c_f>, <c_g, c_g>, <c_g, cons>, <c_g, leaf>, <c_g, lin>, <c_g, nil>, <c_g, node>, <c_g, pair>, <c_g, s>, <c_g, snoc>, <c_g, z>, <cons>, <cons, a>, <cons, b>, <cons, c_a>, <cons, c_b>, <cons, c_c>, <cons, c_f>, <cons, c_g>, <cons, cons>, <cons, leaf>, <cons, lin>, <cons, nil>, <cons, node>, <cons, pair>, <cons, s>, <cons, snoc>, <cons, z>, <leaf>, <leaf, a>, <leaf, b>, <leaf, c_a>, <leaf, c_b>, <leaf, c_c>, <leaf, c_f>, <leaf, c_g>, <leaf, cons>, <leaf, leaf>, <leaf, lin>, <leaf, nil>, <leaf, node>, <leaf, pair>, <leaf, s>, <leaf, snoc>, <leaf, z>, <lin>, <lin, a>, <lin, b>, <lin, c_a>, <lin, c_b>, <lin, c_c>, <lin, c_f>, <lin, c_g>, <lin, cons>, <lin, leaf>, <lin, lin>, <lin, nil>, <lin, node>, <lin, pair>, <lin, s>, <lin, snoc>, <lin, z>, <nil>, <nil, a>, <nil, b>, <nil, c_a>, <nil, c_b>, <nil, c_c>, <nil, c_f>, <nil, c_g>, <nil, cons>, <nil, leaf>, <nil, lin>, <nil, nil>, <nil, node>, <nil, pair>, <nil, s>, <nil, snoc>, <nil, z>, <node>, <node, a>, <node, b>, <node, c_a>, <node, c_b>, <node, c_c>, <node, c_f>, <node, c_g>, <node, cons>, <node, leaf>, <node, lin>, <node, nil>, <node, node>, <node, pair>, <node, s>, <node, snoc>, <node, z>, <pair>, <pair, a>, <pair, b>, <pair, c_a>, <pair, c_b>, <pair, c_c>, <pair, c_f>, <pair, c_g>, <pair, cons>, <pair, leaf>, <pair, lin>, <pair, nil>, <pair, node>, <pair, pair>, <pair, s>, <pair, snoc>, <pair, z>, <s>, <s, a>, <s, b>, <s, c_a>, <s, c_b>, <s, c_c>, <s, c_f>, <s, c_g>, <s, cons>, <s, leaf>, <s, lin>, <s, nil>, <s, node>, <s, pair>, <s, s>, <s, snoc>, <s, z>, <snoc>, <snoc, a>, <snoc, b>, <snoc, c_a>, <snoc, c_b>, <snoc, c_c>, <snoc, c_f>, <snoc, c_g>, <snoc, cons>, <snoc, leaf>, <snoc, lin>, <snoc, nil>, <snoc, node>, <snoc, pair>, <snoc, s>, <snoc, snoc>, <snoc, z>, <z>, <z, a>, <z, b>, <z, c_a>, <z, c_b>, <z, c_c>, <z, c_f>, <z, c_g>, <z, cons>, <z, leaf>, <z, lin>, <z, nil>, <z, node>, <z, pair>, <z, s>, <z, snoc>, <z, z>}"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
