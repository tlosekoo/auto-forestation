open Term

let problem_1 = [(Database_for_tests.p_l, Database_for_tests.p_nil)]
let solution_1 = Some Database_for_tests.sub_l_to_nil
let problem_2 = [(Database_for_tests.p_x, Database_for_tests.p_n)]

let solution_2_1 =
  Some (Substitution.add Database_for_tests.n Database_for_tests.p_x Substitution.empty)

let solution_2_2 =
  Some (Substitution.add Database_for_tests.x Database_for_tests.p_n Substitution.empty)

let problem_3 =
  [
    (Database_for_tests.p_n, Database_for_tests.p_sx);
    (Database_for_tests.p_x, Database_for_tests.p_sz);
    (Database_for_tests.p_l, Database_for_tests.p_nil);
  ]

let solution_3 =
  Some
    (Substitution.resolve_links_substitution
       (Substitution.add Database_for_tests.l Database_for_tests.p_nil
          (Substitution.add Database_for_tests.x Database_for_tests.p_sz
             Database_for_tests.sub_n_to_sx)))

let problem_4 =
  [
    (Database_for_tests.p_n, Database_for_tests.p_sx);
    (Database_for_tests.p_x, Database_for_tests.p_sn);
  ]

let solution_4 = None
let problems = [problem_1; problem_2; problem_3; problem_4]
let solutions = [[solution_1]; [solution_2_1; solution_2_2]; [solution_3]; [solution_4]]

let check (u : (Pattern.t * Pattern.t) list) (expected_solution : Substitution.t option) : bool =
  let problem = Unification.of_list u in
  let sub_opt = Unification.solve problem in
  (* let () =
       Format.fprintf Format.std_formatter
         "Checking unification problem\n%a\ngot\n%a\nexpected\n%a\n\n"
         (Printing.pp_list_and_brackets (Printing.pp_pair Pattern.pp Pattern.pp))
         u (Printing.pp_opt Substitution.pp) sub_opt (Printing.pp_opt Substitution.pp)
         expected_solution
     in *)
  (Option.equal Substitution.equal) sub_opt expected_solution

let%test "unif 1" = check problem_1 solution_1
let%test "unif 2" = check problem_2 solution_2_1 || check problem_2 solution_2_2
let%test "unif 3" = check problem_3 solution_3
let%test "unif 4" = check problem_4 solution_4

let lemma_solution_is_correct (u : (Pattern.t * Pattern.t) list) : bool =
  let problem = Unification.of_list u in
  let sub_opt = Unification.solve problem in
  if Option.is_some sub_opt then
    let sub = Option.get sub_opt in
    let apply_sub = Substitution.apply_on_pattern sub in
    let pairs_of_patterns_that_should_be_equal =
      List.map (fun (p1, p2) -> (apply_sub p1, apply_sub p2)) u
    in
    List.for_all (fun (p1, p2) -> p1 = p2) pairs_of_patterns_that_should_be_equal
  else
    true

let%test "solutions are correct" = List.for_all lemma_solution_is_correct problems

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
