open Term
open Aliases

let%expect_test "pp" =
  Map_typedSymbol_typedFunction.pp Format.std_formatter
    Database_for_tests.constraints_of_sub_n_to_sx_and_l_to_nil;
  [%expect "
    {
    l -> nil  ;  n -> s
    }"]

let%test "meet works 1" =
  let meet =
    Map_typedSymbol_typedFunction.union_if_compatible_opt
      Database_for_tests.constraints_of_sub_n_to_sx_and_l_to_nil
      Database_for_tests.constraints_of_sub_n_to_z_and_l_to_nil
  in
  (Option.equal Map_typedSymbol_typedFunction.equal) meet Database_for_tests.constraints_12

let%test "meet works 2" =
  let meet =
    Map_typedSymbol_typedFunction.union_if_compatible_opt
      Database_for_tests.constraints_of_sub_n_to_sx_and_l_to_nil
      Database_for_tests.constraints_of_sub_n_to_sx_and_x_to_z
  in
  (Option.equal Map_typedSymbol_typedFunction.equal) meet Database_for_tests.constraints_13

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
