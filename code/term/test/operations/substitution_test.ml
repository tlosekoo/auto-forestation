open Term
(* open Aliases *)

let%test "creation success" =
  let n_to_pz = (Database_for_tests.n, Database_for_tests.p_z) in
  let _ = Substitution.create_of_list [n_to_pz; n_to_pz] in
  true

let%test "creation failure" =
  try
    let n_to_pz = (Database_for_tests.n, Database_for_tests.p_z) in
    let n_to_psz = (Database_for_tests.n, Database_for_tests.p_sz) in
    let _ = Substitution.create_of_list [n_to_pz; n_to_psz] in
    false
  with
  | _ -> true

let%test "creation_fail" =
  try
    let _sub_n_to_sx_and_l_to_z =
      Substitution.add Database_for_tests.l Database_for_tests.p_z Database_for_tests.sub_n_to_sx
    in
    false
  with
  | _ -> true

let%test "eq 1" =
  Substitution.equal Database_for_tests.sub_x_to_z_and_n_to_sx
    Database_for_tests.sub_n_to_sx_and_x_to_z
  = true

let%test "eq 2" =
  Substitution.equal Database_for_tests.sub_n_to_sx Database_for_tests.sub_n_to_sx_and_x_to_z
  = false

let%test "eq 3" =
  Substitution.equal Database_for_tests.sub_n_to_sx_and_l_to_nil
    Database_for_tests.sub_n_to_sx_and_x_to_z
  = false

let sub_1 : Substitution.t =
  let y = Typed_symbol.create (Symbol.create "y") Database_for_tests.nat in
  (* let py = Pattern.from_var y in *)
  let yy = Typed_symbol.create (Symbol.create "yy") Database_for_tests.nat in
  let pyy = Pattern.from_var yy in
  let yyy = Typed_symbol.create (Symbol.create "yyy") Database_for_tests.nat in
  let pyyy = Pattern.from_var yyy in
  let bindings =
    [
      (y, Pattern.create Database_for_tests.s_symbol [pyy]);
      (yy, Pattern.create Database_for_tests.s_symbol [pyyy]);
      (yyy, Database_for_tests.p_z);
    ]
  in
  Substitution.create_of_list bindings

let%expect_test "pp sub_1" =
  Substitution.pp Format.std_formatter sub_1;
  [%expect "
    {
    y -> s(yy)  ;  yy -> s(yyy)  ;  yyy -> z
    }"]

let%test "x_to_0( S(x) ) = S(0)" =
  Pattern.equal
    (Substitution.apply_on_pattern Database_for_tests.sub_n_to_z Database_for_tests.p_sn)
    Database_for_tests.p_sz

let%test "x_to_0(x_to_S_x( S(x) )) = S(S(0))" =
  Pattern.equal
    (Substitution.apply_on_pattern Database_for_tests.sub_n_to_z
       (Substitution.apply_on_pattern Database_for_tests.sub_n_to_sn Database_for_tests.p_sn))
    Database_for_tests.p_ssz

let%test "union 1" =
  let union =
    Substitution.union_if_compatible Database_for_tests.sub_n_to_sx
      Database_for_tests.sub_n_to_sx_and_x_to_z
  in
  Substitution.equal union Database_for_tests.sub_n_to_sx_and_x_to_z

let seq_union_works (subs : Substitution.t Seq.t) : bool =
  let un = Substitution.seq_union_if_compatible subs in
  let union_contains_each = Seq.for_all (fun sub -> Substitution.is_subobject sub un) subs in
  let union_sub =
    Substitution.for_all
      (fun k v -> Seq.exists (fun sub -> Substitution.contains_at sub k v) subs)
      un
  in
  union_contains_each && union_sub

let list_union_works (subs : Substitution.t List.t) : bool =
  let un = Substitution.list_union_if_compatible subs in
  let union_contains_each = List.for_all (fun sub -> Substitution.is_subobject sub un) subs in
  let union_sub =
    Substitution.for_all
      (fun k v -> List.exists (fun sub -> Substitution.contains_at sub k v) subs)
      un
  in
  union_contains_each && union_sub

let%test "test union 1 fail" =
  try
    seq_union_works
      (List.to_seq
         [Database_for_tests.sub_n_to_sx_and_l_to_nil; Database_for_tests.sub_n_to_sz_and_x_to_sz])
  with
  | _ -> true

let%test "test union 2 fail" =
  try
    list_union_works
      [Database_for_tests.sub_n_to_sx_and_l_to_nil; Database_for_tests.sub_n_to_sz_and_x_to_sz]
  with
  | _ -> true

let%test "test union 3 success" =
  let data =
    [Database_for_tests.sub_n_to_sx_and_x_to_z; Database_for_tests.sub_n_to_sx_and_l_to_nil]
  in
  seq_union_works (List.to_seq data) && list_union_works data

let%test "test union 4" =
  let data = [Database_for_tests.sub_n_to_sx_and_x_to_z] in
  seq_union_works (List.to_seq data) && list_union_works data

let s =
  let _na = Typed_symbol.create (Symbol.create "_na") Database_for_tests.pair_nat_nat in
  let pat =
    Pattern.create Database_for_tests.pair_symbol
      [Pattern.from_var Database_for_tests.n; Pattern.from_var Database_for_tests.x]
  in
  Substitution.add _na pat Substitution.empty

let%test "test union 3" = seq_union_works (List.to_seq [s])

(*
      let%test "test union 3 " =
        union_works Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
          Database_for_tests.handmade_shallow_sub_x_to_z *)

let%test "apply_on_var 1" =
  Pattern.equal
    (Substitution.apply_on_var Database_for_tests.sub_n_to_z Database_for_tests.n)
    Database_for_tests.p_z

let%test "apply_on_var 2" =
  Pattern.equal
    (Substitution.apply_on_var Database_for_tests.sub_n_to_z Database_for_tests.l)
    Database_for_tests.p_l

let%test "apply_on_var 3" =
  Pattern.equal
    (Substitution.apply_on_var Database_for_tests.sub_n_to_sx_and_x_to_z Database_for_tests.n)
    Database_for_tests.p_sx

let%test "apply_on_pattern 1" =
  Pattern.equal
    (Substitution.apply_on_pattern Database_for_tests.sub_n_to_sn Database_for_tests.p_n)
    Database_for_tests.p_sn

let%test "apply_on_pattern 2" =
  Pattern.equal
    (Substitution.apply_on_pattern Database_for_tests.sub_n_to_sx_and_x_to_z Database_for_tests.p_n)
    Database_for_tests.p_sx

let%test "apply_on_pattern 3" =
  Pattern.equal
    (Substitution.apply_on_pattern Database_for_tests.sub_n_to_sx_and_l_to_nil
       Database_for_tests.p_consnl)
    Database_for_tests.p_conssxnil

let sub_n_to_sz_and_x_to_z =
  Substitution.add Database_for_tests.n Database_for_tests.p_sz Database_for_tests.sub_x_to_z

let%test "resolve_link 1" =
  Substitution.equal
    (Substitution.resolve_links_substitution Database_for_tests.sub_n_to_sx_and_x_to_z)
    sub_n_to_sz_and_x_to_z

let%test "resolve_link 2" =
  Pattern.equal
    (Substitution.apply_on_pattern
       (Substitution.resolve_links_substitution Database_for_tests.sub_n_to_sx_and_x_to_z)
       Database_for_tests.p_n)
    Database_for_tests.p_sz

let%test "is_defined_on 1" =
  Substitution.mem Database_for_tests.n Database_for_tests.sub_n_to_sx_and_l_to_nil = true

let%test "is_defined_on 2" =
  Substitution.mem Database_for_tests.l Database_for_tests.sub_n_to_sx_and_l_to_nil = true

let%test "is_defined_on 3" =
  Substitution.mem Database_for_tests.x Database_for_tests.sub_n_to_sx_and_l_to_nil = false

(* let%expect_test "composition" =
   let s1 = Database_for_tests.sub_n_to_sn_and_x_to_ssz in
   let s2 = Database_for_tests.sub_n_to_sx_and_l_to_nil in
   let s12 = Substitution.compose s1 s2 in
   Substitution.pp Format.std_formatter s12;
   [%expect "
     {
     n -> s(s(x))  ;  x -> s(s(z))
     }"] *)

let%expect_test "composition_with_undefined_is_identity" =
  let s1 = Database_for_tests.sub_n_to_sn_and_x_to_ssz in
  let s2 = Database_for_tests.sub_n_to_sx_and_l_to_nil in
  let s12 = Substitution.compose_with_undefined_is_injection s1 s2 in
  Substitution.pp Format.std_formatter s12;
  [%expect "
    {
    l -> nil  ;  n -> s(s(x))  ;  x -> s(s(z))
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
