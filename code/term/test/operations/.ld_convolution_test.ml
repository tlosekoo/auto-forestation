open Term

let random_inputs =
  [
    [];
    [[]];
    [[11; 19; 90]; []; [11; 58]; [34; 73; 85; 16]];
    [[]; [15]; [81]];
    [[23]];
    [[20]; [97; 31; 72]; [83; 16; 53; 52]];
    [[26; 85; 13; 16]; [5; 11; 52; 28]];
    [[86; 56; 7; 19]; [83; 5]];
  ]

(* let test_lemma_convolution_involution () =
   List.for_all (lemma_convolution_is_almost_involution_1 Convolution.Right) random_inputs
   && List.for_all (lemma_convolution_is_almost_involution_2 Convolution.Right) random_inputs *)

(* let%test "lemma_convolution_involution" = test_lemma_convolution_involution () *)
let%test "convolution test 1" = Convolution.convolute_generic Convolution.Right [] = []
let%test "convolution test 2" = Convolution.convolute_generic Convolution.Right [[]] = []

let%test "convolution test 3 - Right" =
  Convolution.convolute_generic Convolution.Right [[1; 2]; [3]] = [[1]; [2; 3]]

let%expect_test "convolution test 3 - Right" =
  let cl = Convolution.convolute_generic Convolution.Right [[1; 2]; [3]] in
  (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Printing.pp_int))
    Format.std_formatter cl;
  [%expect "[[1], [2, 3]]"]

let%test "convolution test 4 - Right" =
  Convolution.convolute_generic Convolution.Right [[1; 2]; [3]; [5; 6; 7]]
  = [[5]; [1; 6]; [2; 3; 7]]

let%test "convolution test 3 - Left" =
  Convolution.convolute_generic Convolution.Left [[1; 2]; [3]] = [[1; 3]; [2]]

let%test "convolution test 4 - Left" =
  Convolution.convolute_generic Convolution.Left [[1; 2]; [3]; [5; 6; 7]] = [[1; 3; 5]; [2; 6]; [7]]

let%expect_test "convolution test 3 - Complete" =
  let cl = Convolution.convolute_generic Convolution.Complete [[1; 2]; [3]] in
  Format.fprintf Format.std_formatter "%a"
    (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Printing.pp_int))
    cl;
  [%expect "[[1, 3], [2, 3]]"]

let%test "convolution test 4 - Complete" =
  Convolution.convolute_generic Convolution.Complete [[1; 2]; [3]; [5; 6; 7]]
  = [[1; 3; 5]; [1; 3; 6]; [1; 3; 7]; [2; 3; 5]; [2; 3; 6]; [2; 3; 7]]

let%test "convolution test 4 - Complete" =
  Convolution.convolute_generic Convolution.Complete [[]; []] = []

(* let%test "convolution test 5 - Inductive points" =
   Convolution.convolute_generic Convolution.InductivePoints
     [Database_for_tests.s_symbol; Database_for_tests.cons_symbol; Database_for_tests.cons_symbol]
     [[1]; [2; 3]; [4; 5]]
   = [[1; 3; 5]] *)

let%expect_test "pp" =
  Convolution.pp Format.std_formatter Convolution.Right;
  [%expect "right"]

let () = Printing.pp_ok ()
