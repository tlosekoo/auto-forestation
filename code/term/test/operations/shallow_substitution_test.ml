(* open Term
   open Aliases

   let%test "creation success" =
     let n_to_z = (Database_for_tests.n, (Database_for_tests.z_symbol, [])) in
     let _ = Shallow_substitution.create_of_list [n_to_z; n_to_z] in
     true

   let%test "creation failure" =
     try
       let n_to_z = (Database_for_tests.n, (Database_for_tests.z_symbol, [])) in
       let n_to_sn = (Database_for_tests.n, (Database_for_tests.s_symbol, [Database_for_tests.n])) in
       let _ = Shallow_substitution.create_of_list [n_to_z; n_to_sn] in
       false
     with
     | _ -> true

   let%test "creation_fail" =
     try
       let _sub =
         Shallow_substitution.add Database_for_tests.n (Database_for_tests.z_symbol, [])
           (Shallow_substitution.add Database_for_tests.l
              (Database_for_tests.s_symbol, [Database_for_tests.n])
              Shallow_substitution.empty)
       in
       false
     with
     | _ -> true

   let%test "eq 1" =
     Shallow_substitution.equal Database_for_tests.handmade_shallow_sub_x_to_z_and_n_to_sx
       Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
     = true

   let%test "eq 2" =
     Shallow_substitution.equal Database_for_tests.handmade_shallow_sub_x_to_z_and_n_to_sx
       Database_for_tests.handmade_shallow_sub_x_to_z
     = false

   let%test "eq 3" =
     Shallow_substitution.equal Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
       Database_for_tests.handmade_shallow_sub_n_to_sx
     = false

   let sub_1 : Shallow_substitution.t =
     let y = Variable.create "y" Database_for_tests.nat in
     (* let py = Pattern.from_var y in *)
     let yy = Variable.create "yy" Database_for_tests.nat in
     (* let pyy = Pattern.from_var yy in *)
     let yyy = Variable.create "yyy" Database_for_tests.nat in
     (* let pyyy = Pattern.from_var yyy in *)
     let bindings =
       [
         (* (Variable.create "xp" Database_for_tests.pair_nat_nat, Database_for_tests.p_pairzn); *)
         (y, (Database_for_tests.s_symbol, [yy]));
         (yy, (Database_for_tests.s_symbol, [yyy]));
         (yyy, (Database_for_tests.z_symbol, []));
       ]
     in
     Shallow_substitution.create_of_list bindings

   let%expect_test "pp sub_1" =
     Shallow_substitution.pp Format.std_formatter sub_1;
     [%expect "{y -> s(yy) ; yy -> s(yyy) ; yyy -> z}"]

   (* let%test "project" =
      let yy = Variable.create "yy" Database_for_tests.nat in
      let yyy = Variable.create "yyy" Database_for_tests.nat in
      let projected = Shallow_substitution.project (Set_variable.singleton yy) sub_1 in
      let handmade_projected =
        Shallow_substitution.of_seq (List.to_seq [(yy, (Database_for_tests.s_symbol, [yyy]))])
      in
      Shallow_substitution.equal projected handmade_projected *)

   let%expect_test "from_substitution 1" =
     Shallow_substitution.pp Format.std_formatter
       (Option.get (Shallow_substitution.from_substitution Database_for_tests.sub_n_to_sz_and_x_to_z));
     [%expect "{n -> s(x) ; x -> z}"]

   let%expect_test "from_substitution 2" =
     Shallow_substitution.pp Format.std_formatter
       (Option.get
          (Shallow_substitution.from_substitution Database_for_tests.sub_n_to_sx_and_l_to_nil));
     [%expect "{l -> nil ; n -> s(x)}"]

   let%expect_test "from_substitution 3" =
     Shallow_substitution.pp Format.std_formatter
       (Option.get (Shallow_substitution.from_substitution Database_for_tests.sub_n_to_sssx));
     [%expect "{n -> s(n_0) ; n_0 -> s(n_0_0) ; n_0_0 -> s(x)}"]

   let to_shallow_and_back (s : Substitution.t) : Substitution.t option =
     s |> Shallow_substitution.from_substitution |> Option.map Shallow_substitution.into_substitution

   let to_shallow_and_back_works_the_same (s : Substitution.t) : bool =
     match to_shallow_and_back s with
     | None -> true
     | Some back ->
         let expanded_s = Substitution.resolve_links_substitution s in
         Set_pattern.for_all
           (fun pattern ->
             let eq =
               Pattern.equal
                 (Substitution.apply_on_pattern expanded_s pattern)
                 (Substitution.apply_on_pattern back pattern)
             in
             if eq then
               true
             else
               let () =
                 Format.fprintf Format.std_formatter
                   "Failed for pattern %a and substitutions %a and %a" Pattern.pp pattern
                   Substitution.pp expanded_s Substitution.pp back
               in
               false)
           Database_for_tests.patterns_of_small_depth

   let test_to_shallow_and_back_works_the_same () : bool =
     List.for_all to_shallow_and_back_works_the_same Database_for_tests.some_non_circular_substitution

   let%test "to_shallow_and_back_works_the_same" = test_to_shallow_and_back_works_the_same ()

   (* let () =
      Format.fprintf Format.std_formatter "AAA\n%a\n" (Set_pattern.pp "\n" Pattern.pp)
        Database_for_tests.patterns_of_small_depth *)

   let%test "to shallow and back 1 " =
     let some_pattern = Database_for_tests.p_gros in
     let vars, patterns_subst = Substitution.from_patterns [some_pattern] in
     let shallow_sub = Option.get (Shallow_substitution.from_substitution patterns_subst) in
     let patterns_subst_back = Shallow_substitution.into_substitution shallow_sub in
     let restituted_pattern = Substitution.apply_on_var patterns_subst_back (List.hd vars) in
     let same_pattern = Pattern.equal restituted_pattern some_pattern in
     same_pattern

   (* let from_substitution_works (sub : Substitution.t) : bool =  *)
   let union_works (s1 : Shallow_substitution.t) (s2 : Shallow_substitution.t) : bool =
     let union = Shallow_substitution.union_if_compatible s1 s2 in
     let s1_sub = Shallow_substitution.is_subobject s1 union in
     let s2_sub = Shallow_substitution.is_subobject s2 union in
     let binding_is_in (s : Shallow_substitution.t) ((k, e) : Shallow_substitution.binding) =
       Shallow_substitution.contains_at s k e
     in
     let union_sub =
       (* Shallow_substitution *)
       Shallow_substitution.to_list union
       |> List.for_all (fun b -> binding_is_in s1 b || binding_is_in s2 b)
     in
     s1_sub && s2_sub && union_sub

   let%test "test union 1 " =
     union_works Database_for_tests.handmade_shallow_sub_n_to_sx_and_l_to_nil
       Database_for_tests.handmade_shallow_sub_x_to_z

   let%test "test union 2 " =
     union_works Database_for_tests.handmade_shallow_sub_x_to_z
       Database_for_tests.handmade_shallow_sub_x_to_z

   let%test "test union 3 " =
     union_works Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
       Database_for_tests.handmade_shallow_sub_x_to_z

   let%test "from substitution normalize" =
     let some_pattern_list = Database_for_tests.some_patterns |> Set_pattern.to_seq |> List.of_seq in
     let vars, sub = Substitution.from_patterns some_pattern_list in
     let shallow = Option.get (Shallow_substitution.from_substitution sub) in
     (* let () = Shallow_substitution.pp Format.std_formatter shallow in *)
     let back = Shallow_substitution.into_substitution shallow in
     let reconstructed_patterns = List.map (Substitution.apply_on_var back) vars in
     let patterns_match = List.equal Pattern.equal reconstructed_patterns some_pattern_list in
     patterns_match

   let%test "is_defined_on 1" =
     Shallow_substitution.is_defined_on Database_for_tests.n
       Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
     = true

   let%test "is_defined_on 2" =
     Shallow_substitution.is_defined_on Database_for_tests.x
       Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
     = true

   let%test "is_defined_on 3" =
     Shallow_substitution.is_defined_on Database_for_tests.l
       Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z
     = false

   module Map_variable = Misc.My_map.Make (Variable)

   let%test "into_var_constraints 1" =
     Var_constraints.equal
       (Shallow_substitution.into_var_constraints
          Database_for_tests.handmade_shallow_sub_n_to_sx_and_l_to_nil)
       (Map_variable.of_list_if_compatible Typed_function.equal
          [
            (Database_for_tests.n, Database_for_tests.s_symbol);
            (Database_for_tests.l, Database_for_tests.nil_symbol);
          ])

   let%test "from_var_constraints 1" =
     let children_of_n =
       Variable_generator.generate_new_children_variables Database_for_tests.n [Database_for_tests.nat]
     in
     let children_of_l = Variable_generator.generate_new_children_variables Database_for_tests.l [] in
     let s =
       Shallow_substitution.create_of_list
         [
           (Database_for_tests.n, (Database_for_tests.s_symbol, children_of_n));
           (Database_for_tests.l, (Database_for_tests.nil_symbol, children_of_l));
         ]
     in
     Shallow_substitution.equal s
       (Shallow_substitution.from_var_constraints
          Database_for_tests.constraints_of_sub_n_to_sx_and_l_to_nil)

   let%test "composition" =
     let composed =
       Shallow_substitution.compose
         (Shallow_substitution.compose Database_for_tests.handmade_shallow_sub_n_to_sx
            Database_for_tests.handmade_shallow_sub_n_to_sn)
         Database_for_tests.handmade_shallow_sub_x_to_z
     in
     Shallow_substitution.equal composed Database_for_tests.handmade_shallow_sub_n_to_sx_and_x_to_z

   let () = Printing.pp_ok () *)
