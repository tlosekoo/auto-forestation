let timer = Misc.Time_logger.create ()

let write_to_file () =
  let str = Misc.Time_logger.to_html timer in
  let filename = "time_log.html" in
  Inout.Write_to_file.write_from_string filename str
