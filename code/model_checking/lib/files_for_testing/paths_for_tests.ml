(*
   let database_for_tests_folder : string =
     match External_files.Sites.database_tests with
     | [folder_path] -> folder_path ^ "/"
     | _ -> assert false

   (*   *)
   (*   *)
   (* Databases folders and files used throughout tests *)

   let basic_types_and_symbols = database_for_tests_folder ^ "basic_types_and_symbols.lp"

   (* let db_test_restriction = database_for_tests_folder ^ "database_restriction.lp" *)
   let db_numerous_tests = database_for_tests_folder ^ "database_for_tests.lp"
   (* let folder_looping_tests = database_for_tests_folder ^ "loops_input_files/" *)
*)
