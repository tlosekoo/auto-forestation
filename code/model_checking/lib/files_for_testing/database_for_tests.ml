open Tree_tuple_formalisms
open Instantiation
open Clause
open Clause.Aliases
open Term.Database_for_tests
(* *)

(* Creating typing_problem u0:  Is there an integer? *)
let patterns_1 = [Term.Database_for_tests.p_n1]

let litteral_1 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol
          (snd Database_for_tests_shocs.a_nat_1))
       patterns_1)
    ~is_positive:true

let typing_problem_0 = Typing_problem.singleton litteral_1

let m_0_shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_nat_1))
    (fst Database_for_tests_shocs.a_nat_1)

(* Creating typing_problem u1:  Is there an integer that is both odd and even? *)
let patterns_11 = [p_n2]
let patterns_12 = [p_n2]

let litteral_11 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol
          (snd Database_for_tests_shocs.a_even))
       patterns_11)
    ~is_positive:true

let litteral_12 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol
          (snd Database_for_tests_shocs.a_odd))
       patterns_12)
    ~is_positive:true

let typing_problem_1 = Typing_problem.of_list [litteral_11; litteral_12]

let m_1_shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_even))
    (fst Database_for_tests_shocs.a_even)

(* Creating typing_problem u2:  Lists and their non-null even size *)
let patterns_21 = [p_l; p_sn]
let patterns_22 = [p_n]

let litteral_21 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol
          (snd Database_for_tests_shocs.a_list_size))
       patterns_21)
    ~is_positive:true

let litteral_22 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol
          (snd Database_for_tests_shocs.a_odd))
       patterns_22)
    ~is_positive:true

let typing_problem_2 = Typing_problem.of_list [litteral_21; litteral_22]

let m_2_shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.of_list
       [snd Database_for_tests_shocs.a_odd; snd Database_for_tests_shocs.a_list_size])
    (Shocs.union (fst Database_for_tests_shocs.a_odd) (fst Database_for_tests_shocs.a_list_size))

(* Creating typing_problem u3:  Is there an integer in the empty language? *)
let patterns_31 = [p_n]

(* wierd *)
let shocs_none_in_nat, some_nat_relation = Shocs.none_in_a_type c_nat

let litteral_31 =
  Litteral.create
    (Atom_patterns.create
       (Predicate_symbol_or_equality.create_from_predicate_symbol some_nat_relation)
       patterns_31)
    ~is_positive:true

let typing_problem_3 = Typing_problem.of_list [litteral_31]

let m_3_shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton some_nat_relation)
    shocs_none_in_nat

(* Metadata about this typing_problems *)
let four_typing_problems = [typing_problem_0; typing_problem_1; typing_problem_2; typing_problem_3]
(* let all_combination_of_four_typing_problems =
   Misc.List_op.
   [u0_prrs; u1_prrs; u2_prrs; u3_prrs] *)

type expected_answer = {is_sat : bool}

let get_satisfiability_of_expected_answer (expected_answer : expected_answer) : bool =
  expected_answer.is_sat

let expected_answer_typing_problem_0 = {is_sat = true}
let expected_answer_typing_problem_1 = {is_sat = false}
let expected_answer_typing_problem_2 = {is_sat = true}
let expected_answer_typing_problem_3 = {is_sat = false}

let expected_answers =
  [
    expected_answer_typing_problem_0;
    expected_answer_typing_problem_1;
    expected_answer_typing_problem_2;
    expected_answer_typing_problem_3;
  ]

(* let problems_and_expected_answers = List.combine some_typing_problems expected_answers *)

let some_litterals =
  Typing_problem.of_list [litteral_1; litteral_11; litteral_12; litteral_21; litteral_22]

let rec all_subsets_of_nodes_of_size_less_than_rec
    (nodes_database : Litteral.t List.t)
    (n : int)
    (accumulator : Set_typing_problem.t) : Set_typing_problem.t =
  match nodes_database with
  | [] -> accumulator
  | h :: t ->
      let smaller_sets =
        Set_typing_problem.filter (fun s -> Typing_problem.cardinal s < n) accumulator
      in
      let smaller_sets_with_pn = Set_typing_problem.map (Typing_problem.add h) smaller_sets in
      let accu_rec = Set_typing_problem.union accumulator smaller_sets_with_pn in
      let all_subsets = all_subsets_of_nodes_of_size_less_than_rec t n accu_rec in
      all_subsets

let all_subsets_of_typing_problems_of_size_less_than (nodes_database : Litteral.t list) (n : int) :
    Set_typing_problem.t =
  all_subsets_of_nodes_of_size_less_than_rec nodes_database n
    (Set_typing_problem.singleton Typing_problem.empty)

let many_typing_problems =
  all_subsets_of_typing_problems_of_size_less_than (Typing_problem.to_list some_litterals) 5

let some_typing_problems_shocs =
  let keep_one_every = 2 in
  many_typing_problems |> Set_typing_problem.to_list
  |> List.filteri (fun i _ -> i mod keep_one_every = 0)

let big_shocs =
  Shocs.list_union
    [
      fst Database_for_tests_shocs.a_even;
      fst Database_for_tests_shocs.a_list_size;
      fst Database_for_tests_shocs.a_nat_1;
    ]

let big_model =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.of_list
       [
         snd Database_for_tests_shocs.a_even;
         snd Database_for_tests_shocs.a_list_size;
         snd Database_for_tests_shocs.a_nat_1;
       ])
    big_shocs

(* solving pattern recognition in ocaml *)

(* open Every_clause *)

(* Length relation *)

let model_length__shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_list_size))
    (fst Database_for_tests_shocs.a_list_size)

let model_length_z_n__shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_lists_of_z_and_their_size))
    (fst Database_for_tests_shocs.a_lists_of_z_and_their_size)

let model_length_all__shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_natlist_nat))
    (fst Database_for_tests_shocs.a_natlist_nat)

let model_length_and_odd_and_nat__shocs =
  ModelShocs.create testing_type_env
    (Set_typed_relation_symbol.of_list
       [
         snd Database_for_tests_shocs.a_natlist_nat;
         snd Database_for_tests_shocs.a_list_size;
         snd Database_for_tests_shocs.a_even;
       ])
    (Shocs.list_union
       [
         fst Database_for_tests_shocs.a_natlist_nat;
         fst Database_for_tests_shocs.a_list_size;
         fst Database_for_tests_shocs.a_even;
       ])

(* Used for generating testing instances *)
let extension_of_instances_of_pattern_recognition = ".hehe"
