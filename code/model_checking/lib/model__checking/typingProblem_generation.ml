open Clause
open Clause.Aliases

let generate_typingProblem_for_one_clause (c : Clause_patterns.t) : Typing_problem.t =
  let body_nodes =
    c |> Clause.Disjunctive_clause_patterns.get_body |> Set_atom_patterns.to_list
    |> List.rev_map (Litteral.create ~is_positive:true)
  in
  let head_nodes_negated =
    c |> Clause.Disjunctive_clause_patterns.get_head |> Set_atom_patterns.to_list
    |> List.rev_map (Litteral.create ~is_positive:false)
  in

  let initial_universe = Typing_problem.of_list (List.append body_nodes head_nodes_negated) in
  initial_universe

let generate_typingProblem_for_all_clauses (clauses : Clause.Aliases.Set_clause_patterns.t) :
    Typing_problem.t Map_clause.t =
  let map_initial_problems =
    clauses |> Clause.Aliases.Set_clause_patterns.to_seq
    |> Seq.map (fun clause ->
           let universe = generate_typingProblem_for_one_clause clause in
           (clause, universe))
    |> Map_clause.of_seq
  in
  map_initial_problems
