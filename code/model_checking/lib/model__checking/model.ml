open Tree_tuple_formalisms
open Term
open Term.Aliases

module type S = sig
  module R : Tree_tuple_formalism.S
  include Misc.Ordered_and_printable.OP

  val create : Term.Datatype_environment.t -> Set_typed_relation_symbol.t -> R.t -> t
  val get_datatype_env : t -> Term.Datatype_environment.t
  val extract_relation_symbols : t -> Misc.My_set.Make(Term.Typed_relation_symbol).t

  (** 'find m p' returns the automaton (if any) associated with 'p'. *)
  val find_polarized : t -> Clause.Predicate_symbol_or_equality.t -> is_positive:bool -> R.t

  (** 'mapi_non_equality_predicates f m' forwards 'f' into 'mapi' of non-equality predicates. *)
  val mapi : (Term.Typed_relation_symbol.t -> R.t -> R.t) -> t -> t

  (** Returns the pair of bindings constituing the map *)
  val get_map_predicate_to_non_equality : t -> Misc.My_map.Make2(Term.Typed_relation_symbol)(R).t

  val extract_recognizer : t -> positive:bool -> R.t
end

module Make (R : Tree_tuple_formalism.S) : S with module R = R = struct
  module R = R
  module Map_predicate_R = Misc.My_map.Make2 (Typed_relation_symbol) (R)
  module Map_datatype_R = Misc.My_map.Make2 (Datatype) (R)
  module Set_R = Misc.My_set.Make2 (R)

  type t = {
    env : Term.Datatype_environment.t;
    necessary_predicates : Set_typed_relation_symbol.t;
    map_equality_predicates : Map_datatype_R.t;
    map_disequality_predicates : Map_datatype_R.t;
    map_non_equality_predicates : Map_predicate_R.t;
    map_negated_non_equality_predicates : Map_predicate_R.t;
  }
  [@@deriving compare, equal]

  let pp (c : Format.formatter) (m : t) : unit =
    Format.fprintf c "|_\n%a\n--\nEquality automata are defined for: %a\n_|"
      (Map_predicate_R.pp_param ~opening:"" ~closing:"" ~sep:"\n;\n"
         ~pp_key:Typed_relation_symbol.pp ~sep_key_value:" ->\n" ~pp_value:R.pp)
      m.map_non_equality_predicates Set_datatype.pp
      (Map_datatype_R.domain m.map_equality_predicates)

  let project_on_relation (recognizer : R.t) (relation : Typed_relation_symbol.t) : R.t =
    let filtered_recognizer =
      R.filter
        (fun rule -> Typed_relation_symbol.equal (R.Rule.get_head_relation rule) relation)
        recognizer
    in
    filtered_recognizer

  let generate_equalities_and_disequalities (env : Term.Datatype_environment.t) :
      Map_datatype_R.t * Map_datatype_R.t =
    let identities = R.identities env in
    let disequalities =
      R.complement_all_defined_relations_and env identities Set_typed_relation_symbol.empty
    in
    let datatypes = Datatype_environment.get_datatypes env in
    let map_eq =
      datatypes |> Set_datatype.to_list
      |> List.map (fun d ->
             let relation = Clause.Relation_generator.create_relation_for_identity d in
             let same_relation_clauses = project_on_relation identities relation in
             (d, same_relation_clauses))
      |> Map_datatype_R.of_list_if_compatible
    in
    let map_diff =
      datatypes |> Set_datatype.to_list
      |> List.map (fun d ->
             let relation = Clause.Relation_generator.create_relation_for_identity d in
             let same_relation_clauses = project_on_relation disequalities relation in
             (d, same_relation_clauses))
      |> Map_datatype_R.of_list_if_compatible
    in
    (map_eq, map_diff)

  let create
      (env : Term.Datatype_environment.t)
      (necessary_predicates : Set_typed_relation_symbol.t)
      (recognizer : R.t) : t =
    (* let time_before = Unix.gettimeofday () in *)
    let complemented_recognizer =
      R.complement_all_defined_relations_and env recognizer necessary_predicates
    in

    (* let time_after = Unix.gettimeofday () in *)
    (* let () =
         Format.fprintf Format.std_formatter "Complementing took %f seconds\n" (time_after -. time_before)
       in *)

    (* TODO: Computes every time - unnecessary - *)
    let map_equality_predicates, map_disequality_predicates =
      generate_equalities_and_disequalities env
    in
    let defined_relations =
      Set_typed_relation_symbol.union (R.extract_defined_relations recognizer) necessary_predicates
    in
    let map_non_equality_predicates =
      defined_relations |> Set_typed_relation_symbol.to_list
      |> List.map (fun r -> (r, project_on_relation recognizer r))
      |> Map_predicate_R.of_list
    in

    let map_negated_non_equality_predicates =
      defined_relations |> Set_typed_relation_symbol.to_list
      |> List.map (fun r -> (r, project_on_relation complemented_recognizer r))
      |> Map_predicate_R.of_list
    in

    let model =
      {
        env;
        necessary_predicates;
        map_non_equality_predicates;
        map_negated_non_equality_predicates;
        map_equality_predicates;
        map_disequality_predicates;
      }
    in
    (* let () =
         Format.fprintf Format.std_formatter
           "Model:\ndatatype env: %a\nnecessary relations: %a\nequalities: %a\ndisequalities: %a\nothers: %a\nnegated others: %a\n"
           Datatype_environment.pp env Set_typed_relation_symbol.pp necessary_predicates
           Map_datatype_R.pp map_equality_predicates Map_datatype_R.pp map_disequality_predicates
           Map_predicate_R.pp map_non_equality_predicates Map_predicate_R.pp
           map_negated_non_equality_predicates
       in *)
    model

  let get_datatype_env (m : t) : Term.Datatype_environment.t = m.env

  let extract_relation_symbols (m : t) : Set_typed_relation_symbol.t =
    let predicates =
      Map_predicate_R.fold
        (fun pred _ -> Set_typed_relation_symbol.add pred)
        m.map_non_equality_predicates Set_typed_relation_symbol.empty
    in
    predicates

  (* let find (m : t) (p : Predicate_symbol_or_equality.t) : R.t =
     match p with
     | Pred relation_symbol -> Map_predicate_R.find relation_symbol m.map_non_equality_predicates
     | Eq d -> Map_datatype_R.find d m.map_equality_predicates *)

  let find_polarized (m : t) (p : Clause.Predicate_symbol_or_equality.t) ~(is_positive : bool) : R.t
      =
    Option.value ~default:R.empty
      (match p with
      | Pred relation_symbol ->
          if is_positive then
            Map_predicate_R.find_opt relation_symbol m.map_non_equality_predicates
          else
            Map_predicate_R.find_opt relation_symbol m.map_negated_non_equality_predicates
      | Eq d ->
          if is_positive then
            Map_datatype_R.find_opt d m.map_equality_predicates
          else
            Map_datatype_R.find_opt d m.map_disequality_predicates)

  let mapi (f : Typed_relation_symbol.t -> R.t -> R.t) (m : t) : t =
    let after_mapi = Map_predicate_R.mapi f m.map_non_equality_predicates in
    {m with map_non_equality_predicates = after_mapi}

  let get_map_predicate_to_non_equality (model : t) : Map_predicate_R.t =
    model.map_non_equality_predicates

  let extract_recognizer (model : t) ~(positive : bool) =
    let map_equalities, map_others =
      if positive then
        (model.map_equality_predicates, model.map_non_equality_predicates)
      else
        (model.map_disequality_predicates, model.map_negated_non_equality_predicates)
    in
    R.union
      (R.list_union (Map_datatype_R.codomain map_equalities |> Set_R.to_list))
      (R.list_union (Map_predicate_R.codomain map_others |> Set_R.to_list))
end
