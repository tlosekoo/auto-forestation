open Clause
open Clause.Aliases
open Tree_tuple_formalisms

module type S = sig
  module R : Tree_tuple_formalism.S
  module Pattern_recognition_R : module type of Pattern_recognition.Make (R)

  val model_check_many_formula :
    time_to_stop:Misc.Time.time_limit ->
    _debug_mode:bool ->
    Model.Make(R).t ->
    Set_clause_patterns.t ->
    Pattern_recognition.t

  val model_check_one_formula :
    time_to_stop:Misc.Time.time_limit ->
    _debug_mode:bool ->
    Model.Make(R).t ->
    Clause_patterns.t ->
    Search_outcome_for_one_formula.t
end

module Make (R : Tree_tuple_formalism.S) :
  S with module R := R and module Pattern_recognition_R := Pattern_recognition.Make(R) = struct
  module Pattern_recognition_R = Pattern_recognition.Make (R)
  module ModelR = Model.Make (R)

  let compute_time_to_stop_heuristics (time_to_stop : float) =
    let current_time = Unix.gettimeofday () in
    let remaining_time = time_to_stop -. current_time in
    let percentage_of_time_we_can_use_on_a_single_checking =
      !Parameters_pattern_recognition.parameters
        .percentage_of_timeout_to_use_to_check_more_formulas_once_one_is_known_to_be_false
    in
    let time_that_percentage_gives_us =
      remaining_time *. (percentage_of_time_we_can_use_on_a_single_checking /. 100.)
    in
    let time_to_stop_heuristics = current_time +. time_that_percentage_gives_us in
    time_to_stop_heuristics

  let model_check_many_formula
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (m : ModelR.t)
      (clauses : Clause.Aliases.Set_clause_patterns.t) : Pattern_recognition.t =
    let map_initial_problems =
      TypingProblem_generation.generate_typingProblem_for_all_clauses clauses
    in
    let time_to_stop_heuristics = Option.map compute_time_to_stop_heuristics time_to_stop in

    let solution =
      Pattern_recognition_R.solve_all_at_the_same_time ~time_to_stop ~time_to_stop_heuristics
        ~_debug_mode m map_initial_problems
    in

    (* TODO: this is for debugging only *)
    (* let () = Timer.write_to_file () in *)
    solution

  let model_check_one_formula
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (m : ModelR.t)
      (clause : Clause_patterns.t) : Search_outcome_for_one_formula.t =
    Map_clause.find clause
      (model_check_many_formula ~time_to_stop ~_debug_mode m (Set_clause_patterns.singleton clause))
end
