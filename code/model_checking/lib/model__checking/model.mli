open Tree_tuple_formalisms
open Clause.Aliases

module type S = sig
  module R : Tree_tuple_formalism.S
  include Misc.Ordered_and_printable.OP

  val create : Term.Datatype_environment.t -> Set_typed_relation_symbol.t -> R.t -> t
  val get_datatype_env : t -> Term.Datatype_environment.t
  val extract_relation_symbols : t -> Set_typed_relation_symbol.t

  (** 'find m p' returns the automaton (if any) associated with 'p'. *)
  val find_polarized : t -> Clause.Predicate_symbol_or_equality.t -> is_positive:bool -> R.t

  (** 'mapi_non_equality_predicates f m' forwards 'f' into 'mapi' of non-equality predicates. *)
  val mapi : (Term.Typed_relation_symbol.t -> R.t -> R.t) -> t -> t

  (** Returns the pair of bindings constituing the map *)
  val get_map_predicate_to_non_equality : t -> Misc.My_map.Make2(Term.Typed_relation_symbol)(R).t

  val extract_recognizer : t -> positive:bool -> R.t
end

module Make : functor (R : Tree_tuple_formalism.S) -> S with module R = R
