open Tree_tuple_formalisms

(* Automaton *)
(* module TypingGoalAutomaton = Typing_goal.Make (Extended_tree_automaton)
   module NodeAutomaton = Nodee.Make (Extended_tree_automaton)
   module UniverseAutomaton = Universe.Make (Extended_tree_automaton)
   module SetUniverseAutomaton = Misc.My_set.Make2 (Universe.Make (Extended_tree_automaton))
   module ModelAutomaton = Model.Make (Extended_tree_automaton)
   module ModelCheckingAutomaton = Model_check.Make (Extended_tree_automaton)
   module PatternRecognitionAutomaton = Pattern_recognition.Make (Extended_tree_automaton)
   module MemoryStateAutomaton = Pr_memory.Memory_state (Extended_tree_automaton)
   module CanonizationAutomaton = Canonization.Make (Extended_tree_automaton)
   module SplitIndependentAutomaton = Split_independent.Make (Extended_tree_automaton)
   module LoopsAutomaton = Loops.Make (Extended_tree_automaton) *)

(* Shocs *)
module Shocs_as_recognizer = struct
  include Shocs
  module Rule = Shoc
end

module ModelShocs = Model.Make (Shocs_as_recognizer)
module ModelCheckingShocs = Model_check.Make (Shocs_as_recognizer)
module PatternRecognitionShocs = Pattern_recognition.Make (Shocs_as_recognizer)
module StepShocs = Pr_step.Make (Shocs_as_recognizer)
module ActionApplierShocs = Pr_action_applier.Make (Shocs_as_recognizer)
module ActionGetterShocs = Pr_action_getter.Make (Shocs_as_recognizer)
module UnfolderShocs = Pr_unfolder.Make (Shocs_as_recognizer)
