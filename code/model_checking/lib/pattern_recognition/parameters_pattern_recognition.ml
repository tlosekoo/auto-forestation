type t = {
  simplify_typing_problem_before_splitting_independent : bool;
  simplify_typing_problem_after_splitting_independent : bool;
  only_use_one_by_one_simplification : bool;
  stop_the_search_if_two_typing_obligations_are_contradictory : bool;
  try_trivial_actions_first_when_unfolding_typing_problem : bool;
  percentage_of_timeout_to_use_to_check_more_formulas_once_one_is_known_to_be_false : float;
}

let parameters : t ref =
  ref
    {
      simplify_typing_problem_before_splitting_independent = false;
      simplify_typing_problem_after_splitting_independent = true;
      only_use_one_by_one_simplification = false;
      stop_the_search_if_two_typing_obligations_are_contradictory = true;
      try_trivial_actions_first_when_unfolding_typing_problem = true;
      percentage_of_timeout_to_use_to_check_more_formulas_once_one_is_known_to_be_false = 1.;
    }

(* Parameters from the time the solver worked faster - what changed? Keeping for comparison *)
(* let parameters : t ref =
   ref
   {
     simplify_typing_problem_before_splitting_independent = true;
     simplify_typing_problem_after_splitting_independent = false;
     only_use_one_by_one_simplification = true;
     stop_the_search_if_two_typing_obligations_are_contradictory = false;
     try_trivial_actions_first_when_unfolding_typing_problem = false;
     percentage_of_timeout_to_use_to_check_more_formulas_once_one_is_known_to_be_false = 1.;
   } *)
