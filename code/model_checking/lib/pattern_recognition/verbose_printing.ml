let pp_history_selection (_debug_mode : bool) (h : History.t) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter
        "We select the following (most interesting) history:\n%a\n\n" History.pp h
  in
  let () = if _debug_mode then Format.pp_print_flush Format.std_formatter () in
  ()

let pp_outcome (_debug_mode : bool) final_outcome =
  let () =
    let pp_final_outcome =
      Misc.Tription.pp Term.Substitution.pp Search_outcome.pp_stopping_reason Misc.Unit.pp
    in
    if _debug_mode then
      Format.fprintf Format.std_formatter "End solving!\nFinal outcome is: %a\n\n" pp_final_outcome
        final_outcome
  in
  let () = if _debug_mode then Format.pp_print_flush Format.std_formatter () in
  ()

let pp_front
    (_debug_mode : bool)
    (step_number : int)
    (current_problems : Map_formula_to_front.t)
    (initial_problems : Map_clause_typingProblem.t) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter
        "-----------------------------------------------------\nAfter %d steps, we have the following map of histories:\n%a\n\n"
        step_number Map_formula_to_front.pp current_problems
  in
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter
        "-----------------------------------------------------\nInitial problems are \n%a\n\n"
        Map_clause_typingProblem.pp initial_problems
  in
  ()

let pp_initial_typing_problem (_debug_mode : bool) (u : Typing_problem.t) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "\nInitial problem is:\n%a\n\n" Typing_problem.pp u
  in
  let () = if _debug_mode then Format.pp_print_flush Format.std_formatter () in
  ()

let pp_solving_beginning
    (_debug_mode : bool)
    (universes : Typing_problem.t list)
    (pp_model : Format.formatter -> 'a -> unit)
    (model : 'a) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Start solving with %d parts!\n\nModel is\n%a\n\n"
        (List.length universes) pp_model model
  in
  let () = if _debug_mode then Format.pp_print_flush Format.std_formatter () in
  ()

let pp_unfolding (_debug_mode : bool) unfolding =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Unfolding is:\n%a\n\n" Unfolding_of_typingProblem.pp
        unfolding
  in
  ()

let pp_actions
    (pp_actions : Format.formatter -> 'actions -> unit)
    (_debug_mode : bool)
    (actions : 'actions) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Actions are:\n%a\n\n" pp_actions actions
  in
  ()

let pp_history_loops (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "History loops!\n"
  in
  ()

let pp_typingProblem_being_handled (_debug_mode : bool) (typingProblem : Typing_problem.t) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "\nHandling universe\n%a\n" Typing_problem.pp
        typingProblem
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_found_memoisation (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "\nFound a memoisation\n"
    (* "%a\n" *)
    (* Partial_proof_one_step.pp us *)
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_not_found_memoisation (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "\nDid not find a memoisation\n"
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_step_details (_debug_mode : bool) =
  (* let () =
       if _debug_mode then
         Format.fprintf Format.std_formatter
           "Which (as expected) gives %d new universes:\n%a\n\n" (List.length u_stepped)
           (Printing.pp_list_sep "\n" (Misc.Pair.pp Term.Substitution.pp Universe.pp))
           u_stepped
     in *)
  (* let () = Format.pp_print_flush Format.std_formatter () in
     let () =
       if _debug_mode then
         Format.fprintf Format.std_formatter "\nAll possible actions are (the are %d):\n%a\n"
           (Actions_universe.cardinal actions)
           Actions_universe.pp actions
     in
     let () = Format.pp_print_flush Format.std_formatter () in *)
  ()

let pp_problem_is_trivial (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Typing problem is trivial (empty).\n\n"
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_problem_is_contradictory (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Typing problem is contradictory!\n\n"
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_problem_is_not_trivial (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Typing problem is not trivial (non empty).\n\n"
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_found_trivial_actions (_debug_mode : bool) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Found trivial action!\n\n"
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_problem_unfolding_summary (_debug_mode : bool) (u_unfolded : Unfolding_of_typingProblem.t) =
  let () =
    if _debug_mode then
      let new_universes_count =
        u_unfolded |> Unfolding_of_typingProblem.to_list |> List.map snd
        |> List.map (fun sur ->
               sur |> Set_typingProblem_and_renaming.to_seq |> Seq.map fst
               |> Set_typing_problem.of_seq |> Set_typing_problem.cardinal)
      in
      Format.fprintf Format.std_formatter
        "There are %d possible actions. They respectively result in %a different universe.\n\n"
        (Unfolding_of_typingProblem.cardinal u_unfolded)
        (Printing.pp_list_and_brackets Misc.Int.pp)
        new_universes_count
  in
  (* let () =
       if _debug_mode then
         Format.fprintf Format.std_formatter
           "Which allows to construct the following partial proof: %a\n\n"
           Unfolding_of_typingProblem.pp u_unfolded
     in *)
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_problem_unfolding_outcome (_debug_mode : bool) (next_histories : Set_history.t) =
  let () =
    if _debug_mode then
      Format.fprintf Format.std_formatter "Which gives %d extended histories\n"
        (Set_history.cardinal next_histories)
  in
  let () =
    if _debug_mode then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_action_about_to_be_applied
    ~(_verbose : bool)
    (pp_action : Format.formatter -> 'action -> unit)
    (action : 'action) =
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter "Action about to be applied:\n%a\n" pp_action action
  in
  let () =
    if _verbose then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_simplification
    ~(_verbose : bool)
    (pp_typing_problem : Format.formatter -> 'tp -> unit)
    (typing_problem_about_to_get_simplified : 'tp)
    (solution : Term.Typed_symbol.t Term.Aliases.Map_typedSymbol.t)
    (simplified_typing_problem : 'tp) =
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter "Typing problem about to be simplified:\n%a\n"
        pp_typing_problem typing_problem_about_to_get_simplified
  in
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter "Solution:\n%a\n"
        (Term.Aliases.Map_typedSymbol.pp Term.Typed_symbol.pp)
        solution
  in
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter "Simplified problem:\n%a\n" pp_typing_problem
        simplified_typing_problem
  in
  let () =
    if _verbose then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_simplification_information
    ~(_verbose : bool)
    (pp_variable_subset_compatibility : Format.formatter -> 'a -> unit)
    (variables_subset_compatibility : 'a)
    (nb_renamings_that_are_position_compatible : int)
    (nb_all_valid_renamings : int) =
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter
        "\nVariable subset compatibility:\n%a\n\nNumber of renamings that are position-compatible: %d\n\nNumber of renamings that are valid: %d\n"
        pp_variable_subset_compatibility variables_subset_compatibility
        nb_renamings_that_are_position_compatible nb_all_valid_renamings
  in
  let () =
    if _verbose then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()

let pp_simplification_one_variable
    ~(_verbose : bool)
    (pp_typing_problem : Format.formatter -> 'tp -> unit)
    (pp_map_compatibilities : Format.formatter -> 'map -> unit)
    (typing_problem : 'tp)
    (map_compatibilities_without_identity : 'map)
    (var_src : Term.Typed_symbol.t)
    (var_tgt : Term.Typed_symbol.t)
    (simplification_in_construction : Term.Aliases.Map_typedSymbol_typedSymbol.t)
    (typing_problem_after_renaming : 'tp) =
  let () =
    if _verbose then
      Format.fprintf Format.std_formatter
        "Simplifying typing problem one variable by one vaiable.\n\nTyping problem:\n%a\n\nMap compatibilities:\n%a\n\nrenaming in construction:\n%a\n
  Can simplify by:\n%a => %a\n\nResulting typing problem:\n%a\n\n"
        pp_typing_problem typing_problem pp_map_compatibilities map_compatibilities_without_identity
        Term.Aliases.Map_typedSymbol_typedSymbol.pp simplification_in_construction
        Term.Typed_symbol.pp var_src Term.Typed_symbol.pp var_tgt pp_typing_problem
        typing_problem_after_renaming
  in
  let () =
    if _verbose then
      Format.pp_print_flush Format.std_formatter ()
  in
  ()
