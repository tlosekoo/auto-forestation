open Term.Aliases
(* open Pr_memory *)

(*
   - Rule_to_constraint: Map each rule to its corresponding constraint on variables of the pattern with which we match it.
   - Node_to_rule: Map between a node and one rule
   - RuleChoice_and_constraints: A pair with a rule choice and a constraint on variables.
   - NodeToRule_and_constraint: One rule and the corresponding constraints of the variables of the patterns
*)

module Make (R : Tree_tuple_formalisms.Tree_tuple_formalism.S) = struct
  module Rule_to_constraints = Misc.My_map.Make2 (R.Rule) (Map_typedSymbol_typedFunction)
  module Node_to_rule = Misc.My_map.Make2 (Litteral) (R.Rule)
  module Rule_and_constraint = Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction)

  module NodeToRule_and_constraint =
    Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction)

  module Every_possible_Rule_and_constraint =
    Misc.My_set.Make2 (Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction))

  module Every_possibe_NodeToRule_and_constraint =
    Misc.My_set.Make2
      (Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction))

  module ModelR = Model.Make (R)

  (* Returns the set of rules that a node can take.
     More precisely, returns the set of constraints that correspond to compatible rules *)
  let fetch_compatible_rules_and_their_constraints_for_one_node
      (model : ModelR.t)
      (node : Litteral.t) : Rule_to_constraints.t =
    let relation = Litteral.get_relation node in
    let is_positive = Litteral.is_positive node in
    let patterns = Litteral.get_patterns node in

    let recognizer = ModelR.find_polarized model relation ~is_positive in
    let rules = R.to_list recognizer in
    let constraints =
      rules
      |> List.filter_map (fun rule ->
             Option.map
               (fun constraints -> (rule, constraints))
               (R.Rule.unify_with_patterns patterns rule))
      |> Rule_to_constraints.of_list
    in
    constraints

  (** Given an action 'a' and a node doing an action '(node_doing_na, na)',
         extends 'a' with 'na' if it is possible, that is if constraints are compatible.
           Otherwise returns None. *)
  let fetch_compatible_rules_and_their_constraints_for_one_node_parameterized
      ~(only_trivial_choices : bool)
      (model : ModelR.t)
      (node : Litteral.t) : Rule_to_constraints.t =
    let rules_and_constraints =
      fetch_compatible_rules_and_their_constraints_for_one_node model node
    in
    if only_trivial_choices then
      Rule_to_constraints.filter
        (fun _rule constraints -> Map_typedSymbol.is_empty constraints)
        rules_and_constraints
    else
      rules_and_constraints

  let extend_universe_choice_with_node_choice
      (current_choices_and_constraint : NodeToRule_and_constraint.t)
      (node_doing_na : Litteral.t)
      (na : Rule_and_constraint.t) : NodeToRule_and_constraint.t option =
    let current_map_node_rule, current_constraint = current_choices_and_constraint in
    let rule_of_na, constraints_of_na = na in
    let extended_constraints_opt =
      Map_typedSymbol_typedFunction.union_if_compatible_opt current_constraint constraints_of_na
    in
    Option.map
      (fun extended_constraints ->
        let extended_map_node_rule =
          Node_to_rule.add node_doing_na rule_of_na current_map_node_rule
        in
        (extended_map_node_rule, extended_constraints))
      extended_constraints_opt

  (* Extends an action 'a' with every compatible action 'an' this node has to propose. *)
  let extend_universe_choice_with_every_node_choice
      ~(_time_to_stop : Misc.Time.time_limit)
      (node_doing_nas : Litteral.t)
      (nas : Rule_to_constraints.t)
      (ua : NodeToRule_and_constraint.t) : Every_possibe_NodeToRule_and_constraint.t =
    let () = Timeout.raise_time_up _time_to_stop in
    nas |> Rule_to_constraints.to_seq
    |> Seq.filter_map (extend_universe_choice_with_node_choice ua node_doing_nas)
    |> Every_possibe_NodeToRule_and_constraint.of_seq

  (** Returns all actions that a universe can do *)
  let get_actions
      ~(_time_to_stop : Misc.Time.time_limit)
      ~(only_trivial_choices : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) : Every_possibe_NodeToRule_and_constraint.t =
    let actions =
      u |> Typing_problem.to_seq
      |> Seq.map (fun node ->
             ( node,
               fetch_compatible_rules_and_their_constraints_for_one_node_parameterized model
                 ~only_trivial_choices node ))
    in
    let actions =
      actions
      |> Seq.fold_left
           (fun actions_of_previous_nodes (current_node, actions_this_node) ->
             actions_of_previous_nodes |> Every_possibe_NodeToRule_and_constraint.to_seq
             |> Seq.map
                  (extend_universe_choice_with_every_node_choice ~_time_to_stop current_node
                     actions_this_node)
             |> Every_possibe_NodeToRule_and_constraint.seq_union)
           (Every_possibe_NodeToRule_and_constraint.singleton
              (Node_to_rule.empty, Map_typedSymbol_typedFunction.empty))
    in
    actions

  let get_trivial_actions_and_untouched_typing_problem
      ~(_time_to_stop : Misc.Time.time_limit)
      (model : ModelR.t)
      (u : Typing_problem.t) =
    let nodes_whose_top_patterns_are_all_chosen =
      Typing_problem.filter
        (fun node ->
          Litteral.get_patterns node |> List.for_all (Stdlib.Fun.negate Term.Pattern.is_variable))
        u
    in
    let other_nodes = Typing_problem.diff u nodes_whose_top_patterns_are_all_chosen in
    let actions_on_those_chosen_nodes =
      get_actions ~_time_to_stop ~only_trivial_choices:true model
        nodes_whose_top_patterns_are_all_chosen
    in
    (other_nodes, actions_on_those_chosen_nodes)
end
