open Term
open Term.Aliases
open Tree_tuple_formalisms

module Make (R : Tree_tuple_formalism.S) = struct
  module Rule_to_constraints = Misc.My_map.Make2 (R.Rule) (Map_typedSymbol_typedFunction)
  module Node_to_rule = Misc.My_map.Make2 (Litteral) (R.Rule)
  module Rule_and_constraint = Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction)

  module NodeToRule_and_constraint =
    Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction)

  module Every_possible_Rule_and_constraint =
    Misc.My_set.Make2 (Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction))

  module Every_possibe_NodeToRule_and_constraint =
    Misc.My_set.Make2
      (Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction))

  module ActionApplierR = Pr_action_applier.Make (R)
  module UnfolderR = Pr_unfolder.Make (R)
  module ModelR = Model.Make (R)

  let update_memory_with_new_typing_problem_unfolding
      (ms : Memory_state.t)
      (typing_problem_being_unfolded : Typing_problem.t)
      (witness : Partial_proof_one_step.t) : Memory_state.t =
    {ms with memoisation = Memoisation_map.add typing_problem_being_unfolded witness ms.memoisation}

  let unfold_trivial_typing_problem_and_update_memory
      (_debug_mode : bool)
      (ms : Memory_state.t)
      (u : Typing_problem.t) =
    let () = Verbose_printing.pp_problem_is_trivial _debug_mode in
    (* Trivial partial proof (not directly SAT because of how memory is updated) *)
    let witness =
      Partial_proof_one_step.OngoingProof
        (Unfolding_of_typingProblem.singleton
           (Substitution.empty, Set_typingProblem_and_renaming.empty))
    in
    let updated_ms = update_memory_with_new_typing_problem_unfolding ms u witness in
    (updated_ms, Set_history.empty)

  let unfold_contradictory_typing_problem_and_update_memory
      (_debug_mode : bool)
      (ms : Memory_state.t)
      (u : Typing_problem.t) =
    let () = Verbose_printing.pp_problem_is_contradictory _debug_mode in
    (* Contradictory partial proof (not directly UNSAT because of how memory is updated) *)
    let witness = Partial_proof_one_step.OngoingProof Unfolding_of_typingProblem.empty in
    let updated_ms = update_memory_with_new_typing_problem_unfolding ms u witness in
    (updated_ms, Set_history.empty)

  let update_memory_after_non_trivial_problem_unfolding
      ~(_debug_mode : bool)
      (ms : Memory_state.t)
      (u_unfolded : Unfolding_of_typingProblem.t)
      (new_typing_problems : Set_typing_problem.t)
      (next_histories : Set_history.t)
      (u : Typing_problem.t) =
    let partial_proof = Partial_proof_one_step.OngoingProof u_unfolded in
    let () = Verbose_printing.pp_problem_unfolding_outcome _debug_mode next_histories in
    let semi_updated_ms = Pr_memory.link_unfolding_to_parent ms u new_typing_problems in
    let updated_ms =
      update_memory_with_new_typing_problem_unfolding semi_updated_ms u partial_proof
    in
    updated_ms

  let unfold_non_trivial_typing_problem_and_update_memory
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (history_to_unfold : History.t)
      (u : Typing_problem.t) =
    let u_unfolded = UnfolderR.unfold_typing_problem ~time_to_stop ~_debug_mode model u in
    let new_typing_problems =
      Partial_proof_one_step.get_children_universes_out_of_universe_unfolding u_unfolded
    in
    let next_histories =
      new_typing_problems |> Set_typing_problem.to_list
      |> List.rev_map (History.add history_to_unfold)
      |> Set_history.of_list
    in
    let updated_ms =
      update_memory_after_non_trivial_problem_unfolding ~_debug_mode ms u_unfolded
        new_typing_problems next_histories u
    in
    (updated_ms, next_histories)

  let unfold_typing_problem_with_no_memoisation
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (history_to_unfold : History.t)
      (u : Typing_problem.t) =
    if Typing_problem.is_empty u then
      unfold_trivial_typing_problem_and_update_memory _debug_mode ms u
    else if
      !Parameters_pattern_recognition.parameters
        .stop_the_search_if_two_typing_obligations_are_contradictory
      && Typing_problem.contains_a_typing_obligation_and_its_complement u
    then
      unfold_contradictory_typing_problem_and_update_memory _debug_mode ms u
    else
      unfold_non_trivial_typing_problem_and_update_memory ~time_to_stop ~_debug_mode model ms
        history_to_unfold u

  let unfold_typing_problem_with_no_memoisation_and_simplify_memory
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (history_to_unfold : History.t)
      (u : Typing_problem.t) =
    let () = Verbose_printing.pp_not_found_memoisation _debug_mode in
    let () = Verbose_printing.pp_step_details _debug_mode in

    let () = Misc.Time_logger.start_time_stamp Timer.timer "unfolding" in
    let updated_ms, next_histories =
      unfold_typing_problem_with_no_memoisation ~time_to_stop ~_debug_mode model ms
        history_to_unfold u
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "unfolding" in

    let () = Misc.Time_logger.start_time_stamp Timer.timer "simplify memory" in
    let updated_and_simplified_ms =
      Pr_memory.simplify updated_ms (Set_typing_problem.singleton u)
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "simplify memory" in

    (updated_and_simplified_ms, next_histories)

  let unfold_typing_problem_with_memoisation
      (_debug_mode : bool)
      (partial_proof_unfolding_u : Partial_proof_one_step.t)
      (ms : Memory_state.t)
      (history_to_unfold : History.t) =
    let () = Verbose_printing.pp_found_memoisation _debug_mode in
    match partial_proof_unfolding_u with
    | Witness _ -> (ms, Set_history.empty)
    | OngoingProof pp ->
        let children_universes =
          Partial_proof_one_step.get_children_universes_out_of_universe_unfolding pp
        in
        let new_histories =
          children_universes |> Set_typing_problem.to_seq
          |> Seq.map (History.add history_to_unfold)
          |> Set_history.of_seq
        in
        (ms, new_histories)

  let unfold_non_looping_history
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (history_to_unfold : History.t) : Memory_state.t * Set_history.t =
    let u = History.get_last_universe history_to_unfold in
    let () = Verbose_printing.pp_typingProblem_being_handled _debug_mode u in
    match Memoisation_map.find_opt u ms.memoisation with
    | Some partial_proof_unfolding_u ->
        unfold_typing_problem_with_memoisation _debug_mode partial_proof_unfolding_u ms
          history_to_unfold
    | None ->
        unfold_typing_problem_with_no_memoisation_and_simplify_memory ~time_to_stop ~_debug_mode
          model ms history_to_unfold u

  let unfold_looping_history (_debug_mode : bool) (ms : Memory_state.t) :
      Memory_state.t * Set_history.t =
    let () = Verbose_printing.pp_history_loops _debug_mode in
    (* Looping means that if it is solvable then it can be done in a shorter way.*)
    (ms, Set_history.empty)

  let unfold_history_one_step_if_necessary
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (h : History.t) : Memory_state.t * Set_history.t =
    if Loops.loops h then
      unfold_looping_history _debug_mode ms
    else
      unfold_non_looping_history ~time_to_stop ~_debug_mode model ms h
end
