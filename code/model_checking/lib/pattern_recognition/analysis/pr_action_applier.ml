open Term
open Term.Aliases
open Clause
open Tree_tuple_formalisms

module Make (R : Tree_tuple_formalism.S) = struct
  module Rule_to_constraints = Misc.My_map.Make2 (R.Rule) (Map_typedSymbol_typedFunction)
  module Node_to_rule = Misc.My_map.Make2 (Litteral) (R.Rule)
  module Rule_and_constraint = Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction)

  module NodeToRule_and_constraint =
    Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction)

  module Every_possible_Rule_and_constraint =
    Misc.My_set.Make2 (Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction))

  module Every_possibe_NodeToRule_and_constraint =
    Misc.My_set.Make2
      (Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction))

  module ActionGetterR = Pr_action_getter.Make (R)
  module ModelR = Model.Make (R)

  (* Generates the resulting nodes from applying one rule to one node. *)
  let apply_rule_to_node
      (node : Litteral.t)
      (rule : R.Rule.t)
      (unifier_patterns_rules : Substitution.t) : Typing_problem.t =
    let relation = Litteral.get_relation node in
    let is_positive = Litteral.is_positive node in
    let patterns_with_substitution_applied =
      node |> Litteral.get_patterns
      |> List.map (Substitution.apply_on_pattern unifier_patterns_rules)
    in
    let subproblems = R.Rule.unfold_no_head_check patterns_with_substitution_applied rule in
    let universe =
      subproblems |> Aliases_typing_obligation.Set_typing_obligation.to_list
      |> List.map (fun (typing_obligation : Tree_tuple_formalisms.Typing_obligation.t) ->
             (* Very ugly *)
             let new_predicate =
               if Predicate_symbol_or_equality.is_equality_predicate relation then
                 let datatype = Pattern.get_type (List.hd typing_obligation.patterns) in
                 Predicate_symbol_or_equality.create_equality_from_datatype datatype
               else
                 Clause.Predicate_symbol_or_equality.create_from_predicate_symbol
                   typing_obligation.predicate_symbol
             in
             let new_atom = Atom_patterns.create new_predicate typing_obligation.patterns in
             let node = Litteral.create new_atom ~is_positive in
             node)
      |> Typing_problem.of_list
    in
    universe

  (* let generate_fresh_variables_for_some_symbol (ts : Typed_function.t) : Typed_symbol.t list =
    let input_datatypes = ts |> Typed_function.get_input_types in
    let output_datatype = ts |> Typed_function.get_output_type in
    let fresh_variable = Term.Symbol_generator.generate_fresh_typed_symbol output_datatype in
    Symbol_generator.generate_children fresh_variable input_datatypes *)

  let constraints_into_substitution (constraints : Map_typedSymbol_typedFunction.t) : Substitution.t
      =
    constraints |> Map_typedSymbol_typedFunction.to_seq
    |> Seq.map (fun (var, rs) ->
           (* TODO: pourquoi ne pas reutiliser `var` instead of generating fresh variable?? *)
           (* let new_vars = generate_fresh_variables_for_some_symbol rs in *)
           let new_vars = Symbol_generator.generate_children var (Typed_function.get_input_types rs) in
           let new_vars_as_patterns = List.map Pattern.from_var new_vars in
           let new_pattern = Pattern.create rs new_vars_as_patterns in
           (var, new_pattern))
    |> Substitution.create_of_seq

  let apply_action ~(time_to_stop : Misc.Time.time_limit) (a : NodeToRule_and_constraint.t) :
      Substitution.t * Typing_problem.t =
    let () = Timeout.raise_time_up time_to_stop in
    let () =
      Verbose_printing.pp_action_about_to_be_applied ~_verbose:false NodeToRule_and_constraint.pp a
    in
    let () = Misc.Time_logger.start_time_stamp Timer.timer "apply action" in

    let map_node_rule, constraints = a in
    let new_substitution = constraints_into_substitution constraints in
    let new_universes =
      map_node_rule |> Node_to_rule.to_seq
      |> Seq.map (fun (node, rule) -> apply_rule_to_node node rule new_substitution)
      |> Typing_problem.seq_union
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "apply action" in

    (new_substitution, new_universes)
end
