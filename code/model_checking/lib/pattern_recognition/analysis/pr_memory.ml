open Term
module Composer_map_variable_variable = Misc.My_map.ComposeMaker (Typed_symbol) (Typed_symbol)

let augment_memory_state_for_initial_typing_problem u (ms : Memory_state.t) : Memory_state.t =
  match Linkage_map.find_opt u ms.link with
  | Some _ -> ms
  | None -> {ms with link = Linkage_map.add u Set_typing_problem.empty ms.link}

let universe_is_known_to_be_sat (ms : Memory_state.t) (u : Typing_problem.t) : bool =
  (* Universe.is_empty u
     || *)
  Option.fold ~none:false ~some:Partial_proof_one_step.is_sat
    (Memoisation_map.find_opt u ms.memoisation)

let solution_to_universe_is_known (ms : Memory_state.t) (u : Typing_problem.t) : bool =
  Option.fold ~none:false ~some:Partial_proof_one_step.is_solved
    (Memoisation_map.find_opt u ms.memoisation)

let universe_is_known_to_be_unsat (ms : Memory_state.t) (u : Typing_problem.t) : bool =
  let result =
    Option.fold ~none:false ~some:Partial_proof_one_step.is_unsat
      (Memoisation_map.find_opt u ms.memoisation)
  in
  result

(** 'and_value mem andd' transforms, if possible, a set of split universes into a witness *)
let and_value (ms : Memory_state.t) (and_set : Set_typingProblem_and_renaming.t) :
    Partial_proof_one_step.witness Option.t =
  (* could be optimized. It is worth it? *)
  if
    Set_typingProblem_and_renaming.exists
      (fun (u, _r) -> universe_is_known_to_be_unsat ms u)
      and_set
  then
    Some (Unsat ())
  else if
    Set_typingProblem_and_renaming.for_all (fun (u, _r) -> universe_is_known_to_be_sat ms u) and_set
  then
    let all_universes, all_renamings =
      and_set |> Set_typingProblem_and_renaming.to_list |> List.split
    in
    let all_universes_solution =
      all_universes
      |> List.rev_map (fun un ->
             Partial_proof_one_step.get_sat_witness (Memoisation_map.find un ms.memoisation))
      |> List.rev
    in
    let all_substitutions =
      List.rev_map2 Composer_map_variable_variable.compose all_renamings all_universes_solution
    in
    let subst_for_this_set_of_universes = Substitution.list_union_if_compatible all_substitutions in
    Some (Sat subst_for_this_set_of_universes)
  else
    None

(** 'simplify_one_step ms u' examines the memory state of 'u' and simplifies it.
   Simplifying means transforming, if possible, a partial proof into a witness. *)
let simplify_one_step (ms : Memory_state.t) (u : Typing_problem.t) :
    Memory_state.t * Set_typing_problem.t =
  match Memoisation_map.find_opt u ms.memoisation with
  | None -> (ms, Set_typing_problem.empty)
  | Some (Witness _) -> (ms, Set_typing_problem.empty)
  | Some (OngoingProof op) -> (
      let or_of_values =
        op |> Unfolding_of_typingProblem.to_list
        |> List.rev_map (Misc.Pair.map_snd (and_value ms))
        |> List.rev_map (fun (sub, witness_opt) ->
               Option.map (fun witness -> (sub, witness)) witness_opt)
      in
      (* a bit overkill to compute this, but no big deal*)
      let interesting_values = Misc.List_op.filter_opt or_of_values in
      let and_set_that_is_sat =
        List.find_map
          (fun (sub, witness) ->
            Option.map
              (fun sub_witness -> (sub, sub_witness))
              (Partial_proof_one_step.get_witness_sat_opt witness))
          interesting_values
      in
      (* In both these cases we do not touch the link map. This could be optimized by removing solved problems of this map. *)
      match and_set_that_is_sat with
      | Some (substitution_from_u, solution_sub_problems) ->
          let global_solution =
            Substitution.compose_with_undefined_is_injection substitution_from_u
              solution_sub_problems
          in
          let witness = Partial_proof_one_step.Witness (Sat global_solution) in
          let updated_ms = {ms with memoisation = Memoisation_map.add u witness ms.memoisation} in
          (updated_ms, Linkage_map.find u ms.link)
      | None ->
          let no_value_from_and_set_is_uninteresting =
            List.length or_of_values = List.length interesting_values
          in
          if
            no_value_from_and_set_is_uninteresting
            && List.for_all
                 (fun (_, w) -> Partial_proof_one_step.is_witness_unsat w)
                 interesting_values
          then
            let witness = Partial_proof_one_step.Witness (Partial_proof_one_step.Unsat ()) in
            let updated_ms = {ms with memoisation = Memoisation_map.add u witness ms.memoisation} in
            (updated_ms, Linkage_map.find u ms.link)
          else
            (* Could optimize here by removing unsat universes from partial proofs. Is it useful? *)
            (ms, Set_typing_problem.empty))

(** 'simplify mem us' tries to convert 'mem[u]' (for all 'u' in 'us') into a witness.
 In succeeding in so for some 'u', it then recursively simplifies every 'pu' whose partial proof uses 'u'. *)
let rec simplify (ms : Memory_state.t) (universes_to_simplify : Set_typing_problem.t) :
    Memory_state.t =
  if not (Set_typing_problem.is_empty universes_to_simplify) then
    let updated_ms, new_universes_to_simplify =
      Set_typing_problem.fold
        (fun u (ms, current_us) ->
          let updated_ms, new_us = simplify_one_step ms u in
          let every_us = Set_typing_problem.union current_us new_us in
          (updated_ms, every_us))
        universes_to_simplify (ms, Set_typing_problem.empty)
    in
    simplify updated_ms new_universes_to_simplify
  else
    ms

(** 'link_unfolding_to_parent ipo u universes' adds 'u' in the set of universes 'ipo[universe]' for every 'universe' in 'universes' *)
let link_unfolding_to_parent
    (ms : Memory_state.t)
    (u : Typing_problem.t)
    (universes_appearing_in_proof_of_u : Set_typing_problem.t) : Memory_state.t =
  universes_appearing_in_proof_of_u |> Set_typing_problem.to_seq
  |> Seq.map (fun universe -> (universe, Linkage_map.find_opt universe ms.link))
  |> Seq.map (Misc.Pair.map_snd (Option.value ~default:Set_typing_problem.empty))
  |> Seq.map (Misc.Pair.map_snd (Set_typing_problem.add u))
  |> fun s -> {ms with link = Linkage_map.add_seq s ms.link}

let set_to_unsat (ms : Memory_state.t) (u : Typing_problem.t) =
  let unsat = Partial_proof_one_step.Witness (Unsat ()) in
  let new_memoisation_map = Memoisation_map.add u unsat ms.memoisation in
  {ms with memoisation = new_memoisation_map}
