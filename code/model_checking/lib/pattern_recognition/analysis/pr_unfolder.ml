open Term
open Term.Aliases
open Tree_tuple_formalisms

module Make (R : Tree_tuple_formalism.S) = struct
  module Rule_to_constraints = Misc.My_map.Make2 (R.Rule) (Map_typedSymbol_typedFunction)
  module TypingProblem_to_rule = Misc.My_map.Make2 (Litteral) (R.Rule)
  module Rule_and_constraint = Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction)

  module TypingProblemToRule_and_constraint =
    Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction)

  module Every_possible_Rule_and_constraint =
    Misc.My_set.Make2 (Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction))

  module Every_possibe_TypingProblemToRule_and_constraint =
    Misc.My_set.Make2
      (Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction))

  module ActionGetterR = Pr_action_getter.Make (R)
  module ActionApplierR = Pr_action_applier.Make (R)
  module ModelR = Model.Make (R)

  type instant =
    | Before_splitting_independent_problems
    | After_splitting_independent_problems

  let simplify_child_typing_problem_if_parameters_decide_so
      (instant : instant)
      (tp : Typing_problem.t) : Typing_problem.t * Map_typedSymbol_typedSymbol.t =
    let condition_for_simplifying_is_met =
      match instant with
      | Before_splitting_independent_problems ->
          !Parameters_pattern_recognition.parameters
            .simplify_typing_problem_before_splitting_independent
      | After_splitting_independent_problems ->
          !Parameters_pattern_recognition.parameters
            .simplify_typing_problem_after_splitting_independent
    in
    if condition_for_simplifying_is_met then
      let simplifying_renaming = Simplify_typing_problem.simplify tp in
      let simplified_tp =
        Typing_problem.apply_renaming
          (Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity simplifying_renaming)
          tp
      in
      (simplified_tp, simplifying_renaming)
    else
      (tp, Map_typedSymbol_typedSymbol.empty)

  let simplify_and_incorporate_changes_into_substitution
      (instant : instant)
      (sub : Substitution.t)
      (typing_problem_to_simplify : Typing_problem.t) =
    let simplified_typing_problem, simplification_renaming_before_splitting =
      simplify_child_typing_problem_if_parameters_decide_so instant typing_problem_to_simplify
    in
    let simplification_renaming_as_substitution : Substitution.t =
      Map_typedSymbol_typedSymbol.map Pattern.from_var simplification_renaming_before_splitting
    in
    let substitution_with_simplification =
      Substitution.compose_with_undefined_is_injection sub simplification_renaming_as_substitution
    in
    (substitution_with_simplification, simplified_typing_problem)

  (* TODO: unify simplification treatment Before/After by putting it all in the substitution *)
  let simplify_split_simplify_normalize__child_of_stepped_typing_problem
      ((substitution_for_stepping, child_of_u) : Substitution.t * Typing_problem.t) =
    let substitution_for_stepping_and_simplify, simplified_child_of_u =
      (simplify_and_incorporate_changes_into_substitution Before_splitting_independent_problems)
        substitution_for_stepping child_of_u
    in
    let splitted_problems = Split_independent.split_independent simplified_child_of_u in
    let splitted_and_simplified_and_normalized_problems =
      splitted_problems |> Set_typing_problem.to_seq
      |> Seq.map
           (simplify_child_typing_problem_if_parameters_decide_so
              After_splitting_independent_problems)
      |> Seq.map (Misc.Pair.map_fst Canonization.canonize)
      |> Seq.map (fun ((simplified_typing_problem, normalisation), simplification) ->
             let simplify_and_normalise =
               Map_typedSymbol_typedSymbol.auto_compose_considering_undefined_is_identity
                 simplification normalisation
             in
             (simplified_typing_problem, simplify_and_normalise))
      |> Set_typingProblem_and_renaming.of_seq
    in
    (* let () =
         Format.fprintf Format.std_formatter
           "Child of u:\n%a\nnSplitted problem:\n%a\n\nSplitted_and_simplified_and_normalized_problems:\n%a\n"
           Typing_problem.pp simplified_child_of_u Set_typing_problem.pp splitted_problems
           Set_typingProblem_and_renaming.pp splitted_and_simplified_and_normalized_problems
       in *)
    (substitution_for_stepping_and_simplify, splitted_and_simplified_and_normalized_problems)

  let unfold_universe_using_explicit_actions
      ~(time_to_stop : Misc.Time.time_limit)
      (actions : ActionGetterR.Every_possibe_NodeToRule_and_constraint.t)
      (typing_problem_to_add_to_every_children : Typing_problem.t) : Unfolding_of_typingProblem.t =
    let u_unfolded =
      actions |> Every_possibe_TypingProblemToRule_and_constraint.to_seq
      |> Seq.map (ActionApplierR.apply_action ~time_to_stop)
      |> Seq.map (Misc.Pair.map_snd (Typing_problem.union typing_problem_to_add_to_every_children))
      |> Seq.map simplify_split_simplify_normalize__child_of_stepped_typing_problem
      |> Unfolding_of_typingProblem.of_seq
    in
    u_unfolded

  let unfold_typing_problem_using_all_actions
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) : Unfolding_of_typingProblem.t =
    let () = Misc.Time_logger.start_time_stamp Timer.timer "getting all actions" in
    let all_actions =
      ActionGetterR.get_actions ~_time_to_stop:time_to_stop ~only_trivial_choices:false model u
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "getting all actions" in

    let u_unfolded =
      unfold_universe_using_explicit_actions ~time_to_stop all_actions Typing_problem.empty
    in
    u_unfolded

  let unfold_typing_problem_using_trivial_actions_if_available
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) =
    let () = Misc.Time_logger.start_time_stamp Timer.timer "getting trivial actions" in
    let untouched_typing_problem, actions_on_trivial_nodes =
      ActionGetterR.get_trivial_actions_and_untouched_typing_problem ~_time_to_stop:time_to_stop
        model u
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "getting trivial actions" in

    if Typing_problem.equal untouched_typing_problem u then
      unfold_typing_problem_using_all_actions ~time_to_stop ~_debug_mode model u
    else
      let () =
        Verbose_printing.pp_actions ActionGetterR.Every_possibe_NodeToRule_and_constraint.pp
          _debug_mode actions_on_trivial_nodes
      in
      unfold_universe_using_explicit_actions ~time_to_stop actions_on_trivial_nodes
        untouched_typing_problem

  let unfold_typing_problem
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) : Unfolding_of_typingProblem.t =
    match
      !Parameters_pattern_recognition.parameters
        .try_trivial_actions_first_when_unfolding_typing_problem
    with
    | false -> unfold_typing_problem_using_all_actions ~time_to_stop ~_debug_mode model u
    | true ->
        unfold_typing_problem_using_trivial_actions_if_available ~time_to_stop ~_debug_mode model u
end
