open Term
open Clause
open Clause.Aliases
open Tree_tuple_formalisms

type t = Map_formula_outcome.t [@@deriving compare, equal]
type t_with_trace = t * Memory_state.t

let pp =
  Map_formula_outcome.pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n"
    ~pp_key:Disjunctive_clause_patterns.pp ~sep_key_value:" -> "
    ~pp_value:Search_outcome_for_one_formula.pp

module Make (R : Tree_tuple_formalism.S) = struct
  module Rule_to_constraints = Misc.My_map.Make2 (R.Rule) (Map_typedSymbol_typedFunction)
  module Node_to_rule = Misc.My_map.Make2 (Litteral) (R.Rule)
  module Rule_and_constraint = Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction)

  module NodeToRule_and_constraint =
    Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction)

  module Every_possible_Rule_and_constraint =
    Misc.My_set.Make2 (Misc.Pair.Make (R.Rule) (Map_typedSymbol_typedFunction))

  module Every_possibe_NodeToRule_and_constraint =
    Misc.My_set.Make2
      (Misc.Pair.Make (Misc.My_map.Make2 (Litteral) (R.Rule)) (Map_typedSymbol_typedFunction))

  module ActionGetterR = Pr_action_getter.Make (R)
  module StepR = Pr_step.Make (R)
  module ModelR = Model.Make (R)

  let get_status_for_initial_problems
      (ms : Memory_state.t)
      (initial_problems : Map_clause_typingProblem.t) : Memoisation_map.value option Map_clause.t =
    Map_clause_typingProblem.map
      ((Stdlib.Fun.flip Memoisation_map.find_opt) ms.memoisation)
      initial_problems

  let status_formula_into_outcome_formula (status_opt : Memoisation_map.value option) :
      Search_outcome_for_one_formula.t option =
    match status_opt with
    | Some (Witness w) -> (
        match w with
        | Sat subst -> Some (Search_outcome.yes subst)
        | Unsat nw -> Some (Search_outcome.no nw))
    | _ -> None

  let get_outcome_for_initial_problems
      (initial_problems : Map_clause_typingProblem.t)
      (ms : Memory_state.t) : Map_formula_outcome.t =
    initial_problems
    |> get_status_for_initial_problems ms
    |> Map_clause.filter_map (fun _ -> status_formula_into_outcome_formula)

  (* Stops as soon as one formula is solved and heuristics says it's over,
     or there is no formula left to check. It is good? I don't know *)
  let should_stop_search_and_return
      ~(time_to_stop_heuristics : Misc.Time.time_limit)
      (initial_problems : Map_clause_typingProblem.t)
      (ms : Memory_state.t) : Map_formula_outcome.t option =
    let current_outcomes_for_initial_problems =
      get_outcome_for_initial_problems initial_problems ms
    in
    let sat_formulas_certificates =
      Map_formula_outcome.filter_map
        (fun _ -> Search_outcome.get_yes)
        current_outcomes_for_initial_problems
    in
    let some_formula_is_false = not (Map_formula_outcome.is_empty sat_formulas_certificates) in
    let some_formula_is_still_in_process =
      Map_formula_outcome.cardinal current_outcomes_for_initial_problems
      < Map_clause_typingProblem.cardinal initial_problems
    in
    if not some_formula_is_still_in_process then
      Some current_outcomes_for_initial_problems
    else
      let heuristics_says_we_should_stop =
        Option.fold ~none:false
          ~some:(fun time_to_not_go_over -> Unix.gettimeofday () > time_to_not_go_over)
          time_to_stop_heuristics
      in
      if some_formula_is_false && heuristics_says_we_should_stop then
        Some current_outcomes_for_initial_problems
      else
        None

  (* open Memory_state *)

  let handle_solved_fronts
      (initial_ms : Memory_state.t)
      (initial_universes : Map_clause_typingProblem.t)
      (current_problems : Map_formula_to_front.t) : Memory_state.t * Map_formula_to_front.t =
    Map_formula_to_front.fold
      (fun formula front (ms, problems) ->
        let corresponding_initial_universe =
          Map_clause_typingProblem.find formula initial_universes
        in
        let memoisation_for_this_problem =
          Memoisation_map.find_opt corresponding_initial_universe
            (Memory_state.get_memoisation_map ms)
        in
        match memoisation_for_this_problem with
        | Some (Witness _) -> (ms, problems) (* change nothing *)
        | Some (OngoingProof _) ->
            if Heap_histories.is_empty front then
              let updated_ms = Pr_memory.set_to_unsat ms corresponding_initial_universe in
              (updated_ms, problems)
            else
              let updated_problems = Map_formula_to_front.add formula front problems in
              (ms, updated_problems)
        | None ->
            let updated_problems = Map_formula_to_front.add formula front problems in
            (ms, updated_problems))
      current_problems (initial_ms, Map_clause.empty)

  let add_timeout_on_unfinished_formulas : Map_formula_to_front.t -> Map_formula_outcome.t =
    Map_clause.map (fun _ -> Search_outcome.maybe Search_outcome.OutOfTime)

  let step_one_unfinished_formula
      ~(_debug_mode : bool)
      ~(time_to_stop : Float.t option)
      (model : ModelR.t)
      (ms : Memory_state.t)
      (front : Heap_histories.t) : Heap_histories.t * Memory_state.t =
    let some_history, rest_of_histories =
      (Heap_histories.get_min front, Heap_histories.delete_min front)
    in
    let () = Verbose_printing.pp_history_selection _debug_mode some_history in
    try
      let new_ms, next_histories =
        StepR.unfold_history_one_step_if_necessary model ms ~time_to_stop ~_debug_mode some_history
      in
      let updated_histories =
        Heap_histories.merge
          (Heap_histories.of_list (Set_history.to_list next_histories))
          rest_of_histories
      in
      (updated_histories, new_ms)
    with
    | Timeout.TimeUp -> (front, ms)

  let step_every_unfinished_formulas
      ~(_debug_mode : bool)
      ~(time_to_stop : Float.t option)
      (model : ModelR.t)
      (initial_ms : Memory_state.t)
      (initial_problems : Map_formula_to_front.t) : Map_formula_to_front.t * Memory_state.t =
    Map_formula_to_front.fold
      (fun formula front (problems, ms) ->
        let new_front, updated_ms =
          step_one_unfinished_formula ~_debug_mode ~time_to_stop model ms front
        in
        let updated_unsolved_formula = Map_clause.add formula new_front problems in
        (updated_unsolved_formula, updated_ms))
      initial_problems (Map_clause.empty, initial_ms)

  let rec recursively_unfold_everything
      ~(_debug_mode : bool)
      ~(time_to_stop : Misc.Time.time_limit)
      ~(time_to_stop_heuristics : Misc.Time.time_limit)
      (model : ModelR.t)
      (initial_universes : Map_clause_typingProblem.t)
      (ms : Memory_state.t)
      (current_problems : Map_formula_to_front.t)
      (_number_of_steps : int) : t * Memory_state.t =
    let () =
      Verbose_printing.pp_front _debug_mode _number_of_steps current_problems initial_universes
    in
    let ms, current_problems = handle_solved_fronts ms initial_universes current_problems in
    match should_stop_search_and_return ~time_to_stop_heuristics initial_universes ms with
    | Some outcomes -> (outcomes, ms)
    | None ->
        if Misc.Time.is_time_up time_to_stop then
          let outcomes = add_timeout_on_unfinished_formulas current_problems in
          (outcomes, ms)
        else
          let stepped_problems, new_ms =
            step_every_unfinished_formulas ~_debug_mode ~time_to_stop model ms current_problems
          in
          recursively_unfold_everything ~_debug_mode ~time_to_stop ~time_to_stop_heuristics model
            initial_universes new_ms stepped_problems (_number_of_steps + 1)

  (* let trivially_unfold_universe (u : Typing_problem.t) : Unfolding_of_universe.t =
     let nodes = Typing_problem.elements u in

     () *)

  let augment_ms_for_initial_universes
      (initial_ms : Memory_state.t)
      (initial_universes : Map_clause_typingProblem.t) : Memory_state.t =
    Map_clause_typingProblem.fold
      (fun _ u ms -> Pr_memory.augment_memory_state_for_initial_typing_problem u ms)
      initial_universes initial_ms

  let project_solutions
      (map_initial_universes : Typing_problem.t Map_clause.t)
      (counter_examples : t) : t =
    Map_clause.mapi
      (fun formula outcome ->
        let intial_universe = Map_clause.find formula map_initial_universes in
        let initial_variables = Typing_problem.get_vars intial_universe in
        let projected_solution =
          (Search_outcome.map_yes (Substitution.project_in initial_variables)) outcome
        in
        projected_solution)
      counter_examples

  let map_initial_universes_to_map_problems (map_initial_universes : Typing_problem.t Map_clause.t)
      =
    Map_clause.map
      (fun u ->
        let h = History.create [u] in
        Heap_histories.singleton h)
      map_initial_universes

  let solve_single_universe
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (formula : Clause_patterns.t)
      (u : Typing_problem.t)
      (already_computed_ms : Memory_state.t) : Search_outcome_for_one_formula.t * Memory_state.t =
    let () = Verbose_printing.pp_initial_typing_problem _debug_mode u in
    let initial_map_universe = Map_clause.singleton formula u in
    let initial_problems = map_initial_universes_to_map_problems initial_map_universe in
    let initial_memory_state =
      augment_ms_for_initial_universes already_computed_ms initial_map_universe
    in

    let outcomes, ms =
      recursively_unfold_everything ~_debug_mode ~time_to_stop ~time_to_stop_heuristics:None model
        initial_map_universe initial_memory_state initial_problems 0
    in
    let projected_solution = project_solutions initial_map_universe outcomes in
    let outcome_of_u = Map_clause.find formula projected_solution in
    (outcome_of_u, ms)

  (* Folding may be slower than doing all at once, and maybe faster? Dont know *)
  let solve_and_keep_memory_disjunction_one_by_one
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (universes : Typing_problem.t list) : Search_outcome_for_one_formula.t * Memory_state.t =
    let initial_memory = Memory_state.empty in
    let initial_outcome = Search_outcome.no () in
    let fake_formula = Clause_patterns.empty_clause in
    let () = Verbose_printing.pp_solving_beginning _debug_mode universes ModelR.pp model in
    let final_outcome, final_memory =
      List.fold_left
        (fun (current_outcome, current_memory_state) u ->
          match current_outcome with
          | Misc.Tription.Yes _ -> (current_outcome, current_memory_state)
          | Misc.Tription.Maybe _ ->
              let outcome_of_u, updated_memory =
                solve_single_universe ~time_to_stop ~_debug_mode model fake_formula u
                  current_memory_state
              in
              let new_outcome =
                if Search_outcome.is_yes outcome_of_u then
                  outcome_of_u
                else
                  current_outcome
              in
              (new_outcome, updated_memory)
          | Misc.Tription.No () ->
              solve_single_universe ~time_to_stop ~_debug_mode model fake_formula u
                current_memory_state)
        (initial_outcome, initial_memory) universes
    in

    let () = Verbose_printing.pp_outcome _debug_mode final_outcome in
    (final_outcome, final_memory)

  let solve_and_keep_memory
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) : Search_outcome_for_one_formula.t * Memory_state.t =
    solve_and_keep_memory_disjunction_one_by_one ~time_to_stop ~_debug_mode model [u]

  let solve
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (u : Typing_problem.t) : Search_outcome_for_one_formula.t =
    fst (solve_and_keep_memory ~time_to_stop ~_debug_mode model u)

  let exists_solve
      ~(time_to_stop : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (us : Typing_problem.t list) : Search_outcome_for_one_formula.t =
    fst (solve_and_keep_memory_disjunction_one_by_one ~time_to_stop ~_debug_mode model us)

  let solve_all_at_the_same_time
      ~(time_to_stop : Misc.Time.time_limit)
      ~(time_to_stop_heuristics : Misc.Time.time_limit)
      ~(_debug_mode : bool)
      (model : ModelR.t)
      (map_formula_universe : Typing_problem.t Map_clause.t) : t =
    let initial_memory = augment_ms_for_initial_universes Memory_state.empty map_formula_universe in
    let initial_problems = map_initial_universes_to_map_problems map_formula_universe in
    let all_outcomes, _end_memory_state =
      recursively_unfold_everything ~time_to_stop ~time_to_stop_heuristics ~_debug_mode model
        map_formula_universe initial_memory initial_problems 0
    in
    let all_outcomes_projected = project_solutions map_formula_universe all_outcomes in
    all_outcomes_projected
end
