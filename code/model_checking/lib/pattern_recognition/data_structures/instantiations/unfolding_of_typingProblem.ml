open Term
include Misc.My_set.Make (Misc.Pair.Make (Substitution) (Set_typingProblem_and_renaming))

let pp =
  pp_param ~opening:"\n" ~closing:"\n\n" ~sep:"\n" ~pp_element:(fun c (subst, solution) ->
      Format.fprintf c "%a\n  =>\n%a" Substitution.pp subst Set_typingProblem_and_renaming.pp
        solution)
