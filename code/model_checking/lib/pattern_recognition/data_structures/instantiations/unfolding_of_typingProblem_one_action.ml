open Term
open Term.Aliases

include
  Misc.Pair.Make
    (Substitution)
    (Misc.My_set.Make (Misc.Pair.Make (Typing_problem) (Map_typedSymbol_typedSymbol)))
