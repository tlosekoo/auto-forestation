(* A binding '(u, S)' mean that when 'u' is solved, only nodes from 'S' should be checked when backpropagating solved universes. *)
include Misc.My_map.Make2 (Typing_problem) (Misc.My_set.Make2 (Typing_problem))

let pp =
  pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n\n" ~pp_key:Typing_problem.pp
    ~sep_key_value:"\nis needed for\n"
    ~pp_value:
      (Set_typing_problem.pp_param ~opening:"{\n  " ~sep:"\n  " ~closing:"\n}"
         ~pp_element:Typing_problem.pp)
