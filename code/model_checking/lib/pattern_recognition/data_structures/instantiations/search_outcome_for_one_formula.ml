type t = Term.Substitution.t Search_outcome.t [@@deriving equal, compare]

let pp = Search_outcome.pp Term.Substitution.pp
