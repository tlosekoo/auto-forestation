(* A binding '(u, PP)' means that PP is the best current solution for the solving of 'u'. *)
include Misc.My_map.Make2 (Typing_problem) (Partial_proof_one_step)
