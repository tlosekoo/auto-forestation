include Misc.My_map.Make2 (Clause.Clause_patterns) (Search_outcome_for_one_formula)

let pp =
  pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n" ~pp_key:Clause.Disjunctive_clause_patterns.pp
    ~sep_key_value:" -> " ~pp_value:Search_outcome_for_one_formula.pp
