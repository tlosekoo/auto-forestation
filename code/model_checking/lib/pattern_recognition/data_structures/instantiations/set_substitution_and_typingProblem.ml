include Misc.My_set.Make (Misc.Pair.Make (Term.Substitution) (Typing_problem))

let pp =
  pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n" ~pp_element:(fun c (subst, solution) ->
      Format.fprintf c "%a\n  :\n%a" Term.Substitution.pp subst Typing_problem.pp solution)
