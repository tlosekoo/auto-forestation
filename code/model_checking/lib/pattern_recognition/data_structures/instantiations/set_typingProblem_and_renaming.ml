open Term.Aliases
include Misc.My_set.Make (Misc.Pair.Make (Misc.My_set.Make (Litteral)) (Map_typedSymbol_typedSymbol))

let pp_debug = pp

let pp c typing_problems_and_their_renaming =
  let typing_problems = typing_problems_and_their_renaming |> to_list |> List.map fst in
  Printing.pp_list_sep "\n" Typing_problem.pp c typing_problems
