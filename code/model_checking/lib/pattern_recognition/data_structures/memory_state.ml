type t = {
  memoisation : Memoisation_map.t;
  link : Linkage_map.t;
}
[@@deriving compare, equal]

let pp (c : Format.formatter) (ms : t) : unit =
  Format.fprintf c "State of proof in memory:\n%a\nlinkage:\n%a\n" Memoisation_map.pp ms.memoisation
    Linkage_map.pp ms.link

let empty : t = {memoisation = Memoisation_map.empty; link = Linkage_map.empty}
let get_memoisation_map (ms : t) : Memoisation_map.t = ms.memoisation
let get_linkage_map (ms : t) : Linkage_map.t = ms.link
