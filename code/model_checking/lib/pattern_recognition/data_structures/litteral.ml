open Term
open Clause

type t = {
  atom : Atom_patterns.t;
  is_positive : Bool.t;
}
[@@deriving equal, compare]

let pp (c : Format.formatter) (l : t) : unit =
  let str_negation = if l.is_positive then "" else "not " in
  Format.fprintf c "%s%a" str_negation Atom_patterns.pp l.atom

let create ~(is_positive : bool) (atom : Atom_patterns.t) : t = {atom; is_positive}
let get_patterns (l : t) : Patterns.t = Atom_patterns.get_argument l.atom
let get_relation (l : t) : Predicate_symbol_or_equality.t = Atom_patterns.get_predicate l.atom
let is_positive (l : t) : bool = l.is_positive

let map_patterns (f : Pattern.t -> Pattern.t) (l : t) : t =
  let mapped_atom = Atom_patterns.self_map (List.map f) l.atom in
  {is_positive = l.is_positive; atom = mapped_atom}

let same_shape_with_var_comparator
    (cmp_var : Typed_symbol.t -> Typed_symbol.t -> int)
    (pn_1 : t)
    (pn_2 : t) : bool =
  let same_relation = Predicate_symbol_or_equality.equal (get_relation pn_1) (get_relation pn_2) in
  let same_positivity = Bool.equal pn_1.is_positive pn_2.is_positive in
  let patterns_1 = get_patterns pn_1 in
  let patterns_2 = get_patterns pn_2 in
  let same_shape_patterns =
    List.compare (Pattern.compare_shape_with_variable_comparison cmp_var) patterns_1 patterns_2 = 0
  in
  same_relation && same_positivity && same_shape_patterns

let same_shape = same_shape_with_var_comparator (fun _ _ -> 0)

let get_variables_dfs_ordered (n : t) : Term.Typed_symbol.t list =
  n |> get_patterns |> List.map Pattern.get_variables_dfs_ordered |> List.flatten

let negate (l : t) : t = {l with is_positive = not l.is_positive}
