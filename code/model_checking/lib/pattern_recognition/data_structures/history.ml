(* open Term *)

include Misc.List_maker.Make (Typing_problem)

let create (ul : Typing_problem.t list) : t =
  if ul = [] then
    raise (Invalid_argument "Cannot build and empty history")
  else
    List.rev ul

let get_universes : t -> Typing_problem.t list = List.rev
let depth : t -> int = List.length
let add (h : t) (u : Typing_problem.t) = u :: h
let get_last_universe : t -> Typing_problem.t = List.hd
let get_all_but_last_universe : t -> t = List.tl

(* Ad-hoc measure. Is it good? Nobody knows. It takes the depth into account, so it is at least correct w.r.t termination *)
let attractiveness (h : t) : int =
  List.fold_left (fun acc u -> acc + Typing_problem.approximate_complexity u) 0 h
  + (2 * (Typing_problem.cardinal (get_last_universe h) + depth h))
(* let attractiveness (h : t) : int = depth h *)

let pp (c : Format.formatter) (h : t) : unit =
  let () = Format.fprintf c "\n---History begin---\n" in
  let () =
    List.iteri (fun i u -> Format.fprintf c "\n%dth:\n%a\n" i Typing_problem.pp u) (get_universes h)
  in
  let () = Format.fprintf c "\n---History ends---\n" in
  ()
