open Term

type witness =
  | Sat of Substitution.t
  | Unsat of Unit.t
[@@deriving equal, compare]

let pp_witness c w =
  match w with
  | Sat sub -> Format.fprintf c "Sat with subsitution %a" Substitution.pp sub
  | Unsat () -> Format.fprintf c "Unsat"

type t =
  | Witness of witness
  | OngoingProof of Unfolding_of_typingProblem.t
[@@deriving equal, compare]

let pp c partial_proof =
  match partial_proof with
  | Witness w -> Format.fprintf c "Proof finished: %a\n" pp_witness w
  | OngoingProof op -> Format.fprintf c "Proof still ongoing: %a\n" Unfolding_of_typingProblem.pp op

let get_witness_sat_opt witness =
  match witness with
  | Sat sub -> Some sub
  | Unsat _ -> None

let is_witness_unsat witness =
  match witness with
  | Unsat _ -> true
  | Sat _ -> false

let is_sat tt =
  match tt with
  | Witness (Sat _) -> true
  | _ -> false

let is_unsat tt =
  match tt with
  | Witness (Unsat ()) -> true
  | _ -> false

let get_sat_witness tt =
  match tt with
  | Witness (Sat sat_witness) -> sat_witness
  | _ -> assert false

let get_ongoing_proof_opt (tt : t) : Unfolding_of_typingProblem.t option =
  match tt with
  | OngoingProof op -> Some op
  | _ -> None

let is_solved (tt : t) : bool = Option.is_none (get_ongoing_proof_opt tt)

let get_children_universes_out_of_universe_unfolding (op : Unfolding_of_typingProblem.t) :
    Set_typing_problem.t =
  let children =
    op |> Unfolding_of_typingProblem.to_seq |> Seq.map snd
    |> Seq.map (fun sur ->
           sur |> Set_typingProblem_and_renaming.to_seq |> Seq.map fst |> Set_typing_problem.of_seq)
    |> Set_typing_problem.seq_union
  in
  children
