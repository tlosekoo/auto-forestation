open Term
open Term.Aliases
include Misc.My_set.Make2 (Litteral)

let pp_param ?(_see_recognizer = true) (c : Format.formatter) (universe : t) : unit =
  pp_param ~opening:"\n[[\n" ~closing:"\n]]\n" ~sep:"\n" ~pp_element:Litteral.pp c universe

let pp = pp_param ~_see_recognizer:false

let get_vars (u : t) : Set_typed_symbol.t =
  u |> to_list
  |> List.rev_map Litteral.get_patterns
  |> List.rev_map (List.rev_map Pattern.get_variables)
  |> List.rev_map Set_typed_symbol.list_union
  |> Set_typed_symbol.list_union

let apply_renaming (renaming : Typed_symbol.t -> Typed_symbol.t) : t -> t =
  map (Litteral.map_patterns (Pattern.map_variable_to_variable renaming))

(* let split_according_to_shape : t -> t list = split_into_equivalence_classes Litteral.same_shape *)

let split_according_to_shape_with_variable_comparator
    (cmp_var : Typed_symbol.t -> Typed_symbol.t -> int) : t -> t list =
  split_into_equivalence_classes (Litteral.same_shape_with_var_comparator cmp_var)

let get_variables_dfs_ordered (us : Litteral.t list) : Typed_symbol.t list =
  List.map Litteral.get_variables_dfs_ordered us |> List.flatten

let contains_a_typing_obligation_and_its_complement (u : t) : bool =
  let are_worth_comparing (n1 : Litteral.t) (n2 : Litteral.t) : bool =
    let same_patterns = Patterns.equal (Litteral.get_patterns n1) (Litteral.get_patterns n2) in
    let same_relation =
      Clause.Predicate_symbol_or_equality.equal (Litteral.get_relation n1)
        (Litteral.get_relation n2)
    in
    let out = same_patterns && same_relation in
    out
  in
  let two_contradictory_typing_problems_are_there =
    u
    |> split_into_equivalence_classes are_worth_comparing
    |> List.exists (fun similar_nodes -> cardinal similar_nodes > 1)
  in
  two_contradictory_typing_problems_are_there

(* let approximate_complexity (u : t) : int = cardinal u *)

let approximate_complexity (u : t) : int =
  (* let sum_of_arities_of_symbols =
    fold
      (fun litteral partial_sum ->
        let arity =
          litteral |> Litteral.get_relation |> Clause.Predicate_symbol_or_equality.into_relation
          |> Term.Typed_relation_symbol.get_arity
        in
        partial_sum + arity)
      u 0
  in *)
  (* sum_of_arities_of_symbols +  *)
  cardinal u 
