open Term
open Term.Aliases

let apply_renaming_to_variables
    (renaming_as_function : Typed_symbol.t -> Typed_symbol.t)
    (vars : Set_typed_symbol.t) : Set_typed_symbol.t =
  Set_typed_symbol.map renaming_as_function vars

let does_renaming_yields_subproblem
    (tp : Typing_problem.t)
    (renaming : Map_typedSymbol_typedSymbol.t) : bool =
  let renamed_tp =
    Typing_problem.apply_renaming
      (Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity renaming)
      tp
  in
  Typing_problem.subset renamed_tp tp
