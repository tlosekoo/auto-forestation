(*
module Inf_Sup = Misc.Pair.Make (Set_typed_symbol) (Set_typed_symbol)

module Map_inf_sup =
  Misc.My_map.Make2 (Typed_symbol) (Misc.Pair.Make (Set_typed_symbol) (Set_typed_symbol))

let compute__inf_sup_of_newly_added_variable_for_a_given_reference_variable
    (var_to_add_is_inf_to_var_of_ref : bool)
    (var_to_add_is_sup_to_var_of_ref : bool)
    (inf_of_var_of_ref : Set_typed_symbol.t)
    (sup_of_var_of_ref : Set_typed_symbol.t) : Inf_Sup.t =
  let sup_of_variable_to_add =
    if var_to_add_is_inf_to_var_of_ref then
      sup_of_var_of_ref
    else
      Set_typed_symbol.empty
  in
  let inf_of_variable_to_add =
    if var_to_add_is_sup_to_var_of_ref then
      inf_of_var_of_ref
    else
      Set_typed_symbol.empty
  in
  (inf_of_variable_to_add, sup_of_variable_to_add)

let use_transitivity_to_compute_a_section_of_inf_sup_of_a_newly_added_variable
    (map_inf_sup : Map_inf_sup.t)
    (variable_to_add : Typed_symbol.t) : Inf_Sup.t =
  Map_inf_sup.fold
    (fun _variable_of_reference (inf_of_var_of_ref, sup_of_var_of_ref) ->
      let var_to_add_is_inf_to_var_of_ref =
        Set_typed_symbol.mem variable_to_add inf_of_var_of_ref
      in
      let var_to_add_is_sup_to_var_of_ref =
        Set_typed_symbol.mem variable_to_add sup_of_var_of_ref
      in
      let inf_of_variable_to_add, sup_of_variable_to_add =
        compute__inf_sup_of_newly_added_variable_for_a_given_reference_variable
          var_to_add_is_inf_to_var_of_ref var_to_add_is_sup_to_var_of_ref inf_of_var_of_ref
          sup_of_var_of_ref
      in
      Misc.Pair.map
        (Set_typed_symbol.union inf_of_variable_to_add)
        (Set_typed_symbol.union sup_of_variable_to_add))
    map_inf_sup
    (Set_typed_symbol.empty, Set_typed_symbol.empty)

let add_variable_to_mapinfsup
    (map_inf_sup : Map_inf_sup.t)
    (unchecked_map_positions : Map_variable_setRelationPlace.t)
    (variable_to_add : Typed_symbol.t)
    (places_of_variable_to_add : Set_relation_place.t) : Map_inf_sup.t =
  let previously_known_inf_of_variable_to_add, previously_known_sup_of_variable_to_add =
    use_transitivity_to_compute_a_section_of_inf_sup_of_a_newly_added_variable map_inf_sup
      variable_to_add
  in
  let already_known =
    Set_typed_symbol.union previously_known_inf_of_variable_to_add
      previously_known_sup_of_variable_to_add
  in
  let projected_uncheck_map_positions_on_unknown =
    Map_variable_setRelationPlace.project_out already_known unchecked_map_positions
  in

  let inf_of_variable_to_add, sup_of_variable_to_add =
    Map_variable_setRelationPlace.fold
      (fun unknown_variable places_of_var_unknown
           (acc_inf_of_variable_to_add, acc_sup_of_variable_to_add) ->
        let var_unknown_is_inf_to_var_to_add =
          Set_relation_place.subset places_of_var_unknown places_of_variable_to_add
        in
        let var_unknown_is_sup_to_var_to_add =
          Set_relation_place.subset places_of_variable_to_add places_of_var_unknown
        in
        let updated_acc_inf_of_variable_to_add =
          if var_unknown_is_inf_to_var_to_add then
            Set_typed_symbol.add unknown_variable acc_inf_of_variable_to_add
          else
            acc_inf_of_variable_to_add
        in
        let updated_acc_sup_of_variable_to_add =
          if var_unknown_is_sup_to_var_to_add then
            Set_typed_symbol.add unknown_variable acc_sup_of_variable_to_add
          else
            acc_sup_of_variable_to_add
        in
        (updated_acc_inf_of_variable_to_add, updated_acc_sup_of_variable_to_add))
      projected_uncheck_map_positions_on_unknown
      (previously_known_inf_of_variable_to_add, previously_known_sup_of_variable_to_add)
  in
  Map_inf_sup.add variable_to_add (inf_of_variable_to_add, sup_of_variable_to_add) map_inf_sup

let compute_variable_compatibility_with_positions (map_positions : Map_variable_setRelationPlace.t)
     : Map_variable_setVariable.t =
   (* let variables = Map_variable_setVariable.domain map_positions in *)
   let initial_map_inf_sup = Map_inf_sup.empty in
   let map_inf_sup, _ =
     Map_variable_setRelationPlace.fold
       (fun variable_to_add positions_variable_to_add (acc_map_inf_sup, unckecked_map_positions) ->
         let updated_map_inf_sup =
           add_variable_to_mapinfsup acc_map_inf_sup unckecked_map_positions variable_to_add
             positions_variable_to_add
         in
         let updated_unchecked_variable =
           Map_variable_setRelationPlace.remove variable_to_add unckecked_map_positions
         in
         (updated_map_inf_sup, updated_unchecked_variable))
       map_positions
       (initial_map_inf_sup, map_positions)
   in
   Map_typedSymbol.map snd map_inf_sup *)

(* module Set_pair_variable = Misc.My_set.Make (Misc.Pair.Make (Typed_symbol) (Typed_symbol))

   let rec compute_all_inferiors_aux (map_positions_unchecked : Map_variable_setRelationPlace.t) :
       Set_pair_variable.t =
     match Map_variable_setRelationPlace.choose_opt map_positions_unchecked with
     | None -> Set_pair_variable.empty
     | Some (variable_to_add, positions_variable_to_add) ->
         let updated_map_positions_unchecked =
           Map_variable.remove variable_to_add map_positions_unchecked
         in
         let pair_id = (variable_to_add, variable_to_add) in
         let pairs_variable_inferior =
           updated_map_positions_unchecked |> Map_variable_setRelationPlace.to_seq
           |> Seq.map (fun (var_unchecked, positions_var_unchecked) ->
                  let variable_to_add_is_inf_to_variable_unchecked_pairs =
                    if Set_relation_place.subset positions_variable_to_add positions_var_unchecked then
                      Set_pair_variable.singleton (variable_to_add, var_unchecked)
                    else
                      Set_pair_variable.empty
                  in
                  let variable_to_add_is_sup_to_variable_unchecked_pairs =
                    if Set_relation_place.subset positions_var_unchecked positions_variable_to_add then
                      Set_pair_variable.singleton (var_unchecked, variable_to_add)
                    else
                      Set_pair_variable.empty
                  in
                  Set_pair_variable.union variable_to_add_is_inf_to_variable_unchecked_pairs
                    variable_to_add_is_sup_to_variable_unchecked_pairs)
           |> Set_pair_variable.seq_union
         in
         let all_pairs_for_variable_to_add = Set_pair_variable.add pair_id pairs_variable_inferior in
         let all_other_pairs = compute_all_inferiors_aux updated_map_positions_unchecked in
         Set_pair_variable.union all_other_pairs all_pairs_for_variable_to_add

   let compute_all_inferiors (map_positions : Map_variable_setRelationPlace.t) : Set_pair_variable.t =
     compute_all_inferiors_aux map_positions

   let compute_variable_compatibility_with_positions (map_positions : Map_variable_setRelationPlace.t)
       : Map_variable_setVariable.t =
     let all_inferiors = compute_all_inferiors map_positions in
     let map_position_as_list =
       Misc.List_op.partition_on_first_element Typed_symbol.compare
         (Set_pair_variable.to_list all_inferiors)
     in
     let map_compatibility =
       Map_variable_setVariable.of_list
         (List.map (Misc.Pair.map_snd Set_typed_symbol.of_list) map_position_as_list)
     in
     map_compatibility *)
