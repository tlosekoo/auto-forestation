open Term
open Term.Aliases

let rename_one_variable_in_typing_problem (var_src : Typed_symbol.t) (var_tgt : Typed_symbol.t) :
    Typing_problem.t -> Typing_problem.t =
  let is_var_src = Typed_symbol.equal var_src in
  Typing_problem.apply_renaming (fun var -> if is_var_src var then var_tgt else var)

let can_variable_be_replaced_by
    (typing_problem : Typing_problem.t)
    (var_src : Typed_symbol.t)
    (var_tgt : Typed_symbol.t) =
  let renamed_typing_problem =
    rename_one_variable_in_typing_problem var_src var_tgt typing_problem
  in
  Typing_problem.subset renamed_typing_problem typing_problem

let can_variable_be_replaced_by_any_of_aux
    (typing_problem : Typing_problem.t)
    (var_src : Typed_symbol.t) : Set_typed_symbol.t -> Typed_symbol.t option =
  Set_typed_symbol.find_opt_predicate (can_variable_be_replaced_by typing_problem var_src)

let can_variable_be_replaced_by_any_of
    (typing_problem : Typing_problem.t)
    (var_src : Typed_symbol.t)
    (vars_tgt : Set_typed_symbol.t) : (Typed_symbol.t * Typed_symbol.t) option =
  Option.map
    (fun var_tgt -> (var_src, var_tgt))
    (can_variable_be_replaced_by_any_of_aux typing_problem var_src vars_tgt)

let can_simplify_typing_problem_one_variable
    (typing_problem : Typing_problem.t)
    (map_compatibilities_without_identity : Map_variable_setVariable.t) :
    (Typed_symbol.t * Typed_symbol.t) option =
  Map_variable_setVariable.find_map map_compatibilities_without_identity (fun var_src vars_tgt ->
      can_variable_be_replaced_by_any_of typing_problem var_src vars_tgt)

let rec simplify_typing_problem_one_variable_by_one_variable_aux
    (typing_problem : Typing_problem.t)
    (map_compatibilities_without_identity : Map_variable_setVariable.t)
    (simplification_in_construction : Map_typedSymbol_typedSymbol.t) =
  match
    can_simplify_typing_problem_one_variable typing_problem map_compatibilities_without_identity
  with
  | None -> (simplification_in_construction, map_compatibilities_without_identity)
  | Some (var_src, var_tgt) ->
      let typing_problem_after_renaming =
        rename_one_variable_in_typing_problem var_src var_tgt typing_problem
      in
      let map_compatibilities_without_var_src =
        map_compatibilities_without_identity
        |> Map_variable_setVariable.remove var_src
        |> Map_variable_setVariable.map (Set_typed_symbol.remove var_src)
      in
      let extended_simplification_in_construction =
        Map_typedSymbol_typedSymbol.add var_src var_tgt simplification_in_construction
      in
      let () =
        Verbose_printing.pp_simplification_one_variable ~_verbose:false Typing_problem.pp
          Map_variable_setVariable.pp typing_problem map_compatibilities_without_identity var_src
          var_tgt simplification_in_construction typing_problem_after_renaming
      in
      simplify_typing_problem_one_variable_by_one_variable_aux typing_problem_after_renaming
        map_compatibilities_without_var_src extended_simplification_in_construction

let simplify_typing_problem_one_variable_by_one_variable
    (typing_problem : Typing_problem.t)
    (map_compatibilities : Map_variable_setVariable.t) :
    Map_typedSymbol_typedSymbol.t * Map_variable_setVariable.t =
  let map_compatibilities_without_identity =
    Map_variable_setVariable.mapi
      (fun var_src vars_tgt -> Set_typed_symbol.remove var_src vars_tgt)
      map_compatibilities
  in
  let initial_renaming = Map_variable_setVariable.empty in
  let renaming_to_iterate, projected_map_compabilities =
    simplify_typing_problem_one_variable_by_one_variable_aux typing_problem
      map_compatibilities_without_identity initial_renaming
  in
  let iterated_renaming =
    Misc.Others.fixpoint_from Map_typedSymbol_typedSymbol.equal
      (Map_typedSymbol_typedSymbol.auto_compose_considering_undefined_is_identity
         renaming_to_iterate)
      renaming_to_iterate
  in
  let projected_map_compabilities_with_identity =
    Map_variable_setVariable.mapi Set_typed_symbol.add projected_map_compabilities
  in
  (iterated_renaming, projected_map_compabilities_with_identity)
