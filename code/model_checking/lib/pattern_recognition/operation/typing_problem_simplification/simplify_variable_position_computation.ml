open Term
open Term.Aliases

let add_if_not_already_defined
    (map_positions : Map_variable_setRelationPlace.t)
    (variable : Typed_symbol.t)
    (position : Relation_place.t) =
  let update_current_positions_for_variable other_positions_opt =
    match other_positions_opt with
    | None -> Some (Set_relation_place.singleton position)
    | Some other_positions -> Some (Set_relation_place.add position other_positions)
  in
  Map_variable_setRelationPlace.update variable update_current_positions_for_variable map_positions

let union_maps_of_positions (maps : Map_variable_setRelationPlace.t list) :
    Map_variable_setRelationPlace.t =
  List.fold_left
    (Map_variable_setRelationPlace.union (fun _ map_1 map_2 ->
         Some (Set_relation_place.union map_1 map_2)))
    Map_variable_setRelationPlace.empty maps

(* Map variable universe map computation *)
let rec compute_variable_positions_one_pattern
    (current_polarized_relation : Polarized_relation_symbol_or_equality.t)
    (current_position : int list)
    (p : Pattern.t) : Map_variable_setRelationPlace.t =
  match Pattern.get_variable_opt p with
  | Some x ->
      Map_variable_setRelationPlace.singleton x
        (Set_relation_place.singleton (current_polarized_relation, current_position))
  | None ->
      let children = Pattern.get_children p in
      let maps_children =
        List.mapi
          (fun i ith_child ->
            compute_variable_positions_one_pattern current_polarized_relation
              (i :: current_position) ith_child)
          children
      in
      union_maps_of_positions maps_children

let compute_variable_positions_one_litteral (l : Litteral.t) : Map_variable_setRelationPlace.t =
  let patterns_of_litteral = Litteral.get_patterns l in
  let polarized_relation_of_litteral = (Litteral.get_relation l, Litteral.is_positive l) in
  let maps_each_pattern =
    List.mapi
      (fun i ith_pattern ->
        compute_variable_positions_one_pattern polarized_relation_of_litteral [i] ith_pattern)
      patterns_of_litteral
  in
  union_maps_of_positions maps_each_pattern

let compute_variable_positions (u : Typing_problem.t) : Map_variable_setRelationPlace.t =
  let maps_each_litteral =
    u |> Typing_problem.to_list |> List.map compute_variable_positions_one_litteral
  in
  union_maps_of_positions maps_each_litteral

let position_inclusion
    (map_positions : Map_variable_setRelationPlace.t)
    (positions_of_var_included : Set_relation_place.t)
    (var_superseding : Typed_symbol.t) : bool =
  Set_relation_place.subset positions_of_var_included
    (Map_variable_setRelationPlace.find var_superseding map_positions)

module Map_variable = Misc.My_map.Make (Typed_symbol)

let compute_variable_compatibility_with_positions (map_positions : Map_variable_setRelationPlace.t)
    : Map_variable_setVariable.t =
  let variables = Map_variable_setVariable.domain map_positions in
  let superseding_variables =
    Map_variable.mapi
      (fun _var_included positions_var_included ->
        Set_typed_symbol.filter (position_inclusion map_positions positions_var_included) variables)
      map_positions
  in
  superseding_variables
