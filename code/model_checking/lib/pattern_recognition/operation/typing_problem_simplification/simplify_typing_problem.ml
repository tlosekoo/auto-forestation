open Term
open Term.Aliases
open Simplify_variable_position_computation
open Typing_problem_manipulation

let extend_one_partial_renaming_with_one_target_variable
    (partial_renaming : Map_typedSymbol_typedSymbol.t)
    (var_src : Typed_symbol.t)
    (var_tgt : Typed_symbol.t) : Map_typedSymbol_typedSymbol.t =
  Map_typedSymbol_typedSymbol.add var_src var_tgt partial_renaming

let add_variable_to_one_partial_renaming_for_every_possible_target_variable
    (var_src : Typed_symbol.t)
    (vars_tgt : Set_typed_symbol.t)
    (partial_renaming : Map_typedSymbol_typedSymbol.t) : Set_renamings.t =
  let partial_renaming_as_function =
    Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity partial_renaming
  in
  let projected_vars_tgt = apply_renaming_to_variables partial_renaming_as_function vars_tgt in
  let every_extension_of_partial_renaming =
    projected_vars_tgt |> Set_typed_symbol.to_seq
    |> Seq.map (extend_one_partial_renaming_with_one_target_variable partial_renaming var_src)
    |> Set_renamings.of_seq
  in
  every_extension_of_partial_renaming

let add_variable_to_every_partial_renamings_for_every_possible_target_variable
    (var_src : Typed_symbol.t)
    (vars_tgt : Set_typed_symbol.t)
    (partial_renamings : Set_renamings.t) : Set_renamings.t =
  partial_renamings |> Set_renamings.to_seq
  |> Seq.map
       (add_variable_to_one_partial_renaming_for_every_possible_target_variable var_src vars_tgt)
  |> Set_renamings.seq_union

let compute_all_possible_renaming_that_is_compatible_with_positions
    (map_compatibility : Map_variable_setVariable.t) : Set_renamings.t =
  Map_variable_setVariable.fold
    add_variable_to_every_partial_renamings_for_every_possible_target_variable map_compatibility
    (Set_renamings.singleton Map_typedSymbol_typedSymbol.empty)

(* let compute_all_valid_simplifications (tp : Typing_problem.t) : Set_renamings.t =
   let () = Misc.Time_logger.start_time_stamp Timer.timer "variables positions" in
   let variables_positions = compute_variable_positions tp in
   let () = Misc.Time_logger.end_time_stamp Timer.timer "variables positions" in
   let () = Misc.Time_logger.start_time_stamp Timer.timer "variable subset compatibility" in
   let _variables_subset_compatibility =
     compute_variable_compatibility_with_positions variables_positions
   in
   let () = Misc.Time_logger.end_time_stamp Timer.timer "variable subset compatibility" in
   Set_renamings.singleton Map_typedSymbol_typedSymbol.empty *)

let compute_all_valid_simplifications (tp : Typing_problem.t) : Set_renamings.t =
  let () = Misc.Time_logger.start_time_stamp Timer.timer "variables positions" in
  let variables_positions = compute_variable_positions tp in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "variables positions" in
  let () = Misc.Time_logger.start_time_stamp Timer.timer "variable subset compatibility" in
  let variables_subset_compatibility =
    compute_variable_compatibility_with_positions variables_positions
  in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "variable subset compatibility" in

  let simplification_one_variable_by_one_variable, projected_variables_subset_compatibility =
    Simplify_typing_problem_one_variable.simplify_typing_problem_one_variable_by_one_variable tp
      variables_subset_compatibility
  in

  let simplified_tp_by_unitary_renaming =
    Typing_problem.apply_renaming
      (Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity
         simplification_one_variable_by_one_variable)
      tp
  in
  let renamings_that_are_position_compatible =
    compute_all_possible_renaming_that_is_compatible_with_positions
      projected_variables_subset_compatibility
  in
  let all_valid_renamings_on_simplified_tp =
    Set_renamings.filter
      (does_renaming_yields_subproblem simplified_tp_by_unitary_renaming)
      renamings_that_are_position_compatible
  in
  let all_valid_composed_renamings =
    Set_renamings.map
      (Map_typedSymbol_typedSymbol.auto_compose_considering_undefined_is_identity
         simplification_one_variable_by_one_variable)
      all_valid_renamings_on_simplified_tp
  in
  let () =
    Verbose_printing.pp_simplification_information ~_verbose:false Map_variable_setVariable.pp
      variables_subset_compatibility
      (Set_renamings.cardinal renamings_that_are_position_compatible)
      (Set_renamings.cardinal all_valid_composed_renamings)
  in
  all_valid_composed_renamings

(* Identity is always valid, so no Optional here *)
let find_a_smallest_simplification (tp : Typing_problem.t) : Map_typedSymbol_typedSymbol.t =
  let all_valid_simplifications = compute_all_valid_simplifications tp in
  let minimal_renaming =
    all_valid_simplifications |> Set_renamings.to_list
    |> Misc.Min_and_max.min_in_list (fun renaming_1 renaming_2 ->
           Int.compare
             (Map_typedSymbol_typedSymbol.cardinal renaming_1)
             (Map_typedSymbol_typedSymbol.cardinal renaming_2))
    |> Option.get
  in
  let () =
    Verbose_printing.pp_simplification ~_verbose:false Typing_problem.pp tp minimal_renaming
      (Typing_problem.apply_renaming
         (Term.Aliases.Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity
            minimal_renaming)
         tp)
  in
  minimal_renaming

(* let simplify : Typing_problem.t -> Map_typedSymbol_typedSymbol.t = find_a_smallest_simplification *)
let simplify_into_smallest (tp : Typing_problem.t) : Map_typedSymbol_typedSymbol.t =
  let () = Misc.Time_logger.start_time_stamp Timer.timer "simplifying" in
  let smallest_simplification = find_a_smallest_simplification tp in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "simplifying" in
  smallest_simplification

let simplify_one_by_one (tp : Typing_problem.t) : Map_typedSymbol_typedSymbol.t =
  tp |> compute_variable_positions
  |> Simplify_variable_position_computation.compute_variable_compatibility_with_positions
  |> Simplify_typing_problem_one_variable.simplify_typing_problem_one_variable_by_one_variable tp
  |> fst

let simplify : Typing_problem.t -> Map_typedSymbol_typedSymbol.t =
  if !Parameters_pattern_recognition.parameters.only_use_one_by_one_simplification then
    simplify_one_by_one
  else
    simplify_into_smallest
