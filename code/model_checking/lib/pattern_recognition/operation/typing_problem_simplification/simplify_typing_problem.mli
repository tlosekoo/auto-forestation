val compute_all_possible_renaming_that_is_compatible_with_positions :
  Map_variable_setVariable.t -> Set_renamings.t

val compute_all_valid_simplifications : Typing_problem.t -> Set_renamings.t

val find_a_smallest_simplification :
  Typing_problem.t -> Term.Aliases_symbol.Map_typedSymbol_typedSymbol.t

val simplify : Typing_problem.t -> Term.Aliases_symbol.Map_typedSymbol_typedSymbol.t
