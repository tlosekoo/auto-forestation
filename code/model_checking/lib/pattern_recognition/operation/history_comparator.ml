type t = History.t [@@deriving equal]

let pp = History.pp
let compare h1 h2 = Int.compare (History.attractiveness h1) (History.attractiveness h2)
