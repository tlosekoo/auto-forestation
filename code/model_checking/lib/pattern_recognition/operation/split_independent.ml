open Term.Aliases

(* Not very optimized. (vars of sets are computed mutliple times) *)
let merge_universes_made_dependent_by_new_universe
    (us : Set_typing_problem.t)
    (u : Typing_problem.t) : Set_typing_problem.t =
  let vars_of_u = Typing_problem.get_vars u in
  let us_not_connected, us_connected =
    Set_typing_problem.partition
      (fun u_in_us ->
        u_in_us |> Typing_problem.get_vars |> Set_typed_symbol.inter vars_of_u
        |> Set_typed_symbol.is_empty)
      us
  in
  let us_connected_united =
    Typing_problem.seq_union (Set_typing_problem.add u us_connected |> Set_typing_problem.to_seq)
  in
  Set_typing_problem.add us_connected_united us_not_connected

let split_independent (u : Typing_problem.t) : Set_typing_problem.t =
  if Typing_problem.is_empty u then
    Set_typing_problem.singleton Typing_problem.empty
  else
    let every_singleton = u |> Typing_problem.to_list |> List.rev_map Typing_problem.singleton in
    let () = Misc.Time_logger.start_time_stamp Timer.timer "splitting independent" in
    let independent_problems =
      List.fold_left merge_universes_made_dependent_by_new_universe Set_typing_problem.empty
        every_singleton
    in
    let () = Misc.Time_logger.end_time_stamp Timer.timer "splitting independent" in
    independent_problems
