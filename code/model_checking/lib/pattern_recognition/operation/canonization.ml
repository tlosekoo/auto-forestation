open Term
open Term.Aliases
module Variable_numbering = Misc.My_map.Make2 (Typed_symbol) (Misc.Int)

let number_variables (ordered_vars : Typed_symbol.t list) : Variable_numbering.t =
  let _, numbering =
    List.fold_left
      (fun (counter, current_numbering) var ->
        match Variable_numbering.find_opt var current_numbering with
        | None ->
            let new_numbering = Variable_numbering.add var counter current_numbering in
            (counter + 1, new_numbering)
        | Some _ -> (counter, current_numbering))
      (0, Variable_numbering.empty) ordered_vars
  in
  numbering

let create_variable_of_rank (d : Datatype.t) (rank : int) : Typed_symbol.t =
  let name = "v" ^ string_of_int rank in
  Typed_symbol.create (Symbol.create name) d

let linearize_equivalence_classes (u : Typing_problem.t list) : Litteral.t list =
  u |> List.map Typing_problem.to_list |> List.map (List.sort Litteral.compare) |> List.flatten

(* let linearize_equivalence_classes_with_variable_positions
     (map_positions : Map_variable_setRelationPlace.t)
     (u : Typing_problem.t list) : Litteral.t list =
   u |> List.map Typing_problem.to_list
   |> List.map (fun litteral ->
   |> List.map (List.sort Litteral.compare) |> List.flatten *)

let create_variable_renaming_from_variable_numbering (numbering : Variable_numbering.t) :
    Map_typedSymbol_typedSymbol.t =
  Variable_numbering.mapi (fun var -> create_variable_of_rank (Typed_symbol.get_type var)) numbering

(* Lousy normalize function *)
let split_according_to_shape_and_variable_position
    (map_positions : Map_variable_setRelationPlace.t)
    (u : Typing_problem.t) : Typing_problem.t list =
  let cmp_var (v1 : Typed_symbol.t) (v2 : Typed_symbol.t) =
    Set_relation_place.compare
      (Map_variable_setRelationPlace.find v1 map_positions)
      (Map_variable_setRelationPlace.find v2 map_positions)
  in
  Typing_problem.split_according_to_shape_with_variable_comparator cmp_var u

(** Returns the variable renaming together with the canonized universe *)
let canonize (u : Typing_problem.t) : Typing_problem.t * Map_typedSymbol_typedSymbol.t =
  let () = Misc.Time_logger.start_time_stamp Timer.timer "canonize" in
  (* TODO: Is already computed for simplification, do not compute twice *)
  let map_positions = Simplify_variable_position_computation.compute_variable_positions u in
  let renaming_map =
    u
    |> split_according_to_shape_and_variable_position map_positions
    |> List.sort Typing_problem.compare |> linearize_equivalence_classes
    |> Typing_problem.get_variables_dfs_ordered |> number_variables
    |> create_variable_renaming_from_variable_numbering
  in
  let renaming = Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity renaming_map in
  let renamed_universe = Typing_problem.apply_renaming renaming u in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "canonize" in

  (renamed_universe, renaming_map)
