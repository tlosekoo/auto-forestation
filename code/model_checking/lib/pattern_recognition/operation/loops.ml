open Term.Aliases
module Map_potential_map = Misc.List_maker.Make (Misc.Pair.Make (Litteral) (Typing_problem))

type map_potential_match = Map_potential_map.t

let rec possibilities_are_compatible
    (matchings_to_do : (Litteral.t * Typing_problem.t) list)
    (current_renaming : Map_typedSymbol_typedSymbol.t) : Map_typedSymbol_typedSymbol.t option =
  match matchings_to_do with
  | [] -> Some current_renaming
  | (ponode_to_match, potential_matches) :: rest_matchings_to_do ->
      Typing_problem.fold
        (fun potential_match renaming_opt ->
          match renaming_opt with
          | Some _ -> renaming_opt
          | None ->
              let current_renaming_updated =
                Litteral_matching.complete_renaming_with_node_matching current_renaming
                  ponode_to_match potential_match
              in
              Option.bind current_renaming_updated
                (possibilities_are_compatible rest_matchings_to_do))
        potential_matches None

let rec merge_similar_nodes
    (sorted_nodes_src : Litteral.t list list)
    (sorted_nodes_tgt : Litteral.t list list) : map_potential_match option =
  match (sorted_nodes_src, sorted_nodes_tgt) with
  | [], _ -> Some []
  | _, [] -> None
  | _, [[]] -> None
  | hd_srcs :: tl_srcs, hd_tgts :: tl_tgts ->
      let s = List.hd hd_srcs in
      let t = List.hd hd_tgts in
      (* let () = Format.fprintf Format.std_formatter "Comparing %a and %a" Litteral.pp s Litteral.pp t in *)
      if Litteral.same_shape s t then
        let tgts = Typing_problem.of_list hd_tgts in
        let local_possible_map = List.rev_map (fun s -> (s, tgts)) hd_srcs in
        let of_rec = merge_similar_nodes tl_srcs tl_tgts in
        Option.map (( @ ) local_possible_map) of_rec
      else
        merge_similar_nodes sorted_nodes_src tl_tgts

(* Could be optimized by sorting every node according to their shape and merging them *)
let assign_to_each_node_the_set_of_possible_match
    (nodes_src : Typing_problem.t)
    (nodes_tgt : Typing_problem.t) : map_potential_match option =
  let srcs =
    Typing_problem.to_list nodes_src
    |> Misc.List_op.split_into_equivalence_classes Litteral.same_shape
  in
  let tgts =
    Typing_problem.to_list nodes_tgt
    |> Misc.List_op.split_into_equivalence_classes Litteral.same_shape
  in
  (* let () =
       Format.fprintf Format.std_formatter "Trying to match\n%a\n\nwith\n%a\n\n"
       (Printing.pp_list_sep "\n--\n" (Printing.pp_list_and_brackets Litteral.pp))
       srcs
       (Printing.pp_list_sep "\n--\n" (Printing.pp_list_and_brackets Litteral.pp))
       tgts
     in *)
  let possible_merges = merge_similar_nodes srcs tgts in
  (* let () =
       Format.fprintf Format.std_formatter "Possible merges:\n%a\n\n"
         (Printing.pp_opt (Printing.pp_assoclist_long Litteral.pp Typing_problem.pp))
         possible_merges
     in *)
  possible_merges

let is_simpler_with_renaming (simpler_one : Typing_problem.t) (harder_one : Typing_problem.t) :
    Map_typedSymbol_typedSymbol.t option =
  let matchings_to_do_opt = assign_to_each_node_the_set_of_possible_match simpler_one harder_one in
  (* let () =
       Format.fprintf Format.std_formatter "matchings_to_do_opt: \n%a\n"
         (Printing.pp_opt Map_potential_map.pp)
         matchings_to_do_opt
     in *)
  Option.bind matchings_to_do_opt (fun matchings_to_do ->
      possibilities_are_compatible matchings_to_do Map_typedSymbol_typedSymbol.empty)
(* *)

let loops_with_proof (h : History.t) : (Typing_problem.t * Map_typedSymbol_typedSymbol.t) option =
  (* let () = Format.fprintf Format.std_formatter "Trying to check loops!\n" in
     let () = Format.pp_print_flush Format.std_formatter () in *)
  let () = Misc.Time_logger.start_time_stamp Timer.timer "loops" in

  let last_universe = History.get_last_universe h in
  let other_universes = History.get_all_but_last_universe h in
  let simpler_universe_opt =
    List.find_map
      (fun u_i -> Option.map (fun r -> (u_i, r)) (is_simpler_with_renaming u_i last_universe))
      other_universes
  in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "loops" in

  (* let () = Format.fprintf Format.std_formatter "Done checking!\n" in
     let () = Format.pp_print_flush Format.std_formatter () in *)
  simpler_universe_opt

let loops (h : History.t) : bool = Option.is_some (loops_with_proof h)
