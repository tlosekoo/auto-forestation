open Term
open Term.Aliases

let complete_renaming_with_pattern_matching
    (current_renaming : Map_typedSymbol_typedSymbol.t)
    (pattern_source : Pattern.t)
    (pattern_target : Pattern.t) : Map_typedSymbol_typedSymbol.t option =
  let () = assert (Pattern.compare_shape pattern_source pattern_target = 0) in
  let vars_source = Pattern.get_variables_dfs_ordered pattern_source in
  let vars_target = Pattern.get_variables_dfs_ordered pattern_target in
  List.fold_left2
    (fun renaming_opt x_src x_tgt ->
      Option.bind renaming_opt (fun renaming ->
          try Some (Map_typedSymbol_typedSymbol.add_compatible_binding x_src x_tgt renaming) with
          | _ -> None))
    (Some current_renaming) vars_source vars_target

let complete_renaming_with_node_matching
    (current_renaming : Map_typedSymbol_typedSymbol.t)
    (node_source : Litteral.t)
    (node_target : Litteral.t) : Map_typedSymbol_typedSymbol.t option =
  let () = assert (Litteral.same_shape node_source node_target) in
  let () = assert (Litteral.same_shape node_source node_target) in
  let patterns_source = Litteral.get_patterns node_source in
  let patterns_target = Litteral.get_patterns node_target in
  List.fold_left2
    (fun renaming_opt p_src p_tgt ->
      Option.bind renaming_opt (fun renaming ->
          complete_renaming_with_pattern_matching renaming p_src p_tgt))
    (Some current_renaming) patterns_source patterns_target
