include Misc.Tription

type stopping_reason =
  | OutOfFuel
  | OutOfTime
  | Other of String.t
[@@deriving compare, equal]

let pp_stopping_reason (c : Format.formatter) (sr : stopping_reason) : unit =
  match sr with
  | OutOfFuel -> Format.pp_print_string c "out of fuel"
  | OutOfTime -> Format.pp_print_string c "out of time"
  | Other reason -> Format.fprintf c "other: %s" reason

type 'a t = ('a, stopping_reason, Unit.t) Misc.Tription.t [@@deriving compare, equal]

let pp (pp_found : Format.formatter -> 'a -> unit) =
  Misc.Tription.pp pp_found pp_stopping_reason Printing.pp_unit

(* More or less a join between the accumulator and `f e`. Useful for defining fold_left. *)
let step_search
    (f : 'input -> 'side_information * 'output t)
    (acc : 'side_information * 'output t)
    (e : 'input) : 'side_information * 'output t =
  match snd acc with
  | Yes _ -> acc
  | Maybe _ ->
      let side, new_element = f e in
      if Misc.Tription.is_yes new_element then
        (side, new_element)
      else
        acc
  | No _ -> f e
