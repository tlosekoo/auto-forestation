(* TODO: use the equivalent in MISC *)

let is_time_up (time_opt : Misc.Time.time_limit) : bool =
  match time_opt with
  | None -> false
  | Some time -> Unix.gettimeofday () > time

exception TimeUp

let raise_time_up (time_opt : Misc.Time.time_limit) : unit =
  if is_time_up time_opt then raise TimeUp
