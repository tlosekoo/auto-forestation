open Clause
open Term.Database_for_tests
open Model_checking
(* [[
   height(_rza_2, _tza_0)
   height(_uza_2, _sza_0)
   height(_vza_2, _wza_0)
   not le(_wza_0, _tza_0)
   not shallower(_rza_0, _sza_0)
   shallower(_rza_0, _tza_0)
   not shallower(_rza_2, _sza_0)
   shallower(_uza_0, _sza_0)
   shallower(_uza_0, _wza_0)
   shallower(_uza_2, _wza_0)
   shallower(_vza_0, _sza_0)
   shallower(_vza_0, _wza_0)
   shallower(_vza_2, _sza_0)
   shallower(_xza_0, _tza_0)
   shallower(_xza_2, _tza_0)
   ]] *)
(*
   _rza_2 -> t1
   _uza_2 -> t2
   _vza_2 -> t3
   _rza_0 -> t4
   _uza_0 -> t5
   _vza_0 -> t6
   _xza_0 -> t7
   _xza_2 -> t8

   _tza_0 -> n1
   _sza_0 -> n2
   _wza_0 -> n3
*)

let atom_height_t1_n1 = Atom_patterns.create_from_predicate_symbol relation_height [p_t1; p_n1]
let atom_height_t2_n2 = Atom_patterns.create_from_predicate_symbol relation_height [p_t2; p_n2]
let atom_height_t3_n3 = Atom_patterns.create_from_predicate_symbol relation_height [p_t3; p_n3]
let atom_le_n3_n1 = Atom_patterns.create_from_predicate_symbol relation_le [p_n3; p_n1]

let atom_shallower_t4_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n2]

let atom_shallower_t4_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n1]

let atom_shallower_t1_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t1; p_n2]

let atom_shallower_t5_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t5; p_n2]

let atom_shallower_t5_n3 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t5; p_n3]

let atom_shallower_t2_n3 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t2; p_n3]

let atom_shallower_t6_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t6; p_n2]

let atom_shallower_t6_n3 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t6; p_n3]

let atom_shallower_t3_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t3; p_n2]

let atom_shallower_t7_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t7; p_n1]

let atom_shallower_t8_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t8; p_n1]

let litteral_height_t1_n1 = Litteral.create ~is_positive:true atom_height_t1_n1
let litteral_height_t2_n2 = Litteral.create ~is_positive:true atom_height_t2_n2
let litteral_height_t3_n3 = Litteral.create ~is_positive:true atom_height_t3_n3
let litteral_le_n3_n1 = Litteral.create ~is_positive:true atom_le_n3_n1
let litteral_not_shallower_t4_n2 = Litteral.create ~is_positive:false atom_shallower_t4_n2
let litteral_shallower_t4_n1 = Litteral.create ~is_positive:true atom_shallower_t4_n1
let litteral_not_shallower_t1_n2 = Litteral.create ~is_positive:false atom_shallower_t1_n2
let litteral_shallower_t5_n2 = Litteral.create ~is_positive:true atom_shallower_t5_n2
let litteral_shallower_t5_n3 = Litteral.create ~is_positive:true atom_shallower_t5_n3
let litteral_shallower_t2_n3 = Litteral.create ~is_positive:true atom_shallower_t2_n3
let litteral_shallower_t6_n2 = Litteral.create ~is_positive:true atom_shallower_t6_n2
let litteral_shallower_t6_n3 = Litteral.create ~is_positive:true atom_shallower_t6_n3
let litteral_shallower_t3_n2 = Litteral.create ~is_positive:true atom_shallower_t3_n2
let litteral_shallower_t7_n1 = Litteral.create ~is_positive:true atom_shallower_t7_n1
let litteral_shallower_t8_n1 = Litteral.create ~is_positive:true atom_shallower_t8_n1

let heavy_typing_problem_to_simplify =
  Typing_problem.of_list
    [
      litteral_height_t1_n1;
      litteral_height_t2_n2;
      litteral_height_t3_n3;
      litteral_le_n3_n1;
      litteral_not_shallower_t4_n2;
      litteral_shallower_t4_n1;
      litteral_not_shallower_t1_n2;
      litteral_shallower_t5_n2;
      litteral_shallower_t5_n3;
      litteral_shallower_t2_n3;
      litteral_shallower_t6_n2;
      litteral_shallower_t6_n3;
      litteral_shallower_t3_n2;
      litteral_shallower_t7_n1;
      litteral_shallower_t8_n1;
    ]

(* Heavy problem to simplify 2 *)

let v0 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v0") Term.Database_for_tests.elt_tree)

let v1 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v1") Term.Database_for_tests.elt_tree)

let v2 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v2") Term.Database_for_tests.elt_tree)

let v3 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v3") Term.Database_for_tests.nat)

let v4 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v4") Term.Database_for_tests.elt_tree)

let v5 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v5") Term.Database_for_tests.nat)

let v6 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v6") Term.Database_for_tests.elt_tree)

let v7 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v7") Term.Database_for_tests.elt_tree)

let v8 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v8") Term.Database_for_tests.elt_tree)

let v9 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v9") Term.Database_for_tests.elt_tree)

let v10 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "v10") Term.Database_for_tests.elt_tree)

(* height(v0, v1)
   height(v2, v3)
   height(v4, v5)
   not le(v5, v1)
   not shallower(v0, v3)
   shallower(v10, v1)
   shallower(v2, v5)
   shallower(v4, v3)
   shallower(v6, v1)
   shallower(v7, v3)
   shallower(v7, v5)
   shallower(v8, v3)
   shallower(v8, v5)
   shallower(v9, v1)
   not shallower(v9, v3) *)
let atom_height_v0_v1 = Atom_patterns.create_from_predicate_symbol relation_height [v0; v1]
let atom_height_v2_v3 = Atom_patterns.create_from_predicate_symbol relation_height [v2; v3]
let atom_height_v4_v5 = Atom_patterns.create_from_predicate_symbol relation_height [v4; v5]
let atom_le_v5_v1 = Atom_patterns.create_from_predicate_symbol relation_le [v5; v1]
let atom_shallower_v0_v3 = Atom_patterns.create_from_predicate_symbol relation_shallower [v0; v3]
let atom_shallower_v10_v1 = Atom_patterns.create_from_predicate_symbol relation_shallower [v10; v1]
let atom_shallower_v2_v5 = Atom_patterns.create_from_predicate_symbol relation_shallower [v2; v5]
let atom_shallower_v4_v3 = Atom_patterns.create_from_predicate_symbol relation_shallower [v4; v3]
let atom_shallower_v6_v1 = Atom_patterns.create_from_predicate_symbol relation_shallower [v6; v1]
let atom_shallower_v7_v3 = Atom_patterns.create_from_predicate_symbol relation_shallower [v7; v3]
let atom_shallower_v7_v5 = Atom_patterns.create_from_predicate_symbol relation_shallower [v7; v5]
let atom_shallower_v8_v3 = Atom_patterns.create_from_predicate_symbol relation_shallower [v8; v3]
let atom_shallower_t8_v5 = Atom_patterns.create_from_predicate_symbol relation_shallower [v8; v5]
let atom_shallower_v9_v1 = Atom_patterns.create_from_predicate_symbol relation_shallower [v9; v1]
let atom_shallower_v9_v3 = Atom_patterns.create_from_predicate_symbol relation_shallower [v9; v3]
let litteral_height_v0_v1 = Litteral.create ~is_positive:true atom_height_v0_v1
let litteral_height_v2_v3 = Litteral.create ~is_positive:true atom_height_v2_v3
let litteral_height_v4_v5 = Litteral.create ~is_positive:true atom_height_v4_v5
let litteral_le_v5_v1 = Litteral.create ~is_positive:false atom_le_v5_v1
let litteral_shallower_v0_v3 = Litteral.create ~is_positive:false atom_shallower_v0_v3
let litteral_shallower_v10_v1 = Litteral.create ~is_positive:true atom_shallower_v10_v1
let litteral_shallower_v2_v5 = Litteral.create ~is_positive:true atom_shallower_v2_v5
let litteral_shallower_v4_v3 = Litteral.create ~is_positive:true atom_shallower_v4_v3
let litteral_shallower_v6_v1 = Litteral.create ~is_positive:true atom_shallower_v6_v1
let litteral_shallower_v7_v3 = Litteral.create ~is_positive:true atom_shallower_v7_v3
let litteral_shallower_v7_v5 = Litteral.create ~is_positive:true atom_shallower_v7_v5
let litteral_shallower_v8_v3 = Litteral.create ~is_positive:true atom_shallower_v8_v3
let litteral_shallower_t8_v5 = Litteral.create ~is_positive:true atom_shallower_t8_v5
let litteral_shallower_v9_v1 = Litteral.create ~is_positive:true atom_shallower_v9_v1
let litteral_shallower_v9_v3 = Litteral.create ~is_positive:false atom_shallower_v9_v3

let heavy_typing_problem_to_simplify_2 =
  Typing_problem.of_list
    [
      litteral_height_v0_v1;
      litteral_height_v2_v3;
      litteral_height_v4_v5;
      litteral_le_v5_v1;
      litteral_shallower_v0_v3;
      litteral_shallower_v10_v1;
      litteral_shallower_v2_v5;
      litteral_shallower_v4_v3;
      litteral_shallower_v6_v1;
      litteral_shallower_v7_v3;
      litteral_shallower_v7_v5;
      litteral_shallower_v8_v3;
      litteral_shallower_t8_v5;
      litteral_shallower_v9_v1;
      litteral_shallower_v9_v3;
    ]
(* 3 *)

(*
    height(t0, n0)
    height(t1, n1)
    height(t2, n2)
    not le(n1, n2)
    shallower(t0, n1)
    shallower(t3, n0)
    shallower(t3, n1)
    shallower(t1, n0)
    shallower(t4, n0)
    shallower(t4, n1)
    shallower(t5, n2)
    shallower(t6, n2)
    shallower(t7, n2)
    shallower(t8, n2)
    not shallower(t9, n0)
    shallower(t9, n2) *)

let p_t0 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "t0") Term.Database_for_tests.elt_tree)

let p_n0 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "n0") Term.Database_for_tests.nat)

let atom_height_t0_n0 = Atom_patterns.create_from_predicate_symbol relation_height [p_t0; p_n0]
let atom_height_t1_n1 = Atom_patterns.create_from_predicate_symbol relation_height [p_t1; p_n1]
let atom_height_t2_n2 = Atom_patterns.create_from_predicate_symbol relation_height [p_t2; p_n2]
let atom_le_n1_n2 = Atom_patterns.create_from_predicate_symbol relation_le [p_n1; p_n2]

let atom_shallower_t0_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t0; p_n1]

let atom_shallower_t3_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t3; p_n0]

let atom_shallower_t3_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t3; p_n1]

let atom_shallower_t1_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t1; p_n0]

let atom_shallower_t4_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n0]

let atom_shallower_t4_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n1]

let atom_shallower_t5_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t5; p_n2]

let atom_shallower_t6_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t6; p_n2]

let atom_shallower_t7_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t7; p_n2]

let atom_shallower_t8_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t8; p_n2]

let atom_shallower_t9_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t9; p_n0]

let atom_shallower_t9_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t9; p_n2]

let litteral_height_t0_n0 = Litteral.create ~is_positive:true atom_height_t0_n0
let litteral_height_t1_n1 = Litteral.create ~is_positive:true atom_height_t1_n1
let litteral_height_t2_n2 = Litteral.create ~is_positive:true atom_height_t2_n2
let litteral_le_n1_n2 = Litteral.create ~is_positive:false atom_le_n1_n2
let litteral_shallower_t0_n1 = Litteral.create ~is_positive:false atom_shallower_t0_n1
let litteral_shallower_t3_n0 = Litteral.create ~is_positive:true atom_shallower_t3_n0
let litteral_shallower_t3_n1 = Litteral.create ~is_positive:true atom_shallower_t3_n1
let litteral_shallower_t4_n0 = Litteral.create ~is_positive:true atom_shallower_t4_n0
let litteral_shallower_t4_n1 = Litteral.create ~is_positive:true atom_shallower_t4_n1
let litteral_shallower_t5_n2 = Litteral.create ~is_positive:true atom_shallower_t5_n2
let litteral_shallower_t6_n2 = Litteral.create ~is_positive:true atom_shallower_t6_n2
let litteral_shallower_t7_n2 = Litteral.create ~is_positive:true atom_shallower_t7_n2
let litteral_shallower_t8_n2 = Litteral.create ~is_positive:true atom_shallower_t8_n2
let litteral_shallower_t9_n0 = Litteral.create ~is_positive:true atom_shallower_t9_n0
let litteral_shallower_t9_n2 = Litteral.create ~is_positive:false atom_shallower_t9_n2

let heavy_typing_problem_to_simplify_3 =
  Typing_problem.of_list
    [
      litteral_height_t0_n0;
      litteral_height_t1_n1;
      litteral_height_t2_n2;
      litteral_le_n1_n2;
      litteral_shallower_t0_n1;
      litteral_shallower_t3_n0;
      litteral_shallower_t3_n1;
      litteral_shallower_t4_n0;
      litteral_shallower_t4_n1;
      litteral_shallower_t5_n2;
      litteral_shallower_t6_n2;
      litteral_shallower_t7_n2;
      litteral_shallower_t8_n2;
      litteral_shallower_t9_n0;
      litteral_shallower_t9_n2;
    ]
