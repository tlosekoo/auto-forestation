open Model_checking
open Instantiation

let model_height_shallower_le =
  ModelShocs.create Term.Database_for_tests.testing_type_env
    (Term.Aliases.Set_typed_relation_symbol.of_list
       [
         Term.Database_for_tests.relation_height;
         Term.Database_for_tests.relation_shallower;
         Term.Database_for_tests.relation_le;
       ])
    Tree_tuple_formalisms.Database_for_tests_shocs.shocs_height_and_shallower_and_le

let _solver_output () =
  ModelCheckingShocs.model_check_one_formula
    ~time_to_stop:(Some (Unix.gettimeofday () +. 20.))
    ~_debug_mode:true model_height_shallower_le Clause.Database_for_tests.clause_height_recursive_1

let _solving_stuff () =
  let () = Misc.Time_logger.start_time_stamp Timer.timer "whole_solving" in

  let solver_output = _solver_output () in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "whole_solving" in

  let () =
    Format.fprintf Format.std_formatter "Solver output:\n%a\n" Search_outcome_for_one_formula.pp
      solver_output
  in
  let () =
    Format.fprintf Format.std_formatter "Size of time log:\n%d\n" (List.length !Timer.timer)
  in
  let () = Timer.write_to_file () in
  ()

let () = _solving_stuff ()

let _simplification_stuff () =
  (* let () = Search_outcome.pp Term.Substitution.pp Format.std_formatter solver_output *)
  let () = Misc.Time_logger.start_time_stamp Timer.timer "whole simplification" in
  let _simplification =
    Simplify_typing_problem.simplify
      Heavy_typing_problem_to_simplify.heavy_typing_problem_to_simplify_3
  in
  let simplified_heavy_typing_problem =
    Typing_problem.apply_renaming
      (Term.Aliases.Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity
         _simplification)
      Heavy_typing_problem_to_simplify.heavy_typing_problem_to_simplify_3
  in
  let () =
    Format.fprintf Format.std_formatter
      "\n\nSimplified typing problem:\n%a\n\nSimplification:\n%a\n" Typing_problem.pp
      simplified_heavy_typing_problem Term.Aliases.Map_typedSymbol_typedSymbol.pp _simplification
  in
  let () = Misc.Time_logger.end_time_stamp Timer.timer "whole simplification" in
  let () = Timer.write_to_file () in

  ()

(* let () = _simplification_stuff () *)
