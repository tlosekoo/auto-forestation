open Model_checking
open Instantiation

let%expect_test "printing typing_problem_0_prrs" =
  Typing_problem.pp Format.std_formatter Database_for_tests.typing_problem_0;
  [%expect "
  [[
  r_n(n1)
  ]]"]

let%expect_test "printing typing_problem_1_prrs" =
  Typing_problem.pp Format.std_formatter Database_for_tests.typing_problem_1;
  [%expect "
  [[
  even(n2)
  odd(n2)
  ]]"]

let%expect_test "printing typing_problem_2_prrs" =
  Typing_problem.pp Format.std_formatter Database_for_tests.typing_problem_2;
  [%expect "
    [[
    odd(n)
    r_l_n(l, s(n))
    ]]"]

let%expect_test "printing typing_problem_3_prrs" =
  Typing_problem.pp Format.std_formatter Database_for_tests.typing_problem_3;
  [%expect "
    [[
    r_nat(n)
    ]]"]

let%expect_test "model length" =
  ModelShocs.pp Format.std_formatter Database_for_tests.model_length__shocs;
  [%expect
    "
    |_

    r_l_n ->
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
    }
    ;
    r_n ->
    {
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_z_n" =
  ModelShocs.pp Format.std_formatter Database_for_tests.model_length_z_n__shocs;
  [%expect
    "
    |_

    r_l_n ->
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
    }
    ;
    r_n ->
    {
      r_n(z) <= True
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_all" =
  ModelShocs.pp Format.std_formatter Database_for_tests.model_length_all__shocs;
  [%expect
    "
    |_

    r_natlist_x_nat ->
    {
      r_natlist_x_nat(cons(x_0_0, x_0_1), s(x_1_0)) <= True
      r_natlist_x_nat(cons(x_0_0, x_0_1), z) <= True
      r_natlist_x_nat(nil, s(x_1_0)) <= True
      r_natlist_x_nat(nil, z) <= True
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_odd_isnat" =
  ModelShocs.pp Format.std_formatter Database_for_tests.model_length_and_odd_and_nat__shocs;
  [%expect
    "
    |_

    even ->
    {
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
    }
    ;
    odd ->
    {
      odd(s(x_0_0)) <= even(x_0_0)
    }
    ;
    r_l_n ->
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
    }
    ;
    r_n ->
    {
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }
    ;
    r_natlist_x_nat ->
    {
      r_natlist_x_nat(cons(x_0_0, x_0_1), s(x_1_0)) <= True
      r_natlist_x_nat(cons(x_0_0, x_0_1), z) <= True
      r_natlist_x_nat(nil, s(x_1_0)) <= True
      r_natlist_x_nat(nil, z) <= True
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "clauses length" =
  Clause.Aliases.Set_clause_patterns.pp Format.std_formatter
    Clause.Database_for_tests.chc_system_for_length;
  [%expect
    "
    {
    length(nil, z) <= True
    length(cons(x, l), s(n)) <= length(l, n)
    eq_nat(n1, n2) <= length(l, n1) /\\ length(l, n2)
    length(l, n) <= length(cons(x, l), s(n))
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
