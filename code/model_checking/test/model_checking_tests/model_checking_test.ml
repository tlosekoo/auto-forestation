open Model_checking
open Database_for_tests
open Clause.Aliases
open Instantiation

(* The correct model for length is OK *)
let%test "model length works" =
  let clauses = Clause.Database_for_tests.chc_system_for_length |> Set_clause_patterns.to_list in
  let solver_output =
    clauses
    |> List.map
         (ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
            model_length__shocs)
  in
  (* let () =
       Format.fprintf Format.std_formatter "Clauses:\n%a\n\n"
         (Printing.pp_list_and_brackets_sep "\n" Clause_patterns.pp)
         clauses
     in
     let () =
       Format.fprintf Format.std_formatter "Outputs:\n%a\n"
         (Printing.pp_list_and_brackets (Search_outcome.pp Term.Substitution.pp))
         solver_output
     in *)
  List.for_all Search_outcome.is_no solver_output

(* The model for length that only recognize lists of zeros and their size does not satisfy everything *)
(* let%test "model length_z 1" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_z_n__shocs chc_length_nil_z
     in
     Search_outcome.is_no solver_output

   let%test "model length_z 2" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_z_n__shocs chc_length_l_n_implies_length_consxl_sn
     in
     Search_outcome.is_yes solver_output

   let%test "model length_z 3" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_z_n__shocs chc_length_consxl_s_implies_length_l_n
     in
     Search_outcome.is_no solver_output

   let%test "model length_z 4" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_z_n__shocs chc_length_l_n1_and_length_l_n2_implies_eq_n1_n2
     in
     Search_outcome.is_no solver_output

   (* The model for length that recognizes everything does not satisfy everything *)
   let%test "model length_all 1" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_all__shocs chc_length_nil_z
     in
     Search_outcome.is_no solver_output

   let%test "model length_all 2" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_all__shocs chc_length_l_n_implies_length_consxl_sn
     in
     Search_outcome.is_no solver_output

   let%test "model length_all 3" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_all__shocs chc_length_consxl_s_implies_length_l_n
     in
     Search_outcome.is_no solver_output

   let%test "model length_all 4" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula ~time_to_stop:None ~_debug_mode:false
         model_length_all__shocs chc_length_l_n1_and_length_l_n2_implies_eq_n1_n2
     in
     Search_outcome.is_yes solver_output

   (* TIMEOUT *)
   let%test "model length_all timeout" =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula
         ~time_to_stop:(Some (Unix.gettimeofday () +. 0.0005))
         ~_debug_mode:false model_length_all__shocs chc_length_l_n1_and_length_l_n2_implies_eq_n1_n2
     in
     Search_outcome.is_yes solver_output

   let () = Printing.pp_ok () *)

let model_height_shallower_le =
  ModelShocs.create Term.Database_for_tests.testing_type_env
    (Term.Aliases.Set_typed_relation_symbol.of_list
       [
         Term.Database_for_tests.relation_height;
         Term.Database_for_tests.relation_shallower;
         Term.Database_for_tests.relation_le;
       ])
    Tree_tuple_formalisms.Database_for_tests_shocs.shocs_height_and_shallower_and_le

let%expect_test "model height shallower le" =
  ModelShocs.pp Format.std_formatter model_height_shallower_le;
  [%expect
    "
    |_

    height ->
    {
      height(leaf, z) <= True
      height(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= height(x_0_0, x_1_0) /\\ shallower(x_0_2, x_1_0)
      height(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= height(x_0_2, x_1_0) /\\ shallower(x_0_0, x_1_0)
    }
    ;
    le ->
    {
      le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
      le(z, s(x_1_0)) <= True
    }
    ;
    shallower ->
    {
      shallower(leaf, s(x_1_0)) <= True
      shallower(leaf, z) <= True
      shallower(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= shallower(x_0_0, x_1_0) /\\ shallower(x_0_2, x_1_0)
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "property to check" =
  Clause.Disjunctive_clause_patterns.pp Format.std_formatter
    Clause.Database_for_tests.clause_height_property;
  [%expect "eq_nat(n1, n2) <= height(t1, n1) /\\ height(t1, n2)"]

let%expect_test "model checking height property" =
  let solver_output =
    ModelCheckingShocs.model_check_one_formula
      ~time_to_stop:(Some (Unix.gettimeofday () +. 5.))
      ~_debug_mode:false model_height_shallower_le Clause.Database_for_tests.clause_height_property
  in
  Search_outcome.pp Term.Substitution.pp Format.std_formatter solver_output;
  [%expect "
    No: ()"]

let%expect_test "height recursive definition 1" =
  Clause.Disjunctive_clause_patterns.pp Format.std_formatter
    Clause.Database_for_tests.clause_height_recursive_1;
  [%expect
    "height(node(t1, e, t2), s(n2)) \\/ le(n1, n3) <= height(t1, n1) /\\ height(t1, n2) /\\ height(t2, n3)"]

let%expect_test "model checking height recursive definition 1 " =
  let solver_output =
    ModelCheckingShocs.model_check_one_formula
      ~time_to_stop:(Some (Unix.gettimeofday () +. 5.))
      ~_debug_mode:false model_height_shallower_le
      Clause.Database_for_tests.clause_height_recursive_1
  in
  Search_outcome.pp Term.Substitution.pp Format.std_formatter solver_output;
  [%expect "
    Maybe: out of time"]

(* let%expect_test "height recursive definition 2" =
     Clause.Disjunctive_clause_patterns.pp Format.std_formatter
       Clause.Database_for_tests.clause_height_recursive_2;
     [%expect
       "height(node(t1, e, t2), s(n2)) <= height(t1, n1) /\\ height(t2, n2) /\\ height(t2, n3) /\\ le(n1, n3)"]

   let%expect_test "model checking height recursive definition 2 " =
     let solver_output =
       ModelCheckingShocs.model_check_one_formula
         ~time_to_stop:(Some (Unix.gettimeofday () +. 15.))
         ~_debug_mode:false model_height_shallower_le
         Clause.Database_for_tests.clause_height_recursive_2
     in
     Search_outcome.pp Term.Substitution.pp Format.std_formatter solver_output;
     [%expect "
       Maybe: out of time"] *)

(* TODO: solve this timeout *)
