open Model_checking

let%expect_test "universe generation" =
  let inductive_clause = Clause.Database_for_tests.chc_length_l_n_implies_length_consxl_sn in
  let corresponding_universe =
    TypingProblem_generation.generate_typingProblem_for_one_clause inductive_clause
  in
  Format.fprintf Format.std_formatter "Clause:\n%a\n\nCorresponding typing problem:\n%a\n"
    Clause.Disjunctive_clause_patterns.pp inductive_clause Typing_problem.pp corresponding_universe;
  [%expect
    "
  Clause:
  length(cons(x, l), s(n)) <= length(l, n)

  Corresponding typing problem:

  [[
  length(l, n)
  not length(cons(x, l), s(n))
  ]]"]
