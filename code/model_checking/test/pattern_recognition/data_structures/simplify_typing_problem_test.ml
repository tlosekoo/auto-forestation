open Model_checking
open Term
open Clause

(* Some datatype *)
let some_type = Datatype.create "d"

(* Relation symbols *)
let symbol_r = Symbol.create "r"
let symbol_p = Symbol.create "p"
let symbol_q = Symbol.create "q"
let symbol_s = Symbol.create "s"
let unary_r = Typed_relation_symbol.create symbol_r [some_type]
let binary_r = Typed_relation_symbol.create symbol_r [some_type; some_type]
let unary_p = Typed_relation_symbol.create symbol_p [some_type]
let unary_q = Typed_relation_symbol.create symbol_q [some_type]
let unary_s = Typed_relation_symbol.create symbol_s [some_type]

(* Variables *)
let x = Typed_symbol.create (Symbol.create "x") some_type
let y = Typed_symbol.create (Symbol.create "y") some_type
let z = Typed_symbol.create (Symbol.create "z") some_type
let a = Typed_symbol.create (Symbol.create "a") some_type
let b = Typed_symbol.create (Symbol.create "b") some_type

(* Patterns *)
let px = Pattern.from_var x
let py = Pattern.from_var y
let pz = Pattern.from_var z
let pa = Pattern.from_var a
let pb = Pattern.from_var b

(* Atoms *)
let atom_r_x = Atom_patterns.create_from_predicate_symbol unary_r [px]
let atom_r_y = Atom_patterns.create_from_predicate_symbol unary_r [py]
let atom_r_x_x = Atom_patterns.create_from_predicate_symbol binary_r [px; px]
let atom_r_y_y = Atom_patterns.create_from_predicate_symbol binary_r [py; py]
let atom_r_x_y = Atom_patterns.create_from_predicate_symbol binary_r [px; py]
let atom_r_y_x = Atom_patterns.create_from_predicate_symbol binary_r [py; px]
let atom_q_x = Atom_patterns.create_from_predicate_symbol unary_q [px]
let atom_p_y = Atom_patterns.create_from_predicate_symbol unary_p [py]
let atom_p_x = Atom_patterns.create_from_predicate_symbol unary_p [px]
let atom_p_a = Atom_patterns.create_from_predicate_symbol unary_p [pa]
let atom_q_b = Atom_patterns.create_from_predicate_symbol unary_q [pb]
let atom_r_y_z = Atom_patterns.create_from_predicate_symbol binary_r [py; pz]
let atom_r_z_a = Atom_patterns.create_from_predicate_symbol binary_r [pz; pa]
let atom_r_x_a = Atom_patterns.create_from_predicate_symbol binary_r [px; pa]
let atom_r_y_b = Atom_patterns.create_from_predicate_symbol binary_r [py; pb]
let atom_r_x_z = Atom_patterns.create_from_predicate_symbol binary_r [px; pz]
let atom_r_a_y = Atom_patterns.create_from_predicate_symbol binary_r [pa; py]
let atom_r_a_z = Atom_patterns.create_from_predicate_symbol binary_r [pa; pz]

(* litterals *)
let r_x = Litteral.create ~is_positive:true atom_r_x
let r_y = Litteral.create ~is_positive:true atom_r_y
let r_x_x = Litteral.create ~is_positive:true atom_r_x_x
let r_y_y = Litteral.create ~is_positive:true atom_r_y_y
let r_x_y = Litteral.create ~is_positive:true atom_r_x_y
let r_y_x = Litteral.create ~is_positive:true atom_r_y_x
let q_x = Litteral.create ~is_positive:true atom_q_x
let p_y = Litteral.create ~is_positive:true atom_p_y
let p_x = Litteral.create ~is_positive:true atom_p_x
let p_a = Litteral.create ~is_positive:true atom_p_a
let q_b = Litteral.create ~is_positive:true atom_q_b
let r_y_z = Litteral.create ~is_positive:true atom_r_y_z
let r_z_a = Litteral.create ~is_positive:true atom_r_z_a
let r_x_a = Litteral.create ~is_positive:true atom_r_x_a
let r_y_b = Litteral.create ~is_positive:true atom_r_y_b
let r_x_z = Litteral.create ~is_positive:true atom_r_x_z
let r_a_y = Litteral.create ~is_positive:true atom_r_a_y
let r_a_z = Litteral.create ~is_positive:true atom_r_a_z

(* Instances *)
let problem_1 = Typing_problem.of_list [r_x; r_y]
let problem_2 = Typing_problem.of_list [r_x_x; r_y_y]
let problem_3 = Typing_problem.of_list [r_x_y; r_y_x]
let problem_4 = Typing_problem.of_list [r_x; r_y; q_x; p_y]
let problem_5 = Typing_problem.of_list [r_x; r_y; q_x; p_y; p_x]
let problem_6 = Typing_problem.of_list [r_x_y; r_x_x]
let problem_7 = Typing_problem.of_list [r_x_y; r_y_z]
let problem_8 = Typing_problem.of_list [r_x_y; r_y_x; r_a_z; r_z_a]
let problem_9 = Typing_problem.of_list [r_x_a; r_y_b; p_a; q_b]
let problem_10 = Typing_problem.of_list [r_x_y; r_x_z; r_a_y; r_a_z]

(* problems printing *)
let%expect_test "problem 1" =
  Typing_problem.pp Format.std_formatter problem_1;
  [%expect "
    [[
    r(x)
    r(y)
    ]]"]

let%expect_test "problem 2" =
  Typing_problem.pp Format.std_formatter problem_2;
  [%expect "
    [[
    r(x, x)
    r(y, y)
    ]]"]

let%expect_test "problem 3" =
  Typing_problem.pp Format.std_formatter problem_3;
  [%expect "
    [[
    r(x, y)
    r(y, x)
    ]]"]

let%expect_test "problem 4" =
  Typing_problem.pp Format.std_formatter problem_4;
  [%expect "
    [[
    p(y)
    q(x)
    r(x)
    r(y)
    ]]"]

let%expect_test "problem 5" =
  Typing_problem.pp Format.std_formatter problem_5;
  [%expect "
    [[
    p(x)
    p(y)
    q(x)
    r(x)
    r(y)
    ]]"]

let%expect_test "problem 6" =
  Typing_problem.pp Format.std_formatter problem_6;
  [%expect "
    [[
    r(x, x)
    r(x, y)
    ]]"]

let%expect_test "problem 7" =
  Typing_problem.pp Format.std_formatter problem_7;
  [%expect "
    [[
    r(x, y)
    r(y, z)
    ]]"]

let%expect_test "problem 8" =
  Typing_problem.pp Format.std_formatter problem_8;
  [%expect "
    [[
    r(a, z)
    r(x, y)
    r(y, x)
    r(z, a)
    ]]"]

let%expect_test "problem 9" =
  Typing_problem.pp Format.std_formatter problem_9;
  [%expect "
    [[
    p(a)
    q(b)
    r(x, a)
    r(y, b)
    ]]"]

let%expect_test "problem 10" =
  Typing_problem.pp Format.std_formatter problem_10;
  [%expect "
    [[
    r(a, y)
    r(a, z)
    r(x, y)
    r(x, z)
    ]]"]

(* Variables positions *)
let%expect_test "positions problem 1" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_1 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect "
    {
    x -> {((r, true), [0])}  ;  y -> {((r, true), [0])}
    }"]

let%expect_test "positions problem 2" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_2 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    x -> {((r, true), [0]), ((r, true), [1])}  ;  y -> {((r, true), [0]), ((r, true), [1])}
    }"]

let%expect_test "positions problem 3" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_3 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    x -> {((r, true), [0]), ((r, true), [1])}  ;  y -> {((r, true), [0]), ((r, true), [1])}
    }"]

let%expect_test "positions problem 4" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_4 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    x -> {((q, true), [0]), ((r, true), [0])}  ;  y -> {((p, true), [0]), ((r, true), [0])}
    }"]

let%expect_test "positions problem 5" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_5 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    x -> {((p, true), [0]), ((q, true), [0]), ((r, true), [0])}  ;  y -> {((p, true), [0]), ((r, true), [0])}
    }"]

let%expect_test "positions problem 6" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_6 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect "
    {
    x -> {((r, true), [0]), ((r, true), [1])}  ;  y -> {((r, true), [1])}
    }"]

let%expect_test "positions problem 7" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_7 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    x -> {((r, true), [0])}  ;  y -> {((r, true), [0]), ((r, true), [1])}  ;  z -> {((r, true), [1])}
    }"]

let%expect_test "positions problem 8" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_8 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    a -> {((r, true), [0]), ((r, true), [1])}  ;  x -> {((r, true), [0]), ((r, true), [1])}  ;  y -> {((r, true), [0]), ((r, true), [1])}  ;  z -> {((r, true), [0]), ((r, true), [1])}
    }"]

let%expect_test "positions problem 9" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_9 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    a -> {((p, true), [0]), ((r, true), [1])}  ;  b -> {((q, true), [0]), ((r, true), [1])}  ;  x -> {((r, true), [0])}  ;  y -> {((r, true), [0])}
    }"]

let%expect_test "positions problem 10" =
  let positions = Simplify_variable_position_computation.compute_variable_positions problem_10 in
  Map_variable_setRelationPlace.pp Format.std_formatter positions;
  [%expect
    "
    {
    a -> {((r, true), [0])}  ;  x -> {((r, true), [0])}  ;  y -> {((r, true), [1])}  ;  z -> {((r, true), [1])}
    }"]

(* All valid substitutions *)

(* Everything *)

let present_total_resolution (problem : Typing_problem.t) : unit =
  let variables_positions =
    Simplify_variable_position_computation.compute_variable_positions problem
  in
  let variables_subset_compatibility =
    Simplify_variable_position_computation.compute_variable_compatibility_with_positions
      variables_positions
  in
  let simplification_one_variable_by_one_variable, projected_variables_subset_compatibility =
    Simplify_typing_problem_one_variable.simplify_typing_problem_one_variable_by_one_variable
      problem variables_subset_compatibility
  in
  let simplified_tp_by_unitary_renaming =
    Typing_problem.apply_renaming
      (Term.Aliases.Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity
         simplification_one_variable_by_one_variable)
      problem
  in

  let renamings_that_are_position_compatible =
    Simplify_typing_problem.compute_all_possible_renaming_that_is_compatible_with_positions
      projected_variables_subset_compatibility
  in
  let all_valid_renamings_on_simplified_tp =
    Set_renamings.filter
      (Typing_problem_manipulation.does_renaming_yields_subproblem simplified_tp_by_unitary_renaming)
      renamings_that_are_position_compatible
  in
  let _all_valid_composed_renamings =
    Set_renamings.map
      (Term.Aliases.Map_typedSymbol_typedSymbol.auto_compose_considering_undefined_is_identity
         simplification_one_variable_by_one_variable)
      all_valid_renamings_on_simplified_tp
  in
  let one_minimal_simplification = Simplify_typing_problem.find_a_smallest_simplification problem in
  let minimized_problem =
    Typing_problem.apply_renaming
      (Term.Aliases.Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity
         one_minimal_simplification)
      problem
  in

  let pp_variables_subset_compatibility =
    Map_variable_setVariable.pp_param ~opening:"{\n" ~closing:"\n}" ~sep:"\n"
      ~pp_key:Term.Typed_symbol.pp ~sep_key_value:" -> " ~pp_value:Term.Aliases.Set_typed_symbol.pp
  in
  let pp_all_valid_simplifications =
    Set_renamings.pp_param ~opening:"{\n" ~closing:"\n}" ~sep:"\n"
      ~pp_element:Term.Aliases.Map_typedSymbol_typedSymbol.pp
  in

  Format.fprintf Format.std_formatter
    "Problem:\n%a\n\nVariable subset compatibility:\n%a\n\nSimplification coming from one variable by one variable:\n%a\n\nSemi simplified typing problem:\n%a\n\n
    Position-valid simplifications:\n%a\n\nAll valid simplification on semi-simplified typing problem:\n%a\n\nA smallest simplification:\n%a\n\nSimplified problem:\n%a\n\n"
    Typing_problem.pp problem pp_variables_subset_compatibility variables_subset_compatibility
    Term.Aliases.Map_typedSymbol_typedSymbol.pp simplification_one_variable_by_one_variable
    Typing_problem.pp simplified_tp_by_unitary_renaming pp_all_valid_simplifications
    renamings_that_are_position_compatible pp_all_valid_simplifications
    all_valid_renamings_on_simplified_tp Term.Aliases.Map_typedSymbol_typedSymbol.pp
    one_minimal_simplification Typing_problem.pp minimized_problem

let%expect_test "positions problem 1" =
  present_total_resolution problem_1;

  [%expect
    "
    Problem:

    [[
    r(x)
    r(y)
    ]]


    Variable subset compatibility:
    {

    x -> {x, y}
    y -> {x, y}

    }

    Simplification coming from one variable by one variable:
    {
    x -> y
    }

    Semi simplified typing problem:

    [[
    r(y)
    ]]



        Position-valid simplifications:
    {
    {
    y -> y
    }
    }

    All valid simplification on semi-simplified typing problem:
    {
    {
    y -> y
    }
    }

    A smallest simplification:
    {
    x -> y  ;  y -> y
    }

    Simplified problem:

    [[
    r(y)
    ]]"]

let%expect_test "positions problem 2" =
  present_total_resolution problem_2;

  [%expect
    "
       Problem:

       [[
       r(x, x)
       r(y, y)
       ]]


       Variable subset compatibility:
       {

       x -> {x, y}
       y -> {x, y}

       }

       Simplification coming from one variable by one variable:
       {
       x -> y
       }

       Semi simplified typing problem:

       [[
       r(y, y)
       ]]



           Position-valid simplifications:
       {
       {
       y -> y
       }
       }

       All valid simplification on semi-simplified typing problem:
       {
       {
       y -> y
       }
       }

       A smallest simplification:
       {
       x -> y  ;  y -> y
       }

       Simplified problem:

       [[
       r(y, y)
       ]]
   "]

let%expect_test "positions problem 3" =
  present_total_resolution problem_3;

  [%expect
    "
       Problem:

       [[
       r(x, y)
       r(y, x)
       ]]


       Variable subset compatibility:
       {

       x -> {x, y}
       y -> {x, y}

       }

       Simplification coming from one variable by one variable:
       {

       }

       Semi simplified typing problem:

       [[
       r(x, y)
       r(y, x)
       ]]



           Position-valid simplifications:
       {
       {
       x -> x  ;  y -> x
       }
       {
       x -> x  ;  y -> y
       }
       {
       x -> y  ;  y -> y
       }
       }

       All valid simplification on semi-simplified typing problem:
       {
       {
       x -> x  ;  y -> y
       }
       }

       A smallest simplification:
       {
       x -> x  ;  y -> y
       }

       Simplified problem:

       [[
       r(x, y)
       r(y, x)
       ]]
   "]

let%expect_test "positions problem 4" =
  present_total_resolution problem_4;

  [%expect
    "
       Problem:

       [[
       p(y)
       q(x)
       r(x)
       r(y)
       ]]


       Variable subset compatibility:
       {

       x -> {x}
       y -> {y}

       }

       Simplification coming from one variable by one variable:
       {

       }

       Semi simplified typing problem:

       [[
       p(y)
       q(x)
       r(x)
       r(y)
       ]]



           Position-valid simplifications:
       {
       {
       x -> x  ;  y -> y
       }
       }

       All valid simplification on semi-simplified typing problem:
       {
       {
       x -> x  ;  y -> y
       }
       }

       A smallest simplification:
       {
       x -> x  ;  y -> y
       }

       Simplified problem:

       [[
       p(y)
       q(x)
       r(x)
       r(y)
       ]]
   "]

let%expect_test "positions problem 5" =
  present_total_resolution problem_5;

  [%expect
    "
       Problem:

       [[
       p(x)
       p(y)
       q(x)
       r(x)
       r(y)
       ]]


       Variable subset compatibility:
       {

       x -> {x}
       y -> {x, y}

       }

       Simplification coming from one variable by one variable:
       {
       y -> x
       }

       Semi simplified typing problem:

       [[
       p(x)
       q(x)
       r(x)
       ]]



           Position-valid simplifications:
       {
       {
       x -> x
       }
       }

       All valid simplification on semi-simplified typing problem:
       {
       {
       x -> x
       }
       }

       A smallest simplification:
       {
       x -> x  ;  y -> x
       }

       Simplified problem:

       [[
       p(x)
       q(x)
       r(x)
       ]]
   "]

let%expect_test "positions problem 6" =
  present_total_resolution problem_6;

  [%expect
    "
         Problem:

         [[
         r(x, x)
         r(x, y)
         ]]


         Variable subset compatibility:
         {

         x -> {x}
         y -> {x, y}

         }

         Simplification coming from one variable by one variable:
         {
         y -> x
         }

         Semi simplified typing problem:

         [[
         r(x, x)
         ]]



             Position-valid simplifications:
         {
         {
         x -> x
         }
         }

         All valid simplification on semi-simplified typing problem:
         {
         {
         x -> x
         }
         }

         A smallest simplification:
         {
         x -> x  ;  y -> x
         }

         Simplified problem:

         [[
         r(x, x)
         ]]
   "]

let%expect_test "positions problem 7" =
  present_total_resolution problem_7;

  [%expect
    "
         Problem:

         [[
         r(x, y)
         r(y, z)
         ]]


         Variable subset compatibility:
         {

         x -> {x, y}
         y -> {y}
         z -> {y, z}

         }

         Simplification coming from one variable by one variable:
         {

         }

         Semi simplified typing problem:

         [[
         r(x, y)
         r(y, z)
         ]]



             Position-valid simplifications:
         {
         {
         x -> x  ;  y -> y  ;  z -> y
         }
         {
         x -> x  ;  y -> y  ;  z -> z
         }
         {
         x -> y  ;  y -> y  ;  z -> y
         }
         {
         x -> y  ;  y -> y  ;  z -> z
         }
         }

         All valid simplification on semi-simplified typing problem:
         {
         {
         x -> x  ;  y -> y  ;  z -> z
         }
         }

         A smallest simplification:
         {
         x -> x  ;  y -> y  ;  z -> z
         }

         Simplified problem:

         [[
         r(x, y)
         r(y, z)
         ]]
   "]

let%expect_test "positions problem 8" =
  present_total_resolution problem_8;

  [%expect
    "
         Problem:

         [[
         r(a, z)
         r(x, y)
         r(y, x)
         r(z, a)
         ]]


         Variable subset compatibility:
         {

         a -> {a, x, y, z}
         x -> {a, x, y, z}
         y -> {a, x, y, z}
         z -> {a, x, y, z}

         }

         Simplification coming from one variable by one variable:
         {

         }

         Semi simplified typing problem:

         [[
         r(a, z)
         r(x, y)
         r(y, x)
         r(z, a)
         ]]



             Position-valid simplifications:
         {
         {
         a -> a  ;  x -> a  ;  y -> a  ;  z -> a
         }
         {
         a -> a  ;  x -> a  ;  y -> a  ;  z -> z
         }
         {
         a -> a  ;  x -> a  ;  y -> y  ;  z -> a
         }
         {
         a -> a  ;  x -> a  ;  y -> y  ;  z -> y
         }
         {
         a -> a  ;  x -> a  ;  y -> y  ;  z -> z
         }
         {
         a -> a  ;  x -> a  ;  y -> z  ;  z -> a
         }
         {
         a -> a  ;  x -> a  ;  y -> z  ;  z -> z
         }
         {
         a -> a  ;  x -> x  ;  y -> a  ;  z -> a
         }
         {
         a -> a  ;  x -> x  ;  y -> a  ;  z -> x
         }
         {
         a -> a  ;  x -> x  ;  y -> a  ;  z -> z
         }
         {
         a -> a  ;  x -> x  ;  y -> x  ;  z -> a
         }
         {
         a -> a  ;  x -> x  ;  y -> x  ;  z -> x
         }
         {
         a -> a  ;  x -> x  ;  y -> x  ;  z -> z
         }
         {
         a -> a  ;  x -> x  ;  y -> y  ;  z -> a
         }
         {
         a -> a  ;  x -> x  ;  y -> y  ;  z -> x
         }
         {
         a -> a  ;  x -> x  ;  y -> y  ;  z -> y
         }
         {
         a -> a  ;  x -> x  ;  y -> y  ;  z -> z
         }
         {
         a -> a  ;  x -> x  ;  y -> z  ;  z -> a
         }
         {
         a -> a  ;  x -> x  ;  y -> z  ;  z -> x
         }
         {
         a -> a  ;  x -> x  ;  y -> z  ;  z -> z
         }
         {
         a -> a  ;  x -> y  ;  y -> a  ;  z -> a
         }
         {
         a -> a  ;  x -> y  ;  y -> a  ;  z -> y
         }
         {
         a -> a  ;  x -> y  ;  y -> a  ;  z -> z
         }
         {
         a -> a  ;  x -> y  ;  y -> y  ;  z -> a
         }
         {
         a -> a  ;  x -> y  ;  y -> y  ;  z -> y
         }
         {
         a -> a  ;  x -> y  ;  y -> y  ;  z -> z
         }
         {
         a -> a  ;  x -> y  ;  y -> z  ;  z -> a
         }
         {
         a -> a  ;  x -> y  ;  y -> z  ;  z -> y
         }
         {
         a -> a  ;  x -> y  ;  y -> z  ;  z -> z
         }
         {
         a -> a  ;  x -> z  ;  y -> a  ;  z -> a
         }
         {
         a -> a  ;  x -> z  ;  y -> a  ;  z -> z
         }
         {
         a -> a  ;  x -> z  ;  y -> y  ;  z -> a
         }
         {
         a -> a  ;  x -> z  ;  y -> y  ;  z -> y
         }
         {
         a -> a  ;  x -> z  ;  y -> y  ;  z -> z
         }
         {
         a -> a  ;  x -> z  ;  y -> z  ;  z -> a
         }
         {
         a -> a  ;  x -> z  ;  y -> z  ;  z -> z
         }
         {
         a -> x  ;  x -> x  ;  y -> x  ;  z -> x
         }
         {
         a -> x  ;  x -> x  ;  y -> x  ;  z -> z
         }
         {
         a -> x  ;  x -> x  ;  y -> y  ;  z -> x
         }
         {
         a -> x  ;  x -> x  ;  y -> y  ;  z -> y
         }
         {
         a -> x  ;  x -> x  ;  y -> y  ;  z -> z
         }
         {
         a -> x  ;  x -> x  ;  y -> z  ;  z -> x
         }
         {
         a -> x  ;  x -> x  ;  y -> z  ;  z -> z
         }
         {
         a -> x  ;  x -> y  ;  y -> x  ;  z -> x
         }
         {
         a -> x  ;  x -> y  ;  y -> x  ;  z -> y
         }
         {
         a -> x  ;  x -> y  ;  y -> x  ;  z -> z
         }
         {
         a -> x  ;  x -> y  ;  y -> y  ;  z -> x
         }
         {
         a -> x  ;  x -> y  ;  y -> y  ;  z -> y
         }
         {
         a -> x  ;  x -> y  ;  y -> y  ;  z -> z
         }
         {
         a -> x  ;  x -> y  ;  y -> z  ;  z -> x
         }
         {
         a -> x  ;  x -> y  ;  y -> z  ;  z -> y
         }
         {
         a -> x  ;  x -> y  ;  y -> z  ;  z -> z
         }
         {
         a -> x  ;  x -> z  ;  y -> x  ;  z -> x
         }
         {
         a -> x  ;  x -> z  ;  y -> x  ;  z -> z
         }
         {
         a -> x  ;  x -> z  ;  y -> y  ;  z -> x
         }
         {
         a -> x  ;  x -> z  ;  y -> y  ;  z -> y
         }
         {
         a -> x  ;  x -> z  ;  y -> y  ;  z -> z
         }
         {
         a -> x  ;  x -> z  ;  y -> z  ;  z -> x
         }
         {
         a -> x  ;  x -> z  ;  y -> z  ;  z -> z
         }
         {
         a -> y  ;  x -> x  ;  y -> x  ;  z -> x
         }
         {
         a -> y  ;  x -> x  ;  y -> x  ;  z -> y
         }
         {
         a -> y  ;  x -> x  ;  y -> x  ;  z -> z
         }
         {
         a -> y  ;  x -> x  ;  y -> y  ;  z -> x
         }
         {
         a -> y  ;  x -> x  ;  y -> y  ;  z -> y
         }
         {
         a -> y  ;  x -> x  ;  y -> y  ;  z -> z
         }
         {
         a -> y  ;  x -> x  ;  y -> z  ;  z -> x
         }
         {
         a -> y  ;  x -> x  ;  y -> z  ;  z -> y
         }
         {
         a -> y  ;  x -> x  ;  y -> z  ;  z -> z
         }
         {
         a -> y  ;  x -> y  ;  y -> y  ;  z -> y
         }
         {
         a -> y  ;  x -> y  ;  y -> y  ;  z -> z
         }
         {
         a -> y  ;  x -> y  ;  y -> z  ;  z -> y
         }
         {
         a -> y  ;  x -> y  ;  y -> z  ;  z -> z
         }
         {
         a -> y  ;  x -> z  ;  y -> y  ;  z -> y
         }
         {
         a -> y  ;  x -> z  ;  y -> y  ;  z -> z
         }
         {
         a -> y  ;  x -> z  ;  y -> z  ;  z -> y
         }
         {
         a -> y  ;  x -> z  ;  y -> z  ;  z -> z
         }
         {
         a -> z  ;  x -> x  ;  y -> x  ;  z -> x
         }
         {
         a -> z  ;  x -> x  ;  y -> x  ;  z -> z
         }
         {
         a -> z  ;  x -> x  ;  y -> y  ;  z -> x
         }
         {
         a -> z  ;  x -> x  ;  y -> y  ;  z -> y
         }
         {
         a -> z  ;  x -> x  ;  y -> y  ;  z -> z
         }
         {
         a -> z  ;  x -> x  ;  y -> z  ;  z -> x
         }
         {
         a -> z  ;  x -> x  ;  y -> z  ;  z -> z
         }
         {
         a -> z  ;  x -> y  ;  y -> y  ;  z -> y
         }
         {
         a -> z  ;  x -> y  ;  y -> y  ;  z -> z
         }
         {
         a -> z  ;  x -> y  ;  y -> z  ;  z -> y
         }
         {
         a -> z  ;  x -> y  ;  y -> z  ;  z -> z
         }
         {
         a -> z  ;  x -> z  ;  y -> y  ;  z -> y
         }
         {
         a -> z  ;  x -> z  ;  y -> y  ;  z -> z
         }
         {
         a -> z  ;  x -> z  ;  y -> z  ;  z -> z
         }
         }

         All valid simplification on semi-simplified typing problem:
         {
         {
         a -> a  ;  x -> a  ;  y -> z  ;  z -> z
         }
         {
         a -> a  ;  x -> x  ;  y -> y  ;  z -> z
         }
         {
         a -> a  ;  x -> z  ;  y -> a  ;  z -> z
         }
         {
         a -> x  ;  x -> x  ;  y -> y  ;  z -> y
         }
         {
         a -> x  ;  x -> y  ;  y -> x  ;  z -> y
         }
         {
         a -> y  ;  x -> x  ;  y -> y  ;  z -> x
         }
         }

         A smallest simplification:
         {
         a -> a  ;  x -> a  ;  y -> z  ;  z -> z
         }

         Simplified problem:

         [[
         r(a, z)
         r(z, a)
         ]]
   "]

let%expect_test "positions problem 9" =
  present_total_resolution problem_9;

  [%expect
    "
         Problem:

         [[
         p(a)
         q(b)
         r(x, a)
         r(y, b)
         ]]


         Variable subset compatibility:
         {

         a -> {a}
         b -> {b}
         x -> {x, y}
         y -> {x, y}

         }

         Simplification coming from one variable by one variable:
         {

         }

         Semi simplified typing problem:

         [[
         p(a)
         q(b)
         r(x, a)
         r(y, b)
         ]]



             Position-valid simplifications:
         {
         {
         a -> a  ;  b -> b  ;  x -> x  ;  y -> x
         }
         {
         a -> a  ;  b -> b  ;  x -> x  ;  y -> y
         }
         {
         a -> a  ;  b -> b  ;  x -> y  ;  y -> y
         }
         }

         All valid simplification on semi-simplified typing problem:
         {
         {
         a -> a  ;  b -> b  ;  x -> x  ;  y -> y
         }
         }

         A smallest simplification:
         {
         a -> a  ;  b -> b  ;  x -> x  ;  y -> y
         }

         Simplified problem:

         [[
         p(a)
         q(b)
         r(x, a)
         r(y, b)
         ]]
   "]

let%expect_test "positions problem 10" =
  present_total_resolution problem_10;

  [%expect
    "
         Problem:

         [[
         r(a, y)
         r(a, z)
         r(x, y)
         r(x, z)
         ]]


         Variable subset compatibility:
         {

         a -> {a, x}
         x -> {a, x}
         y -> {y, z}
         z -> {y, z}

         }

         Simplification coming from one variable by one variable:
         {
         a -> x  ;  y -> z
         }

         Semi simplified typing problem:

         [[
         r(x, z)
         ]]



             Position-valid simplifications:
         {
         {
         x -> x  ;  z -> z
         }
         }

         All valid simplification on semi-simplified typing problem:
         {
         {
         x -> x  ;  z -> z
         }
         }

         A smallest simplification:
         {
         a -> x  ;  x -> x  ;  y -> z  ;  z -> z
         }

         Simplified problem:

         [[
         r(x, z)
         ]]
   "]

open Term.Database_for_tests

let p_t0 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "t0") Term.Database_for_tests.elt_tree)

let p_n0 =
  Term.Pattern.from_var
    (Term.Typed_symbol.create (Term.Symbol.create "n0") Term.Database_for_tests.nat)

let atom_height_t0_n0 = Atom_patterns.create_from_predicate_symbol relation_height [p_t0; p_n0]
let atom_height_t1_n1 = Atom_patterns.create_from_predicate_symbol relation_height [p_t1; p_n1]
let atom_height_t2_n2 = Atom_patterns.create_from_predicate_symbol relation_height [p_t2; p_n2]
let atom_le_n1_n2 = Atom_patterns.create_from_predicate_symbol relation_le [p_n1; p_n2]

let atom_shallower_t0_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t0; p_n1]

let atom_shallower_t3_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t3; p_n0]

let atom_shallower_t3_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t3; p_n1]

let atom_shallower_t1_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t1; p_n0]

let atom_shallower_t4_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n0]

let atom_shallower_t4_n1 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t4; p_n1]

let atom_shallower_t5_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t5; p_n2]

let atom_shallower_t6_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t6; p_n2]

let atom_shallower_t7_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t7; p_n2]

let atom_shallower_t8_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t8; p_n2]

let atom_shallower_t9_n0 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t9; p_n0]

let atom_shallower_t9_n2 =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_t9; p_n2]

let litteral_height_t0_n0 = Litteral.create ~is_positive:true atom_height_t0_n0
let litteral_height_t1_n1 = Litteral.create ~is_positive:true atom_height_t1_n1
let litteral_height_t2_n2 = Litteral.create ~is_positive:true atom_height_t2_n2
let litteral_le_n1_n2 = Litteral.create ~is_positive:false atom_le_n1_n2
let litteral_shallower_t0_n1 = Litteral.create ~is_positive:false atom_shallower_t0_n1
let litteral_shallower_t3_n0 = Litteral.create ~is_positive:true atom_shallower_t3_n0
let litteral_shallower_t3_n1 = Litteral.create ~is_positive:true atom_shallower_t3_n1
let litteral_shallower_t4_n0 = Litteral.create ~is_positive:true atom_shallower_t4_n0
let litteral_shallower_t4_n1 = Litteral.create ~is_positive:true atom_shallower_t4_n1
let litteral_shallower_t5_n2 = Litteral.create ~is_positive:true atom_shallower_t5_n2
let litteral_shallower_t6_n2 = Litteral.create ~is_positive:true atom_shallower_t6_n2
let litteral_shallower_t7_n2 = Litteral.create ~is_positive:true atom_shallower_t7_n2
let litteral_shallower_t8_n2 = Litteral.create ~is_positive:true atom_shallower_t8_n2
let litteral_shallower_t9_n0 = Litteral.create ~is_positive:true atom_shallower_t9_n0
let litteral_shallower_t9_n2 = Litteral.create ~is_positive:false atom_shallower_t9_n2

let heavy_typing_problem_to_simplify_3 =
  Typing_problem.of_list
    [
      litteral_height_t0_n0;
      litteral_height_t1_n1;
      litteral_height_t2_n2;
      litteral_le_n1_n2;
      litteral_shallower_t0_n1;
      litteral_shallower_t3_n0;
      litteral_shallower_t3_n1;
      litteral_shallower_t4_n0;
      litteral_shallower_t4_n1;
      litteral_shallower_t5_n2;
      litteral_shallower_t6_n2;
      litteral_shallower_t7_n2;
      litteral_shallower_t8_n2;
      litteral_shallower_t9_n0;
      litteral_shallower_t9_n2;
    ]

let%expect_test "positions problem heavy3" =
  present_total_resolution heavy_typing_problem_to_simplify_3;

  [%expect
    "
    Problem:

    [[
    height(t0, n0)
    height(t1, n1)
    height(t2, n2)
    not le(n1, n2)
    not shallower(t0, n1)
    shallower(t3, n0)
    shallower(t3, n1)
    shallower(t4, n0)
    shallower(t4, n1)
    shallower(t5, n2)
    shallower(t6, n2)
    shallower(t7, n2)
    shallower(t8, n2)
    shallower(t9, n0)
    not shallower(t9, n2)
    ]]


    Variable subset compatibility:
    {

    n0 -> {n0, n1, n2}
    n1 -> {n1}
    n2 -> {n2}
    t0 -> {t0}
    t1 -> {t0, t1, t2}
    t2 -> {t0, t1, t2}
    t3 -> {t3, t4, t5, t6, t7, t8, t9}
    t4 -> {t3, t4, t5, t6, t7, t8, t9}
    t5 -> {t3, t4, t5, t6, t7, t8, t9}
    t6 -> {t3, t4, t5, t6, t7, t8, t9}
    t7 -> {t3, t4, t5, t6, t7, t8, t9}
    t8 -> {t3, t4, t5, t6, t7, t8, t9}
    t9 -> {t9}

    }

    Simplification coming from one variable by one variable:
    {
    t3 -> t4  ;  t5 -> t8  ;  t6 -> t8  ;  t7 -> t8
    }

    Semi simplified typing problem:

    [[
    height(t0, n0)
    height(t1, n1)
    height(t2, n2)
    not le(n1, n2)
    not shallower(t0, n1)
    shallower(t4, n0)
    shallower(t4, n1)
    shallower(t8, n2)
    shallower(t9, n0)
    not shallower(t9, n2)
    ]]



        Position-valid simplifications:
    {
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n1  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t0  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t1  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t0  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t4  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t8  ;  t8 -> t9  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t8  ;  t9 -> t9
    }
    {
    n0 -> n2  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t2  ;  t2 -> t2  ;  t4 -> t9  ;  t8 -> t9  ;  t9 -> t9
    }
    }

    All valid simplification on semi-simplified typing problem:
    {
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t4 -> t4  ;  t8 -> t8  ;  t9 -> t9
    }
    }

    A smallest simplification:
    {
    n0 -> n0  ;  n1 -> n1  ;  n2 -> n2  ;  t0 -> t0  ;  t1 -> t1  ;  t2 -> t2  ;  t3 -> t4  ;  t4 -> t4  ;  t5 -> t8  ;  t6 -> t8  ;  t7 -> t8  ;  t8 -> t8  ;  t9 -> t9
    }

    Simplified problem:

    [[
    height(t0, n0)
    height(t1, n1)
    height(t2, n2)
    not le(n1, n2)
    not shallower(t0, n1)
    shallower(t4, n0)
    shallower(t4, n1)
    shallower(t8, n2)
    shallower(t9, n0)
    not shallower(t9, n2)
    ]]"]
