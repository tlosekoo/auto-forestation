open Model_checking
open Term.Aliases

let%expect_test "renaming application" =
  let renaming_1 _v = Term.Database_for_tests.x in
  let renaming_2 v =
    if Term.Typed_symbol.equal Term.Database_for_tests.n v then
      Term.Database_for_tests.x
    else
      Term.Database_for_tests.n2
  in
  let some_universe = Database_for_tests.typing_problem_2 in
  let renamed_universe_1 = Typing_problem.apply_renaming renaming_1 some_universe in
  let renamed_universe_2 = Typing_problem.apply_renaming renaming_2 some_universe in
  Format.fprintf Format.std_formatter
    "Renaming universe\n%a\nwith\n(1): _->x \n(2): n->x; _->n2\n\nand it gives\n(1):\n%a\n\n(2):\n%a\n\n"
    Typing_problem.pp some_universe Typing_problem.pp renamed_universe_1 Typing_problem.pp
    renamed_universe_2;
  [%expect
    "
    Renaming universe

    [[
    odd(n)
    r_l_n(l, s(n))
    ]]

    with
    (1): _->x
    (2): n->x; _->n2

    and it gives
    (1):

    [[
    odd(x)
    r_l_n(x, s(x))
    ]]


    (2):

    [[
    odd(x)
    r_l_n(n2, s(x))
    ]]"]

let%expect_test "get vars" =
  Set_typed_symbol.pp Format.std_formatter
    (Typing_problem.get_vars Database_for_tests.typing_problem_2);
  [%expect "{l, n}"]

let%expect_test "preorder" =
  Format.fprintf Format.std_formatter
    "Splitting the following nodes according to shape:\n\n%a\n\nGives:\n\n%a\n" Typing_problem.pp
    Database_for_tests.some_litterals
    (Printing.pp_list_sep "\n\n---------\n\n" Typing_problem.pp)
    (Typing_problem.split_according_to_shape_with_variable_comparator
       (fun _ _ -> 0)
       Database_for_tests.some_litterals);
  [%expect
    "
    Splitting the following nodes according to shape:


    [[
    even(n2)
    odd(n)
    odd(n2)
    r_l_n(l, s(n))
    r_n(n1)
    ]]


    Gives:


    [[
    r_n(n1)
    ]]


    ---------


    [[
    r_l_n(l, s(n))
    ]]


    ---------


    [[
    odd(n)
    odd(n2)
    ]]


    ---------


    [[
    even(n2)
    ]]"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
