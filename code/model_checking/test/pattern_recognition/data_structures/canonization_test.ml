open Model_checking
open Term.Aliases

let canonization_is_idempotent (u : Typing_problem.t) : bool =
  let canon_u, _ = Canonization.canonize u in
  let canon_canon_u, _ = Canonization.canonize canon_u in
  let ok = Typing_problem.equal canon_u canon_canon_u in
  if not ok then
    let () =
      Format.fprintf Format.std_formatter
        "Canonisation is not idempotent!!\n\nu:\n%a\n\ncanon_u:\n%a\n\ncanon_canon_u:\n%a\n\n"
        Typing_problem.pp u Typing_problem.pp canon_u Typing_problem.pp canon_canon_u
    in
    false
  else
    true

(* TODO: not true for now, as ordering is kind of arbitrary *)
(* let%test "test_canonization_is_idempotent" =
   Set_typing_problem.for_all canonization_is_idempotent some_universes *)

(* let canonization_is_reversible (u : Typing_problem.t) : bool =
     let canon_u, canonize_map = Canonization.canonize u in
     let reverse_canonize_map = Option.get (Map_typedSymbol.swap canonize_map) in
     let reversed_canon_u =
       Typing_problem.apply_renaming
         (Map_typedSymbol_typedSymbol.to_function_with_not_mapped_is_identity reverse_canonize_map)
         canon_u
     in
     let ok = Typing_problem.equal u reversed_canon_u in
     if not ok then
       let () =
         Format.fprintf Format.std_formatter
           "Canonisation is not reversible!!\n\nu:\n%a\n\ncanon_u:\n%a\n\nreversed_canon_u:\n%a\n\n"
           (Typing_problem.pp)
           u
           (Typing_problem.pp)
           canon_u
           (Typing_problem.pp)
           reversed_canon_u
       in
       false
     else
       true

   let%test "test_canonization_is_reversible" =
     List.for_all canonization_is_reversible Database_for_tests.some_universes_prrs *)

let canonization_is_label_invariant (u : Typing_problem.t) : bool =
  let vars_of_u = Typing_problem.get_vars u in
  let some_renaming =
    vars_of_u |> Set_typed_symbol.to_list
    |> List.map (fun v ->
           ( v,
             Term.Typed_symbol.create
               (Term.Symbol.create (Term.Typed_symbol.to_string v ^ "_woah_very_fresh"))
               (Term.Typed_symbol.get_type v) ))
    |> Map_typedSymbol.of_list
  in
  let renamed_universe =
    Typing_problem.apply_renaming
      (Map_typedSymbol.to_function_with_not_mapped_is_identity some_renaming)
      u
  in
  (* let () =
       Format.fprintf Format.std_formatter "\n%a\n" (Typing_problem.pp ~see_automata:false) renamed_universe
     in *)
  let canon_u, _ = Canonization.canonize u in
  let canon_renamed_u, _ = Canonization.canonize renamed_universe in
  let out = Typing_problem.equal canon_u canon_renamed_u in
  let () =
    if not out then
      Format.fprintf Format.std_formatter
        "u:\n%a\nrenamed_u:\n%a\n\ncanon_u:\n%a\ncanon_renamed_u:\n%a\n" Typing_problem.pp u
        Typing_problem.pp renamed_universe Typing_problem.pp canon_u Typing_problem.pp
        canon_renamed_u
  in
  out

(* Not really true, as our canonization is not perfect  *)
(* let%test "test_canonization_is_label_invariant" =
   List.for_all canonization_is_label_invariant Database_for_tests.some_typing_problems_shocs *)

let canon_equal_means_isomorphic ((u1, u2) : Typing_problem.t * Typing_problem.t) : bool =
  let canon_1, map_1 = Canonization.canonize u1 in
  let canon_2, map_2 = Canonization.canonize u2 in
  if Typing_problem.equal canon_1 canon_2 then
    let reverse_map_1 = Option.get (Map_typedSymbol.swap map_1) in
    let reverse_map_2 = Option.get (Map_typedSymbol.swap map_2) in
    let map_12 = Map_typedSymbol.auto_compose map_1 reverse_map_2 in
    let map_21 = Map_typedSymbol.auto_compose map_2 reverse_map_1 in
    let u1_as_u2 =
      Typing_problem.apply_renaming
        (Map_typedSymbol.to_function_with_not_mapped_is_identity map_12)
        u1
    in
    let u2_as_u1 =
      Typing_problem.apply_renaming
        (Map_typedSymbol.to_function_with_not_mapped_is_identity map_21)
        u2
    in
    Typing_problem.equal u1 u2_as_u1 && Typing_problem.equal u2 u1_as_u2
  else
    true

let%test "test_canon_equal_means_isomorphic" =
  let some_universes_as_list = Database_for_tests.some_typing_problems_shocs in
  List.for_all canon_equal_means_isomorphic
    (Misc.List_op.cartesian_product some_universes_as_list some_universes_as_list)

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
