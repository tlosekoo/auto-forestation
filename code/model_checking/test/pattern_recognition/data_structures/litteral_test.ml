open Term
open Model_checking

let%expect_test "get_patterns" =
  (Printing.pp_list_and_brackets Pattern.pp)
    Format.std_formatter
    (Litteral.get_patterns Database_for_tests.litteral_21);
  [%expect "[l, s(n)]"]

let%expect_test "get relation" =
  Clause.Predicate_symbol_or_equality.pp Format.std_formatter
    (Litteral.get_relation Database_for_tests.litteral_21);
  [%expect "
    r_l_n"]

let mapped_21 =
  Litteral.map_patterns
    (fun p ->
      if Pattern.equal p Term.Database_for_tests.p_l then Term.Database_for_tests.p_conssxl else p)
    Database_for_tests.litteral_21

let%expect_test "test_map" =
  Litteral.pp Format.std_formatter mapped_21;
  [%expect "r_l_n(cons(s(x), l), s(n))"]

let%expect_test "variables dfs" =
  (Printing.pp_list Typed_symbol.pp)
    Format.std_formatter
    (Litteral.get_variables_dfs_ordered mapped_21);
  [%expect "x, l, n"]

let%test "compare_shape" =
  let litteral_autre =
    Litteral.create
      (Clause.Atom_patterns.create
         (Clause.Predicate_symbol_or_equality.create_from_predicate_symbol
            (snd Tree_tuple_formalisms.Database_for_tests_shocs.a_nat_1))
         [Term.Database_for_tests.p_n])
      ~is_positive:true
  in
  let nodes_a =
    [Database_for_tests.litteral_11; Database_for_tests.litteral_11; Database_for_tests.litteral_1]
  in
  let nodes_b = [Database_for_tests.litteral_12; Database_for_tests.litteral_21; litteral_autre] in
  let comparison_shape = List.map2 Litteral.same_shape nodes_a nodes_b in
  (not (List.nth comparison_shape 0))
  && (not (List.nth comparison_shape 1))
  && List.nth comparison_shape 2

let%expect_test "complement" =
  Litteral.pp Format.std_formatter (Litteral.negate Database_for_tests.litteral_31);
  [%expect "
    not r_nat(n)"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
