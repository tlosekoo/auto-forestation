(* open Model_checking

   let root = "../../../"
   let folder_of_tests = root ^ "test/pattern_recognition/analysis/"
   let folder_loops_testing_files = folder_of_tests ^ "loops_input_files/"

   let get_filename (success : bool) (i : int) =
     let prefix = if success then "success" else "failure" in
     Format.flush_str_formatter
       (Format.fprintf Format.str_formatter "%s%s_%o.lp" folder_loops_testing_files prefix i)

   let filename_failure (i : int) = get_filename false i
   let filename_success (i : int) = get_filename true i
   let every_file_success = List.init 7 (fun i -> filename_success (i + 1))
   let every_file_failure = List.init 7 (fun i -> filename_failure (i + 1))

   let check_file (filename : string) : bool =
     let file_content = Inout.Reader.get_all_lines_from_file filename in
     let content = List.fold_left (fun s -> ( ^ ) (s ^ "\n")) "" file_content in
     let pp_filename_content c = Format.fprintf c "%s" content in
     Use_clingo_as_solver.analysis_returns_sat ~path_temporary_folder:Paths.temporary_clingo_files
       Loops.loops_analysis pp_filename_content

   let%test "test_every_file of loops" =
     let failures = List.map check_file every_file_failure in
     let successes = List.map check_file every_file_success in
     let failures_ok = List.for_all (fun b -> not b) failures in
     let successes_ok = List.for_all (fun b -> b) successes in
     let ok = failures_ok && successes_ok in
     let () =
       if not ok then
         Format.fprintf Format.std_formatter "\n\nfailures:\n%a\n\nsuccesses:\n%a\n\n"
           (Printing.pp_list_and_brackets Printing.pp_bool)
           failures
           (Printing.pp_list_and_brackets Printing.pp_bool)
           successes
     in
     ok

   let u1_is_easier_than_u2
       (u1 : UniversePrrs.t)
       (u2 : UniversePrrs.t)
       (ss_linking_the_two : Term.Shallow_substitution.t) : bool =
     let nodes_2 = UniversePrrs.get_nodes u2 in
     let ssub_2 = UniversePrrs.get_substitution u2 in
     let sub_2 = Term.Shallow_substitution.into_substitution ssub_2 in
     let nodes_1 = UniversePrrs.get_nodes u1 in
     let ssub_1 = UniversePrrs.get_substitution u1 in
     let ss_to_apply_to_u1 = Term.Shallow_substitution.compose ssub_1 ss_linking_the_two in
     let s_to_apply_to_u1 = Term.Shallow_substitution.into_substitution ss_to_apply_to_u1 in
     Aliases.Set_node.for_all
       (fun n1 ->
         let vars_n1 = Node.get_variables n1 in
         let patterns_n1 = List.map (Term.Substitution.apply_on_ext_var s_to_apply_to_u1) vars_n1 in
         let tg_n1 = Node.get_typing_goal n1 in
         Aliases.Set_node.exists
           (fun n2 ->
             let vars_n2 = Node.get_variables n2 in
             let patterns_n2 = List.map (Term.Substitution.apply_on_ext_var sub_2) vars_n2 in
             let tg_n2 = Node.get_typing_goal n2 in
             List.equal (Option.equal Term.Pattern.equal) patterns_n1 patterns_n2
             && Typing_goal.equal tg_n1 tg_n2)
           nodes_2)
       nodes_1

   let loops_produuces_correct_result (h : History.t) : bool =
     let loops_certificate = Loops.loops_with_certificate h in
     match loops_certificate with
     | None -> true
     | Some (depth, subst) ->
         let universes = History.get_universes h in
         let last_universe = List.nth universes (List.length universes - 1) in
         let loops_universe = List.nth universes depth in
         u1_is_easier_than_u2 loops_universe last_universe subst

   (* let hist_u3_0_0 =
        History.create [Database_for_tests.pointing_u3; Database_for_tests.only_element_of_stepped_u3]

      let hist_u3_0_0_x =
        History.create [Database_for_tests.pointing_u3; Database_for_tests.only_element_of_stepped_u3]

      let%test "loops on stepped u3" =
        (* false *)
        true *)

   let () = Printing.pp_ok () *)
