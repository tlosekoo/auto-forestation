open Model_checking
open Term.Aliases

let%test "Empty is simpler than empty" =
  Option.is_some (Loops.is_simpler_with_renaming Typing_problem.empty Typing_problem.empty)

let independent_are_easier (u : Typing_problem.t) : bool =
  let independent_universes = Split_independent.split_independent u in
  let ok =
    Set_typing_problem.for_all
      (fun independent_u ->
        let answer = Loops.is_simpler_with_renaming independent_u u in
        let () =
          if Option.is_none answer then
            Format.fprintf Format.std_formatter "Universe\n%a\nis not simpler than\n%a\n"
              Typing_problem.pp independent_u Typing_problem.pp u
        in
        Option.is_some answer)
      independent_universes
  in
  let () =
    if not ok then
      Format.fprintf Format.std_formatter "Simplifying\n%a\ninto\n\n%a\n" Typing_problem.pp u
        Set_typing_problem.pp independent_universes
  in
  ok

let%test "test_independent_are_independent" =
  List.for_all independent_are_easier Database_for_tests.some_typing_problems_shocs

let%expect_test "simpler" =
  (* let ordered_universes = Set_typing_problem.to_list small_subset_of_universes in *)
  let some_typing_problem =
    List.nth
      (List.filter
         (fun u -> Typing_problem.cardinal u = 3)
         Database_for_tests.some_typing_problems_shocs)
      2
  in
  let splitted_universe = Split_independent.split_independent some_typing_problem in
  let splitted_are_easier =
    splitted_universe |> Set_typing_problem.to_list
    |> List.map (fun su -> Loops.is_simpler_with_renaming su some_typing_problem)
  in
  (* let  = Typing_problem.is_simpler_with_renaming u1 u2 in *)
  Format.fprintf Format.std_formatter
    "\nsome universe:\n%a\n\nits independent universes:\n%a\n\nsplitted are easier:\n%a\n\n"
    Typing_problem.pp some_typing_problem
    (Set_typing_problem.pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n\n--\n\n"
       ~pp_element:Typing_problem.pp)
    splitted_universe
    (Printing.pp_list_and_brackets_sep "\n\n"
       (Printing.pp_opt (Map_typedSymbol.pp_param ~sep:"  ;  " ~pp_value:Term.Typed_symbol.pp)))
    splitted_are_easier;
  [%expect
    "
        some universe:

        [[
        odd(n)
        odd(n2)
        r_n(n1)
        ]]


        its independent universes:


        [[
        odd(n)
        ]]


        --


        [[
        odd(n2)
        ]]


        --


        [[
        r_n(n1)
        ]]



        splitted are easier:
        [Some({
        n1 -> n1
        })

        Some({
        n2 -> n
        })

        Some({
        n -> n
        })]"]

let%expect_test "simpler 2" =
  (* let ordered_universes = Set_typing_problem.to_list small_subset_of_universes in *)
  let some_universe =
    List.nth
      (List.filter
         (fun u -> Typing_problem.cardinal u = 3)
         Database_for_tests.some_typing_problems_shocs)
      6
  in
  let splitted_universe = Split_independent.split_independent some_universe in
  let splitted_are_easier =
    splitted_universe |> Set_typing_problem.to_list
    |> List.map (fun su -> Loops.is_simpler_with_renaming su some_universe)
  in
  (* let  = Typing_problem.is_simpler_with_renaming u1 u2 in *)
  Format.fprintf Format.std_formatter
    "\nsome universe:\n%a\n\nits independent universes:\n%a\n\nsplitted are easier:\n%a\n\n"
    Typing_problem.pp some_universe
    (Set_typing_problem.pp_param ~opening:"\n" ~closing:"\n" ~sep:"\n\n--\n\n"
       ~pp_element:Typing_problem.pp)
    splitted_universe
    (Printing.pp_list_and_brackets_sep "\n\n"
       (Printing.pp_opt (Map_typedSymbol.pp Term.Typed_symbol.pp)))
    splitted_are_easier;
  [%expect
    "
       some universe:

       [[
       even(n2)
       odd(n2)
       r_l_n(l, s(n))
       ]]


       its independent universes:


       [[
       even(n2)
       odd(n2)
       ]]


       --


       [[
       r_l_n(l, s(n))
       ]]



       splitted are easier:
       [Some({
       l -> l  ;  n -> n
       })

       Some({
       n2 -> n2
       })]"]

let%expect_test "split independent empty" =
  let us = Split_independent.split_independent Typing_problem.empty in
  Set_typing_problem.pp Format.std_formatter us;
  [%expect "
       {
       [[

       ]]
       }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
