open Model_checking
open Instantiation
open Tree_tuple_formalisms

let list_some_typing_problems_shocs = Database_for_tests.some_typing_problems_shocs
let solving_using_ocaml_with_trace = PatternRecognitionShocs.solve_and_keep_memory
let solving_using_ocaml = PatternRecognitionShocs.solve

let solver_answers_ocaml =
  List.map
    (solving_using_ocaml ~time_to_stop:None ~_debug_mode:false Database_for_tests.big_model)
    list_some_typing_problems_shocs

type stats = {
  nb_memoised_typing_problems : int;
      (* nb_found_typing_problems : int; *)
      (* mean_split : floats; *)
      (* mean_nb_action:float *)
}

let stats_on_memory (ms : Memory_state.t) : stats =
  let memoisation = ms.memoisation in
  let nb_memoised_typing_problems = Memoisation_map.cardinal memoisation in
  {nb_memoised_typing_problems}

(* This function has only debugging purposes *)
let present_typing_problem_solving
    ?(print_unstable_information = false)
    (solving_function :
      time_to_stop:Misc.Time.time_limit ->
      _debug_mode:bool ->
      Typing_problem.t ->
      Search_outcome_for_one_formula.t * Memory_state.t)
    (u : Typing_problem.t) : unit =
  let () =
    Format.fprintf Format.std_formatter "\nUniverse to model_check:\n%a\n" Typing_problem.pp u
  in
  let beginning = Unix.gettimeofday () in
  let solver_output, trace = solving_function ~time_to_stop:None ~_debug_mode:false u in
  let ending = Unix.gettimeofday () in
  let stats = stats_on_memory trace in
  let () =
    if print_unstable_information then
      Format.fprintf Format.std_formatter
        "\nSolving took %f time unit\nNumber of explored typing_problems is: %d\n\n"
        (ending -. beginning) stats.nb_memoised_typing_problems
  in
  Format.fprintf Format.std_formatter "Solution is: %a\n\n"
    (Search_outcome.pp Term.Substitution.pp)
    solver_output

let%expect_test "Modelpp" =
  ModelShocs.pp Format.std_formatter Database_for_tests.big_model;
  [%expect
    "
    |_

    even ->
    {
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
    }
    ;
    odd ->
    {
      odd(s(x_0_0)) <= even(x_0_0)
    }
    ;
    r_l_n ->
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
    }
    ;
    r_n ->
    {
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }

    --
    Equality automata are defined for: {custom, elt, elt_tree, nat, natlist, nattsil, pair_nat_nat}
    _|"]

let%expect_test "Pattern recognition typing_problem_0" =
  present_typing_problem_solving
    (solving_using_ocaml_with_trace Database_for_tests.big_model)
    Database_for_tests.typing_problem_0;
  [%expect
    "
    Universe to model_check:
    
    [[
    r_n(n1)
    ]]

    Solution is: Yes: {
    n1 -> z
    }"]

let%expect_test "Pattern recognition typing_problem_1" =
  present_typing_problem_solving
    (solving_using_ocaml_with_trace Database_for_tests.big_model)
    Database_for_tests.typing_problem_1;
  [%expect
    "
             Universe to model_check:

             [[
             even(n2)
             odd(n2)
             ]]

             Solution is: No: ()"]

let%expect_test "Pattern recognition typing_problem_2" =
  present_typing_problem_solving
    (solving_using_ocaml_with_trace Database_for_tests.big_model)
    Database_for_tests.typing_problem_2;
  [%expect
    "
             Universe to model_check:

             [[
             odd(n)
             r_l_n(l, s(n))
             ]]

             Solution is: Yes: {
             l -> cons(z, cons(z, nil))  ;  n -> s(z)
             }"]

let%expect_test "Pattern recognition typing_problem_3" =
  present_typing_problem_solving
    (solving_using_ocaml_with_trace Database_for_tests.big_model)
    Database_for_tests.typing_problem_3;
  [%expect
    "
             Universe to model_check:

             [[
             r_nat(n)
             ]]

             Solution is: No: ()"]

let litteral_is_true_in_model (m : ModelShocs.t) (l : Litteral.t) : bool =
  let shocs = ModelShocs.extract_recognizer m ~positive:l.is_positive in
  let ground_l =
    Litteral.map_patterns
      (Term.Datatype_environment.groundify_pattern (ModelShocs.get_datatype_env m))
      l
  in
  (* let () =
       Format.fprintf Format.std_formatter
         "Litteral  %a  (%a once grounded) must be true in model\n%a\n" Litteral.pp l Litteral.pp ground_l
         ModelShocs.pp m
     in *)
  let relation = Clause.Predicate_symbol_or_equality.into_relation ground_l.atom.predicate in
  let typing_obligation = Typing_obligation.create relation ground_l.atom.argument in
  Shocs.accepts shocs typing_obligation

(* This lemma states that the found substitution, if any, is indeed a solution  *)
let lemma_correction_pattern_recognition
    (model : ModelShocs.t)
    (solving_function : Typing_problem.t -> Search_outcome_for_one_formula.t)
    (u : Typing_problem.t) : bool =
  let solver_output = solving_function u in
  match solver_output with
  | Misc.Tription.Yes subst ->
      let litterals =
        u |> Typing_problem.to_seq
        |> Seq.map (Litteral.map_patterns (Term.Substitution.apply_on_pattern subst))
      in

      let substitution_works = Seq.for_all (litteral_is_true_in_model model) litterals in
      substitution_works
  | Misc.Tription.Maybe _ -> true
  | Misc.Tription.No () -> true (* Cannot check for now *)

let lemma_completeness_pattern_recognition
    (solving_function : Typing_problem.t -> Search_outcome_for_one_formula.t)
    (u : Typing_problem.t)
    (expected_answer : Database_for_tests.expected_answer) : bool =
  let solver_output = solving_function u in
  match solver_output with
  | Misc.Tription.Yes _ -> expected_answer.is_sat
  | Misc.Tription.Maybe _ -> false
  | Misc.Tription.No () -> not expected_answer.is_sat

let test_lemma_correction_pattern_recognition_for_every_problem_of_database
    (model : ModelShocs.t)
    (solving_function : Typing_problem.t -> Search_outcome_for_one_formula.t) : bool =
  List.for_all
    (lemma_correction_pattern_recognition model solving_function)
    list_some_typing_problems_shocs

let test_lemma_completeness_pattern_recognition_for_every_problem_of_database
    (solving_function : Typing_problem.t -> Search_outcome_for_one_formula.t) : bool =
  List.for_all2
    (lemma_completeness_pattern_recognition solving_function)
    Database_for_tests.four_typing_problems Database_for_tests.expected_answers

let%test "correction pattern recognition ocaml" =
  test_lemma_correction_pattern_recognition_for_every_problem_of_database
    Database_for_tests.big_model
    (solving_using_ocaml ~time_to_stop:None ~_debug_mode:false Database_for_tests.big_model)

let%test "completeness pattern recognition ocaml" =
  test_lemma_completeness_pattern_recognition_for_every_problem_of_database
    (solving_using_ocaml ~time_to_stop:None ~_debug_mode:false Database_for_tests.big_model)

let%expect_test "step typing_problem_3" =
  let some_typing_problem = Database_for_tests.typing_problem_2 in
  let stepped_u =
    UnfolderShocs.unfold_typing_problem Database_for_tests.big_model ~time_to_stop:None
      ~_debug_mode:false some_typing_problem
  in
  Format.fprintf Format.std_formatter "Universe:\n%a\n\nUnfolded typing_problem:\n%a\n\n"
    Typing_problem.pp some_typing_problem Unfolding_of_typingProblem.pp stepped_u;
  [%expect
    "
        Universe:

        [[
        odd(n)
        r_l_n(l, s(n))
        ]]


        Unfolded typing_problem:

        {
        l -> cons(l_0, l_1)  ;  n -> s(n_0)
        }
          =>

        [[
        r_n(v0)
        ]]


        [[
        even(v0)
        r_l_n(v1, s(v0))
        ]]"]

(* let%expect_test "simplifying typing_problem non-agressively" =
     let u = Database_for_tests.problem_to_simplify in
     let simplified_u, simplifying_renaming =
       Simplify_typing_problem.simplify_typing_problem ~agressively:false u
     in
     Format.fprintf Format.std_formatter
       "Universe:\n%a\n\nSimplified_typing_problem:\n%a\n\nNecessary renaming:\n%a\n" Typing_problem.pp u
       Typing_problem.pp simplified_u Term.Aliases.Map_typedSymbol_typedSymbol.pp simplifying_renaming;
     [%expect
       "
                Universe:

                [[
                r_l_n(l1, n1)
                r_l_n(l2, n1)
                r_l_n(l3, n1)
                r_l_n(l3, n2)
                r_n(n1)
                ]]


                Simplified_typing_problem:

                [[
                r_l_n(l2, n1)
                r_l_n(l3, n1)
                r_l_n(l3, n2)
                r_n(n1)
                ]]


                Necessary renaming:
                {
                l1 -> l2
                }"]

   let%expect_test "simplifying typing_problem agressively" =
     let u = Database_for_tests.problem_to_simplify in
     let simplified_u, simplifying_renaming =
       Simplify_typing_problem.simplify_typing_problem ~agressively:true u
     in
     Format.fprintf Format.std_formatter
       "Universe:\n%a\n\nSimplified_typing_problem:\n%a\n\nNecessary renaming:\n%a\n" Typing_problem.pp u
       Typing_problem.pp simplified_u Term.Aliases.Map_typedSymbol_typedSymbol.pp simplifying_renaming;
     [%expect
       "
       Universe:

       [[
       r_l_n(l1, n1)
       r_l_n(l2, n1)
       r_l_n(l3, n1)
       r_l_n(l3, n2)
       r_n(n1)
       ]]


       Simplified_typing_problem:

       [[
       r_l_n(l3, n1)
       r_n(n1)
       ]]


       Necessary renaming:
       {
       l1 -> l3  ;  l2 -> l3  ;  n2 -> n1
       }"] *)

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
