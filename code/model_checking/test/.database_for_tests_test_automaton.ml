open Model_checking
open Instantiation

let%expect_test "printing of symbols" =
  (* Printing.pp_list_sep "\n" *)
  (* (Printing.pp_assoclist_long Printing.pp_string (Term.Typed_function.pp_parameterized ~complete:true)) *)
  (* Format.std_formatter Database_for_tests.db_numerous_tests.symbols; *)
  Term.Datatype_environment.pp Format.std_formatter Database_for_tests.some_type_env;
  [%expect
    "
    {
    nat -> {s, z}  ;  natlist -> {cons, nil}  ;  nattree -> {leaf, node}  ;  nattsil -> {lin, snoc}  ;  pair_nat_nat -> {pair}
    }"]

let%expect_test "printing u0" =
  UniverseAutomaton.pp_param ~see_automata:true Format.std_formatter Database_for_tests.u0_automaton;
  [%expect
    "
  [[
  [n1] :
  q_n in
  {{{
  Q={q_n},
  Q_f={q_n},
  Delta=
  {
  <s>(q_n) -> q_n
  <z>() -> q_n
  }

  Datatype: <nat>
  Convolution form: right
  }}}

  ]]"]

let%expect_test "printing u1" =
  UniverseAutomaton.pp_param ~see_automata:true Format.std_formatter Database_for_tests.u1_automaton;
  [%expect
    "
  [[
  [n2] :
  qe_n in
  {{{
  Q={qe_n, qo_n},
  Q_f={qe_n},
  Delta=
  {
  <s>(qo_n) -> qe_n
  <z>() -> qe_n
  <s>(qe_n) -> qo_n
  }

  Datatype: <nat>
  Convolution form: right
  }}}

  [n2] :
  qo_n in
  {{{
  Q={qe_n, qo_n},
  Q_f={qo_n},
  Delta=
  {
  <s>(qo_n) -> qe_n
  <z>() -> qe_n
  <s>(qe_n) -> qo_n
  }

  Datatype: <nat>
  Convolution form: right
  }}}

  ]]"]

let%expect_test "printing u2" =
  UniverseAutomaton.pp_param ~see_automata:true Format.std_formatter Database_for_tests.u2_automaton;
  [%expect
    "
    [[
    [l, s(n)] :
    q_l_n in
    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}

    [n] :
    qo_n in
    {{{
    Q={qe_n, qo_n},
    Q_f={qo_n},
    Delta=
    {
    <s>(qo_n) -> qe_n
    <z>() -> qe_n
    <s>(qe_n) -> qo_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}

    ]]"]

let%expect_test "printing u3" =
  UniverseAutomaton.pp_param ~see_automata:true Format.std_formatter Database_for_tests.u3_automaton;
  [%expect
    "
    [[
    [n] :
    q_n in
    {{{
    Q={},
    Q_f={},
    Delta=
    {

    }

    Datatype: <nat>
    Convolution form: left
    }}}

    ]]"]

let%expect_test "model length" =
  ModelAutomaton.pp Format.std_formatter Database_for_tests.model_length__automaton;
  [%expect
    "
    |_
    name: None

    length ->

    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}


    --
    Equality automata are defined for: {nat, natlist, nattree, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_z_n" =
  ModelAutomaton.pp Format.std_formatter Database_for_tests.model_length_z_n__automaton;
  [%expect
    "
    |_
    name: None

    length ->

    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}


    --
    Equality automata are defined for: {nat, natlist, nattree, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_all" =
  ModelAutomaton.pp Format.std_formatter Database_for_tests.model_length_all__automaton;
  [%expect
    "
    |_
    name: None

    length ->

    {{{
    Q={q_comp_nat, q_comp_natlist, q_comp_natlist_x_nat},
    Q_f={q_comp_natlist_x_nat},
    Delta=
    {
    <s>(q_comp_nat) -> q_comp_nat
    <z>() -> q_comp_nat
    <cons>(q_comp_nat, q_comp_natlist) -> q_comp_natlist
    <nil>() -> q_comp_natlist
    <cons, s>(q_comp_nat, q_comp_natlist_x_nat) -> q_comp_natlist_x_nat
    <cons, z>(q_comp_nat, q_comp_natlist) -> q_comp_natlist_x_nat
    <nil, s>(q_comp_nat) -> q_comp_natlist_x_nat
    <nil, z>() -> q_comp_natlist_x_nat
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}


    --
    Equality automata are defined for: {nat, natlist, nattree, nattsil, pair_nat_nat}
    _|"]

let%expect_test "model length_odd_isnat" =
  ModelAutomaton.pp Format.std_formatter Database_for_tests.model_length_and_odd_and_nat__automaton;
  [%expect
    "
    |_
    name: None

    isnat ->

    {{{
    Q={q_n},
    Q_f={q_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}

    ;
    length ->

    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}

    ;
    odd ->

    {{{
    Q={qe_n, qo_n},
    Q_f={qo_n},
    Delta=
    {
    <s>(qo_n) -> qe_n
    <z>() -> qe_n
    <s>(qe_n) -> qo_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}


    --
    Equality automata are defined for: {nat, natlist, nattree, nattsil, pair_nat_nat}
    _|"]

let%expect_test "clauses length" =
  Clause.Ordered_clause_system_patterns.pp Format.std_formatter
    Database_for_tests.chc_system_for_length;
  [%expect
    "
    {
    length(nil, z) <= True -> 0
    length(cons(x, l), s(n)) <= length(l, n) -> 0
    eq_nat(n1, n2) <= length(l, n1) /\\ length(l, n2) -> 0
    length(l, n) <= length(cons(x, l), s(n)) -> 0
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
