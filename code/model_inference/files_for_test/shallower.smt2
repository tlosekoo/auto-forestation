(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (etree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)	
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls etree) (rs etree))
		)
	)
)


(define-fun-rec subtree ((s etree) (t etree)) Bool
	(
		match s
		(
			(leaf true)
			(
				(node ea ta1 ta2) 
				(
					match t 
					(
						(leaf false)
						((node eb tb1 tb2) 
							(ite (= ea eb) (ite (subtree ta1 tb1) (subtree ta2 tb2) false) false)
						)

					)
				)
			)
		)
	)
)

(define-fun-rec shallower ((t etree) (n nat)) Bool
	(
		match t
		(
			(leaf true)
			(
				(node e t1 t2) 
				(
					match n 
					(
						(z false)
;;						((s m) (and (shallower t1 m) (shallower t2 m)))
						((s m) (ite (shallower t1 m) (shallower t2 m) false))
					)
				)
			)
		)
	)
)

(assert (forall ((d nat) (t etree) (s etree))
	(=> (and (shallower t d) (subtree s t)) (shallower s d))
))

(check-sat)
