open Analyze_output

let pp_link
    (pp_text : Format.formatter -> 'a -> unit)
    (text : 'a)
    (pp_link : Format.formatter -> 'b -> unit)
    (link : 'b)
    (c : Format.formatter)
    () : unit =
  Format.fprintf c "<a href=\"%a\">%a</a>\n" pp_link link pp_text text

let path_root_to_log (path_and_name : string) ~(is_positive : bool) =
  let name_and_extension = Filename.basename path_and_name in
  let name = Filename.chop_extension name_and_extension in
  let log_name = Paths.log_name_from_name name in
  let path_results_folder_to_log_folder =
    if is_positive then
      Paths.path_true_folder_from_result_folder
    else
      Paths.path_false_folder_from_result_folder
  in
  (* let path_result_folder_to_log = path_log_folder_from_results_folder ^ log_name in *)
  let path_root_to_log_folder =
    Paths.path_root_to_result_folder ^ path_results_folder_to_log_folder
  in
  let path_root_to_log = path_root_to_log_folder ^ log_name in
  let directory_creation = "mkdir -p " ^ path_root_to_log_folder in
  (* Create folder for log storing *)
  let _ = Sys.command directory_creation in
  path_root_to_log

let pp_as_cell (pp : Format.formatter -> 'a -> unit) (c : Format.formatter) : 'a -> unit =
  Format.fprintf c "<td>%a</td>" pp

let pp_html_line_of_output
    (c : Format.formatter)
    ~(is_positive : bool)
    ((path_and_name, stats) : string * interesting_stats) =
  let name_and_extension = Filename.basename path_and_name in
  let name = Filename.chop_extension name_and_extension in
  let log_name = Paths.log_name_from_name name in
  let path_log_folder_from_results_folder =
    if is_positive then
      Paths.path_true_folder_from_result_folder
    else
      Paths.path_false_folder_from_result_folder
  in
  let path_result_folder_to_log = path_log_folder_from_results_folder ^ log_name in
  let path_instance_from_result_folder = Paths.path_result_folder_to_root ^ path_and_name in
  (* Printing corresponding line in recapitulative file *)
  let () =
    Format.fprintf c "<tr>\n%a\n%a\n%a\n%a\n</tr>\n"
      (pp_as_cell
         (pp_link Printing.pp_string name Printing.pp_string path_instance_from_result_folder))
      ()
      (pp_as_cell
         (pp_link display_solution_type stats.solving_status Printing.pp_string
            path_result_folder_to_log))
      ()
      (pp_as_cell (fun c () -> Format.pp_print_string c (Printf.sprintf "%.4f" stats.total_time)))
      () (pp_as_cell Format.pp_print_int) stats.ice_nb_iterations
  in
  ()

(* Ugly. Too bad. *)
let generate_html_of_stats
    (c : Format.formatter)
    (stat_every_positive_instance : (string * interesting_stats) list)
    (stat_every_negative_instance : (string * interesting_stats) list)
    (stats_positive_instances : proved_stats)
    (stats_negative_instances : proved_stats) : unit =
  let () = Format.fprintf c "<h1>Benchmark results</h1>\n\n" in
  let () =
    Format.fprintf c "True properties as %a ------- False properties as %a\n\n" pp_proved_stats
      stats_positive_instances pp_proved_stats stats_negative_instances
  in
  let () = Format.fprintf c "<h2>POSITIVE INSTANCES</h2>\n\n" in
  let () = Format.fprintf c "<table>\n" in
  let () =
    Format.fprintf c
      "<tr>\n<th>Instance name</th>\n<th>Answer</th>\n<th>Time (in sec)</th>\n<th>ICE iterations</th>\n</tr>\n"
  in
  let () = List.iter (pp_html_line_of_output c ~is_positive:true) stat_every_positive_instance in
  let () = Format.fprintf c "</table>\n" in

  let () = Format.fprintf c "<h2>NEGATIVE INSTANCES</h2>\n\n" in
  let () = Format.fprintf c "<table>\n" in

  let () =
    Format.fprintf c
      "<tr>\n<th>Instance name</th>\n<th>Answer</th>\n<th>Time (in sec)</th>\n<th>ICE iterations</th>\n</tr>\n"
  in
  let () = List.iter (pp_html_line_of_output c ~is_positive:false) stat_every_negative_instance in
  let () = Format.fprintf c "</table>\n" in

  ()
