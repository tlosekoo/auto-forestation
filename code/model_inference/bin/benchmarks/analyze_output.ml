(* let get_filename_and_learning_problems (file : string) : string * Model_inference.Learning_problem.t
     =
   (Filename.basename file, Model_inference.Reader_smtlib.parse_smtlib file) *)

type proved_stats = int * int * int

let pp_proved_stats
    (c : Format.formatter)
    ((nb_proved, nb_neither_proved_nor_disproved, nb_disproved) : proved_stats) : unit =
  Format.fprintf c "(Proved,DontKnow,Disproved): (%d, %d, %d)" nb_proved
    nb_neither_proved_nor_disproved nb_disproved

let categorize_outputs_of_solver (outputs : (_, _, _) Misc.Tription.t list) =
  let nb_proved = List.length (List.filter Misc.Tription.is_yes outputs) in
  let nb_disproved = List.length (List.filter Misc.Tription.is_no outputs) in
  let nb_neither_proved_nor_disproved = List.length (List.filter Misc.Tription.is_maybe outputs) in
  (nb_proved, nb_neither_proved_nor_disproved, nb_disproved)

let compare_solved_instances_by_name (name_1, _, _) (name_2, _, _) : int =
  String.compare name_1 name_2

let display_solution_type (c : Format.formatter) (solution : (_, _, _) Misc.Tription.t) : unit =
  let str_solution =
    match solution with
    | Yes _ -> "Proved"
    | Maybe _ -> "Unknown"
    | No _ -> "Disproved"
  in
  Format.fprintf c "%s" str_solution

type interesting_stats = {
  solving_status : (unit, unit, unit) Misc.Tription.t;
  ice_nb_iterations : int;
  total_time : float;
}
