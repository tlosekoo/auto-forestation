open Benchmarks_common

type solved_instance =
  string
  * Program_to_clauses.Learning_problem.t
  * (Model_inference.Ice_answer_shocs.t * Model_inference.Log.t)

type solved_instances = solved_instance list

let pp_log_one_instance (path_and_name : string) ~(is_positive : bool) (log : Model_inference.Log.t)
    : unit =
  let log_file = path_root_to_log path_and_name ~is_positive in
  let () = Inout.Write_to_file.write_from_pp log_file (fun c -> Model_inference.Log_pp.pp c log) in
  ()

let compute_solution__pp_log__and__return_output
    (name_and_path : string)
    (lp : Program_to_clauses.Learning_problem.t)
    ~(is_positive : bool) : interesting_stats =
  let real_time_monitoring = true in
  let () =
    if real_time_monitoring then
      Format.fprintf Format.std_formatter "Instance %s is ...\n" name_and_path;
    Format.pp_print_flush Format.std_formatter ()
  in
  let output, log =
    Model_inference.Solving_and_logging_shocs.solve_and_return_log ~name:(Some name_and_path) lp
  in

  let total_time = Model_inference.Log_pp.compute_total_time log in
  let ice_nb_iterations = Model_inference.Log.get_number_of_past_ice_iterations log in
  let solving_status =
    let forget _ = () in
    Misc.Tription.map forget forget forget output
  in
  let () =
    if real_time_monitoring then
      let () =
        Format.fprintf Format.std_formatter "%a" Model_inference.Log_pp.pp_solution_summary log
      in
      let () = Format.pp_force_newline Format.std_formatter () in
      let () = Format.pp_print_flush Format.std_formatter () in
      ()
  in
  let () = pp_log_one_instance name_and_path ~is_positive log in
  {solving_status; ice_nb_iterations; total_time}

let solve_every_instance_and_output_stats
    (lps : (string * Program_to_clauses.Learning_problem.t) list)
    ~(is_positive : bool) =
  let name_and_stats =
    List.map
      (fun (name, lp) -> (name, compute_solution__pp_log__and__return_output ~is_positive name lp))
      lps
  in
  let global_stats =
    categorize_outputs_of_solver (List.map (fun stats -> (snd stats).solving_status) name_and_stats)
  in
  (name_and_stats, global_stats)

let run_benchmarks () : unit =
  let paths_smtlib_positive_instances =
    Inout.Reader.ls_filter_extension Paths.smtlib_extension Paths.path_benchmarks_true_from_root
  in
  let paths_smtlib_negative_instances =
    Inout.Reader.ls_filter_extension Paths.smtlib_extension Paths.path_benchmarks_false_from_root
  in

  (* Parse instances *)
  let name_and_positive_instances =
    List.map
      (fun s ->
        (* let () = Format.fprintf Format.std_formatter "\n\nParsing %s\n\n" s in *)
        (s, Program_to_clauses.Smtlib_reader.smtlib_to_clauses s))
      paths_smtlib_positive_instances
  in
  let name_and_negative_instances =
    List.map
      (fun s -> (s, Program_to_clauses.Smtlib_reader.smtlib_to_clauses s))
      paths_smtlib_negative_instances
  in

  let name_and_positive_instances =
    List.sort
      (Misc.Pair.compare String.compare Program_to_clauses.Learning_problem.compare)
      name_and_positive_instances
  in
  let name_and_negative_instances =
    List.sort
      (Misc.Pair.compare String.compare Program_to_clauses.Learning_problem.compare)
      name_and_negative_instances
  in

  let positive_stats, positive_global_stats =
    solve_every_instance_and_output_stats name_and_positive_instances ~is_positive:true
  in
  let negative_stats, negative_global_stats =
    solve_every_instance_and_output_stats name_and_negative_instances ~is_positive:false
  in

  let () =
    Format.fprintf Format.std_formatter "True properties as %a\nFalse properties as %a\n\n"
      pp_proved_stats positive_global_stats pp_proved_stats negative_global_stats
  in

  let html_file = Paths.path_root_to_result_file in
  let out_csv_file = open_out html_file in
  let format_csv_file = Format.formatter_of_out_channel out_csv_file in
  let () =
    generate_html_of_stats format_csv_file positive_stats negative_stats positive_global_stats
      negative_global_stats
  in
  let () = close_out out_csv_file in

  ()
