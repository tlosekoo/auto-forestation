(* path benchmarks *)
let path_benchmarks_from_root = "../../benchmarks/smtlib/"
let path_benchmarks_true_from_root = path_benchmarks_from_root ^ "true/"
let path_benchmarks_false_from_root = path_benchmarks_from_root ^ "false/"
let smtlib_extension = ".smt2"
let path_root = ""
let path_root_to_result_folder = "results/"
let path_root_to_result_file = path_root_to_result_folder ^ "benchmarks.html"
let path_true_folder_from_result_folder = "true/"
let path_false_folder_from_result_folder = "false/"
let path_result_folder_to_root = "../"
let path_result_true_folder_to_root = "../" ^ path_result_folder_to_root
let path_result_false_folder_to_root = "../" ^ path_result_folder_to_root
let log_name_from_name (name : string) : string = "log_" ^ name ^ ".txt"
let path_log_timer = path_root_to_result_folder ^ "time.html"
