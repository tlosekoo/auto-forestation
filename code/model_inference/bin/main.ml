let main () =
  let () = Format.pp_print_newline Format.std_formatter () in
  let usage_msg = "solver  ... <file1>" in
  let input_files = ref [] in
  let anon_fun filename = input_files := filename :: !input_files in
  let debug_mode = ref false in
  let with_automata = ref false in
  let benchmarks_mode = ref false in
  let speclist =
    [
      ("-debug", Arg.Set debug_mode, ": debug mode. Prints a lot of information.");
      ( "-automata",
        Arg.Set with_automata,
        ": Tree automata mode. Uses tree automata as intermediate representation instead of ShoCs."
      );
      ("-benchmarks", Arg.Set benchmarks_mode, ": Benchmark mode. Runs all benchmarks.");
      ("-verbose", Arg.Set Model_inference.Verbosity.is_verbose, ": Verbose mode. Prints more stuff");
    ]
  in
  if not (Check_for_dependencies.clingo_is_installed ()) then
    Format.fprintf Format.std_formatter "Cannot run program: clingo not found.\n"
  else
    let () = Parameters.set_parameters_using_configuration_file () in
    let () = Arg.parse speclist anon_fun usage_msg in
    if !benchmarks_mode then
      (* if !with_automata then
           Benchmarks_automata.run_benchmarks default_parameters convolution
         else *)
      Benchmarks_shocs.run_benchmarks ()
    else
      let input_file =
        match !input_files with
        | [] -> failwith "No input files provided"
        | f :: _ -> Sys.getcwd () ^ "/" ^ f
      in
      let learning_problem = Program_to_clauses.Smtlib_reader.smtlib_to_clauses input_file in
      let log_file = Paths.path_root_to_result_folder ^ Paths.log_name_from_name "default" in
      (* if !with_automata then
           let _final_answer, log =
             Model_inference.Solving_and_logging_automata.solve_and_return_log ~_verbose:!verbose_mode
               learning_problem default_parameters convolution
           in
           Model_inference.Solving_and_logging_automata.pp_log_in_file ~print_short_output:true log
             log_file
         else *)
      let _final_answer, log =
        Model_inference.Solving_and_logging_shocs.solve_and_return_log learning_problem
      in
      let () =
        Model_inference.Solving_and_logging_shocs.pp_log_in_file ~print_short_output:true log_file
          log
      in
      Model_inference.Time_log.write_to_html Paths.path_log_timer log.time_log

let () = main ()
