let my_parameters_learning =
  Model_inference.Parameters_learning.
    {
      timeout = Some 40.;
      approximation_method =
        Program_to_clauses.Approximation.Remove_functionnality_constraint_where_possible;
    }

let my_parameters_inference_shocs =
  Model_inference.Parameters_shocs_inference_projection.
    {
      allowed_subterm_combination = No_two_subterms_from_the_same_term;
      arity_of_projection = Arity_of_parent_relation;
    }

let my_parameters_teacher =
  Model_checking.Parameters_pattern_recognition.
    {
      simplify_typing_problem_before_splitting_independent = true;
      simplify_typing_problem_after_splitting_independent = true;
      only_use_one_by_one_simplification = false;
      stop_the_search_if_two_typing_obligations_are_contradictory = true;
      try_trivial_actions_first_when_unfolding_typing_problem = true;
      percentage_of_timeout_to_use_to_check_more_formulas_once_one_is_known_to_be_false = 1.;
    }

(* Run once at the beginning *)
let set_parameters_using_configuration_file () =
  Model_inference.Parameters_learning.parameters := my_parameters_learning;
  Model_inference.Parameters_shocs_inference_projection.parameters := my_parameters_inference_shocs;
  Model_checking.Parameters_pattern_recognition.parameters := my_parameters_teacher;
  ()
