let clingo_is_installed () : bool =
  let command = "clingo -v &>/dev/null" in
  let return_code = Sys.command command in
  let clingo_is_installed = return_code = 0 in
  clingo_is_installed
