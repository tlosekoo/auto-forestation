(* open Model_inference
open Model_checking.Instantiation

let%expect_test "mainn" =
  let filename =
    "/home/tlosekoo/Documents/THESIS/these_losekoot/auto-forestation_clean/code/model_inference/files_for_test/flip.lp"
  in
  let filename_content_result = Inout.Reader.get_all_lines_from_file filename in
  let filename_content_str =
    match filename_content_result with
    | Ok lines -> List.fold_left (fun s -> ( ^ ) (s ^ "\n")) "" lines
    | Error e -> raise (Failure e)
  in
  let time_limit = 30 in
  let pp c = Format.fprintf c "%s" filename_content_str in

  let () =
    Format.fprintf Format.std_formatter "----------------------Input clingo problem:\n%a\n\n"
      (fun c () -> pp c)
      ()
  in
  let output_solver =
    Io_clingo.Use_clingo_as_solver.run_analysis ~time_limit
      ~path_temporary_folder:Paths.temporary_folder Shocs_finder_clingo.shocs_finder_analysis pp
  in

  let () =
    Format.fprintf Format.std_formatter "  ---------------------------Output of Clingo:\n%a\n\n"
      Io_clingo.Use_clingo_as_solver.pp_solution output_solver
  in

  let shocs =
    Misc.Tription.map_yes
      (Clingo_read_solver_shocs.parse_shocs Term.Database_for_tests.testing_type_env)
      output_solver
  in
  let () =
    Format.fprintf Format.std_formatter
      "-----------------------\nCorresponding SHoCs:\n%a\n\n-----------------\nFINI\n"
      (Misc.Tription.pp Shocs_as_recognizer.pp Io_clingo.Use_clingo_as_solver.pp_stopping_reason
         Misc.Unit.pp)
      shocs
  in
  ();
  [%expect.unreachable]
[@@expect.uncaught_exn
  {|
  (* CR expect_test_collector: This test expectation appears to contain a backtrace.
     This is strongly discouraged as backtraces are fragile.
     Please change this test to not include a backtrace. *)

  (Failure
     "Shoc  (_r_2(<node>), {_r_2([(0, 1)])})  has incompatible types.\
    \nHead relation is _r_2 : <elt_tree> and head functions are <node>: <elt_tree>.")
  Raised at Tree_tuple_formalisms__Shoc_core.create in file "lib/formalisms/shocs/shoc/shoc_core.ml", line 51, characters 4-33
  Called from Stdlib__List.map in file "list.ml", line 92, characters 20-23
  Called from Stdlib__List.map in file "list.ml", line 92, characters 32-39
  Called from Stdlib__List.map in file "list.ml", line 92, characters 32-39
  Called from Stdlib__List.map in file "list.ml", line 92, characters 32-39
  Called from Model_inference__Parse_everything.parse_rule_facts in file "lib/learner_teacher/learner/shocs/io_with_clingo_shocs/read/reader_others/parse_everything.ml", line 89, characters 2-210
  Called from Misc__Tription.map in file "lib/tription.ml", line 21, characters 18-25
  Called from Test_model_inference__Manual_testing.(fun) in file "test/model_generation/manual_testing.ml", line 33, characters 4-131
  Called from Expect_test_collector.Make.Instance_io.exec in file "collector/expect_test_collector.ml", line 234, characters 12-19

  Trailing output
  ---------------
  ----------------------Input clingo problem:

  #const imax=24.

  function(a, elt, 0).
  function(b, elt, 0).
  function(leaf, elt_tree, 0).
  function(node, elt_tree, 3).
  initial_relation(flip, 2).
  relation_types(flip, (elt_tree,elt_tree)).
  initial_relation(memt_elt, 2).
  relation_types(memt_elt, (elt,elt_tree)).
  input_atom(flip, (t_339, t_327)).
  input_litteral(l_376, negative, flip, (t_339, t_327)).
  input_atom(flip, (t_338, t_339)).
  input_litteral(l_375, negative, flip, (t_338, t_339)).
  input_atom(flip, (t_335, t_339)).
  input_litteral(l_374, negative, flip, (t_335, t_339)).
  input_atom(flip, (t_328, t_339)).
  input_litteral(l_373, negative, flip, (t_328, t_339)).
  input_atom(flip, (t_327, t_339)).
  input_litteral(l_372, negative, flip, (t_327, t_339)).
  input_atom(flip, (t_325, t_327)).
  input_litteral(l_371, negative, flip, (t_325, t_327)).
  input_atom(memt_elt, (t_342, t_340)).
  input_litteral(l_370, negative, memt_elt, (t_342, t_340)).
  input_atom(memt_elt, (t_342, t_327)).
  input_litteral(l_369, negative, memt_elt, (t_342, t_327)).
  input_atom(memt_elt, (t_342, t_322)).
  input_litteral(l_368, negative, memt_elt, (t_342, t_322)).
  input_atom(memt_elt, (t_341, t_340)).
  input_litteral(l_367, negative, memt_elt, (t_341, t_340)).
  input_atom(memt_elt, (t_341, t_339)).
  input_litteral(l_366, negative, memt_elt, (t_341, t_339)).
  input_atom(memt_elt, (t_341, t_338)).
  input_litteral(l_365, negative, memt_elt, (t_341, t_338)).
  input_atom(memt_elt, (t_341, t_335)).
  input_litteral(l_364, negative, memt_elt, (t_341, t_335)).
  input_atom(memt_elt, (t_341, t_333)).
  input_litteral(l_363, negative, memt_elt, (t_341, t_333)).
  input_atom(memt_elt, (t_341, t_332)).
  input_litteral(l_362, negative, memt_elt, (t_341, t_332)).
  input_atom(memt_elt, (t_341, t_324)).
  input_litteral(l_361, negative, memt_elt, (t_341, t_324)).
  input_atom(flip, (t_340, t_340)).
  input_litteral(l_360, positive, flip, (t_340, t_340)).
  input_atom(flip, (t_339, t_339)).
  input_litteral(l_359, positive, flip, (t_339, t_339)).
  input_atom(flip, (t_333, t_338)).
  input_litteral(l_358, positive, flip, (t_333, t_338)).
  input_atom(flip, (t_327, t_327)).
  input_litteral(l_357, positive, flip, (t_327, t_327)).
  input_atom(memt_elt, (t_342, t_339)).
  input_litteral(l_356, positive, memt_elt, (t_342, t_339)).
  input_atom(memt_elt, (t_342, t_326)).
  input_litteral(l_355, positive, memt_elt, (t_342, t_326)).
  input_atom(memt_elt, (t_342, t_325)).
  input_litteral(l_354, positive, memt_elt, (t_342, t_325)).
  input_atom(memt_elt, (t_342, t_323)).
  input_litteral(l_353, positive, memt_elt, (t_342, t_323)).
  input_atom(memt_elt, (t_342, t_321)).
  input_litteral(l_352, positive, memt_elt, (t_342, t_321)).
  input_atom(memt_elt, (t_342, t_320)).
  input_litteral(l_351, positive, memt_elt, (t_342, t_320)).
  input_atom(memt_elt, (t_341, t_337)).
  input_litteral(l_350, positive, memt_elt, (t_341, t_337)).
  input_atom(memt_elt, (t_341, t_336)).
  input_litteral(l_349, positive, memt_elt, (t_341, t_336)).
  input_atom(memt_elt, (t_341, t_334)).
  input_litteral(l_348, positive, memt_elt, (t_341, t_334)).
  input_atom(memt_elt, (t_341, t_331)).
  input_litteral(l_347, positive, memt_elt, (t_341, t_331)).
  input_atom(memt_elt, (t_341, t_330)).
  input_litteral(l_346, positive, memt_elt, (t_341, t_330)).
  input_atom(memt_elt, (t_341, t_329)).
  input_litteral(l_345, positive, memt_elt, (t_341, t_329)).
  input_atom(memt_elt, (t_341, t_328)).
  input_litteral(l_344, positive, memt_elt, (t_341, t_328)).
  input_atom(memt_elt, (t_341, t_327)).
  input_litteral(l_343, positive, memt_elt, (t_341, t_327)).
  ground_clause(c_407).
  ground_clause_litteral(c_407, (l_360)).
  ground_clause(c_406).
  ground_clause_litteral(c_406, (l_359)).
  ground_clause(c_405).
  ground_clause_litteral(c_405, (l_358)).
  ground_clause(c_404).
  ground_clause_litteral(c_404, (l_357)).
  ground_clause(c_403).
  ground_clause_litteral(c_403, (l_356)).
  ground_clause(c_402).
  ground_clause_litteral(c_402, (l_355)).
  ground_clause(c_401).
  ground_clause_litteral(c_401, (l_354)).
  ground_clause(c_400).
  ground_clause_litteral(c_400, (l_353)).
  ground_clause(c_399).
  ground_clause_litteral(c_399, (l_352)).
  ground_clause(c_398).
  ground_clause_litteral(c_398, (l_351)).
  ground_clause(c_397).
  ground_clause_litteral(c_397, (l_349)).
  ground_clause(c_396).
  ground_clause_litteral(c_396, (l_347)).
  ground_clause(c_395).
  ground_clause_litteral(c_395, (l_345)).
  ground_clause(c_394).
  ground_clause_litteral(c_394, (l_344)).
  ground_clause(c_393).
  ground_clause_litteral(c_393, (l_343)).
  ground_clause(c_392).
  ground_clause_litteral(c_392, (l_376)).
  ground_clause(c_391).
  ground_clause_litteral(c_391, (l_365;l_375)).
  ground_clause(c_390).
  ground_clause_litteral(c_390, (l_364;l_374)).
  ground_clause(c_389).
  ground_clause_litteral(c_389, (l_373)).
  ground_clause(c_388).
  ground_clause_litteral(c_388, (l_372)).
  ground_clause(c_387).
  ground_clause_litteral(c_387, (l_371)).
  ground_clause(c_386).
  ground_clause_litteral(c_386, (l_370)).
  ground_clause(c_385).
  ground_clause_litteral(c_385, (l_369)).
  ground_clause(c_384).
  ground_clause_litteral(c_384, (l_368)).
  ground_clause(c_383).
  ground_clause_litteral(c_383, (l_367)).
  ground_clause(c_382).
  ground_clause_litteral(c_382, (l_366)).
  ground_clause(c_381).
  ground_clause_litteral(c_381, (l_364;l_350)).
  ground_clause(c_380).
  ground_clause_litteral(c_380, (l_364;l_346)).
  ground_clause(c_379).
  ground_clause_litteral(c_379, (l_363)).
  ground_clause(c_378).
  ground_clause_litteral(c_378, (l_362)).
  ground_clause(c_377).
  ground_clause_litteral(c_377, (l_361;l_348)).
  term_tuple((t_324), 1).
  term_tuple_i((t_324), 0, t_324).
  term_tuple((t_325, t_327), 2).
  term_tuple_i((t_325, t_327), 0, t_325).
  term_tuple_i((t_325, t_327), 1, t_327).
  term_tuple((t_325), 1).
  term_tuple_i((t_325), 0, t_325).
  term_tuple((t_326), 1).
  term_tuple_i((t_326), 0, t_326).
  term_tuple((t_327, t_327), 2).
  term_tuple_i((t_327, t_327), 0, t_327).
  term_tuple_i((t_327, t_327), 1, t_327).
  term_tuple((t_327, t_339), 2).
  term_tuple_i((t_327, t_339), 0, t_327).
  term_tuple_i((t_327, t_339), 1, t_339).
  term_tuple((t_327, t_340), 2).
  term_tuple_i((t_327, t_340), 0, t_327).
  term_tuple_i((t_327, t_340), 1, t_340).
  term_tuple((t_327, t_342), 2).
  term_tuple_i((t_327, t_342), 0, t_327).
  term_tuple_i((t_327, t_342), 1, t_342).
  term_tuple((t_327), 1).
  term_tuple_i((t_327), 0, t_327).
  term_tuple((t_328, t_339), 2).
  term_tuple_i((t_328, t_339), 0, t_328).
  term_tuple_i((t_328, t_339), 1, t_339).
  term_tuple((t_328), 1).
  term_tuple_i((t_328), 0, t_328).
  term_tuple((t_333, t_338), 2).
  term_tuple_i((t_333, t_338), 0, t_333).
  term_tuple_i((t_333, t_338), 1, t_338).
  term_tuple((t_335, t_339), 2).
  term_tuple_i((t_335, t_339), 0, t_335).
  term_tuple_i((t_335, t_339), 1, t_339).
  term_tuple((t_335), 1).
  term_tuple_i((t_335), 0, t_335).
  term_tuple((t_338, t_339), 2).
  term_tuple_i((t_338, t_339), 0, t_338).
  term_tuple_i((t_338, t_339), 1, t_339).
  term_tuple((t_339, t_327), 2).
  term_tuple_i((t_339, t_327), 0, t_339).
  term_tuple_i((t_339, t_327), 1, t_327).
  term_tuple((t_339, t_339), 2).
  term_tuple_i((t_339, t_339), 0, t_339).
  term_tuple_i((t_339, t_339), 1, t_339).
  term_tuple((t_339, t_340), 2).
  term_tuple_i((t_339, t_340), 0, t_339).
  term_tuple_i((t_339, t_340), 1, t_340).
  term_tuple((t_339, t_341), 2).
  term_tuple_i((t_339, t_341), 0, t_339).
  term_tuple_i((t_339, t_341), 1, t_341).
  term_tuple((t_339, t_342), 2).
  term_tuple_i((t_339, t_342), 0, t_339).
  term_tuple_i((t_339, t_342), 1, t_342).
  term_tuple((t_339), 1).
  term_tuple_i((t_339), 0, t_339).
  term_tuple((t_340, t_339), 2).
  term_tuple_i((t_340, t_339), 0, t_340).
  term_tuple_i((t_340, t_339), 1, t_339).
  term_tuple((t_340, t_340), 2).
  term_tuple_i((t_340, t_340), 0, t_340).
  term_tuple_i((t_340, t_340), 1, t_340).
  term_tuple((t_340, t_341), 2).
  term_tuple_i((t_340, t_341), 0, t_340).
  term_tuple_i((t_340, t_341), 1, t_341).
  term_tuple((t_340, t_342), 2).
  term_tuple_i((t_340, t_342), 0, t_340).
  term_tuple_i((t_340, t_342), 1, t_342).
  term_tuple((t_340), 1).
  term_tuple_i((t_340), 0, t_340).
  term_tuple((t_341, t_324), 2).
  term_tuple_i((t_341, t_324), 0, t_341).
  term_tuple_i((t_341, t_324), 1, t_324).
  term_tuple((t_341, t_327), 2).
  term_tuple_i((t_341, t_327), 0, t_341).
  term_tuple_i((t_341, t_327), 1, t_327).
  term_tuple((t_341, t_328), 2).
  term_tuple_i((t_341, t_328), 0, t_341).
  term_tuple_i((t_341, t_328), 1, t_328).
  term_tuple((t_341, t_329), 2).
  term_tuple_i((t_341, t_329), 0, t_341).
  term_tuple_i((t_341, t_329), 1, t_329).
  term_tuple((t_341, t_330), 2).
  term_tuple_i((t_341, t_330), 0, t_341).
  term_tuple_i((t_341, t_330), 1, t_330).
  term_tuple((t_341, t_331), 2).
  term_tuple_i((t_341, t_331), 0, t_341).
  term_tuple_i((t_341, t_331), 1, t_331).
  term_tuple((t_341, t_332), 2).
  term_tuple_i((t_341, t_332), 0, t_341).
  term_tuple_i((t_341, t_332), 1, t_332).
  term_tuple((t_341, t_333), 2).
  term_tuple_i((t_341, t_333), 0, t_341).
  term_tuple_i((t_341, t_333), 1, t_333).
  term_tuple((t_341, t_334), 2).
  term_tuple_i((t_341, t_334), 0, t_341).
  term_tuple_i((t_341, t_334), 1, t_334).
  term_tuple((t_341, t_335), 2).
  term_tuple_i((t_341, t_335), 0, t_341).
  term_tuple_i((t_341, t_335), 1, t_335).
  term_tuple((t_341, t_336), 2).
  term_tuple_i((t_341, t_336), 0, t_341).
  term_tuple_i((t_341, t_336), 1, t_336).
  term_tuple((t_341, t_337), 2).
  term_tuple_i((t_341, t_337), 0, t_341).
  term_tuple_i((t_341, t_337), 1, t_337).
  term_tuple((t_341, t_338), 2).
  term_tuple_i((t_341, t_338), 0, t_341).
  term_tuple_i((t_341, t_338), 1, t_338).
  term_tuple((t_341, t_339), 2).
  term_tuple_i((t_341, t_339), 0, t_341).
  term_tuple_i((t_341, t_339), 1, t_339).
  term_tuple((t_341, t_340), 2).
  term_tuple_i((t_341, t_340), 0, t_341).
  term_tuple_i((t_341, t_340), 1, t_340).
  term_tuple((t_341, t_341), 2).
  term_tuple_i((t_341, t_341), 0, t_341).
  term_tuple_i((t_341, t_341), 1, t_341).
  term_tuple((t_341, t_342), 2).
  term_tuple_i((t_341, t_342), 0, t_341).
  term_tuple_i((t_341, t_342), 1, t_342).
  term_tuple((t_341), 1).
  term_tuple_i((t_341), 0, t_341).
  term_tuple((t_342, t_320), 2).
  term_tuple_i((t_342, t_320), 0, t_342).
  term_tuple_i((t_342, t_320), 1, t_320).
  term_tuple((t_342, t_321), 2).
  term_tuple_i((t_342, t_321), 0, t_342).
  term_tuple_i((t_342, t_321), 1, t_321).
  term_tuple((t_342, t_322), 2).
  term_tuple_i((t_342, t_322), 0, t_342).
  term_tuple_i((t_342, t_322), 1, t_322).
  term_tuple((t_342, t_323), 2).
  term_tuple_i((t_342, t_323), 0, t_342).
  term_tuple_i((t_342, t_323), 1, t_323).
  term_tuple((t_342, t_325), 2).
  term_tuple_i((t_342, t_325), 0, t_342).
  term_tuple_i((t_342, t_325), 1, t_325).
  term_tuple((t_342, t_326), 2).
  term_tuple_i((t_342, t_326), 0, t_342).
  term_tuple_i((t_342, t_326), 1, t_326).
  term_tuple((t_342, t_327), 2).
  term_tuple_i((t_342, t_327), 0, t_342).
  term_tuple_i((t_342, t_327), 1, t_327).
  term_tuple((t_342, t_339), 2).
  term_tuple_i((t_342, t_339), 0, t_342).
  term_tuple_i((t_342, t_339), 1, t_339).
  term_tuple((t_342, t_340), 2).
  term_tuple_i((t_342, t_340), 0, t_342).
  term_tuple_i((t_342, t_340), 1, t_340).
  term_tuple((t_342, t_341), 2).
  term_tuple_i((t_342, t_341), 0, t_342).
  term_tuple_i((t_342, t_341), 1, t_341).
  term_tuple((t_342, t_342), 2).
  term_tuple_i((t_342, t_342), 0, t_342).
  term_tuple_i((t_342, t_342), 1, t_342).
  term_tuple((t_342), 1).
  term_tuple_i((t_342), 0, t_342).
  projection_of_terms((t_342, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_342, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_342, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_342, t_327),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_327),((), (1,1)),(t_340)).
  projection_of_terms((t_342, t_327),((), (1,2)),(t_340)).
  projection_of_terms((t_342, t_326),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_326),((), (1,1)),(t_340)).
  projection_of_terms((t_342, t_326),((), (1,2)),(t_339)).
  projection_of_terms((t_342, t_325),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_325),((), (1,1)),(t_339)).
  projection_of_terms((t_342, t_325),((), (1,2)),(t_340)).
  projection_of_terms((t_342, t_323),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_323),((), (1,1)),(t_327)).
  projection_of_terms((t_342, t_323),((), (1,2)),(t_339)).
  projection_of_terms((t_342, t_322),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_322),((), (1,1)),(t_327)).
  projection_of_terms((t_342, t_322),((), (1,2)),(t_327)).
  projection_of_terms((t_342, t_321),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_321),((), (1,1)),(t_326)).
  projection_of_terms((t_342, t_321),((), (1,2)),(t_340)).
  projection_of_terms((t_342, t_320),((), (1,0)),(t_341)).
  projection_of_terms((t_342, t_320),((), (1,1)),(t_325)).
  projection_of_terms((t_342, t_320),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_338),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_338),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_338),((), (1,2)),(t_339)).
  projection_of_terms((t_341, t_337),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_337),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_337),((), (1,2)),(t_335)).
  projection_of_terms((t_341, t_336),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_336),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_336),((), (1,2)),(t_328)).
  projection_of_terms((t_341, t_335),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_335),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_335),((), (1,2)),(t_327)).
  projection_of_terms((t_341, t_334),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_334),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_334),((), (1,2)),(t_324)).
  projection_of_terms((t_341, t_333),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_333),((), (1,1)),(t_339)).
  projection_of_terms((t_341, t_333),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_332),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_332),((), (1,1)),(t_339)).
  projection_of_terms((t_341, t_332),((), (1,2)),(t_339)).
  projection_of_terms((t_341, t_331),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_331),((), (1,1)),(t_339)).
  projection_of_terms((t_341, t_331),((), (1,2)),(t_327)).
  projection_of_terms((t_341, t_330),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_330),((), (1,1)),(t_335)).
  projection_of_terms((t_341, t_330),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_329),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_329),((), (1,1)),(t_328)).
  projection_of_terms((t_341, t_329),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_328),((), (1,0)),(t_342)).
  projection_of_terms((t_341, t_328),((), (1,1)),(t_327)).
  projection_of_terms((t_341, t_328),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_327),((), (1,0)),(t_341)).
  projection_of_terms((t_341, t_327),((), (1,1)),(t_340)).
  projection_of_terms((t_341, t_327),((), (1,2)),(t_340)).
  projection_of_terms((t_341, t_324),((), (1,0)),(t_341)).
  projection_of_terms((t_341, t_324),((), (1,1)),(t_327)).
  projection_of_terms((t_341, t_324),((), (1,2)),(t_340)).
  projection_of_terms((t_340, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_340, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_340, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_339),((), (0,0)),(t_342)).
  projection_of_terms((t_339),((), (0,1)),(t_340)).
  projection_of_terms((t_339),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_342),((), (0,0)),(t_342)).
  projection_of_terms((t_339, t_342),((), (0,1)),(t_340)).
  projection_of_terms((t_339, t_342),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_341),((), (0,0)),(t_342)).
  projection_of_terms((t_339, t_341),((), (0,1)),(t_340)).
  projection_of_terms((t_339, t_341),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_340),((), (0,0)),(t_342)).
  projection_of_terms((t_339, t_340),((), (0,1)),(t_340)).
  projection_of_terms((t_339, t_340),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_339),((), (0,0)),(t_342)).
  projection_of_terms((t_339, t_339),(((), (0,0)), (1,0)),(t_342, t_342)).
  projection_of_terms((t_339, t_339),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_339, t_339),(((), (0,0)), (1,2)),(t_342, t_340)).
  projection_of_terms((t_339, t_339),((), (0,1)),(t_340)).
  projection_of_terms((t_339, t_339),(((), (0,1)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_339, t_339),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_339, t_339),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_339, t_339),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_339),(((), (0,2)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_339, t_339),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_339, t_339),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_339, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_339, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_339, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_339, t_327),((), (0,0)),(t_342)).
  projection_of_terms((t_339, t_327),(((), (0,0)), (1,0)),(t_342, t_341)).
  projection_of_terms((t_339, t_327),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_339, t_327),(((), (0,0)), (1,2)),(t_342, t_340)).
  projection_of_terms((t_339, t_327),((), (0,1)),(t_340)).
  projection_of_terms((t_339, t_327),(((), (0,1)), (1,0)),(t_340, t_341)).
  projection_of_terms((t_339, t_327),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_339, t_327),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_339, t_327),((), (0,2)),(t_340)).
  projection_of_terms((t_339, t_327),(((), (0,2)), (1,0)),(t_340, t_341)).
  projection_of_terms((t_339, t_327),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_339, t_327),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_339, t_327),((), (1,0)),(t_341)).
  projection_of_terms((t_339, t_327),((), (1,1)),(t_340)).
  projection_of_terms((t_339, t_327),((), (1,2)),(t_340)).
  projection_of_terms((t_338, t_339),((), (0,0)),(t_342)).
  projection_of_terms((t_338, t_339),(((), (0,0)), (1,0)),(t_342, t_342)).
  projection_of_terms((t_338, t_339),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_338, t_339),(((), (0,0)), (1,2)),(t_342, t_340)).
  projection_of_terms((t_338, t_339),((), (0,1)),(t_340)).
  projection_of_terms((t_338, t_339),(((), (0,1)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_338, t_339),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_338, t_339),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_338, t_339),((), (0,2)),(t_339)).
  projection_of_terms((t_338, t_339),(((), (0,2)), (1,0)),(t_339, t_342)).
  projection_of_terms((t_338, t_339),(((), (0,2)), (1,1)),(t_339, t_340)).
  projection_of_terms((t_338, t_339),(((), (0,2)), (1,2)),(t_339, t_340)).
  projection_of_terms((t_338, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_338, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_338, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_335),((), (0,0)),(t_342)).
  projection_of_terms((t_335),((), (0,1)),(t_340)).
  projection_of_terms((t_335),((), (0,2)),(t_327)).
  projection_of_terms((t_335, t_339),((), (0,0)),(t_342)).
  projection_of_terms((t_335, t_339),(((), (0,0)), (1,0)),(t_342, t_342)).
  projection_of_terms((t_335, t_339),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_335, t_339),(((), (0,0)), (1,2)),(t_342, t_340)).
  projection_of_terms((t_335, t_339),((), (0,1)),(t_340)).
  projection_of_terms((t_335, t_339),(((), (0,1)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_335, t_339),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_335, t_339),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_335, t_339),((), (0,2)),(t_327)).
  projection_of_terms((t_335, t_339),(((), (0,2)), (1,0)),(t_327, t_342)).
  projection_of_terms((t_335, t_339),(((), (0,2)), (1,1)),(t_327, t_340)).
  projection_of_terms((t_335, t_339),(((), (0,2)), (1,2)),(t_327, t_340)).
  projection_of_terms((t_335, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_335, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_335, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_333, t_338),((), (0,0)),(t_342)).
  projection_of_terms((t_333, t_338),(((), (0,0)), (1,0)),(t_342, t_342)).
  projection_of_terms((t_333, t_338),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_333, t_338),(((), (0,0)), (1,2)),(t_342, t_339)).
  projection_of_terms((t_333, t_338),((), (0,1)),(t_339)).
  projection_of_terms((t_333, t_338),(((), (0,1)), (1,0)),(t_339, t_342)).
  projection_of_terms((t_333, t_338),(((), (0,1)), (1,1)),(t_339, t_340)).
  projection_of_terms((t_333, t_338),(((), (0,1)), (1,2)),(t_339, t_339)).
  projection_of_terms((t_333, t_338),((), (0,2)),(t_340)).
  projection_of_terms((t_333, t_338),(((), (0,2)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_333, t_338),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_333, t_338),(((), (0,2)), (1,2)),(t_340, t_339)).
  projection_of_terms((t_333, t_338),((), (1,0)),(t_342)).
  projection_of_terms((t_333, t_338),((), (1,1)),(t_340)).
  projection_of_terms((t_333, t_338),((), (1,2)),(t_339)).
  projection_of_terms((t_328),((), (0,0)),(t_342)).
  projection_of_terms((t_328),((), (0,1)),(t_327)).
  projection_of_terms((t_328),((), (0,2)),(t_340)).
  projection_of_terms((t_328, t_339),((), (0,0)),(t_342)).
  projection_of_terms((t_328, t_339),(((), (0,0)), (1,0)),(t_342, t_342)).
  projection_of_terms((t_328, t_339),(((), (0,0)), (1,1)),(t_342, t_340)).
  projection_of_terms((t_328, t_339),(((), (0,0)), (1,2)),(t_342, t_340)).
  projection_of_terms((t_328, t_339),((), (0,1)),(t_327)).
  projection_of_terms((t_328, t_339),(((), (0,1)), (1,0)),(t_327, t_342)).
  projection_of_terms((t_328, t_339),(((), (0,1)), (1,1)),(t_327, t_340)).
  projection_of_terms((t_328, t_339),(((), (0,1)), (1,2)),(t_327, t_340)).
  projection_of_terms((t_328, t_339),((), (0,2)),(t_340)).
  projection_of_terms((t_328, t_339),(((), (0,2)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_328, t_339),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_328, t_339),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_328, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_328, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_328, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_327),((), (0,0)),(t_341)).
  projection_of_terms((t_327),((), (0,1)),(t_340)).
  projection_of_terms((t_327),((), (0,2)),(t_340)).
  projection_of_terms((t_327, t_342),((), (0,0)),(t_341)).
  projection_of_terms((t_327, t_342),((), (0,1)),(t_340)).
  projection_of_terms((t_327, t_342),((), (0,2)),(t_340)).
  projection_of_terms((t_327, t_340),((), (0,0)),(t_341)).
  projection_of_terms((t_327, t_340),((), (0,1)),(t_340)).
  projection_of_terms((t_327, t_340),((), (0,2)),(t_340)).
  projection_of_terms((t_327, t_339),((), (0,0)),(t_341)).
  projection_of_terms((t_327, t_339),(((), (0,0)), (1,0)),(t_341, t_342)).
  projection_of_terms((t_327, t_339),(((), (0,0)), (1,1)),(t_341, t_340)).
  projection_of_terms((t_327, t_339),(((), (0,0)), (1,2)),(t_341, t_340)).
  projection_of_terms((t_327, t_339),((), (0,1)),(t_340)).
  projection_of_terms((t_327, t_339),(((), (0,1)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_327, t_339),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_327, t_339),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_327, t_339),((), (0,2)),(t_340)).
  projection_of_terms((t_327, t_339),(((), (0,2)), (1,0)),(t_340, t_342)).
  projection_of_terms((t_327, t_339),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_327, t_339),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_327, t_339),((), (1,0)),(t_342)).
  projection_of_terms((t_327, t_339),((), (1,1)),(t_340)).
  projection_of_terms((t_327, t_339),((), (1,2)),(t_340)).
  projection_of_terms((t_327, t_327),((), (0,0)),(t_341)).
  projection_of_terms((t_327, t_327),(((), (0,0)), (1,0)),(t_341, t_341)).
  projection_of_terms((t_327, t_327),(((), (0,0)), (1,1)),(t_341, t_340)).
  projection_of_terms((t_327, t_327),(((), (0,0)), (1,2)),(t_341, t_340)).
  projection_of_terms((t_327, t_327),((), (0,1)),(t_340)).
  projection_of_terms((t_327, t_327),(((), (0,1)), (1,0)),(t_340, t_341)).
  projection_of_terms((t_327, t_327),(((), (0,1)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_327, t_327),(((), (0,1)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_327, t_327),((), (0,2)),(t_340)).
  projection_of_terms((t_327, t_327),(((), (0,2)), (1,0)),(t_340, t_341)).
  projection_of_terms((t_327, t_327),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_327, t_327),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_327, t_327),((), (1,0)),(t_341)).
  projection_of_terms((t_327, t_327),((), (1,1)),(t_340)).
  projection_of_terms((t_327, t_327),((), (1,2)),(t_340)).
  projection_of_terms((t_326),((), (0,0)),(t_341)).
  projection_of_terms((t_326),((), (0,1)),(t_340)).
  projection_of_terms((t_326),((), (0,2)),(t_339)).
  projection_of_terms((t_325),((), (0,0)),(t_341)).
  projection_of_terms((t_325),((), (0,1)),(t_339)).
  projection_of_terms((t_325),((), (0,2)),(t_340)).
  projection_of_terms((t_325, t_327),((), (0,0)),(t_341)).
  projection_of_terms((t_325, t_327),(((), (0,0)), (1,0)),(t_341, t_341)).
  projection_of_terms((t_325, t_327),(((), (0,0)), (1,1)),(t_341, t_340)).
  projection_of_terms((t_325, t_327),(((), (0,0)), (1,2)),(t_341, t_340)).
  projection_of_terms((t_325, t_327),((), (0,1)),(t_339)).
  projection_of_terms((t_325, t_327),(((), (0,1)), (1,0)),(t_339, t_341)).
  projection_of_terms((t_325, t_327),(((), (0,1)), (1,1)),(t_339, t_340)).
  projection_of_terms((t_325, t_327),(((), (0,1)), (1,2)),(t_339, t_340)).
  projection_of_terms((t_325, t_327),((), (0,2)),(t_340)).
  projection_of_terms((t_325, t_327),(((), (0,2)), (1,0)),(t_340, t_341)).
  projection_of_terms((t_325, t_327),(((), (0,2)), (1,1)),(t_340, t_340)).
  projection_of_terms((t_325, t_327),(((), (0,2)), (1,2)),(t_340, t_340)).
  projection_of_terms((t_325, t_327),((), (1,0)),(t_341)).
  projection_of_terms((t_325, t_327),((), (1,1)),(t_340)).
  projection_of_terms((t_325, t_327),((), (1,2)),(t_340)).
  projection_of_terms((t_324),((), (0,0)),(t_341)).
  projection_of_terms((t_324),((), (0,1)),(t_327)).
  projection_of_terms((t_324),((), (0,2)),(t_340)).
  projector_tuple(((), (0,0)), 1).
  projector_tuple_i(((), (0,0)), 0, (0,0)).
  projector_tuple((((), (0,0)), (1,0)), 2).
  projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
  projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
  projector_tuple((((), (0,0)), (1,1)), 2).
  projector_tuple_i((((), (0,0)), (1,1)), 0, (0,0)).
  projector_tuple_i((((), (0,0)), (1,1)), 1, (1,1)).
  projector_tuple((((), (0,0)), (1,2)), 2).
  projector_tuple_i((((), (0,0)), (1,2)), 0, (0,0)).
  projector_tuple_i((((), (0,0)), (1,2)), 1, (1,2)).
  projector_tuple(((), (0,1)), 1).
  projector_tuple_i(((), (0,1)), 0, (0,1)).
  projector_tuple((((), (0,1)), (1,0)), 2).
  projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
  projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
  projector_tuple((((), (0,1)), (1,1)), 2).
  projector_tuple_i((((), (0,1)), (1,1)), 0, (0,1)).
  projector_tuple_i((((), (0,1)), (1,1)), 1, (1,1)).
  projector_tuple((((), (0,1)), (1,2)), 2).
  projector_tuple_i((((), (0,1)), (1,2)), 0, (0,1)).
  projector_tuple_i((((), (0,1)), (1,2)), 1, (1,2)).
  projector_tuple(((), (0,2)), 1).
  projector_tuple_i(((), (0,2)), 0, (0,2)).
  projector_tuple((((), (0,2)), (1,0)), 2).
  projector_tuple_i((((), (0,2)), (1,0)), 0, (0,2)).
  projector_tuple_i((((), (0,2)), (1,0)), 1, (1,0)).
  projector_tuple((((), (0,2)), (1,1)), 2).
  projector_tuple_i((((), (0,2)), (1,1)), 0, (0,2)).
  projector_tuple_i((((), (0,2)), (1,1)), 1, (1,1)).
  projector_tuple((((), (0,2)), (1,2)), 2).
  projector_tuple_i((((), (0,2)), (1,2)), 0, (0,2)).
  projector_tuple_i((((), (0,2)), (1,2)), 1, (1,2)).
  projector_tuple(((), (1,0)), 1).
  projector_tuple_i(((), (1,0)), 0, (1,0)).
  projector_tuple(((), (1,1)), 1).
  projector_tuple_i(((), (1,1)), 0, (1,1)).
  projector_tuple(((), (1,2)), 1).
  projector_tuple_i(((), (1,2)), 0, (1,2)).
  term_tuple_root_functions((t_324), (node)).
  term_tuple_root_functions((t_325), (node)).
  term_tuple_root_functions((t_325, t_327), (node,node)).
  term_tuple_root_functions((t_326), (node)).
  term_tuple_root_functions((t_327), (node)).
  term_tuple_root_functions((t_327, t_327), (node,node)).
  term_tuple_root_functions((t_327, t_339), (node,node)).
  term_tuple_root_functions((t_327, t_340), (node,leaf)).
  term_tuple_root_functions((t_327, t_342), (node,a)).
  term_tuple_root_functions((t_328), (node)).
  term_tuple_root_functions((t_328, t_339), (node,node)).
  term_tuple_root_functions((t_333, t_338), (node,node)).
  term_tuple_root_functions((t_335), (node)).
  term_tuple_root_functions((t_335, t_339), (node,node)).
  term_tuple_root_functions((t_338, t_339), (node,node)).
  term_tuple_root_functions((t_339), (node)).
  term_tuple_root_functions((t_339, t_327), (node,node)).
  term_tuple_root_functions((t_339, t_339), (node,node)).
  term_tuple_root_functions((t_339, t_340), (node,leaf)).
  term_tuple_root_functions((t_339, t_341), (node,b)).
  term_tuple_root_functions((t_339, t_342), (node,a)).
  term_tuple_root_functions((t_340), (leaf)).
  term_tuple_root_functions((t_340, t_339), (leaf,node)).
  term_tuple_root_functions((t_340, t_340), (leaf,leaf)).
  term_tuple_root_functions((t_340, t_341), (leaf,b)).
  term_tuple_root_functions((t_340, t_342), (leaf,a)).
  term_tuple_root_functions((t_341), (b)).
  term_tuple_root_functions((t_341, t_324), (b,node)).
  term_tuple_root_functions((t_341, t_327), (b,node)).
  term_tuple_root_functions((t_341, t_328), (b,node)).
  term_tuple_root_functions((t_341, t_329), (b,node)).
  term_tuple_root_functions((t_341, t_330), (b,node)).
  term_tuple_root_functions((t_341, t_331), (b,node)).
  term_tuple_root_functions((t_341, t_332), (b,node)).
  term_tuple_root_functions((t_341, t_333), (b,node)).
  term_tuple_root_functions((t_341, t_334), (b,node)).
  term_tuple_root_functions((t_341, t_335), (b,node)).
  term_tuple_root_functions((t_341, t_336), (b,node)).
  term_tuple_root_functions((t_341, t_337), (b,node)).
  term_tuple_root_functions((t_341, t_338), (b,node)).
  term_tuple_root_functions((t_341, t_339), (b,node)).
  term_tuple_root_functions((t_341, t_340), (b,leaf)).
  term_tuple_root_functions((t_341, t_341), (b,b)).
  term_tuple_root_functions((t_341, t_342), (b,a)).
  term_tuple_root_functions((t_342), (a)).
  term_tuple_root_functions((t_342, t_320), (a,node)).
  term_tuple_root_functions((t_342, t_321), (a,node)).
  term_tuple_root_functions((t_342, t_322), (a,node)).
  term_tuple_root_functions((t_342, t_323), (a,node)).
  term_tuple_root_functions((t_342, t_325), (a,node)).
  term_tuple_root_functions((t_342, t_326), (a,node)).
  term_tuple_root_functions((t_342, t_327), (a,node)).
  term_tuple_root_functions((t_342, t_339), (a,node)).
  term_tuple_root_functions((t_342, t_340), (a,leaf)).
  term_tuple_root_functions((t_342, t_341), (a,b)).
  term_tuple_root_functions((t_342, t_342), (a,a)).
  function_tuple_i((a), 0, a).
  function_tuple_i((a,a), 0, a).
  function_tuple_i((a,a), 1, a).
  function_tuple_i((a,b), 0, a).
  function_tuple_i((a,b), 1, b).
  function_tuple_i((a,leaf), 0, a).
  function_tuple_i((a,leaf), 1, leaf).
  function_tuple_i((a,node), 0, a).
  function_tuple_i((a,node), 1, node).
  function_tuple_i((b), 0, b).
  function_tuple_i((b,a), 0, b).
  function_tuple_i((b,a), 1, a).
  function_tuple_i((b,b), 0, b).
  function_tuple_i((b,b), 1, b).
  function_tuple_i((b,leaf), 0, b).
  function_tuple_i((b,leaf), 1, leaf).
  function_tuple_i((b,node), 0, b).
  function_tuple_i((b,node), 1, node).
  function_tuple_i((leaf), 0, leaf).
  function_tuple_i((leaf,a), 0, leaf).
  function_tuple_i((leaf,a), 1, a).
  function_tuple_i((leaf,b), 0, leaf).
  function_tuple_i((leaf,b), 1, b).
  function_tuple_i((leaf,leaf), 0, leaf).
  function_tuple_i((leaf,leaf), 1, leaf).
  function_tuple_i((leaf,node), 0, leaf).
  function_tuple_i((leaf,node), 1, node).
  function_tuple_i((node), 0, node).
  function_tuple_i((node,a), 0, node).
  function_tuple_i((node,a), 1, a).
  function_tuple_i((node,b), 0, node).
  function_tuple_i((node,b), 1, b).
  function_tuple_i((node,leaf), 0, node).
  function_tuple_i((node,leaf), 1, leaf).
  function_tuple_i((node,node), 0, node).
  function_tuple_i((node,node), 1, node).
  function_tuple_types((a), (elt)).
  function_tuple_types((a,a), (elt,elt)).
  function_tuple_types((a,b), (elt,elt)).
  function_tuple_types((a,leaf), (elt,elt_tree)).
  function_tuple_types((a,node), (elt,elt_tree)).
  function_tuple_types((b), (elt)).
  function_tuple_types((b,a), (elt,elt)).
  function_tuple_types((b,b), (elt,elt)).
  function_tuple_types((b,leaf), (elt,elt_tree)).
  function_tuple_types((b,node), (elt,elt_tree)).
  function_tuple_types((leaf), (elt_tree)).
  function_tuple_types((leaf,a), (elt_tree,elt)).
  function_tuple_types((leaf,b), (elt_tree,elt)).
  function_tuple_types((leaf,leaf), (elt_tree,elt_tree)).
  function_tuple_types((leaf,node), (elt_tree,elt_tree)).
  function_tuple_types((node), (elt_tree)).
  function_tuple_types((node,a), (elt_tree,elt)).
  function_tuple_types((node,b), (elt_tree,elt)).
  function_tuple_types((node,leaf), (elt_tree,elt_tree)).
  function_tuple_types((node,node), (elt_tree,elt_tree)).
  type_tuple((elt), 1).
  type_tuple_i((elt), 0, elt).
  type_tuple((elt,elt), 2).
  type_tuple_i((elt,elt), 0, elt).
  type_tuple_i((elt,elt), 1, elt).
  type_tuple((elt,elt_tree), 2).
  type_tuple_i((elt,elt_tree), 0, elt).
  type_tuple_i((elt,elt_tree), 1, elt_tree).
  type_tuple((elt_tree), 1).
  type_tuple_i((elt_tree), 0, elt_tree).
  type_tuple((elt_tree,elt), 2).
  type_tuple_i((elt_tree,elt), 0, elt_tree).
  type_tuple_i((elt_tree,elt), 1, elt).
  type_tuple((elt_tree,elt_tree), 2).
  type_tuple_i((elt_tree,elt_tree), 0, elt_tree).
  type_tuple_i((elt_tree,elt_tree), 1, elt_tree).

    ---------------------------Output of Clingo:
  Yes: relation(flip, 2)
  relation(memt_elt, 2)
  relation_type(flip, 0, elt_tree)
  relation_type(flip, 1, elt_tree)
  relation_type(memt_elt, 0, elt)
  relation_type(memt_elt, 1, elt_tree)
  relation((r, 1), 1)
  relation_type((r, 1), 0, elt_tree)
  relation((r, 2), 1)
  relation_type((r, 2), 0, elt_tree)
  relation((r, 3), 1)
  relation_type((r, 3), 0, elt)
  formal_atom(((4, memt_elt, (t_341, t_329)), ((), (1, 1))), (r, 2))
  formal_atom(((4, memt_elt, (t_341, t_337)), ((), (1, 2))), (r, 2))
  formal_atom(((4, memt_elt, (t_342, t_320)), ((), (1, 1))), (r, 1))
  formal_atom(((4, memt_elt, (t_342, t_321)), ((), (1, 1))), (r, 1))
  formal_atom(((4, flip, (t_327, t_327)), ((), (1, 0))), (r, 4))
  formal_atom(((4, flip, (t_327, t_327)), ((), (0, 0))), (r, 4))
  formal_atom(((4, memt_elt, (t_341, t_327)), ((), (1, 0))), (r, 4))
  formal_atom(((4, flip, (t_333, t_338)), (((), (0, 1)), (1, 2))), flip)
  formal_atom(((4, memt_elt, (t_342, t_326)), ((), (1, 2))), (r, 1))
  formal_atom(((4, flip, (t_333, t_338)), (((), (0, 2)), (1, 1))), flip)
  formal_atom(((4, flip, (t_327, t_327)), (((), (0, 1)), (1, 1))), flip)
  formal_atom(((4, flip, (t_333, t_338)), ((), (1, 0))), (r, 3))
  formal_atom(((4, flip, (t_333, t_338)), ((), (0, 0))), (r, 3))
  formal_atom(((4, memt_elt, (t_342, t_339)), ((), (1, 0))), (r, 3))
  formal_atom(((4, (r, 2), t_328), ((), (0, 1))), (r, 2))
  formal_atom(((4, (r, 2), t_335), ((), (0, 2))), (r, 2))
  formal_atom(((4, (r, 1), t_325), ((), (0, 1))), (r, 1))
  formal_atom(((4, (r, 1), t_326), ((), (0, 2))), (r, 1))
  formal_atom(((4, (r, 2), t_327), ((), (0, 0))), (r, 4))
  formal_atom(((4, (r, 1), t_339), ((), (0, 0))), (r, 3))
  rule((4, memt_elt, (t_341, t_327)), memt_elt)
  rule((4, memt_elt, (t_341, t_329)), memt_elt)
  rule((4, memt_elt, (t_341, t_337)), memt_elt)
  rule((4, memt_elt, (t_342, t_320)), memt_elt)
  rule((4, memt_elt, (t_342, t_321)), memt_elt)
  rule((4, memt_elt, (t_342, t_326)), memt_elt)
  rule((4, memt_elt, (t_342, t_339)), memt_elt)
  rule((4, flip, (t_327, t_327)), flip)
  rule((4, flip, (t_333, t_338)), flip)
  rule((4, flip, (t_340, t_340)), flip)
  rule((4, (r, 2), t_328), (r, 2))
  rule((4, (r, 2), t_335), (r, 2))
  rule((4, (r, 1), t_325), (r, 1))
  rule((4, (r, 1), t_326), (r, 1))
  rule((4, (r, 2), t_327), (r, 2))
  rule((4, (r, 4), t_341), (r, 4))
  rule((4, (r, 1), t_339), (r, 1))
  rule((4, (r, 3), t_342), (r, 3))
  rule_head_symbol((4, flip, (t_340, t_340)), 0, leaf)
  rule_head_symbol((4, flip, (t_340, t_340)), 1, leaf)
  rule_head_symbol((4, flip, (t_333, t_338)), 0, node)
  rule_head_symbol((4, flip, (t_333, t_338)), 1, node)
  rule_head_symbol((4, flip, (t_327, t_327)), 0, node)
  rule_head_symbol((4, flip, (t_327, t_327)), 1, node)
  rule_head_symbol((4, memt_elt, (t_342, t_339)), 0, a)
  rule_head_symbol((4, memt_elt, (t_342, t_339)), 1, node)
  rule_head_symbol((4, memt_elt, (t_342, t_326)), 0, a)
  rule_head_symbol((4, memt_elt, (t_342, t_326)), 1, node)
  rule_head_symbol((4, memt_elt, (t_342, t_321)), 0, a)
  rule_head_symbol((4, memt_elt, (t_342, t_321)), 1, node)
  rule_head_symbol((4, memt_elt, (t_342, t_320)), 0, a)
  rule_head_symbol((4, memt_elt, (t_342, t_320)), 1, node)
  rule_head_symbol((4, memt_elt, (t_341, t_337)), 0, b)
  rule_head_symbol((4, memt_elt, (t_341, t_337)), 1, node)
  rule_head_symbol((4, memt_elt, (t_341, t_329)), 0, b)
  rule_head_symbol((4, memt_elt, (t_341, t_329)), 1, node)
  rule_head_symbol((4, memt_elt, (t_341, t_327)), 0, b)
  rule_head_symbol((4, memt_elt, (t_341, t_327)), 1, node)
  rule_head_symbol((4, (r, 3), t_342), 0, a)
  rule_head_symbol((4, (r, 1), t_339), 0, node)
  rule_head_symbol((4, (r, 4), t_341), 0, b)
  rule_head_symbol((4, (r, 2), t_327), 0, node)
  rule_head_symbol((4, (r, 1), t_326), 0, node)
  rule_head_symbol((4, (r, 1), t_325), 0, node)
  rule_head_symbol((4, (r, 2), t_335), 0, node)
  rule_head_symbol((4, (r, 2), t_328), 0, node)
  rule_formal_atom_in_body((4, memt_elt, (t_341, t_327)), ((4, memt_elt, (t_341, t_327)), ((), (1, 0))))
  rule_formal_atom_in_body((4, memt_elt, (t_341, t_329)), ((4, memt_elt, (t_341, t_329)), ((), (1, 1))))
  rule_formal_atom_in_body((4, memt_elt, (t_341, t_337)), ((4, memt_elt, (t_341, t_337)), ((), (1, 2))))
  rule_formal_atom_in_body((4, memt_elt, (t_342, t_320)), ((4, memt_elt, (t_342, t_320)), ((), (1, 1))))
  rule_formal_atom_in_body((4, memt_elt, (t_342, t_321)), ((4, memt_elt, (t_342, t_321)), ((), (1, 1))))
  rule_formal_atom_in_body((4, memt_elt, (t_342, t_326)), ((4, memt_elt, (t_342, t_326)), ((), (1, 2))))
  rule_formal_atom_in_body((4, memt_elt, (t_342, t_339)), ((4, memt_elt, (t_342, t_339)), ((), (1, 0))))
  rule_formal_atom_in_body((4, flip, (t_327, t_327)), ((4, flip, (t_327, t_327)), ((), (0, 0))))
  rule_formal_atom_in_body((4, flip, (t_327, t_327)), ((4, flip, (t_327, t_327)), (((), (0, 1)), (1, 1))))
  rule_formal_atom_in_body((4, flip, (t_327, t_327)), ((4, flip, (t_327, t_327)), ((), (1, 0))))
  rule_formal_atom_in_body((4, flip, (t_333, t_338)), ((4, flip, (t_333, t_338)), ((), (0, 0))))
  rule_formal_atom_in_body((4, flip, (t_333, t_338)), ((4, flip, (t_333, t_338)), (((), (0, 1)), (1, 2))))
  rule_formal_atom_in_body((4, flip, (t_333, t_338)), ((4, flip, (t_333, t_338)), (((), (0, 2)), (1, 1))))
  rule_formal_atom_in_body((4, flip, (t_333, t_338)), ((4, flip, (t_333, t_338)), ((), (1, 0))))
  rule_formal_atom_in_body((4, (r, 2), t_328), ((4, (r, 2), t_328), ((), (0, 1))))
  rule_formal_atom_in_body((4, (r, 2), t_335), ((4, (r, 2), t_335), ((), (0, 2))))
  rule_formal_atom_in_body((4, (r, 1), t_325), ((4, (r, 1), t_325), ((), (0, 1))))
  rule_formal_atom_in_body((4, (r, 1), t_326), ((4, (r, 1), t_326), ((), (0, 2))))
  rule_formal_atom_in_body((4, (r, 2), t_327), ((4, (r, 2), t_327), ((), (0, 0))))
  rule_formal_atom_in_body((4, (r, 1), t_339), ((4, (r, 1), t_339), ((), (0, 0))))
  relation((r, 4), 1)
  formal_atom_parameter(((4, memt_elt, (t_341, t_327)), ((), (1, 0))), 0, (1, 0))
  formal_atom_parameter(((4, memt_elt, (t_341, t_329)), ((), (1, 1))), 0, (1, 1))
  formal_atom_parameter(((4, memt_elt, (t_341, t_337)), ((), (1, 2))), 0, (1, 2))
  formal_atom_parameter(((4, memt_elt, (t_342, t_320)), ((), (1, 1))), 0, (1, 1))
  formal_atom_parameter(((4, memt_elt, (t_342, t_321)), ((), (1, 1))), 0, (1, 1))
  formal_atom_parameter(((4, memt_elt, (t_342, t_326)), ((), (1, 2))), 0, (1, 2))
  formal_atom_parameter(((4, memt_elt, (t_342, t_339)), ((), (1, 0))), 0, (1, 0))
  formal_atom_parameter(((4, flip, (t_327, t_327)), ((), (0, 0))), 0, (0, 0))
  formal_atom_parameter(((4, flip, (t_327, t_327)), (((), (0, 1)), (1, 1))), 0, (0, 1))
  formal_atom_parameter(((4, flip, (t_327, t_327)), (((), (0, 1)), (1, 1))), 1, (1, 1))
  formal_atom_parameter(((4, flip, (t_327, t_327)), ((), (1, 0))), 0, (1, 0))
  formal_atom_parameter(((4, flip, (t_333, t_338)), ((), (0, 0))), 0, (0, 0))
  formal_atom_parameter(((4, flip, (t_333, t_338)), (((), (0, 1)), (1, 2))), 0, (0, 1))
  formal_atom_parameter(((4, flip, (t_333, t_338)), (((), (0, 1)), (1, 2))), 1, (1, 2))
  formal_atom_parameter(((4, flip, (t_333, t_338)), (((), (0, 2)), (1, 1))), 0, (0, 2))
  formal_atom_parameter(((4, flip, (t_333, t_338)), (((), (0, 2)), (1, 1))), 1, (1, 1))
  formal_atom_parameter(((4, flip, (t_333, t_338)), ((), (1, 0))), 0, (1, 0))
  formal_atom_parameter(((4, (r, 2), t_328), ((), (0, 1))), 0, (0, 1))
  formal_atom_parameter(((4, (r, 2), t_335), ((), (0, 2))), 0, (0, 2))
  formal_atom_parameter(((4, (r, 1), t_325), ((), (0, 1))), 0, (0, 1))
  formal_atom_parameter(((4, (r, 1), t_326), ((), (0, 2))), 0, (0, 2))
  formal_atom_parameter(((4, (r, 2), t_327), ((), (0, 0))), 0, (0, 0))
  formal_atom_parameter(((4, (r, 1), t_339), ((), (0, 0))), 0, (0, 0))
  relation_type((r, 4), 0, elt) |}]

(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* *)
(* let%expect_test "mainn" =
   let filename =
     "/home/tlosekoo/Documents/THESIS/these_losekoot/auto-forestation/code/model_inference/files_for_test/shallower_model_to_infer.lp"
   in
   let filename_content_result = Inout.Reader.get_all_lines_from_file filename in
   let filename_str =
     match filename_content_result with
     | Ok lines -> List.fold_left (fun s -> ( ^ ) (s ^ "\n")) "" lines
     | Error e -> raise (Failure e)
   in
   (* let () = Format.fprintf Format.std_formatter "%s" filename_str in *)
   let time_limit = 30 in
   let shallower =
     Term.Typed_relation_symbol.create (Term.Symbol.create "shallower")
       [Database_for_tests.nattree; Database_for_tests.nat]
   in
   let leq =
     Term.Typed_relation_symbol.create (Term.Symbol.create "leq") Database_for_tests.c_nat_nat
   in
   let final_relations = Set_typed_relation_symbol.of_list [shallower; leq] in
   let pp c = Format.fprintf c "%s" filename_str in

   let output_solver =
     Io_clingo.Use_clingo_as_solver.run_analysis ~time_limit
       ~path_temporary_folder:Paths.temporary_folder Shocs_finder_clingo.model_finder_analysis pp
   in

   let _clingo_output, _rrs, model_tription =
     compute_all_from_solver_output final_relations output_solver
   in
   let model = Option.get (Misc.Tription.get_yes model_tription) in


   (* type*)
   let elt = Datatype.create "elt" in
   let elt = Datatype.create "elt" in
   (* variables *)
   let t2 = Pattern.from_var (Typed_symbol.create (Symbol.create "t2") Database_for_tests.nattree) in
   let m = Pattern.from_var (Typed_symbol.create (Symbol.create "m") Database_for_tests.nat) in
   let t1 = Pattern.from_var (Typed_symbol.create (Symbol.create "t1") Database_for_tests.nattree) in
   let e = Pattern.from_var (Typed_symbol.create (Symbol.create "e") elt) in
   let shallower_t2_m = Atom_patterns.create_from_predicate_symbol shallower [t2; m] in
   let shallower_t1_m = Atom_patterns.create_from_predicate_symbol shallower [t1; m] in
   let shallower_nodeat1t2_sm =
     Atom_patterns.create_from_predicate_symbol shallower
       [
         Pattern.create Database_for_tests.node_symbol [e; t1; t2];
         Pattern.create Database_for_tests.s_symbol [m];
       ]
   in
   let property_to_verify =
     Disjunctive_clause_patterns.from_body_and_head
       ~body:[shallower_t1_m; shallower_nodeat1t2_sm]
       ~head:[shallower_t2_m]
   in

   let () =
     Format.fprintf Format.std_formatter "Model:\n%a\nProperty to verify :\n%a\n" ModelPrrs.pp model
       Disjunctive_clause_patterns.pp property_to_verify
   in

   (* shallower(t2, m) <= shallower(t1, m) /\ shallower(node(e, t1, t2), s(m)) *)
   ();
   [%expect
     "
     Model:
     |_

     leq ->
     [
     Final relation is
     leq : <nat * nat>

     Transitions:
     {
     leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
     leq(z, s(x_1_0)) <= True
     leq(z, z) <= True
     shallower(leaf, s(x_1_0)) <= True
     shallower(leaf, z) <= True
     shallower(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= shallower(x_0_1, x_1_0) /\\ shallower(x_0_2, x_1_0)
     }
     ]


     ;
     shallower ->
     [
     Final relation is
     shallower : <nattree * nat>

     Transitions:
     {
     leq(s(x_0_0), s(x_1_0)) <= leq(x_0_0, x_1_0)
     leq(z, s(x_1_0)) <= True
     leq(z, z) <= True
     shallower(leaf, s(x_1_0)) <= True
     shallower(leaf, z) <= True
     shallower(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= shallower(x_0_1, x_1_0) /\\ shallower(x_0_2, x_1_0)
     }
     ]



     --
     Equality automata are defined for: {elt, nat, natlist, nattree, nattsil, pair_nat_nat}
     _|"] *) *)
