open Model_inference
open Clause.Aliases

(* open Printer_clingo_automata_test *)

(* let%expect_test "model reduction printing" =
     (snd
        (Model_labelling_automaton.model_generalisation_into_clingo ~_verbose:false model_list_length))
       state_constraints Format.std_formatter;
     [%expect
       "
             symbol(cons).
             symbol(leaf).
             symbol(lin).
             symbol(nil).
             symbol(node).
             symbol(pair).
             symbol(s).
             symbol(snoc).
             symbol(z).

             state((a_0, q_n)).
             state_sort((a_0, q_n), s_5).
             state((a_0, q_l_n)).
             state_sort((a_0, q_l_n), s_6).


             :- not final(length, (a_0, q_l_n)).
             :-  final(length, (a_0, q_l_n)), not final(length, (a_0, q_n)).
             :-  final(length, (a_0, q_n)).
             {final(length, (a_0, q_l_n))}."]

   let%expect_test "model reduction printing" =
     (snd
        (Model_labelling_automaton.model_generalisation_into_clingo ~_verbose:false
           model_length_and_odd_and_nat__automaton))
       state_constraints_ Format.std_formatter;
     [%expect
       "
             symbol(cons).
             symbol(leaf).
             symbol(lin).
             symbol(nil).
             symbol(node).
             symbol(pair).
             symbol(s).
             symbol(snoc).
             symbol(z).

             state((a_0, q_n)).
             state_sort((a_0, q_n), s_12).


             state((a_3, q_n)).
             state_sort((a_3, q_n), s_13).
             state((a_3, q_l_n)).
             state_sort((a_3, q_l_n), s_14).


             state((a_8, qe_n)).
             state((a_8, qo_n)).
             state_sort((a_8, qe_n), s_15).
             state_sort((a_8, qo_n), s_15).

             not not merged((a_8, qe_n),(a_8, qo_n)) :- merged((a_8, qo_n),(a_8, qe_n)).

             {final(isnat, (a_0, q_n))}.
             {final(length, (a_3, q_l_n))}.
             {final(odd, (a_8, qo_n))}.
             {final(odd, (a_8, qe_n))}."]

   let%expect_test "model reduction all solutions" =
     Printing.pp_list_sep "\n\n"
       (Printing.pp_binding Printing.pp_string Io_clingo.Use_clingo_as_solver.pp_solution)
       Format.std_formatter
       (solution_for_states_constraints ());
     [%expect
       "
              -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))

             1 -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))
             final(length, (a_0, q_l_n))

             2 -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))

             3 -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))

             12 -> No: ()

             13 -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))
             final(length, (a_0, q_l_n))

             23 -> Yes: mapped_to((a_0, q_n), (a_0, q_n))
             mapped_to((a_0, q_l_n), (a_0, q_l_n))

             123 -> No: ()"] *)

(*

   *)

(* let find_clingo_output_for_system (ground_clause_system : Set_clause_patterns.t) :
       Shocs_finder_clingo.tription_outcome =
     let time_limit = 60 in
     let _debug_mode = false in
     (* let env = testing_datataype_environment in *)
     Shocs_finder_clingo.run_clingo_model_finder ~time_limit ground_clause_system

   let compute_all_from_solver_output
       (final_relations : Set_typed_relation_symbol.t)
       (clingo_outcome : Shocs_finder_clingo.tription_outcome) =
     let rrs =
       clingo_outcome
       |> Misc.Tription.map_yes
            (Shocs_finder_clingo.from_clingo_output_to_rrs testing_datataype_environment)
     in
     let model =
       rrs
       |> Misc.Tription.map_yes
            (Shocs_finder_clingo.from_rrs_to_model testing_datataype_environment final_relations)
     in
     (clingo_outcome, rrs, model)

   let find_output_and_rrs_and_model_for_system
       (final_relations : Set_typed_relation_symbol.t)
       (ground_clause_system : Set_clause_patterns.t) :
       Shocs_finder_clingo.tription_outcome
       * Shocs_finder_clingo.tription_rrs
       * Shocs_finder_clingo.tription_model =
     let clingo_outcome = find_clingo_output_for_system ground_clause_system in
     compute_all_from_solver_output final_relations clingo_outcome *)

let get_testing_stuff (ground_clause_system : Set_clause_patterns.t) :
    Shocs_finder_clingo.testing_stuff =
  let time_limit = 30 in
  let env = Term.Database_for_tests.testing_type_env in
  Shocs_finder_clingo.get_testing_stuff ~additional_clingo_options:"--seed=0" ~time_limit env
    ground_clause_system

let present_problem_and_solution (ground_clause_system : Set_clause_patterns.t) : unit =
  let () = Printer_clingo_shocs_test.printing_instance ground_clause_system in
  let () = Fresh_label_generator_shocs.reset () in
  let testing_stuff = get_testing_stuff ground_clause_system in
  let sorted_clingo_output =
    Misc.Tription.map_yes
      (List.sort Io_clingo.Output_facts.Fact.compare)
      testing_stuff.clingo_output
  in
  let () =
    Format.fprintf Format.std_formatter
      "---------Clingo output:\n%a\n\n---------SHoCs finding:\n%a\n\n"
      Io_clingo.Use_clingo_as_solver.pp_solution sorted_clingo_output
      (Misc.Tription.pp Tree_tuple_formalisms.Shocs.pp
         Io_clingo.Use_clingo_as_solver.pp_stopping_reason Misc.Unit.pp)
      testing_stuff.shocs_output
  in
  ()

let%expect_test "model finding 1" =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_1;
  [%expect{|
    -------Clauses:
    {

    }

    ---------Clingo clauses:
    #const imax=1.


    ---------Clingo output:
    Yes:

    ---------SHoCs finding:
    Yes: {

    } |}]

let%expect_test "model finding 2 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_2;

  [%expect
    {|
       -------Clauses:
       {
       r_l(nil) <= True
       }

       ---------Clingo clauses:
       #const imax=2.

       function(nil, natlist, 0).
       initial_relation(r_l, 1).
       relation_types(r_l, (natlist)).
       input_atom(r_l, (t_0)).
       input_litteral(l_1, positive, r_l, (t_0)).
       ground_clause(c_2).
       ground_clause_litteral(c_2, (l_1)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple_root_functions((t_0), (nil)).
       function_tuple_i((nil), 0, nil).
       function_tuple_types((nil), (natlist)).
       type_tuple((natlist), 1).
       type_tuple_i((natlist), 0, natlist).

       ---------Clingo output:
       Yes: relation(r_l, 1)
       relation_type(r_l, 0, natlist)
       rule((0, r_l, t_0), r_l)
       rule_head_symbol((0, r_l, t_0), 0, nil)

       ---------SHoCs finding:
       Yes: {
         r_l(nil) <= True
       } |}]

let%expect_test "model finding 3 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_3;

  [%expect
    {|
       -------Clauses:
       {
       r_l_n(nil, z) <= True
       }

       ---------Clingo clauses:
       #const imax=3.

       function(nil, natlist, 0).
       function(z, nat, 0).
       initial_relation(r_l_n, 2).
       relation_types(r_l_n, (natlist,nat)).
       input_atom(r_l_n, (t_1, t_0)).
       input_litteral(l_2, positive, r_l_n, (t_1, t_0)).
       ground_clause(c_3).
       ground_clause_litteral(c_3, (l_2)).
       term_tuple((t_1, t_0), 2).
       term_tuple_i((t_1, t_0), 0, t_1).
       term_tuple_i((t_1, t_0), 1, t_0).
       term_tuple_root_functions((t_1, t_0), (nil,z)).
       function_tuple_i((nil,z), 0, nil).
       function_tuple_i((nil,z), 1, z).
       function_tuple_types((nil,z), (natlist,nat)).
       type_tuple((natlist,nat), 2).
       type_tuple_i((natlist,nat), 0, natlist).
       type_tuple_i((natlist,nat), 1, nat).

       ---------Clingo output:
       Yes: relation(r_l_n, 2)
       relation_type(r_l_n, 0, natlist)
       relation_type(r_l_n, 1, nat)
       rule((0, r_l_n, (t_1, t_0)), r_l_n)
       rule_head_symbol((0, r_l_n, (t_1, t_0)), 0, nil)
       rule_head_symbol((0, r_l_n, (t_1, t_0)), 1, z)

       ---------SHoCs finding:
       Yes: {
         r_l_n(nil, z) <= True
       } |}]

let%expect_test "model finding 4 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_4;

  [%expect
    {|
       -------Clauses:
       {
       r_n(s(z)) <= True
       }

       ---------Clingo clauses:
       #const imax=3.

       function(s, nat, 1).
       function(z, nat, 0).
       initial_relation(r_n, 1).
       relation_types(r_n, (nat)).
       input_atom(r_n, (t_1)).
       input_litteral(l_2, positive, r_n, (t_1)).
       ground_clause(c_3).
       ground_clause_litteral(c_3, (l_2)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       projection_of_terms((t_1),((), (0,0)),(t_0)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       term_tuple_root_functions((t_0), (z)).
       term_tuple_root_functions((t_1), (s)).
       function_tuple_i((s), 0, s).
       function_tuple_i((z), 0, z).
       function_tuple_types((s), (nat)).
       function_tuple_types((z), (nat)).
       type_tuple((nat), 1).
       type_tuple_i((nat), 0, nat).

       ---------Clingo output:
       Yes: relation(r_n, 1)
       relation_type(r_n, 0, nat)
       rule((0, r_n, t_1), r_n)
       rule_head_symbol((0, r_n, t_1), 0, s)

       ---------SHoCs finding:
       Yes: {
         r_n(s(x_0_0)) <= True
       } |}]

let%expect_test "model finding 5 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_5;

  [%expect
    {|
       -------Clauses:
       {
       False <= r_n(z)
       }

       ---------Clingo clauses:
       #const imax=2.

       function(z, nat, 0).
       initial_relation(r_n, 1).
       relation_types(r_n, (nat)).
       input_atom(r_n, (t_0)).
       input_litteral(l_1, negative, r_n, (t_0)).
       ground_clause(c_2).
       ground_clause_litteral(c_2, (l_1)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple_root_functions((t_0), (z)).
       function_tuple_i((z), 0, z).
       function_tuple_types((z), (nat)).
       type_tuple((nat), 1).
       type_tuple_i((nat), 0, nat).

       ---------Clingo output:
       Yes: relation(r_n, 1)
       relation_type(r_n, 0, nat)

       ---------SHoCs finding:
       Yes: {

       } |}]

let%expect_test "model finding 11 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_11;

  [%expect
    {|
       -------Clauses:
       {
       r_c(c_f(c_f(c_a))) <= True
       }

       ---------Clingo clauses:
       #const imax=4.

       function(c_a, custom, 0).
       function(c_f, custom, 1).
       initial_relation(r_c, 1).
       relation_types(r_c, (custom)).
       input_atom(r_c, (t_0)).
       input_litteral(l_3, positive, r_c, (t_0)).
       ground_clause(c_4).
       ground_clause_litteral(c_4, (l_3)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_2), 1).
       term_tuple_i((t_2), 0, t_2).
       projection_of_terms((t_1),((), (0,0)),(t_2)).
       projection_of_terms((t_0),((), (0,0)),(t_1)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       term_tuple_root_functions((t_0), (c_f)).
       term_tuple_root_functions((t_1), (c_f)).
       term_tuple_root_functions((t_2), (c_a)).
       function_tuple_i((c_a), 0, c_a).
       function_tuple_i((c_f), 0, c_f).
       function_tuple_types((c_a), (custom)).
       function_tuple_types((c_f), (custom)).
       type_tuple((custom), 1).
       type_tuple_i((custom), 0, custom).

       ---------Clingo output:
       Yes: relation(r_c, 1)
       relation_type(r_c, 0, custom)
       rule((0, r_c, t_0), r_c)
       rule_head_symbol((0, r_c, t_0), 0, c_f)

       ---------SHoCs finding:
       Yes: {
         r_c(c_f(x_0_0)) <= True
       } |}]

let%expect_test "model finding 12 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_12;

  [%expect
    {|
    -------Clauses:
    {
    r_c(c_f(c_b)) <= True
    False <= r_c(c_f(c_a))
    }

    ---------Clingo clauses:
    #const imax=5.

    function(c_a, custom, 0).
    function(c_b, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_1)).
    input_litteral(l_5, negative, r_c, (t_1)).
    input_atom(r_c, (t_0)).
    input_litteral(l_4, positive, r_c, (t_0)).
    ground_clause(c_7).
    ground_clause_litteral(c_7, (l_4)).
    ground_clause(c_6).
    ground_clause_litteral(c_6, (l_5)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    term_tuple((t_3), 1).
    term_tuple_i((t_3), 0, t_3).
    projection_of_terms((t_1),((), (0,0)),(t_3)).
    projection_of_terms((t_0),((), (0,0)),(t_2)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_b)).
    term_tuple_root_functions((t_3), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_b), 0, c_b).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_b), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom).

    ---------Clingo output:
    Yes: formal_atom(((0, r_c, t_0), ((), (0, 0))), r_c)
    formal_atom_parameter(((0, r_c, t_0), ((), (0, 0))), 0, (0, 0))
    relation(r_c, 1)
    relation_type(r_c, 0, custom)
    rule((0, r_c, t_0), r_c)
    rule((0, r_c, t_2), r_c)
    rule_formal_atom_in_body((0, r_c, t_0), ((0, r_c, t_0), ((), (0, 0))))
    rule_head_symbol((0, r_c, t_0), 0, c_f)
    rule_head_symbol((0, r_c, t_2), 0, c_b)

    ---------SHoCs finding:
    Yes: {
      r_c(c_b) <= True
      r_c(c_f(x_0_0)) <= r_c(x_0_0)
    } |}]

let%expect_test "model finding 13 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_13;

  [%expect
    {|
    -------Clauses:
    {
    r_c(c_f(c_f(c_a))) <= True
    False <= r_c(c_f(c_a))
    }

    ---------Clingo clauses:
    #const imax=4.

    function(c_a, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_1)).
    input_litteral(l_4, negative, r_c, (t_1)).
    input_atom(r_c, (t_0)).
    input_litteral(l_3, positive, r_c, (t_0)).
    ground_clause(c_6).
    ground_clause_litteral(c_6, (l_3)).
    ground_clause(c_5).
    ground_clause_litteral(c_5, (l_4)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    projection_of_terms((t_1),((), (0,0)),(t_2)).
    projection_of_terms((t_0),((), (0,0)),(t_1)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom).

    ---------Clingo output:
    Yes: formal_atom(((1, r_c, t_0), ((), (0, 0))), (r, 1))
    formal_atom_parameter(((1, r_c, t_0), ((), (0, 0))), 0, (0, 0))
    relation(r_c, 1)
    relation((r, 1), 1)
    relation_type(r_c, 0, custom)
    relation_type((r, 1), 0, custom)
    rule((1, r_c, t_0), r_c)
    rule((1, (r, 1), t_1), (r, 1))
    rule_formal_atom_in_body((1, r_c, t_0), ((1, r_c, t_0), ((), (0, 0))))
    rule_head_symbol((1, r_c, t_0), 0, c_f)
    rule_head_symbol((1, (r, 1), t_1), 0, c_f)

    ---------SHoCs finding:
    Yes: {
      _r_1(c_f(x_0_0)) <= True
      r_c(c_f(x_0_0)) <= _r_1(x_0_0)
    } |}]

let%expect_test "model finding even_f " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_even_f;

  [%expect
    {|
    -------Clauses:
    {
    r_c(c_a) <= True
    r_c(c_f(c_f(c_a))) <= True
    r_c(c_f(c_f(c_f(c_f(c_a))))) <= True
    False <= r_c(c_f(c_a))
    False <= r_c(c_f(c_f(c_f(c_a))))
    }

    ---------Clingo clauses:
    #const imax=6.

    function(c_a, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_3)).
    input_litteral(l_9, negative, r_c, (t_3)).
    input_atom(r_c, (t_1)).
    input_litteral(l_8, negative, r_c, (t_1)).
    input_atom(r_c, (t_4)).
    input_litteral(l_7, positive, r_c, (t_4)).
    input_atom(r_c, (t_2)).
    input_litteral(l_6, positive, r_c, (t_2)).
    input_atom(r_c, (t_0)).
    input_litteral(l_5, positive, r_c, (t_0)).
    ground_clause(c_14).
    ground_clause_litteral(c_14, (l_7)).
    ground_clause(c_13).
    ground_clause_litteral(c_13, (l_6)).
    ground_clause(c_12).
    ground_clause_litteral(c_12, (l_5)).
    ground_clause(c_11).
    ground_clause_litteral(c_11, (l_9)).
    ground_clause(c_10).
    ground_clause_litteral(c_10, (l_8)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    term_tuple((t_3), 1).
    term_tuple_i((t_3), 0, t_3).
    term_tuple((t_4), 1).
    term_tuple_i((t_4), 0, t_4).
    projection_of_terms((t_3),((), (0,0)),(t_4)).
    projection_of_terms((t_2),((), (0,0)),(t_3)).
    projection_of_terms((t_1),((), (0,0)),(t_2)).
    projection_of_terms((t_0),((), (0,0)),(t_1)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_f)).
    term_tuple_root_functions((t_3), (c_f)).
    term_tuple_root_functions((t_4), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom).

    ---------Clingo output:
    Yes: formal_atom(((1, r_c, t_2), ((), (0, 0))), (r, 1))
    formal_atom(((1, (r, 1), t_3), ((), (0, 0))), r_c)
    formal_atom_parameter(((1, r_c, t_2), ((), (0, 0))), 0, (0, 0))
    formal_atom_parameter(((1, (r, 1), t_3), ((), (0, 0))), 0, (0, 0))
    relation(r_c, 1)
    relation((r, 1), 1)
    relation_type(r_c, 0, custom)
    relation_type((r, 1), 0, custom)
    rule((1, r_c, t_2), r_c)
    rule((1, r_c, t_4), r_c)
    rule((1, (r, 1), t_3), (r, 1))
    rule_formal_atom_in_body((1, r_c, t_2), ((1, r_c, t_2), ((), (0, 0))))
    rule_formal_atom_in_body((1, (r, 1), t_3), ((1, (r, 1), t_3), ((), (0, 0))))
    rule_head_symbol((1, r_c, t_2), 0, c_f)
    rule_head_symbol((1, r_c, t_4), 0, c_a)
    rule_head_symbol((1, (r, 1), t_3), 0, c_f)

    ---------SHoCs finding:
    Yes: {
      _r_1(c_f(x_0_0)) <= r_c(x_0_0)
      r_c(c_a) <= True
      r_c(c_f(x_0_0)) <= _r_1(x_0_0)
    } |}]

let%expect_test "model finding even_f2 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_even_f2;

  [%expect
    {|
       -------Clauses:
       {
       r_c(c_a) <= True
       r_c(c_f(c_f(c_a))) <= r_c(c_a)
       False <= r_c(c_a) /\ r_c(c_f(c_a))
       }

       ---------Clingo clauses:
       #const imax=4.

       function(c_a, custom, 0).
       function(c_f, custom, 1).
       initial_relation(r_c, 1).
       relation_types(r_c, (custom)).
       input_atom(r_c, (t_2)).
       input_litteral(l_6, negative, r_c, (t_2)).
       input_atom(r_c, (t_1)).
       input_litteral(l_5, negative, r_c, (t_1)).
       input_atom(r_c, (t_2)).
       input_litteral(l_4, positive, r_c, (t_2)).
       input_atom(r_c, (t_0)).
       input_litteral(l_3, positive, r_c, (t_0)).
       ground_clause(c_9).
       ground_clause_litteral(c_9, (l_4)).
       ground_clause(c_8).
       ground_clause_litteral(c_8, (l_6;l_3)).
       ground_clause(c_7).
       ground_clause_litteral(c_7, (l_5;l_6)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_2), 1).
       term_tuple_i((t_2), 0, t_2).
       projection_of_terms((t_1),((), (0,0)),(t_2)).
       projection_of_terms((t_0),((), (0,0)),(t_1)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       term_tuple_root_functions((t_0), (c_f)).
       term_tuple_root_functions((t_1), (c_f)).
       term_tuple_root_functions((t_2), (c_a)).
       function_tuple_i((c_a), 0, c_a).
       function_tuple_i((c_f), 0, c_f).
       function_tuple_types((c_a), (custom)).
       function_tuple_types((c_f), (custom)).
       type_tuple((custom), 1).
       type_tuple_i((custom), 0, custom).

       ---------Clingo output:
       Yes: formal_atom(((1, r_c, t_0), ((), (0, 0))), (r, 1))
       formal_atom_parameter(((1, r_c, t_0), ((), (0, 0))), 0, (0, 0))
       relation(r_c, 1)
       relation((r, 1), 1)
       relation_type(r_c, 0, custom)
       relation_type((r, 1), 0, custom)
       rule((1, r_c, t_0), r_c)
       rule((1, r_c, t_2), r_c)
       rule((1, (r, 1), t_1), (r, 1))
       rule_formal_atom_in_body((1, r_c, t_0), ((1, r_c, t_0), ((), (0, 0))))
       rule_head_symbol((1, r_c, t_0), 0, c_f)
       rule_head_symbol((1, r_c, t_2), 0, c_a)
       rule_head_symbol((1, (r, 1), t_1), 0, c_f)

       ---------SHoCs finding:
       Yes: {
         _r_1(c_f(x_0_0)) <= True
         r_c(c_a) <= True
         r_c(c_f(x_0_0)) <= _r_1(x_0_0)
       } |}]

let%expect_test "model finding even_f3 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_even_f3;

  [%expect
    {|
       -------Clauses:
       {
       r_c(c_a) <= True
       r_c(c_f(c_f(c_a))) <= True
       r_c(c_f(c_f(c_f(c_f(c_a))))) <= True
       r_c(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_f(c_a))))))))))))))) <= True
       False <= r_c(c_f(c_a))
       False <= r_c(c_f(c_f(c_f(c_a))))
       }

       ---------Clingo clauses:
       #const imax=16.

       function(c_a, custom, 0).
       function(c_f, custom, 1).
       initial_relation(r_c, 1).
       relation_types(r_c, (custom)).
       input_atom(r_c, (t_13)).
       input_litteral(l_20, negative, r_c, (t_13)).
       input_atom(r_c, (t_11)).
       input_litteral(l_19, negative, r_c, (t_11)).
       input_atom(r_c, (t_14)).
       input_litteral(l_18, positive, r_c, (t_14)).
       input_atom(r_c, (t_12)).
       input_litteral(l_17, positive, r_c, (t_12)).
       input_atom(r_c, (t_10)).
       input_litteral(l_16, positive, r_c, (t_10)).
       input_atom(r_c, (t_0)).
       input_litteral(l_15, positive, r_c, (t_0)).
       ground_clause(c_26).
       ground_clause_litteral(c_26, (l_18)).
       ground_clause(c_25).
       ground_clause_litteral(c_25, (l_17)).
       ground_clause(c_24).
       ground_clause_litteral(c_24, (l_16)).
       ground_clause(c_23).
       ground_clause_litteral(c_23, (l_15)).
       ground_clause(c_22).
       ground_clause_litteral(c_22, (l_20)).
       ground_clause(c_21).
       ground_clause_litteral(c_21, (l_19)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_2), 1).
       term_tuple_i((t_2), 0, t_2).
       term_tuple((t_3), 1).
       term_tuple_i((t_3), 0, t_3).
       term_tuple((t_4), 1).
       term_tuple_i((t_4), 0, t_4).
       term_tuple((t_5), 1).
       term_tuple_i((t_5), 0, t_5).
       term_tuple((t_6), 1).
       term_tuple_i((t_6), 0, t_6).
       term_tuple((t_7), 1).
       term_tuple_i((t_7), 0, t_7).
       term_tuple((t_8), 1).
       term_tuple_i((t_8), 0, t_8).
       term_tuple((t_9), 1).
       term_tuple_i((t_9), 0, t_9).
       term_tuple((t_10), 1).
       term_tuple_i((t_10), 0, t_10).
       term_tuple((t_11), 1).
       term_tuple_i((t_11), 0, t_11).
       term_tuple((t_12), 1).
       term_tuple_i((t_12), 0, t_12).
       term_tuple((t_13), 1).
       term_tuple_i((t_13), 0, t_13).
       term_tuple((t_14), 1).
       term_tuple_i((t_14), 0, t_14).
       projection_of_terms((t_13),((), (0,0)),(t_14)).
       projection_of_terms((t_12),((), (0,0)),(t_13)).
       projection_of_terms((t_11),((), (0,0)),(t_12)).
       projection_of_terms((t_10),((), (0,0)),(t_11)).
       projection_of_terms((t_9),((), (0,0)),(t_10)).
       projection_of_terms((t_8),((), (0,0)),(t_9)).
       projection_of_terms((t_7),((), (0,0)),(t_8)).
       projection_of_terms((t_6),((), (0,0)),(t_7)).
       projection_of_terms((t_5),((), (0,0)),(t_6)).
       projection_of_terms((t_4),((), (0,0)),(t_5)).
       projection_of_terms((t_3),((), (0,0)),(t_4)).
       projection_of_terms((t_2),((), (0,0)),(t_3)).
       projection_of_terms((t_1),((), (0,0)),(t_2)).
       projection_of_terms((t_0),((), (0,0)),(t_1)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       term_tuple_root_functions((t_0), (c_f)).
       term_tuple_root_functions((t_1), (c_f)).
       term_tuple_root_functions((t_10), (c_f)).
       term_tuple_root_functions((t_11), (c_f)).
       term_tuple_root_functions((t_12), (c_f)).
       term_tuple_root_functions((t_13), (c_f)).
       term_tuple_root_functions((t_14), (c_a)).
       term_tuple_root_functions((t_2), (c_f)).
       term_tuple_root_functions((t_3), (c_f)).
       term_tuple_root_functions((t_4), (c_f)).
       term_tuple_root_functions((t_5), (c_f)).
       term_tuple_root_functions((t_6), (c_f)).
       term_tuple_root_functions((t_7), (c_f)).
       term_tuple_root_functions((t_8), (c_f)).
       term_tuple_root_functions((t_9), (c_f)).
       function_tuple_i((c_a), 0, c_a).
       function_tuple_i((c_f), 0, c_f).
       function_tuple_types((c_a), (custom)).
       function_tuple_types((c_f), (custom)).
       type_tuple((custom), 1).
       type_tuple_i((custom), 0, custom).

       ---------Clingo output:
       Yes: formal_atom(((1, r_c, t_0), ((), (0, 0))), (r, 1))
       formal_atom(((1, (r, 1), t_1), ((), (0, 0))), r_c)
       formal_atom_parameter(((1, r_c, t_0), ((), (0, 0))), 0, (0, 0))
       formal_atom_parameter(((1, (r, 1), t_1), ((), (0, 0))), 0, (0, 0))
       relation(r_c, 1)
       relation((r, 1), 1)
       relation_type(r_c, 0, custom)
       relation_type((r, 1), 0, custom)
       rule((1, r_c, t_0), r_c)
       rule((1, r_c, t_14), r_c)
       rule((1, (r, 1), t_1), (r, 1))
       rule_formal_atom_in_body((1, r_c, t_0), ((1, r_c, t_0), ((), (0, 0))))
       rule_formal_atom_in_body((1, (r, 1), t_1), ((1, (r, 1), t_1), ((), (0, 0))))
       rule_head_symbol((1, r_c, t_0), 0, c_f)
       rule_head_symbol((1, r_c, t_14), 0, c_a)
       rule_head_symbol((1, (r, 1), t_1), 0, c_f)

       ---------SHoCs finding:
       Yes: {
         _r_1(c_f(x_0_0)) <= r_c(x_0_0)
         r_c(c_a) <= True
         r_c(c_f(x_0_0)) <= _r_1(x_0_0)
       } |}]

let%expect_test "model finding length " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_length;

  [%expect
    {|
       -------Clauses:
       {
       length(nil, z) <= True
       length(nil, s(z)) <= length(cons(z, nil), s(s(z)))
       False <= length(nil, s(z))
       length(cons(s(z), nil), s(z)) <= length(nil, z)
       length(cons(z, nil), s(z)) <= length(nil, z)
       }

       ---------Clingo clauses:
       #const imax=7.

       function(cons, natlist, 2).
       function(nil, natlist, 0).
       function(s, nat, 1).
       function(z, nat, 0).
       initial_relation(length, 2).
       relation_types(length, (natlist,nat)).
       input_atom(length, (t_4, t_2)).
       input_litteral(l_12, negative, length, (t_4, t_2)).
       input_atom(length, (t_3, t_1)).
       input_litteral(l_11, negative, length, (t_3, t_1)).
       input_atom(length, (t_3, t_0)).
       input_litteral(l_10, negative, length, (t_3, t_0)).
       input_atom(length, (t_5, t_1)).
       input_litteral(l_9, positive, length, (t_5, t_1)).
       input_atom(length, (t_4, t_1)).
       input_litteral(l_8, positive, length, (t_4, t_1)).
       input_atom(length, (t_3, t_1)).
       input_litteral(l_7, positive, length, (t_3, t_1)).
       input_atom(length, (t_3, t_0)).
       input_litteral(l_6, positive, length, (t_3, t_0)).
       ground_clause(c_17).
       ground_clause_litteral(c_17, (l_6)).
       ground_clause(c_16).
       ground_clause_litteral(c_16, (l_12;l_7)).
       ground_clause(c_15).
       ground_clause_litteral(c_15, (l_11)).
       ground_clause(c_14).
       ground_clause_litteral(c_14, (l_10;l_9)).
       ground_clause(c_13).
       ground_clause_litteral(c_13, (l_10;l_8)).
       term_tuple((t_0, t_0), 2).
       term_tuple_i((t_0, t_0), 0, t_0).
       term_tuple_i((t_0, t_0), 1, t_0).
       term_tuple((t_0, t_1), 2).
       term_tuple_i((t_0, t_1), 0, t_0).
       term_tuple_i((t_0, t_1), 1, t_1).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1, t_0), 2).
       term_tuple_i((t_1, t_0), 0, t_1).
       term_tuple_i((t_1, t_0), 1, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_3, t_0), 2).
       term_tuple_i((t_3, t_0), 0, t_3).
       term_tuple_i((t_3, t_0), 1, t_0).
       term_tuple((t_3, t_1), 2).
       term_tuple_i((t_3, t_1), 0, t_3).
       term_tuple_i((t_3, t_1), 1, t_1).
       term_tuple((t_3), 1).
       term_tuple_i((t_3), 0, t_3).
       term_tuple((t_4, t_1), 2).
       term_tuple_i((t_4, t_1), 0, t_4).
       term_tuple_i((t_4, t_1), 1, t_1).
       term_tuple((t_4, t_2), 2).
       term_tuple_i((t_4, t_2), 0, t_4).
       term_tuple_i((t_4, t_2), 1, t_2).
       term_tuple((t_5, t_1), 2).
       term_tuple_i((t_5, t_1), 0, t_5).
       term_tuple_i((t_5, t_1), 1, t_1).
       projection_of_terms((t_5, t_1),((), (0,0)),(t_1)).
       projection_of_terms((t_5, t_1),(((), (0,0)), (1,0)),(t_1, t_0)).
       projection_of_terms((t_5, t_1),((), (0,1)),(t_3)).
       projection_of_terms((t_5, t_1),(((), (0,1)), (1,0)),(t_3, t_0)).
       projection_of_terms((t_5, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_4, t_2),((), (0,0)),(t_0)).
       projection_of_terms((t_4, t_2),(((), (0,0)), (1,0)),(t_0, t_1)).
       projection_of_terms((t_4, t_2),((), (0,1)),(t_3)).
       projection_of_terms((t_4, t_2),(((), (0,1)), (1,0)),(t_3, t_1)).
       projection_of_terms((t_4, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_4, t_1),((), (0,0)),(t_0)).
       projection_of_terms((t_4, t_1),(((), (0,0)), (1,0)),(t_0, t_0)).
       projection_of_terms((t_4, t_1),((), (0,1)),(t_3)).
       projection_of_terms((t_4, t_1),(((), (0,1)), (1,0)),(t_3, t_0)).
       projection_of_terms((t_4, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_3, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_1),((), (0,0)),(t_0)).
       projection_of_terms((t_1, t_0),((), (0,0)),(t_0)).
       projection_of_terms((t_0, t_1),((), (1,0)),(t_0)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       projector_tuple((((), (0,0)), (1,0)), 2).
       projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
       projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
       projector_tuple(((), (0,1)), 1).
       projector_tuple_i(((), (0,1)), 0, (0,1)).
       projector_tuple((((), (0,1)), (1,0)), 2).
       projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
       projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
       projector_tuple(((), (1,0)), 1).
       projector_tuple_i(((), (1,0)), 0, (1,0)).
       term_tuple_root_functions((t_0), (z)).
       term_tuple_root_functions((t_0, t_0), (z,z)).
       term_tuple_root_functions((t_0, t_1), (z,s)).
       term_tuple_root_functions((t_1), (s)).
       term_tuple_root_functions((t_1, t_0), (s,z)).
       term_tuple_root_functions((t_3), (nil)).
       term_tuple_root_functions((t_3, t_0), (nil,z)).
       term_tuple_root_functions((t_3, t_1), (nil,s)).
       term_tuple_root_functions((t_4, t_1), (cons,s)).
       term_tuple_root_functions((t_4, t_2), (cons,s)).
       term_tuple_root_functions((t_5, t_1), (cons,s)).
       function_tuple_i((cons,s), 0, cons).
       function_tuple_i((cons,s), 1, s).
       function_tuple_i((nil), 0, nil).
       function_tuple_i((nil,s), 0, nil).
       function_tuple_i((nil,s), 1, s).
       function_tuple_i((nil,z), 0, nil).
       function_tuple_i((nil,z), 1, z).
       function_tuple_i((s), 0, s).
       function_tuple_i((s,z), 0, s).
       function_tuple_i((s,z), 1, z).
       function_tuple_i((z), 0, z).
       function_tuple_i((z,s), 0, z).
       function_tuple_i((z,s), 1, s).
       function_tuple_i((z,z), 0, z).
       function_tuple_i((z,z), 1, z).
       function_tuple_types((cons,s), (natlist,nat)).
       function_tuple_types((nil), (natlist)).
       function_tuple_types((nil,s), (natlist,nat)).
       function_tuple_types((nil,z), (natlist,nat)).
       function_tuple_types((s), (nat)).
       function_tuple_types((s,z), (nat,nat)).
       function_tuple_types((z), (nat)).
       function_tuple_types((z,s), (nat,nat)).
       function_tuple_types((z,z), (nat,nat)).
       type_tuple((nat), 1).
       type_tuple_i((nat), 0, nat).
       type_tuple((nat,nat), 2).
       type_tuple_i((nat,nat), 0, nat).
       type_tuple_i((nat,nat), 1, nat).
       type_tuple((natlist), 1).
       type_tuple_i((natlist), 0, natlist).
       type_tuple((natlist,nat), 2).
       type_tuple_i((natlist,nat), 0, natlist).
       type_tuple_i((natlist,nat), 1, nat).

       ---------Clingo output:
       Yes: formal_atom(((0, length, (t_5, t_1)), (((), (0, 1)), (1, 0))), length)
       formal_atom_parameter(((0, length, (t_5, t_1)), (((), (0, 1)), (1, 0))), 0, (0, 1))
       formal_atom_parameter(((0, length, (t_5, t_1)), (((), (0, 1)), (1, 0))), 1, (1, 0))
       relation(length, 2)
       relation_type(length, 0, natlist)
       relation_type(length, 1, nat)
       rule((0, length, (t_3, t_0)), length)
       rule((0, length, (t_5, t_1)), length)
       rule_formal_atom_in_body((0, length, (t_5, t_1)), ((0, length, (t_5, t_1)), (((), (0, 1)), (1, 0))))
       rule_head_symbol((0, length, (t_3, t_0)), 0, nil)
       rule_head_symbol((0, length, (t_3, t_0)), 1, z)
       rule_head_symbol((0, length, (t_5, t_1)), 0, cons)
       rule_head_symbol((0, length, (t_5, t_1)), 1, s)

       ---------SHoCs finding:
       Yes: {
         length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
         length(nil, z) <= True
       } |}]

let%expect_test "model finding length2 " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_length2;

  [%expect
    {|
        -------Clauses:
        {
        length(nil, z) <= True
        length(nil, s(z)) <= length(cons(z, nil), s(s(z)))
        length(cons(z, cons(z, nil)), s(s(z))) <= length(cons(z, nil), s(z))
        False <= length(nil, s(z))
        length(cons(s(z), nil), s(z)) <= length(nil, z)
        length(cons(z, nil), s(z)) <= length(nil, z)
        }

        ---------Clingo clauses:
        #const imax=8.

        function(cons, natlist, 2).
        function(nil, natlist, 0).
        function(s, nat, 1).
        function(z, nat, 0).
        initial_relation(length, 2).
        relation_types(length, (natlist,nat)).
        input_atom(length, (t_4, t_2)).
        input_litteral(l_15, negative, length, (t_4, t_2)).
        input_atom(length, (t_4, t_1)).
        input_litteral(l_14, negative, length, (t_4, t_1)).
        input_atom(length, (t_3, t_1)).
        input_litteral(l_13, negative, length, (t_3, t_1)).
        input_atom(length, (t_3, t_0)).
        input_litteral(l_12, negative, length, (t_3, t_0)).
        input_atom(length, (t_6, t_1)).
        input_litteral(l_11, positive, length, (t_6, t_1)).
        input_atom(length, (t_5, t_2)).
        input_litteral(l_10, positive, length, (t_5, t_2)).
        input_atom(length, (t_4, t_1)).
        input_litteral(l_9, positive, length, (t_4, t_1)).
        input_atom(length, (t_3, t_1)).
        input_litteral(l_8, positive, length, (t_3, t_1)).
        input_atom(length, (t_3, t_0)).
        input_litteral(l_7, positive, length, (t_3, t_0)).
        ground_clause(c_21).
        ground_clause_litteral(c_21, (l_7)).
        ground_clause(c_20).
        ground_clause_litteral(c_20, (l_15;l_8)).
        ground_clause(c_19).
        ground_clause_litteral(c_19, (l_14;l_10)).
        ground_clause(c_18).
        ground_clause_litteral(c_18, (l_13)).
        ground_clause(c_17).
        ground_clause_litteral(c_17, (l_12;l_11)).
        ground_clause(c_16).
        ground_clause_litteral(c_16, (l_12;l_9)).
        term_tuple((t_0, t_0), 2).
        term_tuple_i((t_0, t_0), 0, t_0).
        term_tuple_i((t_0, t_0), 1, t_0).
        term_tuple((t_0, t_1), 2).
        term_tuple_i((t_0, t_1), 0, t_0).
        term_tuple_i((t_0, t_1), 1, t_1).
        term_tuple((t_0), 1).
        term_tuple_i((t_0), 0, t_0).
        term_tuple((t_1, t_0), 2).
        term_tuple_i((t_1, t_0), 0, t_1).
        term_tuple_i((t_1, t_0), 1, t_0).
        term_tuple((t_1), 1).
        term_tuple_i((t_1), 0, t_1).
        term_tuple((t_3, t_0), 2).
        term_tuple_i((t_3, t_0), 0, t_3).
        term_tuple_i((t_3, t_0), 1, t_0).
        term_tuple((t_3, t_1), 2).
        term_tuple_i((t_3, t_1), 0, t_3).
        term_tuple_i((t_3, t_1), 1, t_1).
        term_tuple((t_3), 1).
        term_tuple_i((t_3), 0, t_3).
        term_tuple((t_4, t_1), 2).
        term_tuple_i((t_4, t_1), 0, t_4).
        term_tuple_i((t_4, t_1), 1, t_1).
        term_tuple((t_4, t_2), 2).
        term_tuple_i((t_4, t_2), 0, t_4).
        term_tuple_i((t_4, t_2), 1, t_2).
        term_tuple((t_4), 1).
        term_tuple_i((t_4), 0, t_4).
        term_tuple((t_5, t_2), 2).
        term_tuple_i((t_5, t_2), 0, t_5).
        term_tuple_i((t_5, t_2), 1, t_2).
        term_tuple((t_6, t_1), 2).
        term_tuple_i((t_6, t_1), 0, t_6).
        term_tuple_i((t_6, t_1), 1, t_1).
        projection_of_terms((t_6, t_1),((), (0,0)),(t_1)).
        projection_of_terms((t_6, t_1),(((), (0,0)), (1,0)),(t_1, t_0)).
        projection_of_terms((t_6, t_1),((), (0,1)),(t_3)).
        projection_of_terms((t_6, t_1),(((), (0,1)), (1,0)),(t_3, t_0)).
        projection_of_terms((t_6, t_1),((), (1,0)),(t_0)).
        projection_of_terms((t_5, t_2),((), (0,0)),(t_0)).
        projection_of_terms((t_5, t_2),(((), (0,0)), (1,0)),(t_0, t_1)).
        projection_of_terms((t_5, t_2),((), (0,1)),(t_4)).
        projection_of_terms((t_5, t_2),(((), (0,1)), (1,0)),(t_4, t_1)).
        projection_of_terms((t_5, t_2),((), (1,0)),(t_1)).
        projection_of_terms((t_4),((), (0,0)),(t_0)).
        projection_of_terms((t_4),((), (0,1)),(t_3)).
        projection_of_terms((t_4, t_2),((), (0,0)),(t_0)).
        projection_of_terms((t_4, t_2),(((), (0,0)), (1,0)),(t_0, t_1)).
        projection_of_terms((t_4, t_2),((), (0,1)),(t_3)).
        projection_of_terms((t_4, t_2),(((), (0,1)), (1,0)),(t_3, t_1)).
        projection_of_terms((t_4, t_2),((), (1,0)),(t_1)).
        projection_of_terms((t_4, t_1),((), (0,0)),(t_0)).
        projection_of_terms((t_4, t_1),(((), (0,0)), (1,0)),(t_0, t_0)).
        projection_of_terms((t_4, t_1),((), (0,1)),(t_3)).
        projection_of_terms((t_4, t_1),(((), (0,1)), (1,0)),(t_3, t_0)).
        projection_of_terms((t_4, t_1),((), (1,0)),(t_0)).
        projection_of_terms((t_3, t_1),((), (1,0)),(t_0)).
        projection_of_terms((t_1),((), (0,0)),(t_0)).
        projection_of_terms((t_1, t_0),((), (0,0)),(t_0)).
        projection_of_terms((t_0, t_1),((), (1,0)),(t_0)).
        projector_tuple(((), (0,0)), 1).
        projector_tuple_i(((), (0,0)), 0, (0,0)).
        projector_tuple((((), (0,0)), (1,0)), 2).
        projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
        projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
        projector_tuple(((), (0,1)), 1).
        projector_tuple_i(((), (0,1)), 0, (0,1)).
        projector_tuple((((), (0,1)), (1,0)), 2).
        projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
        projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
        projector_tuple(((), (1,0)), 1).
        projector_tuple_i(((), (1,0)), 0, (1,0)).
        term_tuple_root_functions((t_0), (z)).
        term_tuple_root_functions((t_0, t_0), (z,z)).
        term_tuple_root_functions((t_0, t_1), (z,s)).
        term_tuple_root_functions((t_1), (s)).
        term_tuple_root_functions((t_1, t_0), (s,z)).
        term_tuple_root_functions((t_3), (nil)).
        term_tuple_root_functions((t_3, t_0), (nil,z)).
        term_tuple_root_functions((t_3, t_1), (nil,s)).
        term_tuple_root_functions((t_4), (cons)).
        term_tuple_root_functions((t_4, t_1), (cons,s)).
        term_tuple_root_functions((t_4, t_2), (cons,s)).
        term_tuple_root_functions((t_5, t_2), (cons,s)).
        term_tuple_root_functions((t_6, t_1), (cons,s)).
        function_tuple_i((cons), 0, cons).
        function_tuple_i((cons,s), 0, cons).
        function_tuple_i((cons,s), 1, s).
        function_tuple_i((nil), 0, nil).
        function_tuple_i((nil,s), 0, nil).
        function_tuple_i((nil,s), 1, s).
        function_tuple_i((nil,z), 0, nil).
        function_tuple_i((nil,z), 1, z).
        function_tuple_i((s), 0, s).
        function_tuple_i((s,z), 0, s).
        function_tuple_i((s,z), 1, z).
        function_tuple_i((z), 0, z).
        function_tuple_i((z,s), 0, z).
        function_tuple_i((z,s), 1, s).
        function_tuple_i((z,z), 0, z).
        function_tuple_i((z,z), 1, z).
        function_tuple_types((cons), (natlist)).
        function_tuple_types((cons,s), (natlist,nat)).
        function_tuple_types((nil), (natlist)).
        function_tuple_types((nil,s), (natlist,nat)).
        function_tuple_types((nil,z), (natlist,nat)).
        function_tuple_types((s), (nat)).
        function_tuple_types((s,z), (nat,nat)).
        function_tuple_types((z), (nat)).
        function_tuple_types((z,s), (nat,nat)).
        function_tuple_types((z,z), (nat,nat)).
        type_tuple((nat), 1).
        type_tuple_i((nat), 0, nat).
        type_tuple((nat,nat), 2).
        type_tuple_i((nat,nat), 0, nat).
        type_tuple_i((nat,nat), 1, nat).
        type_tuple((natlist), 1).
        type_tuple_i((natlist), 0, natlist).
        type_tuple((natlist,nat), 2).
        type_tuple_i((natlist,nat), 0, natlist).
        type_tuple_i((natlist,nat), 1, nat).

        ---------Clingo output:
        Yes: formal_atom(((0, length, (t_6, t_1)), (((), (0, 1)), (1, 0))), length)
        formal_atom_parameter(((0, length, (t_6, t_1)), (((), (0, 1)), (1, 0))), 0, (0, 1))
        formal_atom_parameter(((0, length, (t_6, t_1)), (((), (0, 1)), (1, 0))), 1, (1, 0))
        relation(length, 2)
        relation_type(length, 0, natlist)
        relation_type(length, 1, nat)
        rule((0, length, (t_3, t_0)), length)
        rule((0, length, (t_6, t_1)), length)
        rule_formal_atom_in_body((0, length, (t_6, t_1)), ((0, length, (t_6, t_1)), (((), (0, 1)), (1, 0))))
        rule_head_symbol((0, length, (t_3, t_0)), 0, nil)
        rule_head_symbol((0, length, (t_3, t_0)), 1, z)
        rule_head_symbol((0, length, (t_6, t_1)), 0, cons)
        rule_head_symbol((0, length, (t_6, t_1)), 1, s)

        ---------SHoCs finding:
        Yes: {
          length(cons(x_0_0, x_0_1), s(x_1_0)) <= length(x_0_1, x_1_0)
          length(nil, z) <= True
        } |}]

let%expect_test "model finding shallower " =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_shallower3;

  [%expect
    {|
       -------Clauses:
       {
       shallower(leaf, s(z)) <= True
       shallower(leaf, z) <= True
       shallower(node(leaf, a, leaf), s(s(z))) <= shallower(leaf, s(z))
       shallower(node(leaf, a, leaf), s(z)) <= shallower(leaf, z)
       shallower(node(leaf, a, node(leaf, a, leaf)), s(s(z))) <= shallower(node(leaf, a, leaf), s(z))
       shallower(node(node(leaf, a, leaf), a, leaf), s(s(z))) <= shallower(node(leaf, a, leaf), s(z))
       False <= shallower(node(leaf, a, leaf), z)
       shallower(node(leaf, a, leaf), z) <= shallower(node(leaf, a, node(leaf, a, leaf)), s(z))
       shallower(node(leaf, a, leaf), z) <= shallower(node(node(leaf, a, leaf), a, leaf), s(z))
       }

       ---------Clingo clauses:
       #const imax=9.

       function(a, elt, 0).
       function(leaf, elt_tree, 0).
       function(node, elt_tree, 3).
       function(s, nat, 1).
       function(z, nat, 0).
       initial_relation(shallower, 2).
       relation_types(shallower, (elt_tree,nat)).
       input_atom(shallower, (t_6, t_1)).
       input_litteral(l_20, negative, shallower, (t_6, t_1)).
       input_atom(shallower, (t_6, t_0)).
       input_litteral(l_19, negative, shallower, (t_6, t_0)).
       input_atom(shallower, (t_5, t_1)).
       input_litteral(l_18, negative, shallower, (t_5, t_1)).
       input_atom(shallower, (t_5, t_0)).
       input_litteral(l_17, negative, shallower, (t_5, t_0)).
       input_atom(shallower, (t_4, t_1)).
       input_litteral(l_16, negative, shallower, (t_4, t_1)).
       input_atom(shallower, (t_3, t_1)).
       input_litteral(l_15, negative, shallower, (t_3, t_1)).
       input_atom(shallower, (t_6, t_1)).
       input_litteral(l_14, positive, shallower, (t_6, t_1)).
       input_atom(shallower, (t_6, t_0)).
       input_litteral(l_13, positive, shallower, (t_6, t_0)).
       input_atom(shallower, (t_5, t_2)).
       input_litteral(l_12, positive, shallower, (t_5, t_2)).
       input_atom(shallower, (t_5, t_1)).
       input_litteral(l_11, positive, shallower, (t_5, t_1)).
       input_atom(shallower, (t_5, t_0)).
       input_litteral(l_10, positive, shallower, (t_5, t_0)).
       input_atom(shallower, (t_4, t_2)).
       input_litteral(l_9, positive, shallower, (t_4, t_2)).
       input_atom(shallower, (t_3, t_2)).
       input_litteral(l_8, positive, shallower, (t_3, t_2)).
       ground_clause(c_29).
       ground_clause_litteral(c_29, (l_14)).
       ground_clause(c_28).
       ground_clause_litteral(c_28, (l_13)).
       ground_clause(c_27).
       ground_clause_litteral(c_27, (l_20;l_12)).
       ground_clause(c_26).
       ground_clause_litteral(c_26, (l_19;l_11)).
       ground_clause(c_25).
       ground_clause_litteral(c_25, (l_18;l_9)).
       ground_clause(c_24).
       ground_clause_litteral(c_24, (l_18;l_8)).
       ground_clause(c_23).
       ground_clause_litteral(c_23, (l_17)).
       ground_clause(c_22).
       ground_clause_litteral(c_22, (l_16;l_10)).
       ground_clause(c_21).
       ground_clause_litteral(c_21, (l_15;l_10)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_3, t_1), 2).
       term_tuple_i((t_3, t_1), 0, t_3).
       term_tuple_i((t_3, t_1), 1, t_1).
       term_tuple((t_3, t_2), 2).
       term_tuple_i((t_3, t_2), 0, t_3).
       term_tuple_i((t_3, t_2), 1, t_2).
       term_tuple((t_4, t_1), 2).
       term_tuple_i((t_4, t_1), 0, t_4).
       term_tuple_i((t_4, t_1), 1, t_1).
       term_tuple((t_4, t_2), 2).
       term_tuple_i((t_4, t_2), 0, t_4).
       term_tuple_i((t_4, t_2), 1, t_2).
       term_tuple((t_5, t_0), 2).
       term_tuple_i((t_5, t_0), 0, t_5).
       term_tuple_i((t_5, t_0), 1, t_0).
       term_tuple((t_5, t_1), 2).
       term_tuple_i((t_5, t_1), 0, t_5).
       term_tuple_i((t_5, t_1), 1, t_1).
       term_tuple((t_5, t_2), 2).
       term_tuple_i((t_5, t_2), 0, t_5).
       term_tuple_i((t_5, t_2), 1, t_2).
       term_tuple((t_5), 1).
       term_tuple_i((t_5), 0, t_5).
       term_tuple((t_6, t_0), 2).
       term_tuple_i((t_6, t_0), 0, t_6).
       term_tuple_i((t_6, t_0), 1, t_0).
       term_tuple((t_6, t_1), 2).
       term_tuple_i((t_6, t_1), 0, t_6).
       term_tuple_i((t_6, t_1), 1, t_1).
       term_tuple((t_6), 1).
       term_tuple_i((t_6), 0, t_6).
       term_tuple((t_7, t_0), 2).
       term_tuple_i((t_7, t_0), 0, t_7).
       term_tuple_i((t_7, t_0), 1, t_0).
       term_tuple((t_7, t_1), 2).
       term_tuple_i((t_7, t_1), 0, t_7).
       term_tuple_i((t_7, t_1), 1, t_1).
       term_tuple((t_7), 1).
       term_tuple_i((t_7), 0, t_7).
       projection_of_terms((t_7, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_6, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_5),((), (0,0)),(t_6)).
       projection_of_terms((t_5),((), (0,1)),(t_7)).
       projection_of_terms((t_5),((), (0,2)),(t_6)).
       projection_of_terms((t_5, t_2),((), (0,0)),(t_6)).
       projection_of_terms((t_5, t_2),(((), (0,0)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_5, t_2),((), (0,1)),(t_7)).
       projection_of_terms((t_5, t_2),(((), (0,1)), (1,0)),(t_7, t_1)).
       projection_of_terms((t_5, t_2),((), (0,2)),(t_6)).
       projection_of_terms((t_5, t_2),(((), (0,2)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_5, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_5, t_1),((), (0,0)),(t_6)).
       projection_of_terms((t_5, t_1),(((), (0,0)), (1,0)),(t_6, t_0)).
       projection_of_terms((t_5, t_1),((), (0,1)),(t_7)).
       projection_of_terms((t_5, t_1),(((), (0,1)), (1,0)),(t_7, t_0)).
       projection_of_terms((t_5, t_1),((), (0,2)),(t_6)).
       projection_of_terms((t_5, t_1),(((), (0,2)), (1,0)),(t_6, t_0)).
       projection_of_terms((t_5, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_5, t_0),((), (0,0)),(t_6)).
       projection_of_terms((t_5, t_0),((), (0,1)),(t_7)).
       projection_of_terms((t_5, t_0),((), (0,2)),(t_6)).
       projection_of_terms((t_4, t_2),((), (0,0)),(t_6)).
       projection_of_terms((t_4, t_2),(((), (0,0)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_4, t_2),((), (0,1)),(t_7)).
       projection_of_terms((t_4, t_2),(((), (0,1)), (1,0)),(t_7, t_1)).
       projection_of_terms((t_4, t_2),((), (0,2)),(t_5)).
       projection_of_terms((t_4, t_2),(((), (0,2)), (1,0)),(t_5, t_1)).
       projection_of_terms((t_4, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_4, t_1),((), (0,0)),(t_6)).
       projection_of_terms((t_4, t_1),(((), (0,0)), (1,0)),(t_6, t_0)).
       projection_of_terms((t_4, t_1),((), (0,1)),(t_7)).
       projection_of_terms((t_4, t_1),(((), (0,1)), (1,0)),(t_7, t_0)).
       projection_of_terms((t_4, t_1),((), (0,2)),(t_5)).
       projection_of_terms((t_4, t_1),(((), (0,2)), (1,0)),(t_5, t_0)).
       projection_of_terms((t_4, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_3, t_2),((), (0,0)),(t_5)).
       projection_of_terms((t_3, t_2),(((), (0,0)), (1,0)),(t_5, t_1)).
       projection_of_terms((t_3, t_2),((), (0,1)),(t_7)).
       projection_of_terms((t_3, t_2),(((), (0,1)), (1,0)),(t_7, t_1)).
       projection_of_terms((t_3, t_2),((), (0,2)),(t_6)).
       projection_of_terms((t_3, t_2),(((), (0,2)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_3, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_3, t_1),((), (0,0)),(t_5)).
       projection_of_terms((t_3, t_1),(((), (0,0)), (1,0)),(t_5, t_0)).
       projection_of_terms((t_3, t_1),((), (0,1)),(t_7)).
       projection_of_terms((t_3, t_1),(((), (0,1)), (1,0)),(t_7, t_0)).
       projection_of_terms((t_3, t_1),((), (0,2)),(t_6)).
       projection_of_terms((t_3, t_1),(((), (0,2)), (1,0)),(t_6, t_0)).
       projection_of_terms((t_3, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_1),((), (0,0)),(t_0)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       projector_tuple((((), (0,0)), (1,0)), 2).
       projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
       projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
       projector_tuple(((), (0,1)), 1).
       projector_tuple_i(((), (0,1)), 0, (0,1)).
       projector_tuple((((), (0,1)), (1,0)), 2).
       projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
       projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
       projector_tuple(((), (0,2)), 1).
       projector_tuple_i(((), (0,2)), 0, (0,2)).
       projector_tuple((((), (0,2)), (1,0)), 2).
       projector_tuple_i((((), (0,2)), (1,0)), 0, (0,2)).
       projector_tuple_i((((), (0,2)), (1,0)), 1, (1,0)).
       projector_tuple(((), (1,0)), 1).
       projector_tuple_i(((), (1,0)), 0, (1,0)).
       term_tuple_root_functions((t_0), (z)).
       term_tuple_root_functions((t_1), (s)).
       term_tuple_root_functions((t_3, t_1), (node,s)).
       term_tuple_root_functions((t_3, t_2), (node,s)).
       term_tuple_root_functions((t_4, t_1), (node,s)).
       term_tuple_root_functions((t_4, t_2), (node,s)).
       term_tuple_root_functions((t_5), (node)).
       term_tuple_root_functions((t_5, t_0), (node,z)).
       term_tuple_root_functions((t_5, t_1), (node,s)).
       term_tuple_root_functions((t_5, t_2), (node,s)).
       term_tuple_root_functions((t_6), (leaf)).
       term_tuple_root_functions((t_6, t_0), (leaf,z)).
       term_tuple_root_functions((t_6, t_1), (leaf,s)).
       term_tuple_root_functions((t_7), (a)).
       term_tuple_root_functions((t_7, t_0), (a,z)).
       term_tuple_root_functions((t_7, t_1), (a,s)).
       function_tuple_i((a), 0, a).
       function_tuple_i((a,s), 0, a).
       function_tuple_i((a,s), 1, s).
       function_tuple_i((a,z), 0, a).
       function_tuple_i((a,z), 1, z).
       function_tuple_i((leaf), 0, leaf).
       function_tuple_i((leaf,s), 0, leaf).
       function_tuple_i((leaf,s), 1, s).
       function_tuple_i((leaf,z), 0, leaf).
       function_tuple_i((leaf,z), 1, z).
       function_tuple_i((node), 0, node).
       function_tuple_i((node,s), 0, node).
       function_tuple_i((node,s), 1, s).
       function_tuple_i((node,z), 0, node).
       function_tuple_i((node,z), 1, z).
       function_tuple_i((s), 0, s).
       function_tuple_i((z), 0, z).
       function_tuple_types((a), (elt)).
       function_tuple_types((a,s), (elt,nat)).
       function_tuple_types((a,z), (elt,nat)).
       function_tuple_types((leaf), (elt_tree)).
       function_tuple_types((leaf,s), (elt_tree,nat)).
       function_tuple_types((leaf,z), (elt_tree,nat)).
       function_tuple_types((node), (elt_tree)).
       function_tuple_types((node,s), (elt_tree,nat)).
       function_tuple_types((node,z), (elt_tree,nat)).
       function_tuple_types((s), (nat)).
       function_tuple_types((z), (nat)).
       type_tuple((elt), 1).
       type_tuple_i((elt), 0, elt).
       type_tuple((elt,nat), 2).
       type_tuple_i((elt,nat), 0, elt).
       type_tuple_i((elt,nat), 1, nat).
       type_tuple((elt_tree), 1).
       type_tuple_i((elt_tree), 0, elt_tree).
       type_tuple((elt_tree,nat), 2).
       type_tuple_i((elt_tree,nat), 0, elt_tree).
       type_tuple_i((elt_tree,nat), 1, nat).
       type_tuple((nat), 1).
       type_tuple_i((nat), 0, nat).

       ---------Clingo output:
       Yes: formal_atom(((0, shallower, (t_5, t_2)), (((), (0, 0)), (1, 0))), shallower)
       formal_atom(((0, shallower, (t_5, t_2)), (((), (0, 2)), (1, 0))), shallower)
       formal_atom_parameter(((0, shallower, (t_5, t_2)), (((), (0, 0)), (1, 0))), 0, (0, 0))
       formal_atom_parameter(((0, shallower, (t_5, t_2)), (((), (0, 0)), (1, 0))), 1, (1, 0))
       formal_atom_parameter(((0, shallower, (t_5, t_2)), (((), (0, 2)), (1, 0))), 0, (0, 2))
       formal_atom_parameter(((0, shallower, (t_5, t_2)), (((), (0, 2)), (1, 0))), 1, (1, 0))
       relation(shallower, 2)
       relation_type(shallower, 0, elt_tree)
       relation_type(shallower, 1, nat)
       rule((0, shallower, (t_5, t_2)), shallower)
       rule((0, shallower, (t_6, t_0)), shallower)
       rule((0, shallower, (t_6, t_1)), shallower)
       rule_formal_atom_in_body((0, shallower, (t_5, t_2)), ((0, shallower, (t_5, t_2)), (((), (0, 0)), (1, 0))))
       rule_formal_atom_in_body((0, shallower, (t_5, t_2)), ((0, shallower, (t_5, t_2)), (((), (0, 2)), (1, 0))))
       rule_head_symbol((0, shallower, (t_5, t_2)), 0, node)
       rule_head_symbol((0, shallower, (t_5, t_2)), 1, s)
       rule_head_symbol((0, shallower, (t_6, t_0)), 0, leaf)
       rule_head_symbol((0, shallower, (t_6, t_0)), 1, z)
       rule_head_symbol((0, shallower, (t_6, t_1)), 0, leaf)
       rule_head_symbol((0, shallower, (t_6, t_1)), 1, s)

       ---------SHoCs finding:
       Yes: {
         shallower(leaf, s(x_1_0)) <= True
         shallower(leaf, z) <= True
         shallower(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= shallower(x_0_0, x_1_0) /\ shallower(x_0_2, x_1_0)
       } |}]

let%expect_test "model finding example thomas" =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_thomas;
  [%expect
    {|
         -------Clauses:
         {
         r2_c(c_a) <= True
         r2_c(c_f(c_a)) <= True
         r_c(c_a) <= True
         r_c(c_f(c_a)) <= True
         False <= r2_c(c_f(c_f(c_a)))
         }

         ---------Clingo clauses:
         #const imax=4.

         function(c_a, custom, 0).
         function(c_f, custom, 1).
         initial_relation(r2_c, 1).
         relation_types(r2_c, (custom)).
         initial_relation(r_c, 1).
         relation_types(r_c, (custom)).
         input_atom(r2_c, (t_0)).
         input_litteral(l_7, negative, r2_c, (t_0)).
         input_atom(r2_c, (t_2)).
         input_litteral(l_6, positive, r2_c, (t_2)).
         input_atom(r2_c, (t_1)).
         input_litteral(l_5, positive, r2_c, (t_1)).
         input_atom(r_c, (t_2)).
         input_litteral(l_4, positive, r_c, (t_2)).
         input_atom(r_c, (t_1)).
         input_litteral(l_3, positive, r_c, (t_1)).
         ground_clause(c_12).
         ground_clause_litteral(c_12, (l_6)).
         ground_clause(c_11).
         ground_clause_litteral(c_11, (l_5)).
         ground_clause(c_10).
         ground_clause_litteral(c_10, (l_4)).
         ground_clause(c_9).
         ground_clause_litteral(c_9, (l_3)).
         ground_clause(c_8).
         ground_clause_litteral(c_8, (l_7)).
         term_tuple((t_0), 1).
         term_tuple_i((t_0), 0, t_0).
         term_tuple((t_1), 1).
         term_tuple_i((t_1), 0, t_1).
         term_tuple((t_2), 1).
         term_tuple_i((t_2), 0, t_2).
         projection_of_terms((t_1),((), (0,0)),(t_2)).
         projection_of_terms((t_0),((), (0,0)),(t_1)).
         projector_tuple(((), (0,0)), 1).
         projector_tuple_i(((), (0,0)), 0, (0,0)).
         term_tuple_root_functions((t_0), (c_f)).
         term_tuple_root_functions((t_1), (c_f)).
         term_tuple_root_functions((t_2), (c_a)).
         function_tuple_i((c_a), 0, c_a).
         function_tuple_i((c_f), 0, c_f).
         function_tuple_types((c_a), (custom)).
         function_tuple_types((c_f), (custom)).
         type_tuple((custom), 1).
         type_tuple_i((custom), 0, custom).

         ---------Clingo output:
         Yes: formal_atom(((1, r2_c, t_1), ((), (0, 0))), (r, 1))
         formal_atom_parameter(((1, r2_c, t_1), ((), (0, 0))), 0, (0, 0))
         relation(r2_c, 1)
         relation(r_c, 1)
         relation((r, 1), 1)
         relation_type(r2_c, 0, custom)
         relation_type(r_c, 0, custom)
         relation_type((r, 1), 0, custom)
         rule((1, r2_c, t_1), r2_c)
         rule((1, r2_c, t_2), r2_c)
         rule((1, r_c, t_1), r_c)
         rule((1, r_c, t_2), r_c)
         rule((1, (r, 1), t_2), (r, 1))
         rule_formal_atom_in_body((1, r2_c, t_1), ((1, r2_c, t_1), ((), (0, 0))))
         rule_head_symbol((1, r2_c, t_1), 0, c_f)
         rule_head_symbol((1, r2_c, t_2), 0, c_a)
         rule_head_symbol((1, r_c, t_1), 0, c_f)
         rule_head_symbol((1, r_c, t_2), 0, c_a)
         rule_head_symbol((1, (r, 1), t_2), 0, c_a)

         ---------SHoCs finding:
         Yes: {
           _r_1(c_a) <= True
           r2_c(c_a) <= True
           r2_c(c_f(x_0_0)) <= _r_1(x_0_0)
           r_c(c_a) <= True
           r_c(c_f(x_0_0)) <= True
         } |}]

let%expect_test "model finding example length cons le fun nat" =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_length_cons_le_fun_nat;
  [%expect
    {|
         -------Clauses:
         {
         length(nil, z) <= True
         r_fcons(s(z), nil, cons(s(z), nil)) <= True
         r_fcons(z, cons(z, nil), cons(z, cons(z, nil))) <= True
         r_fcons(z, nil, cons(z, nil)) <= True
         le(z, s(z)) <= length(cons(z, nil), s(z)) /\ length(nil, z) /\ r_fcons(z, nil, cons(z, nil))
         length(cons(z, nil), s(z)) <= length(nil, z)
         }

         ---------Clingo clauses:
         #const imax=7.

         function(cons, natlist, 2).
         function(nil, natlist, 0).
         function(s, nat, 1).
         function(z, nat, 0).
         initial_relation(le, 2).
         relation_types(le, (nat,nat)).
         initial_relation(length, 2).
         relation_types(length, (natlist,nat)).
         initial_relation(r_fcons, 3).
         relation_types(r_fcons, (nat,natlist,natlist)).
         input_atom(length, (t_3, t_1)).
         input_litteral(l_14, negative, length, (t_3, t_1)).
         input_atom(length, (t_2, t_0)).
         input_litteral(l_13, negative, length, (t_2, t_0)).
         input_atom(r_fcons, (t_0, t_2, t_3)).
         input_litteral(l_12, negative, r_fcons, (t_0, t_2, t_3)).
         input_atom(le, (t_0, t_1)).
         input_litteral(l_11, positive, le, (t_0, t_1)).
         input_atom(length, (t_3, t_1)).
         input_litteral(l_10, positive, length, (t_3, t_1)).
         input_atom(length, (t_2, t_0)).
         input_litteral(l_9, positive, length, (t_2, t_0)).
         input_atom(r_fcons, (t_1, t_2, t_5)).
         input_litteral(l_8, positive, r_fcons, (t_1, t_2, t_5)).
         input_atom(r_fcons, (t_0, t_3, t_4)).
         input_litteral(l_7, positive, r_fcons, (t_0, t_3, t_4)).
         input_atom(r_fcons, (t_0, t_2, t_3)).
         input_litteral(l_6, positive, r_fcons, (t_0, t_2, t_3)).
         ground_clause(c_20).
         ground_clause_litteral(c_20, (l_9)).
         ground_clause(c_19).
         ground_clause_litteral(c_19, (l_8)).
         ground_clause(c_18).
         ground_clause_litteral(c_18, (l_7)).
         ground_clause(c_17).
         ground_clause_litteral(c_17, (l_6)).
         ground_clause(c_16).
         ground_clause_litteral(c_16, (l_12;l_13;l_14;l_11)).
         ground_clause(c_15).
         ground_clause_litteral(c_15, (l_13;l_10)).
         term_tuple((t_0, t_0), 2).
         term_tuple_i((t_0, t_0), 0, t_0).
         term_tuple_i((t_0, t_0), 1, t_0).
         term_tuple((t_0, t_1), 2).
         term_tuple_i((t_0, t_1), 0, t_0).
         term_tuple_i((t_0, t_1), 1, t_1).
         term_tuple((t_0, t_2, t_3), 3).
         term_tuple_i((t_0, t_2, t_3), 0, t_0).
         term_tuple_i((t_0, t_2, t_3), 1, t_2).
         term_tuple_i((t_0, t_2, t_3), 2, t_3).
         term_tuple((t_0, t_2), 2).
         term_tuple_i((t_0, t_2), 0, t_0).
         term_tuple_i((t_0, t_2), 1, t_2).
         term_tuple((t_0, t_3, t_4), 3).
         term_tuple_i((t_0, t_3, t_4), 0, t_0).
         term_tuple_i((t_0, t_3, t_4), 1, t_3).
         term_tuple_i((t_0, t_3, t_4), 2, t_4).
         term_tuple((t_0, t_3), 2).
         term_tuple_i((t_0, t_3), 0, t_0).
         term_tuple_i((t_0, t_3), 1, t_3).
         term_tuple((t_0), 1).
         term_tuple_i((t_0), 0, t_0).
         term_tuple((t_1, t_2, t_5), 3).
         term_tuple_i((t_1, t_2, t_5), 0, t_1).
         term_tuple_i((t_1, t_2, t_5), 1, t_2).
         term_tuple_i((t_1, t_2, t_5), 2, t_5).
         term_tuple((t_1), 1).
         term_tuple_i((t_1), 0, t_1).
         term_tuple((t_2, t_0), 2).
         term_tuple_i((t_2, t_0), 0, t_2).
         term_tuple_i((t_2, t_0), 1, t_0).
         term_tuple((t_2, t_3), 2).
         term_tuple_i((t_2, t_3), 0, t_2).
         term_tuple_i((t_2, t_3), 1, t_3).
         term_tuple((t_2), 1).
         term_tuple_i((t_2), 0, t_2).
         term_tuple((t_3, t_1), 2).
         term_tuple_i((t_3, t_1), 0, t_3).
         term_tuple_i((t_3, t_1), 1, t_1).
         term_tuple((t_3), 1).
         term_tuple_i((t_3), 0, t_3).
         projection_of_terms((t_3),((), (0,0)),(t_0)).
         projection_of_terms((t_3),((), (0,1)),(t_2)).
         projection_of_terms((t_3, t_1),((), (0,0)),(t_0)).
         projection_of_terms((t_3, t_1),(((), (0,0)), (1,0)),(t_0, t_0)).
         projection_of_terms((t_3, t_1),((), (0,1)),(t_2)).
         projection_of_terms((t_3, t_1),(((), (0,1)), (1,0)),(t_2, t_0)).
         projection_of_terms((t_3, t_1),((), (1,0)),(t_0)).
         projection_of_terms((t_2, t_3),((), (1,0)),(t_0)).
         projection_of_terms((t_2, t_3),((), (1,1)),(t_2)).
         projection_of_terms((t_1),((), (0,0)),(t_0)).
         projection_of_terms((t_1, t_2, t_5),((), (0,0)),(t_0)).
         projection_of_terms((t_1, t_2, t_5),(((), (0,0)), (2,0)),(t_0, t_1)).
         projection_of_terms((t_1, t_2, t_5),(((), (0,0)), (2,1)),(t_0, t_2)).
         projection_of_terms((t_1, t_2, t_5),((), (2,0)),(t_1)).
         projection_of_terms((t_1, t_2, t_5),((), (2,1)),(t_2)).
         projection_of_terms((t_0, t_3),((), (1,0)),(t_0)).
         projection_of_terms((t_0, t_3),((), (1,1)),(t_2)).
         projection_of_terms((t_0, t_3, t_4),((), (1,0)),(t_0)).
         projection_of_terms((t_0, t_3, t_4),(((), (1,0)), (2,0)),(t_0, t_0)).
         projection_of_terms((t_0, t_3, t_4),(((), (1,0)), (2,1)),(t_0, t_3)).
         projection_of_terms((t_0, t_3, t_4),((), (1,1)),(t_2)).
         projection_of_terms((t_0, t_3, t_4),(((), (1,1)), (2,0)),(t_2, t_0)).
         projection_of_terms((t_0, t_3, t_4),(((), (1,1)), (2,1)),(t_2, t_3)).
         projection_of_terms((t_0, t_3, t_4),((), (2,0)),(t_0)).
         projection_of_terms((t_0, t_3, t_4),((), (2,1)),(t_3)).
         projection_of_terms((t_0, t_2, t_3),((), (2,0)),(t_0)).
         projection_of_terms((t_0, t_2, t_3),((), (2,1)),(t_2)).
         projection_of_terms((t_0, t_1),((), (1,0)),(t_0)).
         projector_tuple(((), (0,0)), 1).
         projector_tuple_i(((), (0,0)), 0, (0,0)).
         projector_tuple((((), (0,0)), (1,0)), 2).
         projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
         projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
         projector_tuple((((), (0,0)), (2,0)), 2).
         projector_tuple_i((((), (0,0)), (2,0)), 0, (0,0)).
         projector_tuple_i((((), (0,0)), (2,0)), 1, (2,0)).
         projector_tuple((((), (0,0)), (2,1)), 2).
         projector_tuple_i((((), (0,0)), (2,1)), 0, (0,0)).
         projector_tuple_i((((), (0,0)), (2,1)), 1, (2,1)).
         projector_tuple(((), (0,1)), 1).
         projector_tuple_i(((), (0,1)), 0, (0,1)).
         projector_tuple((((), (0,1)), (1,0)), 2).
         projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
         projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
         projector_tuple(((), (1,0)), 1).
         projector_tuple_i(((), (1,0)), 0, (1,0)).
         projector_tuple((((), (1,0)), (2,0)), 2).
         projector_tuple_i((((), (1,0)), (2,0)), 0, (1,0)).
         projector_tuple_i((((), (1,0)), (2,0)), 1, (2,0)).
         projector_tuple((((), (1,0)), (2,1)), 2).
         projector_tuple_i((((), (1,0)), (2,1)), 0, (1,0)).
         projector_tuple_i((((), (1,0)), (2,1)), 1, (2,1)).
         projector_tuple(((), (1,1)), 1).
         projector_tuple_i(((), (1,1)), 0, (1,1)).
         projector_tuple((((), (1,1)), (2,0)), 2).
         projector_tuple_i((((), (1,1)), (2,0)), 0, (1,1)).
         projector_tuple_i((((), (1,1)), (2,0)), 1, (2,0)).
         projector_tuple((((), (1,1)), (2,1)), 2).
         projector_tuple_i((((), (1,1)), (2,1)), 0, (1,1)).
         projector_tuple_i((((), (1,1)), (2,1)), 1, (2,1)).
         projector_tuple(((), (2,0)), 1).
         projector_tuple_i(((), (2,0)), 0, (2,0)).
         projector_tuple(((), (2,1)), 1).
         projector_tuple_i(((), (2,1)), 0, (2,1)).
         term_tuple_root_functions((t_0), (z)).
         term_tuple_root_functions((t_0, t_0), (z,z)).
         term_tuple_root_functions((t_0, t_1), (z,s)).
         term_tuple_root_functions((t_0, t_2), (z,nil)).
         term_tuple_root_functions((t_0, t_2, t_3), (z,nil,cons)).
         term_tuple_root_functions((t_0, t_3), (z,cons)).
         term_tuple_root_functions((t_0, t_3, t_4), (z,cons,cons)).
         term_tuple_root_functions((t_1), (s)).
         term_tuple_root_functions((t_1, t_2, t_5), (s,nil,cons)).
         term_tuple_root_functions((t_2), (nil)).
         term_tuple_root_functions((t_2, t_0), (nil,z)).
         term_tuple_root_functions((t_2, t_3), (nil,cons)).
         term_tuple_root_functions((t_3), (cons)).
         term_tuple_root_functions((t_3, t_1), (cons,s)).
         function_tuple_i((cons), 0, cons).
         function_tuple_i((cons,s), 0, cons).
         function_tuple_i((cons,s), 1, s).
         function_tuple_i((nil), 0, nil).
         function_tuple_i((nil,cons), 0, nil).
         function_tuple_i((nil,cons), 1, cons).
         function_tuple_i((nil,z), 0, nil).
         function_tuple_i((nil,z), 1, z).
         function_tuple_i((s), 0, s).
         function_tuple_i((s,nil,cons), 0, s).
         function_tuple_i((s,nil,cons), 1, nil).
         function_tuple_i((s,nil,cons), 2, cons).
         function_tuple_i((z), 0, z).
         function_tuple_i((z,cons), 0, z).
         function_tuple_i((z,cons), 1, cons).
         function_tuple_i((z,cons,cons), 0, z).
         function_tuple_i((z,cons,cons), 1, cons).
         function_tuple_i((z,cons,cons), 2, cons).
         function_tuple_i((z,nil), 0, z).
         function_tuple_i((z,nil), 1, nil).
         function_tuple_i((z,nil,cons), 0, z).
         function_tuple_i((z,nil,cons), 1, nil).
         function_tuple_i((z,nil,cons), 2, cons).
         function_tuple_i((z,s), 0, z).
         function_tuple_i((z,s), 1, s).
         function_tuple_i((z,z), 0, z).
         function_tuple_i((z,z), 1, z).
         function_tuple_types((cons), (natlist)).
         function_tuple_types((cons,s), (natlist,nat)).
         function_tuple_types((nil), (natlist)).
         function_tuple_types((nil,cons), (natlist,natlist)).
         function_tuple_types((nil,z), (natlist,nat)).
         function_tuple_types((s), (nat)).
         function_tuple_types((s,nil,cons), (nat,natlist,natlist)).
         function_tuple_types((z), (nat)).
         function_tuple_types((z,cons), (nat,natlist)).
         function_tuple_types((z,cons,cons), (nat,natlist,natlist)).
         function_tuple_types((z,nil), (nat,natlist)).
         function_tuple_types((z,nil,cons), (nat,natlist,natlist)).
         function_tuple_types((z,s), (nat,nat)).
         function_tuple_types((z,z), (nat,nat)).
         type_tuple((nat), 1).
         type_tuple_i((nat), 0, nat).
         type_tuple((nat,nat), 2).
         type_tuple_i((nat,nat), 0, nat).
         type_tuple_i((nat,nat), 1, nat).
         type_tuple((nat,natlist), 2).
         type_tuple_i((nat,natlist), 0, nat).
         type_tuple_i((nat,natlist), 1, natlist).
         type_tuple((nat,natlist,natlist), 3).
         type_tuple_i((nat,natlist,natlist), 0, nat).
         type_tuple_i((nat,natlist,natlist), 1, natlist).
         type_tuple_i((nat,natlist,natlist), 2, natlist).
         type_tuple((natlist), 1).
         type_tuple_i((natlist), 0, natlist).
         type_tuple((natlist,nat), 2).
         type_tuple_i((natlist,nat), 0, natlist).
         type_tuple_i((natlist,nat), 1, nat).
         type_tuple((natlist,natlist), 2).
         type_tuple_i((natlist,natlist), 0, natlist).
         type_tuple_i((natlist,natlist), 1, natlist).

         ---------Clingo output:
         Yes: relation(le, 2)
         relation(length, 2)
         relation(r_fcons, 3)
         relation_type(le, 0, nat)
         relation_type(le, 1, nat)
         relation_type(length, 0, natlist)
         relation_type(length, 1, nat)
         relation_type(r_fcons, 0, nat)
         relation_type(r_fcons, 1, natlist)
         relation_type(r_fcons, 2, natlist)
         rule((0, le, (t_0, t_1)), le)
         rule((0, length, (t_2, t_0)), length)
         rule((0, length, (t_3, t_1)), length)
         rule((0, r_fcons, (t_0, t_2, t_3)), r_fcons)
         rule((0, r_fcons, (t_0, t_3, t_4)), r_fcons)
         rule((0, r_fcons, (t_1, t_2, t_5)), r_fcons)
         rule_head_symbol((0, le, (t_0, t_1)), 0, z)
         rule_head_symbol((0, le, (t_0, t_1)), 1, s)
         rule_head_symbol((0, length, (t_2, t_0)), 0, nil)
         rule_head_symbol((0, length, (t_2, t_0)), 1, z)
         rule_head_symbol((0, length, (t_3, t_1)), 0, cons)
         rule_head_symbol((0, length, (t_3, t_1)), 1, s)
         rule_head_symbol((0, r_fcons, (t_0, t_2, t_3)), 0, z)
         rule_head_symbol((0, r_fcons, (t_0, t_2, t_3)), 1, nil)
         rule_head_symbol((0, r_fcons, (t_0, t_2, t_3)), 2, cons)
         rule_head_symbol((0, r_fcons, (t_0, t_3, t_4)), 0, z)
         rule_head_symbol((0, r_fcons, (t_0, t_3, t_4)), 1, cons)
         rule_head_symbol((0, r_fcons, (t_0, t_3, t_4)), 2, cons)
         rule_head_symbol((0, r_fcons, (t_1, t_2, t_5)), 0, s)
         rule_head_symbol((0, r_fcons, (t_1, t_2, t_5)), 1, nil)
         rule_head_symbol((0, r_fcons, (t_1, t_2, t_5)), 2, cons)

         ---------SHoCs finding:
         Yes: {
           le(z, s(x_1_0)) <= True
           length(cons(x_0_0, x_0_1), s(x_1_0)) <= True
           length(nil, z) <= True
           r_fcons(s(x_0_0), nil, cons(x_2_0, x_2_1)) <= True
           r_fcons(z, cons(x_1_0, x_1_1), cons(x_2_0, x_2_1)) <= True
           r_fcons(z, nil, cons(x_2_0, x_2_1)) <= True
         } |}]

let%expect_test "model finding example length cons le fun nat" =
  present_problem_and_solution Clause.Database_for_tests.ground_clause_system_bug;
  [%expect
    {|
               -------Clauses:
               {
               plus(z, s(z), s(z)) <= True
               plus(z, z, z) <= True
               False <= plus(z, s(s(z)), s(z))
               }

               ---------Clingo clauses:
               #const imax=4.

               function(s, nat, 1).
               function(z, nat, 0).
               initial_relation(plus, 3).
               relation_types(plus, (nat,nat,nat)).
               input_atom(plus, (t_0, t_2, t_1)).
               input_litteral(l_5, negative, plus, (t_0, t_2, t_1)).
               input_atom(plus, (t_0, t_1, t_1)).
               input_litteral(l_4, positive, plus, (t_0, t_1, t_1)).
               input_atom(plus, (t_0, t_0, t_0)).
               input_litteral(l_3, positive, plus, (t_0, t_0, t_0)).
               ground_clause(c_8).
               ground_clause_litteral(c_8, (l_4)).
               ground_clause(c_7).
               ground_clause_litteral(c_7, (l_3)).
               ground_clause(c_6).
               ground_clause_litteral(c_6, (l_5)).
               term_tuple((t_0, t_0, t_0), 3).
               term_tuple_i((t_0, t_0, t_0), 0, t_0).
               term_tuple_i((t_0, t_0, t_0), 1, t_0).
               term_tuple_i((t_0, t_0, t_0), 2, t_0).
               term_tuple((t_0, t_0), 2).
               term_tuple_i((t_0, t_0), 0, t_0).
               term_tuple_i((t_0, t_0), 1, t_0).
               term_tuple((t_0, t_1, t_1), 3).
               term_tuple_i((t_0, t_1, t_1), 0, t_0).
               term_tuple_i((t_0, t_1, t_1), 1, t_1).
               term_tuple_i((t_0, t_1, t_1), 2, t_1).
               term_tuple((t_0, t_2, t_1), 3).
               term_tuple_i((t_0, t_2, t_1), 0, t_0).
               term_tuple_i((t_0, t_2, t_1), 1, t_2).
               term_tuple_i((t_0, t_2, t_1), 2, t_1).
               term_tuple((t_0), 1).
               term_tuple_i((t_0), 0, t_0).
               term_tuple((t_1, t_0), 2).
               term_tuple_i((t_1, t_0), 0, t_1).
               term_tuple_i((t_1, t_0), 1, t_0).
               term_tuple((t_1), 1).
               term_tuple_i((t_1), 0, t_1).
               projection_of_terms((t_1),((), (0,0)),(t_0)).
               projection_of_terms((t_1, t_0),((), (0,0)),(t_0)).
               projection_of_terms((t_0, t_2, t_1),((), (1,0)),(t_1)).
               projection_of_terms((t_0, t_2, t_1),(((), (1,0)), (2,0)),(t_1, t_0)).
               projection_of_terms((t_0, t_2, t_1),((), (2,0)),(t_0)).
               projection_of_terms((t_0, t_1, t_1),((), (1,0)),(t_0)).
               projection_of_terms((t_0, t_1, t_1),(((), (1,0)), (2,0)),(t_0, t_0)).
               projection_of_terms((t_0, t_1, t_1),((), (2,0)),(t_0)).
               projector_tuple(((), (0,0)), 1).
               projector_tuple_i(((), (0,0)), 0, (0,0)).
               projector_tuple(((), (1,0)), 1).
               projector_tuple_i(((), (1,0)), 0, (1,0)).
               projector_tuple((((), (1,0)), (2,0)), 2).
               projector_tuple_i((((), (1,0)), (2,0)), 0, (1,0)).
               projector_tuple_i((((), (1,0)), (2,0)), 1, (2,0)).
               projector_tuple(((), (2,0)), 1).
               projector_tuple_i(((), (2,0)), 0, (2,0)).
               term_tuple_root_functions((t_0), (z)).
               term_tuple_root_functions((t_0, t_0), (z,z)).
               term_tuple_root_functions((t_0, t_0, t_0), (z,z,z)).
               term_tuple_root_functions((t_0, t_1, t_1), (z,s,s)).
               term_tuple_root_functions((t_0, t_2, t_1), (z,s,s)).
               term_tuple_root_functions((t_1), (s)).
               term_tuple_root_functions((t_1, t_0), (s,z)).
               function_tuple_i((s), 0, s).
               function_tuple_i((s,z), 0, s).
               function_tuple_i((s,z), 1, z).
               function_tuple_i((z), 0, z).
               function_tuple_i((z,s,s), 0, z).
               function_tuple_i((z,s,s), 1, s).
               function_tuple_i((z,s,s), 2, s).
               function_tuple_i((z,z), 0, z).
               function_tuple_i((z,z), 1, z).
               function_tuple_i((z,z,z), 0, z).
               function_tuple_i((z,z,z), 1, z).
               function_tuple_i((z,z,z), 2, z).
               function_tuple_types((s), (nat)).
               function_tuple_types((s,z), (nat,nat)).
               function_tuple_types((z), (nat)).
               function_tuple_types((z,s,s), (nat,nat,nat)).
               function_tuple_types((z,z), (nat,nat)).
               function_tuple_types((z,z,z), (nat,nat,nat)).
               type_tuple((nat), 1).
               type_tuple_i((nat), 0, nat).
               type_tuple((nat,nat), 2).
               type_tuple_i((nat,nat), 0, nat).
               type_tuple_i((nat,nat), 1, nat).
               type_tuple((nat,nat,nat), 3).
               type_tuple_i((nat,nat,nat), 0, nat).
               type_tuple_i((nat,nat,nat), 1, nat).
               type_tuple_i((nat,nat,nat), 2, nat).

               ---------Clingo output:
               Yes: formal_atom(((1, plus, (t_0, t_1, t_1)), ((), (1, 0))), (r, 1))
               formal_atom_parameter(((1, plus, (t_0, t_1, t_1)), ((), (1, 0))), 0, (1, 0))
               relation(plus, 3)
               relation((r, 1), 1)
               relation_type(plus, 0, nat)
               relation_type(plus, 1, nat)
               relation_type(plus, 2, nat)
               relation_type((r, 1), 0, nat)
               rule((1, plus, (t_0, t_0, t_0)), plus)
               rule((1, plus, (t_0, t_1, t_1)), plus)
               rule((1, (r, 1), t_0), (r, 1))
               rule_formal_atom_in_body((1, plus, (t_0, t_1, t_1)), ((1, plus, (t_0, t_1, t_1)), ((), (1, 0))))
               rule_head_symbol((1, plus, (t_0, t_0, t_0)), 0, z)
               rule_head_symbol((1, plus, (t_0, t_0, t_0)), 1, z)
               rule_head_symbol((1, plus, (t_0, t_0, t_0)), 2, z)
               rule_head_symbol((1, plus, (t_0, t_1, t_1)), 0, z)
               rule_head_symbol((1, plus, (t_0, t_1, t_1)), 1, s)
               rule_head_symbol((1, plus, (t_0, t_1, t_1)), 2, s)
               rule_head_symbol((1, (r, 1), t_0), 0, z)

               ---------SHoCs finding:
               Yes: {
                 _r_1(z) <= True
                 plus(z, s(x_1_0), s(x_2_0)) <= _r_1(x_1_0)
                 plus(z, z, z) <= True
               } |}]

let%test "ok" =
  let () = Printing.pp_ok () in
  true
