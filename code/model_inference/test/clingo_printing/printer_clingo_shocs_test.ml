open Clause.Aliases

let printing_instance (clause_system : Set_clause_patterns.t) : unit =
  let () = Model_inference.Fresh_label_generator_shocs.reset () in
  let () =
    Format.fprintf Format.std_formatter "-------Clauses:\n%a\n\n---------Clingo clauses:\n%a\n"
      Set_clause_patterns.pp clause_system
      (Fun.flip Model_inference.Shocs_finder_clingo.pp_instance)
      clause_system
  in
  ()

let%expect_test "clauses_1 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_1;
  [%expect "
    -------Clauses:
    {

    }

    ---------Clingo clauses:
    #const imax=1."]

let%expect_test "clauses_2 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_2;
  [%expect
    {|
    -------Clauses:
    {
    r_l(nil) <= True
    }

    ---------Clingo clauses:
    #const imax=2.

    function(nil, natlist, 0).
    initial_relation(r_l, 1).
    relation_types(r_l, (natlist)).
    input_atom(r_l, (t_0)).
    input_litteral(l_1, positive, r_l, (t_0)).
    ground_clause(c_2).
    ground_clause_litteral(c_2, (l_1)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple_root_functions((t_0), (nil)).
    function_tuple_i((nil), 0, nil).
    function_tuple_types((nil), (natlist)).
    type_tuple((natlist), 1).
    type_tuple_i((natlist), 0, natlist). |}]

let%expect_test "clauses_3 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_3;
  [%expect
    "
    -------Clauses:
    {
    r_l_n(nil, z) <= True
    }

    ---------Clingo clauses:
    #const imax=3.

    function(nil, natlist, 0).
    function(z, nat, 0).
    initial_relation(r_l_n, 2).
    relation_types(r_l_n, (natlist,nat)).
    input_atom(r_l_n, (t_1, t_0)).
    input_litteral(l_2, positive, r_l_n, (t_1, t_0)).
    ground_clause(c_3).
    ground_clause_litteral(c_3, (l_2)).
    term_tuple((t_1, t_0), 2).
    term_tuple_i((t_1, t_0), 0, t_1).
    term_tuple_i((t_1, t_0), 1, t_0).
    term_tuple_root_functions((t_1, t_0), (nil,z)).
    function_tuple_i((nil,z), 0, nil).
    function_tuple_i((nil,z), 1, z).
    function_tuple_types((nil,z), (natlist,nat)).
    type_tuple((natlist,nat), 2).
    type_tuple_i((natlist,nat), 0, natlist).
    type_tuple_i((natlist,nat), 1, nat)."]

let%expect_test "clauses_4 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_4;
  [%expect
    "
    -------Clauses:
    {
    r_n(s(z)) <= True
    }

    ---------Clingo clauses:
    #const imax=3.

    function(s, nat, 1).
    function(z, nat, 0).
    initial_relation(r_n, 1).
    relation_types(r_n, (nat)).
    input_atom(r_n, (t_1)).
    input_litteral(l_2, positive, r_n, (t_1)).
    ground_clause(c_3).
    ground_clause_litteral(c_3, (l_2)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    projection_of_terms((t_1),((), (0,0)),(t_0)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (z)).
    term_tuple_root_functions((t_1), (s)).
    function_tuple_i((s), 0, s).
    function_tuple_i((z), 0, z).
    function_tuple_types((s), (nat)).
    function_tuple_types((z), (nat)).
    type_tuple((nat), 1).
    type_tuple_i((nat), 0, nat)."]

let%expect_test "clauses_5 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_5;
  [%expect
    "
    -------Clauses:
    {
    False <= r_n(z)
    }

    ---------Clingo clauses:
    #const imax=2.

    function(z, nat, 0).
    initial_relation(r_n, 1).
    relation_types(r_n, (nat)).
    input_atom(r_n, (t_0)).
    input_litteral(l_1, negative, r_n, (t_0)).
    ground_clause(c_2).
    ground_clause_litteral(c_2, (l_1)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple_root_functions((t_0), (z)).
    function_tuple_i((z), 0, z).
    function_tuple_types((z), (nat)).
    type_tuple((nat), 1).
    type_tuple_i((nat), 0, nat)."]

let%expect_test "clauses_11 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_11;
  [%expect
    "
    -------Clauses:
    {
    r_c(c_f(c_f(c_a))) <= True
    }

    ---------Clingo clauses:
    #const imax=4.

    function(c_a, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_0)).
    input_litteral(l_3, positive, r_c, (t_0)).
    ground_clause(c_4).
    ground_clause_litteral(c_4, (l_3)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    projection_of_terms((t_1),((), (0,0)),(t_2)).
    projection_of_terms((t_0),((), (0,0)),(t_1)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom)."]

let%expect_test "clauses_12 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_12;
  [%expect
    "
    -------Clauses:
    {
    r_c(c_f(c_b)) <= True
    False <= r_c(c_f(c_a))
    }

    ---------Clingo clauses:
    #const imax=5.

    function(c_a, custom, 0).
    function(c_b, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_1)).
    input_litteral(l_5, negative, r_c, (t_1)).
    input_atom(r_c, (t_0)).
    input_litteral(l_4, positive, r_c, (t_0)).
    ground_clause(c_7).
    ground_clause_litteral(c_7, (l_4)).
    ground_clause(c_6).
    ground_clause_litteral(c_6, (l_5)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    term_tuple((t_3), 1).
    term_tuple_i((t_3), 0, t_3).
    projection_of_terms((t_1),((), (0,0)),(t_3)).
    projection_of_terms((t_0),((), (0,0)),(t_2)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_b)).
    term_tuple_root_functions((t_3), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_b), 0, c_b).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_b), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom)."]

let%expect_test "clauses_13 in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_13;
  [%expect
    "
    -------Clauses:
    {
    r_c(c_f(c_f(c_a))) <= True
    False <= r_c(c_f(c_a))
    }

    ---------Clingo clauses:
    #const imax=4.

    function(c_a, custom, 0).
    function(c_f, custom, 1).
    initial_relation(r_c, 1).
    relation_types(r_c, (custom)).
    input_atom(r_c, (t_1)).
    input_litteral(l_4, negative, r_c, (t_1)).
    input_atom(r_c, (t_0)).
    input_litteral(l_3, positive, r_c, (t_0)).
    ground_clause(c_6).
    ground_clause_litteral(c_6, (l_3)).
    ground_clause(c_5).
    ground_clause_litteral(c_5, (l_4)).
    term_tuple((t_0), 1).
    term_tuple_i((t_0), 0, t_0).
    term_tuple((t_1), 1).
    term_tuple_i((t_1), 0, t_1).
    term_tuple((t_2), 1).
    term_tuple_i((t_2), 0, t_2).
    projection_of_terms((t_1),((), (0,0)),(t_2)).
    projection_of_terms((t_0),((), (0,0)),(t_1)).
    projector_tuple(((), (0,0)), 1).
    projector_tuple_i(((), (0,0)), 0, (0,0)).
    term_tuple_root_functions((t_0), (c_f)).
    term_tuple_root_functions((t_1), (c_f)).
    term_tuple_root_functions((t_2), (c_a)).
    function_tuple_i((c_a), 0, c_a).
    function_tuple_i((c_f), 0, c_f).
    function_tuple_types((c_a), (custom)).
    function_tuple_types((c_f), (custom)).
    type_tuple((custom), 1).
    type_tuple_i((custom), 0, custom)."]

let%expect_test "clauses_shallower in clingo" =
  printing_instance Clause.Database_for_tests.ground_clause_system_shallower;
  [%expect
    "
       -------Clauses:
       {
       shallower(leaf, s(z)) <= True
       shallower(leaf, z) <= True
       shallower(node(leaf, a, leaf), s(s(z))) <= shallower(leaf, s(z))
       shallower(node(leaf, a, leaf), s(z)) <= shallower(leaf, z)
       shallower(node(node(leaf, a, leaf), a, leaf), s(s(z))) <= shallower(node(leaf, a, leaf), s(z))
       False <= shallower(node(leaf, a, leaf), z)
       }

       ---------Clingo clauses:
       #const imax=8.

       function(a, elt, 0).
       function(leaf, elt_tree, 0).
       function(node, elt_tree, 3).
       function(s, nat, 1).
       function(z, nat, 0).
       initial_relation(shallower, 2).
       relation_types(shallower, (elt_tree,nat)).
       input_atom(shallower, (t_5, t_1)).
       input_litteral(l_15, negative, shallower, (t_5, t_1)).
       input_atom(shallower, (t_5, t_0)).
       input_litteral(l_14, negative, shallower, (t_5, t_0)).
       input_atom(shallower, (t_4, t_1)).
       input_litteral(l_13, negative, shallower, (t_4, t_1)).
       input_atom(shallower, (t_4, t_0)).
       input_litteral(l_12, negative, shallower, (t_4, t_0)).
       input_atom(shallower, (t_5, t_1)).
       input_litteral(l_11, positive, shallower, (t_5, t_1)).
       input_atom(shallower, (t_5, t_0)).
       input_litteral(l_10, positive, shallower, (t_5, t_0)).
       input_atom(shallower, (t_4, t_2)).
       input_litteral(l_9, positive, shallower, (t_4, t_2)).
       input_atom(shallower, (t_4, t_1)).
       input_litteral(l_8, positive, shallower, (t_4, t_1)).
       input_atom(shallower, (t_3, t_2)).
       input_litteral(l_7, positive, shallower, (t_3, t_2)).
       ground_clause(c_21).
       ground_clause_litteral(c_21, (l_11)).
       ground_clause(c_20).
       ground_clause_litteral(c_20, (l_10)).
       ground_clause(c_19).
       ground_clause_litteral(c_19, (l_15;l_9)).
       ground_clause(c_18).
       ground_clause_litteral(c_18, (l_14;l_8)).
       ground_clause(c_17).
       ground_clause_litteral(c_17, (l_13;l_7)).
       ground_clause(c_16).
       ground_clause_litteral(c_16, (l_12)).
       term_tuple((t_0), 1).
       term_tuple_i((t_0), 0, t_0).
       term_tuple((t_1), 1).
       term_tuple_i((t_1), 0, t_1).
       term_tuple((t_3, t_2), 2).
       term_tuple_i((t_3, t_2), 0, t_3).
       term_tuple_i((t_3, t_2), 1, t_2).
       term_tuple((t_4, t_0), 2).
       term_tuple_i((t_4, t_0), 0, t_4).
       term_tuple_i((t_4, t_0), 1, t_0).
       term_tuple((t_4, t_1), 2).
       term_tuple_i((t_4, t_1), 0, t_4).
       term_tuple_i((t_4, t_1), 1, t_1).
       term_tuple((t_4, t_2), 2).
       term_tuple_i((t_4, t_2), 0, t_4).
       term_tuple_i((t_4, t_2), 1, t_2).
       term_tuple((t_4), 1).
       term_tuple_i((t_4), 0, t_4).
       term_tuple((t_5, t_0), 2).
       term_tuple_i((t_5, t_0), 0, t_5).
       term_tuple_i((t_5, t_0), 1, t_0).
       term_tuple((t_5, t_1), 2).
       term_tuple_i((t_5, t_1), 0, t_5).
       term_tuple_i((t_5, t_1), 1, t_1).
       term_tuple((t_5), 1).
       term_tuple_i((t_5), 0, t_5).
       term_tuple((t_6, t_0), 2).
       term_tuple_i((t_6, t_0), 0, t_6).
       term_tuple_i((t_6, t_0), 1, t_0).
       term_tuple((t_6, t_1), 2).
       term_tuple_i((t_6, t_1), 0, t_6).
       term_tuple_i((t_6, t_1), 1, t_1).
       term_tuple((t_6), 1).
       term_tuple_i((t_6), 0, t_6).
       projection_of_terms((t_6, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_5, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_4),((), (0,0)),(t_5)).
       projection_of_terms((t_4),((), (0,1)),(t_6)).
       projection_of_terms((t_4),((), (0,2)),(t_5)).
       projection_of_terms((t_4, t_2),((), (0,0)),(t_5)).
       projection_of_terms((t_4, t_2),(((), (0,0)), (1,0)),(t_5, t_1)).
       projection_of_terms((t_4, t_2),((), (0,1)),(t_6)).
       projection_of_terms((t_4, t_2),(((), (0,1)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_4, t_2),((), (0,2)),(t_5)).
       projection_of_terms((t_4, t_2),(((), (0,2)), (1,0)),(t_5, t_1)).
       projection_of_terms((t_4, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_4, t_1),((), (0,0)),(t_5)).
       projection_of_terms((t_4, t_1),(((), (0,0)), (1,0)),(t_5, t_0)).
       projection_of_terms((t_4, t_1),((), (0,1)),(t_6)).
       projection_of_terms((t_4, t_1),(((), (0,1)), (1,0)),(t_6, t_0)).
       projection_of_terms((t_4, t_1),((), (0,2)),(t_5)).
       projection_of_terms((t_4, t_1),(((), (0,2)), (1,0)),(t_5, t_0)).
       projection_of_terms((t_4, t_1),((), (1,0)),(t_0)).
       projection_of_terms((t_4, t_0),((), (0,0)),(t_5)).
       projection_of_terms((t_4, t_0),((), (0,1)),(t_6)).
       projection_of_terms((t_4, t_0),((), (0,2)),(t_5)).
       projection_of_terms((t_3, t_2),((), (0,0)),(t_4)).
       projection_of_terms((t_3, t_2),(((), (0,0)), (1,0)),(t_4, t_1)).
       projection_of_terms((t_3, t_2),((), (0,1)),(t_6)).
       projection_of_terms((t_3, t_2),(((), (0,1)), (1,0)),(t_6, t_1)).
       projection_of_terms((t_3, t_2),((), (0,2)),(t_5)).
       projection_of_terms((t_3, t_2),(((), (0,2)), (1,0)),(t_5, t_1)).
       projection_of_terms((t_3, t_2),((), (1,0)),(t_1)).
       projection_of_terms((t_1),((), (0,0)),(t_0)).
       projector_tuple(((), (0,0)), 1).
       projector_tuple_i(((), (0,0)), 0, (0,0)).
       projector_tuple((((), (0,0)), (1,0)), 2).
       projector_tuple_i((((), (0,0)), (1,0)), 0, (0,0)).
       projector_tuple_i((((), (0,0)), (1,0)), 1, (1,0)).
       projector_tuple(((), (0,1)), 1).
       projector_tuple_i(((), (0,1)), 0, (0,1)).
       projector_tuple((((), (0,1)), (1,0)), 2).
       projector_tuple_i((((), (0,1)), (1,0)), 0, (0,1)).
       projector_tuple_i((((), (0,1)), (1,0)), 1, (1,0)).
       projector_tuple(((), (0,2)), 1).
       projector_tuple_i(((), (0,2)), 0, (0,2)).
       projector_tuple((((), (0,2)), (1,0)), 2).
       projector_tuple_i((((), (0,2)), (1,0)), 0, (0,2)).
       projector_tuple_i((((), (0,2)), (1,0)), 1, (1,0)).
       projector_tuple(((), (1,0)), 1).
       projector_tuple_i(((), (1,0)), 0, (1,0)).
       term_tuple_root_functions((t_0), (z)).
       term_tuple_root_functions((t_1), (s)).
       term_tuple_root_functions((t_3, t_2), (node,s)).
       term_tuple_root_functions((t_4), (node)).
       term_tuple_root_functions((t_4, t_0), (node,z)).
       term_tuple_root_functions((t_4, t_1), (node,s)).
       term_tuple_root_functions((t_4, t_2), (node,s)).
       term_tuple_root_functions((t_5), (leaf)).
       term_tuple_root_functions((t_5, t_0), (leaf,z)).
       term_tuple_root_functions((t_5, t_1), (leaf,s)).
       term_tuple_root_functions((t_6), (a)).
       term_tuple_root_functions((t_6, t_0), (a,z)).
       term_tuple_root_functions((t_6, t_1), (a,s)).
       function_tuple_i((a), 0, a).
       function_tuple_i((a,s), 0, a).
       function_tuple_i((a,s), 1, s).
       function_tuple_i((a,z), 0, a).
       function_tuple_i((a,z), 1, z).
       function_tuple_i((leaf), 0, leaf).
       function_tuple_i((leaf,s), 0, leaf).
       function_tuple_i((leaf,s), 1, s).
       function_tuple_i((leaf,z), 0, leaf).
       function_tuple_i((leaf,z), 1, z).
       function_tuple_i((node), 0, node).
       function_tuple_i((node,s), 0, node).
       function_tuple_i((node,s), 1, s).
       function_tuple_i((node,z), 0, node).
       function_tuple_i((node,z), 1, z).
       function_tuple_i((s), 0, s).
       function_tuple_i((z), 0, z).
       function_tuple_types((a), (elt)).
       function_tuple_types((a,s), (elt,nat)).
       function_tuple_types((a,z), (elt,nat)).
       function_tuple_types((leaf), (elt_tree)).
       function_tuple_types((leaf,s), (elt_tree,nat)).
       function_tuple_types((leaf,z), (elt_tree,nat)).
       function_tuple_types((node), (elt_tree)).
       function_tuple_types((node,s), (elt_tree,nat)).
       function_tuple_types((node,z), (elt_tree,nat)).
       function_tuple_types((s), (nat)).
       function_tuple_types((z), (nat)).
       type_tuple((elt), 1).
       type_tuple_i((elt), 0, elt).
       type_tuple((elt,nat), 2).
       type_tuple_i((elt,nat), 0, elt).
       type_tuple_i((elt,nat), 1, nat).
       type_tuple((elt_tree), 1).
       type_tuple_i((elt_tree), 0, elt_tree).
       type_tuple((elt_tree,nat), 2).
       type_tuple_i((elt_tree,nat), 0, elt_tree).
       type_tuple_i((elt_tree,nat), 1, nat).
       type_tuple((nat), 1).
       type_tuple_i((nat), 0, nat)."]

let%test "ok" =
  let () = Printing.pp_ok () in
  true
