open Parameters_learning

let extract_clauses
    ?(name : string option = None)
    (lp : Program_to_clauses.Learning_problem.t)
    (parameters : Parameters_learning.t) : Clause.Aliases.Set_clause_patterns.t =
  let starting_time = Unix.gettimeofday () in
  let env = Program_to_clauses.Learning_problem.get_environement lp in
  (* let predicates_of_program = Program_to_clauses.Learning_problem.get_predicates_symbols lp in *)
  let can_underapproximate, can_overapproximate =
    Program_to_clauses.Analyse_approximation.analyse_possible_approximations lp
  in
  let clauses =
    Program_to_clauses.Approximation.extract_clauses_based_on_approximation_sets_and_method
      parameters.approximation_method lp can_underapproximate can_overapproximate
  in
  let predicates_eligible_for_under_and_over_approximation =
    (can_underapproximate, can_overapproximate)
  in
  let log_beginning =
    Log_starting_information.
      {
        name;
        lp;
        predicates_eligible_for_under_and_over_approximation;
        clauses;
        env;
        parameters;
        starting_time;
      }
  in
  let () = Log.set_starting_log log_beginning in
  clauses

let solve_and_return_log ?(name : string option = None) (lp : Program_to_clauses.Learning_problem.t)
    : Ice_answer_shocs.t * Log.t =
  (* (parameters : Parameters_learning.t)  *)

  (* Reset log for each solving *)
  let () = Log.reset_log () in
  (* let () = Fresh_label_generator_shocs.reset () in *)
  let env = Program_to_clauses.Learning_problem.get_environement lp in
  let clauses = extract_clauses ~name lp !Parameters_learning.parameters in
  let model_solution =
    Ice_shocs.infer_model ~timeout:!Parameters_learning.parameters.timeout env clauses
  in
  let final_answer = model_solution in
  (* let ending_time = Unix.gettimeofday () in *)
  (* let log_ending = Log_shocs.{final_answer; ending_time} in *)
  (* let log = (log_beginning, ice_log, log_ending) in *)
  let log = Log.copy_current_log () in
  (final_answer, log)

let pp_log_in_file ~(print_short_output : bool) (log_file : string) (log : Log.t) =
  let () = Inout.Write_to_file.write_from_pp log_file (fun c -> Log_pp.pp c log) in
  if print_short_output then
    let () = Format.pp_force_newline Format.std_formatter () in
    let () = Log_pp.pp_solution_summary Format.std_formatter log in
    let () = Format.fprintf Format.std_formatter "See more details in file '%s'\n" log_file in
    ()
