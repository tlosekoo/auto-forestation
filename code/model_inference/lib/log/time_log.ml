let write_to_html (file_path : string) (time_log : Misc.Time_logger.t) =
  let html_text = Misc.Time_logger.to_html time_log in
  Inout.Write_to_file.write_from_string file_path html_text
