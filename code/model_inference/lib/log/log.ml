module Ice_one_step_log_shocs =
  Ice_one_step_log.Make (Model_checking.Instantiation.ModelShocs) (Solver_state_shocs)
    (Ice_answer_shocs)

(* Global values! *)

type t = {
  time_log : Misc.Time_logger.t;
  ice_log : Ice_log_shocs.ice_log ref;
  log_starting_information : Log_starting_information.t Option.t ref;
  log_ending_information : Log_ending_information.t Option.t ref;
}

let create_log () =
  {
    time_log = Misc.Time_logger.create ();
    ice_log = ref Ice_log_shocs.empty;
    log_starting_information = ref None;
    log_ending_information = ref None;
  }

let log = create_log ()

let copy_current_log () : t =
  {
    time_log = ref !(log.time_log);
    ice_log = ref !(log.ice_log);
    log_starting_information = ref !(log.log_starting_information);
    log_ending_information = ref !(log.log_ending_information);
  }

let reset_log () =
  log.log_starting_information := None;
  log.log_ending_information := None;
  log.time_log := !(Misc.Time_logger.create ());
  log.ice_log := Ice_log_shocs.empty

let set_starting_log (starting_log : Log_starting_information.t) : unit =
  log.log_starting_information := Some starting_log

let set_ending_log (answer : Ice_answer_shocs.t) : unit =
  let final_time = Unix.gettimeofday () in
  let ending_log = Log_ending_information.{final_time; answer} in
  log.log_ending_information := Some ending_log

let set_ground_examples_beginning (ground_examples_beginning : Clause.Aliases.Set_clause_patterns.t)
    : unit =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.set_ground_examples_beginning !ice_log.log_current_step
      ground_examples_beginning
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let set_learner_answer (learner_answer : Learner_shocs.t) : unit =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.set_learner_answer !ice_log.log_current_step learner_answer
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let set_teachers_answer (teachers_answer : Model_checking.Pattern_recognition.t) : unit =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.set_teachers_answer !ice_log.log_current_step teachers_answer
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let tick_beginning_ice_loop () =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.tick_beginning_ice_loop !ice_log.log_current_step
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let tick_after_model_generation () =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.tick_after_model_generation !ice_log.log_current_step
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let tick_after_model_checking () =
  let ice_log = log.ice_log in
  let updated_current_step_log =
    Ice_one_step_log_shocs.tick_after_model_checking !ice_log.log_current_step
  in
  ice_log := {!ice_log with log_current_step = updated_current_step_log}

let validate_complete_step () =
  let old_ice_log = !(log.ice_log) in
  let updated_ice_log = Ice_log_shocs.validate_complete_step old_ice_log in
  log.ice_log := updated_ice_log

let get_number_of_past_ice_iterations (log : t) = List.length !(log.ice_log).log_finished_steps
