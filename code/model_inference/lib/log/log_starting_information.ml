open Clause.Aliases
open Program_to_clauses

type t = {
  name : String.t Option.t;
  lp : Learning_problem.t;
  predicates_eligible_for_under_and_over_approximation :
    Set_typed_relation_symbol.t * Set_typed_relation_symbol.t;
  clauses : Set_clause_patterns.t;
  env : Term.Datatype_environment.t;
  parameters : Parameters_learning.t;
  starting_time : Float.t;
}
[@@deriving compare, equal]
