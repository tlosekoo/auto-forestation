type t = {
  answer : Ice_answer_shocs.t;
  final_time : Float.t;
}
[@@deriving compare, equal]
