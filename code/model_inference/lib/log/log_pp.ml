open Log

let get_last_recorded_time (l : t) : float =
  match !(l.log_ending_information) with
  | Some log_ending_information -> log_ending_information.final_time
  | None -> (
      let last_recorded_time_log = Ice_log_shocs.get_last_time !(l.ice_log) in
      match last_recorded_time_log with
      | None ->
          let starting_time = (Option.get !(l.log_starting_information)).starting_time in
          starting_time
      | Some last_time -> last_time)

let compute_total_time (l : t) : float =
  let starting_time = (Option.get !(l.log_starting_information)).starting_time in
  let ending_time = get_last_recorded_time l in
  ending_time -. starting_time

let pp_approximation (c : Format.formatter) (starting_log : Log_starting_information.t) =
  let possible_underapproximation, possible_overapproximation =
    starting_log.predicates_eligible_for_under_and_over_approximation
  in
  Format.fprintf c "over-approximation: %a\nunder-approximation: %a\n"
    Term.Aliases.Set_typed_relation_symbol.pp possible_overapproximation
    Term.Aliases.Set_typed_relation_symbol.pp possible_underapproximation

let pp_parameters (c : Format.formatter) (starting_log : Log_starting_information.t) =
  Format.fprintf c "Inference procedure has parameters:\n%a\n" Parameters_learning.pp
    starting_log.parameters

let pp_problem_presentation (c : Format.formatter) (starting_log : Log_starting_information.t) =
  Format.fprintf c "\nLearning problem is:\n\n%a\n\n" Program_to_clauses.Learning_problem.pp
    starting_log.lp

let pp_clause_system (c : Format.formatter) (starting_log : Log_starting_information.t) =
  Format.fprintf c "\nClause system for inference is:\n\n%a\n\n"
    Clause.Aliases.Set_clause_patterns.pp starting_log.clauses

let pp_name c (starting_log : Log_starting_information.t) =
  match starting_log.name with
  | None -> ()
  | Some name -> Format.fprintf c "Solving %s...\n\n" name

let pp_solving_time (c : Format.formatter) (l : t) =
  Format.fprintf c "\nSolving took %f seconds.\n" (compute_total_time l)

let pp_solution_summary (c : Format.formatter) (log : t) =
  let log_ending_information = Option.get !(log.log_ending_information) in
  Format.fprintf c "%a.\n"
    (Ice_answer_shocs.pp_param ~print_details:false)
    log_ending_information.answer

let pp_solution (c : Format.formatter) (ending_information : Log_ending_information.t) =
  Ice_answer_shocs.pp_param ~print_details:true c ending_information.answer

let pp_log_current_step
    (c : Format.formatter)
    (log_current_step : Ice_log_shocs.Log_one_step.partial_log_one_step) =
  match log_current_step.ground_examples_beginning with
  | None -> Format.fprintf c "\n"
  | Some ground_examples_beginning -> (
      let () =
        Format.fprintf c "Ground examples:\n%a\n\n" Clause.Aliases.Set_clause_patterns.pp
          ground_examples_beginning
      in
      match log_current_step.learner_answer with
      | None -> Format.fprintf c "Last ice step stopped before learner could answer\n"
      | Some learner_answer -> (
          let () = Format.fprintf c "Learner output:\n%a\n\n" Learner_shocs.pp learner_answer in
          match log_current_step.teachers_answer with
          | None -> Format.fprintf c "Last ice step stopped before teacher could answer\n"
          | Some teachers_answer ->
              let () =
                Format.fprintf c "Teachers output:\n%a\n\n" Model_checking.Pattern_recognition.pp
                  teachers_answer
              in
              ()))

let pp_steps (c : Format.formatter) (log : t) : unit =
  let ice_log = !(log.ice_log) in
  let log_ending_information = Option.get !(log.log_ending_information) in
  let log_finished_steps = ice_log.log_finished_steps in
  let log_finished_steps_in_correct_order = List.rev log_finished_steps in
  let log_current_step = ice_log.log_current_step in

  (* let total_time_learner = List.fold_left ( +. ) 0. durations_of_model_generations in
     let total_time_teacher = List.fold_left ( +. ) 0. durations_of_model_checkings in
     let durations_of_steps =
       List.map2 ( +. ) durations_of_model_generations durations_of_model_checkings
     in *)
  let () =
    List.iteri (Ice_one_step_log_shocs.pp_one_step_log c) log_finished_steps_in_correct_order
  in
  let () = pp_log_current_step c log_current_step in
  let total_time_learner =
    List.fold_left ( +. ) 0.0
      (List.map Ice_one_step_log_shocs.get_duration_model_generation
         log_finished_steps_in_correct_order)
  in
  let total_time_teacher =
    List.fold_left ( +. ) 0.0
      (List.map Ice_one_step_log_shocs.get_duration_model_checking
         log_finished_steps_in_correct_order)
  in
  let () =
    Format.fprintf c "Total time: %f\nLearner time: %f\nTeacher time: %f\n" (compute_total_time log)
      total_time_learner total_time_teacher
  in
  let () =
    Format.fprintf c "Reasons for stopping: %a\n\n"
      (Ice_answer_shocs.pp_param ~print_details:true)
      log_ending_information.answer
  in
  ()

let pp_complete_log (c : Format.formatter) (l : t) : unit =
  let log_starting_information = Option.get !(l.log_starting_information) in
  let log_ending_information = Option.get !(l.log_ending_information) in
  let () = pp_name c log_starting_information in
  let () = pp_parameters c log_starting_information in
  let () = pp_problem_presentation c log_starting_information in
  let () = pp_approximation c log_starting_information in
  let () = pp_clause_system c log_starting_information in
  let () = pp_solving_time c l in
  let () = pp_solution c log_ending_information in
  let () = Format.fprintf c "\n-------------------\nSTEPS:\n" in
  let () = pp_steps c l in
  ()

let pp (c : Format.formatter) (l : t) : unit =
  (* try *)
  pp_complete_log c l
(* with
   | Invalid_argument err ->
     let new_error_message = "Failed to print log. Must be malformed:  " ^ err in
     failwith new_error_message *)
