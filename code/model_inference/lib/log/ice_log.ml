module Make
    (Model : Model_checking.Model.S)
    (Solver_state : Solver_state.S with module Model := Model)
    (Ice_answer : Ice_answer.S with module Model := Model and module Solver_state := Solver_state) =
struct
  module Log_one_step = Ice_one_step_log.Make (Model) (Solver_state) (Ice_answer)

  type log_finished_steps = Log_one_step.log_one_step list

  type ice_log = {
    log_finished_steps : log_finished_steps;
    log_current_step : Log_one_step.partial_log_one_step;
  }

  let empty : ice_log = {log_finished_steps = []; log_current_step = Log_one_step.empty_partial_log}

  let get_last_time (ice_log : ice_log) : float option =
    let last_time_current_step =
      Log_one_step.get_last_time_partial_log_one_step ice_log.log_current_step
    in
    match last_time_current_step with
    | Some last_time -> Some last_time
    | None -> (
        match List.nth_opt ice_log.log_finished_steps 0 with
        | None -> None
        | Some last_finished_log ->
            let last_time_previous_steps =
              Log_one_step.get_last_time_complete_log_one_step last_finished_log
            in
            Some last_time_previous_steps)

  let validate_complete_step (ice_log : ice_log) =
    let past_steps = ice_log.log_finished_steps in
    let new_complete_step =
      Log_one_step.convert_complete_partial_log_into_log ice_log.log_current_step
    in
    let log_finished_steps = new_complete_step :: past_steps in
    let log_current_step = Log_one_step.empty_partial_log in
    {log_finished_steps; log_current_step}
end
