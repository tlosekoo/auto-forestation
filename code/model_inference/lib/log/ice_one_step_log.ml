open Clause.Aliases
(* open Program_to_clauses *)

module Make
    (Model : Model_checking.Model.S)
    (Solver_state : Solver_state.S with module Model := Model)
    (Ice_answer : Ice_answer.S with module Model := Model and module Solver_state := Solver_state) =
struct
  let empty_log = []

  type partial_log_one_step = {
    ground_examples_beginning : Set_clause_patterns.t Option.t;
    learner_answer : Learner_shocs.t Option.t;
    teachers_answer : Model_checking.Pattern_recognition.t Option.t;
    time_beginning_loop : Float.t Option.t;
    time_after_model_generation : Float.t Option.t;
    time_after_model_checking : Float.t Option.t;
  }
  [@@deriving compare, equal]

  type log_one_step = {
    ground_examples_beginning : Set_clause_patterns.t;
    learner_answer : Learner_shocs.t;
    teachers_answer : Model_checking.Pattern_recognition.t;
    time_beginning_loop : Float.t;
    time_after_model_generation : Float.t;
    time_after_model_checking : Float.t;
  }
  [@@deriving compare, equal]

  let convert_complete_partial_log_into_log (partial_log : partial_log_one_step) : log_one_step =
    try
      let ground_examples_beginning = Option.get partial_log.ground_examples_beginning in
      let learner_answer = Option.get partial_log.learner_answer in
      let teachers_answer = Option.get partial_log.teachers_answer in
      let time_beginning_loop = Option.get partial_log.time_beginning_loop in
      let time_after_model_generation = Option.get partial_log.time_after_model_generation in
      let time_after_model_checking = Option.get partial_log.time_after_model_checking in
      {
        ground_examples_beginning;
        learner_answer;
        teachers_answer;
        time_beginning_loop;
        time_after_model_generation;
        time_after_model_checking;
      }
    with
    | Invalid_argument err ->
        failwith
          ("Tried to convert a partial log into a log but not all fields were filled in:  " ^ err)

  (* Setting fields of the log *)
  let set_ground_examples_beginning
      (partial_log : partial_log_one_step)
      (ground_examples_beginning : Set_clause_patterns.t) =
    {partial_log with ground_examples_beginning = Some ground_examples_beginning}

  let set_learner_answer (partial_log : partial_log_one_step) (learner_answer : Learner_shocs.t) =
    {partial_log with learner_answer = Some learner_answer}

  let set_teachers_answer
      (partial_log : partial_log_one_step)
      (teachers_answer : Model_checking.Pattern_recognition.t) =
    {partial_log with teachers_answer = Some teachers_answer}

  let tick_beginning_ice_loop (partial_log : partial_log_one_step) =
    let time_now = Unix.gettimeofday () in
    {partial_log with time_beginning_loop = Some time_now}

  let tick_after_model_generation (partial_log : partial_log_one_step) =
    let time_now = Unix.gettimeofday () in
    {partial_log with time_after_model_generation = Some time_now}

  let tick_after_model_checking (partial_log : partial_log_one_step) =
    let time_now = Unix.gettimeofday () in
    {partial_log with time_after_model_checking = Some time_now}

  (* usage of the log *)
  let empty_partial_log : partial_log_one_step =
    {
      ground_examples_beginning = None;
      learner_answer = None;
      teachers_answer = None;
      time_beginning_loop = None;
      time_after_model_generation = None;
      time_after_model_checking = None;
    }

  (* Pretty-printing of the log *)
  let get_last_time_complete_log_one_step (one_step_log : log_one_step) : float =
    one_step_log.time_after_model_checking

  let get_last_time_partial_log_one_step (one_step_log : partial_log_one_step) : float option =
    let times =
      List.flatten
        [
          Option.to_list one_step_log.time_beginning_loop;
          Option.to_list one_step_log.time_after_model_generation;
          Option.to_list one_step_log.time_after_model_checking;
        ]
    in
    if times = [] then
      None
    else
      Some (List.fold_left Float.max Float.min_float times)

  let get_duration_model_generation (log_one_step : log_one_step) : float =
    log_one_step.time_after_model_generation -. log_one_step.time_beginning_loop

  let get_duration_model_checking (log_one_step : log_one_step) : float =
    log_one_step.time_after_model_checking -. log_one_step.time_after_model_generation

  let pp_one_step_log (c : Format.formatter) (step_num : int) (one_step_log : log_one_step) =
    let duration_model_generation = get_duration_model_generation one_step_log in
    let duration_model_checking = get_duration_model_checking one_step_log in
    let duration_step = duration_model_checking +. duration_model_generation in
    let ground_examples_beginning = one_step_log.ground_examples_beginning in
    let learner_answer = one_step_log.learner_answer in
    let answer_of_teacher = one_step_log.teachers_answer in
    Format.fprintf c
      "-------------------------------------------\nStep %d, which took %f s (model generation: %f,  model checking: %f):\n\nGround examples at the beginning of this step are:\n%a\n\nLearner proposed model:\n%a\n\nAnswer of teacher:\n%a\n\n"
      step_num duration_step duration_model_generation duration_model_checking
      Set_clause_patterns.pp ground_examples_beginning Learner_shocs.pp learner_answer
      Model_checking.Pattern_recognition.pp answer_of_teacher
end
