(* type tription_outcome = Io_clingo.Use_clingo_as_solver.outcome *)

type t =
  ( Tree_tuple_formalisms.Shocs.t,
    Io_clingo.Use_clingo_as_solver.stopping_reason,
    unit )
  Misc.Tription.t

include Set.OrderedType with type t := t

val run_shocs_finder :
  time_limit:int -> Term.Datatype_environment.t -> Clause.Aliases.Set_clause_patterns.t -> t

(* *)
(* *)
(* *)
(* *)
(* *)
(* FOR TESTING PURPOSE ONLY *)
type testing_stuff = {
  pp_instance : Format.formatter -> unit;
  clingo_output : Io_clingo.Use_clingo_as_solver.outcome;
  shocs_output : t;
}

val get_testing_stuff :
  additional_clingo_options:string ->
  time_limit:int ->
  Term.Datatype_environment.t ->
  Clause.Aliases.Set_clause_patterns.t ->
  testing_stuff

val pp_instance : Clause.Aliases.Set_clause_patterns.t -> Format.formatter -> unit
val shocs_finder_analysis : Io_clingo.Analysis.t
