(* Type Environment *)
let func = "function"
let function_input_type = "function_input_type"

(* Relation definition *)
let initial_relation_symbol = "initial_relation"
let relation_symbol = "relation"
let relation_type = "relation_type"
let relation_types = "relation_types"

(* Term definition *)
let term = "term"
let term_child = "term_child"
let projection_of_terms = "projection_of_terms"

(* Projector *)
let projector_tuple = "projector_tuple"
let projector_tuple_i = "projector_tuple_i"

(* Initial atom *)
let input_atom = "input_atom"

(* term tuple *)
let term_tuple = "term_tuple"
let term_tuple_i = "term_tuple_i"
let term_tuple_root_functions = "term_tuple_root_functions"
let function_tuple_i = "function_tuple_i"
let function_tuple_types = "function_tuple_types"
let type_tuple = "type_tuple"
let type_tuple_i = "type_tuple_i"

(* Litteral *)
let input_litteral = "input_litteral"
let positive = "positive"
let negative = "negative"

(* input clauses *)

let ground_clause = "ground_clause"
let ground_clause_litteral = "ground_clause_litteral"

(* Rule definition *)
let rule = "rule"
let rule_head_symbol = "rule_head_symbol"
let rule_formal_atom_in_body = "rule_formal_atom_in_body"

(* Formal atoms *)
let formal_atom = "formal_atom"
let formal_atom_parameter = "formal_atom_parameter"

(* Debugging things *)
let true_by_some_rule = "true_by_some_rule"
let true_by_rule = "true_by_rule"
let false_by_all_rules = "false_by_all_rules"
(* let imax = "imax" *)
