open Term
open Tree_tuple_formalisms

(* Writing *)
module Map_term_label = Misc.My_map.Make2 (Term.Pattern) (Misc.String)
module Map_terms_label = Misc.My_map.Make2 (Term.Patterns) (Misc.String)
module Map_label_functions = Misc.My_map.Make2 (Misc.String) (Typed_functions)
module Map_atom_label = Misc.My_map.Make2 (Clause.Atom_patterns) (Misc.String)
module Map_clause_label = Misc.My_map.Make2 (Clause.Clause_patterns) (Misc.String)
module Map_projector_label = Misc.My_map.Make2 (Projector) (Misc.String)
module Projected_subterm = Misc.Pair.Make (Projector) (Pattern)
module Projected_subterms = Misc.List_maker.Make (Projected_subterm)
module Projectors_and_subterms = Misc.Pair.Make (Misc.List_maker.Make (Projector)) (Patterns)
module Set_projectors_and_subterms = Misc.My_set.Make (Projectors_and_subterms)
module Set_projected_subterms = Misc.My_set.Make2 (Projected_subterms)
module Set_set_projectors_and_subterms = Misc.My_set.Make2 (Set_projectors_and_subterms)
module Set_projector = Misc.My_set.Make2 (Projector)
module Projector_tuple = Misc.List_maker.Make (Projector)
module Set_projectorTuple = Misc.My_set.Make2 (Projector_tuple)
module Map_patterns_projection = Misc.My_map.Make2 (Patterns) (Set_projectors_and_subterms)
module Map_projectorTuple_label = Misc.My_map.Make2 (Misc.List_maker.Make (Projector)) (Misc.String)

module Map_patterns_projection_and_set_patterns =
  Misc.Pair.Make (Map_patterns_projection) (Term.Aliases.Set_patterns)

(* Reading *)
module Map_label_function = Misc.My_map.Make2 (Misc.String) (Typed_function)
module Map_label_relation = Misc.My_map.Make2 (Misc.String) (Typed_relation_symbol)
module Map_arg_relation = Misc.My_map.Make2 (Io_clingo.Output_facts.Arg) (Typed_relation_symbol)
module Facts = Misc.List_maker.Make (Io_clingo.Output_facts.Fact)

module Map_arg_facts =
  Misc.My_map.Make2
    (Io_clingo.Output_facts.Arg)
    (Misc.List_maker.Make (Io_clingo.Output_facts.Fact))

module Map_arg_bodyAtom = Misc.My_map.Make2 (Io_clingo.Output_facts.Arg) (Formal_atom)
