open Term
open Subfact_printer

let pp_fact_imax (c : Format.formatter) (depth : int) = Format.fprintf c "#const imax=%d.\n\n" depth

let pp_fact_function (c : Format.formatter) (functionn : Typed_function.t) =
  Format.fprintf c "%s(%a, %a, %d).\n" Clingo_words.func pp_typed_function functionn pp_datatype
    (Typed_function.get_output_type functionn)
    (Typed_function.get_arity functionn)

(* let pp_fact_function_input
     (c : Format.formatter)
     (functionn : Typed_function.t)
     (i : int)
     (ith_type : Datatype.t) =
   Format.fprintf c "%s(%a, %d, %a).\n" Clingo_words.function_input_type pp_typed_function functionn
     i pp_datatype ith_type *)

(* let pp_fact_ith_type_of_typedRelationSymbol
     (relation : Typed_relation_symbol.t)
     (c : Format.formatter)
     (i : int)
     (datatype : Datatype.t) : unit =
   Format.fprintf c "%s(%a, %d, %a).\n" Clingo_words.relation_type pp_relation_name relation i
     Datatype.pp datatype *)

let pp_fact_relation_name_and_arity c (relation : Typed_relation_symbol.t) =
  Format.fprintf c "%s(%a, %d).\n" Clingo_words.initial_relation_symbol pp_relation_name relation
    (Typed_relation_symbol.get_arity relation)

let pp_fact_relation_datatypes
    (c : Format.formatter)
    (relation : Typed_relation_symbol.t)
    (datatypes : Datatypes.t) =
  Format.fprintf c "%s(%a, %a).\n" Clingo_words.relation_types pp_relation_name relation
    pp_datatypes datatypes

let pp_fact_term_root_function (the_function : Typed_function.t) (term_label : string) c =
  Format.fprintf c "%s(%s, %a).\n" Clingo_words.term term_label pp_typed_function the_function

let pp_fact_term_ith_child
    (term_label : string)
    (c : Format.formatter)
    (i : int)
    (child_label : string) : unit =
  Format.fprintf c "%s(%s, %d, %s).\n" Clingo_words.term_child term_label i child_label

let pp_fact_ith_argument_termTuple
    (c : Format.formatter)
    (label_termTuple : string)
    (i : int)
    (label_ith_term : string) =
  Format.fprintf c "%s(%s, %d, %s).\n" Clingo_words.term_tuple_i label_termTuple i label_ith_term

let pp_fact_term_tuple (c : Format.formatter) (label_termTuple : string) (length_tuple : int) =
  Format.fprintf c "%s(%s, %d).\n" Clingo_words.term_tuple label_termTuple length_tuple

let pp_fact_atom (defined_relation : Typed_relation_symbol.t) (terms_id : string) c =
  Format.fprintf c "%s(%a, %s).\n" Clingo_words.input_atom pp_relation_name defined_relation
    terms_id

let pp_fact_litteral
    c
    (litteral_label : string)
    (relation_atom : Typed_relation_symbol.t)
    (label_patterns : string)
    (is_positive : bool) : unit =
  Format.fprintf c "%s(%s, %a, %a, %s).\n" Clingo_words.input_litteral litteral_label
    pp_positivity_clingo is_positive pp_relation_name relation_atom label_patterns

let pp_fact_ground_clause c (clause_label : string) =
  Format.fprintf c "%s(%s).\n" Clingo_words.ground_clause clause_label

let pp_fact_ground_clause_litterals (clause_label : string) (labeled_litterals : string list) c =
  if labeled_litterals = [] then
    ()
  else
    Format.fprintf c "%s(%s, (%a)).\n" Clingo_words.ground_clause_litteral clause_label
      (Printing.pp_list_sep ";" Misc.String.pp)
      labeled_litterals

let pp_fact_projection
    (c : Format.formatter)
    (label_patterns : string)
    (label_projectors : string)
    (label_children_patterns : string) =
  Format.fprintf c "%s(%s,%s,%s).\n" Clingo_words.projection_of_terms label_patterns
    label_projectors label_children_patterns

let pp_fact_projectorTuple (c : Format.formatter) (label_projectorTuple : string) (size_tuple : int)
    =
  Format.fprintf c "%s(%s, %d).\n" Clingo_words.projector_tuple label_projectorTuple size_tuple

let pp_fact_projectorTuple_i
    (c : Format.formatter)
    (label_projectorTuple : string)
    (i : int)
    (ith_projector : Tree_tuple_formalisms.Projector.t) =
  Format.fprintf c "%s(%s, %d, %s).\n" Clingo_words.projector_tuple_i label_projectorTuple i
    (Label_generator_shocs.label_projector ith_projector)

let pp_fact_rootFunctionTuple_of_termTuple
    (c : Format.formatter)
    (label_pattern : string)
    (root_functions : Typed_functions.t) =
  Format.fprintf c "%s(%s, %a).\n" Clingo_words.term_tuple_root_functions label_pattern
    Subfact_printer.pp_typed_functions root_functions

let pp_facts_functionTuple_i (c : Format.formatter) (root_functions : Typed_functions.t) =
  let pp_functions channel () =
    Format.fprintf channel "%a" Subfact_printer.pp_typed_functions root_functions
  in
  List.iteri
    (fun i ith_function ->
      Format.fprintf c "%s(%a, %d, %a).\n" Clingo_words.function_tuple_i pp_functions () i
        Subfact_printer.pp_typed_function ith_function)
    root_functions

let pp_fact_functionTuple_typeTuple (c : Format.formatter) (function_tuple : Typed_functions.t) =
  Format.fprintf c "%s(%a, %a).\n" Clingo_words.function_tuple_types
    Subfact_printer.pp_typed_functions function_tuple Subfact_printer.pp_datatypes
    (Typed_functions.get_output_types function_tuple)

let pp_fact_type_tuple_is (c : Format.formatter) (type_tuple : Datatypes.t) =
  let pp_types channel () = Format.fprintf channel "%a" Subfact_printer.pp_datatypes type_tuple in
  List.iteri
    (fun i ith_datatype ->
      Format.fprintf c "%s(%a, %d, %a).\n" Clingo_words.type_tuple_i pp_types () i
        Subfact_printer.pp_datatype ith_datatype)
    type_tuple

let pp_fact_type_tuple (c : Format.formatter) (type_tuple : Datatypes.t) =
  Format.fprintf c "%s(%a, %d).\n" Clingo_words.type_tuple Subfact_printer.pp_datatypes type_tuple
    (List.length type_tuple)
