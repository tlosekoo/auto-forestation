(** `get_every_subcombination_and_projectors_of_patterns set_of_tuple_of_patterns`
	returns the set of possible projections of every element of `set_of_tuple_of_patterns`, i.e. a
	map from every tuple of patterns to its projections.
	It also returns the set of reached patterns and projectors 
*)
val get_every_subcombination_and_projectors_of_patterns :
  Term.Aliases.Set_patterns.t ->
  Modules_used_for_translation.Map_patterns_projection.t
  * Term.Aliases.Set_patterns.t
  * Modules_used_for_translation.Set_projectorTuple.t
