open Clause.Aliases

(* open Model_checking *)
open Modules_used_for_translation

let c = Misc.Counter.get_new_counter ()
let reset () = Misc.Counter.reset c

let get_fresh_label () =
  let i = Misc.Counter.get_and_incr_counter c in
  let label = "" ^ string_of_int i in
  label

type personalisations =
  | Relation
  | Clause
  | Term
  | Litteral

let get_prefix_of_personalisation (which : personalisations) =
  match which with
  | Relation -> "r"
  | Clause -> "c"
  | Term -> "t"
  | Litteral -> "l"

let get_personalized_fresh_label (which : personalisations) =
  let fresh_label = get_fresh_label () in
  let personalized_prefix = get_prefix_of_personalisation which in
  personalized_prefix ^ "_" ^ fresh_label

let get_fresh_label_relation () = get_personalized_fresh_label Relation
let get_fresh_label_clause () = get_personalized_fresh_label Clause
let get_fresh_label_term () = get_personalized_fresh_label Term
let get_fresh_label_litteral () = get_personalized_fresh_label Litteral

let label_set_of_pattern (patterns : Set_pattern.t) : Map_term_label.t =
  patterns |> Set_pattern.to_list
  |> List.map (fun p -> (p, get_fresh_label_term ()))
  |> Map_term_label.of_list

let label_set_of_litterals (atoms : Set_atom_patterns.t) : Map_atom_label.t =
  atoms |> Set_atom_patterns.to_list
  |> List.map (fun atom -> (atom, get_fresh_label_litteral ()))
  |> Map_atom_label.of_list

let label_list_of_clauses (clauses : Clause.Clause_patterns.t list) : Map_clause_label.t =
  clauses
  |> List.map (fun clause -> (clause, get_fresh_label_clause ()))
  |> Map_clause_label.of_list

let label_env (env : Term.Datatype_environment.t) : Map_label_function.t =
  env |> Term.Datatype_environment.get_constructors |> Set_typed_function.to_list
  |> List.map (fun f -> (Term.Typed_function.to_string f, f))
  |> Map_label_function.of_list
