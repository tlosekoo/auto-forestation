(* Prints everything clingo needs in order to infer a model from these examples *)
val ground_constraints_into_clingo :
  Clause.Aliases.Set_clause_patterns.t -> Format.formatter -> unit
