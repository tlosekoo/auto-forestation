open Term
open Clause.Aliases
open Clause
open Modules_used_for_translation
open Fresh_label_generator_shocs
open Label_generator_shocs
open Get_components
open Terms_printer
open Highlevel_printer

let ground_constraints_into_clingo (clauses : Set_clause_patterns.t) (c : Format.formatter) : unit =
  (* TODO: resetting, a good idea? *)
  let () = Fresh_label_generator_shocs.reset () in
  let () = Term.Symbol_generator.reset_counter () in
  let list_clauses = Set_clause_patterns.to_list clauses in
  let all_negative_atoms_of_clauses =
    Set_atom_patterns.list_union (List.map Clause_patterns.get_negative list_clauses)
  in
  let all_positive_atoms_of_clauses =
    Set_atom_patterns.list_union (List.map Clause_patterns.get_positive list_clauses)
  in
  let all_atoms_of_clauses =
    Set_atom_patterns.union all_negative_atoms_of_clauses all_positive_atoms_of_clauses
  in
  let all_patterns_of_atoms =
    all_atoms_of_clauses |> Set_atom_patterns.to_list |> List.map Atom_patterns.get_argument
  in
  let all_projections, all_patternsTuples, all_projectorTuples =
    Term_combination.get_every_subcombination_and_projectors_of_patterns
      (Set_patterns.of_list all_patterns_of_atoms)
  in

  let predicates_appearing_in_clauses =
    list_clauses
    |> List.map Clause.Clause_patterns.extract_non_equality_predicates
    |> Set_typed_relation_symbol.list_union
  in

  let labeled_subterms =
    all_atoms_of_clauses |> get_all_subterms_of_a_set_of_atoms |> label_set_of_pattern
  in
  let all_subterms = Map_term_label.domain labeled_subterms in
  let labeled_positive_litterals = label_set_of_litterals all_positive_atoms_of_clauses in
  let labeled_negative_litterals = label_set_of_litterals all_negative_atoms_of_clauses in
  let labeled_clauses = label_list_of_clauses list_clauses in
  let labeled_termTuples =
    label_termTuples labeled_subterms (Set_patterns.to_list all_patternsTuples)
  in
  let all_function_symbols_of_clauses =
    labeled_subterms |> Map_term_label.domain |> Set_pattern.to_list
    |> List.map Pattern.get_root_symbol |> Set_typed_function.of_list
  in
  let map_labelPatterns_rootFunctions =
    labeled_termTuples |> Map_terms_label.to_list
    |> List.map (fun (patterns, label) -> (label, Option.get (Patterns.get_root_symbols patterns)))
    |> Map_label_functions.of_list
  in
  let () = pp_imax c all_subterms in
  let () = pp_functions c all_function_symbols_of_clauses in
  let () = pp_predicates c predicates_appearing_in_clauses in
  let () =
    pp_labeled_litterals labeled_termTuples c ~is_positive:false labeled_negative_litterals
  in
  let () = pp_labeled_litterals labeled_termTuples c ~is_positive:true labeled_positive_litterals in

  let () =
    pp_labeled_clauses labeled_negative_litterals labeled_positive_litterals c labeled_clauses
  in

  let () =
    pp_all_regarding_terms c labeled_subterms map_labelPatterns_rootFunctions all_projections
      all_patternsTuples all_projectorTuples
  in
  ()
