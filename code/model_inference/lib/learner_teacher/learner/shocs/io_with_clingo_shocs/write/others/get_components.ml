open Term
open Clause
open Clause.Aliases

let rec get_subterms (p : Pattern.t) : Set_pattern.t =
  let children = Pattern.get_children p in
  let subterms_of_children = List.map get_subterms children in
  let all_strict_subterms = Set_pattern.list_union subterms_of_children in
  Set_pattern.add p all_strict_subterms

let get_all_subterms_of_a_set_of_atoms (atoms : Set_atom_patterns.t) : Set_pattern.t =
  atoms |> Set_atom_patterns.to_list
  |> List.map Atom_patterns.get_argument
  |> Misc.List_op.map_ll get_subterms |> List.map Set_pattern.list_union |> Set_pattern.list_union
