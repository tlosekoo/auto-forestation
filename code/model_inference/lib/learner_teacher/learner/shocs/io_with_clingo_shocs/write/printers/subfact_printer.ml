open Term

let pp_list_in_clingo (pp_a : Format.formatter -> 'a -> unit) =
  Printing.pp_list_sep_surrounded "," "(" ")" pp_a

let pp_relation_name c r = Format.fprintf c "%s" (Typed_relation_symbol.to_string r)
let pp_typed_function = Typed_function.pp
let pp_typed_functions = pp_list_in_clingo pp_typed_function
let pp_datatype = Datatype.pp
let pp_datatypes = pp_list_in_clingo pp_datatype

let positivity_as_str ~(is_positive : bool) : string =
  match is_positive with
  | false -> Clingo_words.negative
  | true -> Clingo_words.positive

let pp_positivity_clingo c is_positive = Format.fprintf c "%s" (positivity_as_str ~is_positive)
