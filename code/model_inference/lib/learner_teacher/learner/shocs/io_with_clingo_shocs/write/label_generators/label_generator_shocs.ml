open Term
open Clause.Aliases
open Clause
open Modules_used_for_translation

let rec comma_separated_elements (l : string list) =
  match l with
  | [] -> ""
  | [e] -> e
  | e :: l' -> e ^ ", " ^ comma_separated_elements l'

let label_patterns (labeled_term : Map_term_label.t) (patterns : Term.Patterns.t) : string =
  let labels_patterns = List.map ((Stdlib.Fun.flip Map_term_label.find) labeled_term) patterns in
  "(" ^ comma_separated_elements labels_patterns ^ ")"

let label_termTuples (labeled_term : Map_term_label.t) (terms : Term.Patterns.t list) :
    Map_terms_label.t =
  terms
  |> List.map (fun tuple -> (tuple, label_patterns labeled_term tuple))
  |> Map_terms_label.of_list

let label_atoms (labeled_term : Map_term_label.t) (atoms : Set_atom_patterns.t) : Map_atom_label.t =
  let label_one_atom atom =
    let relation =
      Option.get
        (Predicate_symbol_or_equality.get_typed_relation_symbol_opt
           (Clause.Atom_patterns.get_predicate atom))
    in
    let args = atom |> Clause.Atom_patterns.get_argument |> label_patterns labeled_term in
    "(" ^ Typed_relation_symbol.to_string relation ^ ", " ^ args ^ ")"
  in
  atoms |> Set_atom_patterns.to_list
  |> List.map (fun atom -> (atom, label_one_atom atom))
  |> Map_atom_label.of_list

let label_projector (projector : Tree_tuple_formalisms.Projector.t) =
  let str_projector_term = string_of_int projector.projector_term in
  let str_projector_child = string_of_int projector.projector_child in
  "(" ^ str_projector_term ^ "," ^ str_projector_child ^ ")"

let label_projectorTuple (projectors : Tree_tuple_formalisms.Projector.t list) : string =
  projectors |> List.map label_projector
  |> List.fold_left (fun acc label -> "(" ^ acc ^ ", " ^ label ^ ")") "()"

let label_projectorTuples (projectors_list : Tree_tuple_formalisms.Projector.t list list) :
    Map_projectorTuple_label.t =
  projectors_list
  |> List.map (fun tuple -> (tuple, label_projectorTuple tuple))
  |> Map_projectorTuple_label.of_list
