open Term
open Clause.Aliases
open Clause
open Modules_used_for_translation
open Fact_printer

(* TODO THINK ABOUT A NICE UPPER-BOUND *)
let pp_imax (c : Format.formatter) (all_subterms_of_clauses : Set_pattern.t) =
  let max_depth =
    Set_pattern.cardinal all_subterms_of_clauses
    (* all_terms |> Set_pattern.to_list |> List.map Pattern.depth |> Misc.Min_and_max.max_pilist *)
  in
  pp_fact_imax c (max_depth + 1)

let pp_function_into_clingo (c : Format.formatter) (functionn : Typed_function.t) =
  let () = pp_fact_function c functionn in
  (* let input_datatypes = Typed_function.get_input_types functionn in *)
  (* let () = List.iteri (pp_fact_function_input c functionn) input_datatypes in *)
  ()

let pp_functions (c : Format.formatter) (functions : Set_typed_function.t) =
  Set_typed_function.iter (pp_function_into_clingo c) functions

let pp_predicate (c : Format.formatter) (predicate : Typed_relation_symbol.t) : unit =
  let () = pp_fact_relation_name_and_arity c predicate in
  let type_relation = Typed_relation_symbol.get_types predicate in
  let () = pp_fact_relation_datatypes c predicate type_relation in
  ()

let pp_predicates (c : Format.formatter) (predicates : Set_typed_relation_symbol.t) : unit =
  Set_typed_relation_symbol.iter (pp_predicate c) predicates

let pp_atom (c : Format.formatter) (relation : Typed_relation_symbol.t) (label_patterns : string) :
    unit =
  let () = pp_fact_atom relation label_patterns c in
  ()

let pp_labeled_litteral
    (labeled_termTuples : Map_terms_label.t)
    (c : Format.formatter)
    ~(is_positive : bool)
    (atom : Atom_patterns.t)
    (litteral_label : string) =
  let relation =
    Option.get
      (Predicate_symbol_or_equality.get_typed_relation_symbol_opt
         (Atom_patterns.get_predicate atom))
  in
  let args = Atom_patterns.get_argument atom in
  let label_patterns = Map_terms_label.find args labeled_termTuples in
  let () = pp_atom c relation label_patterns in
  let () = pp_fact_litteral c litteral_label relation label_patterns is_positive in
  ()

let pp_labeled_litterals
    (labeled_termTuples : Map_terms_label.t)
    (c : Format.formatter)
    ~(is_positive : bool)
    (labeled_litterals : Map_atom_label.t) =
  Map_atom_label.iter
    (fun atom litteral_label ->
      pp_labeled_litteral labeled_termTuples c ~is_positive atom litteral_label)
    labeled_litterals

let pp_labeled_clause
    (labeled_negative_litterals : Map_atom_label.t)
    (labeled_positive_litterals : Map_atom_label.t)
    (c : Format.formatter)
    (clause : Clause_patterns.t)
    (clause_label : string) =
  let labels_positive_litterals =
    clause |> Clause_patterns.get_positive |> Set_atom_patterns.to_list
    |> List.map (fun a -> Map_atom_label.find a labeled_positive_litterals)
  in
  let labels_negative_litterals =
    clause |> Clause_patterns.get_negative |> Set_atom_patterns.to_list
    |> List.map (fun a -> Map_atom_label.find a labeled_negative_litterals)
  in
  let labeled_litterals = List.append labels_negative_litterals labels_positive_litterals in
  let () = pp_fact_ground_clause c clause_label in
  let () = pp_fact_ground_clause_litterals clause_label labeled_litterals c in
  ()

let pp_labeled_clauses
    (labeled_negative_litterals : Map_atom_label.t)
    (labeled_positive_litterals : Map_atom_label.t)
    (c : Format.formatter)
    (labeled_clauses : Map_clause_label.t) =
  Map_clause_label.iter
    (pp_labeled_clause labeled_negative_litterals labeled_positive_litterals c)
    labeled_clauses
