open Modules_used_for_translation
open Term
open Term.Aliases

let pp_term_tuple_is
    (c : Format.formatter)
    (map_term_label : Map_term_label.t)
    (label_termTuple : string)
    (patterns : Patterns.t) =
  let patterns_labels = List.map ((Stdlib.Fun.flip Map_term_label.find) map_term_label) patterns in
  List.iteri (Fact_printer.pp_fact_ith_argument_termTuple c label_termTuple) patterns_labels

let pp_term_tuple_and_term_tuple_is
    (c : Format.formatter)
    (map_term_label : Map_term_label.t)
    (map_termTuple_label : Map_terms_label.t)
    (patterns : Patterns.t) =
  let patterns_id = Map_terms_label.find patterns map_termTuple_label in
  let () = Fact_printer.pp_fact_term_tuple c patterns_id (List.length patterns) in
  pp_term_tuple_is c map_term_label patterns_id patterns

let pp_one_projection_of_a_termTuple
    (c : Format.formatter)
    (map_termTuple_label : Map_terms_label.t)
    (map_projectorTuple_label : Map_projectorTuple_label.t)
    (labeled_parent_termTuple : string)
    ((projectors, children_patterns) : Projectors_and_subterms.t) =
  let label_projectors = Map_projectorTuple_label.find projectors map_projectorTuple_label in
  let label_children_patterns = Map_terms_label.find children_patterns map_termTuple_label in
  Fact_printer.pp_fact_projection c labeled_parent_termTuple label_projectors
    label_children_patterns

let pp_every_projection_of_a_termTuple
    (c : Format.formatter)
    (map_termTuple_label : Map_terms_label.t)
    (map_projectorTuple_label : Map_projectorTuple_label.t)
    (patterns : Patterns.t)
    (every_projected : Set_projectors_and_subterms.t) =
  let label_patterns = Map_terms_label.find patterns map_termTuple_label in
  Set_projectors_and_subterms.iter
    (pp_one_projection_of_a_termTuple c map_termTuple_label map_projectorTuple_label label_patterns)
    every_projected

let pp_definition_projectorTuple
    (c : Format.formatter)
    (map_projectorTuple_label : Map_projectorTuple_label.t)
    (projectors : Projector_tuple.t) =
  let label_projector_tuple = Map_projectorTuple_label.find projectors map_projectorTuple_label in
  let () = Fact_printer.pp_fact_projectorTuple c label_projector_tuple (List.length projectors) in
  let () = List.iteri (Fact_printer.pp_fact_projectorTuple_i c label_projector_tuple) projectors in
  ()

let pp_everything_about_type_tuple (c : Format.formatter) (type_tuple : Datatypes.t) =
  let () = Fact_printer.pp_fact_type_tuple c type_tuple in
  let () = Fact_printer.pp_fact_type_tuple_is c type_tuple in
  ()

let pp_everything_about_function_tuples
    (c : Format.formatter)
    (map_patternsLabel_functions : Map_label_functions.t) =
  let () =
    Map_label_functions.iter
      (Fact_printer.pp_fact_rootFunctionTuple_of_termTuple c)
      map_patternsLabel_functions
  in
  let function_tuples = Map_label_functions.codomain map_patternsLabel_functions in
  let type_tuple =
    function_tuples |> Set_typed_functions.to_list
    |> List.map Typed_functions.get_output_types
    |> Set_datatypes.of_list
  in
  let () = Set_typed_functions.iter (Fact_printer.pp_facts_functionTuple_i c) function_tuples in
  let () =
    Set_typed_functions.iter (Fact_printer.pp_fact_functionTuple_typeTuple c) function_tuples
  in
  let () = Set_datatypes.iter (pp_everything_about_type_tuple c) type_tuple in
  ()

let pp_all_regarding_terms
    (c : Format.formatter)
    (map_term_label : Map_term_label.t)
    (map_patternsLabel_functions : Map_label_functions.t)
    (all_projections : Map_patterns_projection.t)
    (all_patternsTuple_as_set : Set_patterns.t)
    (all_projectorTuple : Set_projectorTuple.t) =
  let all_patterns_tuples_as_list = Set_patterns.to_list all_patternsTuple_as_set in
  let map_termTuple_label =
    Label_generator_shocs.label_termTuples map_term_label all_patterns_tuples_as_list
  in
  let map_projectorTuple_label =
    all_projectorTuple |> Set_projectorTuple.to_list |> Label_generator_shocs.label_projectorTuples
  in
  let () =
    List.iter
      (pp_term_tuple_and_term_tuple_is c map_term_label map_termTuple_label)
      all_patterns_tuples_as_list
  in
  let () =
    Map_patterns_projection.iter
      (pp_every_projection_of_a_termTuple c map_termTuple_label map_projectorTuple_label)
      all_projections
  in
  let () =
    Set_projectorTuple.iter
      (pp_definition_projectorTuple c map_projectorTuple_label)
      all_projectorTuple
  in
  let () = pp_everything_about_function_tuples c map_patternsLabel_functions in
  ()
