open Term
open Term.Aliases
open Tree_tuple_formalisms
open Modules_used_for_translation

let from_projected_subterms_to_projectors_and_subterms (projected_subterms : Projected_subterms.t) :
    Projectors_and_subterms.t =
  List.split projected_subterms

let filter_projections_with__allowed_subterm_combination
    (projector_to_add : Projector.t)
    (eligible_combinations_to_extend : Set_projected_subterms.t) : Set_projected_subterms.t =
  match !Parameters_shocs_inference_projection.parameters.allowed_subterm_combination with
  | Any_combination -> eligible_combinations_to_extend
  | No_two_subterms_from_the_same_term ->
      Set_projected_subterms.filter
        (fun projected_subterms ->
          let last_added_projected_subterm = List.nth_opt projected_subterms 0 in
          match last_added_projected_subterm with
          | None -> true
          | Some (last_projector, _) ->
              last_projector.projector_term < projector_to_add.projector_term)
        eligible_combinations_to_extend

let filter_projections_with_arity_of_projection
    (max_arity : int)
    (eligible_combinations_to_extend : Set_projected_subterms.t) : Set_projected_subterms.t =
  eligible_combinations_to_extend
  |> Set_projected_subterms.filter (fun ps -> List.length ps < max_arity)

let extend_subcombination_with_new_projection
    (max_arity : int)
    (projector_to_add : Projector.t)
    (subcombinations : Set_projected_subterms.t)
    (new_projected_pattern : Projected_subterm.t) : Set_projected_subterms.t =
  let projected_subterms_of_increasable_arity =
    filter_projections_with_arity_of_projection max_arity subcombinations
  in
  let projected_subterms_to_extend =
    filter_projections_with__allowed_subterm_combination projector_to_add
      projected_subterms_of_increasable_arity
  in

  let projected_subterms_extended =
    Set_projected_subterms.map (List.cons new_projected_pattern) projected_subterms_to_extend
  in
  let updated_generated_combination =
    Set_projected_subterms.union subcombinations projected_subterms_extended
  in
  updated_generated_combination

let rec create_subcombination_and_projectors
    (max_arity : int)
    (index_currently_examined_pattern : int)
    (current_projector : Projector.t)
    (previously_generated_subcombination : Set_projected_subterms.t)
    (to_choose_from : Pattern.t list list) =
  match to_choose_from with
  | [] -> previously_generated_subcombination
  | current_children_to_choose_from :: other_children_to_choose_from -> (
      match current_children_to_choose_from with
      | [] ->
          let new_projector =
            Projector.{projector_term = current_projector.projector_term + 1; projector_child = 0}
          in
          create_subcombination_and_projectors max_arity index_currently_examined_pattern
            new_projector previously_generated_subcombination other_children_to_choose_from
      | pattern_to_choose :: rest_of_current_children_to_choose_from ->
          let projected_pattern = (current_projector, pattern_to_choose) in

          let extended_generated_combination =
            extend_subcombination_with_new_projection max_arity current_projector
              previously_generated_subcombination projected_pattern
          in

          let new_projector =
            Projector.
              {
                projector_term = current_projector.projector_term;
                projector_child = current_projector.projector_child + 1;
              }
          in
          let new_to_choose_from =
            rest_of_current_children_to_choose_from :: other_children_to_choose_from
          in
          create_subcombination_and_projectors max_arity
            (index_currently_examined_pattern + 1)
            new_projector extended_generated_combination new_to_choose_from)

let every_possible_subcombination_as_set_projected_subterms
    (max_arity : int)
    (patterns : Patterns.t) : Set_projected_subterms.t =
  let children_of_patterns = List.map Pattern.get_children patterns in
  let empty_subcombination = [] in
  let initial_set_of_combination = Set_projected_subterms.singleton empty_subcombination in
  let initial_projector = Projector.{projector_term = 0; projector_child = 0} in
  let all_subcombination =
    create_subcombination_and_projectors max_arity 1 initial_projector initial_set_of_combination
      children_of_patterns
  in
  let all_allowed_subcombination_in_reverse_order =
    Set_projected_subterms.remove empty_subcombination all_subcombination
  in
  let all_allowed_subcombination_in_correct_order =
    Set_projected_subterms.map List.rev all_allowed_subcombination_in_reverse_order
  in
  all_allowed_subcombination_in_correct_order

(* HERE IS THE RESTRICTION OF TUPLE ARITY *)
let get_maximal_arity_for_projection_of_patterns (patterns : Patterns.t) : int =
  match !Parameters_shocs_inference_projection.parameters.arity_of_projection with
  | Fixed_arity max_arity -> max_arity
  | Arity_of_parent_relation -> List.length patterns

let get_every_possible_subcombination_in_nice_structure (patterns : Patterns.t) :
    Set_projectors_and_subterms.t =
  let max_arity = get_maximal_arity_for_projection_of_patterns patterns in
  let every_combination =
    every_possible_subcombination_as_set_projected_subterms max_arity patterns
  in
  let every_combination_unzipped =
    every_combination |> Set_projected_subterms.to_list
    |> List.map from_projected_subterms_to_projectors_and_subterms
    |> Set_projectors_and_subterms.of_list
  in
  every_combination_unzipped

let add_subcombinations_if_not_already_there
    (patterns : Patterns.t)
    (accumulator : Map_patterns_projection.t) : Map_patterns_projection.t =
  match Map_patterns_projection.find_opt patterns accumulator with
  | Some _ -> accumulator
  | None ->
      Map_patterns_projection.add patterns
        (get_every_possible_subcombination_in_nice_structure patterns)
        accumulator

let one_step_compute_subcombinations_of_patterns
    ((accumulator, set_of_patterns_to_add) : Map_patterns_projection.t * Set_patterns.t) :
    Map_patterns_projection.t * Set_patterns.t =
  let new_map =
    Set_patterns.fold add_subcombinations_if_not_already_there set_of_patterns_to_add accumulator
  in
  let subpatterns =
    set_of_patterns_to_add |> Set_patterns.to_list
    |> List.map (fun patterns -> Map_patterns_projection.find patterns new_map)
    |> List.map (fun projectors_and_subterms ->
           projectors_and_subterms |> Set_projectors_and_subterms.to_list |> List.map snd
           |> Set_patterns.of_list)
    |> Set_patterns.list_union
  in
  let patterns_already_handled = Map_patterns_projection.domain new_map in
  let subpatterns_to_handle =
    Set_patterns.filter
      (Stdlib.Fun.negate ((Stdlib.Fun.flip Set_patterns.mem) patterns_already_handled))
      subpatterns
  in
  (new_map, subpatterns_to_handle)

let compute_all_subcombination_of_patterns (all_initial_patterns : Set_patterns.t) =
  let initial_accumulator = Map_patterns_projection.empty in
  Misc.Others.fixpoint_from Map_patterns_projection_and_set_patterns.equal
    one_step_compute_subcombinations_of_patterns
    (initial_accumulator, all_initial_patterns)

let get_every_subcombination_and_projectors_of_patterns (all_initial_patterns : Set_patterns.t) =
  let all_projections, all_patternTuple_left =
    compute_all_subcombination_of_patterns all_initial_patterns
  in
  let () = assert (Set_patterns.is_empty all_patternTuple_left) in
  let all_patternTuple = Map_patterns_projection.domain all_projections in
  let all_projectorTuple =
    all_projections |> Map_patterns_projection.codomain |> Set_set_projectors_and_subterms.to_list
    |> List.map Set_projectors_and_subterms.to_list
    |> List.map (List.map fst)
    |> List.map Set_projectorTuple.of_list
    |> Set_projectorTuple.list_union
  in
  (all_projections, all_patternTuple, all_projectorTuple)
