(*
   (c : Format.formatter)
   (map_term_label : Map_term_label.t)
   (map_patternsLabel_functions : Map_label_functions.t)
   (all_projections : Map_patterns_projection.t)
   (all_patternsTuple_as_set : Set_patterns.t)
   (all_projectorTuple : Set_projectorTuple.t)
*)
val pp_all_regarding_terms :
  Format.formatter ->
  Modules_used_for_translation.Map_term_label.t ->
  Modules_used_for_translation.Map_label_functions.t ->
  Modules_used_for_translation.Map_patterns_projection.t ->
  Term.Aliases.Set_patterns.t ->
  Modules_used_for_translation.Set_projectorTuple.t ->
  unit
