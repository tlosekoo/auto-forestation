open Tree_tuple_formalisms
open Io_clingo
open Io_clingo.Output_facts
open Term
open Modules_used_for_translation
open Rule_filters
open Fact_manipulation
open Arg_converter

let split_rules_related_facts_for_one_rule (rule_related_facts_of_one_id : Output_facts.t) :
    Fact.t * Fact.t list * Fact.t list =
  let rule_fact =
    match List.filter is_fact_rule rule_related_facts_of_one_id with
    | [rule_fact] -> rule_fact
    | _ -> assert false
  in
  let facts_for_rule_head_symbol =
    List.filter is_fact_rule_head_symbol rule_related_facts_of_one_id
  in
  let facts_for_formal_atom_in_body =
    List.filter is_fact_rule_formal_atom_in_body rule_related_facts_of_one_id
  in
  (rule_fact, facts_for_rule_head_symbol, facts_for_formal_atom_in_body)

let from_rule_fact_to_relation (map_arg_relation : Map_arg_relation.t) (rule_fact : Fact.t) :
    Typed_relation_symbol.t =
  let relation_arg = extract_second_argument_of_fact rule_fact in
  Map_arg_relation.find relation_arg map_arg_relation

let from_rules_formal_body_atoms_to_body
    (map_arg_formalAtom : Map_arg_bodyAtom.t)
    (facts_for_formal_atom_in_body : Fact.t list) : Shoc_body.t =
  let formal_atoms_args_in_body =
    List.map extract_second_argument_of_fact facts_for_formal_atom_in_body
  in
  (* let () =
       Format.fprintf Format.std_formatter
         "From rules formal body atoms to body:\nMap:\n%a\n\nfacts:\n%a\n\n" Map_arg_bodyAtom.pp
         map_arg_formalAtom Output_facts.pp_fact_list facts_for_formal_atom_in_body
     in *)
  let formal_atoms_in_body =
    Shoc_body.of_list
      (List.map
         (fun arg_representing_atom ->
           Map_arg_bodyAtom.find arg_representing_atom map_arg_formalAtom)
         formal_atoms_args_in_body)
  in
  formal_atoms_in_body

let from_rules_head_function_to_head_functions
    (map_label_function : Map_label_function.t)
    (facts_head_symbol : Fact.t list) : Typed_functions.t =
  let from_function_arg_to_function function_arg =
    Map_label_function.find (convert_arg_into_string function_arg) map_label_function
  in
  let sorted_args = from_facts_id_i_ithElement_to_ordered_list_of_elements facts_head_symbol in
  let functions = List.map from_function_arg_to_function sorted_args in
  functions

let create_rrc_from_rules_related_facts_one_id
    (map_label_function : Map_label_function.t)
    (map_arg_relation : Map_arg_relation.t)
    (map_arg_formalAtom : Map_arg_bodyAtom.t)
    (rule_related_facts_of_one_id : Output_facts.t) : Shoc.t =
  let rule_fact, facts_for_rule_head_symbol, facts_for_formal_atom_in_body =
    split_rules_related_facts_for_one_rule rule_related_facts_of_one_id
  in

  let relation = from_rule_fact_to_relation map_arg_relation rule_fact in
  let head_functions =
    from_rules_head_function_to_head_functions map_label_function facts_for_rule_head_symbol
  in
  let formal_atoms_in_body =
    from_rules_formal_body_atoms_to_body map_arg_formalAtom facts_for_formal_atom_in_body
  in
  Shoc.create relation head_functions formal_atoms_in_body

let parse_rule_facts
    (map_label_function : Map_label_function.t)
    (map_arg_relation : Map_arg_relation.t)
    (map_arg_formalAtom : Map_arg_bodyAtom.t)
    (facts : Output_facts.t) : Shocs.t =
  let rule_related_facts = List.filter is_fact_rule_related facts in
  let rule_related_facts_separated_by_rule = separate_facts_by_first_arg rule_related_facts in
  (* let () =
       Format.fprintf Format.std_formatter "facts for each rule:\n%a\n" Map_arg_facts.pp
         rule_related_facts_separated_by_rule
     in *)
  rule_related_facts_separated_by_rule |> Map_arg_facts.to_list |> List.map snd
  |> List.map
       (create_rrc_from_rules_related_facts_one_id map_label_function map_arg_relation
          map_arg_formalAtom)
  |> Shocs.of_list

let from_relation_type_facts_to_types (relation_type_facts : Fact.t list) : Datatypes.t =
  let from_datatype_arg_into_datatype (datatype_arg : Arg.t) =
    Datatype.create (convert_arg_into_string datatype_arg)
  in
  let datatype_args = from_facts_id_i_ithElement_to_ordered_list_of_elements relation_type_facts in
  let datatypes = List.map from_datatype_arg_into_datatype datatype_args in
  datatypes

let create_relation_from_corresponding_facts (relation_related_facts_of_one_id : Fact.t list) :
    Typed_relation_symbol.t =
  let relation_fact =
    match List.filter is_relation_fact relation_related_facts_of_one_id with
    | [relation_fact] -> relation_fact
    | _ -> assert false
  in
  let relation_name = convert_arg_into_string (extract_first_argument_of_fact relation_fact) in
  let relation_type_facts = List.filter is_relation_type_fact relation_related_facts_of_one_id in
  let datatypes = from_relation_type_facts_to_types relation_type_facts in

  Typed_relation_symbol.create (Symbol.create relation_name) datatypes

let parse_all_relation_symbols (all_facts : Fact.t list) : Map_arg_relation.t =
  let facts_about_relation_types = List.filter is_relation_related_fact all_facts in
  let relation_related_facts_separated_by_relation_name =
    separate_facts_by_first_arg facts_about_relation_types
  in
  let labeled_relations =
    relation_related_facts_separated_by_relation_name |> Map_arg_facts.to_list
    |> List.map (Misc.Pair.map_snd create_relation_from_corresponding_facts)
    |> Map_arg_relation.of_list
  in
  labeled_relations

let from_parameter_pair_to_projector (pair : Arg.t) : Projector.t =
  match pair with
  | Tuple [projector_term_arg; projector_child_arg] ->
      let projector_term = extract_int_from_numArg projector_term_arg in
      let projector_child = extract_int_from_numArg projector_child_arg in
      Projector.{projector_term; projector_child}
  | _ -> assert false

let parse_one_formal_atom
    (map_arg_relation : Map_arg_relation.t)
    (facts_formal_atom_related_for_one_id : Fact.t list) : Formal_atom.t =
  let formal_atom_fact =
    match List.filter is_fact_formal_atom facts_formal_atom_related_for_one_id with
    | [formal_atom_fact] -> formal_atom_fact
    | _ -> assert false
  in
  let formal_atom_parameter_facts =
    List.filter is_fact_formal_atom_parameter facts_formal_atom_related_for_one_id
  in
  let relation_arg = extract_second_argument_of_fact formal_atom_fact in
  let predicate = Map_arg_relation.find relation_arg map_arg_relation in
  let parameters_ordered =
    from_facts_id_i_ithElement_to_ordered_list_of_elements formal_atom_parameter_facts
  in
  let projectors = List.map from_parameter_pair_to_projector parameters_ordered in
  let atom = Formal_atom.{predicate; projectors} in
  (* let () =
       Format.fprintf Format.std_formatter "facts:\n%a\n\nprojectors:\n%a\n\nbodu atom:\n%a\n\n"
         Output_facts.pp_fact_list facts_formal_atom_related_for_one_id
         (Printing.pp_list_and_brackets Rrc.Projector.pp)
         projectors
         Rrc.Body_atom.pp atom
     in *)
  atom

let parse_all_formalAtom (map_arg_relation : Map_arg_relation.t) (facts : Fact.t list) :
    Map_arg_bodyAtom.t =
  let facts_about_formal_atom_related = List.filter is_fact_formal_atom_related facts in
  let formal_atoms_splitted_by_id = separate_facts_by_first_arg facts_about_formal_atom_related in
  let arg_to_formalAtom =
    Map_arg_facts.map (parse_one_formal_atom map_arg_relation) formal_atoms_splitted_by_id
  in
  (* let () = Format.fprintf Format.std_formatter "%a\n" Map_arg_bodyAtom.pp arg_to_formalAtom in *)
  arg_to_formalAtom
