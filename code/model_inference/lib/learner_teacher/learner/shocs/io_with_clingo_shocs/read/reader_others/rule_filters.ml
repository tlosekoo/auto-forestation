open Io_clingo
open Io_clingo.Output_facts
open Io_clingo.Output_facts.Fact

(* Relation *)
let is_relation_fact (fact : Fact.t) : bool = fact.predicate = Clingo_words.relation_symbol
let is_relation_type_fact (fact : Fact.t) : bool = fact.predicate = Clingo_words.relation_type

let is_relation_related_fact (fact : Fact.t) : bool =
  is_relation_fact fact || is_relation_type_fact fact

(* rule *)
let is_fact_rule (fact : Output_facts.Fact.t) : bool = fact.predicate = Clingo_words.rule

let is_fact_rule_head_symbol (fact : Output_facts.Fact.t) : bool =
  fact.predicate = Clingo_words.rule_head_symbol

let is_fact_rule_formal_atom_in_body (fact : Output_facts.Fact.t) : bool =
  fact.predicate = Clingo_words.rule_formal_atom_in_body

let is_fact_rule_related (fact : Fact.t) : bool =
  is_fact_rule fact || is_fact_rule_head_symbol fact || is_fact_rule_formal_atom_in_body fact

(* let filter_facts_rule = List.filter is_fact_rule
   let filter_facts_related_rule = List.filter is_fact_related_to_rules_facts *)
let is_fact_formal_atom (fact : Fact.t) : bool = fact.predicate = Clingo_words.formal_atom

let is_fact_formal_atom_parameter (fact : Fact.t) : bool =
  fact.predicate = Clingo_words.formal_atom_parameter

let is_fact_formal_atom_related (fact : Fact.t) : bool =
  is_fact_formal_atom fact || is_fact_formal_atom_parameter fact
