open Io_clingo.Output_facts

let rec convert_arg_into_string (arg : Arg.t) : string =
  match arg with
  | Sym str -> str
  | Str str -> str
  | Num n -> string_of_int n
  | Tuple args ->
      List.fold_left (fun s1 -> ( ^ ) (s1 ^ "_")) "" (List.map convert_arg_into_string args)

let extract_int_from_numArg (arg : Arg.t) : int =
  match arg with
  | Num n -> n
  | _ -> assert false
