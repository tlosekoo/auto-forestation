open Io_clingo
open Io_clingo.Output_facts
open Io_clingo.Output_facts.Fact
open Modules_used_for_translation
open Arg_converter

let extract_first_argument_of_fact (fact : Fact.t) : Arg.t = List.hd fact.args
let extract_second_argument_of_fact (fact : Fact.t) : Arg.t = List.nth fact.args 1

let filter_on_first_argument (arg : Arg.t) (fact : Fact.t) : bool =
  Arg.equal (extract_first_argument_of_fact fact) arg

let separate_facts_by_first_arg (related_facts : Output_facts.t) : Map_arg_facts.t =
  let all_first_args =
    related_facts |> List.map extract_first_argument_of_fact |> List.sort_uniq Arg.compare
  in
  all_first_args
  |> List.map (fun arg_identifier ->
         let related_facts =
           related_facts |> List.filter (filter_on_first_argument arg_identifier)
         in
         (arg_identifier, related_facts))
  |> Map_arg_facts.of_list

let from_facts_id_i_ithElement_to_ordered_list_of_elements (facts : Fact.t list) : Arg.t List.t =
  let second_and_third_arguments =
    List.map
      (fun fact_head_symbol -> (List.nth fact_head_symbol.args 1, List.nth fact_head_symbol.args 2))
      facts
  in
  let converted_second_and_third_arguments =
    List.map (Misc.Pair.map_fst extract_int_from_numArg) second_and_third_arguments
  in
  let sorted_elements =
    converted_second_and_third_arguments
    |> List.sort (fun (rank1, _) (rank2, _) -> Int.compare rank1 rank2)
    |> List.map snd
  in
  sorted_elements
