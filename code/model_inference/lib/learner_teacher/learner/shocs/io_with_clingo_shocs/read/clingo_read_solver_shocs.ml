open Parse_everything

let parse_shocs (env : Term.Datatype_environment.t) (facts : Io_clingo.Output_facts.t) :
    Tree_tuple_formalisms.Shocs.t =
  let labeled_env = Fresh_label_generator_shocs.label_env env in
  let map_arg_relation = parse_all_relation_symbols facts in
  let map_arg_formalAtom = parse_all_formalAtom map_arg_relation facts in
  parse_rule_facts labeled_env map_arg_relation map_arg_formalAtom facts
