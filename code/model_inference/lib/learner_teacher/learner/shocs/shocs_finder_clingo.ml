type t =
  ( Tree_tuple_formalisms.Shocs.t,
    Io_clingo.Use_clingo_as_solver.stopping_reason,
    Unit.t )
  Misc.Tription.t
[@@deriving equal, compare]

let shocs_finder_analysis =
  Io_clingo.Analysis.create ~name:"shocs_finder" ~algorithm_paths:[Paths.clingo_model_finder]

let pp_instance (constraints : Clause.Aliases.Set_clause_patterns.t) =
  let pp_constraints = Model_printer_prrs.ground_constraints_into_clingo constraints in
  let pp_input (c : Format.formatter) = pp_constraints c in
  pp_input

let run_clingo_model_finder
    ?(additional_clingo_options = "")
    ~(time_limit : int)
    (constraints : Clause.Aliases.Set_clause_patterns.t) : Io_clingo.Use_clingo_as_solver.outcome =
  let output_solver =
    Io_clingo.Use_clingo_as_solver.run_analysis ~time_limit
      ~additional_clingo_options:(additional_clingo_options ^ " " ^ Parameters_clingo.to_string ())
      ~path_temporary_folder:Paths.temporary_folder shocs_finder_analysis (pp_instance constraints)
  in
  output_solver

let run_shocs_finder
    ~(time_limit : int)
    (env : Term.Datatype_environment.t)
    (constraints : Clause.Aliases.Set_clause_patterns.t) : t =
  let new_rrs =
    Misc.Tription.map_yes
      (Clingo_read_solver_shocs.parse_shocs env)
      (run_clingo_model_finder ~time_limit constraints)
  in
  new_rrs

(* FOR TESTING PURPOSE ONLY *)
type testing_stuff = {
  pp_instance : Format.formatter -> unit;
  clingo_output : Io_clingo.Use_clingo_as_solver.outcome;
  shocs_output : t;
}

let get_testing_stuff
    ~(additional_clingo_options : string)
    ~(time_limit : int)
    (env : Term.Datatype_environment.t)
    (counter_examples : Clause.Aliases.Set_clause_patterns.t) : testing_stuff =
  let pp_instance = pp_instance counter_examples in
  let clingo_output =
    run_clingo_model_finder ~additional_clingo_options ~time_limit counter_examples
  in
  let shocs_output =
    Misc.Tription.map_yes (Clingo_read_solver_shocs.parse_shocs env) clingo_output
  in
  {pp_instance; clingo_output; shocs_output}
