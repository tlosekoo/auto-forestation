type allowed_subterm_combination =
  | No_two_subterms_from_the_same_term
  | Any_combination

type arity_of_projection =
  (* | Maximal_arity_of_initial_relation *)
  | Arity_of_parent_relation
  | Fixed_arity of int

type t = {
  allowed_subterm_combination : allowed_subterm_combination;
  arity_of_projection : arity_of_projection;
}

let parameters =
  ref
    {
      allowed_subterm_combination = No_two_subterms_from_the_same_term;
      arity_of_projection = Arity_of_parent_relation;
    }
