type t = {
  parallel_mode : string;
  models : int;
}

let parameters = ref {parallel_mode = "4,split"; models = 77}

let to_string () =
  "--parallel-mode=" ^ !parameters.parallel_mode ^ " --models=" ^ Int.to_string !parameters.models
