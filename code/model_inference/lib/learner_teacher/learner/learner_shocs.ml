(** This file implements the learner part of the ICE procedure *)

open Clause.Aliases
open Model_checking.Instantiation
open Tree_tuple_formalisms

(* Real code section *)
type t = (ModelShocs.t, Io_clingo.Use_clingo_as_solver.stopping_reason, Unit.t) Misc.Tription.t
[@@deriving equal, compare]

let pp (c : Format.formatter) (model_proposition : t) =
  match model_proposition with
  | No () -> Format.fprintf c "Contradictory ground constraints"
  | Maybe _sr -> Format.fprintf c "Maybe"
  | Yes m -> Format.fprintf c "Found Model:\n%a\n" ModelShocs.pp m

(* Verbose printing section *)
let verbose_learner_is_starting () =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\n--------------------Learner is starting\n"
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

let verbose_learner_has_ended model =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\nInferred model is:\n%a\n\n" pp model
  in
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\n--------------------Learner has ended\n"
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

(* real code again *)
let create_initial_model
    (env : Term.Datatype_environment.t)
    (predicates : Set_typed_relation_symbol.t) =
  let initial_model = ModelShocs.create env predicates Shocs.empty in
  initial_model

let infer_model
    ~(time_to_stop : float option)
    (env : Term.Datatype_environment.t)
    (relations_of_model : Set_typed_relation_symbol.t)
    (constraints : Clause.Aliases.Set_clause_patterns.t) : t =
  let () = verbose_learner_is_starting () in

  let time_limit =
    Io_clingo.Use_clingo_as_solver.from_time_to_stop_to_time_available time_to_stop
  in
  let shocs = Shocs_finder_clingo.run_shocs_finder ~time_limit env constraints in
  let model = Misc.Tription.map_yes (ModelShocs.create env relations_of_model) shocs in

  let () = verbose_learner_has_ended model in
  model
