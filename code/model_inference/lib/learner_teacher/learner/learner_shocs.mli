(** This file implements the learner part of the ICE procedure *)
type t =
  ( Model_checking.Instantiation.ModelShocs.t,
    Io_clingo.Use_clingo_as_solver.stopping_reason,
    unit )
  Misc.Tription.t

include Misc.Ordered_and_printable.OP with type t := t

val create_initial_model :
  Term.Datatype_environment.t ->
  Clause.Aliases.Set_typed_relation_symbol.t ->
  Model_checking.Instantiation.ModelShocs.t

(* the parameters of `infer_model` are a time to stop and a verbosity level, and then
      - The datatype environment in which all types live
      - The set of relations that we are trying to infer
      - A set of ground examples

   It then returns a model in which every relation that we are trying to infer are mapped to a PRRS satisfying the gorund constraints, if any.
*)
val infer_model :
  time_to_stop:float option ->
  Term.Datatype_environment.t ->
  Clause.Aliases.Set_typed_relation_symbol.t ->
  Clause.Aliases.Set_clause_patterns.t ->
  t
