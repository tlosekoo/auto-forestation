open Program_to_clauses
open Approximation

type t = {
  timeout : Float.t Option.t;
  approximation_method : Approximation.approximation_method;
}
[@@deriving compare, equal]

let pp (c : Format.formatter) (p : t) : unit =
  Format.fprintf c "Timeout: %a (sec)\nApproximation method: %a\n\n"
    (Printing.pp_opt Format.pp_print_float)
    p.timeout Approximation.pp_approximation_method p.approximation_method

let parameters = ref {timeout = None; approximation_method = No_approximation}
