open Term
open Model_checking
open Clause
open Clause.Aliases

let verbose_teacher_is_starting () =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\n--------------------Teacher is starting\n"
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

let verbose_teacher_has_ended () =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\n--------------------Teacher has ended\n"
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

module Make (R : Tree_tuple_formalisms.Tree_tuple_formalism.S) = struct
  module ModelCheckingR = Model_check.Make (R)
  module ModelR = Model.Make (R)

  let check_clauses ~(time_to_stop : float option) (m : ModelR.t) (clauses : Set_clause_patterns.t)
      : Pattern_recognition.t =
    Misc.Time_logger.start_time_stamp Log.log.time_log "teacher";
    let () = verbose_teacher_is_starting () in

    let output =
      ModelCheckingR.model_check_many_formula ~time_to_stop ~_debug_mode:false m clauses
    in

    let () = verbose_teacher_has_ended () in
    Misc.Time_logger.end_time_stamp Log.log.time_log "teacher";

    output

  (* TODO use grounding functions here too *)
  let convert_sat_witness_to_learning_constraint
      (datatype_env : Term.Datatype_environment.t)
      (formula_and_substitution : Clause_patterns.t * Substitution.t) : Clause_patterns.t =
    formula_and_substitution |> Groundify_counter_examples.groundify datatype_env
    |> fun (formula, substitution) ->
    Clause_patterns.apply_substitution formula substitution
    |> Disjunctive_clause_patterns.simplify_clause_by_unification |> Option.get
  (* This Option is always Some(_) because `formula` is false in some model, so it is not trivially true. *)
end
