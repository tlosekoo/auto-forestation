open Term
open Clause
open Clause.Aliases

let groundify_remaining_variables
    (env : Datatype_environment.t)
    ((clause, subst) : Clause_patterns.t * Substitution.t) : Substitution.t =
  let almost_ground_clause = Clause_patterns_operations.apply_substitution clause subst in
  let all_atoms =
    Set_atom_patterns.union
      (Clause_patterns.get_negative almost_ground_clause)
      (Clause_patterns.get_positive almost_ground_clause)
  in
  let variables_in_need_of_groundification =
    all_atoms |> Set_atom_patterns.to_list
    |> List.map Atom_patterns.get_argument
    |> List.flatten |> List.map Pattern.get_variables |> Set_typed_symbol.list_union
  in
  variables_in_need_of_groundification |> Set_typed_symbol.to_list
  |> List.map (fun v -> (v, Datatype_environment.groundify_variable env v))
  |> Substitution.create_of_list

let groundify (env : Datatype_environment.t) ((clause, subst) : Clause_patterns.t * Substitution.t)
    : Clause_patterns.t * Substitution.t =
  (* let () = Format.fprintf Format.std_formatter "HEYYYYYYYYYY\n" in *)
  let additionnal_substitution = groundify_remaining_variables env (clause, subst) in
  let extended_substitution =
    Substitution.compose_with_undefined_is_injection subst additionnal_substitution
  in
  let projected_extended_substitution =
    Substitution.project_in (Clause_patterns.extract_variables clause) extended_substitution
  in
  (* let () =
       Format.fprintf Format.std_formatter
         "Clause is:\n%a\nSubstitution is:\n%a\nAdditional constraints are:\n%a\nResulting substitution is:\n%a\nOnce projected it is:\n%a\n"
         Clause_patterns.pp clause Substitution.pp subst Substitution.pp additionnal_substitution
         Substitution.pp extended_substitution Substitution.pp projected_extended_substitution
     in *)
  (clause, projected_extended_substitution)

let groundify_and_simplify
    (datatype_env : Term.Datatype_environment.t)
    (formula : Clause_patterns.t) : Clause_patterns.t option =
  groundify datatype_env (formula, Substitution.empty) |> fun (formula, substitution) ->
  Clause_patterns.apply_substitution formula substitution
  |> Disjunctive_clause_patterns.simplify_clause_by_unification
