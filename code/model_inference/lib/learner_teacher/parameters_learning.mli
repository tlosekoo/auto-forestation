type t = {
  timeout : float option;
  approximation_method : Program_to_clauses.Approximation.approximation_method;
}

include Misc.Ordered_and_printable.OP with type t := t

val parameters : t ref
