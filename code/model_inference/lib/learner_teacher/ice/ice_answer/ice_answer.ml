open Clause.Aliases
open Stopping_reason

module type S = sig
  module Model : Model_checking.Model.S
  module Solver_state : Solver_state.S with module Model := Model

  type t = (Model.t, stopping_reason * Solver_state.t, Set_clause_patterns.t) Misc.Tription.t

  val compare : t -> t -> int
  val equal : t -> t -> bool
  val pp_param : print_details:bool -> Format.formatter -> t -> unit
end

module Make
    (Model : Model_checking.Model.S)
    (Solver_state : Solver_state.S with module Model := Model) :
  S with module Model = Model and module Solver_state = Solver_state = struct
  module Model = Model
  module Solver_state = Solver_state

  type t = (Model.t, stopping_reason * Solver_state.t, Set_clause_patterns.t) Misc.Tription.t
  [@@deriving compare, equal]

  let pp_param ~(print_details : bool) : Format.formatter -> t -> unit =
    let pp_yes = if print_details then Model.pp else Printing.pp_ignore_arg_and_print "" in
    let pp_maybe c (stopping_reason, last_solver_state) =
      let () = Format.fprintf c " %a" pp_stopping_reason stopping_reason in
      if print_details then
        Format.fprintf c "\nLast solver state:\n\n%a\n\n\n" Solver_state.pp last_solver_state
      else
        ()
    in
    let pp_no =
      Set_clause_patterns.pp_param ~opening:"Contradictory set of ground constraints:\n{\n"
        ~closing:"\n}" ~sep:"\n"
    in
    Misc.Tription.pp pp_yes pp_maybe pp_no
end
