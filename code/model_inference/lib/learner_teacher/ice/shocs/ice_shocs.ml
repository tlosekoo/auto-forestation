open Term
open Clause.Aliases
open Ice_shocs_verbose_printing

(* TODO: use Utils for timeout *)
let rec handle_output_teacher
    (time_to_stop : float option)
    (solver_state : Solver_state_shocs.t)
    (map_clauses_outcomes : Model_checking.Pattern_recognition.t) =
  let all_clauses_are_verified =
    Clause.Aliases.Map_clause.for_all
      (fun _clause model_checkout_output -> Misc.Tription.is_no model_checkout_output)
      map_clauses_outcomes
  in

  if all_clauses_are_verified then
    let final_answer = Misc.Tription.Yes (Solver_state_shocs.get_current_best_model solver_state) in
    final_answer
  else
    let counter_examples =
      map_clauses_outcomes |> Clause.Aliases.Map_clause.bindings
      |> List.map (Misc.Pair.map_snd Misc.Tription.get_yes)
      |> List.filter_map (fun (clause, subst_opt) ->
             Option.map (fun subst -> (clause, subst)) subst_opt)
    in
    let some_clauses_are_found_false = counter_examples <> [] in
    if not some_clauses_are_found_false then
      let stopping_reasons =
        map_clauses_outcomes |> Clause.Aliases.Map_clause.bindings
        |> List.map (Misc.Pair.map_snd Misc.Tription.get_maybe)
        |> List.filter_map (fun (clause, subst_opt) ->
               Option.map (fun subst -> (clause, subst)) subst_opt)
      in
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter "Some clauses could not be checked:\n%a\n"
             (Printing.pp_assoclist_long Clause.Clause_patterns.pp
                Model_checking.Search_outcome.pp_stopping_reason)
             stopping_reasons)
      in

      let timeout_answer =
        Misc.Tription.Maybe (Stopping_reason.Other error_message, solver_state)
      in
      timeout_answer
    else
      let new_learning_constraints =
        List.map
          (Teachers.Teacher_shocs.convert_sat_witness_to_learning_constraint
             (Solver_state_shocs.get_datatype_environment solver_state))
          counter_examples
      in
      let () = verbose_ground_counterexamples new_learning_constraints in

      let updated_learning_constraints =
        Set_clause_patterns.union
          (Solver_state_shocs.get_accumulated_learning_constraints solver_state)
          (Set_clause_patterns.of_list new_learning_constraints)
      in
      let simplified_learning_constraints =
        Clause.Simplify_examples.simplify_examples updated_learning_constraints
      in
      let new_solver_state =
        Solver_state_shocs.set_accumulated_learning_constraints solver_state
          simplified_learning_constraints
      in

      infer_model_rec new_solver_state time_to_stop

and infer_model_rec (solver_state : Solver_state_shocs.t) (time_to_stop : Misc.Time.time_limit) :
    Ice_answer_shocs.t =
  let () = verbose_new_step in
  Misc.Time_logger.start_time_stamp Log.log.time_log "ice";
  Log.set_ground_examples_beginning
    (Solver_state_shocs.get_accumulated_learning_constraints solver_state);
  Log.tick_beginning_ice_loop ();

  if Misc.Time.is_time_up time_to_stop then (
    let final_answer = Misc.Tription.Maybe (Stopping_reason.TimeoutIceLoop, solver_state) in
    Misc.Time_logger.end_time_stamp Log.log.time_log "ice";
    final_answer)
  else
    let () = Misc.Time_logger.start_time_stamp Log.log.time_log "learner" in
    let candidate_model_opt =
      Learner_shocs.infer_model ~time_to_stop
        (Solver_state_shocs.get_datatype_environment solver_state)
        (Solver_state_shocs.get_relations_to_infer solver_state)
        (Solver_state_shocs.get_accumulated_learning_constraints solver_state)
    in
    let () = Misc.Time_logger.end_time_stamp Log.log.time_log "learner" in

    Log.tick_after_model_generation ();
    Log.set_learner_answer candidate_model_opt;
    match candidate_model_opt with
    | No () ->
        let final_answer =
          Misc.Tription.No (Solver_state_shocs.get_accumulated_learning_constraints solver_state)
        in
        Misc.Time_logger.end_time_stamp Log.log.time_log "ice";
        final_answer
    | Maybe _sr ->
        let final_answer = Misc.Tription.Maybe (Stopping_reason.TimeoutLearner, solver_state) in
        Misc.Time_logger.end_time_stamp Log.log.time_log "ice";
        final_answer
    | Yes candidate_model ->
        let teachers_output =
          Teachers.Teacher_shocs.check_clauses ~time_to_stop candidate_model
            (Solver_state_shocs.get_clauses_program_and_property solver_state)
        in
        let new_solver_state =
          Solver_state_shocs.set_current_best_model solver_state candidate_model
        in
        Misc.Time_logger.end_time_stamp Log.log.time_log "ice";
        Log.tick_after_model_checking ();
        Log.set_teachers_answer teachers_output;
        Log.validate_complete_step ();
        let () = verbose_teacher_output teachers_output in
        handle_output_teacher time_to_stop new_solver_state teachers_output

(** If the set of chcs is satisfiable, returns a model. If it is not, returns 'None'. Do not necessarily terminate *)
let infer_model
    ?(timeout = None)
    (env : Datatype_environment.t)
    (* (predicates_of_the_program : Set_typed_relation_symbol.t) *)
      (clauses : Set_clause_patterns.t) : Ice_answer_shocs.t =
  let () = verbose_inference_starting clauses in
  let initial_solver_state = Solver_state_shocs.initialize env clauses in
  let time_to_stop = Option.map (( +. ) (Unix.gettimeofday ())) timeout in
  let model_solution = infer_model_rec initial_solver_state time_to_stop in
  let () = Log.set_ending_log model_solution in
  let () = verbose_solution_printing model_solution in
  model_solution
