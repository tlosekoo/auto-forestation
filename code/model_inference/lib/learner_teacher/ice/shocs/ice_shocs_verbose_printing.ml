open Clause
open Clause.Aliases

let verbose_inference_starting (clauses : Set_clause_patterns.t) =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter
        "\n\n\n----------------------------------------------\n\nStarting inference of model on clause system %a\n"
        Set_clause_patterns.pp clauses
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

let verbose_solution_printing (model_solution : Ice_answer_shocs.t) =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter
        "\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\nFound model:\n[[[%a\n]]]"
        (Ice_answer_shocs.pp_param ~print_details:true)
        model_solution
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

let verbose_new_step =
  if !Verbosity.is_verbose then (
    Format.fprintf Format.std_formatter "\nModel inference procedure, step -%d, is beginning.\n"
      (Log.get_number_of_past_ice_iterations Log.log);
    Format.pp_print_flush Format.std_formatter ())

let verbose_teacher_output (teachers_output : Model_checking.Pattern_recognition.t) =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "\n\n@@@@Teacher said:\n%a\n"
        Model_checking.Pattern_recognition.pp teachers_output
  in
  let () = if !Verbosity.is_verbose then Format.pp_print_flush Format.std_formatter () in
  ()

let verbose_ground_counterexamples ground_counter_examples =
  let () =
    if !Verbosity.is_verbose then
      Format.fprintf Format.std_formatter "Counter-examples are:\n%a\n"
        (Printing.pp_list_sep "\n" Disjunctive_clause_patterns.pp)
        ground_counter_examples
  in
  ()
