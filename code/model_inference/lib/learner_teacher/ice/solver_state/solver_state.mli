module type S = sig
  module Model : Model_checking.Model.S
  include Misc.Ordered_and_printable.OP

  val initialize : Term.Datatype_environment.t -> Clause.Aliases.Set_clause_patterns.t -> t

  (* Getters *)
  val get_datatype_environment : t -> Term.Datatype_environment.t
  val get_clauses_program_and_property : t -> Clause.Aliases.Set_clause_patterns.t
  val get_relations_to_infer : t -> Term.Aliases.Set_typed_relation_symbol.t
  val get_current_best_model : t -> Model.t
  val get_accumulated_learning_constraints : t -> Clause.Aliases.Set_clause_patterns.t

  (* setters *)
  val set_current_best_model : t -> Model.t -> t
  val set_accumulated_learning_constraints : t -> Clause.Aliases.Set_clause_patterns.t -> t
end
