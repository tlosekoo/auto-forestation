open Clause.Aliases
open Model_checking.Instantiation

type t = {
  datatype_environment : Term.Datatype_environment.t;
  clauses_to_check_for_satisfiability : Set_clause_patterns.t;
  relations_to_infer : Set_typed_relation_symbol.t;
  accumulated_learning_constraints : Set_clause_patterns.t;
  current_best_model : ModelShocs.t;
}
[@@deriving compare, equal]

let pp (c : Format.formatter) (s : t) =
  Format.fprintf c
    "Clauses:\n%a\n\nAccumulated learning constraints:\n%a\n\nCurrent best model:\n%a\n\n\n"
    Set_clause_patterns.pp s.clauses_to_check_for_satisfiability
    (Set_clause_patterns.pp_param ~opening:"{\n" ~closing:"\n}" ~sep:"\n")
    s.accumulated_learning_constraints ModelShocs.pp s.current_best_model

let initialize_with_ground_instances_of_clauses = false

let initialize_learning_constraints
    (datatype_env : Term.Datatype_environment.t)
    (all_clauses : Set_clause_patterns.t) : Set_clause_patterns.t =
  if initialize_with_ground_instances_of_clauses then
    Set_clause_patterns.filter_map
      (Groundify_counter_examples.groundify_and_simplify datatype_env)
      all_clauses
  else
    Set_clause_patterns.empty

let initialize
    (datatype_environment : Term.Datatype_environment.t)
    (clauses_to_check_for_satisfiability : Set_clause_patterns.t) : t =
  let accumulated_learning_constraints =
    initialize_learning_constraints datatype_environment clauses_to_check_for_satisfiability
  in
  (* let () =
       Format.fprintf Format.std_formatter "Initial ground constraints:\n%a\n" Set_clause_patterns.pp
         accumulated_learning_constraints
     in *)
  let relations_to_infer =
    clauses_to_check_for_satisfiability |> Set_clause_patterns.to_list
    |> List.map Clause.Clause_patterns.extract_non_equality_predicates
    |> Set_typed_relation_symbol.list_union
  in
  let current_best_model =
    Learner_shocs.create_initial_model datatype_environment relations_to_infer
  in

  let initial_solver_state =
    {
      datatype_environment;
      clauses_to_check_for_satisfiability;
      relations_to_infer;
      accumulated_learning_constraints;
      current_best_model;
    }
  in
  initial_solver_state

let get_current_best_model (solver_state : t) : ModelShocs.t = solver_state.current_best_model

let get_datatype_environment (solver_state : t) : Term.Datatype_environment.t =
  solver_state.datatype_environment

let get_relations_to_infer (solver_state : t) : Set_typed_relation_symbol.t =
  solver_state.relations_to_infer

let get_accumulated_learning_constraints (solver_state : t) : Set_clause_patterns.t =
  solver_state.accumulated_learning_constraints

let get_clauses_program_and_property (solver_state : t) : Set_clause_patterns.t =
  solver_state.clauses_to_check_for_satisfiability

(* Setters *)
let set_current_best_model (solver_state : t) (new_model : ModelShocs.t) : t =
  {solver_state with current_best_model = new_model}

let set_accumulated_learning_constraints
    (solver_state : t)
    (new_learning_constraints : Set_clause_patterns.t) : t =
  {solver_state with accumulated_learning_constraints = new_learning_constraints}
