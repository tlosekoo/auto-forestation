type stopping_reason =
  | TimeoutLearner
  | TimeoutIceLoop
  | TimeoutTeacher of Model_checking.Search_outcome.stopping_reason
  | Other of String.t
[@@deriving compare, equal]

let pp_stopping_reason (c : Format.formatter) (sr : stopping_reason) : unit =
  match sr with
  | TimeoutLearner -> Format.pp_print_string c "timeout during learner"
  | TimeoutIceLoop -> Format.pp_print_string c "timeout at the beginning of a new ICE loop"
  | TimeoutTeacher stop ->
      Format.fprintf c "timeout during teacher: %a" Model_checking.Search_outcome.pp_stopping_reason
        stop
  | Other sr -> Format.fprintf c "other: %s" sr
