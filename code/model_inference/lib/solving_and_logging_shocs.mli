val solve_and_return_log :
  ?name:string option ->
  Program_to_clauses.Learning_problem.t ->
  (* Parameters_learning.t -> *)
  Ice_answer_shocs.t * Log.t

val pp_log_in_file : print_short_output:bool -> string -> Log.t -> unit
