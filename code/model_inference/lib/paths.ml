(* let clingo_model_generalisation_directory : string =
   match My_external_files.Sites.clingo_model_generalisation with
   | [folder_path] -> folder_path ^ "/"
   | _ -> assert false *)

let clingo_model_finder_directory : string =
  match My_external_files.Sites.clingo_model_finder with
  | [folder_path] -> folder_path ^ "/"
  | _ -> assert false

let testing_folder : string =
  match My_external_files.Sites.files_test with
  | [folder_path] -> folder_path ^ "/"
  | _ -> assert false

(* let clingo_model_generalisation = clingo_model_generalisation_directory ^ "model_generalisation.lp" *)
let clingo_model_finder = clingo_model_finder_directory ^ "model_finder.lp"
let temporary_folder = "/tmp/"
