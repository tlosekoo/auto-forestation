(set-logic HORN)


(declare-datatypes ((nat 0) (nat_bin_tree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(leaf)
			(node (hd nat) (ls nat_bin_tree) (rs nat_bin_tree))
		)
	)
)


(assert (forall ((i nat) (t1 nat_bin_tree) (t2 nat_bin_tree))
			(= t1 (node i t1 t2))))


(check-sat)
