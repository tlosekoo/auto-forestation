(set-logic HORN)


(declare-datatypes ((nat 0) (elt 0) (elt_bin_tree 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(a)
			(b)
		)
		(
			(leaf)
			(node (hd elt) (ls elt_bin_tree) (rs elt_bin_tree))
		)
	)
)

(define-fun-rec swap ((t elt_bin_tree)) elt_bin_tree
	(
		match t
		(
			(leaf leaf)
			((node e t1 t2) (node e (swap t2) (swap t1)))
		)
	)
)

(define-fun-rec member ((e elt)(t elt_bin_tree)) Bool
	(
		match t
		(
			(leaf false)
			((node e2 t1 t2) (ite (= e e2) true (ite (member e t1) true (member e t2))))
		)
	)
)

(assert (forall ((e elt) (t1 elt_bin_tree))
			(=> (member e t1) (member e (swap t1)))))


(check-sat)
