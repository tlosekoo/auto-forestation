TODO : UPDATE THIS FILE


This logic program infers a regular relational system that generalizes a set of concrete (ground) Horn clauses.

# Predicates used in this program #

## Input predicates ##

The following predicates form the input:

- function(SYMBOL, OUTPUT_TYPE, ARITY)
- function_input_type(SYMBOL, I, ITH_INPUT_TYPE)
- term(ID, ROOT_SYMBOL)
- term_child(ID, I, ITH_CHILD)
- relation(RELATION, ARITY)
- relation_type(RELATION, I, ITH_DATATYPE)
- input_atom(ID, RELATION_SYMBOL)
- input_atom_parameter(ID, I, TERM)
- input_litteral(ID, POSITIVITY, ATOM_ID)
- ground_clause(ID)
- ground_clause_litteral(ID, LITTERAL)

We use the assumption that every term is given a unique name and that all its subterms are also present.



## Solving predicates ##

### Representation of rules ###

- rule(ID, HEAD_PREDICATE)
- rule_head_symbol(ID, I, ITH_HEAD_SYMBOL)
- rule_formal_atom_in_body(ID, ID_FORMAL_ATOM_IN_BODY)

- formal_atom(ID, RELATION)
- formal_atom_parameter(ID, I, (PROJECTOR_TERM, PROJECTOR_CHILD))


### Representation of concrete atom ###

- concrete_atom(ID, RELATION_SYMBOL).
- concrete_atom_parameter(ID, I, ITH_TERM).

### Checking trueness of concrete atom ###

- maybe_true(ID_CONCRETE_ATOM, ID_RULE)
- not_true_among_maybe_true(ID_CONCRETE_ATOM, ID_RULE)
- true_by_rule(ID_CONCRETE_ATOM, ID_RULE)
- true_by_some_rule(ID_CONCRETE_ATOM)


### Constraints for satisfying the Horn clauses ###

- mbt_litteral(LITTERAl)
- mbt_concrete_atom(CONCRETE_ATOM)
- mbf_concrete_atom(CONCRETE_ATOM)

### For generating necessary concrete atoms ###

- concrete_atom_matches_rule_predicate_symbol(ID_CONCRETE_ATOM, ID_RULE)
- concrete_atom_matches_ith_rule_symbol(ID_CONCRETE_ATOM, ID_RULE, I)
- concrete_atm_matches_rule(ID_CONCRETE_ATOM, ID_RULE)

### For generating rules ###

- rule_generator(ID_NEW_RULE, ID_CONCRETE_ATOM)
- formal_atom_generator(ID_FORMAL_ATOM_IN_BODY, ID_CONCRETE_ATOM, NEW_BINARY_VARIABLES_ENCODING, NEW_I, NEW_PROJECTOR_TERM, NEW_PROJECTOR_CHILD, true) 


# How it works #

- For every (concrete) Horn clause that must be satisfied, an atom that must be true is chosen.
- Every atom that is chosen to be true yields a concrete atom needing to be either true or false.
- For every concrete atom that must be true, a rule may be created (one maximum, is it a problem?), together with new atoms that must be true.
- Every concrete relation that must be true is indeed true.
- Every concrete relation aaplication that must be false is indeed false.



# Contraintes ne restreignant pas l'expressivité #

- La formule de minimisation
- Élimination de symétrie (à trouver)

# Constraintes restreignant l'expressivité #

- Ne pas avoir deux atomes de corps de règle sur le même sous-ensemble de variables
- Limiter l'arité des atomes dans le corps des règles
- Imposer l'ordre des variables dans les prédicats

