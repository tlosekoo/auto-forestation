open Program_to_clauses

let main ()  = 
let file_to_parse = Paths.files_for_testing_folder ^ "allzero_car_is.smt2" in
let learning_problem = Smtlib_reader.smtlib_to_clauses  file_to_parse in
Format.fprintf Format.std_formatter "%a" Learning_problem.pp learning_problem

  let () = main()