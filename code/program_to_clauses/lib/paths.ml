let files_for_testing_folder : string =
  match My_external_files.Sites.files_test with
  | [folder_path] -> folder_path ^ "/"
  | _ -> assert false
