open Type_converter
open Intermediate_datatypes

(* include Converter_smtlib_clause *)
open Term
open Smtlib_utils.V_2_6
open Clause
open Clause.Aliases

(* Converts the signature of an SMTLIB function declaration into a function (or relation if the output type is a boolean) *)
let convert_parsed_function_declaration_into_predicate_or_function (decl : Ast.ty Ast.fun_decl) :
    predicate_or_function =
  let name = Symbol.create decl.fun_name in
  let body_datatypes = List.map convert_smtlib_type_into_datatype decl.fun_args in

  if is_ty_bool decl.fun_ret then
    let predicate_symbol = Typed_relation_symbol.create name body_datatypes in
    Pred predicate_symbol
  else
    let symbol_datatype = convert_smtlib_type_into_datatype decl.fun_ret in
    let associated_Typed_symbol = Typed_function.create name symbol_datatype body_datatypes in
    Func associated_Typed_symbol

let get_and_register_symbol_of_function_definition (def : Ast.fun_def) : predicate_or_function =
  let declaration = def.fr_decl in
  let types_of_args = List.map snd declaration.fun_args in
  let declaration_without_named_variables = Ast.{declaration with fun_args = types_of_args} in
  let function_or_predicate =
    convert_parsed_function_declaration_into_predicate_or_function
      declaration_without_named_variables
  in
  let () =
    match function_or_predicate with
    | Func func -> Mutable_data.add_labeled_function func
    | Pred pred -> Mutable_data.add_labeled_relation pred
  in
  function_or_predicate

let convert_definition (def : Ast.fun_def) : predicate_or_function * Set_clause_patterns.t =
  let decl = def.fr_decl in
  let function_or_relation = get_and_register_symbol_of_function_definition def in

  let definition = def.fr_body in
  let variables_env = Parse_datatypes_declaration.convert_typed_vars_into_var_env decl.fun_args in
  let function_parameters =
    List.map Pattern.from_var
      (List.map
         (fun (var_name, smtlib_var_type) ->
           let datatype = convert_smtlib_type_into_datatype smtlib_var_type in
           Typed_symbol.create (Symbol.create var_name) datatype)
         decl.fun_args)
  in
  let handle_one_possibility_function func fic_of_one_case =
    let guards, value_of_body =
      decompose_clause_in_construction_that_uses_patterns fic_of_one_case
    in
    let parameters_of_f_as_predicate__value = function_parameters @ [value_of_body] in
    let f_as_predicate_symbol = Typed_relation_symbol.from_function func in
    let f_atom_value =
      Atom_patterns.create_from_predicate_symbol f_as_predicate_symbol
        parameters_of_f_as_predicate__value
    in
    let fic =
      create_clause_in_construction_from_precondition_and_atom guards
        (Extended_atom_patterns.Atom f_atom_value)
    in

    (* CONJUNCTION INTO DISJUNCTION *)
    let chc_of_implication = clause_in_construction_into_implicative_clause fic in
    Option.to_list chc_of_implication
  in

  let handle_one_possibility_predicate pred fic_of_one_case =
    let guards, value_of_body =
      decompose_clause_in_construction_that_uses_formula fic_of_one_case
    in
    let parameters_of_predicate = function_parameters in
    let pred_atom = Atom_patterns.create_from_predicate_symbol pred parameters_of_predicate in
    let all_preconditions_including_atom = Clause_patterns.add_positive pred_atom guards in
    let all_preconditions_including_value_of_predicate_opt =
      Conjunctive_clause_patterns.add_positive_ExtendedAtomPatterns value_of_body guards
    in
    (* for iff *)
    let fic_of_reverse_implication =
      create_clause_in_construction_from_precondition_and_atom all_preconditions_including_atom
        value_of_body
    in
    let clause_of_reverse_implication =
      clause_in_construction_into_implicative_clause fic_of_reverse_implication
    in

    let fic_of_implication_opt =
      Option.map
        (fun guards ->
          create_clause_in_construction_from_precondition_and_atom guards
            (Extended_atom_patterns.Atom pred_atom))
        all_preconditions_including_value_of_predicate_opt
    in

    let clause_of_implication =
      Option.bind fic_of_implication_opt clause_in_construction_into_implicative_clause
    in
    let clauses =
      List.append
        (Option.to_list clause_of_implication)
        (Option.to_list clause_of_reverse_implication)
    in
    clauses
  in

  match function_or_relation with
  | Func func ->
      let possible__preconditions_and_value_of_function_body =
        Converter_smtlib_term.convert_smtlib_unquantified_term ~returns_boolean:false variables_env
          definition
      in
      let definition =
        possible__preconditions_and_value_of_function_body
        |> List.map (handle_one_possibility_function func)
        |> List.flatten
      in
      (Func func, Set_clause_patterns.of_list definition)
  | Pred pred ->
      let possible__preconditions_and_value_of_predicate_body =
        Converter_smtlib_term.convert_smtlib_unquantified_term ~returns_boolean:true variables_env
          definition
      in
      let definition =
        possible__preconditions_and_value_of_predicate_body
        |> List.map (handle_one_possibility_predicate pred)
        |> List.flatten
      in

      (Pred pred, Set_clause_patterns.of_list definition)

let convert_parsed_definition_rec_or_not (def_stmt : Ast.stmt) :
    predicate_or_function * Set_clause_patterns.t =
  let definition =
    match def_stmt with
    | Ast.Stmt_fun_def def -> def
    | Ast.Stmt_fun_rec def -> def
    | _ -> raise (Invalid_argument "Should never happen")
  in
  convert_definition definition

let convert_smtlib_definitions_into_corresponding_definitions (defs : Ast.stmt list) :
    (predicate_or_function * Set_clause_patterns.t) list =
  let definitions =
    List.fold_left
      (fun past_definitions fun_def ->
        let new_definition = convert_parsed_definition_rec_or_not fun_def in
        new_definition :: past_definitions)
      [] defs
  in
  definitions
(* let convert_parsed_definitions_into_their_environment_and_corresponding_definitions
     (defs : Ast.stmt list) : Learning_problem.LabeledDefs.t =
   let definitions =
     List.fold_left
       (fun (partial_function_env, partial_predicate_env, definitions) fun_def ->
         let pred_or_func, chcs_definition = convert_parsed_definition_rec_or_not fun_def in
         match pred_or_func with
         | Pred pred ->
             let name = Typed_relation_symbol.to_string pred in
             let new_definition = Predicate_definition.PredicateDefinition chcs_definition in
             let partial_predicate_env_updated = Map_string.add name pred partial_predicate_env in
             (* let updated_map_predicate_to_name =
                  Map_predicate_symbol.add pred name map_predicate_to_name
                in *)
             let updated_definitions = List.append definitions [(pred, new_definition)] in

             (partial_function_env, partial_predicate_env_updated, updated_definitions)
         | Func func ->
             let name = Typed_function.to_string func in
             let functionality_constraint = create_determinism_constraint_for_function func in

             let new_definition =
               Predicate_definition.FunctionDefinition
                 {core = chcs_definition; functionality = functionality_constraint}
             in
             (* let associated_predicate = Typed_relation_symbol.from_function func in *)
             (* let updated_map_definition = Map_string.add name new_definition map_definitions in *)
             let partial_function_env_updated = Map_string.add name func partial_function_env in
             let pred = Typed_relation_symbol.from_function func in
             let updated_definitions = List.append definitions [(pred, new_definition)] in
             (* let updated_map_predicate_to_name =
                  Map_predicate_symbol.add pred name map_predicate_to_name
                in
             *)
             (partial_function_env_updated, partial_predicate_env, updated_definitions))
       (Map_string.empty, Map_string.empty, [])
       defs
   in
   (functions_env, predicates_env, definitions) *)
