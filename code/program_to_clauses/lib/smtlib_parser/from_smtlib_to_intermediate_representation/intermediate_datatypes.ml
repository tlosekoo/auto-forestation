module Smtlib = Smtlib_utils.V_2_6
open Term
open Clause
module Map_string = Misc.My_map.Make (Misc.String)

type atomOrPattern =
  | HeadAtom of Extended_atom_patterns.t
  | HeadPattern of Pattern.t

type clause_in_construction = {
  guard : Conjunctive_clause_patterns.t;
  goal : atomOrPattern;
}

type predicate_or_function =
  | Pred of Typed_relation_symbol.t
  | Func of Typed_function.t

let pp_predicate_of_function (c : Format.formatter) (predicate_or_function : predicate_or_function)
    =
  match predicate_or_function with
  | Pred p -> Format.fprintf c "Predicate %a" Typed_relation_symbol.pp p
  | Func f -> Format.fprintf c "Predicate %a" Typed_function.pp f

(* Printers for debugging purpose *)

let pp_atom_or_pattern (c : Format.formatter) (atom_or_pattern : atomOrPattern) : unit =
  match atom_or_pattern with
  | HeadAtom extended_atom -> Format.fprintf c "%a" Extended_atom_patterns.pp extended_atom
  | HeadPattern p -> Format.fprintf c "%a" Term.Pattern.pp p

let pp_clause_in_construction (c : Format.formatter) (clause : clause_in_construction) : unit =
  Format.fprintf c "%a => %a" Conjunctive_clause_patterns.pp clause.guard pp_atom_or_pattern
    clause.goal
(*   *)
(*   *)
(*   *)
(*   *)
(* Some functions that are useful for nearly every following functions. *)

let create_clause_in_construction_from_precondition_and_atom
    (guard : Conjunctive_clause_patterns.t)
    (goal : Extended_atom_patterns.t) : clause_in_construction =
  {guard; goal = HeadAtom goal}

(* Given a list of pairs (preconditions_i, pattern_i), computes the pair ([preconditions_1, ..., preconditions_n], [pattern_1, ..., pattern_n]) *)
let extract_pattern_from_atomOrPattern (ap : atomOrPattern) : Pattern.t =
  match ap with
  | HeadPattern p -> p
  | HeadAtom _ -> assert false

let extract_extendedAtomPatterns_from_atomOrPattern (ap : atomOrPattern) : Extended_atom_patterns.t
    =
  match ap with
  | HeadPattern _ -> assert false
  | HeadAtom formula -> formula

let extract_atom_from_atomOrPattern (ap : atomOrPattern) : Atom_patterns.t =
  match extract_extendedAtomPatterns_from_atomOrPattern ap with
  | Extended_atom_patterns.True -> assert false
  | Extended_atom_patterns.False -> assert false
  | Extended_atom_patterns.Atom atom -> atom

(* Returns None if the clause becomes are trivial *)
let put_head_atom_in_positive_guards (fic : clause_in_construction) : Clause_patterns.t option =
  let goal = extract_extendedAtomPatterns_from_atomOrPattern fic.goal in
  match goal with
  | Extended_atom_patterns.True -> Some fic.guard
  | Extended_atom_patterns.False -> None
  | Atom goal_atom -> Some (Clause_patterns.add_positive goal_atom fic.guard)

let decompose_clause_in_construction_that_uses_patterns (fic : clause_in_construction) :
    Clause_patterns.t * Pattern.t =
  (fic.guard, extract_pattern_from_atomOrPattern fic.goal)

let decompose_clause_in_construction_that_uses_formula (fic : clause_in_construction) :
    Clause_patterns.t * Extended_atom_patterns.t =
  (fic.guard, extract_extendedAtomPatterns_from_atomOrPattern fic.goal)

let conjunction_guard_and_append_args
    (subterms_and_their_preconditions : (Clause_patterns.t * 'a) list) : Clause_patterns.t * 'a list
    =
  let formulas = List.map fst subterms_and_their_preconditions in
  let patterns = List.map snd subterms_and_their_preconditions in
  let disjunction = Conjunctive_clause_patterns.conjunction formulas in
  (disjunction, patterns)

let add_in_guard (new_guard : Clause_patterns.t) (fic : clause_in_construction) :
    clause_in_construction =
  let new_guard = Conjunctive_clause_patterns.conjunction [new_guard; fic.guard] in
  {guard = new_guard; goal = fic.goal}

(* Converts a conjunctive clause into its negation in disjunctive form *)
let negate_conjunctive_into_disjunctive = Clause_patterns.swap_positives_and_negatives

(* HERE FROM CONJUNCTIVE TO DISJUNCTIVE *)
let clause_in_construction_into_implicative_clause (fic : clause_in_construction) =
  Disjunctive_clause_patterns.add_positive_ExtendedAtomPatterns
    (extract_extendedAtomPatterns_from_atomOrPattern fic.goal)
    (negate_conjunctive_into_disjunctive fic.guard)
