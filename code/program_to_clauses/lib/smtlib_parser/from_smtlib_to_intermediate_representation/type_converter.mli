val is_ty_bool : Smtlib_utils.V_2_6.Ast.ty -> bool
val convert_smtlib_type_into_datatype : Smtlib_utils.V_2_6.Ast.ty -> Term.Datatype.t
