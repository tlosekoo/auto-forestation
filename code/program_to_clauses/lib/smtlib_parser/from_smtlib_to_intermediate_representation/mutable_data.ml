open Term
open Term.Aliases
open Intermediate_datatypes

type labeled_variable_type = Datatype.t Map_string.t
type labeled_function = Typed_function.t Map_string.t
type labeled_relation = Typed_relation_symbol.t Map_string.t

type parsed_signature = {
  labeled_constructors : labeled_function ref;
  labeled_functions : labeled_function ref;
  labeled_relations : labeled_relation ref;
}

let create () : parsed_signature =
  {
    labeled_constructors = ref Map_string.empty;
    labeled_functions = ref Map_string.empty;
    labeled_relations = ref Map_string.empty;
  }
(* labeled datatypes? *)

let current_signature = create ()

let reset () =
  current_signature.labeled_constructors := Map_string.empty;
  current_signature.labeled_functions := Map_string.empty;
  current_signature.labeled_relations := Map_string.empty;
  ()

let set_labeled_constructors (datatype_env : Datatype_environment.t) =
  let constructors = Datatype_environment.get_constructors datatype_env in
  let constructors_map =
    Set_typed_function.fold
      (fun rs -> Map_string.add (Typed_function.to_string rs) rs)
      constructors Map_string.empty
  in
  current_signature.labeled_constructors := constructors_map

let add_labeled_function (func : Typed_function.t) =
  let label = Typed_function.to_string func in
  current_signature.labeled_functions :=
    Map_string.add label func !(current_signature.labeled_functions)

let add_labeled_relation (relation : Typed_relation_symbol.t) =
  let label = Typed_relation_symbol.to_string relation in
  current_signature.labeled_relations :=
    Map_string.add label relation !(current_signature.labeled_relations)

let add_labeled_function_or_relation (func_or_pred : Intermediate_datatypes.predicate_or_function) =
  match func_or_pred with
  | Pred p -> add_labeled_relation p
  | Func f -> add_labeled_function f

(* May fail if label is undefined *)
let find_opt_labeled_relation (label : string) : Typed_relation_symbol.t option =
  Map_string.find_opt label !(current_signature.labeled_relations)

let find_opt_labeled_constructor (label : string) : Typed_function.t option =
  Map_string.find_opt label !(current_signature.labeled_constructors)

let find_opt_labeled_function (label : string) : Typed_function.t option =
  Map_string.find_opt label !(current_signature.labeled_functions)

let find_opt_labeled_function_or_constructor (label : string) : Typed_function.t option =
  match find_opt_labeled_constructor label with
  | None -> find_opt_labeled_function label
  | Some constructor -> Some constructor
