open Smtlib_utils.V_2_6
open Term

(* Indicates whether an Ast.ty type is bool *)
let is_ty_bool (t : Ast.ty) : bool =
  match t with
  | Ty_bool -> true
  | _ -> false

(* Given as Ast.ty, creates a simple datatype representing it *)
let convert_smtlib_type_into_datatype (smt_type : Ast.ty) : Datatype.t =
  match smt_type with
  (* We do not handle bool as input! *)
  | Ty_bool -> raise (Invalid_argument "Bool is not a function type!")
  | Ty_real -> raise (Invalid_argument "Real is not a function type!")
  | Ty_app (ty_var, ty_args) ->
      if ty_args = [] then
        Datatype.create ty_var
      else
        let error_message =
          Format.flush_str_formatter
            (Format.fprintf Format.str_formatter
               "Datatype is parametrized, which is not supported! ty_args:\n%a\n"
               (Printing.pp_list_and_brackets Ast.pp_ty)
               ty_args)
        in
        raise (Invalid_argument error_message)
  | Ty_arrow (_ty_list, _ty) -> raise (Invalid_argument "Cannot handle arrow types")
  | _ -> failwith "Did not anticipage this case"
