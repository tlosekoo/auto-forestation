open Term
open Clause

(*   *)
(*   *)
(*   *)
(*   *)
(*   *)
(* Some small helper functions *)

(* Given two patterns, creates the predicate that states that they are equal *)
let create_equality_predicate (t1 : Pattern.t) (t2 : Pattern.t) : Atom_patterns.t =
  let datatype_t1 = Pattern.get_type t1 in
  let datatype_t2 = Pattern.get_type t2 in
  let () =
    if not (Datatype.equal datatype_t1 datatype_t2) then
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter
             "Equality predicate type mismatch between %a and %a. They have type %a and %a.\n"
             Pattern.pp t1 Pattern.pp t2 Datatype.pp datatype_t1 Datatype.pp datatype_t2)
      in
      raise (Invalid_argument error_message)
  in

  let eq_predicate = Predicate_symbol_or_equality.create_equality_from_datatype datatype_t1 in
  let eq_atom = Atom_patterns.create eq_predicate [t1; t2] in
  eq_atom
