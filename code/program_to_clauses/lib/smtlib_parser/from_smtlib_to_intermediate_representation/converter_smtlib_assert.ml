open Intermediate_datatypes
open Smtlib_utils.V_2_6
open Clause.Aliases
open Mutable_data

let convert_unquantified_term_into_clause (var_env : labeled_variable_type) (t : Ast.term) :
    Set_clause_patterns.t =
  let fics_resulting_from_transformation =
    Converter_smtlib_term.convert_smtlib_unquantified_term var_env ~returns_boolean:true t
  in
  fics_resulting_from_transformation
  |> List.filter_map clause_in_construction_into_implicative_clause
  |> Set_clause_patterns.of_list

(* It is supposed here that the term is a first-order universally quantified term *)
let convert_assert_into_formula (a : Ast.term) : Set_clause_patterns.t =
  let var_env, unquantified_formula =
    match a with
    | Ast.Forall (vars, formula) ->
        let var_env = Parse_datatypes_declaration.convert_typed_vars_into_var_env vars in
        (var_env, formula)
    | formula -> (Map_string.empty, formula)
  in
  convert_unquantified_term_into_clause var_env unquantified_formula
