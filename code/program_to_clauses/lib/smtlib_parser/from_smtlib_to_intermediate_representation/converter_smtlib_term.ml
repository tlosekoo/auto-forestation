open Term
open Smtlib_utils.V_2_6
open Clause
open Intermediate_datatypes
open Mutable_data

(*   *)
(*   *)
(*   *)
(*   *)
(*   *)
(* Convertion of an Ast.term representing a **term** into a conjunctive list of chcs *)

(* Given a term that match with 'Const', creates the possible (preconditions, value)'. *)
let convert_smtlib_const (var_env : labeled_variable_type) (const_str : string) :
    clause_in_construction =
  let const_is_variable = Map_string.mem const_str var_env in
  if const_is_variable then
    let var_str = const_str in
    let datatype_of_variable = Map_string.find var_str var_env in
    let var = Typed_symbol.create (Symbol.create var_str) datatype_of_variable in
    let var_as_pattern = Pattern.from_var var in
    {guard = Conjunctive_clause_patterns.true_clause; goal = HeadPattern var_as_pattern}
  else
    let func_opt = Mutable_data.find_opt_labeled_function_or_constructor const_str in
    match func_opt with
    | None ->
        let error_message = "Unknown symbol found in Ast.Const. It matched with " ^ const_str in
        failwith error_message
    | Some func ->
        if Typed_function.get_arity func != 0 then
          let error_message =
            Format.flush_str_formatter
              (Format.fprintf Format.str_formatter
                 "Non-constant found in Ast.Const. It matched with %a"
                 (Typed_function.pp_parameterized ~complete:true)
                 func)
          in
          raise (Invalid_argument error_message)
        else
          let pattern_of_constructor = Pattern.create func [] in
          {
            guard = Conjunctive_clause_patterns.true_clause;
            goal = HeadPattern pattern_of_constructor;
          }

(* Given an smtlib term, creates every possible value of it (depending on the preconditions): (preconditions, pattern)
   There can be many because of the match construct *)
let rec convert_smtlib_unquantified_term
    (var_env : labeled_variable_type)
    ~(returns_boolean : bool)
    (t : Ast.term) : clause_in_construction list =
  match t with
  | Ast.False when returns_boolean ->
      let false_chc =
        {
          guard = Conjunctive_clause_patterns.true_clause;
          goal = HeadAtom Extended_atom_patterns.False;
        }
      in
      [false_chc]
  | Ast.True when returns_boolean ->
      let true_chc =
        {
          guard = Conjunctive_clause_patterns.true_clause;
          goal = HeadAtom Extended_atom_patterns.True;
        }
      in
      [true_chc]
  (* Special treatment of equality predicate *)
  | Ast.Eq (t1, t2) when returns_boolean -> convert_smtlib_equality_predicate var_env t1 t2
  (* A const seems to be either a variable or a function/constructor symbol *)
  | Ast.Const const -> [convert_smtlib_const var_env const]
  (* An app is a function symbol or a predicate symbol that is applied to some arguments. *)
  | Ast.App (f_as_str, args) -> convert_smtlib_application var_env ~returns_boolean f_as_str args
  (* A match a a matched term with every possible branch *)
  | Ast.Match (term, match_branch_list) ->
      convert_smtlib_match_branches var_env ~returns_boolean term match_branch_list
  | Ast.If (condition, then_branch, else_branch) ->
      convert_smtlib_ite var_env ~returns_boolean condition then_branch else_branch
  | Ast.Imply (body, head) -> convert_smtlib_imply var_env body head
  | Ast.And terms -> convert_smtlib_and var_env terms
  | Ast.Or terms -> convert_smtlib_or var_env terms
  (* | Ast.Not _term -> *)
  (* Any other construct is not handled *)
  | _ ->
      let converts_towards = if returns_boolean then "formula" else "pattern" in
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter
             "Cannot convert %a into a %s.
             It may use constructions not supported in this translation."
             Ast.pp_term t converts_towards)
      in
      raise (Invalid_argument error_message)

(* Given a atomic (Smtlib) predicate, creates the predicate corresponding to its application on its parameters **)
and convert_smtlib_application
    (var_env : labeled_variable_type)
    ~(returns_boolean : bool)
    (f_as_str : string)
    (args : Ast.term list) : clause_in_construction list =
  let fp_opt =
    if returns_boolean then
      Mutable_data.find_opt_labeled_relation f_as_str |> Option.map (fun p -> Pred p)
    else
      Mutable_data.find_opt_labeled_function_or_constructor f_as_str |> Option.map (fun f -> Func f)
  in

  let fp =
    match fp_opt with
    | None ->
        let error_message =
          Format.flush_str_formatter
            (Format.fprintf Format.str_formatter
               "Symbol %s cannot be used as a function/predicate because it is not defined" f_as_str)
        in
        raise (Invalid_argument error_message)
    | Some fp -> fp
  in

  let intermediate_formula_of_substerms =
    List.map (convert_smtlib_unquantified_term var_env ~returns_boolean:false) args
  in

  (* Cartesian product of possibilities *)
  (* Each element of this list is a list of arguments for 'f' (of the form) (preconditions, value) *)
  let every_possible_combination_of_values =
    Misc.List_op.cartesian_product_list intermediate_formula_of_substerms
  in

  (* Given one intermediate formula for each subterm, creates the application *)
  let reconstruct_solution_for_one_combination intermediate_formula_of_each_subterm =
    let guard, values_of_subterms =
      intermediate_formula_of_each_subterm
      |> List.map decompose_clause_in_construction_that_uses_patterns
      |> conjunction_guard_and_append_args
    in

    match fp with
    | Pred pred ->
        let f_app = Atom_patterns.create_from_predicate_symbol pred values_of_subterms in
        {guard; goal = HeadAtom (Atom f_app)}
    | Func func ->
        let head_pattern = Pattern.create func values_of_subterms in
        let guards_and_value_of_f_applied_to_subterms =
          {guard = Conjunctive_clause_patterns.true_clause; goal = HeadPattern head_pattern}
        in
        add_in_guard guard guards_and_value_of_f_applied_to_subterms
  in

  let possible_results =
    List.map reconstruct_solution_for_one_combination every_possible_combination_of_values
  in

  possible_results

(* Given an Smtlib branch, creates the list of possible (precondition, pattern) representing its value *)
and convert_smtlib_match_branches
    (var_env : labeled_variable_type)
    ~(returns_boolean : bool)
    (matched_term : Ast.term)
    (branches : Ast.match_branch list) : clause_in_construction list =
  (* For now, we can only pattern-match on algebraic datatypes *)
  let fics_representing_matched_pattern =
    convert_smtlib_unquantified_term var_env matched_term ~returns_boolean:false
  in

  let handle_one_branch_with_one_possible_input_pattern fic_one_pattern match_branch =
    let guard, pattern_input =
      decompose_clause_in_construction_that_uses_patterns fic_one_pattern
    in
    let handlings_for_this_branch =
      handle_one_branch_of_match var_env ~returns_boolean pattern_input match_branch
    in
    let possibilities_for_this_branch = List.map (add_in_guard guard) handlings_for_this_branch in
    possibilities_for_this_branch
  in

  let handle_all_branches_for_one_input input_preconditions_and_value =
    List.flatten
      (List.map
         (handle_one_branch_with_one_possible_input_pattern input_preconditions_and_value)
         branches)
  in

  let handling_of_all_branches_for_all_inputs =
    List.flatten (List.map handle_all_branches_for_one_input fics_representing_matched_pattern)
  in

  handling_of_all_branches_for_all_inputs

(* Given one branch of a branch term, creates every possible chc=(precondition, term) *)
and handle_one_branch_of_match
    (var_env : labeled_variable_type)
    ~(returns_boolean : bool)
    (pattern_on_which_the_match_is_done : Pattern.t)
    (branch : Ast.match_branch) : clause_in_construction list =
  match branch with
  | Ast.Match_default t -> convert_smtlib_unquantified_term var_env ~returns_boolean t
  | Ast.Match_case (constructor_str, variables_str, t) ->
      let constructor_opt = Mutable_data.find_opt_labeled_constructor constructor_str in
      let constructor =
        match constructor_opt with
        | Some constructor -> constructor
        | None ->
            let error_message =
              "Symbol " ^ constructor_str ^ " cannot be used as matching constructor"
            in
            raise (Invalid_argument error_message)
      in
      if
        Pattern.is_variable pattern_on_which_the_match_is_done
        || constructor = Pattern.get_root_symbol pattern_on_which_the_match_is_done
      then
        let datatypes_input_constructor = Typed_function.get_input_types constructor in
        let variables_as_symbols = List.map Symbol.create variables_str in
        let variables_input_constructor =
          List.map2 Typed_symbol.create variables_as_symbols datatypes_input_constructor
        in
        let vars_input_constructor_as_patterns =
          List.map Pattern.from_var variables_input_constructor
        in
        let constructor_applied_to_variables =
          Pattern.create constructor vars_input_constructor_as_patterns
        in
        let matching_precondition =
          Equality_atom_creator.create_equality_predicate constructor_applied_to_variables
            pattern_on_which_the_match_is_done
        in
        let var_name_to_var_datatype =
          List.map
            (fun var -> (Typed_symbol.to_string var, Typed_symbol.get_type var))
            variables_input_constructor
        in
        (* assert variables are disjoint from variables of var_env *)
        let enriched_var_env =
          Map_string.union_if_compatible Datatype.equal
            (Map_string.of_list var_name_to_var_datatype)
            var_env
        in
        let inside_term_evaluated =
          convert_smtlib_unquantified_term enriched_var_env ~returns_boolean t
        in

        (* conjunctive clause *)
        let clause_for_the_guard =
          Clause_patterns.from_negative_and_positive_formulas ~negative:[]
            ~positive:[matching_precondition]
        in

        let clauses = List.map (add_in_guard clause_for_the_guard) inside_term_evaluated in

        (* let () =
          Format.fprintf Format.std_formatter "\n\nIn the branch for pattern %a.\n" Pattern.pp
            pattern_on_which_the_match_is_done
        in
        let () =
          Format.fprintf Format.std_formatter "\n\nThe atom for the precondition is %a.\n"
            Atom_patterns.pp matching_precondition
        in
        let () =
          Format.fprintf Format.std_formatter "\n\nThe interior has been translated as\n%a\n"
            (Printing.pp_list_and_brackets_sep "\n" Intermediate_datatypes.pp_clause_in_construction)
            inside_term_evaluated
        in
        let () =
          Format.fprintf Format.std_formatter "\n\nThe resulting clauses are\n%a\n"
            (Printing.pp_list_and_brackets_sep "\n" Intermediate_datatypes.pp_clause_in_construction)
            clauses
        in *)

        clauses
      else (* in the case where the constructor does not match *)
        []

(* Given two Ast.terms, creates the predicate stating that they are equal (and its preconditions) *)
and convert_smtlib_equality_predicate
    (var_env : labeled_variable_type)
    (t1 : Ast.term)
    (t2 : Ast.term) : clause_in_construction list =
  let possibilites_of_preconditions_and_pattern_for_t1 =
    convert_smtlib_unquantified_term var_env ~returns_boolean:false t1
  in
  let possibilites_of_preconditions_and_pattern_for_t2 =
    convert_smtlib_unquantified_term ~returns_boolean:false var_env t2
  in
  let possible_combination =
    Misc.List_op.cartesian_product possibilites_of_preconditions_and_pattern_for_t1
      possibilites_of_preconditions_and_pattern_for_t2
  in

  let handle_one_possibility (fic_t1, fic_t2) =
    let guard_t1, val_of_t1 = decompose_clause_in_construction_that_uses_patterns fic_t1 in
    let guard_t2, val_of_t2 = decompose_clause_in_construction_that_uses_patterns fic_t2 in
    let guard = Conjunctive_clause_patterns.conjunction [guard_t1; guard_t2] in
    let eq_atom = Equality_atom_creator.create_equality_predicate val_of_t1 val_of_t2 in
    {guard; goal = HeadAtom (Atom eq_atom)}
  in

  List.map handle_one_possibility possible_combination

and convert_smtlib_ite
    (var_env : labeled_variable_type)
    ~(returns_boolean : bool)
    (condition : Ast.term)
    (then_branch : Ast.term)
    (else_branch : Ast.term) : clause_in_construction list =
  let chcs_condition = convert_smtlib_unquantified_term ~returns_boolean:true var_env condition in
  match chcs_condition with
  | [] -> raise (Failure "No condition??")
  | _ :: _ :: _ ->
      raise (Failure "Why do you need more that one branch in a condition? ite on match?")
  | [context_and_condition] -> (
      let condition = extract_extendedAtomPatterns_from_atomOrPattern context_and_condition.goal in

      let value_of_then_branch =
        convert_smtlib_unquantified_term ~returns_boolean var_env then_branch
      in
      let value_of_else_branch =
        convert_smtlib_unquantified_term ~returns_boolean var_env else_branch
      in

      (* Simplify guards here. A bit of duplicated code, but here we are. *)
      match condition with
      | Extended_atom_patterns.True ->
          let guard_then = context_and_condition.guard in
          let then_branches = List.map (add_in_guard guard_then) value_of_then_branch in
          then_branches
      | Extended_atom_patterns.False ->
          let guard_else = context_and_condition.guard in
          let else_branches = List.map (add_in_guard guard_else) value_of_else_branch in
          else_branches
      | Extended_atom_patterns.Atom condition ->
          let guard_then = Clause_patterns.add_positive condition context_and_condition.guard in
          let guard_else = Clause_patterns.add_negative condition context_and_condition.guard in

          let then_branches = List.map (add_in_guard guard_then) value_of_then_branch in
          let else_branches = List.map (add_in_guard guard_else) value_of_else_branch in

          let all_branches = List.append then_branches else_branches in

          all_branches)

and convert_smtlib_imply
    (var_env : labeled_variable_type)
    (body_term : Ast.term)
    (head_term : Ast.term) : clause_in_construction list =
  let body_formulas = convert_smtlib_unquantified_term var_env ~returns_boolean:true body_term in
  let head_formulas = convert_smtlib_unquantified_term var_env ~returns_boolean:true head_term in
  let body_possible_preconditions =
    List.filter_map put_head_atom_in_positive_guards body_formulas
  in
  let body_combinations = Clause_patterns.cartesian_product_formula body_possible_preconditions in
  let body_head_combinations = Misc.List_op.cartesian_product body_combinations head_formulas in
  let bodies, heads = List.split body_head_combinations in
  List.map2 add_in_guard bodies heads

and convert_smtlib_and (var_env : labeled_variable_type) (terms : Ast.term list) :
    clause_in_construction list =
  let each_term_as_list_of_possible_fic =
    List.map (convert_smtlib_unquantified_term var_env ~returns_boolean:true) terms
  in
  (* let () =
    Format.fprintf Format.std_formatter "\n\nEach formula in construction looks like\n%a\n"
      (Printing.pp_list_sep "\n\n"
         (Printing.pp_list_and_brackets_sep "\n" Intermediate_datatypes.pp_clause_in_construction))
      each_term_as_list_of_possible_fic
  in *)

  (* CNF into DNF *)
  let every_combination = Misc.List_op.cartesian_product_list each_term_as_list_of_possible_fic in
  (* let () =
    Format.fprintf Format.std_formatter "\n\nTheir combination looks like\n%a\n"
      (Printing.pp_list_sep "\n\n"
         (Printing.pp_list_and_brackets_sep "\n" Intermediate_datatypes.pp_clause_in_construction))
      every_combination
  in *)
  let dnf_form_of_combinations = List.map convert_smtlib_and_of_one_world every_combination in
  let result = List.flatten dnf_form_of_combinations in
  (* let () =
    Format.fprintf Format.std_formatter "\n\nOnce conerted, they are:\n%a\n"
      (Printing.pp_list_and_brackets_sep "\n\n" Intermediate_datatypes.pp_clause_in_construction)
      result
  in *)
  result

and convert_smtlib_and_of_one_world (and_of_fics : clause_in_construction list) :
    clause_in_construction list =
  let all_decomposed_formulas =
    List.map Intermediate_datatypes.decompose_clause_in_construction_that_uses_formula and_of_fics
  in
  let all_preconditions, all_results =
    Intermediate_datatypes.conjunction_guard_and_append_args all_decomposed_formulas
  in
  let all_ands =
    List.map
      (Intermediate_datatypes.create_clause_in_construction_from_precondition_and_atom
         all_preconditions)
      all_results
  in
  all_ands

and convert_smtlib_or (var_env : labeled_variable_type) (terms : Ast.term list) :
    clause_in_construction list =
  let each_term_as_list_of_possible_fic =
    List.map (convert_smtlib_unquantified_term var_env ~returns_boolean:true) terms
  in
  (* CNF into DNF *)
  let every_combination = Misc.List_op.cartesian_product_list each_term_as_list_of_possible_fic in
  let dnf_form_of_combinations = List.map convert_smtlib_or_of_one_world every_combination in
  List.flatten dnf_form_of_combinations

and convert_smtlib_or_of_one_world (and_of_fics : clause_in_construction list) :
    clause_in_construction list =
  let all_decomposed_formulas =
    List.map Intermediate_datatypes.decompose_clause_in_construction_that_uses_formula and_of_fics
  in
  let all_preconditions, all_results =
    Intermediate_datatypes.conjunction_guard_and_append_args all_decomposed_formulas
  in
  let all_ors =
    List.filter_map
      (fun element_of_or ->
        let other_goals =
          List.filter (Stdlib.Fun.negate (Extended_atom_patterns.equal element_of_or)) all_results
        in
        let negative_preconditions_opt =
          Clause.Conjunctive_clause_patterns.from_negative_extendedAtomPatterns other_goals
        in
        match negative_preconditions_opt with
        | Some negative_preconditions ->
            let all_preconditions_and_negatives_ones =
              Conjunctive_clause_patterns.conjunction [all_preconditions; negative_preconditions]
            in
            let formula =
              Intermediate_datatypes.create_clause_in_construction_from_precondition_and_atom
                all_preconditions_and_negatives_ones element_of_or
            in
            Some formula
        | None -> None)
      all_results
  in
  all_ors
