include Type_converter
open Term
open Smtlib_utils.V_2_6
open Clause.Aliases

(* exception FailedParsing of string *)

type function_and_predicates_definition =
  (Intermediate_datatypes.predicate_or_function * Set_clause_patterns.t) list

let convert_smtlib_file_into_datatypeEnv_declarations_definitions_asserts (input_file : string) :
    Datatype_environment.t
    * Intermediate_datatypes.predicate_or_function list
    * function_and_predicates_definition
    * Set_clause_patterns.t =
  let parsed_data = Smtlib_utils.V_2_6.parse_file input_file in
  let statements =
    try Result.get_ok parsed_data with
    | _ -> raise (Invalid_argument (Result.get_error parsed_data))
  in

  let stmts = List.map (fun (s : Ast.statement) -> s.stmt) statements in

  let asserts =
    List.filter_map
      (fun x ->
        match x with
        | Ast.Stmt_assert t -> Some t
        | _ -> None)
      stmts
  in

  let datatypes_declaration =
    List.flatten
      (List.filter_map
         (fun x ->
           match x with
           | Ast.Stmt_data decl -> Some decl
           | _ -> None)
         stmts)
  in

  let functions_and_predicates_declaration_smtlib =
    List.filter_map
      (fun x ->
        match x with
        | Ast.Stmt_decl decl -> Some decl
        | _ -> None)
      stmts
  in

  let functions_and_predicates_definition_smtlib =
    List.filter_map
      (fun x ->
        match x with
        | Ast.Stmt_fun_def _decl -> Some x
        | Ast.Stmt_fun_rec _decl -> Some x
        | _ -> None)
      stmts
  in

  let datatype_env =
    Parse_datatypes_declaration.parse_datatypes_declaration datatypes_declaration
  in

  Mutable_data.set_labeled_constructors datatype_env;

  let functions_and_predicates_declaration =
    Converter_function_declarations.parse_all_declaration
      functions_and_predicates_declaration_smtlib
  in

  (* So that functions that are declared can be used in definitions/asserts *)
  List.iter Mutable_data.add_labeled_function_or_relation functions_and_predicates_declaration;

  let reversed_functions_and_predicates_definitions =
    Converter_function_definitions.convert_smtlib_definitions_into_corresponding_definitions
      functions_and_predicates_definition_smtlib
  in

  (* Useful because functions are ordered in dependency order *)
  let functions_and_predicates_definitions =
    List.rev reversed_functions_and_predicates_definitions
  in

  let clauses_of_asserts =
    Set_clause_patterns.list_union
      (List.map Converter_smtlib_assert.convert_assert_into_formula asserts)
  in

  (* let () =
       Format.fprintf Format.std_formatter
         "Intermediate parsing of SMTLIB gives environment:\n%a\n\nDifinitions:\n%a\n\nAsserts:\n%a\n"
         Datatype_environment.pp datatype_env
         (Printing.pp_assoclist_long Intermediate_datatypes.pp_predicate_of_function
            Set_clause_patterns.pp)
         functions_and_predicates_definitions Set_clause_patterns.pp clauses_of_asserts
     in *)
  ( datatype_env,
    functions_and_predicates_declaration,
    functions_and_predicates_definitions,
    clauses_of_asserts )
