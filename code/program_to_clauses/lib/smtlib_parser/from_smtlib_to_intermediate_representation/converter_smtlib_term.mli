val convert_smtlib_unquantified_term :
  Mutable_data.labeled_variable_type ->
  returns_boolean:bool ->
  Smtlib_utils.V_2_6.Ast.term ->
  Intermediate_datatypes.clause_in_construction list
