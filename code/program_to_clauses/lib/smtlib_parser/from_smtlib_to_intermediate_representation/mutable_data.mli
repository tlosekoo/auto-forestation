type labeled_variable_type = Term.Datatype.t Intermediate_datatypes.Map_string.t
type labeled_function = Term.Typed_function.t Intermediate_datatypes.Map_string.t
type labeled_relation = Term.Typed_relation_symbol.t Intermediate_datatypes.Map_string.t

type parsed_signature = {
  labeled_constructors : labeled_function ref;
  labeled_functions : labeled_function ref;
  labeled_relations : labeled_relation ref;
}

val create : unit -> parsed_signature
val current_signature : parsed_signature
val reset : unit -> unit
val set_labeled_constructors : Term.Datatype_environment.t -> unit
val add_labeled_function : Term.Typed_function.t -> unit
val add_labeled_relation : Term.Typed_relation_symbol.t -> unit
val add_labeled_function_or_relation : Intermediate_datatypes.predicate_or_function -> unit
val find_opt_labeled_relation : string -> Term.Typed_relation_symbol.t option
val find_opt_labeled_constructor : string -> Term.Typed_function.t option
val find_opt_labeled_function : string -> Term.Typed_function.t option
val find_opt_labeled_function_or_constructor : string -> Term.Typed_function.t option
