val convert_smtlib_definitions_into_corresponding_definitions :
  Smtlib_utils.V_2_6.Ast.stmt list ->
  (Intermediate_datatypes.predicate_or_function * Clause.Aliases.Set_clause_patterns.t) list
