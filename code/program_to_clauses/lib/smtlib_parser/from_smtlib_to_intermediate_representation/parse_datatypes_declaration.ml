include Intermediate_datatypes
include Mutable_data
include Type_converter
open Term
open Smtlib_utils.V_2_6
open Clause.Aliases

(* Converts a list of typed vars into a variable environment *)
let convert_typed_vars_into_var_env (vars : Ast.typed_var list) : labeled_variable_type =
  let vars_with_datatype =
    List.map (fun (s, smt_type) -> (s, convert_smtlib_type_into_datatype smt_type)) vars
  in
  let var_env = Map_string.of_list vars_with_datatype in
  var_env

let convert_smtlib_constructor_decl_into_constructor (d : Datatype.t) (c : Ast.cstor) :
    Typed_function.t =
  let name = Symbol.create c.cstor_name in
  let parameters_smtlib_type = List.map snd c.cstor_args in
  let parameters_type = List.map convert_smtlib_type_into_datatype parameters_smtlib_type in
  let constructor = Typed_function.create name d parameters_type (* ~is_constructor:true *) in
  constructor

(* Returns every constructor of this datatype *)
let convert_smtlib_datatype_definition
    (((name, arity), constructor_list) : (string * int) * Ast.cstor list) : Set_typed_function.t =
  let datatype = Datatype.create name in
  if arity > 0 then
    raise (Invalid_argument "Cannot handle parametric datatypes")
  else
    let constructors =
      List.map (convert_smtlib_constructor_decl_into_constructor datatype) constructor_list
    in

    Set_typed_function.of_list constructors

let parse_datatypes_declaration (datatype_declaration_list : ((string * int) * Ast.cstor list) list)
    : Datatype_environment.t =
  (* let () =
       Format.fprintf Format.std_formatter
         "------------------------create_type_environment - Begin\n"
     in
     let () =
       Format.fprintf Format.std_formatter "declarations:\n%a\n"
         (Printing.pp_list_and_brackets
            (Printing.pp_binding
               (Printing.pp_pair Printing.pp_string Printing.pp_int)
               (Printing.pp_ignore_arg_and_print "?")))
         datatype_declaration_list
     in *)
  let datatypes_constructors =
    List.map convert_smtlib_datatype_definition datatype_declaration_list
    |> Set_typed_function.list_union
  in
  let env = Datatype_environment.build datatypes_constructors in
  (* let () =
       Format.fprintf Format.std_formatter
         "------------------------create_type_environment - End\n"
     in *)
  env
