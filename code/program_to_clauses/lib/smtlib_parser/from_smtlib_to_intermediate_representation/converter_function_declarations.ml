open Term
open Smtlib_utils.V_2_6
open Type_converter

let parse_one_declaration (declaration : Ast.ty Ast.fun_decl) :
    Intermediate_datatypes.predicate_or_function =
  let name = Symbol.create declaration.fun_name in
  let parameters_type = List.map convert_smtlib_type_into_datatype declaration.fun_args in
  let predicate_or_function =
    if is_ty_bool declaration.fun_ret then
      let relation = Typed_relation_symbol.create name parameters_type in
      Intermediate_datatypes.Pred relation
    else
      let function_return_type =
        Type_converter.convert_smtlib_type_into_datatype declaration.fun_ret
      in
      let func = Typed_function.create name function_return_type parameters_type in
      Intermediate_datatypes.Func func
  in
  let () =
    Format.fprintf Format.std_formatter "Function:\n%a\n\n"
      Intermediate_datatypes.pp_predicate_of_function predicate_or_function
  in
  predicate_or_function


let parse_all_declaration = List.map parse_one_declaration
