open Smtlib_utils.V_2_6

val convert_typed_vars_into_var_env : Ast.typed_var list -> Mutable_data.labeled_variable_type

val parse_datatypes_declaration :
  ((string * int) * Intermediate_datatypes.Smtlib.Ast.cstor list) list ->
  Term.Datatype_environment.t
