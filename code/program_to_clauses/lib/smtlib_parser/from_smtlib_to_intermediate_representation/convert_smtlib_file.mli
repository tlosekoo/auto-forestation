type function_and_predicates_definition =
  (Intermediate_datatypes.predicate_or_function * Clause.Aliases.Set_clause_patterns.t) list

val convert_smtlib_file_into_datatypeEnv_declarations_definitions_asserts :
  string ->
  Term.Datatype_environment.t
  * Intermediate_datatypes.predicate_or_function list
  * function_and_predicates_definition
  * Clause.Aliases.Set_clause_patterns.t
