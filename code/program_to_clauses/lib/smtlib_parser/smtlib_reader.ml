open Term
open Clause
open Clause.Aliases

let create_determinism_constraint_for_function (f : Typed_function.t) : Clause_patterns.t =
  let output_datatype = Typed_function.get_output_type f in
  let input_datatype = Typed_function.get_input_types f in
  let vars_of_function_input =
    List.map Symbol_generator.generate_fresh_typed_symbol input_datatype
  in
  let x = Symbol_generator.generate_fresh_typed_symbol output_datatype in
  let y = Symbol_generator.generate_fresh_typed_symbol output_datatype in

  let function_input = List.map Pattern.from_var vars_of_function_input in
  let px = Pattern.from_var x in
  let py = Pattern.from_var y in

  let pf = Typed_relation_symbol.from_function f in

  let atom_f_x = Atom_patterns.create_from_predicate_symbol pf (function_input @ [px]) in

  let atom_f_y = Atom_patterns.create_from_predicate_symbol pf (function_input @ [py]) in
  let eq_x_y =
    Atom_patterns.create
      (Predicate_symbol_or_equality.create_equality_from_datatype output_datatype)
      [px; py]
  in

  let determinism_chc =
    Disjunctive_clause_patterns.from_body_and_head ~body:[atom_f_x; atom_f_y] ~head:[eq_x_y]
  in
  determinism_chc

let rec pattern_into_its_relational_form_and_preconditions (pattern : Pattern.t) :
    Pattern.t * Set_atom_patterns.t =
  match Pattern.get_root_symbol_opt pattern with
  | None -> (pattern, Set_atom_patterns.empty)
  | Some root_function -> (
      match Mutable_data.find_opt_labeled_function (Typed_function.to_string root_function) with
      | None ->
          (* root_function is a constructor *)
          let children_pattern = Pattern.get_children pattern in
          let relational_children, list_preconditions =
            List.split
              (List.map pattern_into_its_relational_form_and_preconditions children_pattern)
          in
          let preconditions = Set_atom_patterns.list_union list_preconditions in
          let relational_pattern = Pattern.create root_function relational_children in
          (relational_pattern, preconditions)
      | Some func ->
          (* root function is not a constructor *)
          let associated_predicate = Typed_relation_symbol.from_function func in
          let type_of_f = Typed_function.get_output_type func in
          let variable_representing_the_value_of_f =
            Symbol_generator.generate_fresh_typed_symbol type_of_f
          in
          let var_as_pattern = Pattern.from_var variable_representing_the_value_of_f in

          let children_patterns = Pattern.get_children pattern in
          let relational_children_patterns, list_preconditions =
            List.split
              (List.map pattern_into_its_relational_form_and_preconditions children_patterns)
          in
          let parameters = relational_children_patterns @ [var_as_pattern] in

          let precondition_relation =
            Atom_patterns.create_from_predicate_symbol associated_predicate parameters
          in
          let all_preconditions =
            Set_atom_patterns.add precondition_relation
              (Set_atom_patterns.list_union list_preconditions)
          in
          (var_as_pattern, all_preconditions))

let atom_into_its_relational_form_and_preconditions (atom : Atom_patterns.t) :
    Atom_patterns.t * Set_atom_patterns.t =
  let argument_patterns = Atom_patterns.get_argument atom in
  let argument_relational_patterns, list_preconditions =
    List.split (List.map pattern_into_its_relational_form_and_preconditions argument_patterns)
  in
  let relational_atom =
    Atom_patterns.create (Atom_patterns.get_predicate atom) argument_relational_patterns
  in
  let preconditions = Set_atom_patterns.list_union list_preconditions in
  (relational_atom, preconditions)

let clause_into_its_relational_form (clause : Clause_patterns.t) : Clause_patterns.t =
  let list_head_atoms = Disjunctive_clause_patterns.get_head clause |> Set_atom_patterns.to_list in
  let list_body_atoms = Disjunctive_clause_patterns.get_body clause |> Set_atom_patterns.to_list in
  let list_relational_head_atoms, list_head_preconditions =
    List.split (List.map atom_into_its_relational_form_and_preconditions list_head_atoms)
  in
  let list_relational_body_atoms, list_body_preconditions =
    List.split (List.map atom_into_its_relational_form_and_preconditions list_body_atoms)
  in
  let head_preconditions = Set_atom_patterns.list_union list_head_preconditions in
  let body_preconditions = Set_atom_patterns.list_union list_body_preconditions in
  let relational_body_atoms = Set_atom_patterns.of_list list_relational_body_atoms in
  let preconditions =
    Set_atom_patterns.list_union [relational_body_atoms; body_preconditions; head_preconditions]
  in
  let list_preconditions = Set_atom_patterns.to_list preconditions in
  let relational_clause =
    Clause_patterns.from_negative_and_positive_formulas ~negative:list_preconditions
      ~positive:list_relational_head_atoms
  in
  relational_clause

let convert_definition_to_relational_one
    (predicate_or_function : Intermediate_datatypes.predicate_or_function)
    (clauses : Set_clause_patterns.t) : Learning_problem.LabeledDef.t =
  let relational_clauses = Set_clause_patterns.map clause_into_its_relational_form clauses in
  match predicate_or_function with
  | Pred pred ->
      let predicate_definition = Predicate_definition.PredicateDefinition relational_clauses in
      (pred, predicate_definition)
  | Func func ->
      let functionality_clause = create_determinism_constraint_for_function func in
      let function_definition =
        Predicate_definition.FunctionDefinition
          {core = relational_clauses; functionality = functionality_clause}
      in
      let associated_predicate = Typed_relation_symbol.from_function func in
      (associated_predicate, function_definition)

let make_definition_relational
    (datatype_env : Datatype_environment.t)
    (declarations : Intermediate_datatypes.predicate_or_function list)
    (definitions : Convert_smtlib_file.function_and_predicates_definition)
    (asserts : Set_clause_patterns.t) : Learning_problem.t =
  let declarations_as_definitions =
    List.map (Misc.Pair.create_from_snd_then_fst Set_clause_patterns.empty) declarations
  in
  let definitions_and_declarations = List.append definitions declarations_as_definitions in
  let list_function_or_relation, list_definition = List.split definitions_and_declarations in
  let relational_definitions =
    List.map2 convert_definition_to_relational_one list_function_or_relation list_definition
  in
  let relational_asserts = Set_clause_patterns.map clause_into_its_relational_form asserts in
  Learning_problem.create datatype_env relational_definitions relational_asserts

let smtlib_to_clauses (smtlib_file : string) : Learning_problem.t =
  Mutable_data.reset ();
  let datatype_env, declarations, definitions, asserts =
    Convert_smtlib_file.convert_smtlib_file_into_datatypeEnv_declarations_definitions_asserts
      smtlib_file
  in
  let learning_problem = make_definition_relational datatype_env declarations definitions asserts in
  let simplified_learning_problem = Learning_problem.simplify learning_problem in
  simplified_learning_problem
