open Term
open Clause.Aliases
open Clause
module Map_predicate_symbol_string = Misc.My_map.Make2 (Typed_relation_symbol) (Misc.String)
module Map_predicate_symbol = Misc.My_map.Make (Typed_relation_symbol)
module LabeledDef = Misc.Pair.Make (Typed_relation_symbol) (Predicate_definition)

module LabeledDefs =
  Misc.List_maker.Make (Misc.Pair.Make (Typed_relation_symbol) (Predicate_definition))

type t = {
  env : Datatype_environment.t;
  definitions : LabeledDefs.t;
  properties : Set_clause_patterns.t;
}
[@@deriving compare, equal]

let create
    (env : Datatype_environment.t)
    (definitions : LabeledDefs.t)
    (properties : Set_clause_patterns.t) : t =
  {env; definitions; properties}

let pp (c : Format.formatter) (data : t) : unit =
  Format.fprintf c "env: %a\ndefinition:\n%a\nproperties:\n%a\n" Term.Datatype_environment.pp
    data.env
    (LabeledDefs.pp_param ~opening:"{\n" ~closing:"\n}\n" ~sep:"\n")
    data.definitions Set_clause_patterns.pp data.properties

let get_properties (lp : t) : Set_clause_patterns.t = lp.properties
let get_definitions (lp : t) : LabeledDefs.t = lp.definitions
let get_environement (lp : t) : Term.Datatype_environment.t = lp.env

let get_predicates_symbols (lp : t) : Set_typed_relation_symbol.t =
  Set_typed_relation_symbol.of_list (List.map fst lp.definitions)

let simplify (lp : t) : t =
  let new_definitions = List.map (Misc.Pair.map_snd Predicate_definition.simplify) lp.definitions in
  let new_properties =
    Set_clause_patterns.filter_map Disjunctive_clause_patterns.simplify_clause_by_unification
      lp.properties
  in
  {env = lp.env; definitions = new_definitions; properties = new_properties}
