open Clause
open Clause.Aliases

type t =
  | PredicateDefinition of Set_clause_patterns.t
  | FunctionDefinition of {
      core : Set_clause_patterns.t;
      functionality : Clause_patterns.t;
    }
[@@deriving compare, equal]

let pp (c : Format.formatter) (def : t) : unit =
  match def with
  | PredicateDefinition clauses -> Format.fprintf c "P:\n%a\n" Set_clause_patterns.pp clauses
  | FunctionDefinition both_systems ->
      Format.fprintf c "F:\n%a\n%a\n" Set_clause_patterns.pp both_systems.core
        Disjunctive_clause_patterns.pp both_systems.functionality

let extract_clauses_without_injectivity (ad : t) : Set_clause_patterns.t =
  match ad with
  | PredicateDefinition clauses -> clauses
  | FunctionDefinition {core; functionality = _functionality} -> core

let extract_clauses_with_injectivity (ad : t) : Set_clause_patterns.t =
  match ad with
  | PredicateDefinition clauses -> clauses
  | FunctionDefinition {core; functionality} -> Set_clause_patterns.add functionality core

let simplify (clauses : t) : t =
  match clauses with
  | PredicateDefinition clauses ->
      let simplified_clauses =
        Set_clause_patterns.filter_map
          Clause.Disjunctive_clause_patterns.simplify_clause_by_unification clauses
      in
      PredicateDefinition simplified_clauses
  | FunctionDefinition {core; functionality} ->
      let core =
        Set_clause_patterns.filter_map
          Clause.Disjunctive_clause_patterns.simplify_clause_by_unification core
      in
      FunctionDefinition {core; functionality}
