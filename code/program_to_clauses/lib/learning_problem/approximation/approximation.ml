open Term
open Clause
open Clause.Aliases
open Dependencies_computation

type approximation_method =
  (* | Remove_every_clause_that_can_be_safely_removed *)
  | Remove_functionnality_constraint_where_possible
  | No_approximation
[@@deriving compare, equal]

let pp_approximation_method (c : Format.formatter) (approximation_method : approximation_method) =
  let str_description =
    match approximation_method with
    (* | Remove_every_clause_that_can_be_safely_removed ->
        "remove every clause that can be safely removed" *)
    | Remove_functionnality_constraint_where_possible ->
        "remove functionnality constraint where possible"
    | No_approximation -> "No approximation"
  in
  Format.fprintf c "%s" str_description

let clause_can_be_removed_in_definition
    (can_overapproximate : bool)
    (can_underapproximate : bool)
    (relation : Typed_relation_symbol.t)
    (clause : Clause_patterns.t) : bool =
  let relation_has_positive_occurence = has_positive_occurence relation clause in
  let relation_has_negative_occurence = has_negative_occurence relation clause in
  let clause_can_dissapear =
    (can_underapproximate && not relation_has_negative_occurence)
    || (can_overapproximate && not relation_has_positive_occurence)
  in
  (* let () =
       Format.fprintf Format.std_formatter
         "Clause\n%a\ncan dissapear: %b\nwhen (over/under)approximating (%b, %b) relation %a\n\n"
         Clause_patterns.pp clause clause_can_dissapear can_overapproximate can_underapproximate
         Typed_relation_symbol.pp relation
     in *)
  clause_can_dissapear

let extract_clauses_from_approximation_sets_by_removing_functionality_constraint_if_possible
    (lp : Learning_problem.t)
    (_allowed_under_approximation : Set_typed_relation_symbol.t)
    (allowed_over_approximation : Set_typed_relation_symbol.t) : Set_clause_patterns.t =
  let properties = Learning_problem.get_properties lp in
  let clauses_definitions =
    lp |> Learning_problem.get_definitions
    |> List.map (fun (ps, def) ->
           match def with
           | Predicate_definition.PredicateDefinition clauses -> clauses
           | Predicate_definition.FunctionDefinition {core; functionality} ->
               let can_overapproximate =
                 Set_typed_relation_symbol.mem ps allowed_over_approximation
               in
               if can_overapproximate then
                 core
               else
                 Set_clause_patterns.add functionality core)
  in
  let all_clauses = List.fold_left Set_clause_patterns.union properties clauses_definitions in
  all_clauses

let extract_clauses_from_approximation_sets_by_removing_nothing
    (lp : Learning_problem.t)
    (_allowed_under_approximation : Set_typed_relation_symbol.t)
    (_allowed_over_approximation : Set_typed_relation_symbol.t) : Set_clause_patterns.t =
  let properties = Learning_problem.get_properties lp in
  let clauses_definitions =
    lp |> Learning_problem.get_definitions
    |> List.map (fun (_, def) ->
           match def with
           | Predicate_definition.PredicateDefinition clauses -> clauses
           | Predicate_definition.FunctionDefinition {core; functionality} ->
               Set_clause_patterns.add functionality core)
  in
  let all_clauses = List.fold_left Set_clause_patterns.union properties clauses_definitions in
  all_clauses

let extract_clauses_based_on_approximation_sets_and_method
    (approximation_type : approximation_method) =
  match approximation_type with
  | Remove_functionnality_constraint_where_possible ->
      extract_clauses_from_approximation_sets_by_removing_functionality_constraint_if_possible
  | No_approximation -> extract_clauses_from_approximation_sets_by_removing_nothing
