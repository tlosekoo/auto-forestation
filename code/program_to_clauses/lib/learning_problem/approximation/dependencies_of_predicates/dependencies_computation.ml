open Dependencies_of_predicates
open Term
open Clause
open Clause.Aliases

let extract_predicates_symbols_in_clause_that_must_be_compared
    (in_definition_of : Typed_relation_symbol.t)
    (clause : Clause_patterns.t) : Set_typed_relation_symbol.t * Set_typed_relation_symbol.t =
  let is_predicate_interesting (p : Atom_patterns.t) : Typed_relation_symbol.t option =
    let predicate_of_p = Atom_patterns.get_predicate p in
    match predicate_of_p with
    | Eq _ -> None
    | Pred p -> if Typed_relation_symbol.equal in_definition_of p then None else Some p
  in
  let body_filtered =
    clause |> Disjunctive_clause_patterns.get_body |> Set_atom_patterns.to_list
    |> List.filter_map is_predicate_interesting
    |> Set_typed_relation_symbol.of_list
  in
  let head_filtered =
    clause |> Disjunctive_clause_patterns.get_head |> Set_atom_patterns.to_list
    |> List.filter_map is_predicate_interesting
    |> Set_typed_relation_symbol.of_list
  in

  (body_filtered, head_filtered)

let has_positive_occurence (ps : Typed_relation_symbol.t) (clause : Clause_patterns.t) : bool =
  Set_atom_patterns.exists
    (fun pa ->
      match Atom_patterns.get_predicate pa with
      | Eq _ -> false
      | Pred p -> Typed_relation_symbol.equal p ps)
    (Clause_patterns.get_positive clause)

let has_negative_occurence (ps : Typed_relation_symbol.t) (clause : Clause_patterns.t) : bool =
  Set_atom_patterns.exists
    (fun pa ->
      match Atom_patterns.get_predicate pa with
      | Eq _ -> false
      | Pred p -> Typed_relation_symbol.equal p ps)
    (Clause_patterns.get_negative clause)

(* ugly *)
let build_dependency_of_predicates_one_clause
    (in_definition_of : Typed_relation_symbol.t)
    (cl : Clause_patterns.t) : DependenciesOnePredicate.t =
  let body_predicate, head_predicate =
    extract_predicates_symbols_in_clause_that_must_be_compared in_definition_of cl
  in
  let predicate_appears_positively_in_this_clause = has_positive_occurence in_definition_of cl in
  let predicate_appears_negatively_in_this_clause = has_negative_occurence in_definition_of cl in
  let positive_def_contexts =
    [
      Context.{in_positive_def = true; is_positive_occurence = true};
      Context.{in_positive_def = true; is_positive_occurence = false};
    ]
  in

  let negative_def_contexts =
    [
      Context.{in_positive_def = false; is_positive_occurence = true};
      Context.{in_positive_def = false; is_positive_occurence = false};
    ]
  in

  let predicates_appearing_in_clause = [head_predicate; body_predicate] in
  let double_empty = [Set_typed_relation_symbol.empty; Set_typed_relation_symbol.empty] in

  let dependencies =
    List.append
      (if predicate_appears_negatively_in_this_clause then
         List.combine negative_def_contexts predicates_appearing_in_clause
       else
         List.combine negative_def_contexts double_empty)
      (if predicate_appears_positively_in_this_clause then
         List.combine positive_def_contexts predicates_appearing_in_clause
       else
         List.combine positive_def_contexts double_empty)
  in
  DependenciesOnePredicate.of_list dependencies

(* Is not generic enough to handle recursively defined predicates, but can be generalised *)
let build_dependency_of_predicates_one_definition
    (definition_of : Typed_relation_symbol.t)
    (def : Predicate_definition.t) : DependenciesOnePredicate.t =
  let all_clauses_definition = Predicate_definition.extract_clauses_with_injectivity def in
  all_clauses_definition |> Set_clause_patterns.to_list
  |> List.map (build_dependency_of_predicates_one_clause definition_of)
  |> List.fold_left DependenciesOnePredicate.union DependenciesOnePredicate.empty

let build_dependency_of_predicates (lp : Learning_problem.t) : DependenciesPredicates.t =
  lp |> Learning_problem.get_definitions
  |> List.map (fun (ps, def) -> (ps, build_dependency_of_predicates_one_definition ps def))
