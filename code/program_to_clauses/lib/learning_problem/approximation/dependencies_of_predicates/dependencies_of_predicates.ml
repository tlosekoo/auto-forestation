open Clause.Aliases

module Context = struct
  type t = {
    in_positive_def : Bool.t;
    is_positive_occurence : Bool.t;
  }
  [@@deriving equal, compare]

  let pp (c : Format.formatter) (v : t) : unit =
    let str_definition =
      if v.in_positive_def then
        "positive"
      else
        "negative"
    in
    let str_occurence = if v.is_positive_occurence then "positive" else "negative" in
    Format.fprintf c "%s occurence in %s definition" str_occurence str_definition
end

module DependenciesOnePredicate = struct
  module Map_context_predicates = Misc.My_map.Make2 (Context) (Set_typed_relation_symbol)
  include Map_context_predicates

  let union : t -> t -> t =
    Map_context_predicates.merge (fun _context ps1_opt ps2_opt ->
        let ps1 = Option.value ps1_opt ~default:Set_typed_relation_symbol.empty in
        let ps2 = Option.value ps2_opt ~default:Set_typed_relation_symbol.empty in
        Some (Set_typed_relation_symbol.union ps1 ps2))

  let empty =
    let contexts =
      [
        Context.{in_positive_def = true; is_positive_occurence = true};
        Context.{in_positive_def = false; is_positive_occurence = true};
        Context.{in_positive_def = true; is_positive_occurence = false};
        Context.{in_positive_def = false; is_positive_occurence = false};
      ]
    in
    contexts
    |> List.map (fun context -> (context, Set_typed_relation_symbol.empty))
    |> Map_context_predicates.of_list
end

module DependenciesPredicates =
  Misc.List_maker.Make (Misc.Pair.Make (Term.Typed_relation_symbol) (DependenciesOnePredicate))
