open Clause
open Clause.Aliases
open Approximation_lattice
open Dependencies_of_predicates
open Dependencies_computation

let constraints_from_context_and_approximation
    (context : Context.t)
    (approximation_of_defined_predicate : LatticeOnePredicate.t) : LatticeOnePredicate.t =
  (* If clause is of form `? \/ p`, with `p` the predicate being defined, then there is something to do only if
     p must not be underapproximated. That is, if {-} not subset of approximation_of_defined_predicate. *)
  let defined_predicate_forbidden_approximation =
    if context.in_positive_def then LatticeOnePredicate.under else LatticeOnePredicate.over
  in
  let danger =
    not
      (LatticeOnePredicate.is_below defined_predicate_forbidden_approximation
         approximation_of_defined_predicate)
  in
  let operation_to_remove =
    if context.is_positive_occurence then LatticeOnePredicate.over else LatticeOnePredicate.under
  in
  let new_constraints =
    if danger then
      LatticeOnePredicate.complement operation_to_remove
    else
      LatticeOnePredicate.top
  in
  (* let () =
       Format.fprintf Format.std_formatter
         "Context: %a\nApproximation of defined predicate: %a\ndefined predicate forbidden approximation:  %a\n
         Operation to remove:  %a\n  new constraints:  %a\n"
         Context.pp context LatticeOnePredicate.pp approximation_of_defined_predicate
         LatticeOnePredicate.pp defined_predicate_forbidden_approximation LatticeOnePredicate.pp
         operation_to_remove LatticeOnePredicate.pp new_constraints
     in *)
  new_constraints

let approximation_lattice_of_context_occurences_and_approx_of_defined_symbol
    (approximations_of_defined_symbol : LatticeOnePredicate.t)
    ((context, occurences_ps) : Context.t * Set_typed_relation_symbol.t) : ApproximationLattice.t =
  ApproximationLattice.of_list
    (List.map
       (fun ps ->
         (ps, constraints_from_context_and_approximation context approximations_of_defined_symbol))
       (Set_typed_relation_symbol.to_list occurences_ps))

let new_approximation_constraints_for_one_definition
    (dependencies_for_this_predicate : DependenciesOnePredicate.t)
    (approximations_predicate_symbol : LatticeOnePredicate.t) : ApproximationLattice.t =
  dependencies_for_this_predicate |> DependenciesOnePredicate.to_list
  |> List.map
       (approximation_lattice_of_context_occurences_and_approx_of_defined_symbol
          approximations_predicate_symbol)
  |> ApproximationLattice.meet_list

let propagate_approximation_constraints
    (approximations_constraints_from_properties : ApproximationLattice.t)
    (dependencies : DependenciesPredicates.t) : ApproximationLattice.t =
  List.fold_right
    (fun (predicate_being_defined, dependencies) approximations ->
      (* let () =
           Format.fprintf Format.std_formatter
             "Propagating constraints due to definition of %a\nIt has dependencies %a\n"
             Typed_relation_symbol.pp predicate_being_defined DependenciesOnePredicate.pp dependencies
         in
         let () =
           Format.fprintf Format.std_formatter "Before propagation, approximations are\n%a\n"
             ApproximationLattice.pp approximations
         in *)
      let constraints_for_current_definition =
        ApproximationLattice.find predicate_being_defined approximations
      in
      let new_constraints_approximation =
        new_approximation_constraints_for_one_definition dependencies
          constraints_for_current_definition
      in
      (* let () =
           Format.fprintf Format.std_formatter "New approximation generated: %a\n"
             ApproximationLattice.pp new_constraints_approximation
         in *)
      let updated_approximations =
        ApproximationLattice.meet approximations new_constraints_approximation
      in
      (* let () =
           Format.fprintf Format.std_formatter "Updating constraints for %a from\n%a\nto\n%a\n"
             Typed_relation_symbol.pp predicate_being_defined ApproximationLattice.pp approximations
             ApproximationLattice.pp updated_approximations
         in
         let () =
           Format.fprintf Format.std_formatter "New constraints:\n%a\n" ApproximationLattice.pp
             new_constraints_approximation
         in *)
      updated_approximations)
    dependencies approximations_constraints_from_properties

let generate_possible_approximations_from_properties (lp : Learning_problem.t) :
    ApproximationLattice.t =
  let all_clauses = Learning_problem.get_properties lp in
  let list_of_all_clauses = Set_clause_patterns.to_list all_clauses in
  let all_positively_occuring_predicate_symbols =
    list_of_all_clauses
    |> List.map Clause_patterns.get_positive
    |> Set_atom_patterns.list_union |> Set_atom_patterns.to_list
    |> List.map Atom_patterns.get_predicate
    |> List.filter_map Predicate_symbol_or_equality.get_typed_relation_symbol_opt
  in
  let all_negatively_occuring_predicate_symbols =
    list_of_all_clauses
    |> List.map Clause_patterns.get_negative
    |> Set_atom_patterns.list_union |> Set_atom_patterns.to_list
    |> List.map Atom_patterns.get_predicate
    |> List.filter_map Predicate_symbol_or_equality.get_typed_relation_symbol_opt
  in
  let lattice_underapproximation_possibility =
    all_positively_occuring_predicate_symbols
    |> List.map (fun ps -> (ps, LatticeOnePredicate.under))
    |> List.map (fun (ps, t) -> ApproximationLattice.singleton ps t)
  in

  let lattice_overapproximation_possibility =
    all_negatively_occuring_predicate_symbols
    |> List.map (fun ps -> (ps, LatticeOnePredicate.over))
    |> List.map (fun (ps, t) -> ApproximationLattice.singleton ps t)
  in

  let lattice_constraints =
    ApproximationLattice.meet_list
      (lattice_overapproximation_possibility @ lattice_underapproximation_possibility)
  in

  (* let lattice_constraints =
       all_predicate_applications |> Set_litteral_patterns.to_list
       |> List.map (fun l ->
              let pa = Atom_patterns.get_atom l in
              let is_positive = Atom_patterns.is_positive l in
              let ps = Predicate_patterns.get_predicate_symbol pa in
              let lattice_pa =
                if is_positive then
                  Approximation_lattice.LatticeOnePredicate.under
                else
                  Approximation_lattice.LatticeOnePredicate.over
              in
              (ps, lattice_pa))
       |> List.map (fun (ps, t) -> ApproximationLattice.singleton ps t)
       |> ApproximationLattice.meet_list
     in *)
  let all_predicate_symbols = Learning_problem.get_predicates_symbols lp in
  let top =
    all_predicate_symbols |> Set_typed_relation_symbol.to_list
    |> List.map (fun ps -> (ps, LatticeOnePredicate.top))
    |> ApproximationLattice.of_list
  in
  (* Useful for representing every predicate symbol in the lattice, do not remove. *)
  ApproximationLattice.meet top lattice_constraints

let generate_lattice_of_possible_approximation (lp : Learning_problem.t) : ApproximationLattice.t =
  let initial_constraints = generate_possible_approximations_from_properties lp in
  let dependencies = build_dependency_of_predicates lp in
  let constraints = propagate_approximation_constraints initial_constraints dependencies in
  constraints

(* return is possible_underapproximations and possible_overapproximation *)
let analyse_possible_approximations (lp : Learning_problem.t) :
    Set_typed_relation_symbol.t * Set_typed_relation_symbol.t =
  (* let () = Format.fprintf Format.std_formatter "Starting to analyse approximation\n\n" in *)
  let constraints_list =
    ApproximationLattice.to_list (generate_lattice_of_possible_approximation lp)
  in
  let can_overapproximate =
    Set_typed_relation_symbol.of_list
      (List.filter_map
         (fun (ps, lattice) ->
           if LatticeOnePredicate.is_above lattice LatticeOnePredicate.over then Some ps else None)
         constraints_list)
  in
  let can_underapproximate =
    Set_typed_relation_symbol.of_list
      (List.filter_map
         (fun (ps, lattice) ->
           if LatticeOnePredicate.is_above lattice LatticeOnePredicate.under then Some ps else None)
         constraints_list)
  in
  (can_underapproximate, can_overapproximate)
