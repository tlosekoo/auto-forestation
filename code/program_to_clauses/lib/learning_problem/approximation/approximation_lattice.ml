open Term

module OverUnder = struct
  type t =
    | Over
    | Under
  [@@deriving equal, compare]

  let pp (c : Format.formatter) (v : t) =
    let str =
      match v with
      | Over -> "+"
      | Under -> "-"
    in
    Format.fprintf c "%s" str
end

module SetOverUnder = Misc.My_set.Make (OverUnder)

module LatticeOnePredicate = struct
  type t = SetOverUnder.t [@@deriving equal, compare]

  let pp = SetOverUnder.pp
  let join = SetOverUnder.union
  let meet = SetOverUnder.inter
  let top = SetOverUnder.of_list [Over; Under]
  let bot = SetOverUnder.of_list []
  let over = SetOverUnder.of_list [Over]
  let under = SetOverUnder.of_list [Under]
  let meet_list = List.fold_left meet top
  let join_list = List.fold_left join bot
  let is_below (below : t) (reference : t) : bool = SetOverUnder.subset below reference
  let is_above (above : t) (reference : t) : bool = SetOverUnder.subset reference above
  let complement = SetOverUnder.diff top
end

(* This lattice represents approximations that can be done.
   For example, Top = {+, -} means that a relation can be both over-approximated and under-approximated. *)
module ApproximationLattice = struct
  module ApproxEachPredicate = Misc.My_map.Make2 (Typed_relation_symbol) (LatticeOnePredicate)
  include ApproxEachPredicate

  (* is identity for join *and* meet at the same time because the map is initialized for no relation.. *)
  let empty = ApproxEachPredicate.empty

  let join =
    ApproxEachPredicate.merge (fun _predicate val1_opt val2_opt ->
        let val_1 = Option.value val1_opt ~default:LatticeOnePredicate.bot in
        let val_2 = Option.value val2_opt ~default:LatticeOnePredicate.bot in
        Some (LatticeOnePredicate.join val_1 val_2))

  let meet =
    ApproxEachPredicate.merge (fun _predicate val1_opt val2_opt ->
        let val_1 = Option.value val1_opt ~default:LatticeOnePredicate.top in
        let val_2 = Option.value val2_opt ~default:LatticeOnePredicate.top in
        Some (LatticeOnePredicate.meet val_1 val_2))

  let join_list = List.fold_left join empty
  let meet_list = List.fold_left meet empty
end
