(set-logic HORN)


(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)

(declare-fun iss (nat) Bool)

(define-fun-rec allzero ((l natlist)) Bool
	(
		match l
		(
			(nil 
				true
			)
			( (cons n ll) 
				(ite
					(= n z)
					(allzero ll)
					false 
				)
			)
		)
	)
)

(define-fun car ((l natlist)) nat
	(
		match l
		(
			(nil z)
			((cons n ll) n)
		)
	)
)


(assert (forall ((l natlist))
			(=> (allzero l) (iss (car l)))
))

(check-sat)

