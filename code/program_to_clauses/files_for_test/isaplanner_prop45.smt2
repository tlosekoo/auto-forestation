(set-logic HORN)

(declare-datatypes ((elt 0)(pair 0)(list2 0)(list 0))
  (
    ((a)(b))
    ((pair2 (proj1-pair elt) (proj2-pair elt)))
    ((nil2) (cons2 (head2 elt) (tail2 list2)))
    ((nil) (cons (head pair) (tail list)))))

(define-fun-rec zip ((x list2) (y list2)) list
  (match x
    ((nil2 nil)
     ((cons2 z x2)
      (match y
        ((nil2 nil)
         ((cons2 x3 x4) (cons (pair2 z x3) (zip x2 x4)))))))))

(define-fun zip_concat ((x elt) (y list2) (z list2)) list
  (match z
    ((nil2 nil)
     ((cons2 y2 ys) (cons (pair2 x y2) (zip y ys))))))

(assert
    (forall ((x elt) (y elt) (xs list2) (ys list2))
      (= (zip (cons2 x xs) (cons2 y ys))
        (cons (pair2 x y) (zip xs ys)))))

(check-sat)
