(set-logic HORN)


(declare-datatypes ((nat 0) (natlist 0)) 
	(
		(
			(z) 
			(s (pred nat))
		)
		(
			(nil)
			(cons (hd nat) (tl natlist))
		)
	)
)
(declare-fun ici_fun (natlist nat) nat)
(declare-fun ici_rel (natlist nat) Bool)

(define-fun-rec le ((n1 nat) (n2 nat)) Bool
	(
		match n2
		(
			(z 
				false
			)
			( (s nn2) 
				(
					match n1
					( 
						(z true)
						((s nn1) (le nn1 nn2))
					)
				)
			)
		)
	)
)
;(define-fun-rec le ((n1 nat) (n2 nat)) Bool
;	(
;		match n1
;		(
;			(z 
;				(
;					match n2
;					(
;						( z false)
;						( ( s nn2) true)
;					)
;				)
;			)
;			( (s nn1) 
;				(
;					match n2
;					( 
;						(z false)
;						((s nn2) (le nn1 nn2))
;					)
;				)
;			)
;		)
;	)
;)


(define-fun-rec length ((l natlist)) nat
	(
		match l 
		(
			(  nil    z)
			( (cons x ll) (s (length ll)))
		)
	)
)




(assert (forall ((x nat) (l natlist))
			(le (length l) (length (cons x l)))))

(check-sat)

