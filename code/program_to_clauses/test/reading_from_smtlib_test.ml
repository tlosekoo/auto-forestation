open Program_to_clauses

let file_to_parse_from = Paths.files_for_testing_folder ^ "length_cons_le.smt2"
let file_to_parse_from2 = Paths.files_for_testing_folder ^ "prefix_append.smt2"
let file_to_parse_from_allz_car_is = Paths.files_for_testing_folder ^ "allzero_car_is.smt2"
let file_to_parse_from_all_definitions = Paths.files_for_testing_folder ^ "all_definition.smt2"

let%expect_test "transformation from SMTLIB into clauses" =
  let () = Term.Symbol_generator.reset_counter () in
  let learning_problem = Smtlib_reader.smtlib_to_clauses file_to_parse_from in
  Format.fprintf Format.std_formatter "%a" Learning_problem.pp learning_problem;
  [%expect
    "
    env: {
    nat -> {s, z}  ;  natlist -> {cons, nil}
    }
    definition:
    {
    (le, P:
    {
    le(z, s(nn2)) <= True
    False <= le(n1, z)
    le(s(nn1), s(nn2)) <= le(nn1, nn2)
    le(nn1, nn2) <= le(s(nn1), s(nn2))
    }
    )
    (length, F:
    {
    length(nil, z) <= True
    length(cons(x, ll), s(_a)) <= length(ll, _a)
    }
    eq_nat(_c, _d) <= length(_b, _c) /\\ length(_b, _d)
    )
    }

    properties:
    {
    le(_e, _f) <= length(l, _e) /\\ length(cons(x, l), _f)
    }"]

let%expect_test "transformation from SMTLIB into clauses 2" =
  let learning_problem = Smtlib_reader.smtlib_to_clauses file_to_parse_from2 in
  Format.fprintf Format.std_formatter "%a" Learning_problem.pp learning_problem;
  [%expect
    "
    env: {
    elt -> {a, b}  ;  eltlist -> {cons, nil}
    }
    definition:
    {
    (prefix, P:
    {
    prefix(nil, y) <= True
    prefix(cons(y2, zs), cons(y2, ys)) <= prefix(zs, ys)
    prefix(zs, ys) <= prefix(cons(y2, zs), cons(y2, ys))
    eq_elt(z, y2) <= prefix(cons(z, zs), cons(y2, ys))
    False <= prefix(cons(z, zs), nil)
    }
    )
    (append, F:
    {
    append(nil, l2, l2) <= True
    append(cons(h1, t1), l2, cons(h1, _g)) <= append(t1, l2, _g)
    }
    eq_eltlist(_j, _k) <= append(_h, _i, _j) /\\ append(_h, _i, _k)
    )
    }

    properties:
    {
    prefix(l1, _l) <= append(l1, l2, _l)
    }"]

(* TODO:verify *)
let%expect_test "transformation from SMTLIB into clauses all definitions" =
  let learning_problem = Smtlib_reader.smtlib_to_clauses file_to_parse_from_all_definitions in
  Format.fprintf Format.std_formatter "%a" Learning_problem.pp learning_problem;
  [%expect
    "
    env: {
    elist -> {econs, enil}  ;  elt -> {a, b}  ;  epair -> {epairc}  ;  etree -> {leaf, node}  ;  nat -> {s, z}  ;  nlist -> {ncons, nnil}  ;  npair -> {npairc}
    }
    definition:
    {
    (le_nat, P:
    {
    le_nat(z, s(nn2)) <= True
    le_nat(s(nn1), s(nn2)) <= le_nat(nn1, nn2)
    le_nat(nn1, nn2) <= le_nat(s(nn1), s(nn2))
    False <= le_nat(s(nn1), z)
    False <= le_nat(z, z)
    }
    )
    (leq_nat, P:
    {
    leq_nat(z, n2) <= True
    leq_nat(s(nn1), s(nn2)) <= leq_nat(nn1, nn2)
    leq_nat(nn1, nn2) <= leq_nat(s(nn1), s(nn2))
    False <= leq_nat(s(nn1), z)
    }
    )
    (leq_elt, P:
    {
    leq_elt(a, e2) <= True
    leq_elt(b, b) <= True
    False <= leq_elt(b, a)
    }
    )
    (le_elt, P:
    {
    le_elt(a, b) <= True
    False <= le_elt(a, a)
    False <= le_elt(b, e2)
    }
    )
    (plus, F:
    {
    plus(n, z, n) <= True
    plus(n, s(mm), s(_m)) <= plus(n, mm, _m)
    }
    eq_nat(_p, _q) <= plus(_n, _o, _p) /\\ plus(_n, _o, _q)
    )
    (minus, F:
    {
    minus(s(u), z, s(u)) <= True
    minus(z, y, z) <= True
    minus(s(u), s(x2), _r) <= minus(u, x2, _r)
    }
    eq_nat(_u, _v) <= minus(_s, _t, _u) /\\ minus(_s, _t, _v)
    )
    (not_zero, P:
    {
    not_zero(s(nn)) <= True
    False <= not_zero(z)
    }
    )
    (is_even, P:
    {
    is_even(z) <= True
    is_even(s(s(n3))) <= is_even(n3)
    is_even(n3) <= is_even(s(s(n3)))
    False <= is_even(s(z))
    }
    )
    (is_odd, P:
    {
    is_odd(s(z)) <= True
    is_odd(s(s(n3))) <= is_odd(n3)
    is_odd(n3) <= is_odd(s(s(n3)))
    False <= is_odd(z)
    }
    )
    (max_rec, F:
    {
    max_rec(s(u), z, s(u)) <= True
    max_rec(z, y, y) <= True
    max_rec(s(u), s(x2), s(_w)) <= max_rec(u, x2, _w)
    }
    eq_nat(_z, _aa) <= max_rec(_x, _y, _aa) /\\ max_rec(_x, _y, _z)
    )
    (max_ite, F:
    {
    leq_nat(x, y) \\/ max_ite(x, y, x) <= True
    max_ite(x, y, y) <= leq_nat(x, y)
    }
    eq_nat(_da, _ea) <= max_ite(_ba, _ca, _da) /\\ max_ite(_ba, _ca, _ea)
    )
    (min_ite, F:
    {
    leq_nat(x, y) \\/ min_ite(x, y, y) <= True
    min_ite(x, y, x) <= leq_nat(x, y)
    }
    eq_nat(_ha, _ia) <= min_ite(_fa, _ga, _ha) /\\ min_ite(_fa, _ga, _ia)
    )
    (double, F:
    {
    double(z, z) <= True
    double(s(nn), s(s(_ja))) <= double(nn, _ja)
    }
    eq_nat(_la, _ma) <= double(_ka, _la) /\\ double(_ka, _ma)
    )
    (not_nil_elt, P:
    {
    not_nil_elt(econs(x, ll)) <= True
    False <= not_nil_elt(enil)
    }
    )
    (take_elt, F:
    {
    take_elt(s(u), enil, enil) <= True
    take_elt(z, y, enil) <= True
    take_elt(s(u), econs(x2, x3), econs(x2, _na)) <= take_elt(u, x3, _na)
    }
    eq_elist(_qa, _ra) <= take_elt(_oa, _pa, _qa) /\\ take_elt(_oa, _pa, _ra)
    )
    (length_elt, F:
    {
    length_elt(enil, z) <= True
    length_elt(econs(x, ll), s(_sa)) <= length_elt(ll, _sa)
    }
    eq_nat(_ua, _va) <= length_elt(_ta, _ua) /\\ length_elt(_ta, _va)
    )
    (last, F:
    {
    last(ncons(y, nnil), y) <= True
    last(nnil, z) <= True
    last(ncons(y, ncons(x2, x3)), _wa) <= last(ncons(x2, x3), _wa)
    }
    eq_nat(_ya, _za) <= last(_xa, _ya) /\\ last(_xa, _za)
    )
    (length_nat, F:
    {
    length_nat(nnil, z) <= True
    length_nat(ncons(x, ll), s(_ab)) <= length_nat(ll, _ab)
    }
    eq_nat(_cb, _db) <= length_nat(_bb, _cb) /\\ length_nat(_bb, _db)
    )
    (count_elt, F:
    {
    count_elt(x, enil, z) <= True
    eq_elt(h1, x) \\/ count_elt(x, econs(h1, t1), _eb) <= count_elt(x, t1, _eb)
    count_elt(x, econs(x, t1), s(_fb)) <= count_elt(x, t1, _fb)
    }
    eq_nat(_ib, _jb) <= count_elt(_gb, _hb, _ib) /\\ count_elt(_gb, _hb, _jb)
    )
    (append_elt, F:
    {
    append_elt(enil, l2, l2) <= True
    append_elt(econs(h1, t1), l2, econs(h1, _kb)) <= append_elt(t1, l2, _kb)
    }
    eq_elist(_nb, _ob) <= append_elt(_lb, _mb, _nb) /\\ append_elt(_lb, _mb, _ob)
    )
    (delete_all_elt, F:
    {
    delete_all_elt(x, enil, enil) <= True
    delete_all_elt(h, econs(h, t), _qb) <= delete_all_elt(h, t, _qb)
    eq_elt(x, h) \\/ delete_all_elt(x, econs(h, t), econs(h, _pb)) <= delete_all_elt(x, t, _pb)
    }
    eq_elist(_tb, _ub) <= delete_all_elt(_rb, _sb, _tb) /\\ delete_all_elt(_rb, _sb, _ub)
    )
    (delete_one_elt, F:
    {
    delete_one_elt(h, econs(h, t), t) <= True
    delete_one_elt(x, enil, enil) <= True
    eq_elt(x, h) \\/ delete_one_elt(x, econs(h, t), econs(h, _vb)) <= delete_one_elt(x, t, _vb)
    }
    eq_elist(_yb, _zb) <= delete_one_elt(_wb, _xb, _yb) /\\ delete_one_elt(_wb, _xb, _zb)
    )
    (mem_elt, P:
    {
    mem_elt(h, econs(h, t)) <= True
    eq_elt(e, h) \\/ mem_elt(e, econs(h, t)) <= mem_elt(e, t)
    eq_elt(e, h) \\/ mem_elt(e, t) <= mem_elt(e, econs(h, t))
    False <= mem_elt(e, enil)
    }
    )
    (insert_elt, F:
    {
    insert_elt(x, enil, econs(x, enil)) <= True
    insert_elt(x, econs(y, z), econs(y, _ac)) \\/ leq_elt(x, y) <= insert_elt(x, z, _ac)
    insert_elt(x, econs(y, z), econs(x, econs(y, z))) <= leq_elt(x, y)
    }
    eq_elist(_dc, _ec) <= insert_elt(_bc, _cc, _dc) /\\ insert_elt(_bc, _cc, _ec)
    )
    (insert_nat, F:
    {
    insert_nat(x, nnil, ncons(x, nnil)) <= True
    insert_nat(x, ncons(y, z), ncons(y, _fc)) \\/ leq_nat(x, y) <= insert_nat(x, z, _fc)
    insert_nat(x, ncons(y, z), ncons(x, ncons(y, z))) <= leq_nat(x, y)
    }
    eq_nlist(_ic, _jc) <= insert_nat(_gc, _hc, _ic) /\\ insert_nat(_gc, _hc, _jc)
    )
    (sort_elt, F:
    {
    sort_elt(enil, enil) <= True
    sort_elt(econs(y, z), _kc) <= insert_elt(y, _lc, _kc) /\\ sort_elt(z, _lc)
    }
    eq_elist(_nc, _oc) <= sort_elt(_mc, _nc) /\\ sort_elt(_mc, _oc)
    )
    (sort_nat, F:
    {
    sort_nat(nnil, nnil) <= True
    sort_nat(ncons(y, z), _pc) <= insert_nat(y, _qc, _pc) /\\ sort_nat(z, _qc)
    }
    eq_nlist(_sc, _tc) <= sort_nat(_rc, _sc) /\\ sort_nat(_rc, _tc)
    )
    (sorted_elt, P:
    {
    sorted_elt(econs(x, enil)) <= True
    sorted_elt(enil) <= True
    }
    )
    (drop_elt, F:
    {
    drop_elt(s(u), enil, enil) <= True
    drop_elt(z, l, l) <= True
    drop_elt(s(u), econs(x2, x3), _uc) <= drop_elt(u, x3, _uc)
    }
    eq_elist(_xc, _yc) <= drop_elt(_vc, _wc, _xc) /\\ drop_elt(_vc, _wc, _yc)
    )
    (prefix_elt, P:
    {
    prefix_elt(enil, l) <= True
    prefix_elt(econs(y2, zs), econs(y2, ys)) <= prefix_elt(zs, ys)
    prefix_elt(zs, ys) <= prefix_elt(econs(y2, zs), econs(y2, ys))
    eq_elt(z, y2) <= prefix_elt(econs(z, zs), econs(y2, ys))
    False <= prefix_elt(econs(z, zs), enil)
    }
    )
    (numnodes, F:
    {
    numnodes(leaf, z) <= True
    numnodes(node(e, t1, t2), s(_zc)) <= numnodes(t1, _ad) /\\ numnodes(t2, _bd) /\\ plus(_ad, _bd, _zc)
    }
    eq_nat(_dd, _ed) <= numnodes(_cd, _dd) /\\ numnodes(_cd, _ed)
    )
    (shallower, P:
    {
    shallower(leaf, n) <= True
    shallower(node(e, t1, t2), s(m)) <= shallower(t1, m) /\\ shallower(t2, m)
    shallower(t2, m) <= shallower(t1, m) /\\ shallower(node(e, t1, t2), s(m))
    shallower(t1, m) <= shallower(node(e, t1, t2), s(m))
    False <= shallower(node(e, t1, t2), z)
    }
    )
    (subtree, P:
    {
    subtree(leaf, t) <= True
    subtree(node(eb, ta1, ta2), node(eb, tb1, tb2)) <= subtree(ta1, tb1)
    subtree(node(eb, ta1, ta2), node(eb, tb1, tb2)) <= subtree(ta2, tb2)
    False <= subtree(node(ea, ta1, ta2), leaf)
    eq_elt(ea, eb) <= subtree(node(ea, ta1, ta2), node(eb, tb1, tb2))
    subtree(ta1, tb1) <= subtree(node(eb, ta1, ta2), node(eb, tb1, tb2))
    subtree(ta2, tb2) <= subtree(node(eb, ta1, ta2), node(eb, tb1, tb2))
    }
    )
    (memt_elt, P:
    {
    memt_elt(e2, node(e2, t1, t2)) <= True
    eq_elt(e, e2) \\/ memt_elt(e, node(e2, t1, t2)) <= memt_elt(e, t1)
    eq_elt(e, e2) \\/ memt_elt(e, t1) \\/ memt_elt(e, node(e2, t1, t2)) <= memt_elt(e, t2)
    False <= memt_elt(e, leaf)
    eq_elt(e, e2) \\/ memt_elt(e, t1) \\/ memt_elt(e, t2) <= memt_elt(e, node(e2, t1, t2))
    }
    )
    (height, F:
    {
    height(leaf, z) <= True
    height(node(e, t1, t2), s(_fd)) <= height(t1, _gd) /\\ height(t2, _hd) /\\ max_ite(_gd, _hd, _fd)
    }
    eq_nat(_jd, _kd) <= height(_id, _jd) /\\ height(_id, _kd)
    )
    (depth, F:
    {
    depth(leaf, z) <= True
    depth(node(e, t1, t2), s(_nd)) \\/ leq_nat(_ld, _md) <= depth(t1, _ld) /\\ depth(t1, _nd) /\\ depth(t2, _md)
    depth(node(e, t1, t2), s(_od)) <= depth(t1, _pd) /\\ depth(t2, _od) /\\ depth(t2, _qd) /\\ leq_nat(_pd, _qd)
    }
    eq_nat(_sd, _td) <= depth(_rd, _sd) /\\ depth(_rd, _td)
    )
    (flip, F:
    {
    flip(leaf, leaf) <= True
    flip(node(e, t1, t2), node(e, _ud, _vd)) <= flip(t1, _vd) /\\ flip(t2, _ud)
    }
    eq_etree(_xd, _yd) <= flip(_wd, _xd) /\\ flip(_wd, _yd)
    )
    (swap_elt, F:
    {
    swap_elt(epairc(fst, snd), epairc(snd, fst)) <= True
    }
    eq_epair(_ae, _be) <= swap_elt(_zd, _ae) /\\ swap_elt(_zd, _be)
    )
    (swap_nat, F:
    {
    swap_nat(npairc(fst, snd), npairc(snd, fst)) <= True
    }
    eq_npair(_de, _ee) <= swap_nat(_ce, _de) /\\ swap_nat(_ce, _ee)
    )
    }

    properties:
    {

    }"]

let%expect_test "transformation from SMTLIB into clauses 3" =
  let learning_problem = Smtlib_reader.smtlib_to_clauses file_to_parse_from_allz_car_is in
  Format.fprintf Format.std_formatter "%a" Learning_problem.pp learning_problem;
  [%expect
    "
      Function:
      Predicate iss

      env: {
      nat -> {s, z}  ;  natlist -> {cons, nil}
      }
      definition:
      {
      (allzero, P:
      {
      allzero(nil) <= True
      allzero(cons(z, ll)) <= allzero(ll)
      eq_nat(n, z) <= allzero(cons(n, ll))
      allzero(ll) <= allzero(cons(z, ll))
      }
      )
      (car, F:
      {
      car(cons(n, ll), n) <= True
      car(nil, z) <= True
      }
      eq_nat(_ge, _he) <= car(_fe, _ge) /\\ car(_fe, _he)
      )
      (iss, P:
      {

      }
      )
      }

      properties:
      {
      iss(_ie) <= allzero(l) /\\ car(l, _ie)
      }"]

let%test "ok" =
  let () = Printing.pp_ok () in
  true
