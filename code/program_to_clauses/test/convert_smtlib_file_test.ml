open Program_to_clauses
open Term
open Clause.Aliases

let file_to_parse_from = Paths.files_for_testing_folder ^ "length_cons_le.smt2"

let pp_predicate_or_function
    (c : Format.formatter)
    (pred_or_func : Intermediate_datatypes.predicate_or_function) =
  match pred_or_func with
  | Pred pred -> Format.fprintf c "Predicate: %a" Typed_relation_symbol.pp pred
  | Func func -> Format.fprintf c "Function: %a" Typed_function.pp func

let pp_definitions
    (c : Format.formatter)
    (definitions : Convert_smtlib_file.function_and_predicates_definition) =
  Printing.pp_assoclist_long pp_predicate_or_function Set_clause_patterns.pp c definitions

let%expect_test "transformation from SMTLIB into clauses" =
  let () = Term.Symbol_generator.reset_counter () in
  let datataype_env, declarations, definitions, asserts =
    Convert_smtlib_file.convert_smtlib_file_into_datatypeEnv_declarations_definitions_asserts
      file_to_parse_from
  in
  Format.fprintf Format.std_formatter
    "Datatype environment:\n%a\n\nDeclarations:\n%a\n\nDefinitions:\n%a\n\nasserts:\n%a\n"
    Term.Datatype_environment.pp datataype_env
    (Printing.pp_list_sep "\n" Intermediate_datatypes.pp_predicate_of_function)
    declarations pp_definitions definitions Set_clause_patterns.pp asserts;

  [%expect
    "
    Datatype environment:
    {
    nat -> {s, z}  ;  natlist -> {cons, nil}
    }

    Declarations:


    Definitions:
    {
    Predicate: le -> {
    le(nn1, nn2) <= eq_nat(s(nn1), n1) /\\ eq_nat(s(nn2), n2) /\\ le(n1, n2)
    le(n1, n2) <= eq_nat(s(nn1), n1) /\\ eq_nat(s(nn2), n2) /\\ le(nn1, nn2)
    le(n1, n2) <= eq_nat(s(nn2), n2) /\\ eq_nat(z, n1)
    False <= eq_nat(z, n2) /\\ le(n1, n2)
    }
    Function: length -> {
    length(l, s(length(ll))) <= eq_natlist(cons(x, ll), l)
    length(l, z) <= eq_natlist(nil, l)
    }
    }

    asserts:
    {
    le(length(l), length(cons(x, l))) <= True
    }"]
