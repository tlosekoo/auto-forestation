open Program_to_clauses
open Term.Aliases
open Approximation_lattice
open Dependencies_of_predicates

let input_file = Paths.files_for_testing_folder ^ "max_nat.smt2"

let learning_problem =
  let () = Term.Symbol_generator.reset_counter () in
  Smtlib_reader.smtlib_to_clauses input_file

let%expect_test "max_nat learning problem" =
  Format.fprintf Format.std_formatter "%a\n" Learning_problem.pp learning_problem;
  [%expect
    "
    env: {
    nat -> {s, z}
    }
    definition:
    {
    (leq, P:
    {
    leq(z, n2) <= True
    leq(s(nn1), s(nn2)) <= leq(nn1, nn2)
    leq(nn1, nn2) <= leq(s(nn1), s(nn2))
    False <= leq(s(nn1), z)
    }
    )
    (max, F:
    {
    leq(n, m) \\/ max(n, m, n) <= True
    max(n, m, m) <= leq(n, m)
    }
    eq_nat(_c, _d) <= max(_a, _b, _c) /\\ max(_a, _b, _d)
    )
    }

    properties:
    {
    eq_nat(i, _e) <= max(i, j, _e)
    }"]

(* Is expecting:
   Over: {max}
   Under: {}
*)

let%expect_test "Lattice of possible approximation due to constraints only" =
  let initial_approximation_lattice =
    Analyse_approximation.generate_possible_approximations_from_properties learning_problem
  in
  Format.fprintf Format.std_formatter "Lattice of initial approximation:\n%a\n\n"
    Approximation_lattice.ApproximationLattice.pp initial_approximation_lattice;
  [%expect "
    Lattice of initial approximation:
    {
    leq -> {+, -}  ;  max -> {+}
    }"]

let%expect_test "Dependencies between predicates" =
  let dependencies = Dependencies_computation.build_dependency_of_predicates learning_problem in
  Format.fprintf Format.std_formatter "Lattice of approximation:\n%a\n\n"
    Dependencies_of_predicates.DependenciesPredicates.pp dependencies;
  [%expect
    "
    Lattice of approximation:
    [(leq, {
    negative occurence in negative definition -> {}  ;  positive occurence in negative definition -> {}  ;  negative occurence in positive definition -> {}  ;  positive occurence in positive definition -> {}
    }), (max, {
    negative occurence in negative definition -> {}  ;  positive occurence in negative definition -> {}  ;  negative occurence in positive definition -> {leq}  ;  positive occurence in positive definition -> {leq}
    })]"]

let%expect_test "Propagation of constraints over max" =
  let dependencies = Dependencies_computation.build_dependency_of_predicates learning_problem in
  let initial_approximation_lattice =
    Analyse_approximation.generate_possible_approximations_from_properties learning_problem
  in
  let leq_symbol, dependencies_leq = List.nth dependencies 0 in
  let max_symbol, dependencies_max = List.nth dependencies 1 in

  let () =
    Format.fprintf Format.std_formatter "Dependencies are:\n%a: %a\n%a: %a\n\n"
      Term.Typed_relation_symbol.pp max_symbol
      Dependencies_of_predicates.DependenciesOnePredicate.pp dependencies_max
      Term.Typed_relation_symbol.pp leq_symbol
      Dependencies_of_predicates.DependenciesOnePredicate.pp dependencies_leq
  in

  let lattice_of_max =
    Approximation_lattice.ApproximationLattice.find max_symbol initial_approximation_lattice
  in

  let () =
    Format.fprintf Format.std_formatter "The initial (and final) lattice of max is  %a\n"
      Approximation_lattice.LatticeOnePredicate.pp lattice_of_max
  in

  let propagation_of_constraint_lattice_over_max =
    Analyse_approximation.new_approximation_constraints_for_one_definition dependencies_max
      lattice_of_max
  in

  let () =
    Format.fprintf Format.std_formatter
      "The definition of max causes the following constraints: %a\n"
      Approximation_lattice.ApproximationLattice.pp propagation_of_constraint_lattice_over_max
  in

  let approximation_lattice_from_each_context =
    dependencies_max |> Dependencies_of_predicates.DependenciesOnePredicate.to_list
    |> List.map (fun (context, predicates_appearing_in_this_context) ->
           ( context,
             Analyse_approximation
             .approximation_lattice_of_context_occurences_and_approx_of_defined_symbol
               lattice_of_max
               (context, predicates_appearing_in_this_context) ))
  in

  let () =
    Format.fprintf Format.std_formatter "Approximation lattice for each context:\n%a\n"
      (Printing.pp_assoclist_long Dependencies_of_predicates.Context.pp
         Approximation_lattice.ApproximationLattice.pp)
      approximation_lattice_from_each_context
  in

  let context_positive_occurence_in_positive_definition =
    Dependencies_of_predicates.Context.{in_positive_def = true; is_positive_occurence = true}
  in

  (* let predicates_appearing_in_this_context =
       Dependencies_of_predicates.DependenciesOnePredicate.find
         context_positive_occurence_in_positive_definition dependencies_max
     in *)
  let generated_lattice_for_this_context =
    Analyse_approximation.constraints_from_context_and_approximation
      context_positive_occurence_in_positive_definition lattice_of_max
  in

  let () =
    Format.fprintf Format.std_formatter
      "In context %a and from constraints %a, we generate constraints %a\n" Context.pp
      context_positive_occurence_in_positive_definition LatticeOnePredicate.pp lattice_of_max
      LatticeOnePredicate.pp generated_lattice_for_this_context
  in

  ();
  [%expect
    {|
    Dependencies are:
    max: {
    negative occurence in negative definition -> {}  ;  positive occurence in negative definition -> {}  ;  negative occurence in positive definition -> {leq}  ;  positive occurence in positive definition -> {leq}
    }
    leq: {
    negative occurence in negative definition -> {}  ;  positive occurence in negative definition -> {}  ;  negative occurence in positive definition -> {}  ;  positive occurence in positive definition -> {}
    }

    The initial (and final) lattice of max is  {+}
    The definition of max causes the following constraints: {
    leq -> {}
    }
    Approximation lattice for each context:
    {
    positive occurence in positive definition -> {
    leq -> {-}
    }
    negative occurence in positive definition -> {
    leq -> {+}
    }
    positive occurence in negative definition -> {

    }
    negative occurence in negative definition -> {

    }
    }
    In context positive occurence in positive definition and from constraints {+}, we generate constraints {-} |}]

let%expect_test "Lattice of possible approximation" =
  let approximation_lattice =
    Analyse_approximation.generate_lattice_of_possible_approximation learning_problem
  in
  Format.fprintf Format.std_formatter "Lattice of approximation:\n%a\n\n"
    Approximation_lattice.ApproximationLattice.pp approximation_lattice;
  [%expect "
    Lattice of approximation:
    {
    leq -> {}  ;  max -> {+}
    }"]

let%expect_test "Possible approximation" =
  let under, over = Analyse_approximation.analyse_possible_approximations learning_problem in
  Format.fprintf Format.std_formatter "Over approximation: %a\nUnder approximation: %a \n\n"
    Set_typed_relation_symbol.pp over Set_typed_relation_symbol.pp under;
  [%expect "
    Over approximation: {max}
    Under approximation: {}"]

let%expect_test "shallower" =
  let input_file = Paths.files_for_testing_folder ^ "shallower.smt2" in
  let learning_problem = Smtlib_reader.smtlib_to_clauses input_file in
  Format.fprintf Format.std_formatter "%a\n" Learning_problem.pp learning_problem;
  [%expect
    "
    env: {
    elt -> {a, b}  ;  etree -> {leaf, node}  ;  nat -> {s, z}
    }
    definition:
    {
    (subtree, P:
    {
    subtree(leaf, t) <= True
    subtree(node(eb, ta1, ta2), node(eb, tb1, tb2)) <= subtree(ta1, tb1) /\\ subtree(ta2, tb2)
    subtree(ta2, tb2) <= subtree(ta1, tb1) /\\ subtree(node(eb, ta1, ta2), node(eb, tb1, tb2))
    False <= subtree(node(ea, ta1, ta2), leaf)
    eq_elt(ea, eb) <= subtree(node(ea, ta1, ta2), node(eb, tb1, tb2))
    subtree(ta1, tb1) <= subtree(node(eb, ta1, ta2), node(eb, tb1, tb2))
    }
    )
    (shallower, P:
    {
    shallower(leaf, n) <= True
    shallower(node(e, t1, t2), s(m)) <= shallower(t1, m) /\\ shallower(t2, m)
    shallower(t2, m) <= shallower(t1, m) /\\ shallower(node(e, t1, t2), s(m))
    shallower(t1, m) <= shallower(node(e, t1, t2), s(m))
    False <= shallower(node(e, t1, t2), z)
    }
    )
    }

    properties:
    {
    shallower(s, d) <= shallower(t, d) /\\ subtree(s, t)
    }"]

let%test "ok" =
  let () = Printing.pp_ok () in
  true
