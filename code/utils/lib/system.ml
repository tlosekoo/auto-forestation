let sleepf (duration : float) : unit =
  let sleep_command = Printf.sprintf "sleep %f" duration in
  ignore (Sys.command sleep_command)
