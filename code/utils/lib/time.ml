exception TimeUp

type time_limit = float option

let is_time_up (time_opt : float option) : bool =
  match time_opt with
  | None -> false
  | Some time -> Unix.gettimeofday () > time

let raise_time_up (time_opt : float option) : unit = if is_time_up time_opt then raise TimeUp

