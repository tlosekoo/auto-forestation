let rec fixpoint_from (eq : 'a -> 'a -> bool) (f : 'a -> 'a) (e : 'a) : 'a =
  if eq (f e) e then
    e
  else
    fixpoint_from eq f (f e)

let lexicographical_order (cmp1 : 'a -> 'a -> int) (cmp2 : 'b -> 'b -> int) : 'a * 'b -> 'a * 'b -> int =
  fun (a1, b1) (a2, b2) ->
    let compare_fst = cmp1 a1 a2 in
    if compare_fst <> 0 then
      compare_fst
    else
      cmp2 b1 b2