(** 'max cmp x y' returns the 'max' between 'x' and 'y' based on the given 'cmp' comparison function. *)
val max : ('a -> 'a -> int) -> 'a -> 'a -> 'a

(** 'min cmp x y' returns the 'min' between 'x' and 'y' based on the given 'cmp' comparison function. *)
val min : ('a -> 'a -> int) -> 'a -> 'a -> 'a

(** 'fixpoint_from eq f e' iterates 'f', starting from 'e', until 'eq f(x) x'. Doesn't necessarily terminate. *)
val fixpoint_from : ('a -> 'a -> bool) -> ('a -> 'a) -> 'a -> 'a

(* Given two orders, compute the lexical ordering of a given pair. *)
val lexicographical_order : ('a -> 'a -> int) -> ('b -> 'b -> int) -> 'a * 'b -> 'a * 'b -> int


val for_n : ('a -> 'a) -> 'a -> int -> 'a