libs=(inout printing misc term io_clingo clause tree_tuple_formalisms program_to_clauses model_checking model_inference)
# echo ${libs[*]}

from=inout
to=model_checking

if (($# == 1)); then
   from=${1%/}
   to=$from
fi
if (($# == 2)); then
   from=${1%/}
   to=${2%/}
fi

pin=false

echo -e "Updating pin from \"$from\" and to \"$to\"...\n"

for lib in "${libs[@]}"; do
	if [ "$lib" == "$from" ]; then
    	pin=true
	fi
	if [ "$pin" = true ] ; then
		#For desperate situation
		#echo -e "\nBuilding \"$lib\"!\n"
		echo -e "\nUnpinning and cleaning \"$lib\"!\n"
		(cd $lib ; opam pin remove . -y )
		(cd $lib ; dune clean)
		# (cd $lib ; dune clean ; rm *.opam)
	fi
	if [ "$lib" == "$to" ]; then
    	pin=false
	fi
	echo ""
done
