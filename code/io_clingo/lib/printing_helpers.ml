let rec pp_list_aux
    ~(sep : string)
    (pp_element : Format.formatter -> 'a -> unit)
    (c : Format.formatter)
    (l : 'a list) : unit =
  match l with
  | [] -> ()
  | [e] -> pp_element c e
  | e :: l' -> Format.fprintf c "%a%s%a" pp_element e sep (pp_list_aux ~sep pp_element) l'

let pp_list
    ?(sep = ", ")
    ?(left = "[")
    ?(right = "]")
    (pp : Format.formatter -> 'a -> unit)
    (channel : Format.formatter)
    (l : 'a list) : unit =
  Format.fprintf channel "%s%a%s" left (pp_list_aux ~sep pp) l right

(* let pp_comma_and_space (c : Format.formatter) () : unit = Format.fprintf c ", " *)
