type stopping_reason = Timeout

val equal_stopping_reason : stopping_reason -> stopping_reason -> bool
val compare_stopping_reason : stopping_reason -> stopping_reason -> int

type outcome = (Output_facts.t, stopping_reason, unit) Misc.Tription.t

val equal_outcome : outcome -> outcome -> bool
val compare_outcome : outcome -> outcome -> int

(** This module provides a basic way of interaction with the clingo solver (via command line and buffer files *)

(** Given a temporary folder to store buffer files, an analysis to run, and an input for this analysis,
   	runs the analysis on this input and, if solvable, returns the associated solver data. *)
val run_analysis :
  ?time_limit:int ->
  ?additional_clingo_options:string ->
  path_temporary_folder:string ->
  Analysis.t ->
  (Format.formatter -> unit) ->
  outcome

val pp_stopping_reason : Format.formatter -> stopping_reason -> unit
val pp_solution : Format.formatter -> outcome -> unit

(* Converts the time to stop , for example 1st january 2023, 19:35:58.57, to the number of seconds until that point in time is reached (min. 1sec) *)
val from_time_to_stop_to_time_available : Float.t option -> int

(* Debugging *)
val transform_clingo_output_to_facts : string -> string -> outcome

val extract_model_text_from_clingo_output :
  string -> string -> ((String.t, stopping_reason, unit) Misc.Tription.t, String.t) Result.t
