type t

(** The name should be constituted from 'a'-'z' and '_' only. Raises an exception otherwise.*)
val create : name:string -> algorithm_paths:string list -> t

val get_name : t -> string
val get_algorithm_path : t -> string list
