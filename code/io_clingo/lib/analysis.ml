type t = {
  name : String.t;
  algorithm_paths : String.t list;
}
[@@derive equal, compare]

let code_a = Char.code 'a'
let code_z = Char.code 'z'
let code_A = Char.code 'A'
let code_Z = Char.code 'Z'
let code__ = Char.code '_'

let is_valid_char (c : char) : bool =
  let orc = Char.code c in
  let c_is_lowercase = code_a <= orc && orc <= code_z in
  let c_is_uppercase = code_A <= orc && orc <= code_Z in
  let c_is__ = orc = code__ in
  c_is_lowercase || c_is_uppercase || c_is__

let create ~(name : string) ~(algorithm_paths : string list) : t =
  if String.for_all is_valid_char name then
    {name; algorithm_paths}
  else
    let error_message =
      Format.sprintf
        "\"%s\"  is not a valid analysis name because it contains characters that are not from 'a'-'z', 'A'-'Z' or '_'."
        name
    in
    raise (Failure error_message)

let get_name (tt : t) = tt.name
let get_algorithm_path (tt : t) = tt.algorithm_paths
