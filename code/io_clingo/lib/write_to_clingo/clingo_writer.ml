module type Printable = sig
  type t

  val pp : Format.formatter -> t -> unit
end

module PrintableClause (X : Printable) = struct
  type t = X.t list * X.t list

  (** Head then body *)
  let create (head : X.t list) (body : X.t list) = (head, body)

  let pp (c : Format.formatter) ((head, body) : t) =
    let pp_list_x = Printing_helpers.pp_list ~sep:", " ~left:"" ~right:"" X.pp in
    Format.fprintf c "%a :- %a." pp_list_x head pp_list_x body
end

let pp_clauses
    (type printable)
    (module P : Printable with type t = printable)
    (c : Format.formatter)
    (l : PrintableClause(P).t list) : unit =
  let module PP = PrintableClause (P) in
  Printing_helpers.pp_list ~sep:"\n" ~left:"" ~right:"" PP.pp c l

(* module PP_true : Printable = struct
     type t = unit
     let pp c () = Format.fprintf c "#true"
   end
   module PP_false : Printable = struct
     type t = unit
     let pp c () = Format.fprintf c "#false"
   end *)

let pp_true (c : Format.formatter) : unit = Format.fprintf c "#true"
let pp_false (c : Format.formatter) : unit = Format.fprintf c "#false"
