let clingo_input_file_extension = "lp"
let clingo_output_file_extension = "lpo"
let clingo_error_file_extension = "lperr"

let get_filenames_for_clingo_input_output_error ~(path : string) (a : Analysis.t) :
    string * string * string =
  let prefix_for_this_analaysis = Analysis.get_name a in
  let extensions_input_output =
    [clingo_input_file_extension; clingo_output_file_extension; clingo_error_file_extension]
  in
  let input_and_output_file =
    Inout.Write_to_file.get_new_filenames path prefix_for_this_analaysis extensions_input_output
  in
  let input_file = List.nth input_and_output_file 0 in
  let output_file = List.nth input_and_output_file 1 in
  let error_file = List.nth input_and_output_file 2 in
  (input_file, output_file, error_file)

let run_clingo_algorithm
    ~(time_limit : int)
    ~(additional_clingo_options : string)
    (input_filename : string)
    (output_filename : string)
    (error_filename : string)
    (a : Analysis.t) : unit =
  let filenames_clingo_solving =
    List.fold_left
      (fun space_separated_files -> ( ^ ) (space_separated_files ^ " "))
      "" (Analysis.get_algorithm_path a)
  in
  (* let additional_clingo_options = Option.value ~default:"" additional_clingo_options in *)
  let command =
    Printf.sprintf "clingo %s %s %s --outf=1 --time-limit %d 1>%s 2>%s" filenames_clingo_solving
      input_filename additional_clingo_options time_limit output_filename error_filename
  in
  (* let () = Format.fprintf Format.std_formatter "\ncommand:   %s\n" command in *)
  let _ = Sys.command command in
  ()

type stopping_reason = Timeout [@@deriving equal, compare]
type outcome = (Output_facts.t, stopping_reason, Unit.t) Misc.Tription.t [@@deriving equal, compare]

let pp_stopping_reason c (sr : stopping_reason) : unit =
  match sr with
  | Timeout -> Format.fprintf c "Timeout"

let pp_solution = Misc.Tription.pp Output_facts.pp_fact_list pp_stopping_reason Printing.pp_unit

let extract_model_text_from_clingo_output (output_filename : string) (error_filename : string) :
    ((String.t, stopping_reason, unit) Misc.Tription.t, String.t) Result.t =
  Result.bind (Inout.Reader.get_all_lines_from_file output_filename) (fun lines ->
      let lines_without_comment =
        List.filter (fun line -> line |> String.starts_with ~prefix:"%" |> not) lines
      in
      (* let () =
           Format.fprintf Format.std_formatter "Lines without comment:\n%a\n"
             (Printing.pp_list_and_brackets_sep "\n" Misc.String.pp)
             lines_without_comment
         in *)
      let line_stating_satisfiability = List.nth_opt lines_without_comment 0 in
      match line_stating_satisfiability with
      | Some "ANSWER" -> Result.ok (Misc.Tription.Yes (List.nth lines_without_comment 1))
      | Some "INCONSISTENT" -> Result.ok (Misc.Tription.No ())
      | Some "UNKNOWN" ->
          let lines_of_error = Inout.Reader.get_all_lines_from_file error_filename in
          Result.bind lines_of_error (fun lines ->
              let interrupted_by_signal =
                List.exists
                  (String.starts_with ~prefix:"*** Info : (clingo): INTERRUPTED by signal!")
                  lines
              in
              let solving_stopped_by_signal =
                List.exists
                  (String.starts_with ~prefix:"*** Info : (clingo): Sending shutdown signal...")
                  lines
              in
              if interrupted_by_signal || solving_stopped_by_signal then
                Result.ok (Misc.Tription.Maybe Timeout)
              else
                Result.error (Format.sprintf "Malformed file: %s" output_filename))
      | Some line_stating_satisfiability ->
          let lines_appended = String.concat "\n" lines in
          Result.Error
            (Format.sprintf
               "Failed to parse clingo output for file %s. Line stating satisfiability is: %s. All lines are %s"
               output_filename line_stating_satisfiability lines_appended)
      | None ->
          Result.Error
            (Format.sprintf "Failed to parse clingo output for file %s. What happended?"
               output_filename))

(* Given the analysis type and the analysis input, returns the filename of the solver's output *)
let run_clingo
    ~(time_limit : int)
    ~(additional_clingo_options : string)
    ~(path_temporary_folder : string)
    (a : Analysis.t)
    (pp_necessary_information : Format.formatter -> unit) : string * string =
  let filename_clingo_input, filename_clingo_output, filename_clingo_errors =
    get_filenames_for_clingo_input_output_error ~path:path_temporary_folder a
  in
  let () = Inout.Write_to_file.write_from_pp filename_clingo_input pp_necessary_information in
  let () =
    run_clingo_algorithm ~time_limit ~additional_clingo_options filename_clingo_input
      filename_clingo_output filename_clingo_errors a
  in
  (filename_clingo_output, filename_clingo_errors)

(* Read clingo output file and parse it *)
let transform_clingo_output_to_facts (output_file : string) (errors_file : string) : outcome =
  let model_text_result = extract_model_text_from_clingo_output output_file errors_file in
  (* let () =
       if Result.is_ok model_text_result then
         Format.fprintf Format.std_formatter "%a"
           (Misc.Tription.pp Misc.String.pp pp_stopping_reason Misc.Unit.pp)
           (Result.get_ok model_text_result)
     in *)
  let model_result =
    Result.bind model_text_result (fun model_text_opt ->
        match model_text_opt with
        | Yes model_text ->
            Result.map Misc.Tription.yes (Clingo_reader.parse_clingo_model_from_string model_text)
        | No nw -> Result.Ok (Misc.Tription.No nw)
        | Maybe mw -> Result.Ok (Misc.Tription.Maybe mw))
  in
  match model_result with
  | Error error_message -> raise (Failure error_message)
  | Ok model -> model

(*   *)
(*   *)
(* Function to be called from outside. Runs the analysis *)
let run_analysis
    ?(time_limit = 0)
    ?(additional_clingo_options = "")
    ~(path_temporary_folder : string)
    (a : Analysis.t)
    (pp_necessary_information : Format.formatter -> unit) : outcome =
  let output, errors =
    run_clingo ~time_limit ~additional_clingo_options ~path_temporary_folder a
      pp_necessary_information
  in
  transform_clingo_output_to_facts output errors

let from_time_to_stop_to_time_available (time_to_stop : Float.t option) : int =
  let time_left = Option.map (fun t -> t -. Unix.gettimeofday ()) time_to_stop in
  let seconds_available =
    Option.fold ~none:0 ~some:(fun ft -> Int.max (Float.to_int ft) 1) time_left
  in
  seconds_available
