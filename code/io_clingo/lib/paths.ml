let temporary_folder = "/tmp/"

let clingo_files_path : string =
  match External_files.Sites.clingo_files with
  | [folder_path] -> folder_path ^ "/"
  | _ -> assert false
