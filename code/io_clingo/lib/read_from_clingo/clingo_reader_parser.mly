
%token <int> NUM
%token <string> IDENTIFIER
%token MINUS
%token SPACE
%token COMMA
%token LPAREN RPAREN
%token DOT
%token QUOTE
%token EOF

%{
	(* because dune is retarted, the following line is necessary to prevent autoreference. *)
	module Io_clingo = struct end
	open Output_facts
%}

%start main             /* the entry point */
%type < Fact.t List.t > main
%%
main:
    facts EOF             	               { $1 }
;

facts:
									    	{ [] } 
  | fact 						            { [$1] }
  | fact SPACE facts				        { $1::$3 }
;

fact:
	dotless_fact DOT					    { $1 }
;

dotless_fact:
	  IDENTIFIER                              { Fact.{predicate=$1;args=[]} }
	| predicate=IDENTIFIER LPAREN args=args RPAREN           { Fact.{predicate;args} }
;

arg:
      MINUS NUM        					    { Num (0 - $2) }
    | NUM            					    { Num $1 }
	| IDENTIFIER 						    { Sym $1 }
	| QUOTE IDENTIFIER QUOTE 			    { Str $2 }
	| QUOTE NUM QUOTE 			            { Str (string_of_int $2) }
	| LPAREN args RPAREN                    { Tuple $2 }
;

args:
											{ [] }
	| arg									{ [$1] }
	| arg COMMA args						{ $1::$3 }

