let parse_clingo_model_from_string (s : string) : (Output_facts.t, String.t) Result.t =
  try
    let lexbuf = Lexing.from_string s in
    let model = Clingo_reader_parser.main Clingo_reader_lexer.token lexbuf in
    Result.Ok model
  with
  | Failure caught_error ->
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter
             "Cannot convert string                %s                into facts.\nError is %s" s
             caught_error)
      in
      Result.Error error_message
  | Clingo_reader_parser.Error ->
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter
             "Cannot convert string \n%s\n into a model:Parsing error." s)
      in
      Result.Error error_message
