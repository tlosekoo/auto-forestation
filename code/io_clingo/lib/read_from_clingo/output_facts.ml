open Printing_helpers

module Arg = struct
  type t =
    | Str of String.t
    | Sym of String.t
    | Num of Int.t
    | Tuple of t List.t
  [@@deriving equal, compare]

  let rec pp (c : Format.formatter) (arrrg : t) : unit =
    match arrrg with
    | Str str -> Format.fprintf c "\"%s\"" str
    | Sym str -> Format.pp_print_string c str
    | Num n -> Format.pp_print_int c n
    | Tuple arrrgs -> pp_list ~left:"(" ~right:")" pp c arrrgs
end

module Args = struct
  type t = Arg.t List.t [@@deriving compare, equal]

  let pp (c : Format.formatter) : t -> unit =
    Format.fprintf c "%a" (pp_list ~left:"" ~right:"" Arg.pp)
end

module Fact = struct
  type t = {
    predicate : String.t;
    args : Args.t;
  }
  [@@deriving equal, compare]

  let pp (c : Format.formatter) (fakt : t) : unit =
    if fakt.args = [] then
      Format.fprintf c "%s" fakt.predicate
    else
      Format.fprintf c "%s(%a)" fakt.predicate Args.pp fakt.args
end

type t = Fact.t List.t [@@deriving equal, compare]

let pp_fact_list : Format.formatter -> t -> unit =
  Format.pp_print_list ~pp_sep:Format.pp_print_newline Fact.pp

module Set_of_args = struct
  include Misc.My_set.Make (Args)

  let pp : Format.formatter -> t -> unit = pp_param ~opening:"{" ~closing:"}" ~sep:"  ;  "
end

module Map_predicate_to_args = Misc.My_map.Make2 (Misc.String) (Set_of_args)

(* type arranged_facts = Set_of_Args.t Map_predicate_to_args.t *)

let rec arrange_output_facts (fact_list : Fact.t List.t) : Map_predicate_to_args.t =
  match fact_list with
  | [] -> Map_predicate_to_args.empty
  | h :: t ->
      let rec_arranged_facts = arrange_output_facts t in
      let new_set_of_args =
        match Map_predicate_to_args.find_opt h.predicate rec_arranged_facts with
        | None -> Set_of_args.singleton h.args
        | Some other_args -> Set_of_args.add h.args other_args
      in
      Map_predicate_to_args.add h.predicate new_set_of_args rec_arranged_facts
