(** This file implements the structuration of parsed data from clingo's output *)

(** Given the text of some clingo model, computes the data that it describes. *)
val parse_clingo_model_from_string : string -> (Output_facts.t, string) Result.t
