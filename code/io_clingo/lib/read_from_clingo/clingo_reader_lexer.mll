
{
open Clingo_reader_parser
exception Eof
}
rule token = parse
    (* '\n'                                 { EOL } *)
  | ' '                                          { SPACE }
  | ['0'-'9']+ as num                            { NUM(int_of_string num) }
  | ['a'-'z' '0'-'9' '_' 'A'-'Z' '\'']+ as word  { IDENTIFIER(word) }
  | ','                                          { COMMA }
  | ['"']                                        { QUOTE }
  | '('                                          { LPAREN }
  | ')'                                          { RPAREN }
  | '.'                                          { DOT }
  | '-'                                          { MINUS }
  | eof                                          { EOF }