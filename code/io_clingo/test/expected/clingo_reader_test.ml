open Io_clingo

let algorithm_name = "ici/here_is_test_file.lp"
let complete_path = Paths.clingo_files_path ^ algorithm_name
let test_analysis = Analysis.create ~name:"test" ~algorithm_paths:[complete_path]
let heavy_file_name = "ici/heavy_file.lp"
let complete_path2 = Paths.clingo_files_path ^ heavy_file_name
let test_analysis2 = Analysis.create ~name:"test_heavy" ~algorithm_paths:[complete_path2]
let model_file_name = "ici/model_generalisation_generated_9.lpo"
let complete_path3 = Paths.clingo_files_path ^ model_file_name
let incremental_solving_file_name = "ici/incremental_solving.lp"
let complete_path4 = Paths.clingo_files_path ^ incremental_solving_file_name
let test_analysis4 = Analysis.create ~name:"test_incremental" ~algorithm_paths:[complete_path4]

(* let pp_input (c : Format.formatter) : unit =
   Format.fprintf c "iLikeThisParity(7). iWantToCountTo(10). hello." *)

module PrintableAll = struct
  type t =
    | ILikeThisParity of int
    | IWantToCount of int
    | Hello of unit

  let pp (c : Format.formatter) (v : t) : unit =
    match v with
    | ILikeThisParity v -> Format.fprintf c "iLikeThisParity(%d)" v
    | IWantToCount v -> Format.fprintf c "iWantToCountTo(%d)" v
    | Hello () -> Format.fprintf c "hello"
end

module Clause = Clingo_writer.PrintableClause (PrintableAll)
open PrintableAll

let clause_1 = Clause.create [ILikeThisParity 7] []
let clause_2 = Clause.create [IWantToCount 10] []
let clause_3 = Clause.create [Hello ()] []
let clauses = [clause_1; clause_2; clause_3]
let pp_input1 c = Clingo_writer.pp_clauses (module PrintableAll) c clauses
let pp_nothing _ = ()

let%expect_test "" =
  let model_opt =
    Use_clingo_as_solver.run_analysis ~path_temporary_folder:Paths.temporary_folder test_analysis
      pp_input1
  in
  match model_opt with
  | No () -> assert false
  | Maybe _ -> assert false
  | Yes model ->
      Output_facts.pp_fact_list Format.std_formatter model;
      [%expect
        "
       helloBack
       counting((0, (1, (2, (3, (4, (5, (6, (7, (8, (9, 10)))))))))))
       iLikeThisParity(7)
       iLikeThisParity(5)
       iLikeThisParity(3)
       iLikeThisParity(1)
       bigTerm((), 4, \"Hello\", (thisIsRecursive, ('_strange_symbols_''', 7, \"a\", \"8\")))"]

let%test "" =
  let model_opt =
    Use_clingo_as_solver.run_analysis ~time_limit:1 ~path_temporary_folder:Paths.temporary_folder
      test_analysis2 pp_input1
  in
  match model_opt with
  | No () -> false
  | Yes _ -> false
  | Maybe sr -> (
      match sr with
      | Use_clingo_as_solver.Timeout -> true)

let%expect_test "reading a generated model" =
  let text_model =
    Use_clingo_as_solver.extract_model_text_from_clingo_output complete_path3 "/dev/null"
  in
  Format.fprintf Format.std_formatter "%a"
    (Misc.Tription.pp Misc.String.pp Use_clingo_as_solver.pp_stopping_reason Misc.Unit.pp)
    (Result.get_ok text_model);
  [%expect
    "Yes: mapped_to((a_0,q_gen_da),(a_0,q_gen_da)). mapped_to((a_0,q_gen_ca),(a_0,q_gen_ca)). mapped_to((a_0,q_gen_ba),(a_0,q_gen_ba)). mapped_to((a_5,q_gen_fa),(a_5,q_gen_fa)). final(zip,(a_0,q_gen_ba)). final(zip_concat,(a_5,q_gen_fa)). mapped_to((a_0,q_gen_ea),(a_0,q_gen_ba))."]

let%expect_test "reading a generated model" =
  let read_model =
    Use_clingo_as_solver.transform_clingo_output_to_facts complete_path3 "/dev/null"
  in
  Format.fprintf Format.std_formatter "%a" Use_clingo_as_solver.pp_solution read_model;
  [%expect
    "
     Yes: mapped_to((a_0, q_gen_da), (a_0, q_gen_da))
     mapped_to((a_0, q_gen_ca), (a_0, q_gen_ca))
     mapped_to((a_0, q_gen_ba), (a_0, q_gen_ba))
     mapped_to((a_5, q_gen_fa), (a_5, q_gen_fa))
     final(zip, (a_0, q_gen_ba))
     final(zip_concat, (a_5, q_gen_fa))
     mapped_to((a_0, q_gen_ea), (a_0, q_gen_ba))"]

let%expect_test "" =
  let model_opt =
    Use_clingo_as_solver.run_analysis ~path_temporary_folder:Paths.temporary_folder test_analysis4
      pp_nothing
  in
  match model_opt with
  | No () -> assert false
  | Maybe _ -> assert false
  | Yes model ->
      Output_facts.pp_fact_list Format.std_formatter model;
      [%expect
        "
          p(0)
          p(1)
          p(2)
          p(3)
          p(4)
          p(5)
          q(1, 5)
          q(2, 5)
          q(3, 5)
          q(4, 5)
          query(5)
          q(5, 5)"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
