open Io_clingo
open Output_facts

let fact_1 = Fact.{predicate = "P"; args = [Num 4; Sym "b"]}
let fact_2 = Fact.{predicate = "Q"; args = []}
let fact_3 = Fact.{predicate = "P"; args = [Sym "a"]}
let fact_4 = Fact.{predicate = "R"; args = [Tuple []; Num 7]}
let fact_5 = Fact.{predicate = "R"; args = [Tuple [Sym "c"; Str "d"]; Num 7]}
let facts = [fact_1; fact_2; fact_3; fact_4; fact_5]
let arranged_facts = Output_facts.arrange_output_facts facts

let%expect_test "pp_facts" =
  pp_fact_list Format.std_formatter facts;
  [%expect "
    P(4, b)
    Q
    P(a)
    R((), 7)
    R((c, \"d\"), 7)"]

let%expect_test "pp_arranged_acts" =
  (* let facts_for_p =
       arranged_facts |> Map_predicate_to_args.find "P" |> Set_of_args.to_seq |> List.of_seq
     in
     let facts_for_r =
       arranged_facts |> Map_predicate_to_args.find "R" |> Set_of_args.to_seq |> List.of_seq
     in
     Format.pp_print_list Args.pp Format.std_formatter facts_for_p;
     Format.pp_print_newline Format.std_formatter ();
     Format.pp_print_newline Format.std_formatter ();
     Format.pp_print_list Args.pp Format.std_formatter facts_for_r; *)
  Map_predicate_to_args.pp Format.std_formatter arranged_facts;
  [%expect "
    {
    P -> {a  ;  4, b}  ;  Q -> {}  ;  R -> {(), 7  ;  (c, \"d\"), 7}
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
