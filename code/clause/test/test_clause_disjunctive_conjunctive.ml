open Clause

let clause =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[]
    ~positive:[Database_for_tests.atom_length_nil_z]

let extended_atom_true = Extended_atom_patterns.True
let extended_atom_false = Extended_atom_patterns.False
let extended_atom = Extended_atom_patterns.Atom Database_for_tests.atom_rn_z

(* DISJUNCTIVE *)
let%expect_test "Disjunctive Clause" =
  Disjunctive_clause_patterns.pp Format.std_formatter clause;
  [%expect "length(nil, z) <= True"]

let%expect_test "Adding positive true to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom_true clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "None"]

let%expect_test "Adding positive false to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom_false clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some(length(nil, z) <= True)"]

let%expect_test "Adding positive atom to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some(length(nil, z) \\/ r_n(z) <= True)"]

let%expect_test "Adding negative true to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom_true clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "None"]

let%expect_test "Adding negative false to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom_false clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some(length(nil, z) <= True)"]

let%expect_test "Adding negative atom to disjunctive clause" =
  let augmented_clause =
    Disjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom clause
  in
  (Printing.pp_opt Disjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some(length(nil, z) <= r_n(z))"]

(* CONJUNCTIVE *)
let%expect_test "Conjunctive Clause" =
  Conjunctive_clause_patterns.pp Format.std_formatter clause;
  [%expect "{length([nil, z])}"]

let%expect_test "Adding positive true to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom_true clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some({length([nil, z])})"]

let%expect_test "Adding positive false to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom_false clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "None"]

let%expect_test "Adding positive atom to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_positive_ExtendedAtomPatterns extended_atom clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some({length([nil, z]), r_n([z])})"]

let%expect_test "Adding negative true to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom_true clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "None"]

let%expect_test "Adding negative false to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom_false clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some({length([nil, z])})"]

let%expect_test "Adding negative atom to conjunctive clause" =
  let augmented_clause =
    Conjunctive_clause_patterns.add_negative_ExtendedAtomPatterns extended_atom clause
  in
  (Printing.pp_opt Conjunctive_clause_patterns.pp) Format.std_formatter augmented_clause;
  [%expect "Some({length([nil, z]), ~r_n([z])})"]
