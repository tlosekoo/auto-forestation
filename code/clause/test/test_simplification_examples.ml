open Clause
open Clause.Aliases

let%expect_test "simplification of clauses" =
  let clauses = Database_for_tests.ground_clause_system_length2 in
  let simplified_clauses = Simplify_examples.simplify_examples clauses in
  let () =
    Format.fprintf Format.std_formatter "Clause simplification! Went from\n%a\nto\n%a\n\n"
      Set_clause_patterns.pp clauses Set_clause_patterns.pp simplified_clauses
  in
  ();
  [%expect
    "\n\
    \    Clause simplification! Went from\n\
    \    {\n\
    \    length(nil, z) <= True\n\
    \    length(nil, s(z)) <= length(cons(z, nil), s(s(z)))\n\
    \    length(cons(z, cons(z, nil)), s(s(z))) <= length(cons(z, nil), s(z))\n\
    \    False <= length(nil, s(z))\n\
    \    length(cons(s(z), nil), s(z)) <= length(nil, z)\n\
    \    length(cons(z, nil), s(z)) <= length(nil, z)\n\
    \    }\n\
    \    to\n\
    \    {\n\
    \    length(cons(s(z), nil), s(z)) <= True\n\
    \    length(cons(z, cons(z, nil)), s(s(z))) <= True\n\
    \    length(cons(z, nil), s(z)) <= True\n\
    \    length(nil, z) <= True\n\
    \    False <= length(cons(z, nil), s(s(z)))\n\
    \    False <= length(nil, s(z))\n\
    \    }"]
