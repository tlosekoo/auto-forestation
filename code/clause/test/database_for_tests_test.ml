open Clause

let%expect_test "height properties" =
  Format.fprintf Format.std_formatter
    "Height properties:\n\nBase:\n%a\n\nRecursive 1:\n%a\n\nRecursive 2:\n%a\n\nProperty:\n%a\n"
    Disjunctive_clause_patterns.pp Database_for_tests.clause_height_base
    Disjunctive_clause_patterns.pp Database_for_tests.clause_height_recursive_1
    Disjunctive_clause_patterns.pp Database_for_tests.clause_height_recursive_2
    Disjunctive_clause_patterns.pp Database_for_tests.clause_height_property;
  [%expect
    {|
    Height properties:

    Base:
    height(leaf, z) <= True

    Recursive 1:
    height(node(t1, e, t2), s(n2)) \/ le(n1, n3) <= height(t1, n1) /\ height(t1, n2) /\ height(t2, n3)

    Recursive 2:
    height(node(t1, e, t2), s(n2)) <= height(t1, n1) /\ height(t2, n2) /\ height(t2, n3) /\ le(n1, n3)

    Property:
    eq_nat(n1, n2) <= height(t1, n1) /\ height(t1, n2) |}]
