open Clause
open Term

let%expect_test "empty clause " =
  Format.fprintf Format.std_formatter "%a" Clause_patterns.pp Clause_patterns.empty_clause;
  [%expect "{}"]

let%expect_test "empty disjunctive clause" =
  Format.fprintf Format.std_formatter "%a" Disjunctive_clause_patterns.pp
    Clause_patterns.empty_clause;
  [%expect "False <= True"]

(* let dummy_datatype = Datatype.create "tip" *)
let relation_a1 = Typed_relation_symbol.create (Symbol.create "a1") []
let relation_a2 = Typed_relation_symbol.create (Symbol.create "a2") []
let relation_b = Typed_relation_symbol.create (Symbol.create "b") []
let relation_c = Typed_relation_symbol.create (Symbol.create "c") []
let relation_d = Typed_relation_symbol.create (Symbol.create "d") []
let a1 = Atom_patterns.create_from_predicate_symbol relation_a1 []
let a2 = Atom_patterns.create_from_predicate_symbol relation_a2 []
let b = Atom_patterns.create_from_predicate_symbol relation_b []
let c = Atom_patterns.create_from_predicate_symbol relation_c []
let d = Atom_patterns.create_from_predicate_symbol relation_d []
let a_not_b = Clause_patterns.from_negative_and_positive_formulas ~positive:[a1; a2] ~negative:[b]
let c_not_d = Clause_patterns.from_negative_and_positive_formulas ~positive:[c] ~negative:[d]

let%expect_test "pp a_not_b" =
  Format.fprintf Format.std_formatter "%a" Disjunctive_clause_patterns.pp a_not_b;
  [%expect "a1 \\/ a2 <= b"]

let%expect_test "pp a_not_b" =
  Format.fprintf Format.std_formatter "%a" Clause_patterns.pp a_not_b;
  [%expect "{a1, a2, ~b}"]

let%expect_test "pp c_not_d" =
  Format.fprintf Format.std_formatter "%a" Clause_patterns.pp c_not_d;
  [%expect "{c, ~d}"]

let%expect_test "pp union" =
  Format.fprintf Format.std_formatter "%a" Clause_patterns.pp
    (Clause_patterns.merge_negative_with_negative_and_positive_with_positive [a_not_b; c_not_d]);
  [%expect "{a1, a2, c, ~b, ~d}"]

(* let%expect_test "pp distribution" =
   Format.fprintf Format.std_formatter "%a"
     (Printing.pp_list_sep "\n" Clause_patterns.pp)
     (Clause_patterns.cartesian_product_formula [a_not_b; c_not_d]);
   [%expect "\n    {a2, c}\n    {a1, c}\n    {c, ~b}\n    {a2, ~d}\n    {a1, ~d}\n    {~b, ~d}"] *)

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
