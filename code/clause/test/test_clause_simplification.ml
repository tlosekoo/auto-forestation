(* open Test_clause_patterns *)
open Clause
open Term
open Term.Database_for_tests

let relation_p = Typed_relation_symbol.create (Symbol.create "p") [nat; nat]
let relation_q = Typed_relation_symbol.create (Symbol.create "q") [nat]
let relation_eq_nat = Predicate_symbol_or_equality.create_equality_from_datatype nat
let eq_sx_sy = Atom_patterns.create relation_eq_nat [p_sx; p_sy]
let eq_sx_z = Atom_patterns.create relation_eq_nat [p_sx; p_z]
let eq_sx_sx = Atom_patterns.create relation_eq_nat [p_sx; p_sx]
let p_ssx_y = Atom_patterns.create_from_predicate_symbol relation_p [p_ssx; p_y]
let q_n = Atom_patterns.create_from_predicate_symbol relation_q [p_n]
(* let a1 = Atom_patterns.create_from_predicate_symbol relation_a1 []
   let a2 = Atom_patterns.create_from_predicate_symbol relation_a2 []
   let b = Atom_patterns.create_from_predicate_symbol relation_b [] *)

(* a nice clause *)
let c1 = Disjunctive_clause_patterns.from_body_and_head ~body:[eq_sx_sy] ~head:[p_ssx_y; q_n]
let c1_unified = Disjunctive_clause_patterns.unify_clause_where_possible c1
let c1_simplified = Option.map Disjunctive_clause_patterns.simplify_trivial_equalities c1_unified

(* A clause that may be sat *)
let c2 = Disjunctive_clause_patterns.from_body_and_head ~body:[q_n] ~head:[eq_sx_z]
let c2_unified = Disjunctive_clause_patterns.unify_clause_where_possible c2
let c2_simplified = Option.map Disjunctive_clause_patterns.simplify_trivial_equalities c2_unified

(* a trivial clause *)
let c3 = Disjunctive_clause_patterns.from_body_and_head ~body:[eq_sx_z] ~head:[p_ssx_y]
let c3_unified = Disjunctive_clause_patterns.unify_clause_where_possible c3
let c3_simplified = Option.map Disjunctive_clause_patterns.simplify_trivial_equalities c3_unified

(* another trivial clause *)
let c4 = Disjunctive_clause_patterns.from_body_and_head ~body:[eq_sx_sy] ~head:[eq_sx_sx; q_n]
let c4_unified = Disjunctive_clause_patterns.unify_clause_where_possible c4
let c4_simplified = Option.map Disjunctive_clause_patterns.simplify_trivial_equalities c4_unified

(* Some pretty-printing functions *)
let pp_clause = Disjunctive_clause_patterns.pp
let pp_clause_opt = Printing.pp_opt pp_clause
let pp_clause_opt_opt = Printing.pp_opt pp_clause_opt

let%expect_test "pp c1 simplified" =
  Format.fprintf Format.std_formatter "c1:\n%a\n\nc1_unified:\n%a\n\nc1_simplified:\n%a\n\n"
    pp_clause c1 pp_clause_opt c1_unified pp_clause_opt_opt c1_simplified;
  [%expect
    "\n\
    \    c1:\n\
    \    p(s(s(x)), y) \\/ q(n) <= eq_nat(s(x), s(y))\n\n\
    \    c1_unified:\n\
    \    Some(p(s(s(y)), y) \\/ q(n) <= eq_nat(s(y), s(y)))\n\n\
    \    c1_simplified:\n\
    \    Some(Some(p(s(s(y)), y) \\/ q(n) <= True))"]

let%expect_test "pp c2 simplified" =
  Format.fprintf Format.std_formatter "c2:\n%a\n\nc2_unified:\n%a\n\nc2_simplified:\n%a\n\n"
    pp_clause c2 pp_clause_opt c2_unified pp_clause_opt_opt c2_simplified;
  [%expect
    "\n\
    \    c2:\n\
    \    eq_nat(s(x), z) <= q(n)\n\n\
    \    c2_unified:\n\
    \    Some(eq_nat(s(x), z) <= q(n))\n\n\
    \    c2_simplified:\n\
    \    Some(Some(eq_nat(s(x), z) <= q(n)))"]

let%expect_test "pp c3 simplified" =
  Format.fprintf Format.std_formatter "c3:\n%a\n\nc3_unified:\n%a\n\nc3_simplified:\n%a\n\n"
    pp_clause c3 pp_clause_opt c3_unified pp_clause_opt_opt c3_simplified;
  [%expect
    "\n\
    \    c3:\n\
    \    p(s(s(x)), y) <= eq_nat(s(x), z)\n\n\
    \    c3_unified:\n\
    \    None\n\n\
    \    c3_simplified:\n\
    \    None"]

let%expect_test "pp c4 simplified" =
  Format.fprintf Format.std_formatter "c4:\n%a\n\nc4_unified:\n%a\n\nc4_simplified:\n%a\n\n"
    pp_clause c4 pp_clause_opt c4_unified pp_clause_opt_opt c4_simplified;
  [%expect
    "\n\
    \    c4:\n\
    \    eq_nat(s(x), s(x)) \\/ q(n) <= eq_nat(s(x), s(y))\n\n\
    \    c4_unified:\n\
    \    Some(eq_nat(s(y), s(y)) \\/ q(n) <= eq_nat(s(y), s(y)))\n\n\
    \    c4_simplified:\n\
    \    Some(None)"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
