open Aliases_predicate
open Clause_patterns_core

let merge_negative_with_negative_and_positive_with_positive (fs : t list) =
  let negative = Set_atom_patterns.list_union (List.map get_negative fs) in
  let positive = Set_atom_patterns.list_union (List.map get_positive fs) in
  {negative; positive}

let cartesian_product_formula (fs : t list) : t list =
  let formula_as_list_of_atoms (formula : t) =
    let positive =
      formula |> get_positive |> Set_atom_patterns.to_list |> List.map (fun pa -> (pa, true))
    in

    let negative =
      formula |> get_negative |> Set_atom_patterns.to_list |> List.map (fun pa -> (pa, false))
    in
    List.append positive negative
  in
  let list_of_atoms_as_formula (atoms : (Atom_patterns.t * bool) list) =
    let positive, negative =
      atoms |> List.partition_map (fun (pa, b) -> if b then Left pa else Right pa)
    in
    from_negative_and_positive_formulas ~positive ~negative
  in

  let all_formulas_as_lists_of_atoms = List.map formula_as_list_of_atoms fs in
  let cartesian_product = Misc.List_op.cartesian_product_list all_formulas_as_lists_of_atoms in
  let all_formulas = List.map list_of_atoms_as_formula cartesian_product in
  all_formulas

let empty_clause : t = from_negative_and_positive_formulas ~positive:[] ~negative:[]

let apply_substitution (clause_patterns : t) (s : Term.Substitution.t) : t =
  let positive =
    Set_atom_patterns.map (Atom_patterns.apply_substitution s) clause_patterns.positive
  in
  let negative =
    Set_atom_patterns.map (Atom_patterns.apply_substitution s) clause_patterns.negative
  in
  {positive; negative}

let extract_variables (clause_patterns : t) : Term.Aliases.Set_typed_symbol.t =
  let body, head = (get_negative clause_patterns, get_positive clause_patterns) in
  let atoms = SetAtom.union body head in
  let vars =
    atoms |> SetAtom.to_list
    |> List.map Atom_patterns.get_argument
    |> List.flatten
    |> List.map Term.Pattern.get_variables
    |> Term.Aliases.Set_typed_symbol.list_union
  in
  vars

(* let extract_type_environment (clause : t) : Term.Datatype_environment.t =
   let all_atoms = SetAtom.union (get_negative clause) (get_positive clause) in
   let all_functions = all_atoms |> SetAtom.to_list |> List.map Atom_patterns.get_argument |> List.flatten |> List.map Term.Pattern. *)
