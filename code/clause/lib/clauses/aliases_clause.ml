module Set_clause_patterns = struct
  include Misc.My_set.Make2 (Clause_patterns)

  let pp_param = pp_param ~pp_element:Disjunctive_clause_patterns.pp
  let pp = pp_param ~opening:"{\n" ~closing:"\n}" ~sep:"\n"
end

module Set_clause_relation_symbol = struct
  include Misc.My_set.Make2 (Clause_typed_relation_symbol)

  let pp =
    pp_param ~opening:"{\n" ~closing:"}" ~sep:"\n" ~pp_element:Clause_typed_relation_symbol.pp
end

(* module Map_predicate_or_equality = Misc.My_map.Make (Predicate_symbol_or_equality) *)
module Map_predicate = Misc.My_map.Make (Predicate_symbol_or_equality)
module Map_clause = Misc.My_map.Make (Clause_patterns)
