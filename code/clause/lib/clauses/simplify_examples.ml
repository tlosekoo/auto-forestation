open Aliases

type positivity =
  | Positive
  | Negative

let get_atom_singleton (clause : Clause_patterns.t) =
  let negative_atoms = Clause_patterns.get_negative clause in
  let positive_atoms = Clause_patterns.get_positive clause in
  if Set_atom_patterns.cardinal negative_atoms + Set_atom_patterns.cardinal positive_atoms = 1 then
    match Set_atom_patterns.choose_opt positive_atoms with
    | Some positive_atom -> Some (Positive, positive_atom)
    | None -> Some (Negative, Set_atom_patterns.choose negative_atoms)
  else
    None

(* Returns `None` if the clause if trivially true, and else an equivalent clause (w.r.t. sets `necessarily_*`. *)
let simplify_clause_from_obvious
    (necessarily_true : Set_atom_patterns.t)
    (necessarily_false : Set_atom_patterns.t)
    (clause : Clause_patterns.t) : Clause_patterns.t option =
  let negative_atoms = Clause_patterns.get_negative clause in
  let positive_atoms = Clause_patterns.get_positive clause in
  let negative_and_false_atoms = Set_atom_patterns.inter negative_atoms necessarily_false in
  let positive_and_false_atoms = Set_atom_patterns.inter positive_atoms necessarily_false in
  let negative_and_true_atoms = Set_atom_patterns.inter negative_atoms necessarily_true in
  let positive_and_true_atoms = Set_atom_patterns.inter positive_atoms necessarily_true in
  let clause_is_not_trivially_true =
    Set_atom_patterns.is_empty positive_and_true_atoms
    && Set_atom_patterns.is_empty negative_and_false_atoms
  in
  if clause_is_not_trivially_true then
    let new_positive_atoms = Set_atom_patterns.diff positive_atoms positive_and_false_atoms in
    let new_negative_atoms = Set_atom_patterns.diff negative_atoms negative_and_true_atoms in
    let simplified_clause =
      Clause_patterns.from_negative_and_positive_formulas
        ~negative:(Set_atom_patterns.to_list new_negative_atoms)
        ~positive:(Set_atom_patterns.to_list new_positive_atoms)
    in
    Some simplified_clause
  else
    None

let rec compute_simplifiable_examples
    (clauses : Set_clause_patterns.t)
    (necessarily_true : Set_atom_patterns.t)
    (necessarily_false : Set_atom_patterns.t) =
  let simplified_clauses =
    Set_clause_patterns.filter_map
      (simplify_clause_from_obvious necessarily_true necessarily_false)
      clauses
  in
  let singleton_clauses, other_clauses =
    Set_clause_patterns.partition
      (fun clause -> Option.is_some (get_atom_singleton clause))
      simplified_clauses
  in
  if Set_clause_patterns.is_empty singleton_clauses then
    (necessarily_true, necessarily_false, other_clauses)
  else
    let singleton_atoms =
      singleton_clauses |> Set_clause_patterns.to_list |> List.filter_map get_atom_singleton
    in
    let updated_necessarily_true, updated_necessarily_false =
      List.fold_left
        (fun (true_atoms, false_atoms) single_atom ->
          match single_atom with
          | Positive, positive_atom -> (Set_atom_patterns.add positive_atom true_atoms, false_atoms)
          | Negative, negative_atom -> (true_atoms, Set_atom_patterns.add negative_atom false_atoms))
        (necessarily_true, necessarily_false)
        singleton_atoms
    in
    compute_simplifiable_examples other_clauses updated_necessarily_true updated_necessarily_false

let simplify_examples (clauses : Set_clause_patterns.t) : Set_clause_patterns.t =
  let necessarily_true, necessarily_false, other_clauses =
    compute_simplifiable_examples clauses Set_atom_patterns.empty Set_atom_patterns.empty
  in
  let clauses_for_true_atoms =
    necessarily_true |> Set_atom_patterns.to_list
    |> List.map (fun atom ->
           Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom])
    |> Set_clause_patterns.of_list
  in
  let clauses_for_false_atoms =
    necessarily_false |> Set_atom_patterns.to_list
    |> List.map (fun atom ->
           Clause_patterns.from_negative_and_positive_formulas ~negative:[atom] ~positive:[])
    |> Set_clause_patterns.of_list
  in
  Set_clause_patterns.list_union [other_clauses; clauses_for_true_atoms; clauses_for_false_atoms]
