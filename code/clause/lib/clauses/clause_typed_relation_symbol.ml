include Clause_core.Make (Term.Typed_relation_symbol)
open Aliases_predicate

let pp (c : Format.formatter) (k : t) : unit =
  let pp_set_head_predicate_applications_as_atoms =
    Set_atom_of_typed_relation_symbol.pp_param ~opening:"" ~closing:"" ~sep:" \\/ "
      ~pp_element:Atom_relationSymbol.pp
  in
  let pp_set_body_predicate_applications_as_atoms =
    Set_atom_of_typed_relation_symbol.pp_param ~opening:"" ~closing:"" ~sep:" /\\ "
      ~pp_element:Atom_relationSymbol.pp
  in
  Format.fprintf c "%a <= %a" pp_set_head_predicate_applications_as_atoms k.positive
    pp_set_body_predicate_applications_as_atoms k.negative
