open Aliases_predicate
open Term
open Clause_patterns

let from_body_and_head ~(body : Atom_patterns.t list) ~(head : Atom_patterns.t list) =
  from_negative_and_positive_formulas ~negative:body ~positive:head

let disjunction = merge_negative_with_negative_and_positive_with_positive
let get_head = get_positive
let get_body = get_negative
let false_clause = empty_clause

let pp (c : Format.formatter) (k : t) : unit =
  let pp_set_head_predicate_applications_as_atoms =
    Set_atom_patterns.pp_param ~opening:"" ~closing:"" ~sep:" \\/ " ~pp_element:Atom_patterns.pp
  in
  let pp_set_body_predicate_applications_as_atoms =
    Set_atom_patterns.pp_param ~opening:"" ~closing:"" ~sep:" /\\ " ~pp_element:Atom_patterns.pp
  in
  let bottom_if_no_head =
    if Set_atom_patterns.is_empty k.positive then
      "False"
    else
      ""
  in
  let top_if_no_body =
    if Set_atom_patterns.is_empty k.negative then
      "True"
    else
      ""
  in
  Format.fprintf c "%s%a <= %a%s" bottom_if_no_head pp_set_head_predicate_applications_as_atoms
    k.positive pp_set_body_predicate_applications_as_atoms k.negative top_if_no_body

let rec convert_equality_list_into_list_of_equality_pairs (equality_assertion : Pattern.t list) =
  match equality_assertion with
  | [] -> []
  | [_p] -> []
  | p1 :: p2 :: l -> (p1, p2) :: convert_equality_list_into_list_of_equality_pairs (p2 :: l)

(* Return None if the clause if trivially true *)
let unify_clause_where_possible (clause : t) : t option =
  let body_equality_assertions =
    clause |> get_body |> Set_atom_patterns.to_list
    |> List.filter Atom_patterns.is_equality_predicate
    |> List.map Atom_patterns.get_argument
  in
  let unification_problem =
    Misc.List_op.list_union
      (Misc.Pair.compare Pattern.compare Pattern.compare)
      (List.map convert_equality_list_into_list_of_equality_pairs body_equality_assertions)
  in
  let solution_opt = Unification.solve (Unification.of_list unification_problem) in
  Option.map (apply_substitution clause) solution_opt

(* Returns None if the clause is trivially true *)
let simplify_trivial_equalities (clause : t) : t option =
  let body =
    clause |> get_body |> Set_atom_patterns.to_list
    |> List.map Extended_atom_patterns.simplify_trivial_equalities
  in
  let head =
    clause |> get_head |> Set_atom_patterns.to_list
    |> List.map Extended_atom_patterns.simplify_trivial_equalities
  in
  let clause_is_trivially_true =
    List.exists Extended_atom_patterns.is_false body
    || List.exists Extended_atom_patterns.is_true head
  in

  match clause_is_trivially_true with
  | true -> None
  | false ->
      let simplified_body = List.filter_map Extended_atom_patterns.to_atom_opt body in
      let simplified_head = List.filter_map Extended_atom_patterns.to_atom_opt head in
      Some (from_body_and_head ~body:simplified_body ~head:simplified_head)

let simplify_clause_by_unification (clause : t) : t option =
  Option.bind (unify_clause_where_possible clause) simplify_trivial_equalities

(* Returns None if the clause becomes trivially true *)
let add_positive_ExtendedAtomPatterns
    (extended_atom : Extended_atom_patterns.t)
    (clause : Clause_patterns.t) : Clause_patterns.t option =
  match extended_atom with
  | Extended_atom_patterns.True -> None
  | Extended_atom_patterns.False -> Some clause
  | Extended_atom_patterns.Atom atom -> Some (Clause_patterns.add_positive atom clause)

(* Returns None if the clause becomes trivially true *)
let add_negative_ExtendedAtomPatterns
    (extended_atom : Extended_atom_patterns.t)
    (clause : Clause_patterns.t) : Clause_patterns.t option =
  match extended_atom with
  | Extended_atom_patterns.False -> Some clause
  | Extended_atom_patterns.True -> None
  | Extended_atom_patterns.Atom atom -> Some (Clause_patterns.add_negative atom clause)
