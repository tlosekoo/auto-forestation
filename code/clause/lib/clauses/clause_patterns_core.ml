include Clause_core.Make (Term.Patterns)
(* open Aliases_predicate

   module Clause = struct
     type t = {
       negative : Set_atom_patterns.t;
       positive : Set_atom_patterns.t;
     }
     [@@deriving compare, equal]

     let pp (c : Format.formatter) (k : t) : unit =
       let pp_set_predicate_applications_as_atoms =
         Set_atom_patterns.pp_param ~opening:"" ~closing:"" ~sep:", "
       in
       let pp_set_positive_predicate_applications_as_atoms =
         pp_set_predicate_applications_as_atoms ~pp_element:Atom_patterns.pp
       in
       let pp_set_negative_predicate_applications_as_atoms =
         pp_set_predicate_applications_as_atoms ~pp_element:(fun c ->
             Format.fprintf c "~%a" Atom_patterns.pp)
       in
       let mid_separation =
         if Set_atom_patterns.is_empty k.positive || Set_atom_patterns.is_empty k.negative then
           ""
         else
           ", "
       in
       Format.fprintf c "{%a%s%a}" pp_set_positive_predicate_applications_as_atoms k.positive
         mid_separation pp_set_negative_predicate_applications_as_atoms k.negative

     let from_negative_and_positive_formulas
         ~(negative : Atom_patterns.t list)
         ~(positive : Atom_patterns.t list) =
       let negative = Set_atom_patterns.of_list negative in
       let positive = Set_atom_patterns.of_list positive in
       {negative; positive}

     let get_positive (formula : t) = formula.positive
     let get_negative (formula : t) = formula.negative

     let add_negative (atom : Atom_patterns.t) (f : t) =
       {negative = Set_atom_patterns.add atom f.negative; positive = f.positive}

     let add_positive (atom : Atom_patterns.t) (f : t) =
       {negative = f.negative; positive = Set_atom_patterns.add atom f.positive}
   end

   include Clause *)
