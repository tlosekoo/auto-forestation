type t = Clause_patterns.t

let conjunction = Clause_patterns.merge_negative_with_negative_and_positive_with_positive
let true_clause = Clause_patterns.empty_clause
let pp = Clause_patterns.pp

(* Returns None if the clause becomes trivially false *)
let add_positive_ExtendedAtomPatterns
    (extended_atom : Extended_atom_patterns.t)
    (clause : Clause_patterns.t) : Clause_patterns.t option =
  match extended_atom with
  | Extended_atom_patterns.True -> Some clause
  | Extended_atom_patterns.False -> None
  | Extended_atom_patterns.Atom atom -> Some (Clause_patterns.add_positive atom clause)

(* Returns None if the clause becomes trivially false *)
let add_negative_ExtendedAtomPatterns
    (extended_atom : Extended_atom_patterns.t)
    (clause : Clause_patterns.t) : Clause_patterns.t option =
  match extended_atom with
  | Extended_atom_patterns.False -> Some clause
  | Extended_atom_patterns.True -> None
  | Extended_atom_patterns.Atom atom -> Some (Clause_patterns.add_negative atom clause)

(* Returns None if the clause is trivially false *)
let from_negative_extendedAtomPatterns (atoms : Extended_atom_patterns.t list) :
    Clause_patterns.t option =
  List.fold_left
    (fun partial_clause_opt atom ->
      Option.bind partial_clause_opt (add_negative_ExtendedAtomPatterns atom))
    (Some Clause_patterns.empty_clause) atoms
