module Make (Parameter : Misc.Ordered_and_printable.OP) = struct
  module Atomm = Atom.Make (Parameter)
  module SetAtom = Misc.My_set.Make2 (Atom.Make (Parameter))

  type t = {
    negative : SetAtom.t;
    positive : SetAtom.t;
  }
  [@@deriving compare, equal]

  let pp (c : Format.formatter) (k : t) : unit =
    let pp_set_atom = SetAtom.pp_param ~opening:"" ~closing:"" ~sep:", " in
    let pp_set_positive_atoms = pp_set_atom ~pp_element:Atomm.pp in
    let pp_set_negative_atoms =
      pp_set_atom ~pp_element:(fun c -> Format.fprintf c "~%a" Atomm.pp)
    in
    let mid_separation =
      if SetAtom.is_empty k.positive || SetAtom.is_empty k.negative then
        ""
      else
        ", "
    in
    Format.fprintf c "{%a%s%a}" pp_set_positive_atoms k.positive mid_separation
      pp_set_negative_atoms k.negative

  let from_negative_and_positive_formulas ~(negative : Atomm.t list) ~(positive : Atomm.t list) =
    let negative = SetAtom.of_list negative in
    let positive = SetAtom.of_list positive in
    {negative; positive}

  let get_positive (formula : t) = formula.positive
  let get_negative (formula : t) = formula.negative

  let add_negative (atom : Atomm.t) (f : t) =
    {negative = SetAtom.add atom f.negative; positive = f.positive}

  let add_positive (atom : Atomm.t) (f : t) =
    {negative = f.negative; positive = SetAtom.add atom f.positive}

  (* let extract_predicates (clause : t) : Term.Aliases.Set_typed_relation_symbol.t =
     let negative_predicates = clause |> get_negative |> SetAtom.to_list |> List.map Atomm.get_predicate in
     let positive_predicates = clause |> get_negative |> SetAtom.to_list |> List.map Atomm.get_predicate in
     Term.Aliases.Set_typed_relation_symbol.of_list (List.append negative_predicates positive_predicates) in *)

  let extract_non_equality_predicates (clause : t) : Term.Aliases.Set_typed_relation_symbol.t =
    let negative_predicates =
      clause |> get_negative |> SetAtom.to_list |> List.map Atomm.get_predicate
      |> List.filter_map Predicate_symbol_or_equality.get_typed_relation_symbol_opt
    in
    let positive_predicates =
      clause |> get_positive |> SetAtom.to_list |> List.map Atomm.get_predicate
      |> List.filter_map Predicate_symbol_or_equality.get_typed_relation_symbol_opt
    in
    Term.Aliases.Set_typed_relation_symbol.of_list
      (List.append negative_predicates positive_predicates)

  let swap_positives_and_negatives (clause : t) : t =
    {negative = clause.positive; positive = clause.negative}
end
