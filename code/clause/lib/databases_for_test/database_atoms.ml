open Term.Database_for_tests

(* Some atoms *)
let atom_rl_nil = Atom_patterns.create_from_predicate_symbol relation_l [p_nil]
let atom_rln_nil_z = Atom_patterns.create_from_predicate_symbol relation_l_n [p_nil; p_z]
let atom_rn_sz = Atom_patterns.create_from_predicate_symbol relation_n [p_sz]
let atom_rn_z = Atom_patterns.create_from_predicate_symbol relation_n [p_z]
let atom_rc_a = Atom_patterns.create_from_predicate_symbol relation_c [p_ca]
let atom_rc_ffa = Atom_patterns.create_from_predicate_symbol relation_c [p_cffa]
let atom_rc_fffa = Atom_patterns.create_from_predicate_symbol relation_c [p_cfffa]
let atom_rc_ffffa = Atom_patterns.create_from_predicate_symbol relation_c [p_cffffa]

let atom_rc_ffffffffffffffa =
  Atom_patterns.create_from_predicate_symbol relation_c [p_cffffffffffffffa]

let atom_rc_fa = Atom_patterns.create_from_predicate_symbol relation_c [p_cfa]
let atom_rc_fb = Atom_patterns.create_from_predicate_symbol relation_c [p_cfb]
let atom_r2c_a = Atom_patterns.create_from_predicate_symbol relation2_c [p_ca]
let atom_r2c_fa = Atom_patterns.create_from_predicate_symbol relation2_c [p_cfa]
let atom_r2c_ffa = Atom_patterns.create_from_predicate_symbol relation2_c [p_cffa]

(* Example from learner/teacher *)
let atom_rfcons_sz_nil =
  Atom_patterns.create_from_predicate_symbol relation_fcons [p_sz; p_nil; p_conssznil]

let atom_rfcons_z_consznil =
  Atom_patterns.create_from_predicate_symbol relation_fcons [p_z; p_consznil; p_conszconsznil]

let atom_rfcons_z_nil =
  Atom_patterns.create_from_predicate_symbol relation_fcons [p_z; p_nil; p_consznil]

let atom_le_z_sz = Atom_patterns.create_from_predicate_symbol relation_le [p_z; p_sz]

(* Shallower *)
let atom_shallower_leaf_z =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_leaf; p_z]

let atom_shallower_leaf_sz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_leaf; p_sz]

let atom_shallower_nodeleafaleaf_z =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_nodeleafaleaf; p_z]

let atom_shallower_nodeleafaleaf_sz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_nodeleafaleaf; p_sz]

let atom_shallower_nodeleafaleaf_ssz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_nodeleafaleaf; p_ssz]

let atom_shallower_nodenodeleafaleafaleaf_ssz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_node_nodeleafaleaf_a_leaf; p_ssz]

let atom_shallower_nodeleafanodeleafaleaf_ssz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_node_leaf_a_nodeleafaleaf; p_ssz]

let atom_shallower_nodenodeleafaleafaleaf_sz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_node_nodeleafaleaf_a_leaf; p_sz]

let atom_shallower_nodeleafanodeleafaleaf_sz =
  Atom_patterns.create_from_predicate_symbol relation_shallower [p_node_leaf_a_nodeleafaleaf; p_sz]

(* length *)
let atom_length_nil_z = Atom_patterns.create_from_predicate_symbol relation_length [p_nil; p_z]

let atom_length_consznil_sz =
  Atom_patterns.create_from_predicate_symbol relation_length [p_consznil; p_sz]

let atom_length_consznil_ssz =
  Atom_patterns.create_from_predicate_symbol relation_length [p_consznil; p_ssz]

let atom_length_consznil_z =
  Atom_patterns.create_from_predicate_symbol relation_length [p_consznil; p_z]

let atom_length_nil_sz = Atom_patterns.create_from_predicate_symbol relation_length [p_nil; p_sz]

let atom_length_conssznil_sz =
  Atom_patterns.create_from_predicate_symbol relation_length [p_conssznil; p_sz]

let atom_length_conszconsznil_ssz =
  Atom_patterns.create_from_predicate_symbol relation_length [p_conszconsznil; p_ssz]

let atom_plus_z_z_z = Atom_patterns.create_from_predicate_symbol relation_plus [p_z; p_z; p_z]
let atom_plus_z_sz_sz = Atom_patterns.create_from_predicate_symbol relation_plus [p_z; p_sz; p_sz]
let atom_plus_z_ssz_sz = Atom_patterns.create_from_predicate_symbol relation_plus [p_z; p_ssz; p_sz]
