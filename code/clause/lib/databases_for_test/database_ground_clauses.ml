open Aliases
open Database_atoms

(* Some clauses *)
let clause_rl_nil =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rl_nil]

let clause_rln_nil_z =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rln_nil_z]

let clause_rn_sz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rn_sz]

let clause_not_rn_z =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rn_z] ~positive:[]

let clause_rc_a =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rc_a]

let clause_rc_fa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rc_fa]

let clause_rc_ffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rc_ffa]

let clause_rc_ffffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rc_ffffa]

let clause_rc_ffffffffffffffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[]
    ~positive:[atom_rc_ffffffffffffffa]

let clause_not_rc_fa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rc_fa] ~positive:[]

let clause_not_rc_fffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rc_fffa] ~positive:[]

let clause_rc_fb =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rc_fb]

let clause_rc_a_implies_rc_ffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rc_a] ~positive:[atom_rc_ffa]

let clause_rc_ffa_implies_rc_ffffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rc_ffa]
    ~positive:[atom_rc_ffffa]

let clause_not_rc_fa_and_rc_a =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_rc_fa; atom_rc_a] ~positive:[]

let clause_length_nil_z =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_length_nil_z]

let clause_length_nil_z_implies_length_consznil_sz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_length_nil_z]
    ~positive:[atom_length_consznil_sz]

let clause_length_nil_z_implies_length_conssznil_sz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_length_nil_z]
    ~positive:[atom_length_conssznil_sz]

let clause_length_consznil_sz_implies_length_conszconsznil_ssz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_length_consznil_sz]
    ~positive:[atom_length_conszconsznil_ssz]

let clause_length_consznil_ssz_implies_length_nil_sz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_length_consznil_ssz]
    ~positive:[atom_length_nil_sz]

let clause_length_nil_sz_implies_false =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_length_nil_sz] ~positive:[]

let clause_shallower_base =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_shallower_leaf_z]

let clause_shallower_base2 =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[]
    ~positive:[atom_shallower_leaf_sz]

let clause_shallower_impl_positive =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_shallower_leaf_z]
    ~positive:[atom_shallower_nodeleafaleaf_sz]

let clause_shallower_impl_positive2 =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_shallower_leaf_sz]
    ~positive:[atom_shallower_nodeleafaleaf_ssz]

let clause_shallower_impl_positive3 =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_shallower_nodeleafaleaf_sz]
    ~positive:[atom_shallower_nodenodeleafaleafaleaf_ssz]

let clause_shallower_impl_positive3bis =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_shallower_nodeleafaleaf_sz]
    ~positive:[atom_shallower_nodeleafanodeleafaleaf_ssz]

let clause_shallower_impl_negative =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_shallower_nodeleafaleaf_z]
    ~positive:[]

let clause_shallower_impl_negative2 =
  Clause_patterns.from_negative_and_positive_formulas
    ~negative:[atom_shallower_nodenodeleafaleafaleaf_sz]
    ~positive:[atom_shallower_nodeleafaleaf_z]

let clause_shallower_impl_negative2bis =
  Clause_patterns.from_negative_and_positive_formulas
    ~negative:[atom_shallower_nodeleafanodeleafaleaf_sz]
    ~positive:[atom_shallower_nodeleafaleaf_z]

let clause_rc2_a =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_r2c_a]

let clause_rc2_fa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_r2c_fa]

let clause_not_rc2_ffa =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_r2c_ffa] ~positive:[]

let clause_fcons_sz_nil =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rfcons_sz_nil]

let clause_fcons_z_nil =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_rfcons_z_nil]

let clause_fcons_z_consznil =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[]
    ~positive:[atom_rfcons_z_consznil]

let clause_fcons_length_length_implies_le =
  Clause_patterns.from_negative_and_positive_formulas
    ~negative:[atom_rfcons_z_nil; atom_length_consznil_sz; atom_length_nil_z]
    ~positive:[atom_le_z_sz]

(* clauses bug *)
let clause_plus_zzz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_plus_z_z_z]

let clause_plus_zszsz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[] ~positive:[atom_plus_z_sz_sz]

let clause_plus_zzz_implies_plus_zszsz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_plus_z_z_z]
    ~positive:[atom_plus_z_sz_sz]

let clause_not_plus_zsszsz =
  Clause_patterns.from_negative_and_positive_formulas ~negative:[atom_plus_z_ssz_sz] ~positive:[]

(* Clause system lvl 0 *)
let ground_clause_system_1 = Set_clause_patterns.empty
let ground_clause_system_2 = Set_clause_patterns.singleton clause_rl_nil
let ground_clause_system_3 = Set_clause_patterns.singleton clause_rln_nil_z
let ground_clause_system_4 = Set_clause_patterns.of_list [clause_rn_sz]
let ground_clause_system_5 = Set_clause_patterns.of_list [clause_not_rn_z]

(* Clauses lvl 3 *)
let ground_clause_system_11 = Set_clause_patterns.of_list [clause_rc_ffa]
let ground_clause_system_12 = Set_clause_patterns.of_list [clause_not_rc_fa; clause_rc_fb]
let ground_clause_system_13 = Set_clause_patterns.of_list [clause_not_rc_fa; clause_rc_ffa]

let ground_clause_system_even_f =
  Set_clause_patterns.of_list
    [clause_rc_a; clause_rc_ffa; clause_rc_ffffa; clause_not_rc_fa; clause_not_rc_fffa]

let ground_clause_system_even_f2 =
  Set_clause_patterns.of_list
    [
      clause_rc_a;
      clause_rc_a_implies_rc_ffa;
      (* clause_re_ffa_implies_re_ffffa; *)
      clause_not_rc_fa_and_rc_a;
    ]

let ground_clause_system_thomas =
  Set_clause_patterns.of_list
    [clause_rc_a; clause_rc_fa; clause_rc2_a; clause_rc2_fa; clause_not_rc2_ffa]

let ground_clause_system_even_f3 =
  Set_clause_patterns.of_list
    [
      clause_rc_a;
      clause_rc_ffa;
      clause_rc_ffffa;
      clause_not_rc_fa;
      clause_not_rc_fffa;
      clause_rc_ffffffffffffffa;
    ]

let ground_clause_system_length =
  Set_clause_patterns.of_list
    [
      clause_length_nil_z;
      clause_length_nil_z_implies_length_consznil_sz;
      clause_length_nil_z_implies_length_conssznil_sz;
      clause_length_consznil_ssz_implies_length_nil_sz;
      clause_length_nil_sz_implies_false;
    ]

let ground_clause_system_length2 =
  Set_clause_patterns.of_list
    [
      clause_length_nil_z;
      clause_length_nil_z_implies_length_consznil_sz;
      clause_length_nil_z_implies_length_conssznil_sz;
      clause_length_consznil_ssz_implies_length_nil_sz;
      clause_length_nil_sz_implies_false;
      clause_length_consznil_sz_implies_length_conszconsznil_ssz;
    ]

let ground_clause_system_shallower =
  Set_clause_patterns.of_list
    [
      clause_shallower_base;
      clause_shallower_base2;
      clause_shallower_impl_positive;
      clause_shallower_impl_positive2;
      clause_shallower_impl_positive3;
      clause_shallower_impl_negative;
    ]

let ground_clause_system_shallower2 =
  Set_clause_patterns.of_list
    [
      clause_shallower_base;
      clause_shallower_base2;
      clause_shallower_impl_positive;
      clause_shallower_impl_positive2;
      clause_shallower_impl_positive3;
      clause_shallower_impl_negative;
      clause_shallower_impl_negative2;
    ]

let ground_clause_system_shallower3 =
  Set_clause_patterns.of_list
    [
      clause_shallower_base;
      clause_shallower_base2;
      clause_shallower_impl_positive;
      clause_shallower_impl_positive2;
      clause_shallower_impl_positive3;
      clause_shallower_impl_positive3bis;
      clause_shallower_impl_negative;
      clause_shallower_impl_negative2;
      clause_shallower_impl_negative2bis;
    ]

let ground_clause_system_length_cons_le_fun_nat =
  Set_clause_patterns.of_list
    [
      clause_fcons_z_consznil;
      clause_fcons_sz_nil;
      clause_fcons_z_nil;
      clause_length_nil_z;
      clause_fcons_length_length_implies_le;
      clause_length_nil_z_implies_length_consznil_sz;
    ]

let ground_clause_system_bug =
  Set_clause_patterns.of_list [clause_plus_zzz; clause_plus_zszsz; clause_not_plus_zsszsz]
