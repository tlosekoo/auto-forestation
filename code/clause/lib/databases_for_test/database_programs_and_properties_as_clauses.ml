open Term.Database_for_tests

(* *)
(* *)
(* *)
(* length *)
let atom_length_l_n =
  Atom_patterns.create_from_predicate_symbol relation_length
    [Term.Database_for_tests.p_l; Term.Database_for_tests.p_n]

let atom_length_l_n1 =
  Atom_patterns.create_from_predicate_symbol relation_length
    [Term.Database_for_tests.p_l; Term.Database_for_tests.p_n1]

let atom_length_l_n2 =
  Atom_patterns.create_from_predicate_symbol relation_length
    [Term.Database_for_tests.p_l; Term.Database_for_tests.p_n2]

let atom_length_consxl_sn =
  Atom_patterns.create_from_predicate_symbol relation_length
    [Term.Database_for_tests.p_consxl; Term.Database_for_tests.p_sn]

let atom_length_nil_z =
  Atom_patterns.create_from_predicate_symbol relation_length
    [Term.Database_for_tests.p_nil; Term.Database_for_tests.p_z]

let equality_nat_predicate_symbol =
  Predicate_symbol_or_equality.create_equality_from_datatype Term.Database_for_tests.nat

let eq_n1_n2 =
  Atom_patterns.create equality_nat_predicate_symbol
    [Term.Database_for_tests.p_n1; Term.Database_for_tests.p_n2]

let chc_length_nil_z =
  let body = [] in
  let head = [atom_length_nil_z] in
  Disjunctive_clause_patterns.from_body_and_head ~body ~head

let chc_length_l_n_implies_length_consxl_sn =
  let body = [atom_length_l_n] in
  let head = [atom_length_consxl_sn] in
  Disjunctive_clause_patterns.from_body_and_head ~body ~head

let chc_length_consxl_s_implies_length_l_n =
  let body = [atom_length_consxl_sn] in
  let head = [atom_length_l_n] in
  Disjunctive_clause_patterns.from_body_and_head ~body ~head

let chc_length_l_n1_and_length_l_n2_implies_eq_n1_n2 =
  let body = [atom_length_l_n1; atom_length_l_n2] in
  let head = [eq_n1_n2] in
  Disjunctive_clause_patterns.from_body_and_head ~body ~head

let chc_system_for_length =
  Aliases.Set_clause_patterns.of_list
    [
      chc_length_nil_z;
      chc_length_l_n_implies_length_consxl_sn;
      chc_length_consxl_s_implies_length_l_n;
      chc_length_l_n1_and_length_l_n2_implies_eq_n1_n2;
    ]

(* *)
(* *)
(* *)
(* height *)

let atom_height_leaf_z = Atom_patterns.create_from_predicate_symbol relation_height [p_leaf; p_z]
let atom_height_t1_n1 = Atom_patterns.create_from_predicate_symbol relation_height [p_t1; p_n1]
let atom_height_t1_n2 = Atom_patterns.create_from_predicate_symbol relation_height [p_t1; p_n2]
let atom_height_t2_n2 = Atom_patterns.create_from_predicate_symbol relation_height [p_t2; p_n2]
let atom_height_t2_n3 = Atom_patterns.create_from_predicate_symbol relation_height [p_t2; p_n3]
let atom_le_n1_n3 = Atom_patterns.create_from_predicate_symbol relation_le [p_n1; p_n3]

let atom_eq_t1_t2 =
  Atom_patterns.create
    (Predicate_symbol_or_equality.create_equality_from_datatype elt_tree)
    [p_t1; p_t2]

let atom_eq_n1_n2 =
  Atom_patterns.create (Predicate_symbol_or_equality.create_equality_from_datatype nat) [p_n1; p_n2]

let atom_height__node_t1_e_t2__s_n2 =
  Atom_patterns.create_from_predicate_symbol relation_height [p_node_t1_e_t2; p_sn2]

let clause_height_base =
  Clause_patterns.from_negative_and_positive_formulas ~positive:[atom_height_leaf_z] ~negative:[]

let clause_height_recursive_1 =
  Clause_patterns.from_negative_and_positive_formulas
    ~positive:[atom_height__node_t1_e_t2__s_n2; atom_le_n1_n3]
    ~negative:[atom_height_t1_n1; atom_height_t1_n2; atom_height_t2_n3]

let clause_height_recursive_2 =
  Clause_patterns.from_negative_and_positive_formulas ~positive:[atom_height__node_t1_e_t2__s_n2]
    ~negative:[atom_height_t1_n1; atom_height_t2_n2; atom_height_t2_n3; atom_le_n1_n3]

let clause_height_property =
  Clause_patterns.from_negative_and_positive_formulas ~positive:[atom_eq_n1_n2]
    ~negative:[atom_height_t1_n1; atom_height_t1_n2]

(* height(leaf, z) <= True
   le_nat(z, s(nn2)) <= True
   height(node(e, t1, t2), s(_g)) \/ le_nat(_e, _f) <= height(t1, _e) /\ height(t1, _g) /\ height(t2, _f)
   height(node(e, t1, t2), s(_h)) <= height(t1, _i) /\ height(t2, _h) /\ height(t2, _j) /\ le_nat(_i, _j)
   eq_nat(_n, _o) <= height(t2, _n) /\ height(t2, _o)
   le_nat(s(nn1), s(nn2)) <= le_nat(nn1, nn2)
   le_nat(nn1, nn2) <= le_nat(s(nn1), s(nn2))
   False <= le_nat(s(nn1), z)
   False <= le_nat(z, z) *)
