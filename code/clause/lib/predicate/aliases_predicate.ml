open Term

(* module Atom_of_typed_relation_symbol = struct
     include Atom.Make (Term.Typed_relation_symbol)

     let pp =
       pp_paramm ~pp_predicate_symbol:Predicate_symbol_or_equality.pp
         ~pp_parameter:Term.Typed_relation_symbol.pp
   end *)

module Set_atom_patterns = Misc.My_set.Make2 (Atom.Make (Patterns))
module Set_atom_of_typed_relation_symbol = Misc.My_set.Make2 (Atom.Make (Typed_relation_symbol))
module Set_predicate_symbol_or_equality = Misc.My_set.Make2 (Predicate_symbol_or_equality)
