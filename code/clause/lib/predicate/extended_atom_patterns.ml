open Term

type t =
  | False
  | True
  | Atom of Atom_patterns.t
[@@deriving equal, compare]

let from_bool (b : bool) =
  match b with
  | true -> True
  | false -> False

let from_atom (a : Atom_patterns.t) = Atom a
let is_false = ( = ) False
let is_true = ( = ) True

let pp (c : Format.formatter) (bool_or_atom_pattern : t) : unit =
  match bool_or_atom_pattern with
  | False -> Format.fprintf c "False"
  | True -> Format.fprintf c "True"
  | Atom a -> Format.fprintf c "%a" Atom_patterns.pp a

(* let is_atom (ea : t) = not (is_false ea || is_true ea) *)
let to_atom_opt (ea : t) : Atom_patterns.t option =
  match ea with
  | False
  | True ->
      None
  | Atom a -> Some a

let simplify_trivial_equalities (l : Atom_patterns.t) : t =
  let predicate = Atom_patterns.get_predicate l in
  let witness = Atom_patterns.get_argument l in
  if Predicate_symbol_or_equality.is_equality_predicate predicate then
    let patterns_are_closed = List.for_all Pattern.is_closed witness in
    let terms_are_equal = Misc.List_op.all_equal Pattern.equal witness in
    if patterns_are_closed then (* watch out for parity *)
      from_bool terms_are_equal
    else if terms_are_equal then
      True
    else
      from_atom l
  else
    from_atom l
