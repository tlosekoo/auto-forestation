open Term

module type S = sig
  module Parameter : Misc.Ordered_and_printable.OP

  type t = {
    predicate : Predicate_symbol_or_equality.t;
    argument : Parameter.t;
  }

  include Misc.Ordered_and_printable.OP with type t := t

  val create_from_predicate_symbol : Typed_relation_symbol.t -> Parameter.t -> t
  val create : Predicate_symbol_or_equality.t -> Parameter.t -> t
  val get_predicate : t -> Predicate_symbol_or_equality.t
  val get_argument : t -> Parameter.t
  val is_equality_predicate : t -> bool
  val self_map : (Parameter.t -> Parameter.t) -> t -> t
  val self_mapi : (Predicate_symbol_or_equality.t -> Parameter.t -> Parameter.t) -> t -> t

  val pp_paramm :
    pp_predicate_symbol:(Format.formatter -> Predicate_symbol_or_equality.t -> unit) ->
    pp_parameter:(Format.formatter -> Parameter.t -> unit) ->
    Format.formatter ->
    t ->
    unit

  val pp : Format.formatter -> t -> unit
end

module Make (Parameter : Misc.Ordered_and_printable.OP) : S with module Parameter := Parameter =
struct
  (* module Parameter given as input of this functor is assigned to module Parameter of signature *)
  (* module Parameter = Parameter *)

  type t = {
    predicate : Predicate_symbol_or_equality.t;
    argument : Parameter.t;
  }
  [@@deriving compare, equal]

  let create_from_predicate_symbol
      (predicate_symbol : Typed_relation_symbol.t)
      (argument : Parameter.t) : t =
    let predicate = Predicate_symbol_or_equality.create_from_predicate_symbol predicate_symbol in
    {predicate; argument}

  let create (predicate : Predicate_symbol_or_equality.t) (argument : Parameter.t) : t =
    {predicate; argument}

  let get_predicate (p_app : t) = p_app.predicate
  let get_argument (p_app : t) : Parameter.t = p_app.argument

  let is_equality_predicate (p_app : t) : bool =
    Predicate_symbol_or_equality.is_equality_predicate p_app.predicate

  let pp_paramm
      ~(pp_predicate_symbol : Format.formatter -> Predicate_symbol_or_equality.t -> unit)
      ~(pp_parameter : Format.formatter -> Parameter.t -> unit)
      (c : Format.formatter)
      (pred_app : t) : unit =
    let ps = get_predicate pred_app in
    let param = get_argument pred_app in
    let parameter_should_not_be_printed =
      ps |> Predicate_symbol_or_equality.get_datatype |> Datatypes.equal []
    in
    if parameter_should_not_be_printed then
      Format.fprintf c "%a" pp_predicate_symbol ps
    else
      Format.fprintf c "%a(%a)" pp_predicate_symbol ps pp_parameter param

  let pp : Format.formatter -> t -> unit =
    pp_paramm ~pp_predicate_symbol:Predicate_symbol_or_equality.pp ~pp_parameter:Parameter.pp

  let self_mapi (f : Predicate_symbol_or_equality.t -> Parameter.t -> Parameter.t) (p_app : t) : t =
    let new_argument = f p_app.predicate p_app.argument in
    create p_app.predicate new_argument

  let self_map (f : Parameter.t -> Parameter.t) : t -> t = self_mapi (fun _ -> f)
end

module type SMap = sig
  module Parameter1 : Misc.Ordered_and_printable.OP
  module Parameter2 : Misc.Ordered_and_printable.OP
  module PA1 : S with module Parameter := Parameter1
  module PA2 : S with module Parameter := Parameter2

  val mapi : (Predicate_symbol_or_equality.t -> Parameter1.t -> Parameter2.t) -> PA1.t -> PA2.t
  val map : (Parameter1.t -> Parameter2.t) -> PA1.t -> PA2.t
end

module MakeMap
    (Parameter1 : Misc.Ordered_and_printable.OP)
    (Parameter2 : Misc.Ordered_and_printable.OP) =
struct
  module PA1 = Make (Parameter1)
  module PA2 = Make (Parameter2)

  let mapi (f : Predicate_symbol_or_equality.t -> Parameter1.t -> Parameter2.t) (p_app : PA1.t) :
      PA2.t =
    let new_argument = f p_app.predicate p_app.argument in
    PA2.create p_app.predicate new_argument

  let map (f : Parameter1.t -> Parameter2.t) : PA1.t -> PA2.t = mapi (fun _ -> f)
end
