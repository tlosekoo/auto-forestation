(** Allows to generate relations *)
open Term

(** Given a convoluted datatype 'T' and a string 'id', deterministically creates a state named after this datatype and id (and whose type is 'T')  *)
val create_canonical_relation_from_datatypes :
  ?prefix:string option -> Term.Datatypes.t -> Typed_relation_symbol.t

(** Given a datatype 'T', deterministically creates a relation named after this datatype (and whose type is '<T>')  *)
val create_canonical_relation_from_datatype : Term.Datatype.t -> Typed_relation_symbol.t

val create_relation_for_identity : Datatype.t -> Typed_relation_symbol.t
