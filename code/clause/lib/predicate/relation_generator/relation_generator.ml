open Term

let create_canonical_relation_from_datatypes
    ?(prefix : string option = None)
    (datatypes : Datatypes.t) =
  let string_of_prefix =
    Option.fold ~none:"" ~some:(fun content_prefix -> content_prefix ^ "_") prefix
  in
  let string_of_relation =
    Format.flush_str_formatter
      (Format.fprintf Format.str_formatter "r_%s%a" string_of_prefix Datatypes.pp_no_special_char
         datatypes)
  in
  let state = Typed_relation_symbol.create (Symbol.create string_of_relation) datatypes in
  state

let create_canonical_relation_from_datatype (datatype : Datatype.t) : Typed_relation_symbol.t =
  create_canonical_relation_from_datatypes [datatype]

let create_relation_for_identity (dt : Datatype.t) : Typed_relation_symbol.t =
  create_canonical_relation_from_datatypes [dt; dt] ~prefix:(Some "id")
