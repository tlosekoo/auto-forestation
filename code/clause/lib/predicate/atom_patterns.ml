open Term
include Atom.Make (Patterns)

let pp =
  pp_paramm ~pp_predicate_symbol:Predicate_symbol_or_equality.pp
    ~pp_parameter:Term.Patterns.pp_without_brackets

let apply_substitution (s : Substitution.t) (atom_patterns : t) : t =
  let substitution_application = Substitution.apply_on_pattern s in
  let map_substitution = List.map substitution_application in
  self_map map_substitution atom_patterns

let instantiate (s : Substitution.t) (atom_patterns : t) : t =
  let instantiated_pred = apply_substitution s atom_patterns in
  let new_terms = get_argument instantiated_pred in
  let all_terms = List.for_all (fun t -> Term.Pattern.is_closed t) new_terms in
  if not all_terms then
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Cannot convert %a into terms (either not closed are contain functional symbols)"
           (Printing.pp_list_and_brackets Pattern.pp)
           new_terms)
    in
    raise (Invalid_argument error_message)
  else
    instantiated_pred
