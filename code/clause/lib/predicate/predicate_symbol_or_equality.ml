open Term

module Predicate_symbol_or_equality = struct
  type t =
    | Eq of Datatype.t
    | Pred of Typed_relation_symbol.t
  [@@deriving equal, compare]

  let create_from_predicate_symbol (ps : Typed_relation_symbol.t) : t = Pred ps
  let create_equality_from_datatype (dt : Datatype.t) : t = Eq dt

  let symbol_eq (dt : Datatype.t) : Symbol.t =
    let symbol_str = "eq_" ^ Datatype.to_string dt in
    Symbol.create symbol_str

  let get_symbol (p : t) : Symbol.t =
    match p with
    | Pred pr -> Typed_relation_symbol.get_symbol pr
    | Eq dt -> symbol_eq dt

  let get_datatype (p : t) : Datatypes.t =
    match p with
    | Pred pr -> Typed_relation_symbol.get_types pr
    | Eq dt -> [dt; dt]

  let pp (c : Format.formatter) (ps : t) : unit =
    let s, _cd = (get_symbol ps, get_datatype ps) in
    Format.fprintf c "%a" Symbol.pp s

  let get_typed_relation_symbol_opt (pe : t) : Typed_relation_symbol.t option =
    match pe with
    | Pred pr -> Some pr
    | Eq _ -> None

  let into_relation (pe : t) : Typed_relation_symbol.t =
    match pe with
    | Pred pr -> pr
    | Eq dt -> Relation_generator.create_relation_for_identity dt

  let is_equality_predicate (p : t) : bool =
    match p with
    | Eq _ -> true
    | Pred _ -> false
end

include Predicate_symbol_or_equality
