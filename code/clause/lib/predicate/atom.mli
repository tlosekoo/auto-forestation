open Term

module type S = sig
  module Parameter : Misc.Ordered_and_printable.OP

  type t = {
    predicate : Predicate_symbol_or_equality.t;
    argument : Parameter.t;
  }

  include Misc.Ordered_and_printable.OP with type t := t

  val create_from_predicate_symbol : Typed_relation_symbol.t -> Parameter.t -> t
  val create : Predicate_symbol_or_equality.t -> Parameter.t -> t
  val get_predicate : t -> Predicate_symbol_or_equality.t
  val get_argument : t -> Parameter.t
  val is_equality_predicate : t -> bool
  val self_map : (Parameter.t -> Parameter.t) -> t -> t
  val self_mapi : (Predicate_symbol_or_equality.t -> Parameter.t -> Parameter.t) -> t -> t

  val pp_paramm :
    pp_predicate_symbol:(Format.formatter -> Predicate_symbol_or_equality.t -> unit) ->
    pp_parameter:(Format.formatter -> Parameter.t -> unit) ->
    Format.formatter ->
    t ->
    unit

  val pp : Format.formatter -> t -> unit
end

module type SMap = sig
  module Parameter1 : Misc.Ordered_and_printable.OP
  module Parameter2 : Misc.Ordered_and_printable.OP
  module PA1 : S with module Parameter := Parameter1
  module PA2 : S with module Parameter := Parameter2

  val mapi : (Predicate_symbol_or_equality.t -> Parameter1.t -> Parameter2.t) -> PA1.t -> PA2.t
  val map : (Parameter1.t -> Parameter2.t) -> PA1.t -> PA2.t
end

module Make : functor (Parameter : Misc.Ordered_and_printable.OP) ->
  S with module Parameter := Parameter

module MakeMap : functor
  (Parameter1 : Misc.Ordered_and_printable.OP)
  (Parameter2 : Misc.Ordered_and_printable.OP)
  ->
  SMap
    with module Parameter1 := Parameter1
     and module Parameter2 := Parameter2
     and module PA1 := Make(Parameter1)
     and module PA2 := Make(Parameter2)
