(*   *)
(*   *)
(* Printing helpers *)
let rec pp_list_sep
    (separator : string)
    (pp : Format.formatter -> 'a -> unit)
    (c : Format.formatter)
    (l : 'a list) : unit =
  match l with
  | [] -> ()
  | [e] -> pp c e
  | e :: l' -> Format.fprintf c "%a%s%a" pp e separator (pp_list_sep separator pp) l'

let pp_list_sep_surrounded
    (separator : string)
    (left : string)
    (right : string)
    (pp : Format.formatter -> 'a -> unit)
    (channel : Format.formatter)
    (l : 'a list) : unit =
  Format.fprintf channel "%s%a%s" left (pp_list_sep separator pp) l right

let pp_list pp_a = pp_list_sep ", " pp_a
(* let pp_list : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit = pp_list_sep ", " *)
(* let pp_list (pp_a : (Format.formatter -> 'a -> unit)) (c : Format.formatter)  (l : 'a list) : unit = *)
(* pp_list_sep ", " pp_a c l *)

let pp_list_and_brackets_sep (separator : string) = pp_list_sep_surrounded separator "[" "]"
let pp_list_and_brackets pp_a = pp_list_and_brackets_sep ", " pp_a

let pp_opt (pp : Format.formatter -> 'a -> unit) (c : Format.formatter) (opt : 'a option) : unit =
  match opt with
  | None -> Format.fprintf c "None"
  | Some e -> Format.fprintf c "Some(%a)" pp e

let pp_int (c : Format.formatter) = Format.fprintf c "%d"
let pp_string (c : Format.formatter) = Format.fprintf c "%s"
let pp_bool (c : Format.formatter) = Format.fprintf c "%b"
let pp_char (c : Format.formatter) = Format.fprintf c "%c"

let pp_pair
    (pp_a : Format.formatter -> 'a -> unit)
    (pp_b : Format.formatter -> 'b -> unit)
    (c : Format.formatter)
    ((a, b) : 'a * 'b) : unit =
  Format.fprintf c "(%a, %a)" pp_a a pp_b b

let pp_binding
    ?(sep_key_value = " -> ")
    (pp_key : Format.formatter -> 'a -> unit)
    (pp_val : Format.formatter -> 'b -> unit)
    (c : Format.formatter)
    ((key, value) : 'a * 'b) : unit =
  Format.fprintf c "%a%s%a" pp_key key sep_key_value pp_val value

let pp_assoclist
    (pp_key : Format.formatter -> 'a -> unit)
    (pp_val : Format.formatter -> 'b -> unit)
    (c : Format.formatter)
    (assoclist : ('a * 'b) list) : unit =
  Format.fprintf c "{%a}" (pp_list (pp_binding pp_key pp_val)) assoclist

let pp_assoclist_long
    (pp_key : Format.formatter -> 'a -> unit)
    (pp_val : Format.formatter -> 'b -> unit)
    (c : Format.formatter)
    (assoclist : ('a * 'b) list) : unit =
  Format.fprintf c "{\n%a\n}" (pp_list_sep "\n" (pp_binding pp_key pp_val)) assoclist

let pp_ignore_arg_and_print (to_print : string) (c : Format.formatter) (_arg : 'a) : unit =
  Format.fprintf c "%s" to_print

let pp_ok () = Format.fprintf Format.std_formatter "Ok"
let pp_unit c () = Format.fprintf c "()"
