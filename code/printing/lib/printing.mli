(** This module provides helpers for printing various basic datatypes. *)

(** 'pp_list_sep sep pp_e c l' prints every element of 'l' (with 'pp_e') on channel 'c' and separated by 'sep'. *)
val pp_list_sep : string -> (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(** 'pp_list_sep sep left right pp_e c l' prints the list, separated by 'sep' and surrounded by 'left' and 'right'. *)
val pp_list_sep_surrounded :
  string ->
  string ->
  string ->
  (Format.formatter -> 'a -> unit) ->
  Format.formatter ->
  'a list ->
  unit

(** 'pp_list pp_e c l' prints elements of 'l' on channel 'c' with separator ", ". *)
val pp_list : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(** 'pp_list pp_e c l' prints elements of 'l' on channel 'c' with separator sep, but surrounded by '[' ']' *)
val pp_list_and_brackets_sep :
  string -> (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(** 'pp_list pp_e c l' prints elements of 'l' on channel 'c' with separator ", ", but surrounded by '[' ']' *)
val pp_list_and_brackets : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(** 'pp_opt pp c opt' prints "Some(e)" if opt contains a data and "None" otherwise. *)
val pp_opt : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a option -> unit

(** Prints an int *)
val pp_int : Format.formatter -> int -> unit

(** prints a string *)
val pp_string : Format.formatter -> string -> unit

(** Prints a boolean *)
val pp_bool : Format.formatter -> bool -> unit

(** Prints a char *)
val pp_char : Format.formatter -> char -> unit

(** 'pp_binding pp_s pp_d c (s,d)' prints "s |-> d", as in a mapping from a value 's' to a value 'd'. *)
val pp_binding :
  ?sep_key_value:string ->
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  'a * 'b ->
  unit

(** Printf an entire association list, i.e. function, on a single line *)
val pp_assoclist :
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  ('a * 'b) list ->
  unit

(** Printf an entire association list, i.e. function, with a different line for each binding. *)
val pp_assoclist_long :
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  ('a * 'b) list ->
  unit

(** 'pp_pair pp_a pp_b c (a,b)' pretty-prints the pair '(a,b)' into channel 'c'. *)
val pp_pair :
  (Format.formatter -> 'a -> unit) ->
  (Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  'a * 'b ->
  unit

(** 'pp_ignore_arg_and_print s c a' prints 's' on channel 'c' and ignores 'a'. *)
val pp_ignore_arg_and_print : string -> Format.formatter -> 'a -> unit

(** Prints 'Ok' on std *)
val pp_ok : unit -> unit

val pp_unit : Format.formatter -> unit -> unit
