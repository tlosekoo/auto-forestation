let%expect_test "list printing" =
  (Printing.pp_list_sep_surrounded " SEP " "LEFT- " " -RIGHT" Printing.pp_int)
    Format.std_formatter [2; 4; 6; 8; 5; 9];
  [%expect {| LEFT- 2 SEP 4 SEP 6 SEP 8 SEP 5 SEP 9 -RIGHT |}]

let%expect_test "ok printing" =
  Printing.pp_ok ();
  [%expect {| Ok |}]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
