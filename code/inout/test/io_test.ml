open Inout

let root_from_tests = "../../../"
let path_to_folder_of_this_file = root_from_tests ^ "lib/"
let ls_source_files = Inout.Reader.ls path_to_folder_of_this_file
let ls_source_files_filtered = Inout.Reader.ls_filter_extension ".mli" path_to_folder_of_this_file

let%expect_test "ls_dir" =
  Format.pp_print_list ~pp_sep:Format.pp_print_space Format.pp_print_string Format.std_formatter
    ls_source_files;
  [%expect
    "\n\
    \    ../../../lib/dune ../../../lib/reader.mli ../../../lib/write_to_file.mli\n\
    \    ../../../lib/reader.ml\n\
    \    ../../../lib/write_to_file.ml"]

let%expect_test "ls_dir_filter" =
  Format.pp_print_list ~pp_sep:Format.pp_print_space Format.pp_print_string Format.std_formatter
    ls_source_files_filtered;
  [%expect "\n    ../../../lib/reader.mli\n    ../../../lib/write_to_file.mli"]

let%expect_test "reading unexisting file" =
  let read_plop = Reader.get_all_lines_from_file "plop" in
  match read_plop with
  | Ok _ -> Format.fprintf Format.std_formatter "What?"
  | Error error_message ->
      Format.fprintf Format.std_formatter "%s" error_message;
      [%expect "Failed to read file plop"]

let%test "ok" =
  let _ = Format.fprintf Format.std_formatter "ok" in
  true
