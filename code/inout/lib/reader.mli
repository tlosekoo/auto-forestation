(** This file implements generic input functions *)

(** Cannot fail *)
val get_lines_from_in_channel : in_channel -> string list

val get_all_lines_from_file : string -> (string list, string) Result.t

(** [dir_is_empty dir] is true, if [dir] contains no files except
 * "." and ".."
 *)
val directory_is_empty : string -> bool

(** 'ls dirname' returns the paths of all files that are
  contained in [dirname]. Each file is a path starting with [dirname].
  *)
val ls : string -> string list

(** 'ls_filter_extension ext dirname' acts as 'ls', but only returns files whose extensin is 'ext'. Extension must begin with a dot. *)
val ls_filter_extension : string -> string -> string list
