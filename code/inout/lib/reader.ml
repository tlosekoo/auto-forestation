let directory_is_empty dir = Array.length (Sys.readdir dir) = 0

let ls_filter_extension_opt ~(ext_opt : string option) (dir : string) : string list =
  let files =
    if Sys.is_directory dir then
      Sys.readdir dir |> Array.to_list |> List.map (Filename.concat dir)
    else
      []
  in
  match ext_opt with
  | None -> files
  | Some ext -> List.filter (fun file -> Filename.extension file |> String.equal ext) files

let ls = ls_filter_extension_opt ~ext_opt:None
let ls_filter_extension (ext : string) = ls_filter_extension_opt ~ext_opt:(Some ext)
(* let smtlib_extension = ".smt2" *)

let get_lines_from_in_channel (input : in_channel) : string list =
  let lines = ref [] in
  try
    while true do
      lines := input_line input :: !lines
    done;
    raise (Failure "never gets here")
  with
  | End_of_file -> List.rev !lines

let get_all_lines_from_file (file : string) : (string list, string) Result.t =
  try
    let opened_file = open_in file in
    let lines = get_lines_from_in_channel opened_file in
    close_in opened_file;
    Result.ok lines
  with
  | _ ->
      let error_message = Format.sprintf "Failed to read file %s" file in
      Result.error error_message
