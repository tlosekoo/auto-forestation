(** This module implements basic writing utilities and filename generation *)

(** Given a path 'P', a name 'N', and an extension 'E', returns a unique filename of the form '<P>/<N>_<identifier>.<E>' *)
val get_new_filename : string -> string -> string -> string

(** Given a path 'P', a name 'N', and a list of extensions '[Ei]', returns a list of unique filenames of the form '<P>/<N>_<identifier>.<Ei>'.
	This function throws an exception if extensions are not paiwise disctinct *)
val get_new_filenames : string -> string -> string list -> string list

(** Given a filename and some string data, writes the data in the file. *)
val write_from_string : string -> string -> unit

(** Given a filenaùe and a printing function, prints into the file. *)
val write_from_pp : string -> (Format.formatter -> unit) -> unit
