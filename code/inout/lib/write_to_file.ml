(* Allows for creating multiple temporary not overwritten by the next one *)
let cpt_file = ref 0
let incr_cpt_file () : unit = cpt_file := !cpt_file + 1
let get_cpt_file () : int = !cpt_file

let get_and_incr_cpt_file () : int =
  let cpt = get_cpt_file () in
  let () = incr_cpt_file () in
  cpt

let get_new_filename (path : string) (prefix : string) (extension : string) : string =
  Printf.sprintf "%s%s_generated_%d.%s" path prefix (get_and_incr_cpt_file ()) extension

let get_new_filenames (path : string) (prefix : string) (extensions : string list) : string list =
  let cpt = get_and_incr_cpt_file () in
  let filenames =
    List.map (fun ext -> Printf.sprintf "%s%s_generated_%d.%s" path prefix cpt ext) extensions
  in
  filenames

let write_from_string (filename : string) (data : string) : unit =
  let f = open_out filename in
  let formatter = Format.formatter_of_out_channel f in
  let () = Format.fprintf formatter "%s@." data in
  let () = close_out f in
  ()

let write_from_pp (filename : string) (pp_necessary_information : Format.formatter -> unit) : unit =
  let data = Format.asprintf "%a" (fun c () -> pp_necessary_information c) () in
  let () = write_from_string filename data in
  ()
