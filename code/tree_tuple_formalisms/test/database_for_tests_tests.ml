let many_typed_functions = Term.Database_for_tests.some_typed_functions
let many_datatype = Term.Database_for_tests.some_datatypes
let many_tuple_of_datatypes = Term.Database_for_tests.some_convoluted_datatypes
let many_patterns = Term.Database_for_tests.patterns_of_small_depth ()
let many_tuples_of_patterns = Term.Database_for_tests.many_tuple_of_patterns ()

(* let lemma_same_languages_automata_and_prrs  *)
(* module Comparator_TreeAutomaton_Shocs = Testing_lemmas.Make2 (Tree_automaton) (Shocs) *)

(* let%test "different_formalisms_same_langauge" =
   List.for_all
     (fun (automaton, prrs) ->
       Comparator_TreeAutomaton_Shocs.recognize_same_language automaton prrs many_tuples_of_patterns)
     Datatabase_for_tests.equivalent_automata_and_prrs *)

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
