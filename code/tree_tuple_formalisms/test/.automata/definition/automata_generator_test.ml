open Tree_tuple_formalisms
open Term
open Aliases
module Map_datatype_to_its_automaton = Misc.My_map.Make2 (Datatype) (Tree_automaton)
module Map_datatypes_to_their_terms = Misc.My_map.Make2 (Datatypes) (Set_patterns)

let env = Database_for_tests.some_type_env

let automata_of_basic_datatypes : Map_datatype_to_its_automaton.t =
  let datatypes = Datatype_environment.get_datatypes env in
  let automata =
    datatypes |> Set_datatype.to_list
    |> List.map (fun d -> (d, Tree_automaton_generator.all_in_a_type env [d]))
    |> Map_datatype_to_its_automaton.of_list
  in
  automata

let terms_of_each_datatype =
  let datatypes =
    Database_for_tests_tests.many_tuples_of_patterns |> Set_patterns.to_seq
    |> Seq.map Patterns.get_type |> Set_datatypes.of_seq
  in
  let terms_each_type =
    datatypes |> Set_datatypes.to_list
    |> List.map (fun d ->
           let terms_of_datatype_d =
             Set_patterns.filter
               (fun t -> t |> Patterns.get_type |> Datatypes.equal d)
               Database_for_tests_tests.many_tuples_of_patterns
           in
           (d, terms_of_datatype_d))
    |> Map_datatypes_to_their_terms.of_list
  in
  terms_each_type

let same_type_as_patterns (a : Tree_automaton.t) (patterns : Term.Patterns.t) : bool =
  Datatypes.equal (Tree_automaton.get_type_of_automaton a) (Patterns.get_type patterns)

let lemma_automaton_for_some_type_recognizes_all_and_only_this_type (d : Datatype.t) : bool =
  let a = Tree_automaton_generator.all_in_a_type env [d] in
  let terms_correct_type, terms_other_types =
    terms_of_each_datatype |> Map_datatypes_to_their_terms.to_seq
    |> Seq.partition (fun (datatypes, _terms) -> Datatypes.equal [d] datatypes)
    |> Misc.Pair.map (Seq.map snd) (Seq.map snd)
    |> Misc.Pair.map Set_patterns.seq_union Set_patterns.seq_union
    |> Misc.Pair.map
         (Set_patterns.filter (same_type_as_patterns a))
         (Set_patterns.filter (same_type_as_patterns a))
    (* Map_datatypes_to_their_terms. (Datatypes.create [d]) terms_of_each_datatype *)
  in

  let automaton_accepts_the_term = Tree_automaton.accepts a in
  let accept_correct_terms = Set_patterns.for_all automaton_accepts_the_term terms_correct_type in
  let accept_some_other_terms = Set_patterns.exists automaton_accepts_the_term terms_other_types in

  accept_correct_terms && not accept_some_other_terms

let%test "test_lemma_automaton_for_some_type_recognizes_all_and_only_this_type" =
  Set_datatype.for_all lemma_automaton_for_some_type_recognizes_all_and_only_this_type
    (Map_datatype_to_its_automaton.domain automata_of_basic_datatypes)

let lemma_id_automaton_only_recognizes_correct_type_and_diagonal_relation
    (env : Term.Datatype_environment.t)
    (ps : Set_pattern.t) : bool =
  let types_patterns =
    ps |> Set_pattern.to_list |> List.map Term.Pattern.get_type |> Set_datatype.of_list
  in
  let types_environement = Term.Datatype_environment.get_datatypes env in
  let () = assert (Set_datatype.subset types_patterns types_environement) in
  let cmp_patterns_by_type p1 p2 =
    Term.Datatype.compare (Term.Pattern.get_type p1) (Term.Pattern.get_type p2)
  in
  let patterns_of_each_type = Set_pattern.preorder cmp_patterns_by_type ps in
  let types_and_patterns =
    List.map
      (fun set_patterns -> (Term.Pattern.get_type (Set_pattern.choose set_patterns), set_patterns))
      patterns_of_each_type
  in

  let ok =
    List.for_all
      (fun (datatype, set_patterns_of_this_datatype) ->
        let list_patterns = Set_pattern.to_list set_patterns_of_this_datatype in
        let pairs_of_patterns = Misc.List_op.cartesian_product list_patterns list_patterns in
        let a = Tree_automaton_generator.identity_in_a_type env datatype in
        List.for_all
          (fun (p1, p2) ->
            let automaton_recognizes_a1_a2 = Tree_automaton.accepts a [p1; p2] in
            let p1_eq_p2 = Term.Pattern.equal p1 p2 in
            Bool.equal automaton_recognizes_a1_a2 p1_eq_p2)
          pairs_of_patterns)
      types_and_patterns
  in

  ok

let%test "test_lemma_id_automaton_only_recognizes_correct_type_and_diagonal_relation" =
  lemma_id_automaton_only_recognizes_correct_type_and_diagonal_relation
    Database_for_tests.some_type_env Database_for_tests_tests.many_patterns

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
