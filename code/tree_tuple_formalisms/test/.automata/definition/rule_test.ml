open Tree_tuple_formalisms
open Aliases_automata

(* Prove the following *)
let lemma_getters_and_creator_are_working (r : Transition.t) =
  let rule =
    Transition.create (Transition.get_functions r)
      (Transition.get_convolution_form r)
      (Transition.get_substates r) (Transition.get_towards r)
  in

  rule = r

(* Unfortunately, can only be tested for now *)
let%test "lemma_getters_and_creator_are_working: Should been proved instead of tested" =
  let some_rules = Database_for_tests_automata.some_rules in
  Set_transition.for_all lemma_getters_and_creator_are_working some_rules

let%test "create success" =
  try
    let _ =
      Transition.create Database_for_tests_automata.cons_s Convolution.Right
        [Database_for_tests_automata.q_n; Database_for_tests_automata.q_l_n]
        Database_for_tests_automata.q_l_n
    in
    true
  with
  | _ -> false

let%test "create failure (nb of substates)" =
  try
    let _ =
      Transition.create Database_for_tests_automata.cons_s Convolution.Right
        [Database_for_tests_automata.q_n] Database_for_tests_automata.q_l_n
    in
    false
  with
  | _ -> true

let%test "create failure (type of subsets)" =
  try
    let _ =
      Transition.create Database_for_tests_automata.cons_s Convolution.Right
        [Database_for_tests_automata.q_n; Database_for_tests_automata.q_n]
        Database_for_tests_automata.q_l_n
    in
    false
  with
  | _ -> true

let%test "create failure (type of goal)" =
  try
    let _ =
      Transition.create Database_for_tests_automata.cons_s Convolution.Right
        [Database_for_tests_automata.q_n; Database_for_tests_automata.q_l_n]
        Database_for_tests_automata.q_n_n
    in
    false
  with
  | _ -> true

let%expect_test "pp" =
  Transition.pp Format.std_formatter Database_for_tests_automata.rule_nil_and_z_in__q_l_n;
  [%expect "<nil, z>() -> q_l_n"]

let%test "rules matching 1" =
  Transition.matches_state Database_for_tests_automata.qe_n
    Database_for_tests_automata.rule_s_qo_in__qe_n

let%test "rules matching 2" =
  not
    (Transition.matches_state Database_for_tests_automata.qo_n
       Database_for_tests_automata.rule_s_qo_in__qe_n)

let%test "rules matching 3" =
  not
    (Transition.matches_patterns Database_for_tests_automata.patterns_consznil_sz
       Database_for_tests_automata.rule_s_qo_in__qe_n)

let%test "rules matching 4" =
  Transition.matches_patterns Database_for_tests_automata.patterns_consznil_sz
    Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n

let%test "rules matching 5" =
  not
    (Transition.matches_patterns Database_for_tests_automata.patterns_nil_z
       Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n)

let%test "contains_only_states_from 1" =
  Transition.contains_only_states_from
    (Set_typed_relation_symbol.of_list
       [Database_for_tests_automata.qo_n; Database_for_tests_automata.qe_n])
    Database_for_tests_automata.rule_s_qe_in__qo_n

let%test "contains_only_states_from 2" =
  not
    (Transition.contains_only_states_from
       (Set_typed_relation_symbol.of_list [Database_for_tests_automata.qe_n])
       Database_for_tests_automata.rule_s_qe_in__qo_n)

let%test "contains_only_states_from 3" =
  Transition.contains_only_states_from
    (Set_typed_relation_symbol.of_list
       [
         Database_for_tests_automata.qe_n;
         Database_for_tests_automata.qo_n;
         Database_for_tests_automata.q_l_l;
       ])
    Database_for_tests_automata.rule_s_qe_in__qo_n

let%test "is_rule_matching_constraint_symbols 1" =
  not
    (Transition.matches_constraint_symbols
       [Some Database_for_tests_automata.s_symbol; None]
       Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n)

let%test "is_rule_matching_constraint_symbols 2" =
  Transition.matches_constraint_symbols
    [Some Database_for_tests_automata.cons_symbol; None]
    Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n

let%test "is_rule_matching_constraint_symbols 3" =
  not
    (Transition.matches_constraint_symbols
       [Some Database_for_tests_automata.cons_symbol]
       Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n)

let%test "is_rule_matching_constraint_symbols 3.5" =
  not
    (Transition.matches_constraint_symbols
       [Some Database_for_tests_automata.cons_symbol; None; None]
       Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n)

let%test "is_rule_matching_constraint_symbols 4" =
  not
    (Transition.matches_constraint_symbols [None; None] Database_for_tests_automata.rule_nil_in__q_l)

let%test "is_rule_matching_constraint_symbols 5" =
  Transition.matches_constraint_symbols [None; None]
    Database_for_tests_automata.rule_snoc_and_s_of_qrln_qnp_in__q_rl_n

let%test "is_rule_matching_constraint_symbols 6" =
  Transition.matches_constraint_symbols
    [None; Some Database_for_tests_automata.s_symbol]
    Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n

let%test "get_symbol" =
  Transition.get_functions Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  = Database_for_tests_automata.cons_s

let%test "get substates" =
  Transition.get_substates Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  = [Database_for_tests_automata.q_n; Database_for_tests_automata.q_l_n]

let%test "get towards" =
  Transition.get_towards Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  = Database_for_tests_automata.q_l_n

let%test "get convolution form" =
  Transition.get_convolution_form Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  = Convolution.Right

let%test "get arity of rule symbol 1" =
  Transition.get_arity_of_rule_symbol Database_for_tests_automata.rule_nil_and_z_in__q_l_n = 0

let%test "get arity of rule symbol 2" =
  Transition.get_arity_of_rule_symbol Database_for_tests_automata.rule_s_qe_in__qo_n = 1

let%test "get arity of rule symbol 3" =
  Transition.get_arity_of_rule_symbol
    Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  = 2

let%test "get arity of rule symbol 4" =
  Transition.get_arity_of_rule_symbol Database_for_tests_automata.rule_lin_and_z_in__q_rl_n = 0

let%test "get_nb_symbols_in_rule_symbol 1" =
  Transition.get_nb_symbols_of_rule Database_for_tests_automata.rule_nil_and_z_in__q_l_n = 2

let%test "get_nb_symbols_in_rule_symbol 2" =
  Transition.get_nb_symbols_of_rule Database_for_tests_automata.rule_s_qe_in__qo_n = 1

let%test "extract_states_from_rules 1" =
  let extracted_states =
    Transition.extract_states Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n
  in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.q_n; Database_for_tests_automata.q_l_n]
  in
  Set_typed_relation_symbol.equal extracted_states expected_states

let%test "extract_states_from_rules 2" =
  let extracted_states = Transition.extract_states Database_for_tests_automata.rule_s_qe_in__qo_n in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n; Database_for_tests_automata.qo_n]
  in
  Set_typed_relation_symbol.equal extracted_states expected_states

let%test "extract_states_from_rules 3" =
  let extracted_states =
    List.map Transition.extract_states
      [
        Database_for_tests_automata.rule_s_qe_in__qo_n;
        Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n;
      ]
    |> Set_typed_relation_symbol.list_union
  in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [
        Database_for_tests_automata.q_n;
        Database_for_tests_automata.q_l_n;
        Database_for_tests_automata.qe_n;
        Database_for_tests_automata.qo_n;
      ]
  in

  Set_typed_relation_symbol.equal extracted_states expected_states

(* let%test "extract alphabet " =
   let alphabet =
     Transition.extract_states Database_for_tests_automata.rule_cons_and_s_of_qnnatpad_qlnatlistnat_in_qlnatlistnat
   in
   let expected_alphabet =
     Term.Alphabet.Ranked_alphabet.of_list
       [Term.Database_for_tests_automata.cons_symbol; Term.Database_for_tests_automata.s_symbol]
   in
   Term.Alphabet.Ranked_alphabet.equal alphabet expected_alphabet *)

let partity_reversing_map =
  Map_typedRelationSymbol_typedRelationFunction.to_function_with_not_mapped_is_identity
    Database_for_tests_automata.state_substitution_reverse_parity

let partity_forgetting_map =
  Map_typedRelationSymbol_typedRelationFunction.to_function_with_not_mapped_is_identity
    Database_for_tests_automata.state_substitution_forget_parity

let%test "state map 1 " =
  Transition.state_map partity_reversing_map Database_for_tests_automata.rule_s_qe_in__qo_n
  = Database_for_tests_automata.rule_s_qo_in__qe_n

let%test "state map 1 " =
  Transition.state_map partity_forgetting_map Database_for_tests_automata.rule_s_qe_in__qo_n
  = Database_for_tests_automata.rule_s_qn_in__q_n

let%test "state map 3" =
  try
    let wrong_type_map state =
      if state = Database_for_tests_automata.q_n then
        Database_for_tests_automata.qo_n
      else if state = Database_for_tests_automata.qo_n then
        Database_for_tests_automata.q_l
      else
        Database_for_tests_automata.q_l
    in

    let _ = Transition.state_map wrong_type_map Database_for_tests_automata.rule_s_qe_in__qo_n in
    false
  with
  | _ -> true

let%test "arity" =
  let zz_symbol =
    ( [Database_for_tests_automata.z_symbol; Database_for_tests_automata.z_symbol],
      Convolution.Complete )
  in
  Convoluted_functions.get_arity zz_symbol = 0

let%test "arity of rule" =
  let zz_symbol = [Database_for_tests_automata.z_symbol; Database_for_tests_automata.z_symbol] in
  let r = Transition.create zz_symbol Convolution.Complete [] Database_for_tests_automata.q_n_n in
  Transition.get_arity_of_rule_symbol r = 0

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
