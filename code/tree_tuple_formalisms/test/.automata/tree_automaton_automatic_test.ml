open Tree_tuple_formalisms
open Aliases_automata
module Testing_automata = Testing_lemmas.Make (Tree_automaton)

(*
   TESTING LEMMAS
*)

let lemma_complemented_automaton_xor_automaton () =
  Set_automaton.for_all
    (Testing_automata.complement_recognize_complement_language
       Database_for_tests_automata.some_type_env Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_automata.some_automata

let%test "complemented_automaton_xor_automaton" = lemma_complemented_automaton_xor_automaton ()

let lemma_doubly_complemented_automaton () =
  Set_automaton.for_all
    (Testing_automata.doubly_complement_recognize_same_language
       Database_for_tests_automata.some_type_env Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_automata.some_automata

let%test "doubly_complemented_automaton" = lemma_doubly_complemented_automaton ()

let lemma_reachable_automaton_accepts_the_same_terms () =
  Set_automaton.for_all
    (Testing_automata.minimized_recognize_same_language
       Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_automata.some_automata

let%test "reachable_automaton_accepts_the_same_terms" =
  lemma_reachable_automaton_accepts_the_same_terms ()

let lemma_nothing_in_a_type_recognizes_nothing () =
  Set_datatypes.for_all
    (fun datatypes ->
      Testing_automata.nothing_in_a_type_recognizes_nothing datatypes
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_tuple_of_datatypes

let%test "nothing_in_a_type_recognizes_nothing" = lemma_nothing_in_a_type_recognizes_nothing ()

let lemma_all_in_a_type_recognizes_everything () =
  Set_datatypes.for_all
    (fun datatypes ->
      Testing_automata.all_in_a_type_recognize_all_of_this_datatype
        Term.Database_for_tests.some_type_env datatypes
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_tuple_of_datatypes

let%test "all_in_a_type_recognizes_nothing" = lemma_all_in_a_type_recognizes_everything ()

let lemma_identity_in_a_type_recognizes_diagonal () =
  Set_datatype.for_all
    (fun datatype ->
      Testing_automata.identity_in_a_type_recognizes_only_diagonal_of_this_datatype
        Term.Database_for_tests.some_type_env datatype
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_datatype

let%test "identity_in_a_type_recognizes_diagonal" = lemma_identity_in_a_type_recognizes_diagonal ()

(*
    Other functions that are not in every Tree_tuple_formalism
*)

(* Normalize with new states *)

(* The lemma states that every term is recognized in a normalized automaton if and only if it was recognized in the non-normalized automaton or it is the term to normalize *)
let lemma_normalize_with_new_states_automaton_accepts_the_same_terms_plus_normalized_term
    (a : Tree_automaton.t)
    (ct_to_normalize : Term.Patterns.t)
    (state_for_normalisation : Term.Typed_relation_symbol.t)
    (cts_to_recognize : Set_patterns.t) : bool =
  (* let () = Format.fprintf Format.std_formatter "term %a has convolution degree %d.\n" Convoluted_term.pp ct_to_normalize (Convoluted_term.get_degree_convolution ct_to_normalize) in *)
  let types_are_matching =
    Tree_automaton.get_type_of_automaton a = Term.Patterns.get_type ct_to_normalize
  in

  if not types_are_matching then
    true
  else
    let normalized_automaton =
      Tree_automaton.normalize_with_new_states a ct_to_normalize state_for_normalisation
    in
    let states_recognizing_term_to_normalize_in_automaton =
      Tree_automaton.accepts_in a ct_to_normalize
    in
    let states_recognizing_term_to_normalize_in_normalized_automaton =
      Tree_automaton.accepts_in normalized_automaton ct_to_normalize
    in

    let normalisation_recognizes_what_it_has_normalised =
      not
        (Set_typed_relation_symbol.is_empty
           states_recognizing_term_to_normalize_in_normalized_automaton)
    in

    let check_for_one_term_that_it_works ct_to_recognize =
      let states_recognizing_term_to_recognize_in_automaton =
        Tree_automaton.accepts_in a ct_to_recognize
      in
      let states_recognizing_term_to_recognize_in_normalized_automaton =
        Tree_automaton.accepts_in normalized_automaton ct_to_recognize
      in
      let same_states_plus_the_new_ones =
        if Term.Patterns.equal ct_to_normalize ct_to_recognize then
          Set_typed_relation_symbol.equal
            states_recognizing_term_to_normalize_in_normalized_automaton
            (Set_typed_relation_symbol.add state_for_normalisation
               states_recognizing_term_to_normalize_in_automaton)
        else
          Set_typed_relation_symbol.subset states_recognizing_term_to_recognize_in_automaton
            states_recognizing_term_to_recognize_in_normalized_automaton
      in
      let new_state_only_recognizes_the_term_to_normalize =
        Term.Patterns.equal ct_to_normalize ct_to_recognize
        || Set_typed_relation_symbol.mem state_for_normalisation
             states_recognizing_term_to_recognize_in_normalized_automaton
           = false
      in

      let () =
        if
          (not same_states_plus_the_new_ones) || not new_state_only_recognizes_the_term_to_normalize
        then
          let () =
            Format.fprintf Format.std_formatter
              "\nsame_states_plus_the_new_ones: %b\nnew_state_only_recognizes_the_term_to_normalize: %b \n"
              same_states_plus_the_new_ones new_state_only_recognizes_the_term_to_normalize
          in
          (* let () = Format.fprintf Format.std_formatter "\nautomaton:\n%a\n\nnormalized_automaton:\n%a\n\n" (Tree_automaton.pp_parameterized ~complete:false) a (Tree_automaton.pp_parameterized ~complete:false) normalized_automaton in *)
          let () =
            Format.fprintf Format.std_formatter "Term to recognize: %a\n" Term.Patterns.pp
              ct_to_recognize
          in
          let () =
            Format.fprintf Format.std_formatter "Term to normalize: %a\n" Term.Patterns.pp
              ct_to_normalize
          in
          let () =
            Format.fprintf Format.std_formatter "state for normalization: %a\n"
              Term.Typed_relation_symbol.pp state_for_normalisation
          in
          let () = Format.fprintf Format.std_formatter "Automaton: %a\n" Tree_automaton.pp a in
          let () =
            Format.fprintf Format.std_formatter "normalized automaton: %a\n\n" Tree_automaton.pp
              normalized_automaton
          in
          let () =
            Format.fprintf Format.std_formatter
              "states_recognizing_term_to_normalize_in_automaton: %a\n" Set_typed_relation_symbol.pp
              states_recognizing_term_to_normalize_in_automaton
          in
          let () =
            Format.fprintf Format.std_formatter
              "states_recognizing_term_to_normalize_in_normalized_automaton: %a\n"
              Set_typed_relation_symbol.pp
              states_recognizing_term_to_normalize_in_normalized_automaton
          in
          let () =
            Format.fprintf Format.std_formatter
              "states_recognizing_term_to_recognize_in_automaton: %a\n" Set_typed_relation_symbol.pp
              states_recognizing_term_to_recognize_in_automaton
          in
          let () =
            Format.fprintf Format.std_formatter
              "states_recognizing_term_to_recognize_in_normalized_automaton: %a\n"
              Set_typed_relation_symbol.pp
              states_recognizing_term_to_recognize_in_normalized_automaton
          in
          ()
      in
      same_states_plus_the_new_ones && new_state_only_recognizes_the_term_to_normalize
    in

    let cts_to_examine = cts_to_recognize in

    Aliases.Set_patterns.for_all check_for_one_term_that_it_works cts_to_examine
    && normalisation_recognizes_what_it_has_normalised

let test_normalized_automaton_with_new_states_accepts_the_same_terms_plus_normalized_term () =
  let some_terms_to_normalize = Database_for_tests_tests.many_tuples_of_patterns in
  let lemma_is_true_for_all_test_database =
    Set_automaton.for_all
      (fun automata ->
        Aliases.Set_patterns.for_all
          (fun term_to_normalize ->
            let some_new_state =
              State_generator.generate_a_fresh_state (Term.Patterns.get_type term_to_normalize)
            in
            lemma_normalize_with_new_states_automaton_accepts_the_same_terms_plus_normalized_term
              automata term_to_normalize some_new_state
              Database_for_tests_tests.many_tuples_of_patterns)
          some_terms_to_normalize)
      Database_for_tests_automata.some_automata
  in
  lemma_is_true_for_all_test_database

let%test "normalized_automaton_accepts_the_same_terms" =
  test_normalized_automaton_with_new_states_accepts_the_same_terms_plus_normalized_term ()

(* Normalize *)

(* The lemma states that every term is recognized in a normalized automaton if and only if it was recognized in the non-normalized automaton or it is the term to normalize *)
let lemma_normalize_automaton_accepts_the_same_terms_plus_normalized_term
    (a : Tree_automaton.t)
    (ct_to_normalize : Term.Patterns.t)
    (cts_to_recognize : Aliases.Set_patterns.t) : bool =
  let state_for_normalisation, normalized_automaton = Tree_automaton.normalize a ct_to_normalize in
  let states_recognizing_term_to_normalize_in_automaton =
    Tree_automaton.accepts_in a ct_to_normalize
  in
  let states_recognizing_term_to_normalize_in_normalized_automaton =
    Tree_automaton.accepts_in normalized_automaton ct_to_normalize
  in

  let terms_to_recognize = cts_to_recognize in

  let check_one_term_to_recognize (t : Term.Patterns.t) : bool =
    let states_recognizing_term_to_recognize_in_automaton = Tree_automaton.accepts_in a t in
    let states_recognizing_term_to_recognize_in_normalized_automaton =
      Tree_automaton.accepts_in normalized_automaton t
    in

    let same_states =
      if Tree_automaton.accepts a ct_to_normalize then
        Set_typed_relation_symbol.equal states_recognizing_term_to_normalize_in_normalized_automaton
          states_recognizing_term_to_normalize_in_automaton
      else
        Set_typed_relation_symbol.subset states_recognizing_term_to_recognize_in_automaton
          states_recognizing_term_to_recognize_in_normalized_automaton
    in

    let new_state_recognizes_the_term_to_normalize =
      Set_typed_relation_symbol.mem state_for_normalisation
        states_recognizing_term_to_normalize_in_normalized_automaton
    in
    same_states && new_state_recognizes_the_term_to_normalize
  in

  let ok_for_all_terms_to_check =
    Aliases.Set_patterns.for_all check_one_term_to_recognize terms_to_recognize
  in

  let () =
    if not ok_for_all_terms_to_check then
      let () =
        Format.fprintf Format.std_formatter "\nautomaton:\n%a\n\nnormalized_automaton:\n%a\n\n"
          Tree_automaton.pp a Tree_automaton.pp normalized_automaton
      in
      (* let () =
           Format.fprintf Format.std_formatter "Term to recognize: %a\n" Convoluted_term.pp
             ct_to_recognize
         in *)
      let () =
        Format.fprintf Format.std_formatter "Term to normalize: %a\n" Term.Patterns.pp
          ct_to_normalize
      in
      let () =
        Format.fprintf Format.std_formatter "state for normalization: %a\n"
          Term.Typed_relation_symbol.pp state_for_normalisation
      in
      let () = Format.fprintf Format.std_formatter "Automaton: %a\n" Tree_automaton.pp a in
      let () =
        Format.fprintf Format.std_formatter "normalized automaton: %a\n\n" Tree_automaton.pp
          normalized_automaton
      in
      let () =
        Format.fprintf Format.std_formatter
          "states_recognizing_term_to_normalize_in_automaton: %a\n" Set_typed_relation_symbol.pp
          states_recognizing_term_to_normalize_in_automaton
      in
      let () =
        Format.fprintf Format.std_formatter
          "states_recognizing_term_to_normalize_in_normalized_automaton: %a\n"
          Set_typed_relation_symbol.pp states_recognizing_term_to_normalize_in_normalized_automaton
      in
      (* let () =
           Format.fprintf Format.std_formatter
             "states_recognizing_term_to_recognize_in_automaton: %a\n" Set_typed_relation_symbol.pp
             states_recognizing_term_to_recognize_in_automaton
         in
         let () =
           Format.fprintf Format.std_formatter
             "states_recognizing_term_to_recognize_in_normalized_automaton: %a\n" Set_typed_relation_symbol.pp
             states_recognizing_term_to_recognize_in_normalized_automaton
         in *)
      ()
  in
  ok_for_all_terms_to_check

let test_normalized_automaton_accepts_the_same_terms_plus_normalized_term () =
  let some_terms_to_normalize = Database_for_tests_tests.many_tuples_of_patterns in
  let lemma_is_true_for_all_test_database =
    Set_automaton.for_all
      (fun automata ->
        Aliases.Set_patterns.for_all
          (fun term_to_normalize ->
            lemma_normalize_automaton_accepts_the_same_terms_plus_normalized_term automata
              term_to_normalize some_terms_to_normalize)
          some_terms_to_normalize)
      Database_for_tests_automata.some_automata
  in
  lemma_is_true_for_all_test_database

let%test "normalized_automaton_accepts_the_same_terms" =
  test_normalized_automaton_accepts_the_same_terms_plus_normalized_term ()

(* completes *)
let lemma_completion_is_idempotent (env : Term.Datatype_environment.t) (a : Tree_automaton.t) : bool
    =
  let completed_automaton = Tree_automaton.complete env a in
  let completed_completed_automaton = Tree_automaton.complete env completed_automaton in
  (* Format.fprintf Format.std_formatter "\n1st completion:%a\n2nd completion:%a\n\n" Tree_automaton.pp completed_automaton Tree_automaton.pp completed_completed_automaton ; *)
  let automata_are_equal = Tree_automaton.equal completed_automaton completed_completed_automaton in
  automata_are_equal

let test_completion_is_idempotent () =
  Set_automaton.for_all
    (lemma_completion_is_idempotent Database_for_tests_automata.some_type_env)
    Database_for_tests_automata.some_automata

let%test "completion_is_idempotent" = test_completion_is_idempotent ()

let lemma_completion_completes
    (env : Term.Datatype_environment.t)
    (a : Tree_automaton.t)
    (ts : Set_patterns.t) =
  let terms_of_correct_type_and_convolution =
    Set_patterns.filter (Testing_automata.same_type_as_patterns a) ts
  in
  let completed_automaton = Tree_automaton.complete env a in
  let ok =
    Set_patterns.for_all
      (fun t ->
        let accepted_in_before_completion = Tree_automaton.accepts_in a t in
        let accepted_in_after_completion = Tree_automaton.accepts_in completed_automaton t in
        let accepts_in_more_states =
          Set_typed_relation_symbol.subset accepted_in_before_completion
            accepted_in_after_completion
        in
        let accepted_in_at_least_one_state =
          not (Set_typed_relation_symbol.is_empty accepted_in_after_completion)
        in
        accepts_in_more_states && accepted_in_at_least_one_state)
      terms_of_correct_type_and_convolution
  in
  ok

let test_completion_completes () =
  let some_terms_to_check = Database_for_tests_tests.many_tuples_of_patterns in
  let lemma_is_true_for_all_test_database =
    Set_automaton.for_all
      (fun automata ->
        lemma_completion_completes Database_for_tests_automata.some_type_env automata
          some_terms_to_check)
      Database_for_tests_automata.some_automata
  in
  lemma_is_true_for_all_test_database

let%test "test_completion_completes" = test_completion_completes ()

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
