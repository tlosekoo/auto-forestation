open Term
open Tree_tuple_formalisms
open Database_for_tests_automata
open Aliases_automata

(* open Tree_automata *)

(* Open Term.D *)

let%test "states are correct 1" =
  Typed_relation_symbol.to_string q_n = "q_n"
  && Typed_relation_symbol.get_types q_n = Database_for_tests.c_nat

let%test "states are correct 2" =
  Typed_relation_symbol.to_string q_n_n = "q_n_n"
  && Typed_relation_symbol.get_types q_n_n = Database_for_tests.c_nat_nat

let%test "states are correct 3" =
  Typed_relation_symbol.to_string q_n_l = "q_n_l"
  && Typed_relation_symbol.get_types q_n_l = Database_for_tests.c_nat_natlist

let%test "states are correct 4" =
  Typed_relation_symbol.to_string q_l_n = "q_l_n"
  && Typed_relation_symbol.get_types q_l_n = Database_for_tests.c_natlist_nat

let%test "states are correct 5" =
  Typed_relation_symbol.to_string q = "q" && Typed_relation_symbol.get_types q = []

let%test "states are correct 6" =
  Typed_relation_symbol.to_string q_l_l = "q_l_l"
  && Typed_relation_symbol.get_types q_l_l = Database_for_tests.c_natlist_natlist

let%test "states are correct 7" =
  Typed_relation_symbol.to_string qq_l_l = "qq_l_l"
  && Typed_relation_symbol.get_types qq_l_l = Database_for_tests.c_natlist_natlist

let%test "states are correct 8" =
  Typed_relation_symbol.to_string qe_n = "qe_n"
  && Typed_relation_symbol.get_types qe_n = Database_for_tests.c_nat

let%test "states are correct 9" =
  Typed_relation_symbol.to_string qo_n = "qo_n"
  && Typed_relation_symbol.get_types qo_n = Database_for_tests.c_nat

let automaton_recognizes_all_terms_of_its_type_and_only_those
    (a : Tree_automaton.t)
    (terms : Set_patterns.t) : bool =
  let datatype_a = Tree_automaton.get_type_of_automaton a in

  let of_correct_type, of_other_types =
    Set_patterns.partition (fun t -> t |> Patterns.get_type |> Datatypes.equal datatype_a) terms
  in
  Set_patterns.for_all (Tree_automaton.accepts a) of_correct_type
  && Set_patterns.for_all (fun t -> not (Tree_automaton.accepts a t)) of_other_types

(* let%test "a_nat_1 recognizes nats" =
   automaton_recognizes_all_terms_of_its_type_and_only_those a_nat_1
     (Database_for_tests.every_right_convoluted_term ()) *)

let%expect_test "pp automaton a_nat_1" =
  Tree_automaton.pp Format.std_formatter a_nat_1;
  [%expect
    "
    {{{
    Q={q_n},
    Q_f={q_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_nat_2" =
  Tree_automaton.pp Format.std_formatter a_nat_2;
  [%expect
    "
    {{{
    Q={q_nat},
    Q_f={q_nat},
    Delta=
    {
    <s>(q_nat) -> q_nat
    <z>() -> q_nat
    }

    Datatype: <nat>
    Convolution form: left
    }}}"]

let%expect_test "pp automaton a_natlist_1" =
  Tree_automaton.pp Format.std_formatter a_natlist_1;
  [%expect
    "
  {{{
  Q={q_l, q_n},
  Q_f={q_l},
  Delta=
  {
  <cons>(q_n, q_l) -> q_l
  <nil>() -> q_l
  <s>(q_n) -> q_n
  <z>() -> q_n
  }

  Datatype: <natlist>
  Convolution form: right
  }}}"]

let%expect_test "pp automaton a_natlist_2" =
  Tree_automaton.pp Format.std_formatter a_natlist_2;
  [%expect
    "
  {{{
  Q={q_nat, q_natlist},
  Q_f={q_natlist},
  Delta=
  {
  <s>(q_nat) -> q_nat
  <z>() -> q_nat
  <cons>(q_nat, q_natlist) -> q_natlist
  <nil>() -> q_natlist
  }

  Datatype: <natlist>
  Convolution form: left
  }}}"]

let%expect_test "pp automaton a_even" =
  Tree_automaton.pp Format.std_formatter a_even;
  [%expect
    "
    {{{
    Q={qe_n, qo_n},
    Q_f={qe_n},
    Delta=
    {
    <s>(qo_n) -> qe_n
    <z>() -> qe_n
    <s>(qe_n) -> qo_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_odd" =
  Tree_automaton.pp Format.std_formatter a_odd;
  [%expect
    "
    {{{
    Q={qe_n, qo_n},
    Q_f={qo_n},
    Delta=
    {
    <s>(qo_n) -> qe_n
    <z>() -> qe_n
    <s>(qe_n) -> qo_n
    }

    Datatype: <nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_list_size" =
  Tree_automaton.pp Format.std_formatter a_list_size;
  [%expect
    "
    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <s>(q_n) -> q_n
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_list_of_z_and_their_size" =
  Tree_automaton.pp Format.std_formatter a_lists_of_z_and_their_size;
  [%expect
    "
    {{{
    Q={q_l_n, q_n},
    Q_f={q_l_n},
    Delta=
    {
    <z>() -> q_n
    <cons, s>(q_n, q_l_n) -> q_l_n
    <nil, z>() -> q_l_n
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_natlist_nat" =
  Tree_automaton.pp Format.std_formatter a_natlist_nat;
  [%expect
    "
    {{{
    Q={q_comp_nat, q_comp_natlist, q_comp_natlist_x_nat},
    Q_f={q_comp_natlist_x_nat},
    Delta=
    {
    <s>(q_comp_nat) -> q_comp_nat
    <z>() -> q_comp_nat
    <cons>(q_comp_nat, q_comp_natlist) -> q_comp_natlist
    <nil>() -> q_comp_natlist
    <cons, s>(q_comp_nat, q_comp_natlist_x_nat) -> q_comp_natlist_x_nat
    <cons, z>(q_comp_nat, q_comp_natlist) -> q_comp_natlist_x_nat
    <nil, s>(q_comp_nat) -> q_comp_natlist_x_nat
    <nil, z>() -> q_comp_natlist_x_nat
    }

    Datatype: <natlist * nat>
    Convolution form: right
    }}}"]

let%expect_test "pp automaton a_natlist_natlist_id" =
  Tree_automaton.pp Format.std_formatter a_natlist_natlist_id;
  [%expect
    "
    {{{
    Q={q_nat_x_nat, q_natlist_x_natlist},
    Q_f={q_natlist_x_natlist},
    Delta=
    {
    <s, s>(q_nat_x_nat) -> q_nat_x_nat
    <z, z>() -> q_nat_x_nat
    <cons, cons>(q_nat_x_nat, q_natlist_x_natlist) -> q_natlist_x_natlist
    <nil, nil>() -> q_natlist_x_natlist
    }

    Datatype: <natlist * natlist>
    Convolution form: left
    }}}"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
