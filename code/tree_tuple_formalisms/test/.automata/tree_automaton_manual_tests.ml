open Tree_tuple_formalisms
open Aliases_automata
module Testing_automata = Testing_lemmas.Make (Tree_automaton)

(* let automata_for_tests = Database_for_tests_automata.some_automata *)

(* get_arity *)
let%test "get_arity 1" = Tree_automaton.get_arity Database_for_tests_automata.a_nat_1 = 1
let%test "get_arity 2" = Tree_automaton.get_arity Database_for_tests_automata.a_list_size = 2

(* Create *)
let%test "creation success" =
  try
    let _a_even =
      Tree_automaton.create
        (Set_typed_relation_symbol.singleton Database_for_tests_automata.qe_n)
        (Set_transition.of_list
           [
             Database_for_tests_automata.rule_s_qe_in__qo_n;
             Database_for_tests_automata.rule_s_qo_in__qe_n;
           ])
        Database_for_tests_automata.c_nat Convolution.Right
    in
    true
  with
  | _ -> false

let%test "creation failure wrong final state type 1" =
  try
    let _a_even =
      Tree_automaton.create
        (Set_typed_relation_symbol.singleton Database_for_tests_automata.q_l)
        (Set_transition.of_list
           [
             Database_for_tests_automata.rule_s_qe_in__qo_n;
             Database_for_tests_automata.rule_s_qo_in__qe_n;
             Database_for_tests_automata.rule_z_in__qe_n;
           ])
        Database_for_tests_automata.c_nat Convolution.Right
    in
    false
  with
  | _ -> true

let%test "creation failure wrong final state type 2" =
  try
    let _a_even =
      Tree_automaton.create
        (Set_typed_relation_symbol.singleton Database_for_tests_automata.q_n_n)
        (Set_transition.of_list
           [
             Database_for_tests_automata.rule_s_qe_in__qo_n;
             Database_for_tests_automata.rule_s_qo_in__qe_n;
             Database_for_tests_automata.rule_z_in__qe_n;
           ])
        Database_for_tests_automata.c_nat Convolution.Right
    in
    false
  with
  | _ -> true

let%test "creation failure wrong rule arity" =
  try
    let _a_even =
      Tree_automaton.create
        (Set_typed_relation_symbol.singleton Database_for_tests_automata.q_n)
        (Set_transition.of_list
           [
             Database_for_tests_automata.rule_cons_and_s_of_qn_qln_in__q_l_n;
             Database_for_tests_automata.rule_s_qo_in__qe_n;
             Database_for_tests_automata.rule_z_in__q_n;
           ])
        Database_for_tests_automata.c_nat Convolution.Right
    in
    true
  with
  | _ -> false

let%test "creation failure wrong datatype" =
  try
    let _a_even =
      Tree_automaton.create
        (Set_typed_relation_symbol.singleton Database_for_tests_automata.q_n)
        (Set_transition.of_list
           [
             Database_for_tests_automata.rule_s_qe_in__qo_n;
             Database_for_tests_automata.rule_s_qo_in__qe_n;
             Database_for_tests_automata.rule_z_in__qe_n;
           ])
        Database_for_tests_automata.c_natlist Convolution.Right
    in
    false
  with
  | _ -> true

(* equal *)

let%test "even_odd that forgot parity is equivalent_to_recognizing_all_nat)" =
  let renaming =
    Map_typedRelationSymbol_typedRelationFunction.to_function_with_not_mapped_is_identity
      Database_for_tests_automata.state_substitution_forget_parity
  in
  let a_even_forgot_parity = Tree_automaton.map_state renaming Database_for_tests_automata.a_even in

  let automata_are_equal =
    Tree_automaton.equal a_even_forgot_parity Database_for_tests_automata.a_nat_1
  in
  let automata_are_equivalent =
    Testing_automata.recognize_same_language a_even_forgot_parity
      Database_for_tests_automata.a_nat_1 Database_for_tests_tests.many_tuples_of_patterns
  in
  let ok = automata_are_equivalent && automata_are_equal in

  let () =
    if not ok then
      Format.fprintf Format.std_formatter
        "even forgot parity: \n%a\n\nnat:\n%a\n\nequal: %b\nequivalent:%b\n\n " Tree_automaton.pp
        a_even_forgot_parity Tree_automaton.pp Database_for_tests_automata.a_nat_1
        automata_are_equal automata_are_equivalent
    else
      ()
  in
  ok

let%test "equal and equivalent (reverse_parity even))" =
  let renaming =
    Map_typedRelationSymbol_typedRelationFunction.to_function_with_not_mapped_is_identity
      Database_for_tests_automata.state_substitution_reverse_parity
  in
  let a_even_reverse_parity =
    Tree_automaton.map_state renaming Database_for_tests_automata.a_even
  in
  let automata_equal =
    Tree_automaton.equal a_even_reverse_parity Database_for_tests_automata.a_even
  in

  let automata_equivalent =
    Testing_automata.recognize_same_language a_even_reverse_parity
      Database_for_tests_automata.a_even Database_for_tests_tests.many_tuples_of_patterns
  in
  let ok = automata_equivalent && not automata_equal in

  let () =
    if not ok then
      Format.fprintf Format.std_formatter
        "\neven reverse parity: \n%a\n\neven:\n%a\n\nequal: %b\nequivalent:%b\n\n   "
        Tree_automaton.pp a_even_reverse_parity Tree_automaton.pp Database_for_tests_automata.a_even
        automata_equal automata_equivalent
    else
      ()
  in
  ok

(* get_rules *)
let%test "get rules" =
  let rules = Tree_automaton.get_rules Database_for_tests_automata.a_even in
  let expected_rules =
    Set_transition.of_list
      [
        Database_for_tests_automata.rule_s_qe_in__qo_n;
        Database_for_tests_automata.rule_s_qo_in__qe_n;
        Database_for_tests_automata.rule_z_in__qe_n;
      ]
  in
  Set_transition.equal rules expected_rules

(* get_final_states *)
let%test "get_final_states" =
  let states = Tree_automaton.get_final_states Database_for_tests_automata.a_even in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n (* Database_for_tests_automata.qo_nat *)]
  in
  Set_typed_relation_symbol.equal states expected_states

(* extract_states *)
let%test "extract_states" =
  let states = Tree_automaton.extract_defined_states Database_for_tests_automata.a_even in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n; Database_for_tests_automata.qo_n]
  in
  Set_typed_relation_symbol.equal states expected_states

(* get_type_of_automaton *)
let%test "get_type_of_automaton 1" =
  let datatype_automaton =
    Tree_automaton.get_type_of_automaton Database_for_tests_automata.a_even
  in
  let expected_datatype = Database_for_tests_automata.c_nat in
  Term.Datatypes.equal datatype_automaton expected_datatype

let%test "get_type_of_automaton 2" =
  let datatype_automaton =
    Tree_automaton.get_type_of_automaton
      (Tree_automaton.none_in_a_type Database_for_tests_automata.c_nat_natlist)
  in
  let expected_datatype = Database_for_tests_automata.c_nat_natlist in
  Term.Datatypes.equal datatype_automaton expected_datatype

let%test "get_convolution_form 2" =
  let convolution_form =
    Tree_automaton.get_convolution_form Database_for_tests_automata.a_list_size
  in
  let expected_cf = Convolution.Right in
  convolution_form = expected_cf

(* set_final_states *)
let%test "set_final_states 1" =
  let a_even_modified =
    Tree_automaton.add_final_state Database_for_tests_automata.a_even
      Database_for_tests_automata.qo_n
  in
  let states = Tree_automaton.get_final_states a_even_modified in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n; Database_for_tests_automata.qo_n]
  in
  Set_typed_relation_symbol.equal states expected_states

let%test "set_final_states 1.5" =
  let a_even_modified =
    Tree_automaton.add_final_state Database_for_tests_automata.a_even
      Database_for_tests_automata.qe_n
  in
  let states = Tree_automaton.get_final_states a_even_modified in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n (* Database_for_tests_automata.qo_nat *)]
  in
  Set_typed_relation_symbol.equal states expected_states

let%test "set_final_states 2" =
  let a_even_modified =
    Tree_automaton.add_final_state Database_for_tests_automata.a_even
      Database_for_tests_automata.q_n
  in
  let states = Tree_automaton.get_final_states a_even_modified in
  let expected_states =
    Set_typed_relation_symbol.of_list
      [Database_for_tests_automata.qe_n; Database_for_tests_automata.q_n]
  in
  Set_typed_relation_symbol.equal states expected_states

let%test "set_final_states failure 1" =
  try
    let _a_even_modified =
      Tree_automaton.add_final_state Database_for_tests_automata.a_even
        Database_for_tests_automata.q_l
    in
    false
  with
  | _ -> true

let%test "set_final_states failure 2" =
  try
    let _a_even_modified =
      Tree_automaton.add_final_state Database_for_tests_automata.a_even
        Database_for_tests_automata.q_n_n
    in
    false
  with
  | _ -> true

(* empty automaton *)
let%expect_test "pp empty automaton" =
  Tree_automaton.pp Format.std_formatter
    (Tree_automaton.none_in_a_type Database_for_tests_automata.c_nat_nat);
  [%expect
    "
    {{{
    Q={},
    Q_f={},
    Delta=
    {

    }

    Datatype: <nat * nat>
    Convolution form: left
    }}}"]

let%test "empty_automaton_recognizes_nothing" =
  Testing_automata.nothing_in_a_type_recognizes_nothing Database_for_tests_prrs.c_nat
    Database_for_tests_tests.many_tuples_of_patterns

(* accepts_in *)
let%test "accepts_in 1" =
  Set_typed_relation_symbol.equal
    (Tree_automaton.accepts_in Database_for_tests_automata.a_even
       Database_for_tests_automata.patterns_z)
    (Set_typed_relation_symbol.of_list [Database_for_tests_automata.qe_n])

let%test "accepts_in 2" =
  Set_typed_relation_symbol.equal
    (Tree_automaton.accepts_in Database_for_tests_automata.a_even
       Database_for_tests_automata.patterns_sz)
    (Set_typed_relation_symbol.of_list [Database_for_tests_automata.qo_n])

let%test "accepts_in 3" =
  Set_typed_relation_symbol.equal
    (Tree_automaton.accepts_in Database_for_tests_automata.a_list_size
       Database_for_tests_automata.patterns_z)
    (Set_typed_relation_symbol.of_list [Database_for_tests_automata.q_n])

let%expect_test "accepts_in" =
  let zz_symbol = [Database_for_tests_automata.z_symbol; Database_for_tests_automata.z_symbol] in
  let a =
    Tree_automaton.create Set_typed_relation_symbol.empty
      (Set_transition.of_list
         [Transition.create zz_symbol Convolution.Complete [] Database_for_tests_automata.q_n_n])
      Database_for_tests_automata.c_nat_nat Convolution.Complete
  in
  let t = [Database_for_tests_automata.p_z; Database_for_tests_automata.p_z] in
  let accepted_in = Tree_automaton.accepts_in a t in
  Format.fprintf Format.std_formatter "z_z is accepted in: %a" Set_typed_relation_symbol.pp
    accepted_in;
  [%expect "z_z is accepted in: {q_n_n}"]

let%test "automaton_and_its_size does not recognizes 0" =
  (not
     (Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
        Database_for_tests_automata.patterns_z))
  && not
       (Tree_automaton.accepts Database_for_tests_automata.a_list_size
          Database_for_tests_automata.patterns_z)

let%test "automaton_and_its_size does not recognize S(0)" =
  (not
     (Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
        Database_for_tests_automata.patterns_sz))
  && not
       (Tree_automaton.accepts Database_for_tests_automata.a_list_size
          Database_for_tests_automata.patterns_sz)

let%test "automaton_and_its_size does not recognize (Nil + S(0))" =
  Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
    Database_for_tests_automata.patterns_nil_sz
  && not
       (Tree_automaton.accepts Database_for_tests_automata.a_list_size
          Database_for_tests_automata.patterns_nil_sz)

let%test "automaton_and_its_size does recognize (nil + 0)" =
  (not
     (Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
        Database_for_tests_automata.patterns_nil_z))
  && Tree_automaton.accepts Database_for_tests_automata.a_list_size
       Database_for_tests_automata.patterns_nil_z

let%test "automaton_and_its_size does not recognize (Cons(0, Nil) + 0)" =
  Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
    Database_for_tests_automata.patterns_consznil_z
  && not
       (Tree_automaton.accepts Database_for_tests_automata.a_list_size
          Database_for_tests_automata.patterns_consznil_z)

let%test "automaton_and_its_size does recognize (Cons(S(0), Nil) + S(0))" =
  (not
     (Tree_automaton.accepts Database_for_tests_automata.complemented_automaton_of_list_and_its_size
        Database_for_tests_automata.patterns_conssznil_z))
  && Tree_automaton.accepts Database_for_tests_automata.a_list_size
       Database_for_tests_automata.patterns_conssznil_z

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
