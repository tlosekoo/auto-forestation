open Tree_tuple_formalisms
open Term
open Term.Aliases
(* open Clause *)

(* open Aliases *)

let prss_for_tests = Database_for_tests_shocs.some_shocs

(* get_arity *)
(* let%test "get_arity 1" =
     Shocs.get_datatype Database_for_tests_shocs.a_nat_1 = Database_for_tests_shocs.c_nat

   let%test "get_arity 2" =
     Shocs.get_datatype Database_for_tests_shocs.a_list_size = Database_for_tests_shocs.c_natlist_nat *)

(* Create *)
(* let%test "typing success" =
   try
     let a_even =
         Shocs.of_list
            [
              Database_for_tests_shocs.rule_z_in__re_n;
              Database_for_tests_shocs.rule_s_re_in__ro_n;
              Database_for_tests_shocs.rule_s_ro_in__re_n;
            ]
     in
     Shocs.typecheck a_even
   with
   | _ -> false *)

(* get_rules *)
(* let%test "get rules" =
   let rules = Shocs.get_rrs Database_for_tests_shocs.a_even in
   let expected_rules =
     Shocs.of_list
       [
         Database_for_tests_shocs.rule_s_re_in__ro_n;
         Database_for_tests_shocs.rule_s_ro_in__re_n;
         Database_for_tests_shocs.rule_z_in__re_n;
       ]
   in
   Shocs.equal rules expected_rules *)

(* get_final_states *)
(* let%test "get_final_states" =
   let states = Shocs.get_final_states Database_for_tests_shocs.a_even in
   let expected_states =
     Set_typed_relation_symbol.of_list
       [Term.Database_for_tests.relation_even (* Term.Database_for_tests.relation_oddat *)]
   in
   Set_typed_relation_symbol.equal states expected_states *)

(* extract_predicates *)
let%test "extract predicates" =
  let predicates = Shocs.extract_head_relation_symbols (fst Database_for_tests_shocs.a_even) in
  let expected_predicates =
    Set_typed_relation_symbol.of_list
      [Term.Database_for_tests.relation_even; Term.Database_for_tests.relation_odd]
  in
  Set_typed_relation_symbol.equal predicates expected_predicates

(* set_final_states *)
(* let%test "set_final_states 1" =
   let a_even_modified =
     Shocs.set_final_states_and_datatype Database_for_tests_shocs.a_even
       (Set_typed_relation_symbol.of_list
          [Term.Database_for_tests.relation_odd; Term.Database_for_tests.relation_even])
       Term.Database_for_tests.c_nat
   in
   let new_final = Shocs.get_final_states a_even_modified in
   let expected_final =
     Set_typed_relation_symbol.of_list [Term.Database_for_tests.relation_even; Term.Database_for_tests.relation_odd]
   in
   Set_typed_relation_symbol.equal new_final expected_final *)

(* empty automaton *)
let%expect_test "pp empty" =
  let empty_shocs, relation = Shocs.none_in_a_type Term.Database_for_tests.c_nat_nat in
  Format.fprintf Format.std_formatter "%a:\n%a\n" Typed_relation_symbol.pp relation Shocs.pp
    empty_shocs;
  [%expect "
        r_nat_x_nat:
        {

        }"]

(* accepts_in *)
let%test "accepts_in 1" =
  Set_typed_relation_symbol.equal
    (Shocs.accepts_in (fst Database_for_tests_shocs.a_even) [Term.Database_for_tests.p_z])
    (Set_typed_relation_symbol.of_list [Term.Database_for_tests.relation_even])

let%test "accepts_in 2" =
  Set_typed_relation_symbol.equal
    (Shocs.accepts_in (fst Database_for_tests_shocs.a_even) [Term.Database_for_tests.p_sz])
    (Set_typed_relation_symbol.of_list [Term.Database_for_tests.relation_odd])

let%test "accepts_in 3" =
  Set_typed_relation_symbol.equal
    (Shocs.accepts_in (fst Database_for_tests_shocs.a_list_size) [Term.Database_for_tests.p_z])
    (Set_typed_relation_symbol.of_list [Term.Database_for_tests.relation_n])

let%expect_test "accepts_in" =
  let zz_symbol = [Term.Database_for_tests.z_symbol; Term.Database_for_tests.z_symbol] in
  let a = Shocs.of_list [Shoc.create_from_list Term.Database_for_tests.relation_n_n zz_symbol []] in
  let t = [Term.Database_for_tests.p_z; Term.Database_for_tests.p_z] in
  let accepted_in = Shocs.accepts_in a t in
  Format.fprintf Format.std_formatter "z_z is accepted in: %a" Set_typed_relation_symbol.pp
    accepted_in;
  [%expect "z_z is accepted in: {r_n_n}"]

(* accepts *)

let%expect_test "automaton mod2" =
  Format.fprintf Format.std_formatter "%a\n" Pointed_shocs.pp Database_for_tests_shocs.a_even;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
      odd(s(x_0_0)) <= even(x_0_0)
    }, even)"]

let complement_automaton_mod2 =
  ( Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env
      (fst Database_for_tests_shocs.a_even)
      (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_even)),
    snd Database_for_tests_shocs.a_even )

let complement_automaton_mod3 =
  ( Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env
      (fst Database_for_tests_shocs.a_mod3)
      (Set_typed_relation_symbol.singleton (snd Database_for_tests_shocs.a_mod3)),
    snd Database_for_tests_shocs.a_mod3 )

let%expect_test "automaton mod2" =
  Format.fprintf Format.std_formatter "%a\n" Pointed_shocs.pp Database_for_tests_shocs.a_even;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
      odd(s(x_0_0)) <= even(x_0_0)
    }, even)"]

let%expect_test "automaton complement mod2" =
  Format.fprintf Format.std_formatter "%a" Pointed_shocs.pp complement_automaton_mod2;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      odd(s(x_0_0)) <= even(x_0_0)
      odd(z) <= True
    }, even)"]

let%expect_test "automaton mod3" =
  Format.fprintf Format.std_formatter "%a\n" Pointed_shocs.pp Database_for_tests_shocs.a_mod3;
  [%expect
    "
    ({
      r0_n(s(x_0_0)) <= r2_n(x_0_0)
      r0_n(z) <= True
      r1_n(s(x_0_0)) <= r0_n(x_0_0)
      r2_n(s(x_0_0)) <= r1_n(x_0_0)
    }, r0_n)"]

let%expect_test "automaton complement mod3" =
  Format.fprintf Format.std_formatter "%a" Pointed_shocs.pp complement_automaton_mod3;
  [%expect
    "
    ({
      r0_n(s(x_0_0)) <= r2_n(x_0_0)
      r1_n(s(x_0_0)) <= r0_n(x_0_0)
      r1_n(z) <= True
      r2_n(s(x_0_0)) <= r1_n(x_0_0)
      r2_n(z) <= True
    }, r0_n)"]

let%expect_test "automaton mod6" =
  Format.fprintf Format.std_formatter "%a\n" Pointed_shocs.pp Database_for_tests_shocs.prrs_mod6;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
      odd(s(x_0_0)) <= even(x_0_0)
      r0_n(s(x_0_0)) <= r2_n(x_0_0)
      r0_n(z) <= True
      r1_n(s(x_0_0)) <= r0_n(x_0_0)
      r2_n(s(x_0_0)) <= r1_n(x_0_0)
      r_n(s(x_0_0)) <= odd(x_0_0) /\\ r2_n(x_0_0)
      r_n(z) <= True
    }, r_n)"]

let%expect_test "automaton complement mod6" =
  Format.fprintf Format.std_formatter "%a" Pointed_shocs.pp
    Database_for_tests_shocs.complement_prrs_mod_6;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      odd(s(x_0_0)) <= even(x_0_0)
      odd(z) <= True
      r0_n(s(x_0_0)) <= r2_n(x_0_0)
      r1_n(s(x_0_0)) <= r0_n(x_0_0)
      r1_n(z) <= True
      r2_n(s(x_0_0)) <= r1_n(x_0_0)
      r2_n(z) <= True
      r_n(s(x_0_0)) <= odd(x_0_0)
      r_n(s(x_0_0)) <= r2_n(x_0_0)
    }, r_n)"]

let%expect_test "automaton prrs_of_list_and_its_size" =
  Format.fprintf Format.std_formatter "%a\n" Pointed_shocs.pp Database_for_tests_shocs.a_list_size;
  [%expect
    "
    ({
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }, r_l_n)"]

let%expect_test "automaton complement prrs_of_list_and_its_size" =
  Format.fprintf Format.std_formatter "%a" Pointed_shocs.pp
    Database_for_tests_shocs.complement_prrs_list_and_its_size;
  [%expect
    "
    ({
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0)
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_n(x_0_0)
      r_l_n(cons(x_0_0, x_0_1), z) <= True
      r_l_n(nil, s(x_1_0)) <= True
      r_n(s(x_0_0)) <= r_n(x_0_0)
    }, r_l_n)"]

let%expect_test "pp a_natlist_nat" =
  Pointed_shocs.pp Format.std_formatter Database_for_tests_shocs.a_natlist_nat;
  [%expect
    "
      ({
        r_natlist_x_nat(cons(x_0_0, x_0_1), s(x_1_0)) <= True
        r_natlist_x_nat(cons(x_0_0, x_0_1), z) <= True
        r_natlist_x_nat(nil, s(x_1_0)) <= True
        r_natlist_x_nat(nil, z) <= True
      }, r_natlist_x_nat)"]

let%expect_test "pp complement a_natlist_nat" =
  let complement =
    Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env
      (fst Database_for_tests_shocs.a_natlist_nat)
      Set_typed_relation_symbol.empty
  in
  Shocs.pp Format.std_formatter complement;
  [%expect "
      {

      }"]

let%expect_test "pp complement twice a_natlist_nat" =
  let r = snd Database_for_tests_shocs.a_natlist_nat in
  let complement =
    Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env
      (fst Database_for_tests_shocs.a_natlist_nat)
      (Set_typed_relation_symbol.singleton r)
  in
  let complement_complement =
    Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env complement
      (Set_typed_relation_symbol.singleton r)
  in
  Shocs.pp Format.std_formatter complement_complement;
  [%expect
    "
{
  r_natlist_x_nat(cons(x_0_0, x_0_1), s(x_1_0)) <= True
  r_natlist_x_nat(cons(x_0_0, x_0_1), z) <= True
  r_natlist_x_nat(nil, s(x_1_0)) <= True
  r_natlist_x_nat(nil, z) <= True
}"]

let%expect_test "identity" =
  let shocs, relation =
    Shocs_generator.identity_in_a_type Term.Database_for_tests.testing_type_env
      Term.Database_for_tests.natlist
  in
  let states = Shocs_core.extract_defined_relations shocs in
  Format.fprintf Format.std_formatter "prrs:\n%a\n\nstates:\n%a\n" Pointed_shocs.pp (shocs, relation)
    (Set_typed_relation_symbol.pp_param ~opening:"{" ~closing:"}" ~sep:", "
       ~pp_element:(Term.Typed_relation_symbol.pp_parameterized ~complete:true))
    states;
  [%expect
    "
prrs:
({
  r_id_nat_x_nat(s(x_0_0), s(x_1_0)) <= r_id_nat_x_nat(x_0_0, x_1_0)
  r_id_nat_x_nat(z, z) <= True
  r_id_natlist_x_natlist(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0) /\\ r_id_natlist_x_natlist(x_0_1, x_1_1)
  r_id_natlist_x_natlist(nil, nil) <= True
}, r_id_natlist_x_natlist)

states:
{r_id_nat_x_nat : <nat * nat>, r_id_natlist_x_natlist : <natlist * natlist>}"]

(* Wrong type *)
let%test "automaton_and_its_size does not recognizes 0" =
  (not (Pointed_shocs.accepts Database_for_tests_shocs.a_list_size [Term.Database_for_tests.p_z]))
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
          [Term.Database_for_tests.p_z])

(* Wrong type *)
let%test "automaton_and_its_size does not recognize S(0)" =
  (not (Pointed_shocs.accepts Database_for_tests_shocs.a_list_size [Term.Database_for_tests.p_sz]))
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
          [Term.Database_for_tests.p_sz])

let%test "automaton_and_its_size does recognize (nil + 0)" =
  Pointed_shocs.accepts Database_for_tests_shocs.a_list_size
    [Term.Database_for_tests.p_nil; Term.Database_for_tests.p_z]
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
          [Term.Database_for_tests.p_nil; Term.Database_for_tests.p_z])

let%test "automaton_and_its_size does not recognize (Nil + S(0))" =
  Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
    [Term.Database_for_tests.p_nil; Term.Database_for_tests.p_sz]
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.a_list_size
          [Term.Database_for_tests.p_nil; Term.Database_for_tests.p_sz])

let%test "automaton_and_its_size does not recognize (Cons(0, Nil) + 0)" =
  Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
    [Term.Database_for_tests.p_consznil; Term.Database_for_tests.p_z]
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.a_list_size
          [Term.Database_for_tests.p_consznil; Term.Database_for_tests.p_z])

let%test "automaton_and_its_size does recognize (Cons(S(0), Nil) + S(0))" =
  Pointed_shocs.accepts Database_for_tests_shocs.a_list_size
    [Term.Database_for_tests.p_consznil; Term.Database_for_tests.p_sz]
  && not
       (Pointed_shocs.accepts Database_for_tests_shocs.complement_prrs_list_and_its_size
          [Term.Database_for_tests.p_consznil; Term.Database_for_tests.p_sz])

let%expect_test "automaton minimized_complement prrs_of_list_and_its_size" =
  let minimized_complemented_prrs_of_list_and_its_size =
    Shocs.minimize_while_preserving
      (fst Database_for_tests_shocs.complement_prrs_list_and_its_size)
      (Shocs.extract_defined_relations
         (fst Database_for_tests_shocs.complement_prrs_list_and_its_size))
  in
  Format.fprintf Format.std_formatter "Un-minimized:\n%a\n\n\nMinimized:\n%a" Shocs.pp
    (fst Database_for_tests_shocs.complement_prrs_list_and_its_size)
    Shocs.pp minimized_complemented_prrs_of_list_and_its_size;
  [%expect
    "
    Un-minimized:
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0)
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_n(x_0_0)
      r_l_n(cons(x_0_0, x_0_1), z) <= True
      r_l_n(nil, s(x_1_0)) <= True
      r_n(s(x_0_0)) <= r_n(x_0_0)
    }


    Minimized:
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0)
      r_l_n(cons(x_0_0, x_0_1), z) <= True
      r_l_n(nil, s(x_1_0)) <= True
    }"]

let%expect_test "identity" =
  Format.fprintf Format.std_formatter "Identity natlist:\n%a\n" Pointed_shocs.pp
    (Shocs_generator.identity_in_a_type Term.Database_for_tests.testing_type_env
       Term.Database_for_tests.natlist);
  [%expect
    "
    Identity natlist:
    ({
      r_id_nat_x_nat(s(x_0_0), s(x_1_0)) <= r_id_nat_x_nat(x_0_0, x_1_0)
      r_id_nat_x_nat(z, z) <= True
      r_id_natlist_x_natlist(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0) /\\ r_id_natlist_x_natlist(x_0_1, x_1_1)
      r_id_natlist_x_natlist(nil, nil) <= True
    }, r_id_natlist_x_natlist)"]

let%expect_test "Complement of empty is all" =
  let datatype = Term.Database_for_tests.c_natlist_nat in
  let env = Term.Database_for_tests.testing_type_env in
  let empty_prrs, relation = Shocs_generator.none_in_a_type datatype in
  let complement_empty_prrs =
    Shocs_operation.complement_all_defined_relations_and env empty_prrs
      (Set_typed_relation_symbol.singleton relation)
  in
  Format.fprintf Format.std_formatter
    "For datatype %a,\nempty prrs:\n%a\n\ncomplement of empty prrs:\n%a\n" Term.Datatypes.pp
    datatype Shocs.pp empty_prrs Shocs.pp complement_empty_prrs;
  [%expect
    "
    For datatype <natlist * nat>,
    empty prrs:
    {

    }

    complement of empty prrs:
    {
      r_natlist_x_nat(cons(x_0_0, x_0_1), s(x_1_0)) <= True
      r_natlist_x_nat(cons(x_0_0, x_0_1), z) <= True
      r_natlist_x_nat(nil, s(x_1_0)) <= True
      r_natlist_x_nat(nil, z) <= True
    }"]

let%expect_test "minimisation" =
  let a_odd = Database_for_tests_shocs.a_odd in
  let a_list_size = Database_for_tests_shocs.a_list_size in
  let a_list_size_with_noise = Shocs.union (fst a_list_size) (fst a_odd) in
  let minimized_a_list_size_with_noise =
    Shocs.minimize_while_preserving a_list_size_with_noise
      (Set_typed_relation_symbol.singleton (snd a_list_size))
  in
  Format.fprintf Format.std_formatter "With noise:\n%a\n\nMinimized:\n%a\n" Shocs.pp
    a_list_size_with_noise Shocs.pp minimized_a_list_size_with_noise;
  [%expect
    "
    With noise:
    {
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
      odd(s(x_0_0)) <= even(x_0_0)
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }

    Minimized:
    {
      r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
      r_l_n(nil, z) <= True
      r_n(s(x_0_0)) <= r_n(x_0_0)
      r_n(z) <= True
    }"]

let%expect_test "identitites" =
  Format.fprintf Format.std_formatter "Identities for\n%a\nare:\n%a\n" Datatype_environment.pp
    Term.Database_for_tests.testing_type_env Shocs.pp
    (Shocs.identities Term.Database_for_tests.testing_type_env);
  [%expect
    "
    Identities for
    {
    custom -> {c_a, c_b, c_c, c_f, c_g}  ;  elt -> {a, b}  ;  elt_tree -> {leaf, node}  ;  nat -> {s, z}  ;  natlist -> {cons, nil}  ;  nattsil -> {lin, snoc}  ;  pair_nat_nat -> {pair}
    }
    are:
    {
      r_id_custom_x_custom(c_a, c_a) <= True
      r_id_custom_x_custom(c_b, c_b) <= True
      r_id_custom_x_custom(c_c, c_c) <= True
      r_id_custom_x_custom(c_f(x_0_0), c_f(x_1_0)) <= r_id_custom_x_custom(x_0_0, x_1_0)
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_g(x_1_0, x_1_1)) <= r_id_custom_x_custom(x_0_0, x_1_0) /\\ r_id_custom_x_custom(x_0_1, x_1_1)
      r_id_elt_tree_x_elt_tree(leaf, leaf) <= True
      r_id_elt_tree_x_elt_tree(node(x_0_0, x_0_1, x_0_2), node(x_1_0, x_1_1, x_1_2)) <= r_id_elt_tree_x_elt_tree(x_0_0, x_1_0) /\\ r_id_elt_tree_x_elt_tree(x_0_2, x_1_2) /\\ r_id_elt_x_elt(x_0_1, x_1_1)
      r_id_elt_x_elt(a, a) <= True
      r_id_elt_x_elt(b, b) <= True
      r_id_nat_x_nat(s(x_0_0), s(x_1_0)) <= r_id_nat_x_nat(x_0_0, x_1_0)
      r_id_nat_x_nat(z, z) <= True
      r_id_natlist_x_natlist(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0) /\\ r_id_natlist_x_natlist(x_0_1, x_1_1)
      r_id_natlist_x_natlist(nil, nil) <= True
      r_id_nattsil_x_nattsil(lin, lin) <= True
      r_id_nattsil_x_nattsil(snoc(x_0_0, x_0_1), snoc(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_1, x_1_1) /\\ r_id_nattsil_x_nattsil(x_0_0, x_1_0)
      r_id_pair_nat_nat_x_pair_nat_nat(pair(x_0_0, x_0_1), pair(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0) /\\ r_id_nat_x_nat(x_0_1, x_1_1)
    }"]

let%expect_test "complemented identitites" =
  Format.fprintf Format.std_formatter "Identities for\n%a\nare:\n%a\n" Datatype_environment.pp
    Term.Database_for_tests.testing_type_env Shocs.pp
    (Shocs.complement_all_defined_relations_and Term.Database_for_tests.testing_type_env
       (Shocs.identities Term.Database_for_tests.testing_type_env)
       Set_typed_relation_symbol.empty);
  [%expect
    "
    Identities for
    {
    custom -> {c_a, c_b, c_c, c_f, c_g}  ;  elt -> {a, b}  ;  elt_tree -> {leaf, node}  ;  nat -> {s, z}  ;  natlist -> {cons, nil}  ;  nattsil -> {lin, snoc}  ;  pair_nat_nat -> {pair}
    }
    are:
    {
      r_id_custom_x_custom(c_a, c_b) <= True
      r_id_custom_x_custom(c_a, c_c) <= True
      r_id_custom_x_custom(c_a, c_f(x_1_0)) <= True
      r_id_custom_x_custom(c_a, c_g(x_1_0, x_1_1)) <= True
      r_id_custom_x_custom(c_b, c_a) <= True
      r_id_custom_x_custom(c_b, c_c) <= True
      r_id_custom_x_custom(c_b, c_f(x_1_0)) <= True
      r_id_custom_x_custom(c_b, c_g(x_1_0, x_1_1)) <= True
      r_id_custom_x_custom(c_c, c_a) <= True
      r_id_custom_x_custom(c_c, c_b) <= True
      r_id_custom_x_custom(c_c, c_f(x_1_0)) <= True
      r_id_custom_x_custom(c_c, c_g(x_1_0, x_1_1)) <= True
      r_id_custom_x_custom(c_f(x_0_0), c_a) <= True
      r_id_custom_x_custom(c_f(x_0_0), c_b) <= True
      r_id_custom_x_custom(c_f(x_0_0), c_c) <= True
      r_id_custom_x_custom(c_f(x_0_0), c_f(x_1_0)) <= r_id_custom_x_custom(x_0_0, x_1_0)
      r_id_custom_x_custom(c_f(x_0_0), c_g(x_1_0, x_1_1)) <= True
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_a) <= True
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_b) <= True
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_c) <= True
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_f(x_1_0)) <= True
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_g(x_1_0, x_1_1)) <= r_id_custom_x_custom(x_0_0, x_1_0)
      r_id_custom_x_custom(c_g(x_0_0, x_0_1), c_g(x_1_0, x_1_1)) <= r_id_custom_x_custom(x_0_1, x_1_1)
      r_id_elt_tree_x_elt_tree(leaf, node(x_1_0, x_1_1, x_1_2)) <= True
      r_id_elt_tree_x_elt_tree(node(x_0_0, x_0_1, x_0_2), leaf) <= True
      r_id_elt_tree_x_elt_tree(node(x_0_0, x_0_1, x_0_2), node(x_1_0, x_1_1, x_1_2)) <= r_id_elt_tree_x_elt_tree(x_0_0, x_1_0)
      r_id_elt_tree_x_elt_tree(node(x_0_0, x_0_1, x_0_2), node(x_1_0, x_1_1, x_1_2)) <= r_id_elt_tree_x_elt_tree(x_0_2, x_1_2)
      r_id_elt_tree_x_elt_tree(node(x_0_0, x_0_1, x_0_2), node(x_1_0, x_1_1, x_1_2)) <= r_id_elt_x_elt(x_0_1, x_1_1)
      r_id_elt_x_elt(a, b) <= True
      r_id_elt_x_elt(b, a) <= True
      r_id_nat_x_nat(s(x_0_0), s(x_1_0)) <= r_id_nat_x_nat(x_0_0, x_1_0)
      r_id_nat_x_nat(s(x_0_0), z) <= True
      r_id_nat_x_nat(z, s(x_1_0)) <= True
      r_id_natlist_x_natlist(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0)
      r_id_natlist_x_natlist(cons(x_0_0, x_0_1), cons(x_1_0, x_1_1)) <= r_id_natlist_x_natlist(x_0_1, x_1_1)
      r_id_natlist_x_natlist(cons(x_0_0, x_0_1), nil) <= True
      r_id_natlist_x_natlist(nil, cons(x_1_0, x_1_1)) <= True
      r_id_nattsil_x_nattsil(lin, snoc(x_1_0, x_1_1)) <= True
      r_id_nattsil_x_nattsil(snoc(x_0_0, x_0_1), lin) <= True
      r_id_nattsil_x_nattsil(snoc(x_0_0, x_0_1), snoc(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_1, x_1_1)
      r_id_nattsil_x_nattsil(snoc(x_0_0, x_0_1), snoc(x_1_0, x_1_1)) <= r_id_nattsil_x_nattsil(x_0_0, x_1_0)
      r_id_pair_nat_nat_x_pair_nat_nat(pair(x_0_0, x_0_1), pair(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_0, x_1_0)
      r_id_pair_nat_nat_x_pair_nat_nat(pair(x_0_0, x_0_1), pair(x_1_0, x_1_1)) <= r_id_nat_x_nat(x_0_1, x_1_1)
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
