open Tree_tuple_formalisms
open Database_for_tests_shocs
open Term.Aliases
module Set_shocs = Misc.My_set.Make2 (Shocs)

let lemma_complemented_automaton_xor_automaton () =
  Set_pointed_shocs.for_all
    (Testing_lemmas.complement_recognize_complement_language
       Term.Database_for_tests.testing_type_env Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_shocs.some_shocs

let%test "complemented_automaton_xor_automaton" = lemma_complemented_automaton_xor_automaton ()

let lemma_doubly_complemented_automaton () =
  Set_pointed_shocs.for_all
    (Testing_lemmas.doubly_complement_recognize_same_language
       Term.Database_for_tests.testing_type_env Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_shocs.some_shocs

let%test "doubly_complemented_automaton" = lemma_doubly_complemented_automaton ()

let lemma_reachable_automaton_accepts_the_same_terms () =
  Set_pointed_shocs.for_all
    (Testing_lemmas.minimized_recognize_same_language
       Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_shocs.some_shocs

let%test "reachable_automaton_accepts_the_same_terms" =
  lemma_reachable_automaton_accepts_the_same_terms ()

let lemma_nothing_in_a_type_recognizes_nothing () =
  Set_datatypes.for_all
    (fun datatypes ->
      Testing_lemmas.nothing_in_a_type_recognizes_nothing datatypes
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_tuple_of_datatypes

let%test "nothing_in_a_type_recognizes_nothing" = lemma_nothing_in_a_type_recognizes_nothing ()

let lemma_all_in_a_type_recognizes_everything () =
  Set_datatypes.for_all
    (fun datatypes ->
      Testing_lemmas.all_in_a_type_recognize_all_of_this_datatype
        Term.Database_for_tests.testing_type_env datatypes
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_tuple_of_datatypes

let%test "all_in_a_type_recognizes_nothing" = lemma_all_in_a_type_recognizes_everything ()

let lemma_identity_in_a_type_recognizes_diagonal () =
  Set_datatype.for_all
    (fun datatype ->
      Testing_lemmas.identity_in_a_type_recognizes_only_diagonal_of_this_datatype
        Term.Database_for_tests.testing_type_env datatype
        Database_for_tests_tests.many_tuples_of_patterns)
    Database_for_tests_tests.many_datatype

let%test "identity_in_a_type_recognizes_diagonal" = lemma_identity_in_a_type_recognizes_diagonal ()

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
