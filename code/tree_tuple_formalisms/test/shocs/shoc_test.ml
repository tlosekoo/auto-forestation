open Tree_tuple_formalisms
open Term.Aliases
open Term.Database_for_tests
(* open Aliases *)

let%test "typing success" =
  let rrc =
    Shoc.create_from_list relation_l_n cons_s
      [
        Formal_atom.{predicate = relation_n; projectors = [Database_for_tests_shocs.pi_00]};
        Formal_atom.
          {
            predicate = relation_l_n;
            projectors = [Database_for_tests_shocs.pi_01; Database_for_tests_shocs.pi_10];
          };
      ]
  in
  Shoc.typecheck rrc

let%test "typing failure (number of variables in a body atom)" =
  let rrc =
    Shoc.create_from_list relation_l_n [cons_symbol; s_symbol]
      [
        Formal_atom.
          {
            predicate = relation_n;
            projectors = [Database_for_tests_shocs.pi_00; Database_for_tests_shocs.pi_00];
          };
        Formal_atom.
          {
            predicate = relation_l_n;
            projectors = [Database_for_tests_shocs.pi_01; Database_for_tests_shocs.pi_10];
          };
      ]
  in
  not (Shoc.typecheck rrc)

let%test "typing failure (type of subsets)" =
  let rrc =
    Shoc.create_from_list relation_l_n [cons_symbol; s_symbol]
      [
        Formal_atom.{predicate = relation_n; projectors = [Database_for_tests_shocs.pi_01]};
        Formal_atom.
          {
            predicate = relation_l_n;
            projectors = [Database_for_tests_shocs.pi_01; Database_for_tests_shocs.pi_10];
          };
      ]
  in
  not (Shoc.typecheck rrc)

let%test "typing failure (type of goal)" =
  let rrc =
    Shoc.create_from_list relation_l [cons_symbol; s_symbol]
      [
        Formal_atom.{predicate = relation_n; projectors = [Database_for_tests_shocs.pi_00]};
        Formal_atom.
          {
            predicate = relation_l_n;
            projectors = [Database_for_tests_shocs.pi_01; Database_for_tests_shocs.pi_10];
          };
      ]
  in
  not (Shoc.typecheck rrc)

let%expect_test "pp" =
  Shoc.pp Format.std_formatter Database_for_tests_shocs.rule_nil_and_z_in__r_l_n;
  [%expect "r_l_n(nil, z) <= True"]

(*


let%test "rules matching 1" =
  Transition.matches_state Database_for_tests_shocs.qe_n Database_for_tests_shocs.rule_s_qo_in__qe_n

let%test "rules matching 2" =
  not (Transition.matches_state Database_for_tests_shocs.qo_n Database_for_tests_shocs.rule_s_qo_in__qe_n)

let%test "rules matching 3" =
  not
    (Transition.matches_convoluted_term Term.Database_for_tests_shocs.rct_consznil_sz
       Database_for_tests_shocs.rule_s_qo_in__qe_n)

let%test "rules matching 4" =
  Transition.matches_convoluted_term Term.Database_for_tests_shocs.rct_consznil_sz
    Database_for_tests_shocs.rule_cons_and_s_of_qn_qln_in__q_l_n

let%test "rules matching 5" =
  not
    (Transition.matches_convoluted_term Term.Database_for_tests_shocs.rct_nil_z
       Database_for_tests_shocs.rule_cons_and_s_of_qn_qln_in__q_l_n)

   let%test "contains_only_states_from 1" =
     Transition.contains_only_states_from
       (Set_state.of_list [Database_for_tests_shocs.qo_n; Database_for_tests_shocs.qe_n])
       Database_for_tests_shocs.rule_s_qe_in__qo_n

   let%test "contains_only_states_from 2" =
     not
       (Transition.contains_only_states_from
          (Set_state.of_list [Database_for_tests_shocs.qe_n])
          Database_for_tests_shocs.rule_s_qe_in__qo_n)

   let%test "contains_only_states_from 3" =
     Transition.contains_only_states_from
       (Set_state.of_list [Database_for_tests_shocs.qe_n; Database_for_tests_shocs.qo_n; Database_for_tests_shocs.q_l_l])
       Database_for_tests_shocs.rule_s_qe_in__qo_n


       *)

let%test "is_rule_matching_constraint_symbols 1" =
  not
    (Shoc.matches_partial_symbols [Some s_symbol; None]
       Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n)

let%test "is_rule_matching_constraint_symbols 2" =
  Shoc.matches_partial_symbols [Some cons_symbol; None]
    Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n

let%test "is_rule_matching_constraint_symbols 3" =
  not
    (Shoc.matches_partial_symbols [Some cons_symbol]
       Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n)

let%test "is_rule_matching_constraint_symbols 3.5" =
  not
    (Shoc.matches_partial_symbols [Some cons_symbol; None; None]
       Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n)

let%test "is_rule_matching_constraint_symbols 4" =
  not (Shoc.matches_partial_symbols [None; None] Database_for_tests_shocs.rule_nil_in__r_l)

let%test "is_rule_matching_constraint_symbols 5" =
  Shoc.matches_partial_symbols [Some cons_symbol; Some s_symbol]
    Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n

let%test "is_rule_matching_constraint_symbols 6" =
  Shoc.matches_partial_symbols [None; Some s_symbol]
    Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n

let%test "get_symbol" =
  Shoc.get_functions_symbols Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n
  = [cons_symbol; s_symbol]

let%test "get towards" =
  Shoc.get_head_relation Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n = relation_l_n

let%test "extract_states_from_rules 1" =
  let extracted_states =
    Shoc.extract_predicate_symbols Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n
  in
  let expected_states = Set_typed_relation_symbol.of_list [relation_n; relation_l_n] in
  Set_typed_relation_symbol.equal extracted_states expected_states

let%test "extract_states_from_rules 2" =
  let extracted_states =
    Shoc.extract_predicate_symbols Database_for_tests_shocs.rule_s_re_in__ro_n
  in
  let expected_states = Set_typed_relation_symbol.of_list [relation_even; relation_odd] in
  Set_typed_relation_symbol.equal extracted_states expected_states

let%test "extract_states_from_rules 3" =
  let extracted_states =
    List.map Shoc.extract_predicate_symbols
      [
        Database_for_tests_shocs.rule_s_re_in__ro_n;
        Database_for_tests_shocs.rule_cons_and_s_of_rn_rln_in__r_l_n;
      ]
    |> Set_typed_relation_symbol.list_union
  in
  let expected_states =
    Set_typed_relation_symbol.of_list [relation_n; relation_l_n; relation_even; relation_odd]
  in

  Set_typed_relation_symbol.equal extracted_states expected_states
(* let%test "extract alphabet " =
   let alphabet =
     Transition.extract_states Database_for_tests_shocs.rule_cons_and_s_of_qnnatpad_qlnatlistnat_in_qlnatlistnat
   in
   let expected_alphabet =
     Term.Alphabet.Ranked_alphabet.of_list
       [Term.Database_for_tests_shocs.cons_symbol; Term.Database_for_tests_shocs.s_symbol]
   in
   Term.Alphabet.Ranked_alphabet.equal alphabet expected_alphabet *)

(*

   let partity_reversing_map =
     State_renaming.to_function_with_not_mapped_is_identity
       Database_for_tests_shocs.state_substitution_reverse_parity

   let partity_forgetting_map =
     State_renaming.to_function_with_not_mapped_is_identity
       Database_for_tests_shocs.state_substitution_forget_parity

   let%test "state map 1 " =
     Transition.state_map partity_reversing_map Database_for_tests_shocs.rule_s_qe_in__qo_n
     = Database_for_tests_shocs.rule_s_qo_in__qe_n

   let%test "state map 1 " =
     Transition.state_map partity_forgetting_map Database_for_tests_shocs.rule_s_qe_in__qo_n
     = Database_for_tests_shocs.rule_s_qn_in__q_n

   let%test "state map 3" =
     try
       let wrong_type_map state =
         if state = Database_for_tests_shocs.q_n then
           Database_for_tests_shocs.qo_n
         else if state = Database_for_tests_shocs.qo_n then
           Database_for_tests_shocs.q_l
         else
           Database_for_tests_shocs.q_l
       in

       let _ = Transition.state_map wrong_type_map Database_for_tests_shocs.rule_s_qe_in__qo_n in
       false
     with
     | _ -> true

   let%test "arity" =
     let zz_symbol =
       Term.Typed_functions.create [Database_for_tests_shocs.z_symbol; Database_for_tests_shocs.z_symbol]
     in
     Term.Typed_functions.get_arity Term.Convolution.Complete zz_symbol = 0

   let%test "arity of rule" =
     let zz_symbol =
       Term.Typed_functions.create [Database_for_tests_shocs.z_symbol; Database_for_tests_shocs.z_symbol]
     in
     let r = Transition.create zz_symbol [] Database_for_tests_shocs.q_n_n Term.Convolution.Complete in
     Transition.get_arity_of_rule_symbol r = 0
     
*)

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
