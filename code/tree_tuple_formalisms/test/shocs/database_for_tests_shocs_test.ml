open Term
open Tree_tuple_formalisms
open Database_for_tests_shocs
open Term.Database_for_tests
(* open Aliases *)

(* Open Term.D *)
(* let many_convoluted_terms = Database_for_tests.many_convoluted_terms ()
   let many_rct = Database_for_tests.many_rct () *)

(* let many_tuple_of_patterns = Database_for_tests.build_every_tuple_of_patterns_term_of_small_depth () *)
let%test "datatypes are correct 1" = Database_for_tests.c_nat = [Database_for_tests.nat]
let%test "datatypes are correct 2" = Database_for_tests.c_natlist = [Database_for_tests.natlist]

let%test "datatypes are correct 3" =
  Database_for_tests.c_natlist_nat = [Database_for_tests.natlist; Database_for_tests.nat]

(* let%test "states are correct 1" =
   .get_name q_n = "q_n" && Typed_relation_symbol.get_type q_n = Database_for_tests.c_nat *)

let%test "states are correct 2" =
  Typed_relation_symbol.to_string relation_n_n = "r_n_n"
  && Typed_relation_symbol.get_types relation_n_n = Database_for_tests.c_nat_nat

let%test "states are correct 3" =
  Typed_relation_symbol.to_string relation_n_l = "r_n_l"
  && Typed_relation_symbol.get_types relation_n_l = Database_for_tests.c_nat_natlist

let%test "states are correct 4" =
  Typed_relation_symbol.to_string relation_l_n = "r_l_n"
  && Typed_relation_symbol.get_types relation_l_n = Database_for_tests.c_natlist_nat

let%test "states are correct 5" =
  Typed_relation_symbol.to_string relation_ = "r" && Typed_relation_symbol.get_types relation_ = []

let%test "states are correct 6" =
  Typed_relation_symbol.to_string relation_l_l = "r_l_l"
  && Typed_relation_symbol.get_types relation_l_l = Database_for_tests.c_natlist_natlist

let%test "states are correct 8" =
  Typed_relation_symbol.to_string relation_even = "even"
  && Typed_relation_symbol.get_types relation_even = Database_for_tests.c_nat

let%test "states are correct 9" =
  Typed_relation_symbol.to_string relation_odd = "odd"
  && Typed_relation_symbol.get_types relation_odd = Database_for_tests.c_nat

let%expect_test "pp a_nat_1" =
  Pointed_shocs.pp Format.std_formatter a_nat_1;
  [%expect "
       ({
         r_n(s(x_0_0)) <= r_n(x_0_0)
         r_n(z) <= True
       }, r_n)"]

let%expect_test "pp a_natlist_1" =
  Pointed_shocs.pp Format.std_formatter a_natlist_1;
  [%expect
    "
       ({
         r_l(cons(x_0_0, x_0_1)) <= r_l(x_0_1) /\\ r_n(x_0_0)
         r_l(nil) <= True
         r_n(s(x_0_0)) <= r_n(x_0_0)
         r_n(z) <= True
       }, r_l)"]

let%expect_test "pp a_even" =
  Pointed_shocs.pp Format.std_formatter a_even;
  [%expect
    "
({
  even(s(x_0_0)) <= odd(x_0_0)
  even(z) <= True
  odd(s(x_0_0)) <= even(x_0_0)
}, even)"]

let%expect_test "pp a_odd" =
  Pointed_shocs.pp Format.std_formatter a_odd;
  [%expect
    "
    ({
      even(s(x_0_0)) <= odd(x_0_0)
      even(z) <= True
      odd(s(x_0_0)) <= even(x_0_0)
    }, odd)"]

let%expect_test "pp a_list_size" =
  Pointed_shocs.pp Format.std_formatter a_list_size;
  [%expect
    "
        ({
          r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
          r_l_n(nil, z) <= True
          r_n(s(x_0_0)) <= r_n(x_0_0)
          r_n(z) <= True
        }, r_l_n)"]

let%expect_test "pp a_list_of_z_and_their_size" =
  Pointed_shocs.pp Format.std_formatter a_lists_of_z_and_their_size;
  [%expect
    "
            ({
              r_l_n(cons(x_0_0, x_0_1), s(x_1_0)) <= r_l_n(x_0_1, x_1_0) /\\ r_n(x_0_0)
              r_l_n(nil, z) <= True
              r_n(z) <= True
            }, r_l_n)"]

let%expect_test "shocs height" =
  Shocs.pp Format.std_formatter shocs_height_and_shallower_and_le;
  [%expect
    "
    {
      height(leaf, z) <= True
      height(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= height(x_0_0, x_1_0) /\\ shallower(x_0_2, x_1_0)
      height(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= height(x_0_2, x_1_0) /\\ shallower(x_0_0, x_1_0)
      le(s(x_0_0), s(x_1_0)) <= le(x_0_0, x_1_0)
      le(z, s(x_1_0)) <= True
      shallower(leaf, s(x_1_0)) <= True
      shallower(leaf, z) <= True
      shallower(node(x_0_0, x_0_1, x_0_2), s(x_1_0)) <= shallower(x_0_0, x_1_0) /\\ shallower(x_0_2, x_1_0)
    }"]

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
