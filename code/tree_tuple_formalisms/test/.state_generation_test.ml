open Term
open Tree_tuple_formalisms

let state_generation_is_deterministic (generator : unit -> Typed_relation_symbol.t) : bool =
  let s1 = generator () in
  let s2 = generator () in
  s1 = s2

let%test "state generation" =
  let generator () = State_generator.generate_a_fresh_state Term.Database_for_tests.c_nat_nat in
  not (state_generation_is_deterministic generator)

let%test "state generation 1" =
  let generator () =
    State_generator.create_canonical_state_for_completing_automaton
      Term.Database_for_tests.c_nat_nat
  in
  state_generation_is_deterministic generator

let%test "state generation 2" =
  let generator () =
    State_generator.create_canonical_state_for_identity_automaton Term.Database_for_tests.c_nat_nat
  in
  state_generation_is_deterministic generator

let%test "state generation 3" =
  let generator () =
    (State_generator.create_canonical_state_from_datatypes ~prefix:(Some "hey"))
      Term.Database_for_tests.c_nat_nat
  in
  state_generation_is_deterministic generator

let%test "ok" =
  let _ = Printing.pp_ok () in
  true
