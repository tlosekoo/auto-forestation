let create_canonical_relation_for_completing_automaton =
  Clause.Relation_generator.create_canonical_relation_from_datatypes ~prefix:(Some "comp")

let generate_a_fresh_relation =
  Term.Symbol_generator.generate_fresh_typed_relation_symbol ~prefix:"q_gen_"
