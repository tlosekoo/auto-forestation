open Term.Aliases
open Term

(* aliases *)
module Set_rule = Misc.My_set.Make (Transition)
module Set_int = Misc.My_set.Make (Misc.Int)

type t = {
  rules : Set_rule.t;
  convolution_form : Convolution.t;
}
[@@deriving compare, equal]

let get_rules a = a.rules
let get_convolution_form (a : t) : Convolution.t = a.convolution_form

let extract_defined_states a : Set_typed_relation_symbol.t =
  Set_rule.fold
    (fun r -> Set_typed_relation_symbol.union (Transition.extract_states r))
    (get_rules a) Set_typed_relation_symbol.empty

let readable_order_for_rules (r1 : Transition.t) (r2 : Transition.t) : int =
  let t1 = Transition.get_towards r1 in
  let t2 = Transition.get_towards r2 in
  let tt1 = Typed_relation_symbol.get_types t1 in
  let tt2 = Typed_relation_symbol.get_types t2 in
  let arity1 = List.length tt1 in
  let arity2 = List.length tt2 in
  let cmp1 = Int.compare arity1 arity2 in
  if cmp1 = 0 then
    let cmp2 = Typed_relation_symbol.compare t1 t2 in
    if cmp2 = 0 then
      Transition.compare r1 r2
    else
      cmp2
  else
    cmp1

let pp (c : Format.formatter) (a : t) : unit =
  let sorted_transitions =
    a |> get_rules |> Set_rule.to_list |> List.sort readable_order_for_rules
  in
  Format.fprintf c "\n{{{\nQ=%a,\nDelta=%a\nConvolution form: %a\n}}}\n"
    Set_typed_relation_symbol.pp (extract_defined_states a)
    (Printing.pp_list_sep_surrounded "\n" "\n{\n" "\n}\n" Transition.pp)
    sorted_transitions Convolution.pp (get_convolution_form a)

(* let get_datatype (a : t) = a.datatype *)

(* let get_arity (a : t) : int =
   let datatype_automaton = get_type_of_automaton a in
   let arity = List.length datatype_automaton in
   arity *)

(* let get_arity (a : t) : int = get_arity a *)

let typecheck (rules : Set_rule.t) (cf : Convolution.t) : unit =
  try
    let every_rule_has_the_same_convolution_form =
      Set_rule.for_all
        (fun rule -> rule |> Transition.get_convolution_form |> Convolution.equal cf)
        rules
    in
    if not every_rule_has_the_same_convolution_form then
      raise (Invalid_argument "For now, every rule must have the same convolution")
  with
  | Invalid_argument e ->
      let error_message =
        Format.flush_str_formatter
          (Format.fprintf Format.str_formatter "Cannot create automaton: %s" e)
      in
      raise (Invalid_argument error_message)

(* creates a new automaton from a final state list and rules *)
let create (rules : Set_rule.t) (convolution_form : Convolution.t) : t =
  let () = typecheck rules convolution_form in
  {rules; convolution_form}
