open Term
open Term.Aliases
open Tree_automaton_core
module Set_rule = Misc.My_set.Make (Transition)

let empty_with_custom_convolution (cf : Convolution.t) (datatype : Datatypes.t) : t =
  create Set_typed_relation_symbol.empty Set_rule.empty datatype cf

(* beurk *)
let none_in_a_type = empty_with_custom_convolution Convolution.Left

(* TODO: Not do this *)
let arbitrary_convolution_used_by_default = Convolution.Left

(* let create_rule_from_symbol (symbol : Typed_function.t) : Transition.t =
   let output_datatype = Typed_function.get_output_type symbol in
   let input_datatypes = Typed_function.get_input_types symbol in
   let output_state = create_canonical_state_from_datatype output_datatype in
   let input_states = List.map create_canonical_state_from_datatype input_datatypes in
   let symbol = [symbol] in
   let rule = Transition.create symbol arbitrary_convolution_used_by_default input_states output_state in
   rule *)

(* let create_automaton_name_from_datatype (datatype : Datatype.t) : string =
   let datatype_str = Datatype.get_string_representation datatype in
   let automaton_name = "a_" ^ datatype_str in
   automaton_name *)

let get_children_datatypes_of_convoluted_datatype
    (datatype_env : Datatype_environment.t)
    (convolution_form : Convolution.t)
    (cd : Datatypes.t) : Set_datatypes.t =
  let alphabet_of_this_type =
    Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
      datatype_env cd
  in
  let children_datatype_of_these_convoluted_symbols =
    Set_typed_functions.fold
      (fun cs ->
        Set_datatypes.union
          (cs |> Typed_functions.get_inputs_types
          |> Convolution.convolute_generic convolution_form
          |> Set_datatypes.of_list))
      alphabet_of_this_type Set_datatypes.empty
  in
  children_datatype_of_these_convoluted_symbols

(* not optimized at all.. *)
let get_all_types_needed_for_some_convoluted_type
    (datatype_env : Datatype_environment.t)
    (convolution_form : Convolution.t)
    (cd : Datatypes.t) : Set_datatypes.t =
  let children_of_one_datatype_getter =
    get_children_datatypes_of_convoluted_datatype datatype_env convolution_form
  in
  let accumulator_function convoluted_datatypes =
    let new_datatypes =
      Set_datatypes.fold
        (fun cd -> Set_datatypes.union (children_of_one_datatype_getter cd))
        convoluted_datatypes Set_datatypes.empty
    in
    Set_datatypes.union convoluted_datatypes new_datatypes
  in
  let from = Set_datatypes.singleton cd in
  let every_needed_datatype =
    Misc.Others.fixpoint_from Set_datatypes.equal accumulator_function from
  in

  every_needed_datatype

let all_in_a_type (datatype_env : Datatype_environment.t) (datatypes : Datatypes.t) :
    Tree_automaton_core.t =
  let default_convolution = Convolution.Left in
  let datatypes_needed_for_this_type =
    get_all_types_needed_for_some_convoluted_type datatype_env default_convolution datatypes
  in
  let datatypes_list = datatypes_needed_for_this_type |> Set_datatypes.to_list in
  let functions =
    datatypes_list
    |> List.map
         (Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
            datatype_env)
    |> List.map Set_typed_functions.to_list
    |> List.flatten
  in
  let function_to_rule (functions : Typed_functions.t) =
    let output_datatypes = Typed_functions.get_output_types functions in
    let convoluted_input_datatypes =
      Convolution.convolute_generic default_convolution (Typed_functions.get_inputs_types functions)
    in
    let out_state = State_generator.create_canonical_state_from_datatypes output_datatypes in
    let in_states =
      List.map State_generator.create_canonical_state_from_datatypes convoluted_input_datatypes
    in
    let rule = Transition.create functions default_convolution in_states out_state in
    rule
  in
  let rules = functions |> List.map function_to_rule |> Set_rule.of_list in
  let final_states =
    Set_typed_relation_symbol.singleton
      (State_generator.create_canonical_state_from_datatypes datatypes)
  in
  Tree_automaton_core.create final_states rules datatypes default_convolution

(*
let all_in_a_type (datatype_env : Datatype_environment.t) (datatypes : Datatypes.t) :
    Tree_automaton_core.t =
   let all_types_for_this_automaton =
    Datatype_environment.get_type_dependencies datatype_env datatype
  in
  let all_symbols_for_this_automaton =
    Set_datatype.fold
      (fun d ->
        Set_typed_function.union (Datatype_environment.get_constructors_of_datatype datatype_env d))
      all_types_for_this_automaton Set_typed_function.empty
  in
  let final_state = create_canonical_state_from_datatype datatype in
  let rules =
    Set_typed_function.fold
      (fun rs -> Set_rule.add (create_rule_from_symbol rs))
      all_symbols_for_this_automaton Set_rule.empty
  in
  (* let automaton_name = create_automaton_name_from_datatype datatype in *)
  (* not beautiful, TODO *)
  let automaton =
    Tree_automaton_core.create
      (Set_typed_relation_symbol.singleton final_state)
      rules
      (Typed_relation_symbol.get_types final_state)
      arbitrary_convolution_used_by_default
  in
  automaton *)

(* open Tree_automata *)

open Aliases

(* open Model_checking.Aliases *)

let create_identity_rule (cf : Convolution.t) (ts : Typed_function.t) : Transition.t =
  let root_sybmol = [ts; ts] in
  let ts_output_datatype = Typed_function.get_output_type ts in
  let ts_input_datatype = Typed_function.get_input_types ts in
  let goal_state = State_generator.create_state_for_identity ts_output_datatype in
  let substates = List.map State_generator.create_state_for_identity ts_input_datatype in
  let rule = Transition.create root_sybmol cf substates goal_state in
  rule

let create_identity_rules (env : Datatype_environment.t) (cf : Convolution.t) (dt : Datatype.t) :
    Set_rule.t =
  (* let () = Format.fprintf Format.std_formatter "Creating identity rule for %a in environment %a.\n" Datatype.pp dt Datatype_environment.pp_complete env in  *)
  let constructors = Datatype_environment.get_constructors_of_datatype env dt in
  Set_typed_function.fold
    (fun rs -> Set_rule.add (create_identity_rule cf rs))
    constructors Set_rule.empty

let identity_in_a_type (env : Datatype_environment.t) (dt : Datatype.t) : Tree_automaton_core.t =
  let cf = arbitrary_convolution_used_by_default in
  let datatypes = Datatype_environment.get_type_dependencies env dt in
  let rules =
    Set_datatype.fold
      (fun dt -> Set_rule.union (create_identity_rules env cf dt))
      datatypes Set_rule.empty
  in
  let automaton_datatype = [dt; dt] in
  let final_states =
    Set_typed_relation_symbol.singleton (State_generator.create_state_for_identity dt)
  in
  let id_automaton = Tree_automaton_core.create final_states rules automaton_datatype cf in
  id_automaton
