(* open Term.Typed_functions *)

include Misc.Pair.Make (Term.Typed_functions) (Convolution)

let get_convolution_form = snd
let get_functions = fst

let get_convoluted_input_types ((functions, convolution_form) : t) : Term.Datatypes.t list =
  functions |> Term.Typed_functions.get_inputs_types
  |> Convolution.convolute_generic convolution_form

(* Warning: inefficient *)
let get_arity (convoluted_symbols : t) : int =
  List.length (get_convoluted_input_types convoluted_symbols)

(* Use this instead? *)
(*
  let get_arity (convolution_form : Convolution.t) (cs : t) : int =
  match convolution_form with
  | Right
  | Left ->
      Misc.Min_and_max.max_pilist (List.map Typed_function.get_arity cs)
  | Complete ->
      if List.for_all (fun ts -> Typed_function.get_arity ts = 0) cs then
        0
      else
        List.fold_left (fun acc symbol -> acc * Int.max (Typed_function.get_arity symbol) 1) 1 cs
        *)
