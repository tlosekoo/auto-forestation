(* open Term.Aliases *)

(** This module implements convoluted tree automata. *)

(** An automaton is the data of its final states, transitions, and datatype it recognizes.
    For now, every rule of the automaton must have the same convolution.
   	The set of final states must all be of the same datatype as the automaton.
   *)
type t

include Tree_tuple_formalism.S with type t := t

(** Given a set of final states, a set of rules, the automaton datatype, and the automaton convolution form, creates an automaton.
   	If this automaton does not satisfy the typecheking criteria, an 'Illegal_argument' exception is raised *)
val create : Misc.My_set.Make(Transition).t -> Convolution.t -> t

(** Returns the set of rules of the automaton *)
val get_rules : t -> Misc.My_set.Make(Transition).t

(** Returns the set of final/accepting states of the automaton *)
(* val get_final_states : t -> Set_typed_relation_symbol.t *)

(** Returns the type of any final state (they share the same type) *)
val get_type_of_automaton : t -> Term.Datatypes.t

(** Returns the form of convolution used in automaton *)
val get_convolution_form : t -> Convolution.t

(* val extract_defined_states : t -> Set_typed_relation_symbol.t *)
