open Term
open Term.Aliases

type t = {
  convoluted_functions : Convoluted_typed_functions.t;
  sub_states : Typed_relation_symbol.t List.t;
  towards : Typed_relation_symbol.t;
}
[@@deriving compare, equal]

let get_functions {convoluted_functions; _} =
  Convoluted_typed_functions.get_functions convoluted_functions

let get_convoluted_functions {convoluted_functions; _} = convoluted_functions
let get_substates {sub_states; _} = sub_states
let get_towards {towards; _} = towards

let get_convolution_form {convoluted_functions; _} =
  Convoluted_typed_functions.get_convolution_form convoluted_functions

let pp_parameterized ~(complete : bool) (c : Format.formatter) (r : t) : unit =
  let pp_symbol = Typed_functions.pp_parameterized ~complete in
  let pp_state = Typed_relation_symbol.pp_parameterized ~complete in
  let pp_convolution_form out cf =
    if complete then Format.fprintf out " (%a)" Convolution.pp cf else ()
  in
  Format.fprintf c "%a(%a) -> %a%a" pp_symbol (get_functions r) (Printing.pp_list pp_state)
    (get_substates r) pp_state (get_towards r) pp_convolution_form (get_convolution_form r)

let pp (c : Format.formatter) (r : t) : unit = pp_parameterized ~complete:false c r

(* TODO: CONVOLUTE INPUTS?*)
let typecheck
    (cs : Typed_functions.t)
    (substates : Typed_relation_symbol.t list)
    (towards : Typed_relation_symbol.t)
    (convolution_form : Convolution.t) : unit =
  let input_convoluted_types_of_symbols_non_convoluted = Typed_functions.get_inputs_types cs in
  let input_convoluted_types_of_symbols =
    Convolution.convolute_generic convolution_form input_convoluted_types_of_symbols_non_convoluted
  in
  let output_convoluted_type_of_symbols = Typed_functions.get_output_types cs in

  let datatypes_of_substates = List.map Typed_relation_symbol.get_types substates in
  let goal_state_convoluted_datatype = Typed_relation_symbol.get_types towards in

  let output_types_are_matching =
    Datatypes.equal output_convoluted_type_of_symbols goal_state_convoluted_datatype
  in
  let input_types_are_matching =
    List.length datatypes_of_substates = List.length input_convoluted_types_of_symbols
    && List.for_all2 Datatypes.equal datatypes_of_substates input_convoluted_types_of_symbols
  in

  let types_are_matching = output_types_are_matching && input_types_are_matching in

  if not types_are_matching then
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Typecheck error in rule creation: (Typed_functions, substates, goal_state, convolution) = (%a, %a, %a, %a)\n Input types are matching:%b 
            Output types are matching: %b\n Input types symbols: %a    Substates types:%a \n"
           Typed_functions.pp cs
           (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
           substates Typed_relation_symbol.pp towards Convolution.pp convolution_form
           input_types_are_matching output_types_are_matching
           (Printing.pp_list_and_brackets Datatypes.pp)
           input_convoluted_types_of_symbols
           (Printing.pp_list_and_brackets Datatypes.pp)
           datatypes_of_substates)
    in
    raise (Invalid_argument error_message)
(* Format.fprintf Format.std_formatter "\n\n input symbols type:\n %a \n input states types:\n %a\n\n" (Printing.pp_list_sep (Printing.pp_list Datatype.pp) " +++ ") types_input_symbol  *)
(* (Printing.pp_list_sep (Printing.pp_list Datatype.pp) " --- ") types_input_states ;  *)
(* Format.fprintf Format.std_formatter "\nmatching: %b \n" types_are_matching ; *)

let create
    (functions : Typed_functions.t)
    (convolution_form : Convolution.t)
    (sub_states : Typed_relation_symbol.t list)
    (towards : Typed_relation_symbol.t) : t =
  (* Format.fprintf Format.std_formatter "\n\nCreating a rule with symbol %a and substates %a\n" Typed_functions.pp symbol (Printing.pp_list Typed_relation_symbol.pp) sub_states ; *)
  let () = typecheck functions sub_states towards convolution_form in
  let convoluted_functions = (functions, convolution_form) in
  {convoluted_functions; sub_states; towards}

let state_map (f : Typed_relation_symbol.t -> Typed_relation_symbol.t) (r : t) : t =
  let functions = get_functions r in
  let new_towards = f (get_towards r) in
  let new_substates = List.map f (get_substates r) in
  let new_convolution_form = get_convolution_form r in
  let new_rule = create functions new_convolution_form new_substates new_towards in
  new_rule

(*   *)
(*   *)
(* Some filtering that will help solving *)
let matches_state (state : Typed_relation_symbol.t) (rule : t) : bool =
  Typed_relation_symbol.equal (get_towards rule) state

let matches_patterns (ct : Patterns.t) (rule : t) : bool =
  Typed_functions.equal (Option.get (Patterns.get_root_symbols ct)) (get_functions rule)

(*
   let filter_rules_matching_goal_state (goal_state : Typed_relation_symbol.t) (rules : t list) : t list =
     List.filter (matches_state goal_state) rules

   let filter_rules_matching_Typed_functions (cs : Typed_functions.t) (rules : t list) : t list =
     List.filter (matches_symbol cs) rules 
     *)

let matches_constraint_symbols (symbols_opt : Typed_function.t option list) (rule : t) : bool =
  let rule_symbols = get_functions rule in
  if List.length rule_symbols = List.length symbols_opt then
    List.for_all2
      (fun sym_rule sym_constraint_opt ->
        Option.fold ~none:true ~some:(Typed_function.equal sym_rule) sym_constraint_opt)
      rule_symbols symbols_opt
  else
    false

let get_arity_of_rule_symbol (r : t) = Convoluted_typed_functions.get_arity (get_convoluted_functions r)

let extract_states (r : t) : Set_typed_relation_symbol.t =
  Set_typed_relation_symbol.of_list (get_towards r :: get_substates r)

let contains_only_states_from (states : Set_typed_relation_symbol.t) (rule : t) : bool =
  Set_typed_relation_symbol.subset (extract_states rule) states

let get_nb_symbols_of_rule (r : t) = List.length (get_functions r)
