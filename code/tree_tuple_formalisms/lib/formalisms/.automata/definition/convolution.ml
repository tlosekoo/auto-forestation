type t =
  | Right
  | Left
  | Complete
[@@deriving compare, equal]

let pp (c : Format.formatter) (cf : t) : unit =
  let descriptive_str =
    match cf with
    | Right -> "right"
    | Left -> "left"
    | Complete -> "complete"
  in
  Misc.String.pp c descriptive_str

let is_completely_empty (l : 'a list list) : bool = List.for_all (( = ) []) l

let rec add_line_to_list_of_lines (l : 'a list) (ll : 'a list list) : 'a list list =
  match (l, ll) with
  | [], _ -> ll
  | h :: t, hh :: tt -> (h :: hh) :: add_line_to_list_of_lines t tt
  | h :: t, [] -> [h] :: add_line_to_list_of_lines t ll

let transpose ll = List.fold_right add_line_to_list_of_lines ll []

let convolute_left_right
    ~(align_left : bool)
    (list_of_children_of_each_element_to_convolute : 'a list list) : 'a list list =
  let transpose_input =
    if align_left then
      list_of_children_of_each_element_to_convolute
    else
      List.map List.rev list_of_children_of_each_element_to_convolute
  in
  let transposed = transpose transpose_input in
  let convoluted = if align_left then transposed else List.rev transposed in
  convoluted

let complete_convolute_generic (list_of_children_of_each_element_to_convolute : 'a list list) :
    'a list list =
  (* raise (Invalid_argument "Not implemented") *)
  let children_with_padding_where_it_was_empty =
    List.map
      (fun list_children ->
        match list_children with
        | [] -> [None]
        | children -> List.map Option.some children)
      list_of_children_of_each_element_to_convolute
  in
  let convoluted = Misc.List_op.cartesian_product_list children_with_padding_where_it_was_empty in

  let convoluted_not_only_padding = List.filter (List.exists Option.is_some) convoluted in

  (* SORT!!?!! *)
  List.map Misc.List_op.filter_opt convoluted_not_only_padding

let convolute_generic (convolution_type : t) =
  match convolution_type with
  | Left -> convolute_left_right ~align_left:true
  | Right -> convolute_left_right ~align_left:false
  | Complete -> complete_convolute_generic

