open Term
include Tree_tuple_formalism.Acceptor with type t := Tree_automaton_core.t
include Tree_tuple_formalism.Minimizable with type t := Tree_automaton_core.t
include Tree_tuple_formalism.Complementable with type t := Tree_automaton_core.t

(** 'normalize a ct s' returns an automaton equivalent to 'a' except for the fact that it recognizes 'ct' in 's'.
   This operation does not preserve automaton determinism. *)
val normalize_with_new_states :
  Tree_automaton_core.t -> Patterns.t -> Typed_relation_symbol.t -> Tree_automaton_core.t

(** 'normalize a ct s' returns a pair '(s, a)' of a state and an automaton.
   	This automaton recognizes 'ct' in 's'.
   	For this, it adds transitions when necessary, reusing old ones.
   	This corresponds to the 'norm' function in [Haudebourg2020]. *)
val normalize :
  Tree_automaton_core.t -> Patterns.t -> Typed_relation_symbol.t * Tree_automaton_core.t

(** 'map_state state_map a' returns an automaton identical to 'a', except for states which has been mapped by 'state_map'. *)
val map_state :
  (Typed_relation_symbol.t -> Typed_relation_symbol.t) ->
  Tree_automaton_core.t ->
  Tree_automaton_core.t
