open Term.Aliases
open Term

(* aliases *)
module Set_rule = Misc.My_set.Make (Transition)
module Set_int = Misc.My_set.Make (Misc.Int)
open Tree_automaton_core

let add_rule (a : t) (r : Transition.t) : t =
  create (Set_rule.add r (get_rules a)) (get_convolution_form a)

let filter_rules_matching_symbol (rules : Set_rule.t) (cs : Typed_functions.t) : Set_rule.t =
  Set_rule.filter
    (fun rule -> rule |> Transition.get_functions |> Typed_functions.compare cs |> Int.equal 0)
    rules

(* Returns the set of states that allows to recognize (in 'a') the symbol 'cs', given that children states accept the states "accepts_in_children" *)
let accepts_in_step
    (a : t)
    (cs : Typed_functions.t)
    (accepts_in_children : Set_typed_relation_symbol.t list) : Set_typed_relation_symbol.t =
  let rules_matching_symbol = filter_rules_matching_symbol (get_rules a) cs in
  let rule_matches_substates rule =
    let substates_rule = Transition.get_substates rule in
    let same_arity = List.length accepts_in_children = Transition.get_arity_of_rule_symbol rule in
    same_arity
    && List.for_all2
         (fun substate accepting_states -> Set_typed_relation_symbol.mem substate accepting_states)
         substates_rule accepts_in_children
  in

  let rules_matching_substates_and_symbol =
    Set_rule.filter rule_matches_substates rules_matching_symbol
  in

  (* let () =
       Format.fprintf Format.std_formatter
         "Transitions: %a\nTransitions matching symbols: %a\n and substates: %a\n" Set_rule.pp (get_rules a)
         Set_rule.pp rules_matching_symbol Set_rule.pp rules_matching_substates_and_symbol
     in
     let () =
       Format.fprintf Format.std_formatter "Accepts in children:\n%a\n"
         (Printing.pp_list_and_brackets Set_typed_relation_symbol.pp)
         accepts_in_children
     in *)
  (* let r = Set_rule.choose rules_matching_symbol in
     let s = Transition.get_substates r in *)

  (* let () =
       Format.fprintf Format.std_formatter "Transition:\n%a\nSubstates:\n%a\narity of rule: %d\n" Transition.pp r
         (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
         s (Transition.get_arity_of_rule_symbol r)
     in *)
  let goal_state_of_these_rules =
    Set_rule.fold
      (fun r -> Set_typed_relation_symbol.add (Transition.get_towards r))
      rules_matching_substates_and_symbol Set_typed_relation_symbol.empty
  in
  goal_state_of_these_rules

(* Returns all states 'q' such that 't \in L(a, s)' *)
(* We suppose that patterns are closed *)
let rec accepts_in (a : t) (t : Patterns.t) : Set_typed_relation_symbol.t =
  let root_symbol = Option.get (Patterns.get_root_symbols t) in
  let children = List.map Pattern.get_children t in
  let convoluted_children = Convolution.convolute_generic (get_convolution_form a) children in
  let children_are_accepted_in = List.map (accepts_in a) convoluted_children in
  let accepted_in = accepts_in_step a root_symbol children_are_accepted_in in

  (* let () =
       Format.fprintf Format.std_formatter
         "
     Accepts_in with automaton \n%a\n and term %a: \nRoot symbol: %a\nChildren: %a\nChildren_are_accepted_in: %a\naccepted_in: %a\n"
         pp a Convoluted_term.pp t Typed_functions.pp root_symbol
         (Printing.pp_list_and_brackets Convoluted_term.pp)
         children
         (Printing.pp_list_and_brackets Set_typed_relation_symbol.pp)
         children_are_accepted_in Set_typed_relation_symbol.pp accepted_in
     in *)

  (* let () = Format.pp_print_flush Format.std_formatter () in *)
  accepted_in

(* Checks if 't \in L(a)' *)
let accepts (a : t) (t : Patterns.t) =
  let accepting_witness = Set_typed_relation_symbol.inter (get_final_states a) (accepts_in a t) in
  not (Set_typed_relation_symbol.is_empty accepting_witness)

let fetch_reachable_states_step (a : t) (from : Set_typed_relation_symbol.t) :
    Set_typed_relation_symbol.t =
  let reachable_states_using_only_previously_reachable_states =
    a |> get_rules |> Set_rule.to_list
    |> List.filter_map (fun r ->
           let substates_rule = Set_typed_relation_symbol.of_list (Transition.get_substates r) in
           if Set_typed_relation_symbol.subset substates_rule from then
             Some (Transition.get_towards r)
           else
             None)
    |> Set_typed_relation_symbol.of_list
  in
  reachable_states_using_only_previously_reachable_states

(* Reachable automaton *)
let minimize (a : t) : t =
  let add_reachable_state_one_step (from : Set_typed_relation_symbol.t) =
    Set_typed_relation_symbol.union (fetch_reachable_states_step a from) from
  in
  let initially_accessible_states = Set_typed_relation_symbol.empty in
  let all_reachable_states =
    Misc.Others.fixpoint_from Set_typed_relation_symbol.equal add_reachable_state_one_step
      initially_accessible_states
  in
  let all_usefull_rules =
    Set_rule.filter (Transition.contains_only_states_from all_reachable_states) (get_rules a)
  in
  let reachable_final_states =
    Set_typed_relation_symbol.inter all_reachable_states (get_final_states a)
  in
  let reachable_automaton =
    create reachable_final_states all_usefull_rules (get_type_of_automaton a)
      (get_convolution_form a)
  in

  (* let () =
     if false then
       Format.fprintf Format.std_formatter
         "\n\nOld automaton:%a\n\nReachable states:%a\n\nReduced automaton:%a\n\n" pp a
         (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
         all_reachable_states pp reachable_automaton
       in
  *)
  reachable_automaton

let completes_for_one_symbol
    (a : t)
    (existing_states : Set_typed_relation_symbol.t)
    (cs : Typed_functions.t) : Set_rule.t =
  let a_cf = get_convolution_form a in
  let rules_automaton = get_rules a in
  let rules_that_match_the_symbol =
    Set_rule.filter
      (fun rule -> rule |> Transition.get_functions |> Typed_functions.equal cs)
      rules_automaton
  in

  let output_type = Typed_functions.get_output_types cs in

  let filling_output_state =
    State_generator.create_canonical_state_for_completing_automaton output_type
  in

  let state_has_correct_type the_type state =
    state |> Typed_relation_symbol.get_types |> Datatypes.compare the_type |> Int.equal 0
  in

  let input_types_not_convoluted = Typed_functions.get_inputs_types cs in
  let input_types = Convolution.convolute_generic a_cf input_types_not_convoluted in

  let possible_input_states =
    List.map
      (fun input_type ->
        Set_typed_relation_symbol.filter (state_has_correct_type input_type) existing_states)
      input_types
  in

  let possible_input_states =
    Misc.List_op.cartesian_product_list
      (List.map (fun states -> Set_typed_relation_symbol.to_list states) possible_input_states)
  in

  let possible_input_types_who_do_not_already_have_a_corresponding_rule =
    List.filter
      (fun input_states ->
        not
          (Set_rule.exists
             (fun rule ->
               rule |> Transition.get_substates
               |> List.compare Typed_relation_symbol.compare input_states
               |> Int.equal 0)
             rules_that_match_the_symbol))
      possible_input_states
  in

  let list_of_new_rules =
    List.map
      (fun input_states -> Transition.create cs a_cf input_states filling_output_state)
      possible_input_types_who_do_not_already_have_a_corresponding_rule
  in

  (* if false then (
     Format.fprintf Format.std_formatter
       "\n\
        oooooooooooooooooooooo\n\
        Completing for convoluted symbol:\n\
        %a\n\
       \ in automaton:\n\
        %a\n\
        with states:\n\
        %a\n"
       Typed_functions.pp cs pp a
       (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
       states;
     Format.fprintf Format.std_formatter "\nResults in the follwing rules being added: %a\n"
       (Printing.pp_list_and_brackets Transition.pp)
       new_rules;
     Format.fprintf Format.std_formatter "output_type: %a    input types: %a\n"
       Datatypes.pp output_type
       (Printing.pp_list_and_brackets Datatypes.pp)
       input_types;
     Format.fprintf Format.std_formatter "filling output state: %a\n" Typed_relation_symbol.pp filling_output_state;
     Format.fprintf Format.std_formatter "nb of substates: %d\n" nb_of_substates;
     Format.fprintf Format.std_formatter "cartesian produuct of input state: %a\n"
       (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Typed_relation_symbol.pp))
       cartesian_product_of_input_state;
     Format.fprintf Format.std_formatter "states matching input types: %a\n"
       (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Typed_relation_symbol.pp))
       states_matching_input_type;
     Format.fprintf Format.std_formatter
       "states_matching_input_type_and_not_already_defined_in_automaton: %a\n"
       (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Typed_relation_symbol.pp))
       states_matching_input_type_and_not_already_defined_in_automaton;
     Format.fprintf Format.std_formatter "input_states_of_rules_automaton: %a\n"
       (Printing.pp_list_and_brackets (Printing.pp_list_and_brackets Typed_relation_symbol.pp))
       input_states_of_rules_automaton;
     (* Format.fprintf Format.std_formatter "states matching output type: %a\n"
        (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
        states_matching_output_type ; *)
     Format.fprintf Format.std_formatter "o'o'o'o'o'o'o'o'o'o'\n\n"); *)
  (* new_rules *)
  Set_rule.of_list list_of_new_rules

(* Given a base automaton, the set of states of the new automaton (the states of the base automaton and one state per convoluted datatype) and a convoluted datatype,
   adds the necessary transitions in order to recognize every term of this datatype, either in the basic automaton,
   if it already recognizes it, or else in the new state corresponding to this type *)
let completes_for_one_type
    (datatype_env : Datatype_environment.t)
    (a : t)
    (states : Set_typed_relation_symbol.t)
    (type_to_complete_for : Datatypes.t) : Set_rule.t =
  let convoluted_alphabet_for_this_type =
    Term.Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
      datatype_env type_to_complete_for
  in

  let new_rules_for_each_symbol =
    Set_typed_functions.fold
      (fun cs -> Set_rule.union (completes_for_one_symbol a states cs))
      convoluted_alphabet_for_this_type Set_rule.empty
  in

  (* if false then (
     Format.fprintf Format.std_formatter "\naaaaaaaaaaaaaaaaaaaa\nCompletion for datatype %a:\n"
       Datatypes.pp type_to_complete_for;
     Format.fprintf Format.std_formatter "\nConvoluted alphabet is %a\n"
       (Printing.pp_list Typed_functions.pp)
       convoluted_alphabet_for_this_type;
     Format.fprintf Format.std_formatter "States of this type are: %a\n"
       (Printing.pp_list_and_brackets Typed_relation_symbol.pp)
       states_for_this_type;

     List.iter2
       (fun symbol rules ->
         Format.fprintf Format.std_formatter "\nrules generated for the symbol %a:  %a \n"
           Typed_functions.pp symbol
           (Printing.pp_list_sep "   -   " Transition.pp)
           rules)
       convoluted_alphabet_for_this_type new_rules_for_each_symbol;
     Format.fprintf Format.std_formatter "\n\n";
     Format.fprintf Format.std_formatter "a'a'a'a'a'a'a'a'a'a'a'a'\n"); *)
  new_rules_for_each_symbol

let get_children_convoluted_datatypes_of_convoluted_datatype
    (datatype_env : Datatype_environment.t)
    (convolution_form : Convolution.t)
    (cd : Datatypes.t) : Set_datatypes.t =
  let convoluted_alphabet_for_datatype =
    Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
      datatype_env cd
  in
  let children_datatype_of_these_convoluted_symbols =
    Set_typed_functions.fold
      (fun cs ->
        Set_datatypes.union
          (cs |> Typed_functions.get_inputs_types
          |> Convolution.convolute_generic convolution_form
          |> Set_datatypes.of_list))
      convoluted_alphabet_for_datatype Set_datatypes.empty
  in
  children_datatype_of_these_convoluted_symbols

(* not optimized at all.. *)
let get_all_types_needed_for_some_convoluted_type
    (datatype_env : Term.Datatype_environment.t)
    (convolution_form : Convolution.t)
    (cd : Term.Datatypes.t) : Term.Aliases.Set_datatypes.t =
  let children_of_one_datatype_getter =
    get_children_convoluted_datatypes_of_convoluted_datatype datatype_env convolution_form
  in
  let accumulator_function convoluted_datatypes =
    let new_datatypes =
      Set_datatypes.fold
        (fun cd -> Set_datatypes.union (children_of_one_datatype_getter cd))
        convoluted_datatypes Set_datatypes.empty
    in
    Set_datatypes.union convoluted_datatypes new_datatypes
  in
  let from = Set_datatypes.singleton cd in
  let every_needed_datatype =
    Misc.Others.fixpoint_from Set_datatypes.equal accumulator_function from
  in

  every_needed_datatype

(** Given an automaton, completes the automaton w.r.t. its type, i.e. recognizes every term of the automaton's type in a non-final state if not already recognized *)
let complete (datatype_env : Datatype_environment.t) (a : t) : t =
  let states_already_there = extract_defined_states a in
  (* Adding type of automaton in case of no final state *)
  let every_convoluted_type_of_this_automaton =
    states_already_there |> Set_typed_relation_symbol.to_list
    |> List.map Typed_relation_symbol.get_types
    |> Set_datatypes.of_list
    |> Set_datatypes.add (get_type_of_automaton a)
  in
  (* This adds every children type required. For example, it adds {<nat>, <natlist>} for <natlist,nat> and right-convolution *)
  let every_convoluted_type_needed =
    Set_datatypes.fold
      (fun cd ->
        Set_datatypes.union
          ((get_all_types_needed_for_some_convoluted_type datatype_env (get_convolution_form a)) cd))
      every_convoluted_type_of_this_automaton Set_datatypes.empty
  in

  (* let  = Misc.List_op.list_union compare subtype_of_each_convoluted_type in *)
  let filling_states_of_each_type =
    every_convoluted_type_needed |> Set_datatypes.to_list
    |> List.map State_generator.create_canonical_state_for_completing_automaton
    |> Set_typed_relation_symbol.of_list
  in

  (* if false then (
     Format.fprintf Format.std_formatter "\n-------------------\nCompleting automaton:\n%a\n" pp a;
     Format.fprintf Format.std_formatter "\n every subtype: %a\n"
       (Printing.pp_list_and_brackets Datatypes.pp)
       every_subtype;
     Format.fprintf Format.std_formatter "\nOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\n"); *)
  let all_states =
    Set_typed_relation_symbol.union filling_states_of_each_type states_already_there
  in
  let completing_func = completes_for_one_type datatype_env a all_states in
  let new_rules =
    Set_datatypes.fold
      (fun cd -> Set_rule.union (completing_func cd))
      every_convoluted_type_needed Set_rule.empty
  in
  let all_rules_of_new_automaton = Set_rule.union (get_rules a) new_rules in
  let new_automaton =
    create (get_final_states a) all_rules_of_new_automaton (get_type_of_automaton a)
      (get_convolution_form a)
  in
  (* let new_automaton = create (get_final_states a) all_rules_of_new_automaton new_id in *)
  let reachable_new_automaton = minimize new_automaton in
  reachable_new_automaton

(** Given an automaton, computes its complement w.r.t. its type *)
let complement (datatype_env : Datatype_environment.t) (a : t) : t =
  (* if false then (
     Format.fprintf Format.std_formatter
       "\n\n\n\n-----------------------------------------------------------------------\n\n";
     Format.fprintf Format.std_formatter "\nTrying to complement automaton %a\n" pp a); *)

  (* Format.fprintf Format.std_formatter "------------------------\nTrying to complement automaton %a in environment \n%a\n" pp a *)
  (* Datatype_environment.pp_datatype_environment datatype_env; *)
  let completed_automaton = complete datatype_env a in

  (* Format.fprintf Format.std_formatter "\nThe completed automaton in: %a \n" pp completed_automaton; *)
  let type_automaton = get_type_of_automaton completed_automaton in

  (* let filling_state_of_automaton_type =
     Typed_relation_symbol.create_canonical_state_from_convoluted_datatype type_automaton in *)
  let final_states = get_final_states completed_automaton in
  let states = extract_defined_states completed_automaton in

  let states_of_correct_type =
    Set_typed_relation_symbol.filter
      (fun state -> Datatypes.equal (Typed_relation_symbol.get_types state) type_automaton)
      states
  in
  let non_final_states_of_correct_type =
    Set_typed_relation_symbol.diff states_of_correct_type final_states
  in

  (* Format.fprintf Format.std_formatter "\nType of automaton: %a\n" Datatypes.pp type_automaton;
     Format.fprintf Format.std_formatter "\nstates: %a\n" Set_typed_relation_symbol.pp states;
     Format.fprintf Format.std_formatter "\nfinal states: %a\n" Set_typed_relation_symbol.pp final_states;
     Format.fprintf Format.std_formatter "\nstates of correct type: %a\n" Set_typed_relation_symbol.pp
       states_of_correct_type;
     Format.fprintf Format.std_formatter "\nnon final states of correct type: %a\n" Set_typed_relation_symbol.pp
       non_final_states_of_correct_type;

     Format.fprintf Format.std_formatter
       "\nCreating a new automaton with final states %a and rules:\n%a\n\n" Set_typed_relation_symbol.pp
       non_final_states_of_correct_type Set_rule.pp (get_rules completed_automaton); *)
  let complement_automaton =
    create non_final_states_of_correct_type (get_rules completed_automaton)
      (get_type_of_automaton a) (get_convolution_form a)
  in

  (* create non_final_states_of_correct_type (get_rules completed_automaton) new_id in *)

  (* Format.fprintf Format.std_formatter "\nAutomaton successfully complemented. The complement is %a\n" pp *)
  (* complement_automaton; *)
  complement_automaton

let normalize_typechecks (a : t) (ct : Patterns.t) (s : Typed_relation_symbol.t) : unit =
  let typechecks =
    let term_datatype = Patterns.get_type ct in
    let state_datatype = Typed_relation_symbol.get_types s in
    let automaton_type = get_type_of_automaton a in
    Datatypes.equal term_datatype state_datatype && Datatypes.equal state_datatype automaton_type
    (* let term_arity = Convoluted_term.get_degree_convolution ct in *)
    (* let automaton_has_correct_arity = get_arity a = term_arity in *)
    (* let automaton_datatype = get_type_of_automaton a in *)
  in
  (* && state_datatype = automaton_datatype  in *)
  if not typechecks then
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Normalize typing error: (automaton, convoluted term, state) = (%a,   %a,   %a)" pp a
           Patterns.pp ct Typed_relation_symbol.pp s)
    in
    raise (Invalid_argument error_message)

(* We suppose that patterns are ground *)
let rec generate_new_rules_for_recognizing
    (cf : Convolution.t)
    (ct : Patterns.t)
    (s : Typed_relation_symbol.t) : Set_rule.t =
  let root_symbol = Option.get (Patterns.get_root_symbols ct) in
  let children_not_convoluted = List.map Pattern.get_children ct in
  let children_convoluted = Convolution.convolute_generic cf children_not_convoluted in
  let type_of_children = List.map Patterns.get_type children_convoluted in
  let new_states_for_children = List.map State_generator.generate_a_fresh_state type_of_children in
  let rule_for_this_step = Transition.create root_symbol cf new_states_for_children s in

  let rules_for_subterms =
    List.map2 (generate_new_rules_for_recognizing cf) children_convoluted new_states_for_children
  in
  let all_rules = rules_for_subterms |> Set_rule.list_union |> Set_rule.add rule_for_this_step in
  all_rules

let normalize_with_new_states (a : t) (ct : Patterns.t) (s : Typed_relation_symbol.t) : t =
  let () = normalize_typechecks a ct s in
  let new_rules = generate_new_rules_for_recognizing (get_convolution_form a) ct s in
  let all_rules = Set_rule.union new_rules (get_rules a) in
  let new_automaton =
    create (get_final_states a) all_rules (get_type_of_automaton a) (get_convolution_form a)
  in
  new_automaton

module Map_state_type = Misc.My_map.Make2 (Typed_relation_symbol) (Datatypes)

let rec normalize (a : t) (ct : Patterns.t) : Typed_relation_symbol.t * t =
  (* let () = Format.fprintf Format.std_formatter "------------------------------NORMALIZE-BEGIN\n" in
     let () =
       let states = extract_states a in
       let map_state_type =
         states |> Set_typed_relation_symbol.to_list
         |> List.map (fun s -> (s, Typed_relation_symbol.get_types s))
         |> Map_state_type.of_list
       in

       Format.fprintf Format.std_formatter
         "Trying to normalize automaton%a\nwith term  %a\nThe states are of type: \n%a\nConvolution used is: %a\n\n"
         pp a Patterns.pp ct Map_state_type.pp map_state_type Convolution.pp a.convolution_form
     in *)
  let states_in_which_ct_is_recognized = accepts_in a ct in

  (* let () =
       Format.fprintf Format.std_formatter "States recognizing the term are:\n%a\n\n"
         Set_typed_relation_symbol.pp states_in_which_ct_is_recognized
     in *)
  match Set_typed_relation_symbol.choose_opt states_in_which_ct_is_recognized with
  | None ->
      let new_state = State_generator.generate_a_fresh_state (Patterns.get_type ct) in
      let subpatterns = List.map Pattern.get_children ct in
      let subterms = Convolution.convolute_generic (get_convolution_form a) subpatterns in
      let substates, normalized_automaton_of_subterms =
        List.fold_right
          (fun (st : Patterns.t) ((substates, aut) : Typed_relation_symbol.t list * t) ->
            let substate, new_automaton = normalize aut st in
            let updated_substates = substate :: substates in
            (updated_substates, new_automaton))
          subterms ([], a)
      in

      let top_symbol = Option.get (Patterns.get_root_symbols ct) in
      let new_rule = Transition.create top_symbol (get_convolution_form a) substates new_state in
      let normalized_automaton = add_rule normalized_automaton_of_subterms new_rule in

      (* let () =
           Format.fprintf Format.std_formatter "------------------------------NORMALIZE-END\n"
         in *)
      (new_state, normalized_automaton)
  | Some s ->
      (* let () =
           Format.fprintf Format.std_formatter "------------------------------NORMALIZE-END\n"
         in *)
      (s, a)

let apply_state_substitution_on_rule
    (state_sub : Typed_relation_symbol.t -> Typed_relation_symbol.t)
    (r : Transition.t) : Transition.t =
  let new_rule = Transition.state_map state_sub r in
  new_rule

let map_state (state_sub : Typed_relation_symbol.t -> Typed_relation_symbol.t) (a : t) : t =
  let old_final_states = get_final_states a in
  let old_rules = get_rules a in
  let new_final_states = Set_typed_relation_symbol.map state_sub old_final_states in

  let new_rules = Set_rule.map (apply_state_substitution_on_rule state_sub) old_rules in

  let new_automaton =
    create new_final_states new_rules (get_type_of_automaton a) (get_convolution_form a)
  in

  (* let () =
       Format.fprintf Format.std_formatter "\n--------------------Apply state substitution : DEBUT\n"
     in
     let () = Format.fprintf Format.std_formatter "old_automaton:\n%a\n" pp a in
     let () =
       Format.fprintf Format.std_formatter "substitution:%a\n"
         (Printing.pp_assoclist Typed_relation_symbol.pp Typed_relation_symbol.pp)
         state_substitutions in
     let () = Format.fprintf Format.std_formatter "new_automaton:\n%a\n" pp new_automaton in

     let () =
       Format.fprintf Format.std_formatter "--------------------Apply state substitution : Fin\n" in *)
  new_automaton
