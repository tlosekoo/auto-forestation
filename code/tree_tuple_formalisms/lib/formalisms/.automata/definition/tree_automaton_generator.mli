include Tree_tuple_formalism.Generatable with type t := Tree_automaton_core.t

(** Selects an arbitrary convolution form for generating automata *)
val arbitrary_convolution_used_by_default : Convolution.t
