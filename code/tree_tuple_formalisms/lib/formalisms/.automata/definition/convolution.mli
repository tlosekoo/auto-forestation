(** This module provides a generic implementation of the regular convolution 
(which is kind of like List.combine, but extended to arbitrary number of lists of possibly different size (matrix transposition)) *)

type t =
  | Right
  | Left
  | Complete

include Misc.Ordered_and_printable.OP with type t := t

(** Comparison function *)
val compare : t -> t -> int

(** Equality function *)
val equal : t -> t -> bool

(** pretty-printing *)
val pp : Format.formatter -> t -> unit

(** Returns the desired convolution, depending of the input convolution type. 
For example, [convolute_generic Right] converts [ [a;b] ; [c] ] into [ [a] ; [b ; c]] *)
val convolute_generic : t -> 'a list list -> 'a list list

(** returns true if there is no element in those lists *)
val is_completely_empty : 'a list list -> bool
