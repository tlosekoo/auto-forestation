(** A rule is the data of a convoluted symbol, a list of substates, a goal state, and a convolution form. It must typecheck. *)
open Term

open Term.Aliases
include Misc.Ordered_and_printable.OP

(** 'pp_parameterized ~complete:true' is 'pp_complete' and 'pp_parameterized ~complete:false' is 'pp'. *)
val pp_parameterized : complete:bool -> Format.formatter -> t -> unit

(** From a convoluted symbol 'F', a list of substates 'q1, ..., qn', a goal state 'q', and a convoluted form 'cf', creates the rule 'F(q1, ..., qn) -> q'.
   	Raises an 'IllegalArgument' exception if the rule does not typecheck. *)
val create :
  Term.Typed_functions.t ->
  Convolution.t ->
  Typed_relation_symbol.t list ->
  Typed_relation_symbol.t ->
  t

(** given a rule 'F(q1, ..., qn) -> q' and a state 's', returns 'q'='s'. *)
val matches_state : Typed_relation_symbol.t -> t -> bool

(** Given a rule 'F(q_1, ..., q_n) -> q', returns 'F' *)
val get_functions : t -> Term.Typed_functions.t

(** Given a rule 'F(q_1, ..., q_n) -> q', returns '(q_1, ..., q_n)' *)
val get_substates : t -> Typed_relation_symbol.t list

(** Given a rule 'F(q_1, ..., q_n) -> q', returns 'q' *)
val get_towards : t -> Typed_relation_symbol.t

(** Given a rule 'F(q_1, ..., q_n) -> q', returns its convolution form *)
val get_convolution_form : t -> Convolution.t

(** given a rule 'F(q1, ..., qn) -> q' and states 's1, ..., sm', returns 'qi'='si' for every 'i' ('i' in 0..max(n,m)). *)
(* val matches_substates : Typed_relation_symbol.t list -> t -> bool *)

(** given a rule 'F(q1, ..., qn) -> q' and a term 't', returns true iff the top symbol of 't' matches 'F'. *)
val matches_patterns : Term.Patterns.t -> t -> bool

(** Given a list of options of ranked symbols <Some(f_1), ..., None_n> and a rule <f'_1, ..., f'_n>,
   returns true iff, for every index i (in [0,n]), if f_i is defined then f_i = f'_i.
      Moreover, unspecified f'_i cannot be padding. *)
val matches_constraint_symbols : Term.Typed_function.t option list -> t -> bool

(** 'filter_rules_containing_only_states_from sl rule' returns true iff every state used in 'rule' is in 'sl' *)
val contains_only_states_from : Set_typed_relation_symbol.t -> t -> bool

(** Given a rule 'F(q_1, ..., q_n) -> q', returns the arity of 'F' *)
val get_arity_of_rule_symbol : t -> int

(** Given a rule 'F(q_1, ..., q_n) -> q', returns the number of symbols composing 'F' *)
val get_nb_symbols_of_rule : t -> int

(** Returns the union of states (substates and goal) from each rule *)
val extract_states : t -> Set_typed_relation_symbol.t

(** 'state_map f r' returns a new rule 'r'' which has the same symbols and type as 'r', but every state has been passed by 'f'. *)
val state_map : (Typed_relation_symbol.t -> Typed_relation_symbol.t) -> t -> t
