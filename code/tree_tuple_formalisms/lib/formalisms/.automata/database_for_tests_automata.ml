open Aliases_automata
open Term
include Term.Database_for_tests

let q_n = Typed_relation_symbol.create (Symbol.create "q_n") Database_for_tests.c_nat
let q_n_n = Typed_relation_symbol.create (Symbol.create "q_n_n") Database_for_tests.c_nat_nat
let q_l_n = Typed_relation_symbol.create (Symbol.create "q_l_n") Database_for_tests.c_natlist_nat
let q_rl_n = Typed_relation_symbol.create (Symbol.create "q_rl_n") Database_for_tests.c_nattsil_nat
let q_n_l = Typed_relation_symbol.create (Symbol.create "q_n_l") Database_for_tests.c_nat_natlist
let q = Typed_relation_symbol.create (Symbol.create "q") []

let q_l_l =
  Typed_relation_symbol.create (Symbol.create "q_l_l") Database_for_tests.c_natlist_natlist

let qq_l_l =
  Typed_relation_symbol.create (Symbol.create "qq_l_l") Database_for_tests.c_natlist_natlist

let qe_n = Typed_relation_symbol.create (Symbol.create "qe_n") Database_for_tests.c_nat
let qo_n = Typed_relation_symbol.create (Symbol.create "qo_n") Database_for_tests.c_nat
let qle_l = Typed_relation_symbol.create (Symbol.create "qle_l") Database_for_tests.c_natlist
let qlo_l = Typed_relation_symbol.create (Symbol.create "qlo_l") Database_for_tests.c_natlist
let q_l = Typed_relation_symbol.create (Symbol.create "q_l") Database_for_tests.c_natlist
let some_states = [q_n; q_n_n; q_l_n; q_rl_n; q_n_l; q; q_l_l; qq_l_l; qe_n; qo_n; qle_l; qlo_l]

(* unary *)
let conv_z = [Database_for_tests.z_symbol]
let conv_s = [Database_for_tests.s_symbol]
let conv_nil = [Database_for_tests.nil_symbol]
let conv_cons = [Database_for_tests.cons_symbol]

(* binary *)
(* let conv_nil_z = Typed_functions.create [Database_for_tests.nil_symbol; Database_for_tests.z_symbol]
   let conv_cons_s = Typed_functions.create [Database_for_tests.cons_symbol; Database_for_tests.s_symbol] *)
let rule_z_in__qe_n = Transition.create conv_z Convolution.Right [] qe_n
let rule_s_qo_in__qe_n = Transition.create conv_s Convolution.Right [qo_n] qe_n
let rule_s_qe_in__qo_n = Transition.create conv_s Convolution.Right [qe_n] qo_n
let rule_nil_in__qle_l = Transition.create conv_nil Convolution.Right [] qle_l
let rule_nil_in__q_l = Transition.create conv_nil Convolution.Right [] q_l
let rule_cons_qn_qlo_in__qle_l = Transition.create conv_cons Convolution.Right [q_n; qlo_l] qle_l
let rule_cons_qn_qle_in__qlo_l = Transition.create conv_cons Convolution.Right [q_n; qle_l] qlo_l
let rule_cons_qn_ql_in__q_l = Transition.create conv_cons Convolution.Right [q_n; q_l] q_l
let rule_z_in__q_n = Transition.create conv_z Convolution.Right [] q_n
let rule_s_qn_in__q_n = Transition.create conv_s Convolution.Right [q_n] q_n
let rule_z_in__q_n_left = Transition.create conv_z Convolution.Left [] q_n
let rule_s_qn_in__q_n_left = Transition.create conv_s Convolution.Left [q_n] q_n
let rule_nil_and_z_in__q_l_n = Transition.create Database_for_tests.nil_z Convolution.Right [] q_l_n

let rule_lin_and_z_in__q_rl_n =
  Transition.create Term.Database_for_tests.lin_z Convolution.Left [] q_rl_n

let rule_cons_and_s_of_qn_qln_in__q_l_n =
  Transition.create Database_for_tests.cons_s Convolution.Right [q_n; q_l_n] q_l_n

let rule_snoc_and_s_of_qrln_qnp_in__q_rl_n =
  Transition.create Term.Database_for_tests.snoc_s Convolution.Left [q_rl_n; q_n] q_rl_n

(* let rule_s_and_pad_of_qnnatpadding_in_qnnatpadding = *)
(* Transition.create conv_s_pad [qn_nat_padding] qn_nat_padding Convolution.Right *)

(* let rule_s_and_pad_of_qnp_in_qnp = Transition.create conv_s_pad [qnp] qnp Convolution.Left *)

let some_rules =
  Set_transition.of_list
    [
      rule_z_in__qe_n;
      rule_s_qo_in__qe_n;
      rule_s_qe_in__qo_n;
      rule_nil_in__qle_l;
      rule_nil_in__q_l;
      rule_cons_qn_qlo_in__qle_l;
      rule_cons_qn_qle_in__qlo_l;
      rule_cons_qn_ql_in__q_l;
      rule_z_in__q_n;
      rule_s_qn_in__q_n;
      rule_z_in__q_n_left;
      rule_s_qn_in__q_n_left;
      rule_nil_and_z_in__q_l_n;
      rule_lin_and_z_in__q_rl_n;
      rule_cons_and_s_of_qn_qln_in__q_l_n;
      rule_snoc_and_s_of_qrln_qnp_in__q_rl_n;
    ]

let a_nat_1 =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton q_n)
    (Set_transition.of_list [rule_z_in__q_n; rule_s_qn_in__q_n])
    Database_for_tests.c_nat Convolution.Right

let a_nat_2 =
  Tree_automaton_generator.all_in_a_type Database_for_tests.some_type_env Database_for_tests.c_nat

let a_even =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton qe_n)
    (Set_transition.of_list [rule_z_in__qe_n; rule_s_qe_in__qo_n; rule_s_qo_in__qe_n])
    Database_for_tests.c_nat Convolution.Right

let a_odd =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton qo_n)
    (Set_transition.of_list [rule_z_in__qe_n; rule_s_qe_in__qo_n; rule_s_qo_in__qe_n])
    Database_for_tests.c_nat Convolution.Right

let a_natlist_1 =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton q_l)
    (Set_transition.of_list
       [rule_z_in__q_n; rule_s_qn_in__q_n; rule_nil_in__q_l; rule_cons_qn_ql_in__q_l])
    Database_for_tests.c_natlist Convolution.Right

let a_natlist_2 =
  Tree_automaton_generator.all_in_a_type Database_for_tests.some_type_env
    Database_for_tests.c_natlist

let a_natlist_natlist_id =
  Tree_automaton.identity_in_a_type Database_for_tests.some_type_env Database_for_tests.natlist

let a_list_size =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton q_l_n)
    (Set_transition.of_list
       [
         rule_z_in__q_n;
         rule_s_qn_in__q_n;
         rule_nil_and_z_in__q_l_n;
         rule_cons_and_s_of_qn_qln_in__q_l_n;
       ])
    Database_for_tests.c_natlist_nat Convolution.Right

let a_tsil_size =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton q_rl_n)
    (Set_transition.of_list
       [
         rule_z_in__q_n_left;
         rule_s_qn_in__q_n_left;
         rule_lin_and_z_in__q_rl_n;
         rule_snoc_and_s_of_qrln_qnp_in__q_rl_n;
       ])
    Database_for_tests.c_nattsil_nat Convolution.Left

let a_lists_of_z_and_their_size =
  Tree_automaton.create
    (Set_typed_relation_symbol.singleton q_l_n)
    (Set_transition.of_list
       [rule_z_in__q_n; rule_nil_and_z_in__q_l_n; rule_cons_and_s_of_qn_qln_in__q_l_n])
    Database_for_tests.c_natlist_nat Convolution.Right

let a_natlist_nat =
  let automaton_natlist_nat_empty =
    Tree_automaton.create Set_typed_relation_symbol.empty Set_transition.empty
      Database_for_tests.c_natlist_nat Convolution.Right
  in
  let completed = Tree_automaton.complete some_type_env automaton_natlist_nat_empty in
  let states_of_completed = Tree_automaton.extract_defined_states completed in
  let states_of_the_correct_type =
    Set_typed_relation_symbol.filter
      (fun state ->
        Datatypes.equal (Typed_relation_symbol.get_types state) Database_for_tests.c_natlist_nat)
      states_of_completed
  in
  let automaton_recognizing_all_data =
    Set_typed_relation_symbol.fold
      (fun a s -> Tree_automaton.add_final_state s a)
      states_of_the_correct_type completed
  in
  automaton_recognizing_all_data

let complemented_automaton_of_list_and_its_size =
  let datatypes_env = some_type_env in
  let automaton_list_size = a_list_size in
  let complemented_automaton = Tree_automaton.complement datatypes_env automaton_list_size in
  complemented_automaton

let some_automata_right_convolution =
  Set_automaton.of_list
    [
      a_nat_1;
      a_nat_2;
      a_even;
      a_odd;
      a_natlist_1;
      a_natlist_2;
      a_list_size;
      a_lists_of_z_and_their_size;
      a_natlist_nat;
      a_natlist_natlist_id;
      complemented_automaton_of_list_and_its_size;
    ]

let some_automata_left_convolution = Set_automaton.of_list [a_tsil_size]

let some_automata =
  Set_automaton.union some_automata_right_convolution some_automata_left_convolution

(* TODO: Typechecking? *)
let state_substitution_reverse_parity =
  Map_typedRelationSymbol_typedRelationFunction.of_list [(qo_n, qe_n); (qe_n, qo_n)]

let state_substitution_forget_parity =
  Map_typedRelationSymbol_typedRelationFunction.of_list [(qe_n, q_n); (qo_n, q_n)]
