open Term

type t = {
  predicate : Typed_relation_symbol.t;
  functions : Typed_functions.t;
}
[@@deriving compare, equal]

let create (predicate : Typed_relation_symbol.t) (functions : Typed_functions.t) : t =
  {predicate; functions}

let pp (c : Format.formatter) (h : t) =
  Format.fprintf c "%a(%a)" Typed_relation_symbol.pp h.predicate Term.Typed_functions.pp h.functions

let from_typing_obligation_to_head_opt (typing_obligation : Typing_obligation.t) : t Option.t =
  let functions_opt = Patterns.get_root_symbols typing_obligation.patterns in
  let predicate = typing_obligation.predicate_symbol in
  Option.map (fun functions -> {functions; predicate}) functions_opt

let get_input_types (head : t) : Term.Datatypes.t list =
  head.functions |> List.map Term.Typed_function.get_input_types

let typecheck (head : t) : bool =
  Term.Datatypes.equal
    (Term.Typed_functions.get_output_types head.functions)
    (Term.Typed_relation_symbol.get_types head.predicate)

let map_predicate (m : Typed_relation_symbol.t -> Typed_relation_symbol.t) (h : t) =
  {predicate = m h.predicate; functions = h.functions}
