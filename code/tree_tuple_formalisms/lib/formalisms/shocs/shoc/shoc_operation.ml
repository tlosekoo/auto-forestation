(* This file defines a Shallow Horn Clause (SHoC) *)
open Term
open Term.Aliases
open Shoc_core
open Aliases_typing_obligation

let matches_symbols (ts : Term.Typed_functions.t) (r : t) : bool =
  Term.Typed_functions.equal (get_functions_symbols r) ts

let matches_predicate_symbol (ps : Typed_relation_symbol.t) (r : t) : bool =
  Typed_relation_symbol.equal (get_head_relation r) ps

let matches_partial_symbols (constraints_functions : Term.Typed_function.t Option.t List.t) (r : t)
    : bool =
  let symbol_list_head = get_functions_symbols r in
  if List.length symbol_list_head = List.length constraints_functions then
    List.for_all2
      (fun symbol -> Option.fold ~none:true ~some:(Term.Typed_function.equal symbol))
      symbol_list_head constraints_functions
  else
    false

let matches_head_internal (head : Shoc_head.t) (r : t) : bool =
  let head2 = get_head r in
  Shoc_head.equal head head2

let matches_head (relation : Typed_relation_symbol.t) (function_symbols : Typed_functions.t) :
    t -> bool =
  let head = Shoc_head.create relation function_symbols in
  matches_head_internal head

let map_predicate_symbols (m : Typed_relation_symbol.t -> Typed_relation_symbol.t) (r : t) : t =
  let head, body = (get_head r, get_body r) in
  let new_head = Shoc_head.map_predicate m head in
  let new_body = Shoc_body.map (Formal_atom.map_predicate m) body in
  (new_head, new_body)

(* let unfold (shoc : t) (typing_obligation : Typing_obligation.t) : Clause.Aliases.Set_atom_patterns.t option =
   let root_relation =
   Shoc_body.apply patterns (Shoc_core.get_body shoc) *)

let unfold_no_head_check (patterns : Patterns.t) (shoc : t) : Set_typing_obligation.t =
  Shoc_body.apply patterns (Shoc_core.get_body shoc)

(* Returns the minimum set of constraints on variables appearing in the patterns
   to make the rule applicable. If it is not possible, then returns None. *)
let unify_with_patterns (patterns : Pattern.t list) (shocs : t) :
    Map_typedSymbol_typedFunction.t option =
  (* Format.fprintf Format.std_formatter "Trying to unify\n%a\nwith\n%a\n\n" Patterns.pp patterns
     Shoc_printing.pp shocs; *)
  shocs |> get_functions_symbols
  |> List.fold_left2
       (fun constraints_opt pattern symbol ->
         Option.bind constraints_opt (fun constraints ->
             match Pattern.get_variable_opt pattern with
             | Some var ->
                 Map_typedSymbol_typedFunction.add_compatible_binding_opt var symbol constraints
             | None ->
                 let root_symbol = Pattern.get_root_symbol pattern in
                 if Typed_function.equal root_symbol symbol then constraints_opt else None))
       (Some Map_typedSymbol_typedFunction.empty) patterns
