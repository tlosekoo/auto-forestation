include Misc.My_set.Make2 (Formal_atom)
open Aliases_typing_obligation

let extract_predicates (body : t) : Term.Aliases.Set_typed_relation_symbol.t =
  body |> to_list
  |> List.map Formal_atom.get_predicate
  |> Term.Aliases.Set_typed_relation_symbol.of_list

let apply (patterns : Term.Patterns.t) (rule_body : t) : Set_typing_obligation.t =
  rule_body |> to_list |> List.map (Formal_atom.apply patterns) |> Set_typing_obligation.of_list
