(* This file defines a Shallow Horn Clause (SHoC) *)
open Term
open Shoc_core
module Map_projector_variable = Misc.My_map.Make2 (Projector) (Typed_symbol)

let var_of_projector (p : Projector.t) (datatype : Term.Datatype.t) : Typed_symbol.t =
  let name = "x_" ^ string_of_int p.projector_term ^ "_" ^ string_of_int p.projector_child in
  Typed_symbol.create (Symbol.create name) datatype

let build_head (head : Shoc_head.t) : Clause.Atom_patterns.t =
  (* Atom_patterns.create *)
  let predicate = head.predicate in
  let functions = head.functions in
  (* let () =
       Format.fprintf Format.std_formatter "Head function:\n%a\n" Typed_functions.pp functions
     in *)
  let head_terms =
    List.mapi
      (fun projector_term f ->
        let variables_for_this_function =
          List.mapi
            (fun projector_child var_type ->
              let p = Projector.{projector_term; projector_child} in
              var_of_projector p var_type)
            (Term.Typed_function.get_input_types f)
        in
        let vars_as_patterns = List.map Term.Pattern.from_var variables_for_this_function in
        Term.Pattern.create f vars_as_patterns)
      functions
  in
  (* let () =
       Format.fprintf Format.std_formatter "Head terms:\n%a\n"
         (Printing.pp_list_and_brackets Pattern.pp)
         head_terms
     in
     let () =
       Format.fprintf Format.std_formatter "predicate symbol:\n%a\n"
         (Typed_relation_symbol.pp_parameterized ~complete:true)
         predicate
     in *)
  Clause.Atom_patterns.create_from_predicate_symbol predicate head_terms

let build_body_atom (map_projector_variable : Map_projector_variable.t) (ba : Formal_atom.t) :
    Clause.Atom_patterns.t =
  let variables =
    ba.projectors |> List.map (fun p -> Map_projector_variable.find p map_projector_variable)
  in
  let variables_as_patterns = List.map Term.Pattern.from_var variables in
  Clause.Atom_patterns.create_from_predicate_symbol ba.predicate variables_as_patterns

let build_body (map_projector_variable : Map_projector_variable.t) (ba : Shoc_body.t) =
  ba |> Shoc_body.to_list |> List.map (build_body_atom map_projector_variable)

let to_clause (rrc : t) : Clause.Clause_patterns.t =
  let head, body = rrc in
  let functions = head.functions in
  let functions_types = List.map Term.Typed_function.get_input_types functions in

  let projectors_and_variables =
    List.mapi
      (fun projector_term types ->
        List.mapi
          (fun projector_child type_var ->
            let p = Projector.{projector_term; projector_child} in
            (p, var_of_projector p type_var))
          types)
      functions_types
  in

  let map_projector_variable =
    Map_projector_variable.of_list (List.flatten projectors_and_variables)
  in

  let clause_head = build_head head in
  let clause_body = build_body map_projector_variable body in
  Clause.Disjunctive_clause_patterns.from_body_and_head ~body:clause_body ~head:[clause_head]

let pp (c : Format.formatter) (rrc : t) = Clause.Disjunctive_clause_patterns.pp c (to_clause rrc)
