(* This file defines a Shallow Horn Clause (SHoC) *)
open Term
include Misc.Pair.Make (Shoc_head) (Shoc_body)
module Set_body = Misc.My_set.Make (Misc.My_set.Make (Formal_atom))

let create_from_list
    (predicate : Typed_relation_symbol.t)
    (functions : Term.Typed_functions.t)
    (body : Formal_atom.t list) : t =
  (Shoc_head.{predicate; functions}, Shoc_body.of_list body)

let get_head : t -> Shoc_head.t = fst
let get_body : t -> Shoc_body.t = snd
let get_head_relation (r : t) = (get_head r).predicate

let extract_predicate_symbols_body (r : t) =
  let body = get_body r in
  body |> Shoc_body.to_list
  |> List.map Formal_atom.get_predicate
  |> Term.Aliases.Set_typed_relation_symbol.of_list

let extract_predicate_symbols (r : t) =
  Term.Aliases.Set_typed_relation_symbol.add (get_head_relation r)
    (extract_predicate_symbols_body r)

let typecheck (rrc : t) : bool =
  let head, body = (get_head rrc, get_body rrc) in
  let type_head = Shoc_head.get_input_types head in
  let head_ok = Shoc_head.typecheck head in
  let body_ok = Shoc_body.for_all (Formal_atom.typecheck type_head) body in
  head_ok && body_ok

let create
    (predicate : Typed_relation_symbol.t)
    (functions : Term.Typed_functions.t)
    (body : Shoc_body.t) : t =
  let shoc = (Shoc_head.{predicate; functions}, body) in
  if typecheck shoc then
    shoc
  else
    let error_message =
      Format.flush_str_formatter
        (Format.fprintf Format.str_formatter
           "Shoc  %a  has incompatible types.\nHead relation is %a and head functions are %a." pp
           shoc
           (Typed_relation_symbol.pp_parameterized ~complete:true)
           predicate
           (Typed_functions.pp_parameterized ~complete:true)
           functions)
    in
    raise (Failure error_message)

let get_functions_symbols (r : t) : Term.Typed_functions.t =
  let head = get_head r in
  head.functions
