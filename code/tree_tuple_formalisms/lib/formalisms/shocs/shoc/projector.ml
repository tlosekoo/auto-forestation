open Term

type t = {
  projector_term : Int.t;
  projector_child : Int.t;
}
[@@deriving compare, equal]

let pp (c : Format.formatter) (p : t) =
  Format.fprintf c "(%d, %d)" p.projector_term p.projector_child

let project_on_patterns (patterns : Patterns.t) (pr : t) : Pattern.t =
  let correct_pattern = List.nth patterns pr.projector_term in
  let correct_child_of_correct_pattern =
    List.nth (Pattern.get_children correct_pattern) pr.projector_child
  in
  correct_child_of_correct_pattern

let project_on_datatypes (datatypes_list : Datatypes.t list) (pr : t) : Datatype.t =
  let correct_datatypes = List.nth datatypes_list pr.projector_term in
  let correct_child_of_correct_datatype = List.nth correct_datatypes pr.projector_child in
  correct_child_of_correct_datatype
