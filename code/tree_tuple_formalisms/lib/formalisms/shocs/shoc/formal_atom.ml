open Term
module Projector_list = Misc.List_maker.Make (Projector)

type t = {
  predicate : Typed_relation_symbol.t;
  projectors : Projector_list.t;
}
[@@deriving compare, equal]

let get_predicate (ba : t) = ba.predicate
let get_projectors (ba : t) = ba.projectors

let pp (c : Format.formatter) (ba : t) =
  Format.fprintf c "%a(%a)" Typed_relation_symbol.pp ba.predicate Projector_list.pp ba.projectors

let apply (patterns : Term.Patterns.t) (ba : t) : Typing_obligation.t =
  let new_patterns = List.map (Projector.project_on_patterns patterns) ba.projectors in
  Typing_obligation.create ba.predicate new_patterns

let typecheck (types_of_head : Term.Datatypes.t List.t) (ba : t) : bool =
  let resulting_datatypes = List.map (Projector.project_on_datatypes types_of_head) ba.projectors in
  let predicate_datatypes = ba.predicate in
  Term.Datatypes.equal (Typed_relation_symbol.get_types predicate_datatypes) resulting_datatypes

let map_predicate (m : Typed_relation_symbol.t -> Typed_relation_symbol.t) (ba : t) =
  {predicate = m ba.predicate; projectors = ba.projectors}
