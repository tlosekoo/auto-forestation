open Term
open Term.Aliases
open Shocs_core
open Shocs_matching
open Aliases_typing_obligation

(* Suppose that the typing obligation is ground. *)
let rec accepts (recognizing_shocs : t) (typing_obligation : Typing_obligation.t) : bool =
  let head = Option.get (Shoc_head.from_typing_obligation_to_head_opt typing_obligation) in
  let rules_matching_head = rules_that_match_head head recognizing_shocs in
  exists
    (fun rule_matching_head ->
      let resulting_typing_obligations =
        Shoc.unfold_no_head_check typing_obligation.patterns rule_matching_head
      in
      Set_typing_obligation.for_all
        (fun some_resutlting_typing_onligation ->
          accepts recognizing_shocs some_resutlting_typing_onligation)
        resulting_typing_obligations)
    rules_matching_head

let accepts_in (recognizing_shocs : t) (patterns : Term.Patterns.t) =
  Term.Aliases.Set_typed_relation_symbol.filter
    (fun predicate_symbol ->
      let typing_obligation = Typing_obligation.{predicate_symbol; patterns} in
      accepts recognizing_shocs typing_obligation)
    (extract_head_relation_symbols recognizing_shocs)

(* module Set_body = Misc.My_set.Make (Misc.My_set.Make (Formal_atom)) *)

let complement_for_one_head (shoc : t) (head : Shoc_head.t) : t =
  let body_of_rules_matching_head_in_dnf =
    shoc |> rules_that_match_head head |> to_list |> List.map Shoc.get_body
  in

  (* let body_of_rules_matching_head_in_dnf =
       body_of_rules_that_match_head head shocs |> List.map Rrc.Body.to_list
     in *)
  (* Same representation between cnf and dnf *)
  let dual_formula_in_cnf = body_of_rules_matching_head_in_dnf in
  let dual_formula_in_dnf =
    dual_formula_in_cnf |> Shoc.Set_body.of_list |> Shoc_body.unordered_cartesian_product
    |> Shoc.Set_body.to_list
    (* |> Shoc.Set_body.to_list *)
  in
  (* let dual_formula_in_dnf = dual_formula_in_cnf |> Misc.List_op.cartesian_product_list in *)
  let new_rules = List.rev_map (fun body -> (head, body)) dual_formula_in_dnf in
  of_list new_rules

let complement_for_one_relation_symbol
    (datatype_env : Term.Datatype_environment.t)
    (shocs : t)
    (predicate : Typed_relation_symbol.t) : t =
  let datatype_relation = Typed_relation_symbol.get_types predicate in
  let symbols_of_matching_output_datatype =
    Term.Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
      datatype_env datatype_relation
  in
  symbols_of_matching_output_datatype |> Term.Aliases.Set_typed_functions.to_seq
  |> Seq.map (fun functions -> Shoc_head.{predicate; functions})
  |> Seq.map (complement_for_one_head shocs)
  |> seq_union

let complement_wrt
    (datatype_env : Term.Datatype_environment.t)
    (predicates : Term.Aliases.Set_typed_relation_symbol.t)
    (shocs : t) : t =
  (* let complement_for_each_predicate = *)
  predicates |> Term.Aliases.Set_typed_relation_symbol.to_seq
  |> Seq.map (complement_for_one_relation_symbol datatype_env shocs)
  |> seq_union

let complement_all_defined_relations_and
    (datatype_env : Term.Datatype_environment.t)
    (shocs : t)
    (predicates : Term.Aliases.Set_typed_relation_symbol.t) : t =
  let relations_already_defined = Shocs_core.extract_defined_relations shocs in
  let relations_to_complement =
    Set_typed_relation_symbol.union predicates relations_already_defined
  in
  complement_wrt datatype_env relations_to_complement shocs
