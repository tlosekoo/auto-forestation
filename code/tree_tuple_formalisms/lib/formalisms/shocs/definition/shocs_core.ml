include Misc.My_set.Make2 (Shoc)

let pp = pp_param ~opening:"{\n  " ~closing:"\n}" ~sep:"\n  " ~pp_element:Shoc.pp
(* let typecheck (_rrs : t) = true *)

let extract_head_relation_symbols (rrs : t) =
  rrs |> to_list |> List.map Shoc.get_head_relation
  |> Term.Aliases.Set_typed_relation_symbol.of_list

let extract_defined_relations (rrs : t) : Term.Aliases.Set_typed_relation_symbol.t =
  fold
    (fun r -> Term.Aliases.Set_typed_relation_symbol.union (Shoc.extract_predicate_symbols r))
    rrs Term.Aliases.Set_typed_relation_symbol.empty
