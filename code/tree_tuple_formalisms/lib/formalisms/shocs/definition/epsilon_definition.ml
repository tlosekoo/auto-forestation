module EClause = struct
  type t = {
    head_symbol : Term.Typed_relation_symbol.t;
    body : Shoc_body.t; (* Contains too much information. We shall ignore projector_term *)
  }
  [@@deriving equal, compare]

  let pp (c : Format.formatter) (ec : t) =
    Format.fprintf c "%a :- %a" Term.Typed_relation_symbol.pp ec.head_symbol Shoc_body.pp ec.body
end

include Misc.List_maker.Make (EClause)
