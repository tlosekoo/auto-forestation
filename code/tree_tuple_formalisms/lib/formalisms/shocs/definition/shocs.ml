module Shocs = struct
  include Shocs_core
  include Shocs_matching
  include Shocs_epsilon_definition
  include Shocs_operation
  include Shocs_generator
  include Shocs_minimizer
end

include Shocs
