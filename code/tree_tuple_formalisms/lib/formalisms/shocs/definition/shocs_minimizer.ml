open Term
open Term.Aliases
open Shocs_core

module Propagator_on_predicate_symbols =
  Misc.Accessible_computation.Propagator (Typed_relation_symbol)

let bottom_up_accessible_relations (shocs : t) : Set_typed_relation_symbol.t =
  let rule_list = to_list shocs in
  let constraints =
    rule_list
    |> List.map (fun (shoc : Shoc.t) ->
           let head_predicate = Set_typed_relation_symbol.singleton (Shoc.get_head_relation shoc) in
           let body_predicates = Shoc.extract_predicate_symbols_body shoc in
           (body_predicates, head_predicate))
  in
  let set_constraints = Propagator_on_predicate_symbols.Set_constraints.of_list constraints in
  let initially_accessible_relations = Set_typed_relation_symbol.empty in
  let _unused_rules, accessible_relations =
    Propagator_on_predicate_symbols.propagate (set_constraints, initially_accessible_relations)
  in
  accessible_relations

let up_bottom_accessible_relations
    (shocs : Shocs_core.t)
    (initially_accessible_relations : Set_typed_relation_symbol.t) : Set_typed_relation_symbol.t =
  let rule_list = to_list shocs in
  let constraints =
    rule_list
    |> List.map (fun (shoc : Shoc.t) ->
           let head_predicate = Set_typed_relation_symbol.singleton (Shoc.get_head_relation shoc) in
           let body_predicates = Shoc.extract_predicate_symbols_body shoc in
           (head_predicate, body_predicates))
  in
  let set_constraints = Propagator_on_predicate_symbols.Set_constraints.of_list constraints in
  let _unused_rules, accessible_relations =
    Propagator_on_predicate_symbols.propagate (set_constraints, initially_accessible_relations)
  in
  accessible_relations

(* let minimize (shocs : Shocs_core.t) : Shocs_core.t =
   let forward_accessible_states = bottom_up_accessible_relations shocs in
   (* let backwards_accessible_states = up_bottom_accessible_relations prrs in *)
   (* let accessible_both_forward_and_backward =
        Set_typed_relation_symbol.inter forward_accessible_states backwards_accessible_states
      in *)
   let rule_is_useful (r : Shoc.t) =
     Set_typed_relation_symbol.subset (Shoc.extract_predicate_symbols r) forward_accessible_states
   in
   let minimized_shocs = filter rule_is_useful shocs in
   minimized_shocs *)

let minimize_while_preserving (shocs : Shocs_core.t) (relations : Set_typed_relation_symbol.t) :
    Shocs_core.t =
  let forward_accessible_states = bottom_up_accessible_relations shocs in
  let backwards_accessible_states = up_bottom_accessible_relations shocs relations in
  let accessible_both_forward_and_backward =
    Set_typed_relation_symbol.inter forward_accessible_states backwards_accessible_states
  in
  let rule_is_useful (r : Shoc.t) =
    Set_typed_relation_symbol.subset
      (Shoc.extract_predicate_symbols r)
      accessible_both_forward_and_backward
  in
  let minimized_shocs = filter rule_is_useful shocs in
  minimized_shocs
