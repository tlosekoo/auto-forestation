open Shocs_core
open Term

let rules_that_match_symbol (ts : Term.Typed_functions.t) (rrs : t) : t =
  filter (Shoc.matches_symbols ts) rrs

let rules_that_match_predicate_symbol (ps : Typed_relation_symbol.t) (rrs : t) : t =
  filter (Shoc.matches_predicate_symbol ps) rrs

let rules_that_match_head (head : Shoc_head.t) (rrs : t) : t =
  filter (Shoc.matches_head_internal head) rrs
