open Term
open Term.Aliases
open Shocs_core

let none_in_a_type (datatypes : Datatypes.t) : t * Typed_relation_symbol.t =
  let state = Clause.Relation_generator.create_canonical_relation_from_datatypes datatypes in
  (empty, state)

let none_in_a_relation (relation : Typed_relation_symbol.t) = (empty, relation)

let all_in_a_type (datatype_env : Datatype_environment.t) (datatypes : Datatypes.t) :
    t * Typed_relation_symbol.t =
  let alphabet =
    Datatype_environment.extract_typed_functions_whose_output_type_is_some_tuple_of_datatypes
      datatype_env datatypes
  in
  let relation_symbol =
    Clause.Relation_generator.create_canonical_relation_from_datatypes datatypes
  in
  let clauses =
    alphabet |> Set_typed_functions.to_list
    |> List.map (fun functions -> Shoc.create relation_symbol functions Shoc_body.empty)
    |> of_list
  in
  (clauses, relation_symbol)

let indentity_shoc_for_a_symbol (f : Typed_function.t) : Shoc.t =
  let datatype = Typed_function.get_output_type f in
  let relation = Clause.Relation_generator.create_relation_for_identity datatype in
  let functions = [f; f] in
  let projectors_each_preciate =
    List.mapi
      (fun i d ->
        ( Clause.Relation_generator.create_relation_for_identity d,
          [
            Projector.{projector_term = 0; projector_child = i};
            Projector.{projector_term = 1; projector_child = i};
          ] ))
      (Typed_function.get_input_types f)
  in
  let body_atoms =
    List.map
      (fun (corresponding_relation, projectors_one_predicate) ->
        Formal_atom.{predicate = corresponding_relation; projectors = projectors_one_predicate})
      projectors_each_preciate
  in
  let body = Shoc_body.of_list body_atoms in
  let clause = Shoc.create relation functions body in
  clause

let identity_shocs_for_symbols (fs : Set_typed_function.t) : t =
  fs |> Set_typed_function.to_list |> List.map indentity_shoc_for_a_symbol |> of_list

let identities_for_datatypes_no_dependency
    (env : Datatype_environment.t)
    (datatypes : Set_datatype.t) : t =
  datatypes |> Set_datatype.to_list
  |> List.map (Datatype_environment.get_constructors_of_datatype env)
  |> Set_typed_function.list_union |> identity_shocs_for_symbols

let identity_in_a_type (env : Datatype_environment.t) (dt : Datatype.t) :
    t * Typed_relation_symbol.t =
  let type_dependencies_of_dt = Datatype_environment.get_type_dependencies env dt in
  let final_state = Clause.Relation_generator.create_relation_for_identity dt in
  let clauses = identities_for_datatypes_no_dependency env type_dependencies_of_dt in
  (clauses, final_state)

let identities (env : Datatype_environment.t) : t =
  let all_datatypes = Datatype_environment.get_datatypes env in
  identities_for_datatypes_no_dependency env all_datatypes
