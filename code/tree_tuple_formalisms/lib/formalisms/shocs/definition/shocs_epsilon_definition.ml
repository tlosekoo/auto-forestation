open Term
open Shocs_core
open Shocs_matching

let convert_epsilon_clause (rrs : t) (epsilon_clause : Epsilon_definition.EClause.t) : t =
  let head_predicate = epsilon_clause.head_symbol in

  let relations_in_epsilon_clause = Shoc_body.extract_predicates epsilon_clause.body in
  let map_predicate_symbol_to_list_of_rule_bodies_and_their_head_symbols =
    relations_in_epsilon_clause |> Term.Aliases.Set_typed_relation_symbol.to_list
    |> List.map (fun (ps : Typed_relation_symbol.t) ->
           let rules_matching_predicate_symbol =
             to_list (rules_that_match_predicate_symbol ps rrs)
           in
           let rules_matching_predicate_symbol_and_their_symbols =
             List.map
               (fun rule ->
                 (Shoc.get_functions_symbols rule, Shoc_body.to_list (Shoc.get_body rule)))
               rules_matching_predicate_symbol
           in
           (ps, rules_matching_predicate_symbol_and_their_symbols))
  in
  let list_of_rules_and_their_head_symbols =
    List.map snd map_predicate_symbol_to_list_of_rule_bodies_and_their_head_symbols
  in
  let all_possible_combination =
    Misc.List_op.join_list_of_relations Term.Typed_functions.compare
      list_of_rules_and_their_head_symbols
  in
  let all_rules_per_symbol =
    List.map
      (fun (functions, body_atom_list_listAnd_listOr) ->
        let bodies_listAnd_listOr =
          List.map (List.map Shoc_body.of_list) body_atom_list_listAnd_listOr
        in
        let bodies_listOr = List.map Shoc_body.list_union bodies_listAnd_listOr in
        let rules_listOr = List.map (Shoc.create head_predicate functions) bodies_listOr in
        rules_listOr)
      all_possible_combination
  in
  let all_rules = List.flatten all_rules_per_symbol in
  of_list all_rules

let add_epsilon_definition (rrs : t) (epsilon_definition : Epsilon_definition.t) : t =
  epsilon_definition |> List.rev_map (convert_epsilon_clause rrs) |> list_union |> union rrs
