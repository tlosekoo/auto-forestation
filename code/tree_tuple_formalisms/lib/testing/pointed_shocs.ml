open Term
open Term.Aliases
include Misc.Pair.Make (Shocs) (Term.Typed_relation_symbol)

let get_datatype ((_, relation) : t) = Typed_relation_symbol.get_types relation

let accepts ((shocs, relation) : t) (patterns : Patterns.t) : bool =
  Shocs.accepts shocs (Typing_obligation.create relation patterns)

let complement (env : Datatype_environment.t) ((shocs, relation) : t) : t =
  let the_complement =
    Shocs.complement_all_defined_relations_and env shocs
      (Set_typed_relation_symbol.singleton relation)
  in
  (the_complement, relation)

let minimize ((shocs, relation) : t) : t =
  let the_minimized =
    Shocs.minimize_while_preserving shocs (Set_typed_relation_symbol.singleton relation)
  in
  (the_minimized, relation)

let none_in_a_type = Shocs.none_in_a_type
let identity_in_a_type = Shocs.identity_in_a_type
let all_in_a_type = Shocs.all_in_a_type
