let equivalent_automata_and_prrs =
  [
    (Database_for_tests_automata.a_nat_1, Database_for_tests_prrs.a_nat_1);
    (Database_for_tests_automata.a_odd, Database_for_tests_prrs.a_odd);
    (Database_for_tests_automata.a_even, Database_for_tests_prrs.a_even);
    (Database_for_tests_automata.a_natlist_1, Database_for_tests_prrs.a_natlist_1);
    (Database_for_tests_automata.a_list_size, Database_for_tests_prrs.a_list_size);
    ( Database_for_tests_automata.a_lists_of_z_and_their_size,
      Database_for_tests_prrs.a_lists_of_z_and_their_size );
  ]
