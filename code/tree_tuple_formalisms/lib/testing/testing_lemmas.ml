open Term
open Term.Aliases

(* Check that prrs and patterns have same type *)
let same_type_as_patterns (r : Pointed_shocs.t) (patterns : Term.Patterns.t) : bool =
  Datatypes.equal (Pointed_shocs.get_datatype r) (Patterns.get_type patterns)

let recognize_same_language
    (a1 : Pointed_shocs.t)
    (a2 : Pointed_shocs.t)
    (set_patterns : Aliases.Set_patterns.t) : bool =
  set_patterns
  |> Aliases.Set_patterns.for_all (fun patterns ->
         let a1_recognizes_patterns = Pointed_shocs.accepts a1 patterns in
         let a2_recognizes_patterns = Pointed_shocs.accepts a2 patterns in
         Bool.equal a1_recognizes_patterns a2_recognizes_patterns)

let recognize_complementary_languages_of_the_same_type
    (a1 : Pointed_shocs.t)
    (a2 : Pointed_shocs.t)
    (terms : Set_patterns.t) : bool =
  let same_type =
    Term.Datatypes.equal (Pointed_shocs.get_datatype a1) (Pointed_shocs.get_datatype a2)
  in
  if same_type then
    terms
    |> Set_patterns.filter (same_type_as_patterns a1)
    |> Set_patterns.for_all (fun t ->
           let a1_recognizes_t = Pointed_shocs.accepts a1 t in
           let a2_recognizes_t = Pointed_shocs.accepts a2 t in
           let ok = not (Bool.equal a1_recognizes_t a2_recognizes_t) in
           if not ok then (
             Format.fprintf Format.std_formatter "recognizes the term (a_1, a_2):   (%b, %b)\n"
               a1_recognizes_t a2_recognizes_t;
             Format.fprintf Format.std_formatter "Term: %a\n" Term.Patterns.pp t;
             Format.fprintf Format.std_formatter "a_1:\n %a\n\na_2:\n%a\n\n" Pointed_shocs.pp a1
               Pointed_shocs.pp a2);
           ok)
  else
    false

let complement_recognize_complement_language
    (env : Term.Datatype_environment.t)
    (terms : Set_patterns.t)
    (r : Pointed_shocs.t) : bool =
  let complement_r = Pointed_shocs.complement env r in
  recognize_complementary_languages_of_the_same_type r complement_r terms

let doubly_complement_recognize_same_language
    (env : Term.Datatype_environment.t)
    (terms : Set_patterns.t)
    (r : Pointed_shocs.t) : bool =
  let doubly_complement_r = r |> Pointed_shocs.complement env |> Pointed_shocs.complement env in
  recognize_same_language r doubly_complement_r terms

let minimized_recognize_same_language (terms : Set_patterns.t) (r : Pointed_shocs.t) : bool =
  let minimized_r = Pointed_shocs.minimize r in
  let ok = recognize_same_language r minimized_r terms in
  if not ok then (
    (* Format.fprintf Format.std_formatter "Term: %a\n" Term.Convoluted_term.pp ct; *)
    Format.fprintf Format.std_formatter "Shocs: %a\n" Pointed_shocs.pp r;
    Format.fprintf Format.std_formatter "Reachable Shocs: %a\n\n" Pointed_shocs.pp minimized_r);
  ok

let nothing_in_a_type_recognizes_nothing
    (datatype : Term.Datatypes.t)
    (set_of_patterns : Term.Aliases.Set_patterns.t) : bool =
  let r = Pointed_shocs.none_in_a_type datatype in
  let well_typed_patterns =
    Term.Aliases.Set_patterns.filter
      (fun patterns -> Term.Datatypes.equal datatype (Term.Patterns.get_type patterns))
      set_of_patterns
  in
  let some_term_is_recognized =
    Term.Aliases.Set_patterns.exists (Pointed_shocs.accepts r) well_typed_patterns
  in
  not some_term_is_recognized

let all_in_a_type_recognize_all_of_this_datatype
    (datatype_environment : Datatype_environment.t)
    (datatype : Datatypes.t)
    (set_of_patterns : Set_patterns.t) : bool =
  let r = Pointed_shocs.all_in_a_type datatype_environment datatype in
  let well_typed_patterns =
    Term.Aliases.Set_patterns.filter
      (fun patterns -> Term.Datatypes.equal datatype (Term.Patterns.get_type patterns))
      set_of_patterns
  in
  Term.Aliases.Set_patterns.for_all (Pointed_shocs.accepts r) well_typed_patterns

let identity_in_a_type_recognizes_only_diagonal_of_this_datatype
    (datatype_environment : Datatype_environment.t)
    (datatype : Datatype.t)
    (set_of_patterns : Set_patterns.t) : bool =
  let r = Pointed_shocs.identity_in_a_type datatype_environment datatype in
  let well_typed_patterns =
    Term.Aliases.Set_patterns.filter
      (fun patterns -> Term.Datatypes.equal [datatype; datatype] (Term.Patterns.get_type patterns))
      set_of_patterns
  in
  Term.Aliases.Set_patterns.for_all
    (fun patterns ->
      match patterns with
      | [p1; p2] ->
          let p1_equals_p2 = Pattern.equal p1 p2 in
          let r_accepts = Pointed_shocs.accepts r patterns in
          let ok = Bool.equal p1_equals_p2 r_accepts in
          if not ok then
            Format.fprintf Format.std_formatter "Arg: \n%a\n%a\n" Pointed_shocs.pp r Patterns.pp
              [p1; p2]
          else
            ();
          ok
      (* | _ ->
          Format.fprintf Format.std_formatter
            "There is not exactly two patterns. This is wierd.\n Patterns are\n%a\ntheir datatype is:\n%a\n"
            Patterns.pp patterns Datatypes.pp (Patterns.get_type patterns);
          false *)
      | _ -> false)
    well_typed_patterns

let recognize_same_language
    (a1 : Pointed_shocs.t)
    (a2 : Pointed_shocs.t)
    (set_patterns : Aliases.Set_patterns.t) : bool =
  set_patterns
  |> Aliases.Set_patterns.for_all (fun patterns ->
         let a1_recognizes_patterns = Pointed_shocs.accepts a1 patterns in
         let a2_recognizes_patterns = Pointed_shocs.accepts a2 patterns in
         Bool.equal a1_recognizes_patterns a2_recognizes_patterns)
