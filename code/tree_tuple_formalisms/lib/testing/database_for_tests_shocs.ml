(* open Aliases *)
open Term.Database_for_tests

(* open Clause.Database_for_tests *)
open Term.Aliases
module Set_pointed_shocs = Misc.My_set.Make (Pointed_shocs)

(* Projectors *)
let pi_00 = Projector.{projector_term = 0; projector_child = 0}
let pi_01 = Projector.{projector_term = 0; projector_child = 1}
let pi_02 = Projector.{projector_term = 0; projector_child = 2}
let pi_10 = Projector.{projector_term = 1; projector_child = 0}
let pi_11 = Projector.{projector_term = 1; projector_child = 1}
let pi_12 = Projector.{projector_term = 1; projector_child = 2}
let pi_20 = Projector.{projector_term = 2; projector_child = 0}
let pi_21 = Projector.{projector_term = 2; projector_child = 1}
let pi_22 = Projector.{projector_term = 2; projector_child = 2}

(* binary *)
(* let [nil_symbol]_z = Typed_functions.create [Database_for_tests.nil_symbol; Database_for_tests.z_symbol]
   let [cons_symbol]_s = Typed_functions.create [Database_for_tests.cons_symbol; Database_for_tests.s_symbol] *)
let rule_z_in__re_n = Shoc.create_from_list relation_even [z_symbol] []

let rule_s_ro_in__re_n =
  Shoc.create_from_list relation_even [s_symbol]
    [Formal_atom.{predicate = relation_odd; projectors = [pi_00]}]

let rule_s_re_in__ro_n =
  Shoc.create_from_list relation_odd [s_symbol]
    [Formal_atom.{predicate = relation_even; projectors = [pi_00]}]

let rule_z_in__r0_n = Shoc.create_from_list relation0_n [z_symbol] []

let rule_s_r0_n_in__r1_n =
  Shoc.create_from_list relation1_n [s_symbol]
    [Formal_atom.{predicate = relation0_n; projectors = [pi_00]}]

let rule_s_r1_n_in__r2_n =
  Shoc.create_from_list relation2_n [s_symbol]
    [Formal_atom.{predicate = relation1_n; projectors = [pi_00]}]

let rule_s_r2_n_in__r0_n =
  Shoc.create_from_list relation0_n [s_symbol]
    [Formal_atom.{predicate = relation2_n; projectors = [pi_00]}]

let rule_nil_in__r_le_l = Shoc.create_from_list relation_le_l [nil_symbol] []
let rule_nil_in__r_l = Shoc.create_from_list relation_l [nil_symbol] []

let rule_cons_rn_rlo_in__r_le_l =
  Shoc.create_from_list relation_le_l [cons_symbol]
    [
      Formal_atom.{predicate = relation_n; projectors = [pi_00]};
      Formal_atom.{predicate = relation_lo_l; projectors = [pi_01]};
    ]

let rule_cons_rn_rle_in__r_lo_l =
  Shoc.create_from_list relation_lo_l [cons_symbol]
    [
      Formal_atom.{predicate = relation_n; projectors = [pi_00]};
      Formal_atom.{predicate = relation_le_l; projectors = [pi_01]};
    ]

let rule_cons_rn_rl_in__r_l =
  Shoc.create_from_list relation_l [cons_symbol]
    [
      Formal_atom.{predicate = relation_n; projectors = [pi_00]};
      Formal_atom.{predicate = relation_l; projectors = [pi_01]};
    ]

let rule_z_in__r_n = Shoc.create_from_list relation_n [z_symbol] []

let rule_s_rn_in__r_n =
  Shoc.create_from_list relation_n [s_symbol]
    [Formal_atom.{predicate = relation_n; projectors = [pi_00]}]

let rule_nil_and_z_in__r_l_n = Shoc.create_from_list relation_l_n nil_z []
(* let rule_lin_and_z_in__r_rl_n = Shoc.create relation_l_n .lin_z [] *)

let rule_cons_and_s_of_rn_rln_in__r_l_n =
  Shoc.create_from_list relation_l_n cons_s
    [
      Formal_atom.{predicate = relation_n; projectors = [pi_00]};
      Formal_atom.{predicate = relation_l_n; projectors = [pi_01; pi_10]};
    ]

let some_rules =
  Shocs.of_list
    [
      rule_s_ro_in__re_n;
      rule_s_re_in__ro_n;
      rule_z_in__r0_n;
      rule_s_r0_n_in__r1_n;
      rule_s_r1_n_in__r2_n;
      rule_s_r2_n_in__r0_n;
      rule_nil_in__r_le_l;
      rule_nil_in__r_l;
      rule_cons_rn_rlo_in__r_le_l;
      rule_cons_rn_rle_in__r_lo_l;
      rule_cons_rn_rl_in__r_l;
      rule_z_in__r_n;
      rule_s_rn_in__r_n;
      rule_nil_and_z_in__r_l_n;
      rule_cons_and_s_of_rn_rln_in__r_l_n;
    ]

let a_nat_1 = (Shocs.of_list [rule_z_in__r_n; rule_s_rn_in__r_n], relation_n)

(* let a_nat_2 =
   Automata_generator.generate_automaton_recognizing_some_type Database_for_tests.some_type_env
     Database_for_tests.nat *)

let a_even = (Shocs.of_list [rule_z_in__re_n; rule_s_re_in__ro_n; rule_s_ro_in__re_n], relation_even)

let a_mod3 =
  ( Shocs.of_list [rule_z_in__r0_n; rule_s_r0_n_in__r1_n; rule_s_r1_n_in__r2_n; rule_s_r2_n_in__r0_n],
    relation0_n )

let a_odd = (Shocs.of_list [rule_z_in__re_n; rule_s_re_in__ro_n; rule_s_ro_in__re_n], relation_odd)

let a_natlist_1 =
  ( Shocs.of_list [rule_z_in__r_n; rule_s_rn_in__r_n; rule_nil_in__r_l; rule_cons_rn_rl_in__r_l],
    relation_l )

let a_list_size =
  ( Shocs.of_list
      [
        rule_z_in__r_n;
        rule_s_rn_in__r_n;
        rule_nil_and_z_in__r_l_n;
        rule_cons_and_s_of_rn_rln_in__r_l_n;
      ],
    relation_l_n )

let a_lists_of_z_and_their_size =
  ( Shocs.of_list [rule_z_in__r_n; rule_nil_and_z_in__r_l_n; rule_cons_and_s_of_rn_rln_in__r_l_n],
    relation_l_n )

let shocs_mod2_and_mod3 = Shocs.union (fst a_even) (fst a_mod3)

let epsilon_definition_q0_and_qe_in_qn =
  let body_atom_qe = Formal_atom.{predicate = relation_even; projectors = [pi_00]} in
  let body_atom_q0 = Formal_atom.{predicate = relation0_n; projectors = [pi_00]} in
  let epsilon_clause =
    Epsilon_definition.EClause.
      {head_symbol = relation_n; body = Shoc_body.of_list [body_atom_qe; body_atom_q0]}
  in
  [epsilon_clause]

let prrs_mod6 =
  let rrs_with_new_predicate =
    Shocs.add_epsilon_definition shocs_mod2_and_mod3 epsilon_definition_q0_and_qe_in_qn
  in
  (rrs_with_new_predicate, relation_n)

let complement_prrs_mod_6 =
  ( Shocs.complement_all_defined_relations_and testing_type_env (fst prrs_mod6)
      (Set_typed_relation_symbol.singleton relation_n),
    relation_n )

let complement_prrs_list_and_its_size =
  ( Shocs.complement_all_defined_relations_and testing_type_env (fst a_list_size)
      (Set_typed_relation_symbol.singleton (snd a_list_size)),
    snd a_list_size )

let a_natlist_nat = Shocs.all_in_a_type testing_type_env c_natlist_nat

let some_shocs =
  Set_pointed_shocs.of_list
    [
      a_nat_1;
      a_even;
      a_odd;
      a_natlist_1;
      a_list_size;
      a_lists_of_z_and_their_size;
      a_natlist_nat;
      a_mod3;
      prrs_mod6;
      complement_prrs_mod_6;
      complement_prrs_list_and_its_size;
    ]

let rule_shallower_leaf_z = Shoc.create_from_list relation_shallower [elt_leaf_symbol; z_symbol] []
let rule_shallower_leaf_s = Shoc.create_from_list relation_shallower [elt_leaf_symbol; s_symbol] []

let rule_shallower_node_s =
  Shoc.create_from_list relation_shallower [elt_node_symbol; s_symbol]
    [
      Formal_atom.{predicate = relation_shallower; projectors = [pi_00; pi_10]};
      Formal_atom.{predicate = relation_shallower; projectors = [pi_02; pi_10]};
    ]

let rule_height_leaf_z = Shoc.create_from_list relation_height [elt_leaf_symbol; z_symbol] []

let rule_height_node_s__1 =
  Shoc.create_from_list relation_height [elt_node_symbol; s_symbol]
    [
      Formal_atom.{predicate = relation_shallower; projectors = [pi_00; pi_10]};
      Formal_atom.{predicate = relation_height; projectors = [pi_02; pi_10]};
    ]

let rule_height_node_s__2 =
  Shoc.create_from_list relation_height [elt_node_symbol; s_symbol]
    [
      Formal_atom.{predicate = relation_shallower; projectors = [pi_02; pi_10]};
      Formal_atom.{predicate = relation_height; projectors = [pi_00; pi_10]};
    ]

let rule_le_z_s = Shoc.create_from_list relation_le [z_symbol; s_symbol] []

let rule_le_s_s =
  Shoc.create_from_list relation_le [s_symbol; s_symbol]
    [Formal_atom.{predicate = relation_le; projectors = [pi_00; pi_10]}]

let shocs_shallower =
  Shocs.of_list [rule_shallower_leaf_z; rule_shallower_leaf_s; rule_shallower_node_s]

let shocs_le = Shocs.of_list [rule_le_s_s; rule_le_z_s]

let shocs_height_alone =
  Shocs.of_list [rule_height_leaf_z; rule_height_node_s__1; rule_height_node_s__2]

let shocs_height_and_shallower_and_le =
  Shocs.list_union [shocs_height_alone; shocs_shallower; shocs_le]
