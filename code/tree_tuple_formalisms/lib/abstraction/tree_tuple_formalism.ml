open Clause.Aliases
open Term

(* module type Typed_comparable_and_printable = sig
     type t

     include Misc.Ordered_and_printable.OP with type t := t

     val get_datatype : t -> Term.Datatypes.t
   end *)

module type Acceptor = sig
  type t

  val accepts : t -> Typing_obligation.t -> bool
  val accepts_in : t -> Patterns.t -> Set_typed_relation_symbol.t
end

module type Complementable = sig
  type t

  val complement_all_defined_relations_and :
    Datatype_environment.t -> t -> Set_typed_relation_symbol.t -> t
end

module type Minimizable = sig
  type t

  val minimize_while_preserving : t -> Set_typed_relation_symbol.t -> t
end

module type Generatable = sig
  type t

  val all_in_a_type : Datatype_environment.t -> Datatypes.t -> t * Typed_relation_symbol.t

  (** every and only pairs of term '(t, t)' with 't' of the given type. *)
  val identity_in_a_type : Datatype_environment.t -> Datatype.t -> t * Typed_relation_symbol.t

  val none_in_a_type : Term.Datatypes.t -> t * Typed_relation_symbol.t
  val identities : Datatype_environment.t -> t
end

module type Infos = sig
  type t

  val extract_defined_relations : t -> Set_typed_relation_symbol.t
end

(* module type RuleFul = sig
     type t
   end *)

module type S = sig
  module Rule : Shallow_rule.S
  include Misc.My_set.S with type elt := Rule.t
  include Misc.Ordered_and_printable.OP with type t := t
  include Acceptor with type t := t
  include Complementable with type t := t
  include Minimizable with type t := t
  include Generatable with type t := t
  include Infos with type t := t
  (* include RuleFul with type t := t *)
end
