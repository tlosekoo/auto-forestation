open Aliases_typing_obligation
open Term
open Term.Aliases

module type S = sig
  type t

  include Misc.Ordered_and_printable.OP with type t := t

  val get_functions_symbols : t -> Typed_functions.t
  val get_head_relation : t -> Typed_relation_symbol.t
  val matches_head : Typed_relation_symbol.t -> Typed_functions.t -> t -> bool
  val unify_with_patterns : Patterns.t -> t -> Map_typedSymbol_typedFunction.t option
  val unfold_no_head_check : Patterns.t -> t -> Set_typing_obligation.t
end

(* module type S = sig
     include Tree_tuple_formalism.S
     module Rule : S
     (* module SetRules : Misc.My_set.S with type elt := Rule.t *)

     (* val get_rules : t -> SetRules.t *)
   end *)
