open Term

type t = {
  predicate_symbol : Typed_relation_symbol.t;
  patterns : Patterns.t;
}
[@@deriving compare, equal]

let pp (c : Format.formatter) (typing_obligation : t) : unit =
  Format.fprintf c "%a(%a)" Typed_relation_symbol.pp typing_obligation.predicate_symbol
    Patterns.pp_without_brackets typing_obligation.patterns

let create (predicate_symbol : Typed_relation_symbol.t) (patterns : Patterns.t) =
  {predicate_symbol; patterns}
