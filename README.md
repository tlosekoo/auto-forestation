# auto-forestation #

*auto-forestation* is an automatic prover for relational properties on programs manipulating algebraic datatypes.
It is implemented as an iterative model refinement procedure, 
where a model is an approximation of every relation by a either a relational tree automaton or a set of Shallow Horn Clauses.

## Experimental results on the benchmarks examples ##

We provide four tables of experiments.
There is one for Shallow Horn Clauses and three for the three different convolutions (Right/Left/Complete).
These tables can be accessed from [this page](/website/benchmarks_results)

<!-- [Instructions for reading the trace of the solver](http://people.irisa.fr/Thomas.Genet/AutoForestation/note.pdf) -->

## Installation ##

- Install [clingo](https://github.com/potassco/clingo) and its dependencies. Make sure that clingo is in your path ; 
- Install [opam](https://opam.ocaml.org/doc/Install.html) with ocaml version >=14.0 ;
- Install solver dependencies: `opam install dune dune-site ppx_expect menhir smtlib-utils` ;
- Pin every dune project (in order): go in folder `code` and run `sh pin.sh`
  
## How to use ##

First, go to the `code/model_inference` folder.

`dune build`

For running the model inference procedure on a specific input, run
 ``dune exec main -- ../../benchmarks/smtlib/true/length_cons_le.smt2

For running the model inference procedure on the whole database, run
 `dune exec main -- -benchmarks`

Solver parameters (both for single instance and whole database) can be found in file `code/model_inference/bin/parameters.ml`. 

## How to consult benchmarks results ##

open `code/model_inference/results/benchmarks.html`

## How to switch on/switch of approximation ##

In the file `code/model_inference/bin/parameters.ml` set `approximation_method` to either:

- `Program_to_clauses.Approximation.No_approximation` (no approximation), or to
- `Program_to_clauses.Approximation.Remove_functionnality_constraint_where_possible` (removes functionnality clauses each time that it is possible)
